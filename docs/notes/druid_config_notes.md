Druid Config Notes
-

- Druid 19
- No G1GC
- Split across two boxes (72 core, 784 GB Ram = $30k)

Peons

- 48 threads
- 48 Merge buffers
- 100 MB of ram per Merge Buffer

General
- Broker must use /dev/shm (shared memory)
    - as the Java temp drive
- Same with historical and Peons
- Speeds up disk usage, apparently

With this, on 1.1 million rows, can do 50ms query speeds

Ingestion
- Need my flat protobuf trick, otherwise they 
- go to the JSON format first and then ingest the JSON

String Match
- Need string match to do logs of things
- Hidden Instrument vs. Closed Instrument 
- (Set Closed if row is closed for securities)
