import com.google.protobuf.Timestamp;
import com.google.type.Date;
import com.hedgeos.curve.*;
import com.hedgeos.daycount.DayCount;
import com.hedgeos.general.Currency;
import com.hedgeos.general.MarketData;
import com.hedgeos.general.Quantity;
import com.hedgeos.general.SecurityType;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.position.Position;
import com.hedgeos.security.FixedFloatLegFlag;
import com.hedgeos.security.InterestRateSwap;
import com.hedgeos.security.Security;
import com.hedgeos.security.SecurityUnion;
import com.hedgeos.service.curve.Measure;
import com.hedgeos.service.curve.PositionPriceRequest;
import com.hedgeos.service.curve.PositionPriceResponse;
import com.hedgeos.service.curve.PositionPriceResult;
import com.hedgeos.service.pricing.PricingServer;
import io.grpc.stub.StreamObserver;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;

public class PricingServerTest {
    private static final String STAR_LINE = "************************************************";

    public static class PositionPriceStreamObserverInProcess implements StreamObserver<PositionPriceResponse> {
        public PositionPriceResponse positionPriceResponse;
        public Throwable error;
        boolean completed = false;

        @Override
        public void onNext(PositionPriceResponse positionPriceResponse) {
            this.positionPriceResponse = positionPriceResponse;
        }

        @Override
        public void onError(Throwable throwable) {
            this.error = throwable;
        }

        @Override
        public void onCompleted() {
            completed = true;
        }
    }

    static class CurvePointDatum {
        public final LocalDate date;
        public final double value;

        public CurvePointDatum(String date, double value) {
            this.date = LocalDate.parse(date);
            this.value = value;
        }
    }

    @Test
    public void SWAP_PRICE_TEST() {
        // mock representation of HedgeOS curves that have been previously created

        // 2020-09-30 USD-FED-FUNDS
        CurvePointDatum[] usdFedFundsCurveData = new CurvePointDatum[]{
                new CurvePointDatum("2020-11-02", 8.364286230423974E-4),
                new CurvePointDatum("2020-12-01", 8.119126109410221E-4),
                new CurvePointDatum("2021-01-04", 7.838480584086855E-4),
                new CurvePointDatum("2021-02-02", 7.468679329980909E-4),
                new CurvePointDatum("2021-03-02", 7.166396951309486E-4),
                new CurvePointDatum("2021-04-01", 6.863079242105741E-4),
                new CurvePointDatum("2021-05-03", 6.555627982081224E-4),
                new CurvePointDatum("2021-06-02", 6.344255159044168E-4),
                new CurvePointDatum("2021-07-01", 6.183198609788386E-4),
                new CurvePointDatum("2021-08-03", 5.925623774090168E-4),
                new CurvePointDatum("2021-09-01", 5.743825425230338E-4),
                new CurvePointDatum("2021-10-01", 5.584665909920533E-4),
                new CurvePointDatum("2021-11-02", 5.40011939093682E-4),
                new CurvePointDatum("2022-01-03", 5.284426976992931E-4),
                new CurvePointDatum("2022-04-04", 4.575263670737048E-4),
                new CurvePointDatum("2022-10-03", 3.789593011230457E-4),
                new CurvePointDatum("2023-10-02", 4.568533098875764E-4),
                new CurvePointDatum("2024-10-02", 8.267172781809777E-4),
                new CurvePointDatum("2025-10-02", 0.0014523112800708465),
                new CurvePointDatum("2026-10-02", 0.004220509250091412),
                new CurvePointDatum("2027-10-04", 0.0050128387365161115),
                new CurvePointDatum("2028-10-02", 0.005768183615193283),
                new CurvePointDatum("2029-10-02", 0.006484269245820255),
                new CurvePointDatum("2030-10-02", 0.007139143470877222),
                new CurvePointDatum("2032-10-04", 0.008274194943101613),
                new CurvePointDatum("2035-10-02", 0.009436025562394292),
                new CurvePointDatum("2040-10-02", 0.010602882583037843),
                new CurvePointDatum("2045-10-02", 0.01109014578813196),
                new CurvePointDatum("2050-10-03", 0.011318947468661831),
        };

        // 2020-09-30 USD-LIBOR-1M
        CurvePointDatum[] usdLibor1MCurveData = new CurvePointDatum[]{
                new CurvePointDatum("2020-11-02", 0.0015029943438989294),
                new CurvePointDatum("2020-12-02", 0.0015210987113987921),
                new CurvePointDatum("2021-01-04", 0.0016011202743442773),
                new CurvePointDatum("2021-02-02", 0.001624524398954015),
                new CurvePointDatum("2021-03-02", 0.0016110978149795829),
                new CurvePointDatum("2021-04-06", 0.0015616624373741494),
                new CurvePointDatum("2021-07-02", 0.0014884080763993125),
                new CurvePointDatum("2021-10-04", 0.0014433304508919473),
                new CurvePointDatum("2022-04-04", 0.0013898075903128959),
                new CurvePointDatum("2022-10-03", 0.001323262058174483),
                new CurvePointDatum("2023-10-02", 0.0014083121732735653),
                new CurvePointDatum("2024-10-02", 0.0017819245827688506),
                new CurvePointDatum("2025-10-02", 0.0023808960275201944),
                new CurvePointDatum("2026-10-02", 0.0031147505483816486),
                new CurvePointDatum("2027-10-04", 0.003896266887721968),
                new CurvePointDatum("2028-10-02", 0.004652445576972995),
                new CurvePointDatum("2029-10-02", 0.0053693197627897255),
                new CurvePointDatum("2030-10-02", 0.006031353931435999),
                new CurvePointDatum("2032-10-04", 0.007188613578968693),
                new CurvePointDatum("2035-10-02", 0.008383698999705999),
                new CurvePointDatum("2040-10-02", 0.009594475804875524),
                new CurvePointDatum("2045-10-02", 0.010086663207875662),
                new CurvePointDatum("2050-10-03", 0.010284867792959049),
        };

        // USD-LIBOR-6M 2020-09-30)
        CurvePointDatum[] usdLibor6MCurveData = new CurvePointDatum[]{
                new CurvePointDatum("2021-04-06", 0.003068036912918883),
                new CurvePointDatum("2021-10-04", 0.0025929423393214767),
                new CurvePointDatum("2022-10-03", 0.0028840782266584875),
                new CurvePointDatum("2023-10-02", 0.0033321392601229458),
                new CurvePointDatum("2024-10-02", 0.00390902743790659),
                new CurvePointDatum("2025-10-02", 0.00460669656675971),
                new CurvePointDatum("2026-10-02", 0.005427317006737215),
                new CurvePointDatum("2027-10-04", 0.006274080889562271),
                new CurvePointDatum("2028-10-02", 0.007065533813151128),
                new CurvePointDatum("2029-10-02", 0.007824693651326506),
                new CurvePointDatum("2030-10-02", 0.008524772681009506),
                new CurvePointDatum("2032-10-04", 0.009723612224918628),
                new CurvePointDatum("2035-10-02", 0.01097009383083292),
                new CurvePointDatum("2040-10-02", 0.012240350437537921),
                new CurvePointDatum("2045-10-02", 0.012736777524994308),
                new CurvePointDatum("2050-10-03", 0.012963215544830913),
        };

        // USD-LIBOR-3M 2020-09-30)
        CurvePointDatum[] usdLibor3MCurveData = new CurvePointDatum[]{
                new CurvePointDatum("2021-01-04", 0.002370559572421724),
                new CurvePointDatum("2021-03-16", 0.0024449520687096367),
                new CurvePointDatum("2021-06-17", 0.002287091987774568),
                new CurvePointDatum("2021-09-16", 0.002200319164302233),
                new CurvePointDatum("2021-12-15", 0.0021496830781903434),
                new CurvePointDatum("2022-03-15", 0.0021589925580936782),
                new CurvePointDatum("2022-06-16", 0.002173317399729634),
                new CurvePointDatum("2022-10-03", 0.0021958438900191776),
                new CurvePointDatum("2023-10-02", 0.002396873484883926),
                new CurvePointDatum("2024-10-02", 0.0028217708277821062),
                new CurvePointDatum("2025-10-02", 0.0034586330427559754),
                new CurvePointDatum("2026-10-02", 0.004225640300750751),
                new CurvePointDatum("2027-10-04", 0.005027362035012697),
                new CurvePointDatum("2028-10-02", 0.005793510113155699),
                new CurvePointDatum("2029-10-02", 0.006523025929436642),
                new CurvePointDatum("2030-10-02", 0.007197331401159071),
                new CurvePointDatum("2032-10-04", 0.008368030352607816),
                new CurvePointDatum("2035-10-02", 0.009582078064539114),
                new CurvePointDatum("2040-10-02", 0.010805234142083304),
                new CurvePointDatum("2045-10-02", 0.01130379551027085),
                new CurvePointDatum("2050-10-03", 0.011524227632946553),
        };

        // mock timestamp for this price request
        Instant refTime = Instant.parse("2020-09-30T19:48:41.418Z");

        // let's price a vanilla 3M USD Libor swap
        long securityId = 1111;
        SecurityKey securityKey = SecurityKey.newBuilder().setSecurityId(securityId).build();
        Security security = Security.newBuilder()
                .setDescription("Test USD-LIBOR-3M Swap")
                .setTenor(Tenor.T1Y)
                .setBbTicker("USSWAP 3M TEST")
                .setCurrency(Currency.USD)
                .setSecurityType(SecurityType.IRS)
                .setSecurityKey(securityKey).build();

        // set start and end dates
        LocalDate startDate = LocalDate.of(2020, 9, 29);
        LocalDate terminationDate = LocalDate.of(2021, 9, 29);

        // create the actual swap
        InterestRateSwap swap = InterestRateSwap.newBuilder()
                .setClearingBroker("MSCO")
                .setCounterparty("GSCO")
                .setExecBroker("JEF")
                .setSettleCurrency(Currency.USD)
                .setStartDate(Date.newBuilder().setYear(startDate.getYear()).setMonth(startDate.getMonthValue())
                        .setDay(startDate.getDayOfMonth()))
                .setTerminationDate(Date.newBuilder().setYear(terminationDate.getYear())
                        .setMonth(terminationDate.getMonthValue()).setDay(terminationDate.getDayOfMonth()))
                .setNotional(1_000_000)
                .setSecurity(security)
                .setLeg1FixedFloatFlag(FixedFloatLegFlag.FIXED_LEG)
                .setLeg1Rate(0.0015)
                .setLeg2FixedFloatFlag(FixedFloatLegFlag.FLOAT_LEG)
                .setLeg2Index(Index.USD_LIBOR_3M)
                .build();

        // little cache for converting the reference time into a Google timestamp
        Timestamp timestamp = Timestamp.newBuilder().setSeconds(refTime.getEpochSecond()).build();

        // create the swap position
        Position position = Position.newBuilder()
                .setAsOfTime(timestamp)
                .setPositionId(1)
                .setQty(Quantity.newBuilder().setQuantity(100))
                .setSecurityKey(securityKey).build();

        // we want to request the following measures
        Measure[] measures = new Measure[]{Measure.PV, Measure.DV01};

        // trivially add some fake market data for the swap (note that it won't be used)
        List<MarketData> marketData = new ArrayList<>();
        marketData.add(MarketData.newBuilder().setAsOfTime(timestamp).setUsed(1.0).setSecurityKey(securityKey).build());

        // create the HedgeOS curves from the raw test data above
        List<CurvePoint> libor3MPoints = Stream.of(usdLibor3MCurveData)
                .map(d -> CurvePoint.newBuilder().setDate(Timestamp.newBuilder()
                        .setSeconds(d.date.toEpochSecond(LocalTime.MIDNIGHT, ZoneOffset.UTC)))
                        .setDiscountFactor(d.value).build())
                .collect(Collectors.toList());

        List<CurvePoint> oisPoints = Stream.of(usdFedFundsCurveData)
                .map(d -> CurvePoint.newBuilder().setDate(Timestamp.newBuilder()
                        .setSeconds(d.date.toEpochSecond(LocalTime.MIDNIGHT, ZoneOffset.UTC)))
                        .setDiscountFactor(d.value).build())
                .collect(Collectors.toList());

        Curve usdLibor3MCurve = Curve.newBuilder()
                .setCurveName("USD-LIBOR-3M")
                .setDayCount(DayCount.ACT_365F)
                .setIndex(Index.USD_LIBOR_3M)
                .setCurveType(CurveType.PROJECTION)
                .setAsOfTime(timestamp)
                .addAllPoints(libor3MPoints)
                .setInterpolationDef(InterpolationDef.newBuilder().addInterpolationName("Linear").addStartPoint(0))
                .build();

        Curve usdFedFundsCurve = Curve.newBuilder()
                .setCurveName("USD-FED-FUNDS")
                .setDayCount(DayCount.ACT_365F)
                .setCurrency(Currency.USD)
                .setCurveType(CurveType.DISCOUNT)
                .setAsOfTime(timestamp)
                .addAllPoints(oisPoints)
                .setInterpolationDef(InterpolationDef.newBuilder().addInterpolationName("Linear").addStartPoint(0))
                .build();

        // also add the OIS curve as a projected version
        Curve usdFedFundsCurveProj = Curve.newBuilder()
                .setCurveName("USD-FED-FUNDS")
                .setDayCount(DayCount.ACT_365F)
                .setIndex(Index.USD_FED_FUNDS)
                .setCurveType(CurveType.PROJECTION)
                .setAsOfTime(timestamp)
                .addAllPoints(oisPoints)
                .setInterpolationDef(InterpolationDef.newBuilder().addInterpolationName("Linear").addStartPoint(0))
                .build();

        // assemble the mock price request
        PositionPriceRequest positionPriceRequest = PositionPriceRequest.newBuilder()
                .setAsOfTime(timestamp)
                .addPosition(position)
                .putSecurityBags(securityId, SecurityUnion.newBuilder().setIrsSecurity(swap).build())
                .addAllMeasure(Stream.of(measures).collect(Collectors.toList()))
                .addAllMarketData(marketData)
                .addCurve(usdFedFundsCurve)
                .addCurve(usdLibor3MCurve)
                .addCurve(usdFedFundsCurveProj)
                .setDebug(true)
                .build();

        // ...and let's test
        PositionPriceStreamObserverInProcess inProcessStreamObserver = new PositionPriceStreamObserverInProcess();

        long totalAfterJIT = 0;
        long totalTries = 20;
        boolean debug = positionPriceRequest.getDebug();

        for (int i = 0; i < totalTries; i++) {
            long start = System.currentTimeMillis();

            PricingServer pricingServer = new PricingServer();
            pricingServer.pricePosition(positionPriceRequest, inProcessStreamObserver);

            long end = System.currentTimeMillis();
            long took = end - start;

            if (i > 0) {
                totalAfterJIT += took;
            }

            // switch off curve building debug mode after first iteration
            if (positionPriceRequest.getDebug()) positionPriceRequest = positionPriceRequest.toBuilder()
                    .setDebug(false).build();

            if (debug) {
                if (i == 0)
                    System.out.println("JIT and pricing took(ms)=" + took);
                else
                    System.out.println(STAR_LINE + "\nPricing took(ms)=" + took + "\n" + STAR_LINE);
            }
            if (i > 0) {
                assertTrue("Pricing too slow, took(ms)=" + took, took < 1000);
                assertTrue("Pricing too fast", took > 1); // maybe nothing happened
            } else {
                // the JIT compiler runs the first (only once per JVM), curve build.
                assertTrue("Pricing too slow w JIT, took(ms)=" + took, took < 4000);
            }
        }

        // make sure the average of the non JIT runs, is good
        double avg = totalAfterJIT / (1.0d * totalTries - 1);

        System.out.println(STAR_LINE + "\nAverage pricing time avg(ms)=" + avg + "\n" + STAR_LINE);

        List<PositionPriceResult> resultsList = inProcessStreamObserver.positionPriceResponse.getResultsList();

        for (PositionPriceResult positionPriceResult : resultsList) {
            System.out.println();
            System.out.println(positionPriceResult.getPosition());

            for (int i = 0; i < positionPriceResult.getMeasuresCount(); i++) {
                System.out.println("Measure '" + positionPriceResult.getMeasures(i).name() + "': " + positionPriceResult.getValues(i));

            }
        }
    }
}