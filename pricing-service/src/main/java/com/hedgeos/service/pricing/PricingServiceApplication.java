package com.hedgeos.service.pricing;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableAutoConfiguration
public class PricingServiceApplication implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		int port = 5002;
		String hostname = null;
		if (args.length >= 1) {
			try {
				port = Integer.parseInt(args[0]);
			} catch (NumberFormatException ex) {
				System.err.println("Usage: [port [hostname]]");
				System.err.println("");
				System.err.println("  port      The listen port. Defaults to " + port);
				System.err.println("  hostname  The name clients will see in greet responses. ");
				System.err.println("            Defaults to the machine's hostname");
				System.exit(1);
			}
		}
		if (args.length >= 2) {
			hostname = args[1];
		}

		// from example
		final Server server = ServerBuilder.forPort(port)
				.addService(new PricingServer())
				// .addService(ProtoReflectionService.newInstance())
				// .addService(health.getHealthService())
				.build()
				.start();
		System.out.println("Listening on port " + port);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				// Start graceful shutdown
				server.shutdown();
				try {
					// Wait for RPCs to complete processing
					if (!server.awaitTermination(30, TimeUnit.SECONDS)) {
						// That was plenty of time. Let's cancel the remaining RPCs
						server.shutdownNow();
						// shutdownNow isn't instantaneous, so give a bit of time to clean resources up
						// gracefully. Normally this will be well under a second.
						server.awaitTermination(5, TimeUnit.SECONDS);
					}
				} catch (InterruptedException ex) {
					server.shutdownNow();
				}
			}
		});
		// This would normally be tied to the service's dependencies. For example, if HostnameGreeter
		// used a Channel to contact a required service, then when 'channel.getState() ==
		// TRANSIENT_FAILURE' we'd want to set NOT_SERVING. But HostnameGreeter has no dependencies, so
		// hard-coding SERVING is appropriate.
		// health.setStatus("", ServingStatus.SERVING);

		server.awaitTermination();
	}


	public static void main(String[] args) throws Exception {
		// Close the context so it doesn't stay awake listening for redis
		SpringApplication.run(PricingServiceApplication.class, args).close();
	}
}
