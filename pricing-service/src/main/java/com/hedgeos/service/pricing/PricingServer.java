package com.hedgeos.service.pricing;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.io.CharSource;
import com.hedgeos.curve.*;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.opengamma.OpenGamma;
import com.hedgeos.position.Position;

import com.hedgeos.security.SecurityUnion;
import com.hedgeos.service.curve.PositionPriceRequest;
import com.hedgeos.service.curve.PositionPriceResponse;
import com.hedgeos.service.curve.PositionPriceResult;
import com.hedgeos.service.curve.PricingServiceGrpc;
import com.opengamma.strata.basics.ReferenceData;
import com.opengamma.strata.basics.currency.CurrencyAmount;
import com.opengamma.strata.basics.index.Index;
import com.opengamma.strata.calc.CalculationRules;
import com.opengamma.strata.calc.CalculationRunner;
import com.opengamma.strata.calc.Column;
import com.opengamma.strata.calc.Results;
import com.opengamma.strata.calc.runner.CalculationFunctions;
import com.opengamma.strata.collect.result.Result;
import com.opengamma.strata.collect.timeseries.LocalDateDoubleTimeSeries;
import com.opengamma.strata.data.ImmutableMarketData;
import com.opengamma.strata.data.ImmutableMarketDataBuilder;
import com.opengamma.strata.loader.csv.QuotesCsvLoader;
import com.opengamma.strata.loader.csv.RatesCurvesCsvLoader;
import com.opengamma.strata.market.curve.CurveId;
import com.opengamma.strata.market.curve.RatesCurveGroup;
import com.opengamma.strata.market.observable.IndexQuoteId;
import com.opengamma.strata.market.param.CurrencyParameterSensitivities;
import com.opengamma.strata.measure.StandardComponents;
import com.opengamma.strata.measure.rate.RatesMarketDataLookup;
import com.opengamma.strata.product.Trade;
import io.grpc.stub.StreamObserver;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.ZoneOffset.UTC;

public class PricingServer extends PricingServiceGrpc.PricingServiceImplBase {
    @Override
    public void pricePosition(PositionPriceRequest request, StreamObserver<PositionPriceResponse> responseObserver) {
        // set valuation timestamp
        LocalDate valuationDate = Instant.ofEpochSecond(request.getAsOfTime().getSeconds(),
                request.getAsOfTime().getNanos()).atZone(UTC).toLocalDate();

        // obtain security bags map to link security identifiers with security types
        Map<Long, SecurityUnion> securityUnions = request.getSecurityBagsMap();

        // prepare list of OpenGamma trades to use for pricing
        List<Trade> trades = new ArrayList<>();

        // create a trade for each position
        for (Position position : request.getPositionList()) {
            SecurityKey securityKey = position.getSecurityKey();
            SecurityUnion SecurityUnion = securityUnions.get(securityKey.getSecurityId());

            if (SecurityUnion == null)
                throw new IllegalStateException("no security bag found for key '" + securityKey + "'");

            // extract position size for later
            long quantity = position.getQty().getQuantity();

            // handle position type
            if (SecurityUnion.hasEquitySecurity())    // equity
                trades.add(OpenGamma.getGenericSecurityTrade(quantity, SecurityUnion.getEquitySecurity(), valuationDate));
            else if (SecurityUnion.hasIrsSecurity())  // interest rate swap
                trades.add(OpenGamma.getSwapTrade(quantity, SecurityUnion.getIrsSecurity(), valuationDate));
            else
                throw new IllegalStateException("security bag did not contain any supported position type");
        }

        // create desired OpenGamma measures
        List<Column> measures = request.getMeasureList().stream().map(OpenGamma::getMeasure).map(Column::of)
                .collect(Collectors.toList());

        // prepare data needed by OpenGamma
        // extract market data
        String marketDataCsv = OpenGamma.buildMarketDataCsv(request.getMarket().getMarketDataList());

        // re-create curve group(s)
        Curve discountCurve = null;
        List<Curve> forwardCurves = new ArrayList<>();

        for (Curve curve : request.getCurveList()) {
            if (curve.getCurveType().equals(CurveType.DISCOUNT) && discountCurve == null)
                discountCurve = curve;
            else if (curve.getCurveType().equals(CurveType.PROJECTION))
                forwardCurves.add(curve);
            else
                throw new IllegalStateException("invalid set of curves received");
        }

        if (discountCurve == null) throw new IllegalStateException("missing discount curve");
        if (forwardCurves.size() == 0) throw new IllegalStateException("missing forward curve");

        CurveGroup curveGroup = CurveGroup.newBuilder()
                .setGroupName("HedgeOS-Pricing")
                .setAsOfTime(request.getAsOfTime())
                .setCurrencyToDiscountCurve(CurrencyToDiscountCurve.newBuilder()
                        .setCurrency(discountCurve.getCurrency())
                        .setCurveName(discountCurve.getCurveName()))
                .addAllIndexToForwardCurve(forwardCurves.stream()
                        .map(c -> IndexToForwardCurve.newBuilder()
                                .setIndex(c.getIndex())
                                .setCurveName(c.getCurveName()).build())
                        .collect(Collectors.toList()))
                .build();

        // build groups csv
        String groupsCsv = OpenGamma.buildCurveGroupCsv(curveGroup);

        // reverse-engineer curve definitions to build settings csv
        String settingsCsv = OpenGamma.buildCurveSettingsCsv(
                forwardCurves.stream().map(c -> CurveDef.newBuilder()
                        .setName(c.getCurveName())
                        .setDayCount(c.getDayCount())
                        .setInterpolationDef(c.getInterpolationDef())).collect(Collectors.toList())
        );

        // build curve values csv
        String valuesCsv = OpenGamma.buildCurveValuesCsv(request.getCurveList());

        // print the CSVs if Debug = true on the request
        if (request.getDebug()) {
            System.out.println(groupsCsv);
            System.out.println(settingsCsv);
            System.out.println(valuesCsv);
            System.out.println(marketDataCsv);
        }

        // the complete set of rules for calculating measures
        CalculationFunctions functions = StandardComponents.calculationFunctions();

        // create OG rates curve group
        RatesCurveGroup ratesCurveGroup = Maps.transformValues(
                RatesCurvesCsvLoader.parse(valuationDate::equals, CharSource.wrap(groupsCsv),
                        CharSource.wrap(settingsCsv), ImmutableList.of(CharSource.wrap(valuesCsv))).asMap(),
                x1 -> x1.iterator().next()).get(valuationDate);

        // collect quotes into market data
        ImmutableMarketDataBuilder marketDataBuilder = ImmutableMarketData.builder(valuationDate)
                .values(QuotesCsvLoader.parse(
                        valuationDate::equals, ImmutableList.of(CharSource.wrap(marketDataCsv))
                ).getOrDefault(valuationDate, ImmutableMap.of()));

        // also add the pre-constructed HedgeOS curves into market data
        ratesCurveGroup.getDiscountCurves().forEach((x, curve) -> marketDataBuilder.addValue(
                CurveId.of(ratesCurveGroup.getName(), curve.getName()), curve));
        ratesCurveGroup.getForwardCurves().forEach((x, curve) -> marketDataBuilder.addValue(
                CurveId.of(ratesCurveGroup.getName(), curve.getName()), curve));

        // what follows is a temporary cheat as we do not yet have decided on how to manage fixings
        // TODO fix this temporary cheat until we include fixings in market data
        LocalDateDoubleTimeSeries timeSeries = LocalDateDoubleTimeSeries.builder()
                .put(LocalDate.of(2020, 9, 22), 0.001)
                .put(LocalDate.of(2020, 9, 23), 0.001)
                .put(LocalDate.of(2020, 9, 24), 0.001)
                .put(LocalDate.of(2020, 9, 25), 0.001)
                .put(LocalDate.of(2020, 9, 26), 0.001)
                .put(LocalDate.of(2020, 9, 27), 0.001)
                .put(LocalDate.of(2020, 9, 28), 0.001)
                .put(LocalDate.of(2020, 9, 29), 0.001)
                .put(LocalDate.of(2020, 9, 30), 0.001)
                .build();
        marketDataBuilder.addTimeSeries(IndexQuoteId.of(Index.of("USD-LIBOR-3M")), timeSeries);
        // end cheat

        // get curves here
        CalculationRules rules = CalculationRules.of(functions, RatesMarketDataLookup.of(ratesCurveGroup));

        // use standard reference data for now
        ReferenceData refData = ReferenceData.standard();

        // calculate results
        Results results = CalculationRunner.ofMultiThreaded().calculate(rules, trades, measures,
                marketDataBuilder.build(), refData);

        // assign OpenGamma results to pricing response
        PositionPriceResponse.Builder responseBuilder = PositionPriceResponse.newBuilder();

        for (int i = 0; i < trades.size(); i++) {
            PositionPriceResult.Builder positionPriceResult = PositionPriceResult.newBuilder()
                    .setPosition(request.getPosition(i));

            for (int j = 0; j < request.getMeasureCount(); j++) {
                Result<?> result = results.get(i, j);
                if (result.isFailure()) throw new RuntimeException("OpenGamma: " + result.getFailure().getMessage());

                positionPriceResult.addMeasures(request.getMeasure(j));

                Object value = result.getValue();
                if (value instanceof CurrencyAmount)
                    positionPriceResult.addValues(((CurrencyAmount) value).getAmount());
                else if (value instanceof CurrencyParameterSensitivities)
                    positionPriceResult.addValues(((CurrencyParameterSensitivities) value).getSensitivities()
                            .get(0).getSensitivity().get(0));
                else throw new RuntimeException("OpenGamma measure class not yet implemented: " + value.getClass());
            }

            responseBuilder.addResults(positionPriceResult);
        }

        responseObserver.onNext(responseBuilder.build());
        responseObserver.onCompleted();
    }
}
