package com.hedgeos.router;

import com.google.protobuf.Timestamp;
import com.hedgeos.api.ServiceLocatorInterface;
import com.hedgeos.curve.RouterPricePortfolioRequest;
import com.hedgeos.curve.RouterPricePortfolioResponse;
import com.hedgeos.general.Quantity;
import com.hedgeos.mock.curve.MockServiceLocator;
import com.hedgeos.position.Position;
import com.hedgeos.service.curve.PositionPriceRequest;
import com.hedgeos.service.router.RouterServer;
import com.hedgeos.service.router.curve.YieldCurveMapper;
import io.grpc.stub.StreamObserver;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.hedgeos.mock.curve.FakeSwapMockFactory.FAKE_SWAP_KEY;

public class RouterServerTest {

    @Test
    public void testRouterServerSimple() {
        // full mock construction of the RouterServer with Mock Service Locator
        ServiceLocatorInterface mockServiceLocator = new MockServiceLocator();
        YieldCurveMapper yieldCurveMapper = new YieldCurveMapper(mockServiceLocator);
        RouterServer routerServer = new RouterServer(mockServiceLocator, yieldCurveMapper);

        // call the post-init like Spring will, looks up the services
        routerServer.init();

        Timestamp now = Timestamp.newBuilder().build();
        Quantity hundredQty = Quantity.newBuilder().setQuantity(100L).build();
        Position position = Position.newBuilder().setAsOfTime(now).setQty(hundredQty).setSecurityKey(FAKE_SWAP_KEY).build();
        PositionPriceRequest onePosition = PositionPriceRequest.newBuilder().addPosition(position).build();
        RouterPricePortfolioRequest ppr = RouterPricePortfolioRequest.newBuilder().setRequest(onePosition).build();

        RouterServerMockObserver observer = new RouterServerMockObserver();
        routerServer.pricePortfolio(ppr, observer);

        // TODO:  Get the measures, map in order, get price
        double price =
                observer.responseList.get(0)
                        .getResponses()
                        .getResultsList()
                        .get(0).getValues(0);

        Assert.assertEquals("Price is WRONG", 999.99, price);
    }
}
