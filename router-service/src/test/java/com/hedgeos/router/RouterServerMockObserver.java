package com.hedgeos.router;

import com.hedgeos.curve.RouterPricePortfolioResponse;
import io.grpc.stub.StreamObserver;

import java.util.ArrayList;
import java.util.List;

public class RouterServerMockObserver implements StreamObserver<RouterPricePortfolioResponse> {

    public List<RouterPricePortfolioResponse> responseList = new ArrayList<>();

    @Override
    public void onNext(RouterPricePortfolioResponse routerPricePortfolioResponse) {
        this.responseList.add(routerPricePortfolioResponse);
    }

    @Override
    public void onError(Throwable throwable) { }

    @Override
    public void onCompleted() {}
}


