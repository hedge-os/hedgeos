package com.hedgeos.service.router.curve;

import com.hedgeos.general.Currency;
import com.hedgeos.curve.CurveDef;
import com.hedgeos.curve.CurveGroup;
import com.hedgeos.curve.Index;
import com.hedgeos.general.MarketData;
import com.hedgeos.general.SecurityType;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.mock.curve.CurveGroupMock;
import com.hedgeos.refdata.service.RefDataServiceGrpc;
import com.hedgeos.security.Security;
import com.hedgeos.api.ServiceLocatorInterface;
import com.hedgeos.service.security.SecurityServiceGrpc;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Utility class for performing Yield Curve massaging and fetching.
 */
@Component
@RequiredArgsConstructor
public class YieldCurveMapper {

    private final ServiceLocatorInterface serviceLocatorInterface;

    private SecurityServiceGrpc.SecurityServiceBlockingStub securityServiceBlockingStub;
    private RefDataServiceGrpc.RefDataServiceBlockingStub refDataServiceBlockingStub;

    @PostConstruct
    private void init() {
        securityServiceBlockingStub = serviceLocatorInterface.securityServiceBlockingStub();
        refDataServiceBlockingStub = serviceLocatorInterface.refDataBlockingStub();
    }

    public Map<String, CurveGroup> getCurveGroupsForCurveNames(Collection<String> curveNames) {
        Map<String, CurveGroup> curveGroupMap = new HashMap<>();
        CurveGroup usdCurveGroup = CurveGroupMock.makeUsdCurveGroup();
        curveGroupMap.put("USD-OIS", usdCurveGroup);
        return curveGroupMap;
    }


    public List<String> mapSecuritiesToCurves(Security security,
                                              Map<Index, String> indexToCurveGroupMap,
                                              Map<Currency, String> securityToCurveGroupOverride)
    {
        List<String> curves = new ArrayList<>();
        // TODO:  e.g. If SecurityType is IRS, use the Floating Rate Index
        if (security.getSecurityType() == SecurityType.IRS) {
            Index floatingRate = getFloatingRateIndexForIrs(security);
            String curve = indexToCurveGroupMap.get(floatingRate);
            if (curve == null) {
                throw new RuntimeException("Missing curve configuration (must add to Ref Data," +
                        " Index-To-Curve, for floatingRate=" + floatingRate +
                        ", securityId=" + security.getSecurityKey().getSecurityId());
            }

            curves.add(curve);
        }

        if (security.getCurrency() != null) {
            String curveName =
                    securityToCurveGroupOverride.get(security.getCurrency());
            if (curveName == null)
                throw new RuntimeException("No curve name for Currency="+security.getCurrency());

            curves.add(curveName);
        }

        return curves;
    }

    // TODO:  Needs to be a fast map of the Security info
    // TODO:  Local cache of Redis / Hazelcast.
    private Index getFloatingRateIndexForIrs(Security security) {
        return Index.USD_LIBOR_3M;
    }

    public List<MarketData> marketDatForCurves(List<CurveDef> curveDefs, Map<SecurityKey, MarketData> marketDataMap) {
        List<MarketData> curveMarketData = new ArrayList<>();
        for (CurveDef curveDef : curveDefs) {
            for (Security security : curveDef.getSecuritiesList()) {
                MarketData md = marketDataMap.get(security.getSecurityKey());
                if (md == null) {
                    throw new RuntimeException("Can't have null market data for curve="+security);
                }
                curveMarketData.add(md);
            }
        }

        return curveMarketData;
    }

    public CurveGroup curveGroupForCurves(List<String> curveNames) {
        return CurveGroupMock.makeUsdCurveGroup();
    }
}
