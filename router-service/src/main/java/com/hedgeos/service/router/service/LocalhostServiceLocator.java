package com.hedgeos.service.router.service;

import com.hedgeos.api.Ports;
import com.hedgeos.api.ServiceLocatorInterface;
import com.hedgeos.refdata.service.RefDataServiceGrpc;
import com.hedgeos.service.curve.CurveServiceGrpc;
import com.hedgeos.service.marketdata.MarketDataServiceGrpc;
import com.hedgeos.service.security.SecurityServiceGrpc;
import io.grpc.Deadline;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Abstraction over finding services.
 * Allows looking up the Grpc services to be abstracted
 * This one always goes to localhost, will use Consul going forward soon.
 *
 * (although, mainly only used in the Router)
 *
 * TODO: move to a module about service location
 * TODO: Research gRPC consul service location patterns (and with spring)
 * TODO:  Wrapper which reconnects and load balances through consul (research and code)
 */
@Component
public class LocalhostServiceLocator implements ServiceLocatorInterface {

    @Override
    public SecurityServiceGrpc.SecurityServiceBlockingStub securityServiceBlockingStub() {

        ManagedChannel managedChannel = ManagedChannelBuilder
                .forAddress("localhost", Ports.SEC_SERVICE_GRPC).usePlaintext().build();

        // TODO:  Lower the deadlines to 100ms once caching is in place
        // Add Deadlines to everything
        // https://grpc.io/blog/deadlines/
        // 1 second, because it is your PC
        return SecurityServiceGrpc.newBlockingStub(managedChannel)
                        .withDeadline(Deadline.after(1, TimeUnit.SECONDS));
    }

    @Override
    public RefDataServiceGrpc.RefDataServiceBlockingStub refDataBlockingStub() {
        ManagedChannel managedChannel = ManagedChannelBuilder
                .forAddress("localhost", Ports.REF_SERVICE_GRPC).usePlaintext().build();

        return RefDataServiceGrpc.newBlockingStub(managedChannel)
                        .withDeadline(Deadline.after(1, TimeUnit.SECONDS));
    }

    @Override
    public CurveServiceGrpc.CurveServiceBlockingStub curveServiceBlockingStub() {
        ManagedChannel managedChannel = ManagedChannelBuilder
                .forAddress("localhost", Ports.CURVE_SERVICE_GRPC).usePlaintext().build();

        return CurveServiceGrpc.newBlockingStub(managedChannel)
                .withDeadline(Deadline.after(1, TimeUnit.SECONDS));
    }

    @Override
    public MarketDataServiceGrpc.MarketDataServiceBlockingStub marketDataBlockingStub() {
        ManagedChannel managedChannel = ManagedChannelBuilder
                .forAddress("localhost", Ports.CURVE_SERVICE_GRPC).usePlaintext().build();

        return MarketDataServiceGrpc.newBlockingStub(managedChannel)
                        .withDeadline(Deadline.after(1, TimeUnit.SECONDS));
    }
}
