package com.hedgeos.service.router.rest;

import com.hedgeos.curve.RouterPricePortfolioRequest;
import com.hedgeos.curve.RouterPricePortfolioResponse;
import com.hedgeos.service.router.RouterServer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController("/price")
@RequiredArgsConstructor
public class PriceController {

    public final RouterServer routerServer;

    @GetMapping("/test")
    public String test() { return new Date().toString(); }
    
    @GetMapping("/pricePortfolio")
    public String pricePortfolio(RouterPricePortfolioRequest request) {
        // routerServer.pricePortfolio(request);
        // throw new RuntimeException("todo");
        // return RouterPricePortfolioResponse.newBuilder().build().toString();
        return "hi";
    }
}
