package com.hedgeos.service.router;

import com.hedgeos.curve.*;
import com.hedgeos.depgraph.CurveGraphUtil;
import com.hedgeos.depgraph.DepGraph;
import com.hedgeos.depgraph.Sweeper;
import com.hedgeos.general.*;
import com.hedgeos.general.Currency;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.position.RiskReportRow;
import com.hedgeos.refdata.CurrencyToCurveEntry;
import com.hedgeos.refdata.IndexToCurveEntry;
import com.hedgeos.refdata.service.*;
import com.hedgeos.security.Security;
import com.hedgeos.service.curve.CurveServiceGrpc;
import com.hedgeos.service.marketdata.MarketDataServiceGrpc;
import com.hedgeos.service.router.curve.YieldCurveMapper;
import com.hedgeos.api.ServiceLocatorInterface;
import com.hedgeos.service.security.SecurityRequest;
import com.hedgeos.service.security.SecurityResponse;
import com.hedgeos.service.security.SecurityServiceGrpc;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class RouterServer extends ReportRouterServiceGrpc.ReportRouterServiceImplBase {

    // injected by Spring, can be in-memory for testing
    // localhost locator for PC, or Consul for Prod
    private final ServiceLocatorInterface serviceLocator;
    private final YieldCurveMapper yieldCurveMapper;

    // located after startup (allows the system services, to start services independently)
    private SecurityServiceGrpc.SecurityServiceBlockingStub securityServiceBlockingStub;
    private CurveServiceGrpc.CurveServiceBlockingStub curveServiceBlockingStub;
    private RefDataServiceGrpc.RefDataServiceBlockingStub refDataServiceBlockingStub;
    private MarketDataServiceGrpc.MarketDataServiceBlockingStub marketDataServiceBlockingStub;

    @PostConstruct
    public void init() {
        securityServiceBlockingStub = serviceLocator.securityServiceBlockingStub();
        refDataServiceBlockingStub = serviceLocator.refDataBlockingStub();
        curveServiceBlockingStub = serviceLocator.curveServiceBlockingStub();
        marketDataServiceBlockingStub = serviceLocator.marketDataBlockingStub();
    }

    public void pnl(PnlReportRequest request,
                    StreamObserver<RiskReportRow> responseObserver)
    {
        // TODO:
        // 1.  get positions for request
        // 1.  get positions + marks for yesterday?
        // 2.  get trades for today
        // 3.  price everything
        // 4.  Calculate PnL and Trading PnL etc
    }

    /**
     Price portfolio: a critical entry point to the server from the external systems.
         1.  Build the dep graph
         2.  Add the curves we need for the instruments, and then the market data and ref-data
         3.  Sweep for curves to build, and build them with Curve Service
         4.  Pass the curves and market data to the pricer, and price everything
    **/
    @Override
    public void pricePortfolio(RouterPricePortfolioRequest request,
                               StreamObserver<RouterPricePortfolioResponse> responseObserver)
    {
        // root a dep graph with the positions
        DepGraph depGraph = new DepGraph(request.getRequest().getPositionList());
        // sweep the keys, for those positions
        List<SecurityKey> securityKeys = Sweeper.unresolvedSecurityKeys(depGraph.rootBuilder);

        // grab every security, for the positions
        SecurityRequest securityRequest =
                SecurityRequest.newBuilder()
                        .addAllSecurityKey(securityKeys).build();

        SecurityResponse securityResponse =
                securityServiceBlockingStub.getSecurity(securityRequest);

        // walk the graph, setting the securities
        Sweeper.addSecuritiesForKeys(depGraph.rootBuilder, securityResponse.getSecuritiesList());

        // map each security to one or more curves
        // this is a distinct step, because it allows us to graph the dependency of security --> curve
        Map<SecurityKey, List<String>> securityToCurves = new HashMap<>();

        // get the map of Index --> Curve from RefData in bulk
        IndexToCurveResponse indexToCurves =
                refDataServiceBlockingStub.getIndexToCurveMap(IndexToCurveRequest.newBuilder().build());

        // map up the response for repeated usage on the graph
        Map<Index, String> indexToCurveGroupMap =
                indexToCurves.getIndexToCurveEntriesList().stream().collect(
                        Collectors.toMap(IndexToCurveEntry::getIndex, IndexToCurveEntry::getCurveName));

        // get the table config of Currency --> Curve from RefData in bulk
        CurrencyToCurveResponse currencyToCurveResponse =
                refDataServiceBlockingStub.getCurrencyToCurveMap(CurrencyToCurveRequest.newBuilder().build());

        // map up the results for repeated usage on the graph
        Map<Currency, String> currencyToCurveMap =
                currencyToCurveResponse.getCurrencyToCurveEntriesList().stream().collect(
                        Collectors.toMap(CurrencyToCurveEntry::getCurrency, CurrencyToCurveEntry::getCurveName));

        // get the map of Security --> Curve (overrides the index map), in bulk
        CurveGroupOverrideResponse curveGroupOverride =
                refDataServiceBlockingStub.getSecurityToCurveGroupOverrideMap(CurveGroupOverrideRequest.newBuilder().build());
        // TODO:  Should incorporate the overrides if there are any

        Set<String> curveNames = new HashSet<>();

        // make a map of security --> List <curve>
        // also pick up, distinct curve names across the run
        for (Security security : securityResponse.getSecuritiesList()) {
            List<String> curveNamesForSecurity =
                    yieldCurveMapper.mapSecuritiesToCurves(security, indexToCurveGroupMap, currencyToCurveMap);

            securityToCurves.put(security.getSecurityKey(), curveNamesForSecurity);
            // conflate the list of strings to a set of the curve groups needed
            curveNames.addAll(curveNamesForSecurity);
        }

        // update the dep Graph, with dependencies: security --> Curve
        Map<String, Node> curveNameToNode =
                Sweeper.addSecurityToCurveMapping(depGraph, securityToCurves);

        // get the curve groups, for the curves we need (mapped by Curve --> Curve Group)
        Map<String, CurveGroup> curveNameToGroup = yieldCurveMapper.getCurveGroupsForCurveNames(curveNames);

        // add the curve groups to the graph,
        // (given the Curve to Group and Curve to security maps)
        CurveGraphUtil.addCurveGroupsToGraph(curveNameToNode, curveNameToGroup);

        // get the curve defs up front, to allow for one call where we fetch market data
        Map<CurveGroup, List<CurveDef>> groupToDefMap = new HashMap<>();

        for (CurveGroup curveGroup : curveNameToGroup.values()) {
            // get the curve Defs for the Curve Group and make the yield curve request
            CurveDefRequest.Builder cdr = CurveDefRequest.newBuilder();
            // add the currency and index curves, from the curve group
            cdr.addCurveNames(curveGroup.getCurrencyToDiscountCurve().getCurveName());
            cdr.addAllCurveNames(curveGroup.getIndexToForwardCurveList().stream().map(IndexToForwardCurve::getCurveName).collect(Collectors.toList()));

            CurveDefResponse resp = refDataServiceBlockingStub.fetchCurveDefs(cdr.build());

            // TODO:  Huh?
            // TODO:  Combination of first, Ref Data Calls, and then, Security Service
            groupToDefMap.put(curveGroup, resp.getCurveDefList());
        }

        // fetch the ENTIRE MARKET data set,
        // by sweeping the dep graph
        List<SecurityKey> marketDataSecList = Sweeper.getMarketDataSecurities(depGraph);

        // now go get the market data for those securities which need it (leaves)
        Map<SecurityKey, MarketData> marketDataMap = fetchMarketData(marketDataSecList);

        // Build the curves
        //  - run one group at a time,
        //  - makes it easy to distribute to multiple servers

        List<CurveResponse> curveResponses = new ArrayList<>();

        for (CurveGroup group : curveNameToGroup.values()) {

            // get the curve definitions for the group
            List<CurveDef> curveDefs = groupToDefMap.get(group);
            // grab the market data for only this curve
            List<MarketData> marketData = yieldCurveMapper.marketDatForCurves(curveDefs, marketDataMap);

            // make a curve request with the curve definitions, market data, curve group
            CurveRequest curveRequest  =
                    CurveRequest.newBuilder()
                            .addAllCurveDef(curveDefs)
                            .addAllMarketData(marketData)
                            .setCurveGroup(group).build();

            // send the curve request to build curves
            CurveResponse response = curveServiceBlockingStub.buildCurve(curveRequest);

            // add the curves to the graph, via the nodes
            for (Curve curve : response.getCurveList()) {
                Node curveNode = curveNameToNode.get(curve.getCurveName());
                if (curveNode == null) {
                    throw new RuntimeException("Must have a curve="+curve.getCurveName());
                }

                Node newCurveNode = Node.newBuilder().mergeFrom(curveNode).setCurve(curve).build();

                Sweeper.replaceNode(depGraph, curveNode, newCurveNode);
            }

            curveResponses.add(response);
        }


        // Finally,
        // TODO: send a price request, price it
        // send the response to the caller

    }


    private Map<SecurityKey, MarketData> fetchMarketData(List<SecurityKey> fulLSecurityList) {

        MarketDataRequest mdr =
                MarketDataRequest.newBuilder().addAllSecurityKeys(fulLSecurityList).build();

        MarketDataResponse response = marketDataServiceBlockingStub.getMarketData(mdr);

        List<MarketData> md = response.getMarketDatasList();
        Map<SecurityKey, MarketData> mds = new HashMap<>();
        for (MarketData marketData : md) {
            mds.put(marketData.getSecurityKey(), marketData);
        }
        return mds;
    }

    @Override
    public void buildCurve(RouterCurveRequest request, StreamObserver<RouterCurveResponse> responseObserver) {
        super.buildCurve(request, responseObserver);
    }

    @Override
    public void buildSurface(RouterSurfaceRequest request, StreamObserver<RouterSurfaceResponse> responseObserver) {
        super.buildSurface(request, responseObserver);
    }

}
