package com.hedgeos.service.router;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class RouterApplication implements CommandLineRunner {

	@Autowired
	RouterGrpc routerGrpc;

	@Autowired
	ApplicationContext applicationContext;

	@Override
	public void run(String... args) throws Exception {

		routerGrpc.startGrpc();

	}

	public static void main(String[] args) {
		SpringApplication.run(RouterApplication.class, args);
	}

}
