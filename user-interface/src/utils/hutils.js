
export default function urlSniffPathNoPort(port, path) {
    let parts = window.location.href.split('/')
    let url = parts[2];
    let noPort = url.split(":")
    let fullPath = window.location.protocol + '//' + noPort[0] +':'+port+ '/' + path;

    return fullPath;
}

export function urlSniffPath(path) {
    let parts = window.location.href.split('/')
    let url = parts[2];
    let fullPath = window.location.protocol + '//' + url + '/' + path;
    return fullPath;
}

