'use strict';

import React, { Component } from 'react';
import { render } from 'react-dom';
import { AgGridReact } from '@ag-grid-community/react';
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model';
import { RichSelectModule } from '@ag-grid-enterprise/rich-select';
import { SetFilterModule } from '@ag-grid-enterprise/set-filter';
import { MenuModule } from '@ag-grid-enterprise/menu';
import { ColumnsToolPanelModule } from '@ag-grid-enterprise/column-tool-panel';
import '@ag-grid-community/core/dist/styles/ag-grid.css';
import '@ag-grid-community/core/dist/styles/ag-theme-alpine-dark.css';

class GridExample extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modules: [
                ClientSideRowModelModule,
                RichSelectModule,
                SetFilterModule,
                MenuModule,
                ColumnsToolPanelModule,
            ],
            rowData: [
                {
                    make: 'tyt',
                    exteriorColour: 'fg',
                    interiorColour: 'bw',
                    price: 35000,
                },
                {
                    make: 'frd',
                    exteriorColour: 'bw',
                    interiorColour: 'cb',
                    price: 32000,
                },
                {
                    make: 'prs',
                    exteriorColour: 'cb',
                    interiorColour: 'fg',
                    price: 72000,
                },
                {
                    make: 'tyt',
                    exteriorColour: 'fg',
                    interiorColour: 'bw',
                    price: 35000,
                },
                {
                    make: 'frd',
                    exteriorColour: 'bw',
                    interiorColour: 'cb',
                    price: 32000,
                },
                {
                    make: 'prs',
                    exteriorColour: 'cb',
                    interiorColour: 'fg',
                    price: 72000,
                },
                {
                    make: 'tyt',
                    exteriorColour: 'fg',
                    interiorColour: 'bw',
                    price: 35000,
                },
                {
                    make: 'frd',
                    exteriorColour: 'bw',
                    interiorColour: 'cb',
                    price: 32000,
                },
                {
                    make: 'prs',
                    exteriorColour: 'cb',
                    interiorColour: 'fg',
                    price: 72000,
                },
                {
                    make: 'tyt',
                    exteriorColour: 'fg',
                    interiorColour: 'bw',
                    price: 35000,
                },
                {
                    make: 'frd',
                    exteriorColour: 'bw',
                    interiorColour: 'cb',
                    price: 32000,
                },
                {
                    make: 'prs',
                    exteriorColour: 'cb',
                    interiorColour: 'fg',
                    price: 72000,
                },
                {
                    make: 'tyt',
                    exteriorColour: 'fg',
                    interiorColour: 'bw',
                    price: 35000,
                },
                {
                    make: 'frd',
                    exteriorColour: 'bw',
                    interiorColour: 'cb',
                    price: 32000,
                },
                {
                    make: 'prs',
                    exteriorColour: 'cb',
                    interiorColour: 'fg',
                    price: 72000,
                },
                {
                    make: 'prs',
                    exteriorColour: 'cb',
                    interiorColour: 'fg',
                    price: 72000,
                },
                {
                    make: 'tyt',
                    exteriorColour: 'fg',
                    interiorColour: 'bw',
                    price: 35000,
                },
                {
                    make: 'frd',
                    exteriorColour: 'bw',
                    interiorColour: 'cb',
                    price: 32000,
                },
            ],
            columnDefs: [
                {
                    field: 'make',
                    cellEditor: 'agSelectCellEditor',
                    cellEditorParams: { values: carBrands },
                    filterParams: {
                        valueFormatter: function (params) {
                            return lookupValue(carMappings, params.value);
                        },
                    },
                    valueFormatter: function (params) {
                        return lookupValue(carMappings, params.value);
                    },
                },
                {
                    field: 'exteriorColour',
                    minWidth: 150,
                    cellEditor: 'agRichSelectCellEditor',
                    cellEditorParams: {
                        values: colours,
                        cellRenderer: colourCellRenderer,
                    },
                    filter: 'agSetColumnFilter',
                    filterParams: {
                        values: colours,
                        valueFormatter: function (params) {
                            return lookupValue(colourMappings, params.value);
                        },
                        cellRenderer: colourCellRenderer,
                    },
                    valueFormatter: function (params) {
                        return lookupValue(colourMappings, params.value);
                    },
                    valueParser: function (params) {
                        return lookupKey(colourMappings, params.newValue);
                    },
                    cellRenderer: colourCellRenderer,
                },
                {
                    field: 'interiorColour',
                    minWidth: 150,
                    cellEditor: 'agTextCellEditor',
                    cellEditorParams: { useFormatter: true },
                    filter: 'agSetColumnFilter',
                    filterParams: {
                        values: colours,
                        valueFormatter: function (params) {
                            return lookupValue(colourMappings, params.value);
                        },
                        cellRenderer: colourCellRenderer,
                    },
                    valueFormatter: function (params) {
                        return lookupValue(colourMappings, params.value);
                    },
                    valueParser: function (params) {
                        return lookupKey(colourMappings, params.newValue);
                    },
                    cellRenderer: colourCellRenderer,
                },
                {
                    headerName: 'Retail Price',
                    field: 'price',
                    minWidth: 140,
                    colId: 'retailPrice',
                    valueGetter: function (params) {
                        return params.data.price;
                    },
                    valueFormatter: currencyFormatter,
                    valueSetter: numberValueSetter,
                },
                {
                    headerName: 'Retail Price (incl Taxes)',
                    minWidth: 205,
                    editable: false,
                    valueGetter: function (params) {
                        return params.getValue('retailPrice') * 1.2;
                    },
                    valueFormatter: currencyFormatter,
                },
            ],
            defaultColDef: {
                flex: 1,
                filter: true,
                editable: true,
            },
        };
    }

    onGridReady = (params) => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    };

    onCellValueChanged = (params) => {
        console.log('onCellValueChanged: ', params);
    };

    render() {
        return (
            <div style={{ width: '100%', height: '100%' }}>
                <div
                    id="myGrid"
                    style={{
                        height: '100%',
                        width: '100%',
                    }}
                    className="ag-theme-alpine-dark"
                >
                    <AgGridReact
                        modules={this.state.modules}
                        rowData={this.state.rowData}
                        columnDefs={this.state.columnDefs}
                        defaultColDef={this.state.defaultColDef}
                        onGridReady={this.onGridReady}
                        onCellValueChanged={this.onCellValueChanged.bind(this)}
                    />
                </div>
            </div>
        );
    }
}

var carMappings = {
    tyt: 'Toyota',
    frd: 'Ford',
    prs: 'Porsche',
    nss: 'Nissan',
};
var colourMappings = {
    cb: 'Cadet Blue',
    bw: 'Burlywood',
    fg: 'Forest Green',
};
var carBrands = extractValues(carMappings);
var colours = extractValues(colourMappings);
function extractValues(mappings) {
    return Object.keys(mappings);
}
function lookupValue(mappings, key) {
    return mappings[key];
}
function lookupKey(mappings, name) {
    var keys = Object.keys(mappings);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        if (mappings[key] === name) {
            return key;
        }
    }
}
function colourCellRenderer(params) {
    if (params.value === '(Select All)') {
        return params.value;
    }
    return (
        '<span style="color: ' +
        removeSpaces(params.valueFormatted) +
        '">' +
        params.valueFormatted +
        '</span>'
    );
}
function currencyFormatter(params) {
    var value = Math.floor(params.value);
    if (isNaN(value)) {
        return '';
    }
    return '\xA3' + value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}
function numberValueSetter(params) {
    if (isNaN(parseFloat(params.newValue)) || !isFinite(params.newValue)) {
        return false;
    }
    params.data.price = params.newValue;
    return true;
}
function removeSpaces(str) {
    return str ? str.replace(/\s/g, '') : str;
}

render(<GridExample></GridExample>, document.querySelector('#root'));