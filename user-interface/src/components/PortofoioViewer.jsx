import React, {Component} from "react";
import {connect} from "react-redux";
import { render } from 'react-dom';
import { useState } from 'react';

import {ClientSideRowModelModule} from "@ag-grid-community/all-modules";

import { AgGridReact } from '@ag-grid-community/react';

import { ServerSideRowModelModule } from '@ag-grid-enterprise/server-side-row-model';
import { RowGroupingModule } from '@ag-grid-enterprise/row-grouping';
import { MenuModule } from '@ag-grid-enterprise/menu';
import { ColumnsToolPanelModule } from '@ag-grid-enterprise/column-tool-panel';
import urlSniffPathNoPort from "../utils/hutils";

function formatNumber(value) {
    // this puts commas into the number eg 1000 goes to 1,000,
    // i pulled this from stack overflow, i have no idea how it works
    return Math.floor(value)
        .toString()
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

function currencyFormatter(params) {
    return '$' + formatNumber(params.value)
}

class PortfolioViewer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modules: [
                ServerSideRowModelModule,
                RowGroupingModule,
                MenuModule,
                ColumnsToolPanelModule,
            ],
            currentBreakpoint: "lg",
            compactType: "vertical",
            mounted: false,
            layouts: { lg: props.initialLayout },

            rowData: [
            ],
            backupRowData: [],

            gridOptions: {
                columnTypes: {
                    measure: {
                        width: 100,
                        enableValue: true,
                        aggFunc: 'sum',
                        allowedAggFuncs: ['sum']
                    }
                },

                defaultColDef: {
                    resizable: true,
                    width: 180,
                    filter: "agNumberColumnFilter",
                    filterParams: {
                        applyButton: true,
                        newRowsAction: 'keep'
                    }
                },
                enableSorting: true,
                enableFilter: true,
                rowData: [
                    {businessLine: "Toyota", portfolio: "Celica", price: 35000},
                    {businessLine: "Ford", portfolio: "Mondeo", price: 32000},
                    {businessLine: "Porsche", portfolio: "Boxter", price: 72000}
                ],
                columnDefs: [
                    { field: "businessLine", filter: 'number', rowGroup: true, enableRowGroup: true, enablePivot: false},
                    { field: "portfolio", filter: 'text', rowGroup: true, enableRowGroup: true, enablePivot: false },
                    { field: "domicileMarketValue", type: "measure", filter: 'number', valueFormatter: currencyFormatter },
                    { field: "domicileDailyPnl", type: "measure", filter: 'number', valueFormatter: currencyFormatter },
                    { field: "price", type: "measure", valueFormatter: currencyFormatter },
                    { field: "quantity", type: "measure" },
                    { field: "book", enableRowGroup: true, enablePivot: false },
                    { field: "strategy", enableRowGroup: true, rowGroup: true, enablePivot: false },
                    { field: "secCurrency", filter: 'text', enablePivot: true, enableRowGroup: true },
                    { field: "secCountry", filter: 'text', enablePivot: true, enableRowGroup: true },
                    { field: "bbTicker", filter: 'text', rowGroup: true, enablePivot: true, enableRowGroup: true },
                    { field: "securityId", filter: 'text', enableRowGroup: true, enablePivot: true},
                ],
                enableColResize: true,
                rowModelType: 'serverSide',
                // bring back data 50 rows at a time
                cacheBlockSize: 100,
                rowGroupPanelShow: 'always',
                pivotPanelShow: 'always'
            }
        };



        // grid callbacks
        this.getRowNodeId = this.getRowNodeId.bind(this);
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;

        // if (this.props.rowData) {
        //     this.gridApi.setRowData(this.props.rowData)
        // }
    }

    onCellValueChanged(params) {
        console.log('onCellValueChanged: ', params);
    };

    getRowNodeId(data) {
        return data.symbol;
    }
    //
    // componentWillReceiveProps(nextProps) {
    //     if (this.gridApi) {
    //         const newRowData = nextProps.rowData;
    //
    //         const updatedRows = [];
    //
    //         for (let i = 0; i < newRowData.length; i++) {
    //             let newRow = newRowData[i];
    //             let currentRowNode = this.gridApi.getRowNode(newRow.symbol);
    //
    //             const {data} = currentRowNode;
    //             for (const def of this.state.columnDefs) {
    //                 if (data[def.field] !== newRow[def.field]) {
    //                     updatedRows.push(newRow);
    //                     break;
    //                 }
    //             }
    //         }
    //
    //
    //         this.gridApi.applyTransaction({update: updatedRows});
    //     }
    //
    // }

    render() {

        const onGridReady = (params) => {
            console.log("On grid ready PV *************************************")
            console.log("window.location.href:"+window.location.href);

            let path = 'getRows';
            let sniffedPath = urlSniffPathNoPort(8097, path);
            console.log(sniffedPath);

            // TODO:  Emergency testing use this..
            // sniffedPath = "http://hedge-os.com:8097/getRows";

            this.gridApi = params.api;
            this.gridColumnApi = params.columnApi;

            function ServerSideDatasource() {}

            ServerSideDatasource.prototype.getRows = function (params) {
                let request = params.request;

                let jsonRequest = JSON.stringify(request, null, 2);
                console.log(jsonRequest);

                let httpRequest = new XMLHttpRequest();

                // TODO:  URL Sniffing
                // httpRequest.open('POST', 'http://localhost:8097/getRows');

                console.warn("Druid get rows to="+sniffedPath);

                httpRequest.open('POST', sniffedPath);
                // httpRequest.open('POST', sniffedPath);

                httpRequest.setRequestHeader("Content-type", "application/json");
                httpRequest.send(jsonRequest);
                httpRequest.onreadystatechange = () => {
                    if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                        let result = JSON.parse(httpRequest.responseText);
                        params.successCallback(result.data, result.lastRow);

                        // updateSecondaryColumns(request, result);
                    }
                };
            };

            params.api.setServerSideDatasource(new ServerSideDatasource())

            // this.gridApi.sizeColumnsToFit();
        }


        return (
        <div
            className="ag-theme-alpine-dark"
            style={{ height: '400px', width: '1200px' }}>


            <div style={{ height: '400px', width: '1200px' }}>
                <AgGridReact gridOptions={this.state.gridOptions}
                             modules={this.state.modules}
                             animateRows={true}
                             onGridReady={onGridReady}
                             sideBar={'columns'}>
                </AgGridReact>
            </div>

        </div>
        );
    }
}

export default PortfolioViewer ;

