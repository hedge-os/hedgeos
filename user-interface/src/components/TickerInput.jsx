import React, {Component, useState } from 'react';
import ReactDOM from 'react-dom';
import { AutoComplete } from 'antd';

// import 'OrderTicket.css';
import { Modal, Button } from 'antd';

class TicketInput extends Component {

    constructor() {
        super();
        this.options = [
            { value: 'AAPL', },
            { value: 'AMZN',},
            { value: 'AA', },
            { value: 'B', },
            { value: 'TSLA', },
            { value: 'MSFT', },
        ];

    }

    render() {
        return(
            <AutoComplete
                style={{
                    width: 200,
                }}
                options={this.options}
                placeholder="input NYSE ticker"
                filterOption={(inputValue, option) =>
                    option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                }
            />
        );
    }
}

export default TicketInput;