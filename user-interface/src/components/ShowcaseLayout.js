import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { Responsive, WidthProvider } from "react-grid-layout";
import Provider from "react-redux";
const ResponsiveReactGridLayout = WidthProvider(Responsive);

import PortfolioViewer from "./PortofoioViewer";
import TradeBlotter from "./TradeBlotter";
import OrderTicket from "./OrderTicket";

// import "../chris.css";

export default class ShowcaseLayout extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            currentBreakpoint: "lg",
            compactType: "vertical",
            mounted: false,
            layouts: { lg: props.initialLayout },
        };


        this.onBreakpointChange = this.onBreakpointChange.bind(this);
        this.onCompactTypeChange = this.onCompactTypeChange.bind(this);
        this.onLayoutChange = this.onLayoutChange.bind(this);
        this.onNewLayout = this.onNewLayout.bind(this);
        // this.onGridReady = this.onGridReady(this);
    }

    componentDidMount() {
        this.setState({ mounted: true });
    }

    generateDOM() {
        return _.map(this.state.layouts.lg, function(l, i) {
            return (
                <div key={i} className={l.static ? "static" : ""}>
                    {l.static ? (
                        <span
                            className="text"
                            title="This item is static and cannot be removed or resized."
                        >
              StaticBlah - {i}



            </span>
                    ) : (
                        <span className="text">{i}</span>
                    )}
                </div>
            );
        });
    }

    onBreakpointChange(breakpoint) {
        this.setState({
            currentBreakpoint: breakpoint
        });
    }

    onCompactTypeChange() {
        const { compactType: oldCompactType } = this.state;
        const compactType =
            oldCompactType === "horizontal"
                ? "vertical"
                : oldCompactType === "vertical"
                ? null
                : "horizontal";
        this.setState({ compactType });
    }

    onLayoutChange(layout, layouts) {
        this.props.onLayoutChange(layout, layouts);
    }

    onNewLayout() {
        this.setState({
            layouts: { lg: generateLayout() }
        });
    }

    render() {

        const styles = {
            'fontFamily': '-apple-system, BlinkMacSystemFont,"Segoe UI"',
            'color': 'white',
            // backgroundColor: 'dark grey',
            width: '150px'
        }

        return (
            <div  className="ag-theme-alpine-dark" >
                {/*<div>*/}
                {/*    Current Breakpoint: {this.state.currentBreakpoint} ({*/}
                {/*    this.props.cols[this.state.currentBreakpoint]*/}
                {/*}{" "}*/}
                {/*    columns)*/}
                {/*</div>*/}
                {/*<div>*/}
                {/*    Compaction type:{" "}*/}
                {/*    {_.capitalize(this.state.compactType) || "No Compaction"}*/}
                {/*</div>*/}
                {/*<button onClick={this.onNewLayout}>Generate New Layout</button>*/}
                {/*<button onClick={this.onCompactTypeChange}>*/}
                {/*    Change Compaction Type*/}
                {/*</button>*/}

                <table>
                    <thead/>
                    <tbody>
                        <tr valign={"top"}>
                            <td>
                                <div key="left-nav" style={styles}>
                                    <div align={"center"}>
                                        <h2>hedgeOS</h2>
                                        <table>
                                            <tbody>
                                                <tr><td>- Risk</td></tr>
                                                <tr><td>- Trading</td></tr>
                                                <tr><OrderTicket/></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </td>

                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h3>Risk</h3>
                                                {/*// Port grid*/}
                                                <div key="top-grid" className="static">
                                                    <PortfolioViewer/>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            {/*// Order grid*/}
                                            <td>
                                                <h3>Trading</h3>
                                                <div key="bottom-grid" className="static">
                                                    <TradeBlotter/>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>



                <ResponsiveReactGridLayout
                    {...this.props}
                    layouts={this.state.layouts}
                    onBreakpointChange={this.onBreakpointChange}
                    onLayoutChange={this.onLayoutChange}
                    // WidthProvider option
                    measureBeforeMount={false}
                    // I like to have it animate on mount. If you don't, delete `useCSSTransforms` (it's default `true`)
                    // and set `measureBeforeMount={true}`.
                    // useCSSTransforms={this.state.mounted}
                    // compactType={this.state.compactType}
                    compactType="horizontal"
                    preventCollision={!this.state.compactType} >


                    {/*{this.generateDOM()}*/}
                </ResponsiveReactGridLayout>
            </div>
        );
    }
}

ShowcaseLayout.propTypes = {
    onLayoutChange: PropTypes.func.isRequired
};

ShowcaseLayout.defaultProps = {
    className: "layout",
    rowHeight: 150,
    onLayoutChange: function() {},
    cols: { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 },
    initialLayout:
         [
            {x: 0, y: 0, w: 2, h: 2, i: "left-nav"},
            {x: 1, y: 0, w: 3, h: 1, i: "top-grid"},
            {x: 1, y: 1, w: 3, h: 1, i: "bottom-grid"},
        ]
};
