import {decrementCount} from '../ActionCreators';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import React, {Component} from "react";
import FxPanel from "./FxPanel";
import FxQuoteMatrix from "./FxQuoteMatrix";
import PortfolioGrid from "./PortfolioGrid";

class DecrementButton extends Component {
    render() {
        return (
            <div style={{width: 1250}}>
                <div style={{width: "100%", clear: "both", paddingTop: 25}}>
                    <PortfolioGrid/>
                    {/*<button onClick={() => this.props.decrementCount()}>Decrement Count</button>*/}
                </div>
            </div>
        );
    }
}

DecrementButton.PropTypes = {
    decrementCount: PropTypes.func.isRequired
}

function mapDispatchToProps(dispatch) {
    return {
        decrementCount: () => dispatch(decrementCount())
    };
}

export const DecrementButtonContainer = connect(
    null,
    mapDispatchToProps
)(DecrementButton);
