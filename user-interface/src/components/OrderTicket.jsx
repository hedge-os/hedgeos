import React, {Component, useState } from 'react';
import ReactDOM from 'react-dom';
import { AutoComplete } from 'antd';

// import 'OrderTicket.css';
import { Modal, Button } from 'antd';
import TicketInput from "./TickerInput";

import {
    Form,
    Input,
    Radio,
    Select,
    Cascader,
    DatePicker,
    InputNumber,
    TreeSelect,
    Switch,
} from 'antd';
import FormItemLabel from "antd/es/form/FormItemLabel";


class OrderTicket extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modal2Visible: false,
        };
    }

    handleCancel() {
        // this.setState( {})
        this.setModal2Visible(false);
    }

    setModal2Visible(modal2Visible) {
        console.log("modal 2 visible="+modal2Visible);
        this.setState({ modal2Visible });
    }


    onLimitChange(value) {
        console.log('changed limit', value);
    }

    handleOk () {
        // TODO broken
        this.setModal2Visible(false);
        setTimeout(() => {

        }, 3000);
    };

    render() {

        const styles = {
            backgroundColor: 'black',
            fontFamily: 'Segoe UI',
            color: 'white',
            padding: '0px 0px' // TODO:  Not working, why!?
        };

        return (
            <div style={styles}>

                <Button style={styles} type="primary" onClick={() => this.setModal2Visible(true)}>
                    Order Entry
                </Button>


                <Modal
                    centered
                    visible={this.state.modal2Visible}
                    onOk={() => this.setModal2Visible(false)}
                    onCancel={() => this.setModal2Visible(false)}
                    footer={[
                        <Button key="back" type="primary" onClick={() => this.setModal2Visible(false)} >
                            Submit
                        </Button>,
                        <Button key="submit" onClick={() => this.setModal2Visible(false)}>
                            Cancel
                        </Button>,
                    ]}
                >
                    <h3>Order Entry</h3>
                    <br/>
                    <Form
                        size={"small"}
                        labelCol={{
                            span: 4,
                        }}
                        wrapperCol={{
                            span: 14,
                        }}
                        layout="horizontal"
                    >

                        <Form.Item label="Quantity">
                            <InputNumber />
                        </Form.Item>
                        <Form.Item label="Symbol">
                            <TicketInput/>
                        </Form.Item>
                        <Form.Item label="Order Type">
                            <Select>
                                <Select.Option value="market">Market</Select.Option>
                                <Select.Option value="limit">Limit</Select.Option>
                                <Select.Option value="stop">Stop Limit</Select.Option>
                                <Select.Option value="mit">Market if Touched</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="Limit Price">
                            <InputNumber min={0} max={10000000} step={0.01} onChange={this.onLimitChange()} />
                        </Form.Item>

                    </Form>

                </Modal>
            </div>

        );
    }
}

// export default OrderTicket ;

export default OrderTicket;