import React, {Component} from "react";

import { AgGridReact } from '@ag-grid-community/react';

import { ServerSideRowModelModule } from '@ag-grid-enterprise/server-side-row-model';
import { RowGroupingModule } from '@ag-grid-enterprise/row-grouping';
import { MenuModule } from '@ag-grid-enterprise/menu';
import { ColumnsToolPanelModule } from '@ag-grid-enterprise/column-tool-panel';
import urlSniffPathNoPort from "../utils/hutils";


function formatNumber(value) {
    // this puts commas into the number eg 1000 goes to 1,000,
    // i pulled this from stack overflow, i have no idea how it works
    return Math.floor(value)
        .toString()
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

function currencyFormatter(params) {
    return '$' + formatNumber(params.value)
}

class TradeBlotter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showOrderPopup: false,
            show: false,
            modules: [
                ServerSideRowModelModule,
                RowGroupingModule,
                MenuModule,
                ColumnsToolPanelModule,
            ],
            currentBreakpoint: "lg",
            compactType: "vertical",
            mounted: false,
            layouts: { lg: props.initialLayout },

            rowData: [
            ],
            backupRowData: [],

            gridOptions: {

                columnTypes: {
                    measure: {
                        width: 150,
                        enableValue: true,
                        aggFunc: 'sum',
                        allowedAggFuncs: ['sum']
                    }
                },

                defaultColDef: {
                    width: 180,
                    filter: "agNumberColumnFilter",
                    filterParams: {
                        applyButton: true,
                        newRowsAction: 'keep'
                    }
                },
                enableSorting: true,
                enableFilter: true,
                rowData: [

                ],
                columnDefs: [
                    { field: "portfolio", filter: 'agTextColumnFilter', sortable: true },
                    { field: "price", filter: 'agNumberColumnFilter', type: "measure", sortable: true, valueFormatter: currencyFormatter },
                    { field: "strategy", filter: 'agTextColumnFilter' },
                    { field: "domicileMarketValue", type: "measure", filter: 'number' },
                    { field: "domicileDailyPnl", type: "measure", filter: 'number' },
                    { field: "businessLine", filter: 'number' },
                    { field: "quantity", type: "measure" },
                    { field: "book" },
                    { field: "secCurrency", filter: 'text' },
                    { field: "secCountry", filter: 'text'  },
                    { field: "bbTicker", filter: 'text' },
                    { field: "securityId", filter: 'text' },
                ],
                enableColResize: true,
                rowModelType: 'serverSide',
                // bring back data 50 rows at a time
                cacheBlockSize: 100,
                rowGroupPanelShow: 'never',
                pivotPanelShow: 'never'
            }
        };



        // grid callbacks
        this.getRowNodeId = this.getRowNodeId.bind(this);
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;

        // if (this.props.rowData) {
        //     this.gridApi.setRowData(this.props.rowData)
        // }
    }

    onCellValueChanged(params) {
        console.log('onCellValueChanged: ', params);
    };

    getRowNodeId(data) {
        return data.symbol;
    }

    render() {

        const onGridReady = (params) => {
            console.log("On grid ready PV *************************************")

            console.log("window.location.href:"+window.location.href);

            // TODO:   Must add HA-Proxy here for routing.. (or Envoy!)

            let path = 'order/filterOrdersAgGrid';
            let sniffedPath = urlSniffPathNoPort(8093, path);

            console.log("sniffed path="+sniffedPath);

            // TODO: hacked for now
            // sniffedPath = "http://52.14.203.139:8093/"+path;

            this.gridApi = params.api;
            this.gridColumnApi = params.columnApi;

            function ServerSideDatasource() {}

            ServerSideDatasource.prototype.getRows = function (params) {
                let request = params.request;

                let jsonRequest = JSON.stringify(request, null, 2);
                console.log(jsonRequest);

                let httpRequest = new XMLHttpRequest();

                // AWS
                // httpRequest.open('POST', 'http://18.216.22.242:8097/getRows');
                console.log(sniffedPath);

                httpRequest.open('POST', sniffedPath);

                httpRequest.setRequestHeader("Content-type", "application/json");
                httpRequest.send(jsonRequest);
                httpRequest.onreadystatechange = () => {
                    if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                        let result = JSON.parse(httpRequest.responseText);
                        params.successCallback(result.data, result.lastRow);

                        // updateSecondaryColumns(request, result);
                    }
                };
            };

            params.api.setServerSideDatasource(new ServerSideDatasource())

        }


        return (
        <div>
            <table>
                <tbody>
                    <tr>
                        <td align={"left"} valign={"top"}>

                        </td>

                        <td>
                            <h3>Trade Blotter</h3>
                            <div className="ag-theme-alpine-dark"
                                 style={{ height: '400px', width: '1200px' }}>
                                <AgGridReact gridOptions={this.state.gridOptions}
                                             modules={this.state.modules}
                                             animateRows={true}
                                             onGridReady={onGridReady}
                                             sideBar={'columns'}
                                />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        );
    }
}

export default TradeBlotter ;

