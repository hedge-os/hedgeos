# Hedge OS User interface

### Installation

#### Install Node
```install_node.sh```

Brings down Node.JS 15.X from Nodejs and NPM 7.3.0 (which are leading edge today)

#### Build
```build.sh```

- First runs npm install
- Then npm run build
- Results in dist/index.html and bundle.js

---
Old - we may switch to Golden Layout, use this React Wrapper
---
### golden-layout-react-redux
Minimal example showing how to link redux state to react components embedded within golden-layout.

Run `npm install`, then `npm run dev` and point your browser to `localhost:8080` to run the example.

The example consists of three golden-layout tabs, each with a single React component. There are two buttons and one label which displays a count stored in the Redux state. The buttons increment or decrement this count.
