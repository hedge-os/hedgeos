syntax = "proto3";

option java_multiple_files = true;
option java_package = "com.hedgeos.permission";

import "google/type/date.proto";

// users of hedgeos, not traders
message User {
    int32 user_id = 10;
    string username = 20;
    string first_name = 30;
    string last_name = 40;
    // email the user
    string email_address = 41;
    string alt_email = 42;
    string primary_group_email = 43;

    UserType user_type = 45;

    google.type.Date start_date = 50;
    google.type.Date end_date = 60;
    // users can be in one or more groups,
    // Unix / Windows style
    repeated UserGroup groups = 100;
}

message UserGroup {
    int32 group_id = 10;
    string group_name = 20;
    // this way you can email the group
    string email_address = 30;
}

// purely informational, audit
enum UserType {
    MIDDLE_OFFICE = 0;
    OPERATIONS = 1;
    PORTFOLIO_MANAGER = 2;
    IT = 3;
    RISK = 4;
    OVERSIGHT = 5;
}

// Trade Blotter, Portfolio Viewer
// TODO:  are the reports applications, probably yes
message Application {
    int32 app_id = 10;
    string name = 20;
}

// some default actions, with special meaning
enum ActionEnum {
    UNUSED_ACTION = 0; // to match to database, starts with 1
    PLATFORM_ADMIN = 1; // owner/admin
    PERMISSIONS_ADMIN = 2; // add people to apps
    APP_ADMIN = 3; // admin in an app
    READER = 4;
    WRITER = 5;
    SUBMIT_TRADE = 6;
    UPDATE_TRADE = 7;
    SUBMIT_LOCATE = 8;
    LOGIN = 9;
    IMPERSONATION = 10;
    APP_SUPPORT = 11;
    CUSTOM_A = 12;
    CUSTOM_B = 13;
    CUSTOM_C = 14;
    PRICE_REQUEST = 15;
}

// in the apps, you can take actions
// they may be the defined ones, or define your own
// (database, action table.)
message Action {
    // many actions are well known, use default types
    ActionEnum action_enum = 10;
    // add custom actions to the system
    string custom_action_name = 20;
}

message Feature {
    string name = 10;
}

// What is an AppUser?
// Important, and we manage data permissions, like this.
// A.  users, can take actions, in applications
// B.  users can see certain data, in those apps
//
// We call it an "App User" (which is a, relationship, with attributes)
// it is the way a user is related to an app,
//   ... and what data may be seen ...
//
// Implementation:
//
// In the DB, it may be stored as, as an app_user, or in an, app_group
// in this case, the AppUser APi object, is then generated
//   ... because, a user exits, in a group
// or additionally, it may only be, the user
//      for simplicity, and one-offs
message AppUser {
    User user = 10;
    Application application = 20;
    Action action = 30; // read, write, impersonate
    Feature feature = 31; // delta-hedger enabled, etc.
    // the data you can see
    repeated AppUserData app_datas = 40; // portfolio = CHRIS
}

// an app group is a collection of user permissions
// allowing us to manage at the group level, if we wish
// (slightly preferable, growth focused approach to permissions)
// this way, new joiners, are added to groups, simple
message AppGroup {
    UserGroup group = 10;
    Application application = 20;
    Action action = 30;
    string custom_action = 31;
    repeated AppUserData app_datas = 40;
}

enum DataTypeEnum {
    UNUSED_DATA_TYPE = 0; // to line up the DB with the enum
    EVERYTHING = 1; // C-suite access
    PORTFOLIO = 2; // typically, PMs can see themselves
    PORTFOLIO_GROUP = 3; // one group of PMs
    BUSINESS_LINE = 4; // business owners, can see their PMs
    STRATEGY = 5; // maybe you can see only a strategy, rarely
    TRADES_ONLY = 6;  // certain ops and compliance, only trades
    AGGREGATE_ONLY = 7; // certain risk users, only aggregates
    SUB_STRATEGY = 8; // perhaps an analyst on a sub-strategy
    BOOK = 9; // if you define books
    SUB_BOOK = 10; // if you define sub-books, usable
}

// enums are useful, for major data_types
// however; the database, may easily define additional
// (and either are equivalent to the app via the apis)
message DataType {
    DataTypeEnum data_type_enum = 10;
    string custom_data_type = 20;
}

// map of what data type you can see,
// and the values for the data
// e.g. a PM can see, a Portfolio
message AppUserData {
    DataType data_type = 10;
    // if PORTFOLIO is populated, maybe you see, a handful
    // maybe you see one
    // to see a Portfolio and PortfolioGroup
    // you would have two entries
    repeated string data_values = 20;
    // TODO:  Good test case,
    // one portoflio team and one portfolio
    // TODO:  Which could reconcile, but truly, you check both
}

// what it is you want to do
// in terms of our permissions
message PermissionRequest {
    User user = 10;
    Application app = 20;
    Action action = 30;
    Feature feature = 40;
    DataType data_type = 50;
    string data_value = 60;
}

// various ways you can fail
enum PermissionResultType {
    SUCCESS = 0;
    ERROR = 1;
    UNKNOWN_USER = 2;
    UNKNOWN_APP = 3;
    UNKNOWN_FEATURE = 4;
    NO_USER_APP_MAPPING = 5;
    NO_USER_FEATURE = 6;
    NO_USER_APP_ACTION = 7;
    NO_USER_DATA_TYPE = 8;
    INVALID_USER_DATA_VALUE = 9;
}

message PermissionResponse {
    PermissionResultType perm_result = 10;
    string message = 20;
}
