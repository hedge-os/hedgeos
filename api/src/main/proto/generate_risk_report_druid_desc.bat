protoc -o ../../../../druid-bridge/src/main/resources/risk_report.desc risk_report.proto
cd ../../../../druid-bridge/src/main/resources
docker cp risk_report.desc coordinator:/tmp
docker cp risk_report.desc middlemanager:/tmp