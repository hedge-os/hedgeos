package com.hedgeos.api;

public interface CurveName {
    // usd standard curve names
    String USD_FED_FUNDS = "USD-FED-FUNDS";
    String USD_OIS = "USD-OIS";
    String USD_SOFR = "USD-SOFR";
    String USD_LIBOR_1M = "USD-LIBOR-1M";
    String USD_LIBOR_3M = "USD-LIBOR-3M";
    String USD_LIBOR_6M = "USD-LIBOR-6M";

}
