package com.hedgeos.api;

import com.hedgeos.refdata.service.RefDataServiceGrpc;
import com.hedgeos.service.curve.CurveServiceGrpc;
import com.hedgeos.service.marketdata.MarketDataServiceGrpc;
import com.hedgeos.service.security.SecurityServiceGrpc;

public interface ServiceLocatorInterface {
    // TODO:  Need to do this in Consul or otherwise,
    //  also, return a wrapper which re-connects
    SecurityServiceGrpc.SecurityServiceBlockingStub securityServiceBlockingStub();

    RefDataServiceGrpc.RefDataServiceBlockingStub refDataBlockingStub();

    CurveServiceGrpc.CurveServiceBlockingStub curveServiceBlockingStub();

    MarketDataServiceGrpc.MarketDataServiceBlockingStub marketDataBlockingStub();
}
