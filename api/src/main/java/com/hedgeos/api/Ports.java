package com.hedgeos.api;

import com.hedgeos.key.SecurityKey;

// TODO:  Temporary, replace with Service Location in Consul (spring cloud)
public class Ports {

    public static final int SEC_SERVICE_GRPC = 5051;
    public static final int POS_SERVICE_GRPC = 5053;
    public static final int REF_SERVICE_GRPC = 5091;
    public static final int CURVE_SERVICE_GRPC = 5055;

    // temporary testing
    public static String FAKE_SWAP_NAME = "FAKE LIBOR 3M Swap";
    public static SecurityKey FAKE_SEC_KEY =
            SecurityKey.newBuilder().setName(FAKE_SWAP_NAME).setSecurityId(999L).build();
}
