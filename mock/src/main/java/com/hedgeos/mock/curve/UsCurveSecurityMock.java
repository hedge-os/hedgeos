package com.hedgeos.mock.curve;

import com.hedgeos.curve.Tenor;
import com.hedgeos.general.SecurityType;
import com.opengamma.strata.basics.index.IborIndices;
import com.opengamma.strata.product.deposit.type.TermDepositConventions;
import com.opengamma.strata.product.index.type.IborFutureContractSpecs;
import com.opengamma.strata.product.index.type.OvernightFutureContractSpecs;
import com.opengamma.strata.product.swap.type.FixedIborSwapConventions;
import com.opengamma.strata.product.swap.type.FixedOvernightSwapConventions;
import com.opengamma.strata.product.swap.type.IborIborSwapConventions;
import com.opengamma.strata.product.swap.type.OvernightIborSwapConventions;

public class UsCurveSecurityMock {


    // the original timestamp for all following instrument data is 2020-09-30T19:48:41:418
    public static TestData.SecurityWithMark[] getUsdOisSecurities() {
        // conventions
        String usdFixedTermFedFundOisConvention = FixedOvernightSwapConventions.USD_FIXED_TERM_FED_FUND_OIS.getName();
        String usdFedFund1mCmeConvention = OvernightFutureContractSpecs.USD_FED_FUND_1M_CME.getName();
        String usdFixed1yFedFundOisConvention = FixedOvernightSwapConventions.USD_FIXED_1Y_FED_FUND_OIS.getName();
        String usdFedFundAaLibor3M = OvernightIborSwapConventions.USD_FED_FUND_AA_LIBOR_3M.getName();
        String usdFixed6MLibor3M = FixedIborSwapConventions.USD_FIXED_6M_LIBOR_3M.getName();

        return new TestData.SecurityWithMark[]{
                // OIS swap rate
                TestData.SecurityWithMark.of("USSOA Curncy", SecurityType.OIS, Tenor.T1M, usdFixedTermFedFundOisConvention, 0.00082500),

                // fed funds futures
                TestData.SecurityWithMark.of("FFX0 Comdty", SecurityType.ONF, "2020-11-18", usdFedFund1mCmeConvention, 0.99922500),
                TestData.SecurityWithMark.of("FFZ0 Comdty", SecurityType.ONF, "2020-12-18", usdFedFund1mCmeConvention, 0.99927500),
                TestData.SecurityWithMark.of("FFF1 Comdty", SecurityType.ONF, "2021-01-18", usdFedFund1mCmeConvention, 0.99937500),
                TestData.SecurityWithMark.of("FFG1 Comdty", SecurityType.ONF, "2021-02-18", usdFedFund1mCmeConvention, 0.99942500),
                TestData.SecurityWithMark.of("FFH1 Comdty", SecurityType.ONF, "2021-03-18", usdFedFund1mCmeConvention, 0.99947500),
                TestData.SecurityWithMark.of("FFJ1 Comdty", SecurityType.ONF, "2021-04-18", usdFedFund1mCmeConvention, 0.99952500),
                TestData.SecurityWithMark.of("FFK1 Comdty", SecurityType.ONF, "2021-05-18", usdFedFund1mCmeConvention, 0.99952500),
                TestData.SecurityWithMark.of("FFM1 Comdty", SecurityType.ONF, "2021-06-18", usdFedFund1mCmeConvention, 0.99952500),
                TestData.SecurityWithMark.of("FFN1 Comdty", SecurityType.ONF, "2021-07-18", usdFedFund1mCmeConvention, 0.99962500),
                TestData.SecurityWithMark.of("FFQ1 Comdty", SecurityType.ONF, "2021-08-18", usdFedFund1mCmeConvention, 0.99962500),
                TestData.SecurityWithMark.of("FFU1 Comdty", SecurityType.ONF, "2021-09-18", usdFedFund1mCmeConvention, 0.99962500),
                TestData.SecurityWithMark.of("FFV1 Comdty", SecurityType.ONF, "2021-10-18", usdFedFund1mCmeConvention, 0.99967500),

                // OIS swap rates
                TestData.SecurityWithMark.of("USSO1C Curncy", SecurityType.OIS, Tenor.T15M, usdFixed1yFedFundOisConvention, 0.00052000),
                TestData.SecurityWithMark.of("USSO1F Curncy", SecurityType.OIS, Tenor.T18M, usdFixed1yFedFundOisConvention, 0.00045000),

                // fed funds basis swaps
                TestData.SecurityWithMark.of("USBG2 Curncy", SecurityType.ONI, Tenor.T2Y, usdFedFundAaLibor3M, 0.00180000),
                TestData.SecurityWithMark.of("USBG3 Curncy", SecurityType.ONI, Tenor.T3Y, usdFedFundAaLibor3M, 0.00192500),
                TestData.SecurityWithMark.of("USBG4 Curncy", SecurityType.ONI, Tenor.T4Y, usdFedFundAaLibor3M, 0.00197500),
                TestData.SecurityWithMark.of("USBG5 Curncy", SecurityType.ONI, Tenor.T5Y, usdFedFundAaLibor3M, 0.00198750),

//                // vanilla swaps
//                SecurityWithMark.of("USSWAP2 BGN Curncy", SecurityType.IRS, Tenor.T2Y, usdFixed6MLibor3M, 0.00219700),
//                SecurityWithMark.of("USSWAP3 BGN Curncy", SecurityType.IRS, Tenor.T3Y, usdFixed6MLibor3M, 0.00239900),
//                SecurityWithMark.of("USSWAP4 BGN Curncy", SecurityType.IRS, Tenor.T4Y, usdFixed6MLibor3M, 0.00282600),
//
//                // data incomplete, data taken from USD-LIB-3M data and (almost certainly) assumed to be the same
//                SecurityWithMark.of("USSWAP5 BGN Curncy", SecurityType.IRS, Tenor.T5Y, usdFixed6MLibor3M, 0.00346200),
                TestData.SecurityWithMark.of("USSWAP6 BGN Curncy", SecurityType.IRS, Tenor.T6Y, usdFixed6MLibor3M, 0.00422200),
                TestData.SecurityWithMark.of("USSWAP7 BGN Curncy", SecurityType.IRS, Tenor.T7Y, usdFixed6MLibor3M, 0.00501000),
                TestData.SecurityWithMark.of("USSWAP8 BGN Curncy", SecurityType.IRS, Tenor.T8Y, usdFixed6MLibor3M, 0.00576200),
                TestData.SecurityWithMark.of("USSWAP9 BGN Curncy", SecurityType.IRS, Tenor.T9Y, usdFixed6MLibor3M, 0.00647300),
                TestData.SecurityWithMark.of("USSWAP10 BGN Curncy", SecurityType.IRS, Tenor.T10Y, usdFixed6MLibor3M, 0.00712700),
                TestData.SecurityWithMark.of("USSWAP12 BGN Curncy", SecurityType.IRS, Tenor.T12Y, usdFixed6MLibor3M, 0.00825600),
                TestData.SecurityWithMark.of("USSWAP15 BGN Curncy", SecurityType.IRS, Tenor.T15Y, usdFixed6MLibor3M, 0.00941500),
                TestData.SecurityWithMark.of("USSWAP20 BGN Curncy", SecurityType.IRS, Tenor.T20Y, usdFixed6MLibor3M, 0.01057400),
                TestData.SecurityWithMark.of("USSWAP25 BGN Curncy", SecurityType.IRS, Tenor.T25Y, usdFixed6MLibor3M, 0.01105800),
                TestData.SecurityWithMark.of("USSWAP30 BGN Curncy", SecurityType.IRS, Tenor.T30Y, usdFixed6MLibor3M, 0.01128500),
        };
    }

    public static TestData.SecurityWithMark[] getUsdSofrSecurities() {
        // conventions
        String usdShortDepositT2 = TermDepositConventions.USD_SHORT_DEPOSIT_T2.getName();
        String usdSofr3MImmCme = OvernightFutureContractSpecs.USD_SOFR_3M_IMM_CME.getName();
        String usdFixed1YSofrOis = FixedOvernightSwapConventions.USD_FIXED_1Y_SOFR_OIS.getName();

        return new TestData.SecurityWithMark[]{
                // overnight index
                TestData.SecurityWithMark.of("SOFRRATE Index", SecurityType.DEP, Tenor.T1D, usdShortDepositT2, 0.00070000),

                // overnight futures
                TestData.SecurityWithMark.of("SFRZ0 Comdty", SecurityType.ONF, "2020-12-16", usdSofr3MImmCme, 0.99932500),
                TestData.SecurityWithMark.of("SFRH1 Comdty", SecurityType.ONF, "2021-03-17", usdSofr3MImmCme, 0.99947500),
                TestData.SecurityWithMark.of("SFRM1 Comdty", SecurityType.ONF, "2021-06-16", usdSofr3MImmCme, 0.99957500),
                TestData.SecurityWithMark.of("SFRU1 Comdty", SecurityType.ONF, "2021-09-15", usdSofr3MImmCme, 0.99962500),
                TestData.SecurityWithMark.of("SFRZ1 Comdty", SecurityType.ONF, "2021-12-15", usdSofr3MImmCme, 0.99977500),
                TestData.SecurityWithMark.of("SFRH2 Comdty", SecurityType.ONF, "2022-03-16", usdSofr3MImmCme, 1.00007500),

                // OIS swap rates
                TestData.SecurityWithMark.of("USOSFR2 BGN Curncy", SecurityType.OIS, Tenor.T2Y, usdFixed1YSofrOis, 0.00033000),
                TestData.SecurityWithMark.of("USOSFR3 BGN Curncy", SecurityType.OIS, Tenor.T3Y, usdFixed1YSofrOis, 0.00036100),
                TestData.SecurityWithMark.of("USOSFR4 BGN Curncy", SecurityType.OIS, Tenor.T4Y, usdFixed1YSofrOis, 0.00069500),
                TestData.SecurityWithMark.of("USOSFR5 BGN Curncy", SecurityType.OIS, Tenor.T5Y, usdFixed1YSofrOis, 0.00128300),
                TestData.SecurityWithMark.of("USOSFR7 BGN Curncy", SecurityType.OIS, Tenor.T7Y, usdFixed1YSofrOis, 0.00270000),
                TestData.SecurityWithMark.of("USOSFR10 BGN Curncy", SecurityType.OIS, Tenor.T10Y, usdFixed1YSofrOis, 0.00473000),
                TestData.SecurityWithMark.of("USOSFR12 CMPN Curncy", SecurityType.OIS, Tenor.T12Y, usdFixed1YSofrOis, 0.00581100),
                TestData.SecurityWithMark.of("USOSFR15 BGN Curncy", SecurityType.OIS, Tenor.T15Y, usdFixed1YSofrOis, 0.00692800),
                TestData.SecurityWithMark.of("USOSFR20 BGN Curncy", SecurityType.OIS, Tenor.T20Y, usdFixed1YSofrOis, 0.00804000),
                TestData.SecurityWithMark.of("USOSFR25 CMPN Curncy", SecurityType.OIS, Tenor.T25Y, usdFixed1YSofrOis, 0.00862100),
                TestData.SecurityWithMark.of("USOSFR30 BGN Curncy", SecurityType.OIS, Tenor.T30Y, usdFixed1YSofrOis, 0.00867000),
        };
    }

    public static TestData.SecurityWithMark[] getUsdLibor1MSecurities() {
        // conventions
        String usdLibor1M = IborIndices.USD_LIBOR_1M.getName();
        String usdLibor1MLibor3M = IborIborSwapConventions.USD_LIBOR_1M_LIBOR_3M.getName();

        return new TestData.SecurityWithMark[]{
                // libor index
                TestData.SecurityWithMark.of("US0001M Index", SecurityType.FIX, Tenor.T1M, usdLibor1M, 0.00148250),

                // forward rate agreements
                TestData.SecurityWithMark.of("USFR0AB PREB Curncy", SecurityType.FRA, Tenor.T1M, Tenor.T2M, usdLibor1M, 0.00152000),
                TestData.SecurityWithMark.of("USFR0BC PREB Curncy", SecurityType.FRA, Tenor.T2M, Tenor.T3M, usdLibor1M, 0.00173000),
                TestData.SecurityWithMark.of("USFR0CD PREB Curncy", SecurityType.FRA, Tenor.T3M, Tenor.T4M, usdLibor1M, 0.00167000),
                TestData.SecurityWithMark.of("USFR0DE PREB Curncy", SecurityType.FRA, Tenor.T4M, Tenor.T5M, usdLibor1M, 0.00153000),

                // basis swaps
                TestData.SecurityWithMark.of("USBAAF PREB Curncy", SecurityType.BAS, Tenor.T6M, usdLibor1MLibor3M, 0.00083750),
                TestData.SecurityWithMark.of("USBAAI PREB Curncy", SecurityType.BAS, Tenor.T9M, usdLibor1MLibor3M, 0.00077500),
                TestData.SecurityWithMark.of("USBA1 PREB Curncy", SecurityType.BAS, Tenor.T1Y, usdLibor1MLibor3M, 0.00073750),
                TestData.SecurityWithMark.of("USBA1F PREB Curncy", SecurityType.BAS, Tenor.T18M, usdLibor1MLibor3M, 0.00076250),
                TestData.SecurityWithMark.of("USBA2 PREB Curncy", SecurityType.BAS, Tenor.T2Y, usdLibor1MLibor3M, 0.00086250),
                TestData.SecurityWithMark.of("USBA3 Curncy", SecurityType.BAS, Tenor.T3Y, usdLibor1MLibor3M, 0.00097500),
                TestData.SecurityWithMark.of("USBA4 Curncy", SecurityType.BAS, Tenor.T4Y, usdLibor1MLibor3M, 0.00102500),
                TestData.SecurityWithMark.of("USBA5 Curncy", SecurityType.BAS, Tenor.T5Y, usdLibor1MLibor3M, 0.00106250),
                TestData.SecurityWithMark.of("USBA6 Curncy", SecurityType.BAS, Tenor.T6Y, usdLibor1MLibor3M, 0.00109500),
                TestData.SecurityWithMark.of("USBA7 Curncy", SecurityType.BAS, Tenor.T7Y, usdLibor1MLibor3M, 0.00111500),
                TestData.SecurityWithMark.of("USBA8 Curncy", SecurityType.BAS, Tenor.T8Y, usdLibor1MLibor3M, 0.00112500),
                TestData.SecurityWithMark.of("USBA9 Curncy", SecurityType.BAS, Tenor.T9Y, usdLibor1MLibor3M, 0.00113750),
                TestData.SecurityWithMark.of("USBA10 Curncy", SecurityType.BAS, Tenor.T10Y, usdLibor1MLibor3M, 0.00114950),
                TestData.SecurityWithMark.of("USBA12 Curncy", SecurityType.BAS, Tenor.T12Y, usdLibor1MLibor3M, 0.00116250),
                TestData.SecurityWithMark.of("USBA15 Curncy", SecurityType.BAS, Tenor.T15Y, usdLibor1MLibor3M, 0.00118125),
                TestData.SecurityWithMark.of("USBA20 Curncy", SecurityType.BAS, Tenor.T20Y, usdLibor1MLibor3M, 0.00119375),
                TestData.SecurityWithMark.of("USBA25 Curncy", SecurityType.BAS, Tenor.T25Y, usdLibor1MLibor3M, 0.00120000),
                TestData.SecurityWithMark.of("USBA30 Curncy", SecurityType.BAS, Tenor.T30Y, usdLibor1MLibor3M, 0.00121875),
        };
    }

    public static TestData.SecurityWithMark[] getUsdLibor3MSecurities() {
        // conventions
        String usdLibor3M = IborIndices.USD_LIBOR_3M.getName();
        String usdLibor3MImmCme = IborFutureContractSpecs.USD_LIBOR_3M_IMM_CME.getName();
        String usdFixed6MLibor3M = FixedIborSwapConventions.USD_FIXED_6M_LIBOR_3M.getName();

        return new TestData.SecurityWithMark[]{
                // libor index
                TestData.SecurityWithMark.of("US0003M Index", SecurityType.FIX, Tenor.T3M, usdLibor3M, 0.00233880),

                // futures
                TestData.SecurityWithMark.of("EDZ0 Comdty", SecurityType.IFU, "2020-12-16", usdLibor3MImmCme, 0.99752500),
                TestData.SecurityWithMark.of("EDH1 Comdty", SecurityType.IFU, "2021-03-17", usdLibor3MImmCme, 0.99802500),
                TestData.SecurityWithMark.of("EDM1 Comdty", SecurityType.IFU, "2021-06-16", usdLibor3MImmCme, 0.99807500),
                TestData.SecurityWithMark.of("EDU1 Comdty", SecurityType.IFU, "2021-09-15", usdLibor3MImmCme, 0.99807500),
                TestData.SecurityWithMark.of("EDZ1 Comdty", SecurityType.IFU, "2021-12-15", usdLibor3MImmCme, 0.99782500),
                TestData.SecurityWithMark.of("EDH2 Comdty", SecurityType.IFU, "2022-03-16", usdLibor3MImmCme, 0.99777500),

                // vanilla swaps
                TestData.SecurityWithMark.of("USSWAP2 BGN Curncy", SecurityType.IRS, Tenor.T2Y, usdFixed6MLibor3M, 0.00219700),
                TestData.SecurityWithMark.of("USSWAP3 BGN Curncy", SecurityType.IRS, Tenor.T3Y, usdFixed6MLibor3M, 0.00239900),
                TestData.SecurityWithMark.of("USSWAP4 BGN Curncy", SecurityType.IRS, Tenor.T4Y, usdFixed6MLibor3M, 0.00282600),
                TestData.SecurityWithMark.of("USSWAP5 BGN Curncy", SecurityType.IRS, Tenor.T5Y, usdFixed6MLibor3M, 0.00346200),
                TestData.SecurityWithMark.of("USSWAP6 BGN Curncy", SecurityType.IRS, Tenor.T6Y, usdFixed6MLibor3M, 0.00422200),
                TestData.SecurityWithMark.of("USSWAP7 BGN Curncy", SecurityType.IRS, Tenor.T7Y, usdFixed6MLibor3M, 0.00501000),
                TestData.SecurityWithMark.of("USSWAP8 BGN Curncy", SecurityType.IRS, Tenor.T8Y, usdFixed6MLibor3M, 0.00576200),
                TestData.SecurityWithMark.of("USSWAP9 BGN Curncy", SecurityType.IRS, Tenor.T9Y, usdFixed6MLibor3M, 0.00647300),
                TestData.SecurityWithMark.of("USSWAP10 BGN Curncy", SecurityType.IRS, Tenor.T10Y, usdFixed6MLibor3M, 0.00712700),
                TestData.SecurityWithMark.of("USSWAP12 BGN Curncy", SecurityType.IRS, Tenor.T12Y, usdFixed6MLibor3M, 0.00825600),
                TestData.SecurityWithMark.of("USSWAP15 BGN Curncy", SecurityType.IRS, Tenor.T15Y, usdFixed6MLibor3M, 0.00941500),
                TestData.SecurityWithMark.of("USSWAP20 BGN Curncy", SecurityType.IRS, Tenor.T20Y, usdFixed6MLibor3M, 0.01057400),
                TestData.SecurityWithMark.of("USSWAP25 BGN Curncy", SecurityType.IRS, Tenor.T25Y, usdFixed6MLibor3M, 0.01105800),
                TestData.SecurityWithMark.of("USSWAP30 BGN Curncy", SecurityType.IRS, Tenor.T30Y, usdFixed6MLibor3M, 0.01128500),
        };
    }

    public static TestData.SecurityWithMark[] getUsdLibor6MSecurities() {
        // conventions
        String usdLibor6M = IborIndices.USD_LIBOR_6M.getName();
        String usdLibor3MLibor6M = IborIborSwapConventions.USD_LIBOR_3M_LIBOR_6M.getName();

        return new TestData.SecurityWithMark[]{
                // libor index
                TestData.SecurityWithMark.of("US0006M Index", SecurityType.FIX, Tenor.T6M, usdLibor6M, 0.00259750),

                // basis swaps
                TestData.SecurityWithMark.of("USBFC01 PREB Curncy", SecurityType.BAS, Tenor.T6M, usdLibor3MLibor6M, 0.00065000),
                TestData.SecurityWithMark.of("USBC1 Curncy", SecurityType.BAS, Tenor.T1Y, usdLibor3MLibor6M, 0.00040000),
                TestData.SecurityWithMark.of("USBC2 Curncy", SecurityType.BAS, Tenor.T2Y, usdLibor3MLibor6M, 0.00068125),
                TestData.SecurityWithMark.of("USBC3 Curncy", SecurityType.BAS, Tenor.T3Y, usdLibor3MLibor6M, 0.00092500),
                TestData.SecurityWithMark.of("USBC4 Curncy", SecurityType.BAS, Tenor.T4Y, usdLibor3MLibor6M, 0.00107500),
                TestData.SecurityWithMark.of("USBC5 Curncy", SecurityType.BAS, Tenor.T5Y, usdLibor3MLibor6M, 0.00113500),
                TestData.SecurityWithMark.of("USBC6 Curncy", SecurityType.BAS, Tenor.T6Y, usdLibor3MLibor6M, 0.00118750),
                TestData.SecurityWithMark.of("USBC7 Curncy", SecurityType.BAS, Tenor.T7Y, usdLibor3MLibor6M, 0.00123125),
                TestData.SecurityWithMark.of("USBC8 Curncy", SecurityType.BAS, Tenor.T8Y, usdLibor3MLibor6M, 0.00125625),
                TestData.SecurityWithMark.of("USBC9 Curncy", SecurityType.BAS, Tenor.T9Y, usdLibor3MLibor6M, 0.00128500),
                TestData.SecurityWithMark.of("USBC10 Curncy", SecurityType.BAS, Tenor.T10Y, usdLibor3MLibor6M, 0.00131000),
                TestData.SecurityWithMark.of("USBC12 Curncy", SecurityType.BAS, Tenor.T12Y, usdLibor3MLibor6M, 0.00133750),
                TestData.SecurityWithMark.of("USBC15 Curncy", SecurityType.BAS, Tenor.T15Y, usdLibor3MLibor6M, 0.00136875),
                TestData.SecurityWithMark.of("USBC20 Curncy", SecurityType.BAS, Tenor.T20Y, usdLibor3MLibor6M, 0.00141250),
                TestData.SecurityWithMark.of("USBC25 Curncy", SecurityType.BAS, Tenor.T25Y, usdLibor3MLibor6M, 0.00141250),
                TestData.SecurityWithMark.of("USBC30 Curncy", SecurityType.BAS, Tenor.T30Y, usdLibor3MLibor6M, 0.00141875),
        };
    }
}
