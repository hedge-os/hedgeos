package com.hedgeos.mock.curve;

import com.google.protobuf.Timestamp;
import com.hedgeos.curve.SettlementConvention;
import com.hedgeos.curve.Tenor;
import com.hedgeos.date.ProtoConverter;
import com.hedgeos.daycount.DayCount;
import com.hedgeos.general.MarketData;
import com.hedgeos.general.SecurityType;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.security.Security;
import com.hedgeos.opengamma.OpenGamma;
import com.opengamma.strata.basics.index.IborIndices;
import com.opengamma.strata.product.deposit.type.TermDepositConventions;
import com.opengamma.strata.product.index.type.IborFutureContractSpecs;
import com.opengamma.strata.product.index.type.OvernightFutureContractSpecs;
import com.opengamma.strata.product.swap.type.FixedIborSwapConventions;
import com.opengamma.strata.product.swap.type.FixedOvernightSwapConventions;
import com.opengamma.strata.product.swap.type.IborIborSwapConventions;
import com.opengamma.strata.product.swap.type.OvernightIborSwapConventions;

import java.time.*;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class TestData {
    private static final Map<String, Integer> securityIds = new HashMap<>();
    private static int nextSecurityId = 0;

    private static int getSecurityId(String bbTicker) {
        Integer id = securityIds.get(bbTicker);
        if (id != null) return id;
        securityIds.put(bbTicker, nextSecurityId);
        return nextSecurityId++;
    }

    public static class SecurityWithMark {
        private final Security security;
        private final MarketData.Builder marketData;

        private SecurityWithMark(String bbTicker, SecurityType securityType, Tenor tenor, Tenor fraEnd,
                                 String tenorDate, String convention, double marketPrice) {
            final SecurityKey securityKey = SecurityKey.newBuilder()
                    .setSecurityId(getSecurityId(bbTicker))
                    .setName(bbTicker)
                    .setBbTicker(bbTicker)
                    .build();
            final Security.Builder builder = Security.newBuilder()
                    .setSecurityType(securityType);

            if (tenor != null) {
                builder.setTenor(tenor);
                if (fraEnd != null) builder.setFraEnd(fraEnd);
            } else
                builder.setTenorDate(ProtoConverter.dateFromString(tenorDate));

            security = builder.setSecurityKey(securityKey)
                    .setSettlementConvention(SettlementConvention.newBuilder().setName(convention))
                    .build();

            marketData = MarketData.newBuilder()
                    .setUsed(marketPrice)
                    .setSecurityKey(securityKey);
        }

        static SecurityWithMark of(String bbTicker, SecurityType securityType, Tenor tenor, String convention,
                                   double marketPrice) {
            return new SecurityWithMark(bbTicker, securityType, tenor, null, null, convention,
                    marketPrice);
        }

        static SecurityWithMark of(String bbTicker, SecurityType securityType, Tenor fraStart, Tenor fraEnd,
                                   String convention, double marketPrice) {
            return new SecurityWithMark(bbTicker, securityType, fraStart, fraEnd, null, convention, marketPrice);
        }

        static SecurityWithMark of(String bbTicker, SecurityType securityType, String tenorDate, String convention,
                                   double marketPrice) {
            return new SecurityWithMark(bbTicker, securityType, null, null, tenorDate, convention,
                    marketPrice);
        }

        public Security getSecurity() {
            return security;
        }

        public MarketData getMarketData(Instant timestamp) {
            return marketData.setAsOfTime(
                    Timestamp.newBuilder().setSeconds(timestamp.getEpochSecond()).setNanos(timestamp.getNano())
            ).build();
        }
    }

    /**
     * Bean that holds a UTC date and an interest rate
     */
    public static class RawCurveReference {
        public final String date;
        public final double value;

        public RawCurveReference(String date, double value) {
            this.date = date;
            this.value = value;
        }
    }

    public static class CurveReference {
        private static final int SECONDS_PER_DAY = 24 * 3600;
        private static final long NANOSECONDS_PER_DAY = (long) SECONDS_PER_DAY * 1_000_000_000;

        public final double fractionalYear;
        public final double value;

        private CurveReference(double fractionalYear, double value) {
            this.fractionalYear = fractionalYear;
            this.value = value;
        }

        /**
         * Creates a factory that converts raw curve references into curve references using the provided day count and
         * date parameters
         *
         * @param atTimeOfDay the time of day to assume for each of the dates provided by RawCurveReference
         * @param present     the reference instant that represents the present moment in time
         * @param dayCount    the day count convention to use for the transformation
         * @return a factory method that fulfills the above description
         */
        public static Function<RawCurveReference, CurveReference> getFactory(LocalTime atTimeOfDay, Instant present,
                                                                             DayCount dayCount) {
            // determine OpenGamma day count convention
            com.opengamma.strata.basics.date.DayCount openGammaDayCount = OpenGamma.getDayCount(dayCount);

            // split present time into UTC LocalDate and LocalTime
            LocalDateTime presentDateTime = present.atZone(ZoneOffset.UTC).toLocalDateTime();
            LocalDate presentDate = presentDateTime.toLocalDate();
            LocalTime presentTime = presentDateTime.toLocalTime();

            // to get the intraday fraction right, take into account time of day for both, e.g. if reference time of
            // day is 12:00, and settlement time of day is 19:00 ==> add +7 hours adjustment as a year fraction
            Duration intradayAdjustment = Duration.between(presentTime, atTimeOfDay);

            // and then adjust the fractional year according to how long each day is in fractional terms
            double d = (double) intradayAdjustment.getSeconds() / SECONDS_PER_DAY
                    + (double) intradayAdjustment.getNano() / NANOSECONDS_PER_DAY;

            return rawCurveReference -> {
                // parse settlement date
                LocalDate settlementDate = LocalDate.parse(rawCurveReference.date);

                // determine fractional duration in years between reference and settlement times
                double relativeYearFraction = openGammaDayCount.relativeYearFraction(presentDate, settlementDate);

                // for this, we also need the fractional-year length of one day (note that this step is necessary as the
                // year fraction derived above may equal zero - nonetheless, to adjust for intraday time differences, we
                // need to know how the day count determines the fractional-year length of one day)
                double fractionalDay = openGammaDayCount.relativeYearFraction(presentDate, settlementDate.plusDays(1))
                        - relativeYearFraction;

                return new CurveReference(relativeYearFraction + d * fractionalDay, rawCurveReference.value);
            };
        }
    }

    public static RawCurveReference[] getUsdOisReferenceCurve() {
        return new RawCurveReference[]{
                new RawCurveReference("2020-09-30", 1.0000000000000000),
                new RawCurveReference("2020-11-06", 0.9999152149398243),
                new RawCurveReference("2020-12-01", 0.9998620955863181),
                new RawCurveReference("2020-12-17", 0.9998304955647407),
                new RawCurveReference("2021-01-28", 0.9997442023922819),
                new RawCurveReference("2021-03-01", 0.9996886626906186),
                new RawCurveReference("2021-03-18", 0.9996597138345401),
                new RawCurveReference("2021-04-29", 0.9995980701370238),
                new RawCurveReference("2021-06-01", 0.9995545469790799),
                new RawCurveReference("2021-06-17", 0.9995338619874825),
                new RawCurveReference("2021-07-29", 0.9994772234207770),
                new RawCurveReference("2021-09-01", 0.9994418259301281),
                new RawCurveReference("2021-09-23", 0.9994178118787945),
                new RawCurveReference("2021-11-04", 0.9993799180424909),
                new RawCurveReference("2022-01-05", 0.9993331113033435),
                new RawCurveReference("2022-04-06", 0.9993092800679606),
                new RawCurveReference("2022-10-03", 0.9992508597963444),
                new RawCurveReference("2023-10-02", 0.9986548981748541),
                new RawCurveReference("2024-10-02", 0.9967110285742337),
                new RawCurveReference("2025-10-02", 0.9927818872482310),
                new RawCurveReference("2026-10-02", 0.9867302286172684),
                new RawCurveReference("2027-10-04", 0.9790474383986976),
                new RawCurveReference("2028-10-02", 0.9700760499608916),
                new RawCurveReference("2029-10-02", 0.9601070563154539),
                new RawCurveReference("2030-10-02", 0.9493871212688517),
                new RawCurveReference("2032-10-04", 0.9261095207579400),
                new RawCurveReference("2035-10-02", 0.8912975082315643),
                new RawCurveReference("2040-10-02", 0.8356791001869178),
        };
    }

    public static RawCurveReference[] getUsdSofrReferenceCurve() {
        return new RawCurveReference[]{
                new RawCurveReference("2020-09-30", 1.0000000000000000),
                new RawCurveReference("2020-10-01", 0.9999980555593199),
                new RawCurveReference("2021-03-17", 0.9996757206623532),
                new RawCurveReference("2021-06-16", 0.9995430729655392),
                new RawCurveReference("2021-09-15", 0.9994357030312144),
                new RawCurveReference("2021-12-15", 0.9993409738332305),
                new RawCurveReference("2021-03-16", 0.9992841395462878),
                new RawCurveReference("2022-06-15", 0.9993030846657620),
                new RawCurveReference("2022-10-05", 0.9993264323217068),
                new RawCurveReference("2023-10-04", 0.9988937349298157),
                new RawCurveReference("2024-10-04", 0.9971646607082687),
                new RawCurveReference("2025-10-06", 0.9934526689755853),
                new RawCurveReference("2027-10-06", 0.9808853233846228),
                new RawCurveReference("2030-10-04", 0.9526946431530985),
                new RawCurveReference("2032-10-06", 0.9308156162875899),
                new RawCurveReference("2035-10-04", 0.8984072031002153),
                new RawCurveReference("2040-10-04", 0.8467276929302329),
                new RawCurveReference("2045-10-04", 0.7999004858107287),
                new RawCurveReference("2050-10-05", 0.7645391772820926),
        };
    }

    public static RawCurveReference[] getUsdLibor1MReferenceCurve() {
        return new RawCurveReference[]{
                new RawCurveReference("2020-09-30", 1.0000000000000000),
                new RawCurveReference("2020-11-02", 0.9998641220723825),
                new RawCurveReference("2020-12-02", 0.9997374886571526),
                new RawCurveReference("2021-01-04", 0.9995789720918283),
                new RawCurveReference("2021-02-04", 0.9994352477507837),
                new RawCurveReference("2021-03-02", 0.9993255999188762),
                new RawCurveReference("2021-04-08", 0.9991908744491963),
                new RawCurveReference("2021-07-02", 0.9988902916153181),
                new RawCurveReference("2021-10-07", 0.9985421353341708),
                new RawCurveReference("2022-04-04", 0.9979137875415096),
                new RawCurveReference("2022-10-06", 0.9973376999639182),
                new RawCurveReference("2023-10-05", 0.9957544168966268),
                new RawCurveReference("2024-10-03", 0.9928664960528910),
                new RawCurveReference("2025-10-02", 0.9881404267341781),
                new RawCurveReference("2026-10-02", 0.9814738637357494),
                new RawCurveReference("2027-10-07", 0.9730364296607991),
                new RawCurveReference("2028-10-05", 0.9634607532392522),
                new RawCurveReference("2029-10-04", 0.9529041567322227),
                new RawCurveReference("2030-10-03", 0.9416402985880590),
                new RawCurveReference("2032-10-07", 0.9175358735203518),
                new RawCurveReference("2035-10-04", 0.8822730253585204),
                new RawCurveReference("2040-10-04", 0.8260854470609484),
                new RawCurveReference("2045-10-05", 0.7779176153568024),
                new RawCurveReference("2050-10-06", 0.7353332022436952),
        };
    }

    public static RawCurveReference[] getUsdLibor3MReferenceCurve() {
        return new RawCurveReference[]{
                new RawCurveReference("2020-09-30", 1.0000000000000000),
                new RawCurveReference("2021-01-04", 0.9993771500841355),
                new RawCurveReference("2021-03-16", 0.9988883728204649),
                new RawCurveReference("2021-06-17", 0.9983783035208196),
                new RawCurveReference("2021-09-16", 0.9978929483159542),
                new RawCurveReference("2021-12-15", 0.9974135441704088),
                new RawCurveReference("2022-03-15", 0.9968727133094388),
                new RawCurveReference("2022-06-16", 0.9963022225415750),
                new RawCurveReference("2022-10-03", 0.9955984891538671),
                new RawCurveReference("2023-10-02", 0.9928187509339987),
                new RawCurveReference("2024-10-02", 0.9887501022281704),
                new RawCurveReference("2025-10-02", 0.9828242133397382),
                new RawCurveReference("2026-10-02", 0.9749494651401853),
                new RawCurveReference("2027-10-04", 0.9654243653457335),
                new RawCurveReference("2028-10-02", 0.9547789431816582),
                new RawCurveReference("2029-10-02", 0.9431104825163816),
                new RawCurveReference("2030-10-02", 0.9307469202922876),
                new RawCurveReference("2032-10-04", 0.9047257282891713),
                new RawCurveReference("2035-10-02", 0.8666075057699105),
                new RawCurveReference("2040-10-02", 0.8063615301502227),
                new RawCurveReference("2045-10-02", 0.7546577786905704),
                new RawCurveReference("2050-10-03", 0.7085566249962730),
        };
    }

    public static RawCurveReference[] getUsdLibor6MReferenceCurve() {
        return new RawCurveReference[]{
                new RawCurveReference("2020-09-30", 1.0000000000000000),
                new RawCurveReference("2021-04-06", 0.9986468125441299),
                new RawCurveReference("2021-06-16", 0.9980413268838530),
                new RawCurveReference("2021-10-04", 0.9973909890998532),
                new RawCurveReference("2022-10-03", 0.9942220736420944),
                new RawCurveReference("2023-10-02", 0.9900309896409824),
                new RawCurveReference("2024-10-02", 0.9844503691925462),
                new RawCurveReference("2025-10-02", 0.9771902663752730),
                new RawCurveReference("2026-10-02", 0.9679402177992396),
                new RawCurveReference("2027-10-04", 0.9570267021865273),
                new RawCurveReference("2028-10-02", 0.9451079724316884),
                new RawCurveReference("2029-10-02", 0.9321267184739008),
                new RawCurveReference("2030-10-02", 0.9184785554863442),
                new RawCurveReference("2032-10-04", 0.8901305659598269),
                new RawCurveReference("2035-10-02", 0.8487714213551824),
                new RawCurveReference("2040-10-02", 0.7835887948287188),
                new RawCurveReference("2045-10-02", 0.7281477919641922),
                new RawCurveReference("2050-10-03", 0.6786648314943500),
        };
    }

    public static RawCurveReference[] getOriginalReferenceCurve() {
        return new RawCurveReference[]{
                new RawCurveReference("2020-09-23", 1.00000000000),
                new RawCurveReference("2020-11-06", 0.99989856432),
                new RawCurveReference("2020-12-01", 0.99984551528),
                new RawCurveReference("2020-12-17", 0.99981183285),
                new RawCurveReference("2021-01-28", 0.99973137300),
                new RawCurveReference("2021-03-01", 0.99968027700),
                new RawCurveReference("2021-03-18", 0.99965563200),
                new RawCurveReference("2021-04-29", 0.99959393900),
                new RawCurveReference("2021-06-01", 0.99955046600),
                new RawCurveReference("2021-06-17", 0.99952971100),
                new RawCurveReference("2021-07-29", 0.99947952800),
                new RawCurveReference("2021-09-01", 0.99944413000),
                new RawCurveReference("2021-09-23", 0.99942219800),
                new RawCurveReference("2021-11-04", 0.99938430400),
                new RawCurveReference("2021-12-29", 0.99935891100),
                new RawCurveReference("2022-03-29", 0.99932735600),
                new RawCurveReference("2022-09-26", 0.99931149800),
                new RawCurveReference("2023-09-25", 0.99894040000),
                new RawCurveReference("2024-09-25", 0.99710810000),
                new RawCurveReference("2025-09-25", 0.99335860000),
                new RawCurveReference("2026-09-25", 0.98785570000),
        };
    }

    // the original timestamp for all following instrument data is 2020-09-30T19:48:41:418
    public static SecurityWithMark[] getUsdOisSecurities() {
        // conventions
        String usdFixedTermFedFundOisConvention = FixedOvernightSwapConventions.USD_FIXED_TERM_FED_FUND_OIS.getName();
        String usdFedFund1mCmeConvention = OvernightFutureContractSpecs.USD_FED_FUND_1M_CME.getName();
        String usdFixed1yFedFundOisConvention = FixedOvernightSwapConventions.USD_FIXED_1Y_FED_FUND_OIS.getName();
        String usdFedFundAaLibor3M = OvernightIborSwapConventions.USD_FED_FUND_AA_LIBOR_3M.getName();
        String usdFixed6MLibor3M = FixedIborSwapConventions.USD_FIXED_6M_LIBOR_3M.getName();

        return new SecurityWithMark[]{
                // OIS swap rate
                SecurityWithMark.of("USSOA Curncy", SecurityType.OIS, Tenor.T1M, usdFixedTermFedFundOisConvention, 0.00082500),

                // fed funds futures
                SecurityWithMark.of("FFX0 Comdty", SecurityType.ONF, "2020-11-18", usdFedFund1mCmeConvention, 0.99922500),
                SecurityWithMark.of("FFZ0 Comdty", SecurityType.ONF, "2020-12-18", usdFedFund1mCmeConvention, 0.99927500),
                SecurityWithMark.of("FFF1 Comdty", SecurityType.ONF, "2021-01-18", usdFedFund1mCmeConvention, 0.99937500),
                SecurityWithMark.of("FFG1 Comdty", SecurityType.ONF, "2021-02-18", usdFedFund1mCmeConvention, 0.99942500),
                SecurityWithMark.of("FFH1 Comdty", SecurityType.ONF, "2021-03-18", usdFedFund1mCmeConvention, 0.99947500),
                SecurityWithMark.of("FFJ1 Comdty", SecurityType.ONF, "2021-04-18", usdFedFund1mCmeConvention, 0.99952500),
                SecurityWithMark.of("FFK1 Comdty", SecurityType.ONF, "2021-05-18", usdFedFund1mCmeConvention, 0.99952500),
                SecurityWithMark.of("FFM1 Comdty", SecurityType.ONF, "2021-06-18", usdFedFund1mCmeConvention, 0.99952500),
                SecurityWithMark.of("FFN1 Comdty", SecurityType.ONF, "2021-07-18", usdFedFund1mCmeConvention, 0.99962500),
                SecurityWithMark.of("FFQ1 Comdty", SecurityType.ONF, "2021-08-18", usdFedFund1mCmeConvention, 0.99962500),
                SecurityWithMark.of("FFU1 Comdty", SecurityType.ONF, "2021-09-18", usdFedFund1mCmeConvention, 0.99962500),
                SecurityWithMark.of("FFV1 Comdty", SecurityType.ONF, "2021-10-18", usdFedFund1mCmeConvention, 0.99967500),

                // OIS swap rates
                SecurityWithMark.of("USSO1C Curncy", SecurityType.OIS, Tenor.T15M, usdFixed1yFedFundOisConvention, 0.00052000),
                SecurityWithMark.of("USSO1F Curncy", SecurityType.OIS, Tenor.T18M, usdFixed1yFedFundOisConvention, 0.00045000),

                // fed funds basis swaps
                SecurityWithMark.of("USBG2 Curncy", SecurityType.ONI, Tenor.T2Y, usdFedFundAaLibor3M, 0.00180000),
                SecurityWithMark.of("USBG3 Curncy", SecurityType.ONI, Tenor.T3Y, usdFedFundAaLibor3M, 0.00192500),
                SecurityWithMark.of("USBG4 Curncy", SecurityType.ONI, Tenor.T4Y, usdFedFundAaLibor3M, 0.00197500),
                SecurityWithMark.of("USBG5 Curncy", SecurityType.ONI, Tenor.T5Y, usdFedFundAaLibor3M, 0.00198750),

//                // vanilla swaps
//                SecurityWithMark.of("USSWAP2 BGN Curncy", SecurityType.IRS, Tenor.T2Y, usdFixed6MLibor3M, 0.00219700),
//                SecurityWithMark.of("USSWAP3 BGN Curncy", SecurityType.IRS, Tenor.T3Y, usdFixed6MLibor3M, 0.00239900),
//                SecurityWithMark.of("USSWAP4 BGN Curncy", SecurityType.IRS, Tenor.T4Y, usdFixed6MLibor3M, 0.00282600),
//
//                // data incomplete, data taken from USD-LIB-3M data and (almost certainly) assumed to be the same
//                SecurityWithMark.of("USSWAP5 BGN Curncy", SecurityType.IRS, Tenor.T5Y, usdFixed6MLibor3M, 0.00346200),
                SecurityWithMark.of("USSWAP6 BGN Curncy", SecurityType.IRS, Tenor.T6Y, usdFixed6MLibor3M, 0.00422200),
                SecurityWithMark.of("USSWAP7 BGN Curncy", SecurityType.IRS, Tenor.T7Y, usdFixed6MLibor3M, 0.00501000),
                SecurityWithMark.of("USSWAP8 BGN Curncy", SecurityType.IRS, Tenor.T8Y, usdFixed6MLibor3M, 0.00576200),
                SecurityWithMark.of("USSWAP9 BGN Curncy", SecurityType.IRS, Tenor.T9Y, usdFixed6MLibor3M, 0.00647300),
                SecurityWithMark.of("USSWAP10 BGN Curncy", SecurityType.IRS, Tenor.T10Y, usdFixed6MLibor3M, 0.00712700),
                SecurityWithMark.of("USSWAP12 BGN Curncy", SecurityType.IRS, Tenor.T12Y, usdFixed6MLibor3M, 0.00825600),
                SecurityWithMark.of("USSWAP15 BGN Curncy", SecurityType.IRS, Tenor.T15Y, usdFixed6MLibor3M, 0.00941500),
                SecurityWithMark.of("USSWAP20 BGN Curncy", SecurityType.IRS, Tenor.T20Y, usdFixed6MLibor3M, 0.01057400),
                SecurityWithMark.of("USSWAP25 BGN Curncy", SecurityType.IRS, Tenor.T25Y, usdFixed6MLibor3M, 0.01105800),
                SecurityWithMark.of("USSWAP30 BGN Curncy", SecurityType.IRS, Tenor.T30Y, usdFixed6MLibor3M, 0.01128500),
        };
    }

    public static SecurityWithMark[] getUsdSofrSecurities() {
        // conventions
        String usdShortDepositT2 = TermDepositConventions.USD_SHORT_DEPOSIT_T2.getName();
        String usdSofr3MImmCme = OvernightFutureContractSpecs.USD_SOFR_3M_IMM_CME.getName();
        String usdFixed1YSofrOis = FixedOvernightSwapConventions.USD_FIXED_1Y_SOFR_OIS.getName();

        return new SecurityWithMark[]{
                // overnight index
                SecurityWithMark.of("SOFRRATE Index", SecurityType.DEP, Tenor.T1D, usdShortDepositT2, 0.00070000),

                // overnight futures
                SecurityWithMark.of("SFRZ0 Comdty", SecurityType.ONF, "2020-12-16", usdSofr3MImmCme, 0.99932500),
                SecurityWithMark.of("SFRH1 Comdty", SecurityType.ONF, "2021-03-17", usdSofr3MImmCme, 0.99947500),
                SecurityWithMark.of("SFRM1 Comdty", SecurityType.ONF, "2021-06-16", usdSofr3MImmCme, 0.99957500),
                SecurityWithMark.of("SFRU1 Comdty", SecurityType.ONF, "2021-09-15", usdSofr3MImmCme, 0.99962500),
                SecurityWithMark.of("SFRZ1 Comdty", SecurityType.ONF, "2021-12-15", usdSofr3MImmCme, 0.99977500),
                SecurityWithMark.of("SFRH2 Comdty", SecurityType.ONF, "2022-03-16", usdSofr3MImmCme, 1.00007500),

                // OIS swap rates
                SecurityWithMark.of("USOSFR2 BGN Curncy", SecurityType.OIS, Tenor.T2Y, usdFixed1YSofrOis, 0.00033000),
                SecurityWithMark.of("USOSFR3 BGN Curncy", SecurityType.OIS, Tenor.T3Y, usdFixed1YSofrOis, 0.00036100),
                SecurityWithMark.of("USOSFR4 BGN Curncy", SecurityType.OIS, Tenor.T4Y, usdFixed1YSofrOis, 0.00069500),
                SecurityWithMark.of("USOSFR5 BGN Curncy", SecurityType.OIS, Tenor.T5Y, usdFixed1YSofrOis, 0.00128300),
                SecurityWithMark.of("USOSFR7 BGN Curncy", SecurityType.OIS, Tenor.T7Y, usdFixed1YSofrOis, 0.00270000),
                SecurityWithMark.of("USOSFR10 BGN Curncy", SecurityType.OIS, Tenor.T10Y, usdFixed1YSofrOis, 0.00473000),
                SecurityWithMark.of("USOSFR12 CMPN Curncy", SecurityType.OIS, Tenor.T12Y, usdFixed1YSofrOis, 0.00581100),
                SecurityWithMark.of("USOSFR15 BGN Curncy", SecurityType.OIS, Tenor.T15Y, usdFixed1YSofrOis, 0.00692800),
                SecurityWithMark.of("USOSFR20 BGN Curncy", SecurityType.OIS, Tenor.T20Y, usdFixed1YSofrOis, 0.00804000),
                SecurityWithMark.of("USOSFR25 CMPN Curncy", SecurityType.OIS, Tenor.T25Y, usdFixed1YSofrOis, 0.00862100),
                SecurityWithMark.of("USOSFR30 BGN Curncy", SecurityType.OIS, Tenor.T30Y, usdFixed1YSofrOis, 0.00867000),
        };
    }

    public static SecurityWithMark[] getUsdLibor1MSecurities() {
        // conventions
        String usdLibor1M = IborIndices.USD_LIBOR_1M.getName();
        String usdLibor1MLibor3M = IborIborSwapConventions.USD_LIBOR_1M_LIBOR_3M.getName();

        return new SecurityWithMark[]{
                // libor index
                SecurityWithMark.of("US0001M Index", SecurityType.FIX, Tenor.T1M, usdLibor1M, 0.00148250),

                // forward rate agreements
                SecurityWithMark.of("USFR0AB PREB Curncy", SecurityType.FRA, Tenor.T1M, Tenor.T2M, usdLibor1M, 0.00152000),
                SecurityWithMark.of("USFR0BC PREB Curncy", SecurityType.FRA, Tenor.T2M, Tenor.T3M, usdLibor1M, 0.00173000),
                SecurityWithMark.of("USFR0CD PREB Curncy", SecurityType.FRA, Tenor.T3M, Tenor.T4M, usdLibor1M, 0.00167000),
                SecurityWithMark.of("USFR0DE PREB Curncy", SecurityType.FRA, Tenor.T4M, Tenor.T5M, usdLibor1M, 0.00153000),

                // basis swaps
                SecurityWithMark.of("USBAAF PREB Curncy", SecurityType.BAS, Tenor.T6M, usdLibor1MLibor3M, 0.00083750),
                SecurityWithMark.of("USBAAI PREB Curncy", SecurityType.BAS, Tenor.T9M, usdLibor1MLibor3M, 0.00077500),
                SecurityWithMark.of("USBA1 PREB Curncy", SecurityType.BAS, Tenor.T1Y, usdLibor1MLibor3M, 0.00073750),
                SecurityWithMark.of("USBA1F PREB Curncy", SecurityType.BAS, Tenor.T18M, usdLibor1MLibor3M, 0.00076250),
                SecurityWithMark.of("USBA2 PREB Curncy", SecurityType.BAS, Tenor.T2Y, usdLibor1MLibor3M, 0.00086250),
                SecurityWithMark.of("USBA3 Curncy", SecurityType.BAS, Tenor.T3Y, usdLibor1MLibor3M, 0.00097500),
                SecurityWithMark.of("USBA4 Curncy", SecurityType.BAS, Tenor.T4Y, usdLibor1MLibor3M, 0.00102500),
                SecurityWithMark.of("USBA5 Curncy", SecurityType.BAS, Tenor.T5Y, usdLibor1MLibor3M, 0.00106250),
                SecurityWithMark.of("USBA6 Curncy", SecurityType.BAS, Tenor.T6Y, usdLibor1MLibor3M, 0.00109500),
                SecurityWithMark.of("USBA7 Curncy", SecurityType.BAS, Tenor.T7Y, usdLibor1MLibor3M, 0.00111500),
                SecurityWithMark.of("USBA8 Curncy", SecurityType.BAS, Tenor.T8Y, usdLibor1MLibor3M, 0.00112500),
                SecurityWithMark.of("USBA9 Curncy", SecurityType.BAS, Tenor.T9Y, usdLibor1MLibor3M, 0.00113750),
                SecurityWithMark.of("USBA10 Curncy", SecurityType.BAS, Tenor.T10Y, usdLibor1MLibor3M, 0.00114950),
                SecurityWithMark.of("USBA12 Curncy", SecurityType.BAS, Tenor.T12Y, usdLibor1MLibor3M, 0.00116250),
                SecurityWithMark.of("USBA15 Curncy", SecurityType.BAS, Tenor.T15Y, usdLibor1MLibor3M, 0.00118125),
                SecurityWithMark.of("USBA20 Curncy", SecurityType.BAS, Tenor.T20Y, usdLibor1MLibor3M, 0.00119375),
                SecurityWithMark.of("USBA25 Curncy", SecurityType.BAS, Tenor.T25Y, usdLibor1MLibor3M, 0.00120000),
                SecurityWithMark.of("USBA30 Curncy", SecurityType.BAS, Tenor.T30Y, usdLibor1MLibor3M, 0.00121875),
        };
    }

    public static SecurityWithMark[] getUsdLibor3MSecurities() {
        // conventions
        String usdLibor3M = IborIndices.USD_LIBOR_3M.getName();
        String usdLibor3MImmCme = IborFutureContractSpecs.USD_LIBOR_3M_IMM_CME.getName();
        String usdFixed6MLibor3M = FixedIborSwapConventions.USD_FIXED_6M_LIBOR_3M.getName();

        return new SecurityWithMark[]{
                // libor index
                SecurityWithMark.of("US0003M Index", SecurityType.FIX, Tenor.T3M, usdLibor3M, 0.00233880),

                // futures
                SecurityWithMark.of("EDZ0 Comdty", SecurityType.IFU, "2020-12-16", usdLibor3MImmCme, 0.99752500),
                SecurityWithMark.of("EDH1 Comdty", SecurityType.IFU, "2021-03-17", usdLibor3MImmCme, 0.99802500),
                SecurityWithMark.of("EDM1 Comdty", SecurityType.IFU, "2021-06-16", usdLibor3MImmCme, 0.99807500),
                SecurityWithMark.of("EDU1 Comdty", SecurityType.IFU, "2021-09-15", usdLibor3MImmCme, 0.99807500),
                SecurityWithMark.of("EDZ1 Comdty", SecurityType.IFU, "2021-12-15", usdLibor3MImmCme, 0.99782500),
                SecurityWithMark.of("EDH2 Comdty", SecurityType.IFU, "2022-03-16", usdLibor3MImmCme, 0.99777500),

                // vanilla swaps
                SecurityWithMark.of("USSWAP2 BGN Curncy", SecurityType.IRS, Tenor.T2Y, usdFixed6MLibor3M, 0.00219700),
                SecurityWithMark.of("USSWAP3 BGN Curncy", SecurityType.IRS, Tenor.T3Y, usdFixed6MLibor3M, 0.00239900),
                SecurityWithMark.of("USSWAP4 BGN Curncy", SecurityType.IRS, Tenor.T4Y, usdFixed6MLibor3M, 0.00282600),
                SecurityWithMark.of("USSWAP5 BGN Curncy", SecurityType.IRS, Tenor.T5Y, usdFixed6MLibor3M, 0.00346200),
                SecurityWithMark.of("USSWAP6 BGN Curncy", SecurityType.IRS, Tenor.T6Y, usdFixed6MLibor3M, 0.00422200),
                SecurityWithMark.of("USSWAP7 BGN Curncy", SecurityType.IRS, Tenor.T7Y, usdFixed6MLibor3M, 0.00501000),
                SecurityWithMark.of("USSWAP8 BGN Curncy", SecurityType.IRS, Tenor.T8Y, usdFixed6MLibor3M, 0.00576200),
                SecurityWithMark.of("USSWAP9 BGN Curncy", SecurityType.IRS, Tenor.T9Y, usdFixed6MLibor3M, 0.00647300),
                SecurityWithMark.of("USSWAP10 BGN Curncy", SecurityType.IRS, Tenor.T10Y, usdFixed6MLibor3M, 0.00712700),
                SecurityWithMark.of("USSWAP12 BGN Curncy", SecurityType.IRS, Tenor.T12Y, usdFixed6MLibor3M, 0.00825600),
                SecurityWithMark.of("USSWAP15 BGN Curncy", SecurityType.IRS, Tenor.T15Y, usdFixed6MLibor3M, 0.00941500),
                SecurityWithMark.of("USSWAP20 BGN Curncy", SecurityType.IRS, Tenor.T20Y, usdFixed6MLibor3M, 0.01057400),
                SecurityWithMark.of("USSWAP25 BGN Curncy", SecurityType.IRS, Tenor.T25Y, usdFixed6MLibor3M, 0.01105800),
                SecurityWithMark.of("USSWAP30 BGN Curncy", SecurityType.IRS, Tenor.T30Y, usdFixed6MLibor3M, 0.01128500),
        };
    }

    public static SecurityWithMark[] getUsdLibor6MSecurities() {
        // conventions
        String usdLibor6M = IborIndices.USD_LIBOR_6M.getName();
        String usdLibor3MLibor6M = IborIborSwapConventions.USD_LIBOR_3M_LIBOR_6M.getName();

        return new SecurityWithMark[]{
                // libor index
                SecurityWithMark.of("US0006M Index", SecurityType.FIX, Tenor.T6M, usdLibor6M, 0.00259750),

                // basis swaps
                SecurityWithMark.of("USBFC01 PREB Curncy", SecurityType.BAS, Tenor.T6M, usdLibor3MLibor6M, 0.00065000),
                SecurityWithMark.of("USBC1 Curncy", SecurityType.BAS, Tenor.T1Y, usdLibor3MLibor6M, 0.00040000),
                SecurityWithMark.of("USBC2 Curncy", SecurityType.BAS, Tenor.T2Y, usdLibor3MLibor6M, 0.00068125),
                SecurityWithMark.of("USBC3 Curncy", SecurityType.BAS, Tenor.T3Y, usdLibor3MLibor6M, 0.00092500),
                SecurityWithMark.of("USBC4 Curncy", SecurityType.BAS, Tenor.T4Y, usdLibor3MLibor6M, 0.00107500),
                SecurityWithMark.of("USBC5 Curncy", SecurityType.BAS, Tenor.T5Y, usdLibor3MLibor6M, 0.00113500),
                SecurityWithMark.of("USBC6 Curncy", SecurityType.BAS, Tenor.T6Y, usdLibor3MLibor6M, 0.00118750),
                SecurityWithMark.of("USBC7 Curncy", SecurityType.BAS, Tenor.T7Y, usdLibor3MLibor6M, 0.00123125),
                SecurityWithMark.of("USBC8 Curncy", SecurityType.BAS, Tenor.T8Y, usdLibor3MLibor6M, 0.00125625),
                SecurityWithMark.of("USBC9 Curncy", SecurityType.BAS, Tenor.T9Y, usdLibor3MLibor6M, 0.00128500),
                SecurityWithMark.of("USBC10 Curncy", SecurityType.BAS, Tenor.T10Y, usdLibor3MLibor6M, 0.00131000),
                SecurityWithMark.of("USBC12 Curncy", SecurityType.BAS, Tenor.T12Y, usdLibor3MLibor6M, 0.00133750),
                SecurityWithMark.of("USBC15 Curncy", SecurityType.BAS, Tenor.T15Y, usdLibor3MLibor6M, 0.00136875),
                SecurityWithMark.of("USBC20 Curncy", SecurityType.BAS, Tenor.T20Y, usdLibor3MLibor6M, 0.00141250),
                SecurityWithMark.of("USBC25 Curncy", SecurityType.BAS, Tenor.T25Y, usdLibor3MLibor6M, 0.00141250),
                SecurityWithMark.of("USBC30 Curncy", SecurityType.BAS, Tenor.T30Y, usdLibor3MLibor6M, 0.00141875),
        };
    }

    // original snapshot timestamp is ~2020-09-22T23:00:00
    public static SecurityWithMark[] getOriginalInstruments() {
        // conventions
        String usdFixedTermFedFundOisConvention = FixedOvernightSwapConventions.USD_FIXED_TERM_FED_FUND_OIS.getName();
        String usdFedFund1mCmeConvention = OvernightFutureContractSpecs.USD_FED_FUND_1M_CME.getName();
        String usdFixed1yFedFundOisConvention = FixedOvernightSwapConventions.USD_FIXED_1Y_FED_FUND_OIS.getName();
        String usdFedFundAaLibor3M = OvernightIborSwapConventions.USD_FED_FUND_AA_LIBOR_3M.getName();
        String usdFixed6MLibor3M = FixedIborSwapConventions.USD_FIXED_6M_LIBOR_3M.getName();
        String usdLibor3M = IborIndices.USD_LIBOR_3M.getName();
        String usdLibor3MImmCme = IborFutureContractSpecs.USD_LIBOR_3M_IMM_CME.getName();

        return new SecurityWithMark[]{
                // OIS swap rate
                SecurityWithMark.of("USSOA Curncy", SecurityType.OIS, Tenor.T1M, usdFixedTermFedFundOisConvention, .000830),

                // fed funds futures
                SecurityWithMark.of("FFX0 Comdty", SecurityType.ONF, "2020-11-18", usdFedFund1mCmeConvention, .999225),
                SecurityWithMark.of("FFZ0 Comdty", SecurityType.ONF, "2020-12-18", usdFedFund1mCmeConvention, .999275),
                SecurityWithMark.of("FFF1 Comdty", SecurityType.ONF, "2021-01-18", usdFedFund1mCmeConvention, .999325),
                SecurityWithMark.of("FFG1 Comdty", SecurityType.ONF, "2021-02-18", usdFedFund1mCmeConvention, .999425),
                SecurityWithMark.of("FFH1 Comdty", SecurityType.ONF, "2021-03-18", usdFedFund1mCmeConvention, .999475),
                SecurityWithMark.of("FFJ1 Comdty", SecurityType.ONF, "2021-04-18", usdFedFund1mCmeConvention, .999475),
                SecurityWithMark.of("FFK1 Comdty", SecurityType.ONF, "2021-05-18", usdFedFund1mCmeConvention, .999525),
                SecurityWithMark.of("FFM1 Comdty", SecurityType.ONF, "2021-06-18", usdFedFund1mCmeConvention, .999550),
                SecurityWithMark.of("FFN1 Comdty", SecurityType.ONF, "2021-07-18", usdFedFund1mCmeConvention, .999575),
                SecurityWithMark.of("FFQ1 Comdty", SecurityType.ONF, "2021-08-18", usdFedFund1mCmeConvention, .999625),
                SecurityWithMark.of("FFU1 Comdty", SecurityType.ONF, "2021-09-18", usdFedFund1mCmeConvention, .999650),
                SecurityWithMark.of("FFV1 Comdty", SecurityType.ONF, "2021-10-18", usdFedFund1mCmeConvention, .999675),

                // OIS swap rates
                SecurityWithMark.of("USSO1C Curncy", SecurityType.OIS, Tenor.T15M, usdFixed1yFedFundOisConvention, 0.00050),
                SecurityWithMark.of("USSO1F Curncy", SecurityType.OIS, Tenor.T18M, usdFixed1yFedFundOisConvention, 0.00044),

                // below commented out for now as it requires simultaneous curve building
                // TODO figure out simultaneous curve building
//                // fed funds basis swaps
//                Instrument.of("USBG2 Curncy", SecurityType.ONI, Tenor.T2Y, usdFedFundAaLibor3M, .0017875),
//                Instrument.of("USBG3 Curncy", SecurityType.ONI, Tenor.T3Y, usdFedFundAaLibor3M, .0018750),
//
//                // vanilla swaps
//                Instrument.of("USSWAP2 CMPN Curncy", SecurityType.IRS, Tenor.T2Y, usdFixed6MLibor3M, .002154),
//                Instrument.of("USSWAP3 CMPN Curncy", SecurityType.IRS, Tenor.T3Y, usdFixed6MLibor3M, .002253),
//
//                // index
//                Instrument.of("US0003M Index", SecurityType.FIX, Tenor.T3M, usdLibor3M, .0022513),
//
//                // futures
//                Instrument.of("EDZ0 Comdty", SecurityType.IFU, "2020-12-16", usdLibor3MImmCme, .997525),
//                Instrument.of("EDH1 Comdty", SecurityType.IFU, "2021-03-17", usdLibor3MImmCme, .997975),
//                Instrument.of("EDM1 Comdty", SecurityType.IFU, "2021-06-16", usdLibor3MImmCme, .998025),
//                Instrument.of("EDU1 Comdty", SecurityType.IFU, "2021-09-15", usdLibor3MImmCme, .998025),
//                Instrument.of("EDZ1 Comdty", SecurityType.IFU, "2021-12-15", usdLibor3MImmCme, .997775),
//                Instrument.of("EDH2 Comdty", SecurityType.IFU, "2021-03-16", usdLibor3MImmCme, .997775),
//
//                // swaps
//                Instrument.of("USSWAP2 BGN Curncy", SecurityType.IRS, Tenor.T2Y, usdFixed6MLibor3M, .002205),
//                Instrument.of("USSWAP3 BGN Curncy", SecurityType.IRS, Tenor.T3Y, usdFixed6MLibor3M, .002377),
//                Instrument.of("USSWAP4 BGN Curncy", SecurityType.IRS, Tenor.T4Y, usdFixed6MLibor3M, .002751),
//                Instrument.of("USSWAP5 BGN Curncy", SecurityType.IRS, Tenor.T5Y, usdFixed6MLibor3M, .003340),
//                Instrument.of("USSWAP6 BGN Curncy", SecurityType.IRS, Tenor.T6Y, usdFixed6MLibor3M, .004050),
//                Instrument.of("USSWAP7 BGN Curncy", SecurityType.IRS, Tenor.T7Y, usdFixed6MLibor3M, .004806),
//                Instrument.of("USSWAP8 BGN Curncy", SecurityType.IRS, Tenor.T8Y, usdFixed6MLibor3M, .005535),
//                Instrument.of("USSWAP9 BGN Curncy", SecurityType.IRS, Tenor.T9Y, usdFixed6MLibor3M, .006220),
//                Instrument.of("USSWAP10 BGN Curncy", SecurityType.IRS, Tenor.T10Y, usdFixed6MLibor3M, .006847),
//                Instrument.of("USSWAP12 BGN Curncy", SecurityType.IRS, Tenor.T12Y, usdFixed6MLibor3M, .007933),
//                Instrument.of("USSWAP15 BGN Curncy", SecurityType.IRS, Tenor.T15Y, usdFixed6MLibor3M, .009047),
//                Instrument.of("USSWAP20 BGN Curncy", SecurityType.IRS, Tenor.T20Y, usdFixed6MLibor3M, .010164),
//                Instrument.of("USSWAP25 BGN Curncy", SecurityType.IRS, Tenor.T25Y, usdFixed6MLibor3M, .010613),
//                Instrument.of("USSWAP30 BGN Curncy", SecurityType.IRS, Tenor.T30Y, usdFixed6MLibor3M, .010820),
        };
    }
}
