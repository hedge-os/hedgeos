package com.hedgeos.mock.curve;

import com.hedgeos.curve.Tenor;
import com.hedgeos.general.Currency;
import com.hedgeos.general.SecurityType;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.security.Security;

public class FakeSwapMockFactory {
    public static final SecurityKey FAKE_SWAP_KEY = SecurityKey.newBuilder().setSecurityId(1L).build();

    public static final Security FAKE_SWAP_SEC = Security.newBuilder()
            .setSecurityKey(FAKE_SWAP_KEY)
            .setCurrency(Currency.USD)
            .setSecurityType(SecurityType.IRS)
            .setTenor(Tenor.T5Y).build();

}
