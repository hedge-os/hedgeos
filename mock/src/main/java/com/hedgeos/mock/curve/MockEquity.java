package com.hedgeos.mock.curve;

class MockEquity {
    public final String ticker;
    public final String name;
    public final double price;


    MockEquity(String ticker, String name, double price) {
        this.ticker = ticker;
        this.name = name;
        this.price = price;
    }
}
