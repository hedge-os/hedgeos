package com.hedgeos.mock.curve;

import com.hedgeos.key.SecurityKey;
import com.hedgeos.security.Security;
import com.hedgeos.service.security.SecurityRequest;
import com.hedgeos.service.security.SecurityResponse;
import com.hedgeos.service.security.SecurityServiceGrpc;
import io.grpc.stub.StreamObserver;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.hedgeos.mock.curve.FakeSwapMockFactory.FAKE_SWAP_KEY;
import static com.hedgeos.mock.curve.FakeSwapMockFactory.FAKE_SWAP_SEC;

public class SecurityServiceMock extends SecurityServiceGrpc.SecurityServiceImplBase
{

    Map<SecurityKey, Security> keyToSecurityMap = new ConcurrentHashMap<>();

    public SecurityServiceMock() {
        addFakeSwap();
        addAllSecurities(UsCurveSecurityMock.getUsdLibor1MSecurities());
        addAllSecurities(UsCurveSecurityMock.getUsdLibor3MSecurities());
        addAllSecurities(UsCurveSecurityMock.getUsdLibor6MSecurities());
        addAllSecurities(UsCurveSecurityMock.getUsdOisSecurities());
        addAllSecurities(UsCurveSecurityMock.getUsdSofrSecurities());
    }

    private void addFakeSwap() {
        keyToSecurityMap.put(FAKE_SWAP_KEY, FAKE_SWAP_SEC);
    }

    private void addAllSecurities(TestData.SecurityWithMark[] securityWithMarks) {
        for (TestData.SecurityWithMark securityWithMark : securityWithMarks) {
            Security security = securityWithMark.getSecurity();
            keyToSecurityMap.put(security.getSecurityKey(), security);
        }
    }

    @Override
    public void getSecurity(SecurityRequest request, StreamObserver<SecurityResponse> responseObserver) {
        SecurityResponse.Builder respBuilder = SecurityResponse.newBuilder();
        for (SecurityKey securityKey : request.getSecurityKeyList()) {
            Security resp = keyToSecurityMap.get(securityKey);
            if (resp == null) {
                throw new RuntimeException("Why is mock security null for="+securityKey.getSecurityId());
            }
            respBuilder.addSecurities(resp);
        }

        responseObserver.onNext(respBuilder.build());
        responseObserver.onCompleted();
    }

    @Override
    public void securitySubscription(SecurityRequest request, StreamObserver<SecurityResponse> responseObserver) {
        throw new RuntimeException("Must mock up");
    }
}
