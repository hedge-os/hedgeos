package com.hedgeos.mock.curve;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DruidResultMock {

    public long snap;

    public final String portfolio;
    public final String book;
    public final String strategy;
    public final String businessLine;
    public final String bbTicker;
    public final String name;

    public final double quantity;
    public final double price;
    public final double marketValue;


    public double primaryPnl;
    public double positionPnl;
    public double tradingPnl;
}
