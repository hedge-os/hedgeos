package com.hedgeos.mock.curve;

import com.hedgeos.position.RiskReportRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RiskReportMockGenerator {

    static String[] ports = {"ALEX", "BRIAN", "CHRIS", "KOLJA", "MARCUS"};

    static String[] strats = {"STAT_ARB", "LONG_SHORT", "RISK_PAR", "TREND_F_CORE", "US_MID", "EMERGING",
            "INTERNATIONAL", "DISC_TRD", "MM", "CONV_ARB", "SYSTEM_C"};

    public static List<RiskReportRow.Builder> mockStockForPositions(List<MockEquity> stocks) {
        var results = new ArrayList<RiskReportRow.Builder>();

        int i=0;

        Random r = new Random(0);

        for (MockEquity stock : stocks) {
            i++;

            var amznBuilder = RiskReportRow.newBuilder();

            amznBuilder.setSecCurrency("USD");
            amznBuilder.setBbTicker(stock.ticker);
            amznBuilder.setSecName(stock.name);
            amznBuilder.setBusinessLine("EQUITY");

            amznBuilder.setStrategy("Speculation");

            amznBuilder.setStrategy(strats[i % strats.length]);

            amznBuilder.setSecCountry("United States");


            double move = r.nextDouble() / 100.0;

            amznBuilder.setPortfolio(ports[i % ports.length]);

            amznBuilder.setQuantity(100L * i);
            amznBuilder.setSecurityOpenPrice(stock.price);
            amznBuilder.setSecurityPrice(stock.price);
            amznBuilder.setSecurityId(i);
            amznBuilder.setDomicileDailyPnl(move*stock.price);
            amznBuilder.setDomicileItdPnl(move*stock.price);
            amznBuilder.setDomicileYearlyPnl(move*stock.price);
            amznBuilder.setDomicileMonthlyPnl(move*stock.price);
            amznBuilder.setDomicileMarketValue(amznBuilder.getQuantity() * amznBuilder.getSecurityPrice());

            amznBuilder.setLocalDailyPnl(move*stock.price);

            results.add(amznBuilder);
        }

        return results;
    }



    public static List<RiskReportRow.Builder> mockStockRiskReport() {
        var results = new ArrayList<RiskReportRow.Builder>();

        var amznBuilder = RiskReportRow.newBuilder();
        amznBuilder.setSecCurrency("USD");
        amznBuilder.setBbTicker("AMZN US Equity");
        amznBuilder.setSecName("Amazon Inc.");
        amznBuilder.setBusinessLine("EQUITY");
        amznBuilder.setStrategy("Speculation");
        amznBuilder.setPortfolio("First Portfolio");
        amznBuilder.setSecCountry("United States");

        amznBuilder.setQuantity(100);
        amznBuilder.setSecurityOpenPrice(3150);
        amznBuilder.setSecurityPrice(3202.02);
        amznBuilder.setSecurityId(33);
        amznBuilder.setDomicileDailyPnl(50*100.01);
        amznBuilder.setDomicileItdPnl(50*100.01);
        amznBuilder.setDomicileYearlyPnl(50*100.01);
        amznBuilder.setDomicileMonthlyPnl(50*100.01);
        amznBuilder.setDomicileMarketValue(3202.02*1000);

        amznBuilder.setLocalDailyPnl(50*100);
        results.add(amznBuilder);

        var msftBuilder = RiskReportRow.newBuilder();
        msftBuilder.setSecCurrency("USD");
        amznBuilder.setSecCountry("United States");

        msftBuilder.setBbTicker("MSFT US Equity");
        msftBuilder.setSecName("Microsoft Inc.");
        msftBuilder.setBusinessLine("EQUITY");
        msftBuilder.setStrategy("Value");
        msftBuilder.setPortfolio("First Portfolio");
        msftBuilder.setSecCurrency("USD");
        msftBuilder.setQuantity(50);
        msftBuilder.setSecurityOpenPrice(214.60);
        msftBuilder.setSecurityPrice(215.20);
        msftBuilder.setDomicileMarketValue(215.20*50);
        msftBuilder.setSecurityId(34);
        msftBuilder.setDomicileDailyPnl(50*.80);
        msftBuilder.setDomicileMonthlyPnl(50*.80);
        msftBuilder.setDomicileYearlyPnl(50*.80);
        msftBuilder.setDomicileItdPnl(50*.80);

        msftBuilder.setLocalDailyPnl(50*.80);

        results.add(msftBuilder);

        var tslaBuilder = RiskReportRow.newBuilder();
        tslaBuilder.setSecCurrency("USD");
        amznBuilder.setSecCountry("United States");

        tslaBuilder.setBbTicker("TSLA US Equity");
        tslaBuilder.setStrategy("Growth");
        tslaBuilder.setSecName("Tesla Inc.");
        tslaBuilder.setBusinessLine("EQUITY");
        tslaBuilder.setPortfolio("Second Portfolio");
        tslaBuilder.setQuantity(300);
        tslaBuilder.setDomicileMarketValue(300*610.12);
        tslaBuilder.setSecurityPrice(610.12);
        tslaBuilder.setSecurityOpenPrice(601.22);
        tslaBuilder.setDomicileDailyPnl(9.10*300);
        tslaBuilder.setLocalDailyPnl(9.10*300);

        results.add(tslaBuilder);

        return results;
    }
}
