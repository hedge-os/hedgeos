package com.hedgeos.mock.curve;

import com.hedgeos.api.CurveName;
import com.hedgeos.curve.Index;
import com.hedgeos.general.Currency;
import com.hedgeos.refdata.CurrencyToCurveEntry;
import com.hedgeos.refdata.IndexToCurveEntry;
import com.hedgeos.refdata.service.*;
import io.grpc.stub.StreamObserver;

import static com.hedgeos.api.CurveName.*;

public class RefDataServiceMock extends RefDataServiceGrpc.RefDataServiceImplBase {
    @Override
    public void fetchCurveDefs(CurveDefRequest request, StreamObserver<CurveDefResponse> responseObserver) {
        throw new RuntimeException("make me");
    }

    @Override
    public void getCurrencyToCurveMap(CurrencyToCurveRequest request, StreamObserver<CurrencyToCurveResponse> responseObserver) {
        CurrencyToCurveResponse.Builder builder =
                CurrencyToCurveResponse.newBuilder();

        CurrencyToCurveEntry usdEntry =
                CurrencyToCurveEntry.newBuilder()
                        .setCurrency(Currency.USD)
                        .setCurveName(USD_OIS).build();

        builder.addCurrencyToCurveEntries(usdEntry);
        responseObserver.onNext(builder.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getIndexToCurveMap(IndexToCurveRequest request, StreamObserver<IndexToCurveResponse> responseObserver) {
        IndexToCurveResponse.Builder builder =
                IndexToCurveResponse.newBuilder();

        // TODO:  Check these
        builder.addIndexToCurveEntries(IndexToCurveEntry.newBuilder()
                .setIndex(Index.USD_LIBOR)
                .setCurveName(USD_FED_FUNDS)
                .build());

        builder.addIndexToCurveEntries(IndexToCurveEntry.newBuilder()
                .setIndex(Index.USD_LIBOR_3M)
                .setCurveName(USD_LIBOR_3M)
                .build());

        builder.addIndexToCurveEntries(IndexToCurveEntry.newBuilder()
                .setIndex(Index.USD_LIBOR_1M)
                .setCurveName(USD_LIBOR_1M)
                .build());

        builder.addIndexToCurveEntries(IndexToCurveEntry.newBuilder()
                .setIndex(Index.USD_LIBOR_6M)
                .setCurveName(USD_LIBOR_6M)
                .build());

        responseObserver.onNext(builder.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getSecurityToCurveGroupOverrideMap(CurveGroupOverrideRequest request,
                       StreamObserver<CurveGroupOverrideResponse> responseObserver) {
        // TODO:  Test an override
        responseObserver.onNext(CurveGroupOverrideResponse.getDefaultInstance());
        responseObserver.onCompleted();
    }
}
