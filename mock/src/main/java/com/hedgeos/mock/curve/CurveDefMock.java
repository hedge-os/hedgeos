package com.hedgeos.mock.curve;

import com.hedgeos.curve.CurveDef;
import com.hedgeos.curve.Index;
import com.hedgeos.curve.InterpolationDef;
import com.hedgeos.daycount.DayCount;
import com.hedgeos.string.EnumUtil;
import org.apache.commons.math3.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.hedgeos.mock.curve.CurveGroupMock.usdIndexes;
import static com.hedgeos.mock.curve.TestData.*;
import static com.hedgeos.mock.curve.TestData.getUsdLibor6MReferenceCurve;

public class CurveDefMock {
    public static List<CurveDef> makeUsdCurves() {
        CurveDef.Builder curveDefBuilder = CurveDef.newBuilder()
                .setDayCount(DayCount.ACT_365F)
                .setInterpolationDef(InterpolationDef.newBuilder().addInterpolationName("Linear").addStartPoint(0));

        // prepare curve definitions
        List<CurveDef> curveDefs = new ArrayList<>(usdIndexes.size());

        // start defining the curve pillars and reference data for this request
        Map<Index, Pair<TestData.SecurityWithMark[], TestData.RawCurveReference[]>> fakeCurves = new HashMap<>();
        fakeCurves.put(Index.USD_FED_FUNDS, new Pair<>(UsCurveSecurityMock.getUsdOisSecurities(), getUsdOisReferenceCurve()));
        //  curves.put(Index.USD_SOFR, new Pair<>(getUsdSofrSecurities(), getUsdSofrReferenceCurve()));
        fakeCurves.put(Index.USD_LIBOR_1M, new Pair<>(UsCurveSecurityMock.getUsdLibor1MSecurities(), getUsdLibor1MReferenceCurve()));
        fakeCurves.put(Index.USD_LIBOR_3M, new Pair<>(UsCurveSecurityMock.getUsdLibor3MSecurities(), getUsdLibor3MReferenceCurve()));
        fakeCurves.put(Index.USD_LIBOR_6M, new Pair<>(UsCurveSecurityMock.getUsdLibor6MSecurities(), getUsdLibor6MReferenceCurve()));

        for (Index index : fakeCurves.keySet()) {

            Pair<TestData.SecurityWithMark[], TestData.RawCurveReference[]> curve = fakeCurves.get(index);

            SecurityWithMark[] pillars = curve.getFirst();

            // build the curve def and add it to the list
            curveDefBuilder.setName(EnumUtil.indexToString(index));

            curveDefBuilder.addAllSecurities(
                    Stream.of(pillars).map(TestData.SecurityWithMark::getSecurity).collect(Collectors.toList())
            );
            curveDefs.add(curveDefBuilder.build());
        }

        return curveDefs;
    }
}
