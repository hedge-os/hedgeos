package com.hedgeos.mock.curve;

import com.hedgeos.api.ServiceLocatorInterface;
import com.hedgeos.refdata.service.RefDataServiceGrpc;
import com.hedgeos.service.curve.CurveServiceGrpc;
import com.hedgeos.service.marketdata.MarketDataServiceGrpc;
import com.hedgeos.service.security.SecurityServiceGrpc;
import io.grpc.Deadline;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.testing.GrpcCleanupRule;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class MockServiceLocator implements ServiceLocatorInterface {

//    @Rule
//    public GrpcCleanupRule grpcCleanup = new GrpcCleanupRule();

    public final GrpcCleanupRule grpcCleanup = new GrpcCleanupRule();

    // we laugh at Deadlines
    public final static Deadline MOCK_DEADLINE =
            Deadline.after(10, TimeUnit.SECONDS);

    // TODO:  Review in process service location from gRPC
    private String serverName = InProcessServerBuilder.generateName();

    // TODO:  Add servers here, Curve Mock, etc

    private InProcessServerBuilder serverBuilder =
            InProcessServerBuilder.forName(serverName)
                    .directExecutor()
                    .addService(new SecurityServiceMock())
                    .addService(new RefDataServiceMock());

    private InProcessChannelBuilder channelBuilder =
            InProcessChannelBuilder.forName(serverName)
                    .directExecutor();

    // SecurityServiceGrpc.SecurityServiceImplBase ba

    public MockServiceLocator() {
        // see we add the mock to the server builder

        try {
            grpcCleanup.register(serverBuilder.build().start());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SecurityServiceGrpc.SecurityServiceBlockingStub securityServiceBlockingStub() {
        return SecurityServiceGrpc.newBlockingStub(channelBuilder.build())
                .withDeadline(MOCK_DEADLINE);
    }

    @Override
    public RefDataServiceGrpc.RefDataServiceBlockingStub refDataBlockingStub() {
        return RefDataServiceGrpc.newBlockingStub(channelBuilder.build())
                .withDeadline(MOCK_DEADLINE);
    }

    @Override
    public CurveServiceGrpc.CurveServiceBlockingStub curveServiceBlockingStub() {
        return CurveServiceGrpc.newBlockingStub(channelBuilder.build())
                .withDeadline(MOCK_DEADLINE);
    }

    @Override
    public MarketDataServiceGrpc.MarketDataServiceBlockingStub marketDataBlockingStub() {
        return MarketDataServiceGrpc.newBlockingStub(channelBuilder.build())
                .withDeadline(MOCK_DEADLINE);
    }
}
