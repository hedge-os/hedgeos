package com.hedgeos.mock.curve;

import com.hedgeos.general.MarketData;
import com.hedgeos.general.MarketDataRequest;
import com.hedgeos.general.MarketDataResponse;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.service.marketdata.MarketDataServiceGrpc;
import io.grpc.stub.StreamObserver;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class MarketDataMock extends MarketDataServiceGrpc.MarketDataServiceImplBase {

    Map<SecurityKey, MarketData> marketDataMock = new HashMap<>();

    public MarketDataMock() {
        Instant instant = Instant.now();

        addMarketDataArray(instant, UsCurveSecurityMock.getUsdLibor1MSecurities());
        addMarketDataArray(instant, UsCurveSecurityMock.getUsdLibor6MSecurities());
        addMarketDataArray(instant, UsCurveSecurityMock.getUsdLibor3MSecurities());
        addMarketDataArray(instant, UsCurveSecurityMock.getUsdOisSecurities());
        addMarketDataArray(instant, UsCurveSecurityMock.getUsdSofrSecurities());
    }

    private void addMarketDataArray(Instant instant, TestData.SecurityWithMark[] thearray) {
        for (TestData.SecurityWithMark swm : thearray) {
            MarketData md = swm.getMarketData(instant);
            marketDataMock.put(md.getSecurityKey(), md);
        }
    }

    @Override
    public void getMarketData(MarketDataRequest request, StreamObserver<MarketDataResponse> responseObserver) {
        // TODO:  Should probably stream market data back, not a response in bulk..

        MarketDataResponse.Builder response = MarketDataResponse.newBuilder();
        for (SecurityKey securityKey : request.getSecurityKeysList()) {
            MarketData mocked = marketDataMock.get(securityKey);
            if (mocked == null) {
                System.err.println("Missing Mock MD=" + securityKey);
                continue; // TOD:  Can invent data
            }
            response.addMarketDatas(mocked);
        }
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();

    }
}
