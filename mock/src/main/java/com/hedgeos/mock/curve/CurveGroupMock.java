package com.hedgeos.mock.curve;

import com.google.common.collect.Lists;
import com.google.protobuf.Timestamp;
import com.hedgeos.curve.CurrencyToDiscountCurve;
import com.hedgeos.curve.CurveGroup;
import com.hedgeos.curve.Index;
import com.hedgeos.curve.IndexToForwardCurve;
import com.hedgeos.general.Currency;
import com.hedgeos.string.EnumUtil;

import java.time.Instant;
import java.util.List;

public class CurveGroupMock {

    public static List<Index> usdIndexes =
            Lists.newArrayList(Index.USD_FED_FUNDS, Index.USD_LIBOR_1M, Index.USD_LIBOR_3M, Index.USD_LIBOR_6M);

    /**
     * TODO:  Likely from a database table of Curve Groups (Ref Data)
     */
    public static CurveGroup makeUsdCurveGroup() {
        // set reference time as now
        Instant time = Instant.now();

        // define discount curve index
        Index discountIndex = Index.USD_FED_FUNDS;


        // start defining the curve group for this request and set desired discount curve
        CurveGroup.Builder curveGroupBuilder = CurveGroup.newBuilder()
                .setGroupName("USD-OIS-TEST")
                .setAsOfTime(Timestamp.newBuilder().setSeconds(time.getEpochSecond()).setNanos(time.getNano()))
                .setCurrencyToDiscountCurve(CurrencyToDiscountCurve.newBuilder()
                        .setCurrency(Currency.USD)
                        .setCurveName(EnumUtil.indexToString(discountIndex)));

        for (Index index : usdIndexes) {
            // include each into the curve group
            curveGroupBuilder.addIndexToForwardCurve(IndexToForwardCurve.newBuilder()
                    .setIndex(index)
                    .setCurveName(EnumUtil.indexToString(index)));
        }

        return curveGroupBuilder.build();
    }
}
