import React, {useEffect, useState} from 'react';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';

import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine-dark.css';


const App = () => {
  const [gridApi, setGridApi] = useState(null);
  const [gridColumnApi, setGridColumnApi] = useState(null);

  const [curveData, setCurveData] = useState([]);

  const [count, setCounter] = useState(0);

  const [curve, setCurve] = useState([]);

  const [curveData2, setCurveData2] = useState([]);

  const [points, setPoints] = useState(curveData);

  const [time, setTime] = useState([]);

  function setTimeMs(n) {
    var d = new Date();
    var took = d.getTime()-n;
    setTime(took)
  }

  function activateLasers() {

      var d = new Date();
      var n = d.getTime();
      fetch('http://localhost:8085/go/'+count)
          .then(result => result.json())
          .then(a => setCurveData(a))
          .then(b => setTimeMs(n))
  }

  function activateLasers2(e) {
    console.log(e);
    var d = new Date();
    var n = d.getTime();
    fetch('http://localhost:8085/go/1')
        .then(result => result.json())
        .then(b => setCurveData2(b))
        .then(b => setTimeMs(n))
  }

  const handleInput = event => {
    console.log(event.target.value);
    setCounter(event.target.value);
  }

  const sayHello = (e) => {
    // alert('hello');
    console.log("Hello")
    console.log(e)
    // setPoints(e.points)
    setCurveData2(e.data)
  }

  var gridOptions = {
    columnDefs: [
      { field: 'curveName' },
      { field: 'points'},
      { field: 'discountFactor'},
      { field: 'fractionalYear'}
    ],
    autoGroupColumnDef: {
      headerName: "Curve Name",
      width: 300,
      cellRendererParams: {
        suppressCount: true
      }
    },
    defaultColDef: {
      flex: 1,
      minWidth: 100,
    },
    // treeData: true,
    rowSelection: 'single',
    onSelectionChanged: onSelectionChanged,
    getDataPath: function (data) {
      console.log(data);
      return data.points;
    },
  };

  var gridOptions2 = {
    columnDefs: [
      { field: 'discountFactor'},
      { field: 'fractionalYear'}
    ],
    defaultColDef: {
      flex: 1,
      minWidth: 100,
    },
    rowSelection: 'single',
  };


  function onSelectionChanged () {
    console.log("changed");
    var selectedRows = gridOptions.api.getSelectedRows();
    // document.querySelector('#selectedRows').innerHTML =
    //     selectedRows.length === 1 ? selectedRows[0].curveName : '';
    console.log(selectedRows[0])
    setPoints(selectedRows[0].points)
    //activateLasers2(selectedRows[0])
  }

  const setCount = () => {

  }

  return (
      <div>
        <table>
          <tr>

            <td>

        <button onClick={activateLasers}>
          Generate Curves
        </button>

        <input onChange={handleInput} placeholder="Enter # curves" width={10}/>
<br/>
        <label>Took(ms): {time}</label>
<br/>
        <div className="ag-theme-alpine-dark" style={ { height: 600, width: 400 } }>

          <AgGridReact gridOptions={gridOptions}
              rowData={curveData.curve} onCellClicked={sayHello}>
            <AgGridColumn field="curveName"></AgGridColumn>
            <AgGridColumn field="millisToBuild"></AgGridColumn>
          </AgGridReact>
        </div>


          </td>
            <td>

              <div gridOptions={gridOptions2}
                   className="ag-theme-alpine-dark" style={ { height: 300, width: 400 } }>
                <AgGridReact rowData={curveData2.points}>
                  <AgGridColumn field="discountFactor"></AgGridColumn>
                  <AgGridColumn field="fractionalYear"></AgGridColumn>
                </AgGridReact>

              </div>

            </td>

          </tr>
        </table>
      </div>
  );
};

export default App;