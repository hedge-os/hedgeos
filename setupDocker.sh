# On windows, download and install docker, restart Windows
# Need to turn on HyperV "windows feature", and enable HyperV in your BIOS
# Ubuntu steps for adding docker
# Fedora is similar
sudo apt install docker.io
sudo usermod -a -G docker $USER
# now switch shells to reload the group list for the user
su - $USER
docker ps
docker run hello-world
# now docker compose
sudo apt install docker-compose

# TODO:  Canonical Mini Kubernetes (instead of, compose)
