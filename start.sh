# This is the real start file

# Need the databases and the network is on the databases
# TODO: maybe one called infra and add Kafka here
nohup docker-compose -f db.yml up > /dev/null 2>&1 &

echo "sleep 30 for dbs start"
sleep 30

# Kafka needs to come up
nohup docker-compose -f kafka.yml up > /dev/null 2>&1 &

sleep 10
echo "sleep 10 for kafka start"

# Get our friend druid started
nohup docker-compose -f druid/druid.yml up > /dev/null 2>&1 &

echo "sleep 60 for druid start"

sleep 60 
# Copy the Druid Risk Report (Portfolio Viewer) Protobuf "spec" into Druid for ingestion
./risk_copy.sh

# Now the java services and the websites
nohup docker-compose up > /dev/null 2>&1 &

echo "sleep 20 for java start"

sleep 20
echo started hedgeOS stack


