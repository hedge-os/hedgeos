package com.hedgeos.lib.permission.db.application;

import com.hedgeos.lib.permission.db.user.UserRowMapper;
import com.hedgeos.permission.Application;
import com.hedgeos.permission.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Component
public class ApplicationDao {

    // these are auto-injected by Lombok, RequiredArgsConstructor
    // after being constructed by Spring
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;

    public List<Application> getActiveApplication(@NotNull final Date asOfDate) {

        String sql = "select * from application " +
                " where (end_date is null or end_date > :asOfDate)";

        SqlParameterSource namedParameters =
                new MapSqlParameterSource()
                        .addValue("asOfDate", asOfDate);

        List<Application> appList = jdbcTemplate.query(sql, namedParameters, new ApplicationRowMapper());

        return appList;
    }
}
