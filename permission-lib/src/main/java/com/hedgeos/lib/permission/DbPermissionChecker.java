package com.hedgeos.lib.permission;

import com.hedgeos.permission.PermissionRequest;
import com.hedgeos.permission.PermissionResponse;
import org.springframework.util.Assert;

public class DbPermissionChecker implements  PermissionChecker {

    @Override
    public PermissionResponse checkPermission(PermissionRequest request) {

        Assert.notNull(request, "Request was null");
        Assert.notNull(request.getUser(), "User must not be null");
        Assert.notNull(request.getApp(), "App must not be null");



        return null;
    }
}
