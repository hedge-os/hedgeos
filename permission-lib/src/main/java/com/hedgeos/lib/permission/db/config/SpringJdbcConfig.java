package com.hedgeos.lib.permission.db.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
@ComponentScan("com.hedgeos.lib.permission.db")

// loads from resources/permission.properties
@PropertySource("classpath:permission.properties")
public class SpringJdbcConfig {

    @Value("${db.driverClassName:org.postgresql.Driver}")
    private String dbDriverClass;
    @Value("${db.url:jdbc:postgresql://permissions-db:5433/permission-os}")
    private String dbUrl;
    @Value("${db.username:hedgeos}")
    private String dbUser;
    @Value("${db.password:hedgeos-changeme123}")
    private String dbPwd;

    //register PropertySourcesPlaceholderConfigurer
    //in order to resolve ${...} placeholders
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public NamedParameterJdbcTemplate named() {
        return new NamedParameterJdbcTemplate(jdbcTemplate());
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(permissionsDataSource());
    }

    @Bean
    public DataSource permissionsDataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(dbUrl);
        config.setUsername(dbUser);
        config.setPassword(dbPwd);
        config.setDriverClassName(dbDriverClass);
        // config.addDataSourceProperty("cachePrepStmts", "true");
        // config.addDataSourceProperty("prepStmtCacheSize", "250");
        // config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        return new HikariDataSource(config);
    }
}
