package com.hedgeos.lib.permission.db.application;

import com.hedgeos.permission.Application;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ApplicationRowMapper implements RowMapper<Application> {

    @Override
    public Application mapRow(ResultSet rs, int i) throws SQLException {
        Application.Builder ab = Application.newBuilder();
        ab.setAppId(rs.getInt("app_id"));
        ab.setName(rs.getString("name"));
        return ab.build();
    }
}
