package com.hedgeos.lib.permission.db.user;

import com.hedgeos.date.ProtoConverter;
import com.hedgeos.permission.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User.Builder ub = User.newBuilder();

        ub.setUserId(rs.getInt("user_id"));
        ub.setUsername(rs.getString("username"));
        ub.setFirstName(rs.getString("first_name"));
        ub.setLastName(rs.getString("last_name"));

        Date startDate = rs.getDate("start_date");
        if (startDate != null)
            ub.setStartDate(ProtoConverter.toGoogleDate(startDate));

        Date endDate = rs.getDate("end_date");
        if (endDate != null)
            ub.setEndDate(ProtoConverter.toGoogleDate(endDate));

        // TODO:  Add groups to the user
        // TODO:  Add groups of groups, and make sure they are added
        // TODO:  Work with Chris

        return ub.build();
    }
}
