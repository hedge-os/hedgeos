package com.hedgeos.lib.permission.db.user;

import com.hedgeos.date.ProtoConverter;
import com.hedgeos.permission.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * https://www.baeldung.com/spring-jdbc-jdbctemplate
 *
 * JPA and JOOQ are neat, but the complexity cost is very high.
 * And, if you run into problems (when you do), you lose more than you gained.
 * Spring JDBC, offers a small library, with non-intrusive, abstractions.
 *
 * It is true, you need about 50 LOC, but they are easy, code is then obvious.
 */
@RequiredArgsConstructor
@Slf4j
@Component
public class UserDao {

    // these are auto-injected by Lombok, RequiredArgsConstructor
    // after being constructed by Spring
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;

    // every active user as of a date
    public List<User> getActiveUsers(@NotNull final Date asOfDate) {

        String sql = "select * from users " +
                        " where (end_date is null or end_date > :asOfDate)";

        SqlParameterSource namedParameters =
                new MapSqlParameterSource()
                        .addValue("asOfDate", asOfDate);

        List<User> userList = jdbcTemplate.query(sql, namedParameters, new UserRowMapper());

        return userList;
    }

    /** This gets us data simply,
     * one at a time,
     * ok for thousands of users. */
    public User getUser(@NotNull String username) {
        // log.info("Ds="+ jdbcTemplate.toString());

        String sql =
                "select * from users " +
                "where username = :username ";

        SqlParameterSource namedParameters =
                new MapSqlParameterSource()
                        .addValue("username", username);

        // TODO:  Good to query for a user, always, in case turned off
        User user = jdbcTemplate.queryForObject(sql, namedParameters, new UserRowMapper());

        return user;
    }

    // insert and get the user_id
    public int insertUser(@NotNull final User user) {
        SimpleJdbcInsert simpleJdbcInsert =
                new SimpleJdbcInsert(dataSource).withTableName("users")
                        .usingGeneratedKeyColumns("user_id");

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("username", user.getUsername());
        parameters.put("last_name", user.getLastName());
        parameters.put("first_name", user.getFirstName());
        parameters.put("start_date", user.getStartDate());

        Number id = simpleJdbcInsert.executeAndReturnKey(parameters);

        return id.intValue();
    }

    public final static String UPDATE_USER_SQL =

            "update users " +
            "set username = :username "+
                    ", first_name = :first_name "+
                    ", last_name = :last_name "+
                    ", start_date = :start_date "+
                    ", end_date = :end_date "+
            " where user_id = :user_id ";

    /**
     * Typically for end_dating a user,
     *  but you could update their name, username
     * @param user public the user to be updated
     */
    public void updateUser(@NotNull final User user) {

        Map<String, Object> paramMap = new HashMap<>();

        paramMap.put("user_id", user.getUserId());
        paramMap.put("username", user.getUsername());
        paramMap.put("first_name", user.getFirstName());
        paramMap.put("last_name", user.getLastName());

        if (user.getStartDate() != com.google.type.Date.getDefaultInstance())
            paramMap.put("start_date", ProtoConverter.toDate(user.getStartDate()));
        else
            paramMap.put("start_date", null);

        if (user.getEndDate() != com.google.type.Date.getDefaultInstance())
            paramMap.put("end_date", ProtoConverter.toDate(user.getEndDate()));
        else
            paramMap.put("end_date", null);

        int count = jdbcTemplate.update(UPDATE_USER_SQL, paramMap);

        if (count != 1) {
            throw new RuntimeException("Can't update, no user with id="+user.getUserId());
        }
    }

}
