package com.hedgeos.lib.permission;

import com.hedgeos.permission.PermissionRequest;
import com.hedgeos.permission.PermissionResponse;

public interface PermissionChecker
{
    /**
     * Used as a library, not a service.
     *
     * This way every tier can check the DB,
     * Without a hop.
     *
     * Implementation is typically
     *  "CachedDbPermissionsChecker"
     *
     * but this interface, leaves it open to interpretation.
     *
     * This checks your request against what is known
     *  in the permissions database.
     *
     *  The database permissions are setup by
     *  the permissions and admin front end.
     *
     * Send in a PermissionRequest.
     *
     * Typically from the Kerberos security,
     *  but may be used via other methods (basic auth etc.)
     *
     * And receive a PermissionResponse
     *
     * @param request PermissionRequest by User, App,
     *                DataType, Action, Feature
     * @return PermissionResponse success or failure code, message
     **/
    PermissionResponse checkPermission(PermissionRequest request);

}
