package com.hedgeos.lib.permission.db;

import com.hedgeos.lib.permission.db.application.ApplicationDao;
import com.hedgeos.lib.permission.db.config.SpringJdbcConfig;
import com.hedgeos.permission.Application;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringJdbcConfig.class})
@Slf4j
public class ApplicationDaoTest {

    @Autowired
    public ApplicationDao applicationDao;

    @Ignore
    @Test
    public void testApps() {
        List <Application> apps = applicationDao.getActiveApplication(new Date());
        Assert.notNull(apps, "no applications hedgeos-admin in db");
        Assert.isTrue(apps.size()  >= 7, "not enough applications ="+apps.size());
        for (Application app : apps) {
            log.warn("App="+app.getName());
        }
    }
}
