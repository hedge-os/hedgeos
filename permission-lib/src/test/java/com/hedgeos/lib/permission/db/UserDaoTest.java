package com.hedgeos.lib.permission.db;

import com.hedgeos.lib.permission.db.config.SpringJdbcConfig;
import com.hedgeos.lib.permission.db.user.UserDao;
import com.hedgeos.permission.User;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringJdbcConfig.class})
public class UserDaoTest {

    @Autowired
    public UserDao userDao;

    @Ignore
    @Test
    // @Ignore
    public void testPermissionDao() {
        User user = userDao.getUser("hedgeos-admin");
        Assert.assertNotNull("no user hedgeos-admin in db", user);
        Assert.assertEquals("hegeoos-admin user should exist", user.getUsername(), "hedgeos-admin");
        User updateLast = User.newBuilder(user).setLastName("Admin Last").build();
        userDao.updateUser(updateLast);
        User again = userDao.getUser("hedgeos-admin");
        Assert.assertEquals("Update failed", "Admin Last", again.getLastName());
    }

    @Ignore
    @Test
    public void testFetchUsers() {
        List<User> users = userDao.getActiveUsers(new Date());
        Assert.assertNotNull("no user hedgeos-admin in db", users);
        org.junit.Assert.assertEquals( "wrong number of usres", 3, users.size());
    }

}
