# start up the per service databases
docker-compose up ref-data-postgres
docker-compose up securities-postgres
docker-compose up positions-postgres

# start up the services
docker-compose up ref-data-service