package com.hedgeos.service.position;

import com.google.gson.Gson;
import com.hedgeos.classpath.CpUtil;
import com.hedgeos.general.General;
import com.hedgeos.ui.api.aggrid.ServerSideGetRowsRequest;
import org.junit.Test;

public class TradeBlotterControllerTest {

    @Test
    public void testAgJson() {
        String agJson = CpUtil.loadFromClasspath("fakeTradeBlotter.json");
        System.out.println(agJson);
        ServerSideGetRowsRequest ssr = new Gson().fromJson(agJson, ServerSideGetRowsRequest.class);
        System.out.println("ssr="+ssr);
    }
}
