package com.hedgeos.service.position;

import com.hedgeio.trade.Order;
import com.hedgeos.position.PositionMeta;
import com.hedgeos.service.position.cache.MetaStringCache;
import com.hedgeos.service.position.PositionMetaResolved;
import com.hedgeos.ui.api.hedgeos.UiOrder;
import org.springframework.stereotype.Component;

@Component
public class MetaStringMockCache implements MetaStringCache {

    volatile int i;

    public PositionMetaResolved getPositionMetaStrings(PositionMeta positionMeta) {

        PositionMetaResolved pmr = new PositionMetaResolved();

        // entityManager.find(Position, positionMeta.getPositionId());
        pmr.setPortoflio("TEST-PORT");
        pmr.setStrategy("test-strategy");
        pmr.setBbTicker("TSLA");

        i = i + 1;

        if (i % 2 ==0) {
            pmr.setPortoflio("PORT-TEST");
            pmr.setBbTicker("AAPL");
            pmr.setStrategy("All Tides");
        }

        if (i % 5 == 0) {
            pmr.setPortoflio("CHRIS");
            pmr.setStrategy("Can't lose");
        }

        return pmr;
    }

    @Override
    public void setOrderMetaStrings(UiOrder uiOrder, Order order) {

    }
}
