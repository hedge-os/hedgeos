package com.hedgeos.service.position;

import com.hedgeio.trade.Order;
import com.hedgeio.trade.OrderPlacedRequest;
import com.hedgeos.service.position.sort.HedgeOsOrderIdComparator;
import com.hedgeos.thread.Sleep;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.mockito.MockitoAnnotations;

import javax.swing.*;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;

import static org.mockito.Mockito.mock;

public class OrderCacheTest {

    // takes three gigs as it stands.
    // With sorting, will be about seven GB which is also fine.
    public static final int TWO_MILLION_ORDERS = 2_000_000;
    public static final int HUNDRED_THOU = 100_000;
    public static final int TEN_THOU = 10_000;

    // this works as well, about 6 GB of RAM not too bad
    public static final int FIVE_MILLION_ORDERS = 5_000_000;

    public OrderCacheTest() {
    }

    private OrderCache makeOrderCache(int size) {
        long start = System.currentTimeMillis();

        MockitoAnnotations.initMocks(this);

        MetaStringMockCache mockCache = new MetaStringMockCache();

        OrderCache newOrderCache = new OrderCache(mockCache);

        List<Order.Builder> mockOrders = MockOrderGenerator.generateOrders(size);

        for (Order.Builder mockOrder : mockOrders) {
            OrderPlacedRequest opr =
                    OrderPlacedRequest.newBuilder().addOrders(mockOrder).build();
            newOrderCache.addOrUpdateOrder(opr);
        }

        // need to wait for the cache to clear as it is in the background
        while (newOrderCache.ordersToAdd.size() > 0) {
            Sleep.sleepSec(1);
        }

        System.out.println("Loaded orders, " +
                ", two mil count="+ newOrderCache.portfolioSortedOrderMap.size()+
                ", took(ms)="+(System.currentTimeMillis()-start));

        return newOrderCache;
    }

    @Test
    @DisplayName("Should show we can do basic lookups on Two Million")
    public void testOrderCacheTwoMillion() {
        testOrderCache(makeOrderCache(TWO_MILLION_ORDERS));
        System.gc();System.gc();
    }

    @Test
    @DisplayName("Should show we can do basic lookups on One Hundred K")
    public void testOrderCacheOneHundredThousand() {
        testOrderCache(makeOrderCache(HUNDRED_THOU));
        System.gc();System.gc();
    }

    @Test
    @DisplayName("Should show we can do basic lookups on One Hundred K")
    public void testOrderCacheTenThousand() {
        testOrderCache(makeOrderCache(TEN_THOU));
        System.gc();System.gc();
    }


    @Test
    @DisplayName("Should show we can do basic lookups on Five Million")
    public void testOrderCacheFiveMillion() {
        testOrderCache(makeOrderCache(FIVE_MILLION_ORDERS));
    }

    public void testOrderCache(OrderCache orderCache) {
        System.gc(); System.gc();

        long start = System.currentTimeMillis();
        Map<String, String> columnToPrefiMap = new HashMap<>();
        columnToPrefiMap.put(ColumnNames.PORTOFLIO_COL, "PORT");
        List<SortCol> sortCols = new ArrayList<>();

        SortCol sortCol = new SortCol(ColumnNames.PORTOFLIO_COL, SortOrder.ASCENDING);
        sortCols.add(sortCol);

        long used = Runtime.getRuntime().totalMemory();
        long free = Runtime.getRuntime().freeMemory();

        System.out.println("Used mem="+used+", free="+free);
        Assertions.assertTrue(used < 10_000_000_000L, "used memory over ten gigs?");

        List<Order> orders = orderCache.getMatchingOrders(sortCols,100, 0, columnToPrefiMap);
        long took = System.currentTimeMillis()-start;
        System.out.println("Auto-complete search took(ms)="+took+", matches="+orders.size());
        Assertions.assertTrue(orders.size() > 99, "not enough orders");

    }

    @Test
    public void testJavaSort() {
        List<Integer> unsorted = new ArrayList<>();

    }

    @Test
    @Disabled("Don't want to run in normal unit tests, high memory usage")
    public void testConcurrentSkipListMapIteration()
    {
        long start = System.currentTimeMillis();
        // skip list is what we do today
        ConcurrentSkipListMap<Long, Long> longMap = new ConcurrentSkipListMap<>();

        // see how much faster we can be with a synchronized list if it becomes necessary
        // TODO: Extreme optimization, could do it this way with binarySearch insertion
        List<Long> longList = Collections.synchronizedList(new ArrayList<>());

        for (long i = 0; i < 10_000_000; i++) {
            longMap.put(i, i);
            longList.add(i);
        }

        System.out.println("Seeding took="+(System.currentTimeMillis()-start));

        for (int i=0; i<3; i++) {
            iterateTheSkipLilst(longMap); // about 750ms, not too much slower than the list
            iterateTheList(longList); // about 500ms
        }
    }

    private void iterateTheList(List<Long> longLinst) {
        long startList = System.currentTimeMillis();
        long blah = 1;
        for (Long aLong : longLinst) {
            blah += aLong % 23456789;
        }
        System.out.println("List iter took(ms)="+(System.currentTimeMillis()-startList)+", blah="+blah);
    }

    private void iterateTheSkipLilst(ConcurrentSkipListMap<Long, Long> longMap) {
        long startIter = System.currentTimeMillis();
        int count = 0;
        long random = 0;
        for (Long aLong : longMap.navigableKeySet()) {
            count++;
            random += aLong % 1_000_000; // do some work to avoid unwanted ByteCode optimizations
        }


        System.out.println("Iter CSKM(ms)="+(System.currentTimeMillis()-startIter)+", count="+count+", random="+random);
    }

    @Test
    public void testIterationJavaSpeed() {
        ConcurrentSkipListMap<Order, Order> port = new ConcurrentSkipListMap<>(new HedgeOsOrderIdComparator());
        List<Order.Builder> orderBulder = MockOrderGenerator.generateOrders(10);
        for (Order.Builder builder : orderBulder) {
            port.put(builder.build(), builder.build());
        }
        System.out.println(port.size());

        System.out.println("Start = ");
        long start = System.currentTimeMillis();

        // yeah, keep them sorted in Skip List Map, or even insertion sort in a Linked List?
        // make one pass and get the first 100 matching orders, from the Matches.
        // The matches, are computed, very quickly (machine speed on bitmaps)
        int size = 100_000_000;
        List<Integer> list = Collections.synchronizedList(new ArrayList<>(size));

        PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>();
        for (Integer integer : list) {
            minHeap.add(integer);
        }

        System.out.println("Array List took="+(System.currentTimeMillis()-start));

        // Synchronized LinkedList is 23 ms per 10_000_000 iteration wise with this
        List<Integer> linkedList = Collections.synchronizedList(new LinkedList<>());

        PriorityQueue<Integer> minHeap2 = new PriorityQueue<Integer>();
        for (Integer integer : list) {
            minHeap2.add(integer);
        }

        System.out.println("Linked List took="+(System.currentTimeMillis()-start));

    }
}
