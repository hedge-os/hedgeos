package com.hedgeos.service.position;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.roaringbitmap.RoaringBitmap;

public class RoaringBitmapTrieTest {

    @Test
    public void testRoaringBitmapTrie() {
        String chris = "Chris";
        RoaringBitmapTrie rbt = new RoaringBitmapTrie();
        rbt.put(chris, 11);

        rbt.put("ch", 12);
        rbt.put("chr", 13);

        RoaringBitmap rb = rbt.get("chr");
        Assertions.assertArrayEquals(new int[]{13}, rb.toArray(), "wrong RBs set");
        
        RoaringBitmap rb2 = rbt.get("ch");
        Assertions.assertArrayEquals(new int[]{11, 12}, rb2.toArray(), "wrong RBs set");

    }
}
