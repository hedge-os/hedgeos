-- create a fake position and portfolio trade for -999 for testing the position-service
INSERT INTO public.position_meta (position_id, portfolio_id, sub_manager_id, strategy_id, sub_strategy_id, book_id, sub_book_id, business_line_id, create_time, valid_from, valid_till, create_user)
VALUES (-999, -999, null, -999, null, null, null, -999, '2020-12-17 11:06:40.000000', null, null, 'mock_data');

INSERT INTO public.portfolio_trade (portfolio_trade_id, primary_sid, secondary_sid, position_id, target_quantity, create_time, start_time, end_time, name, trader_working, last_update_time, last_update_user, notes)
VALUES (-999, -999, null, -999, -999, '2020-12-17 11:11:07.000000', '2020-12-17 11:11:11.000000', '2021-12-17 11:11:38.000000', null, 'chris', null, 'chris', 'Mock PM Trade for testing');

INSERT INTO public.order_oms (hedge_os_order_id, parent_hedge_os_order_id, portfolio_trade_id, fix_client_order_id, fix_order_id, fix_orig_client_order_id, exec_broker, fix_incoming_seq_number, fix_outgoing_seq_number, sid, exchange_symbol, side, limit_price, stop_price, order_type, fix_account, exchange, broker, fix_symbol, create_time, create_user, last_update_user, last_update_time, is_allocation_order)
VALUES (-999, null, -999, 'MOCK_CLORDID', 'MOCK_ORDID', 'null', 'MOCK_BROKER', -999, -999, -999, 'MOCK_STOCK', 'BUY', null, null, 'MARKET', '-999', 'MOCK_EXCHANGE', 'MOCK_BROKER', 'MOCK', '2020-12-17 11:14:54.000000', 'mock_user', 'mock_user', '2020-12-17 11:15:04.000000', false);

INSERT INTO public.execution_report (hedge_os_exec_id, hedge_os_order_id, fix_exec_id,
                                     fix_exec_ref_id, fix_incoming_seq_number, fix_outgoing_seq_number,
                                     fix_order_status, quantity,
                                    fix_last_shares, fix_last_price,
                                     fix_cum_qty, fix_avg_price, fix_last_spot_rate,
                                     fix_last_forward_points, create_time, create_user,
                                     last_update_time, last_update_user)
VALUES (-999, -999, '-999',
        null, -999, -999,
        'FILLED', -999, -999, -999, -999, -999.99, 0, 0, '2020-12-17 11:23:41.000000', 'mock_user', '2020-12-17 11:23:50.000000', 'mock_user');

INSERT INTO public.execution_report_last_fill_per_order (
                                                         hedge_os_exec_id,
                                                         hedge_os_order_id,
                                                         fix_exec_id,
                                                         fix_last_shares,
                                                         fix_last_price, fix_cum_qty, fix_avg_price, fix_last_spot_rate, fix_last_forward_points, create_time, create_user, last_update_time, last_update_user)
VALUES (-999, -999, '-999', -999, -999.99, -999, -999.99, 0, 0, '2020-12-17 11:24:56.000000', 'mock_user', '2020-12-17 11:25:06.000000', 'mock_user');

-- Join the order to a position
INSERT INTO public.order_position_xref (hedge_os_order_id, position_id, create_time, valid_from, valid_till, last_update_time, last_update_user)
VALUES (-999, -999, '2020-12-17 16:44:19.000000', null, null, '2020-12-17 16:44:25.000000', 'chris');
