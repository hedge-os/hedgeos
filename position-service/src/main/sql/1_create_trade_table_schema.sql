
-- Note:  Portfolio is in RefData

-- this is an important one, holds the placement of positions
-- you can update where a position lives, bi-temporal on valid_till = null as the latest
create table position_meta (
    position_id bigserial constraint position_meta_pk primary key,
    portfolio_id integer not null,
    sub_manager_id integer null,
    strategy_id integer constraint strategy not null,
    sub_strategy_id integer null,
    book_id integer null,
    sub_book_id integer null,
    business_line_id integer null,
    create_time timestamp,
    valid_from timestamp null,
    valid_till timestamp null,
    create_user varchar(100)
);

-- a trade gets you into a position, may be across many brokers and platforms
-- it represents something a PM wants to achieve (not how they achieve it.)
create table portfolio_trade (
  portfolio_trade_id bigserial
      constraint portfolio_trade_pk primary key, -- unique trade id for this
  primary_sid bigint, -- must exist first as a security (TSLA's sid)
  secondary_sid bigint, -- maybe a second leg, etc, TODO: Review and discuss
  position_id integer not null, -- trade is for a PM (or a "desk" which is a PM in our system terms.)
  target_quantity decimal not null, -- how much you want to enter, negative for shorts
  create_time timestamp not null, -- time created
  start_time timestamp not null, -- time to start the trade
  end_time timestamp not null,
  name varchar(100) null, -- if you want to name a trade
  trader_working varchar(100), -- a trader is working the trade
  last_update_time timestamp,
  last_update_user varchar(100), -- who touched it last
  notes varchar(1000) -- description of the trade (acquire a billion TSLA by Thursday)
);

-- Matches the ems.proto hedge os order
-- Order is a reserved keyword, therefore, table is called order_oms
create table order_oms (
    hedge_os_order_id bigserial
       constraint order_pk primary key, -- like a UUID, but stuffed into a long (by HedgeOS)
    parent_hedge_os_order_id bigint null, -- a hedge-os order, parented this one
    portfolio_trade_id bigint
        constraint order_portfolio_trade_fk
            references portfolio_trade
        null, -- parent pm_trade, may not exist
    fix_client_order_id varchar(50), -- a FIX thing we invent in the EMS
    fix_order_id varchar(50), -- what the broker invents
    fix_orig_client_order_id varchar(50), -- orig client order ID
    exec_broker varchar(100), -- who executed it
    fix_incoming_seq_number int,
    fix_outgoing_seq_number int,
    sid bigint, -- the security id of the order
    exchange_symbol varchar(100), -- if you want can store here for the order
    side varchar(10) not null, -- BUY, SELL, SELL_SHORT (from the proto)
    limit_price decimal, -- what the price is
    stop_price decimal, -- where to stop
    order_type varchar(30), -- limit, market, etc
    fix_account varchar(100), -- the account at the broker
    exchange varchar(50), -- exchange the order is on
    broker varchar(100), -- the broker for the order
    fix_symbol varchar(100), -- should be a SID but this is the FIX symbol
    create_time timestamp,
    create_user varchar(100),
    last_update_user varchar(100),
    last_update_time timestamp,
    -- we can allocate by making children orders who buy from an Execution Desk
    is_allocation_order boolean
);

-- this gets a copy of every execution report
-- these are usually via FIX, but sometimes may not be
create table execution_report(
    hedge_os_exec_id bigserial not null, -- we invent this when inserted
    hedge_os_order_id bigint
        constraint execution_report_order_fk
            references order_oms not null, -- from the order, fills can't happen without an order
    fix_exec_id varchar(100) not null, -- unique for ever exec report every day
    fix_exec_ref_id varchar(100) null, -- for cancel corrects
    fix_incoming_seq_number int null,
    fix_outgoing_seq_number int null,
    fix_order_status varchar(100) null, -- FILLED, DONE_FOR_DAY etc
    quantity decimal not null,
    fix_last_shares decimal null, -- this is the "quantity" of this Fill
    fix_last_price decimal null, -- this is the "price" of this Fill
    fix_cum_qty bigint null, -- from the fill itself
    fix_avg_price decimal null,
    fix_last_spot_rate decimal null, -- for FX orders
    fix_last_forward_points decimal null,
    create_time timestamp,
    create_user varchar(100),
    last_update_time timestamp,
    last_update_user varchar(100)
);

-- anytime there is a FILL processed, also save it here (updating if necessary.)
-- this table is a CACHE of the latest fill because each fill has the Cum Qty for the Order
-- this will have only one execution report, the last for every order
-- because the last execution report for every order will know the quantity
create table execution_report_last_fill_per_order(
    hedge_os_exec_id bigserial not null, -- we invent this when inserted
    hedge_os_order_id bigint,
    fix_exec_id varchar(100) not null, -- unique for ever exec report every day
    fix_last_shares decimal, -- this is the "quantity" of this Fill
    fix_last_price decimal, -- this is the "price" of this Fill
    fix_cum_qty decimal, -- total quantity executed
    fix_avg_price decimal,
    fix_last_spot_rate decimal, -- for FX orders
    fix_last_forward_points decimal,
    create_time timestamp,
    create_user varchar(100),
    last_update_time timestamp,
    last_update_user varchar(100)
);

-- A critical concept for positions
-- An order is done for a security
-- A position is a thing with BUS_LINE --> PORTFOLIO --> STRATEGY (position_metadata)
-- And the order_position_xref, is what puts an Order in a Position
-- (therefore also a Portfolio, Strategy, and whatever else is assigned)

-- This xref design, allows you fun things like:
-- 1.  You can move positions without impacting the position economic data
-- 2.  You can alter the location of an order without touching the order itself

-- Note:  the PK is merely for tracking the row across copies of the system,
-- is not used elsewhere, (for easy updates when necessary, or comparisons.)

-- And in doing so, we mimic what actually happens in the world, which is orders are allocated to positions
-- either directly, or split up and then done so.
-- This should also help with tracking the lineage of allocation pro-rata and other concerns
create table order_position_xref
(
    order_pos_xref_id bigserial not null
        constraint order_position_xref_pk
            primary key,
        hedge_os_order_id bigint not null
        constraint order_position_xref_order_fk
            references order_oms,
    position_id bigint not null
        constraint order_position_xref_pos_position_fk
            references position_meta,
    create_time timestamp not null,
    valid_from timestamp,
    valid_till timestamp,
    last_update_time timestamp not null,
    last_update_user varchar(100) not null
);



-- view which gives us a starting quantity
-- Can be increased speed wise later with timescale etc, will only be for boot of the service
-- after boot, everything happens in memory
create function get_position_latest(in_portfolio_id integer)
    returns TABLE(sid bigint, position_id bigint, quantity numeric)
    language sql
as
$$
    -- position_id breaks it up into portfolio, strategy etc.
    -- the Order is what security you own, and contains the map
    -- the XREF as explained in comment above, maps the order into a position
select o.sid, xref.position_id, sum(erlpo.fix_cum_qty)
from position_meta pm,
     order_oms o,
     execution_report_last_fill_per_order erlpo,
     order_position_xref xref
where
        pm.position_id = xref.position_id
  and xref.hedge_os_order_id = o.hedge_os_order_id
  and o.hedge_os_order_id = erlpo.hedge_os_order_id
  and pm.portfolio_id = in_portfolio_id
group by
    xref.position_id,
    o.sid
$$;

alter function get_position_latest(integer) owner to hedgeos;
