package com.hedgeos.service.position;

import com.hedgeio.trade.*;
import com.hedgeos.position.Position;
import com.hedgeos.position.PositionAndMeta;
import com.hedgeos.position.PositionRequest;
import com.hedgeos.position.PositionResponse;
import com.hedgeos.service.position.dao.ExecutionReportDao;
import com.hedgeos.service.position.dao.OrderDao;
import com.hedgeos.service.position.dao.PositionDao;
import com.hedgeos.service.security.SecurityServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static com.hedgeos.api.Ports.SEC_SERVICE_GRPC;

@Component
@RequiredArgsConstructor
@Slf4j
public class PositionServer extends PositionServiceGrpc.PositionServiceImplBase {

    private SecurityServiceGrpc.SecurityServiceBlockingStub securityServiceBlockingStub;

    // required ags constructor makes this
    private final PositionCache positionCache;
    private final OrderCache orderCache;

    private final OrderDao orderJdbc;
    private final ExecutionReportDao execReportWorker;

    @PostConstruct
    private void init() {
        ManagedChannel managedChannelSec = ManagedChannelBuilder
                .forAddress("localhost", SEC_SERVICE_GRPC).usePlaintext().build();
        // get a client to the position service
        securityServiceBlockingStub = SecurityServiceGrpc.newBlockingStub(managedChannelSec);
    }



    @Override
    public void orderPlaced(OrderPlacedRequest request, StreamObserver<OrderPlacedResponse> responseObserver) {

        positionCache.orderPlaced(request);
        orderCache.addOrUpdateOrder(request);
        //push to the Database (in a background thread - eventual consistency)
        orderJdbc.saveOrders(request.getOrdersList());

        OrderPlacedResponse response =
                OrderPlacedResponse.newBuilder().setStatus(OrderMessageStatus.SUCCESS).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

    }

    @Override
    public void orderFilled(OrderExecutionReportRequest request, StreamObserver<OrderExecutionReportResponse> responseObserver) {

        for (ExecutionReport fill : request.getExecutionReportsList()) {
            positionCache.execution(fill);
            orderCache.addExecutionReport(fill);
        }

        execReportWorker.saveAll(request.getExecutionReportsList());

        responseObserver.onNext(OrderExecutionReportResponse.newBuilder().setStatus(OrderMessageStatus.SUCCESS).build());
        responseObserver.onCompleted();
    }

    /** TODO:  Rename this to --> getPositionById **/
    @Override
    public void getPosition(PositionRequest request, StreamObserver<PositionResponse> responseObserver) {

        if (request.getPositionMeta().getPositionId() == 0L) {
            log.error("Position request without position_id set, necessary to retrieve positions.");
            throw new RuntimeException("Position request without position_id set, necessary to retrieve positions.");
        }

        Position position = positionCache.getPositionByPositionId(request);

        responseObserver.onNext(PositionResponse.newBuilder().addPosition(position).build());

        responseObserver.onCompleted();
    }

    @Override
    public void positionSubscription(PositionRequest request, StreamObserver<PositionResponse> responseObserver) {
        throw new RuntimeException("Must implement");
    }

    @Override
    public void getPositionAndMeta(PositionRequest request, StreamObserver<PositionAndMeta> responseObserver) {
        throw new RuntimeException("Must implement");
    }

    @Override
    public void orderStatusUpdate(OrderStatusRequest request, StreamObserver<OrderStatusResponse> responseObserver) {

    }

}
