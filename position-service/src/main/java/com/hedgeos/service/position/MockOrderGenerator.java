package com.hedgeos.service.position;

import com.hedgeio.trade.OrdType;
import com.hedgeio.trade.Order;
import com.hedgeio.trade.Side;
import com.hedgeos.general.Price;
import com.hedgeos.position.Position;
import com.hedgeos.position.PositionMeta;
import com.hedgeos.service.position.cache.MetaStringCache;
import com.hedgeos.service.refdata.repository.PortfolioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class MockOrderGenerator {

    public static List<Order.Builder> generateOrders(int orderCount) {
        List<Order.Builder> orderBuilder = new ArrayList<>();
        for (int i=0; i< orderCount; i++) {
            Order.Builder ob = Order.newBuilder();
            ob.setHedgeOsOrderId(i);
            ob.setOrderType(OrdType.LIMIT);
            ob.setLimitPrice(Price.newBuilder().setUnits(i % 50).setNanos(i % 50 * 100_000_000).build());
            if (i % 2 == 0)
                ob.setSide(Side.BUY);
            else
                ob.setSide(Side.SELL);

            if (i % 10 == 0)
                ob.setSide(Side.SELL_SHORT);

            ob.setExchangeSymbol(i+"SYMBOL");

            PositionMeta.Builder builder = PositionMeta.newBuilder();
            builder.setPortfolioId(i % 9 + 1); // 1-9 portfolio IDs
            builder.setStrategyId(i % 2);
            builder.setFundId(1);

            ob.setSid(3);
            if (i % 2 == 0)
                ob.setSid(2);

            // forgot to add the meta, duh
            ob.setPositionMeta(builder);

            orderBuilder.add(ob);
        }
        return orderBuilder;
    }
}
