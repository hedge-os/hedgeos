package com.hedgeos.service.position.dao;

import com.hedgeio.trade.ExecutionReport;
import com.hedgeio.trade.Order;
import com.hedgeos.date.ProtoConverter;
import com.hedgeos.general.Price;
import com.hedgeos.general.Quantity;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.position.Position;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class PositionDao {

    // for group by sum the current position on boot-up, per portfolio
    final String POSITION_FUNCTION_SQL =
            "select sid, position_id, quantity from get_position_latest(?)";

    final PositionJdbcConfig positionJdbcConfig;

    public PositionDao(PositionJdbcConfig jdbcConfig) {
        this.positionJdbcConfig = jdbcConfig;
    }

    /**
     * We use the database to to a group by (position_id), sum(qty) on the last set of orders.
     *
     * @param portfolioId the Portfolio we wan to load here
     *                    (is called for every portfolio, threaded to speed up booting.)
     *
     * @return a list of positions with Quantities, security key, and position ID set
     */
    public List<Position> bootstrapPositionsFromDb(int portfolioId) {

        long start = System.currentTimeMillis();

        try (Connection conn = positionJdbcConfig.positionDataSource().getConnection()) {

            PreparedStatement pst = conn.prepareStatement(POSITION_FUNCTION_SQL);
            pst.setInt(1, portfolioId);
            ResultSet rs = pst.executeQuery();

            List<Position> positions = new ArrayList<>();
            while (rs.next()) {
                Position.Builder builder = Position.newBuilder();
                builder.setPositionId(rs.getLong("position_id"));
                builder.setQty(Quantity.newBuilder().setQuantity(rs.getLong("quantity")).build());
                builder.setSecurityKey(SecurityKey.newBuilder().setSecurityId(rs.getLong("sid")).build());
                positions.add(builder.build());
            }

            log.warn("Returning bootstrap positions for port="+portfolioId+
                    ", count="+positions.size()+
                    ", took(ms)="+(System.currentTimeMillis()-start));

            return positions;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
