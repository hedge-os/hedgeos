package com.hedgeos.service.position.cache;

import com.hedgeio.trade.Order;
import com.hedgeos.position.PositionMeta;
import com.hedgeos.service.position.PositionMetaResolved;
import com.hedgeos.service.refdata.domain.Portfolio;
import com.hedgeos.service.refdata.repository.PortfolioRepository;
import com.hedgeos.service.security.repository.SecurityRepository;
import com.hedgeos.service.security.domain.SecurityJpa;
import com.hedgeos.ui.api.hedgeos.UiOrder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * This is an important little cache of Ref + Security Data which
 * Resolves the strings into values everywhere from Hazelcast
 * (for easy Java consumption.), via JPA.
 *
 * This is a surprisingly important task in a distributed financial system,
 * allowing for the renaming of things almost arbitrarily without renaming any of the data itself.
 * (because the data is tied to "id" and not the String names themselves.)
 * **/

@RequiredArgsConstructor
@Component
public class MetaStringCacheImpl implements MetaStringCache {

    // JPA but is in Hazelcast with a Near Cache
    // TODO:  Performance test and improve speed of the near cache
    final PortfolioRepository portfolioRepository;
    final SecurityRepository securityRepository;

    volatile int i;

    @Override
    public PositionMetaResolved getPositionMetaStrings(PositionMeta positionMeta) {

        PositionMetaResolved pmr = new PositionMetaResolved();

        // int fakePortfolioId = positionMeta.getPortfolioId() % 10 + 1;
        int fakePortfolioId = i % 10;

        // TODO:  Investigate the speed of near caching this
        Portfolio portfolio = portfolioRepository.findById(fakePortfolioId);
        if (portfolio == null)
            pmr.setPortoflio("New Manager");
        else
            pmr.setPortoflio(portfolio.getName());

        i = i + 1;

        if (i % 33 == 0)
            pmr.setStrategy("EWING");

        pmr.setStrategy("KOLJA");
        pmr.setBbTicker("TSLA 555 Dic 20 Puts at 35");

        if (i % 33 == 0) {
            pmr.setBbTicker("AAPL");
            pmr.setStrategy("KNICKS");
        }

        if (i % 2 == 0) {
            pmr.setBbTicker("MSFT");
            pmr.setStrategy("CHRIS");
        }

        if (i % 7 == 0) {
            pmr.setBbTicker("AMZN");
        }

        return pmr;
    }

    @Override
    public void setOrderMetaStrings(UiOrder uiOrder, Order order) {
        PositionMetaResolved pmr = this.getPositionMetaStrings(order.getPositionMeta());
        uiOrder.setPortfolio(pmr.getPortoflio());
        uiOrder.setStrategy(pmr.getStrategy());

        Optional<SecurityJpa> security = this.securityRepository.findById(order.getSid());
        // TODO:  Add BB Ticker to Security etc
        security.ifPresent(value -> uiOrder.setBbTicker(value.getName()));


    }
}
