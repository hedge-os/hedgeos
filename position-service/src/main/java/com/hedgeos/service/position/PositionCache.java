package com.hedgeos.service.position;

import com.google.protobuf.Timestamp;
import com.hedgeio.trade.ExecutionReport;
import com.hedgeio.trade.Order;
import com.hedgeio.trade.OrderPlacedRequest;
import com.hedgeio.trade.OrderPositionXref;
import com.hedgeos.general.Quantity;
import com.hedgeos.position.Position;
import com.hedgeos.position.PositionRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Update the quantity from what it was, to the addition which happened (or the creation of a Quantity.)
 *
 * This is one atomic operation in memory:  AtomicLong.getAndAdd
 *
 * If the position exists; we merely do a CHM lookup, and AtomicLong.getAndAdd
 *
 * If there is no position, we chose from one of 32 locks by  modulo on the position_id,
 * lock briefly and make a new AtomicLong in the CHM.  This will be close to zero collisions
 * as it is only occur at  creation of a position we have never seen before, and even then % 32.
 *
 * (and if there is somehow perceived high contention, the 32 can be increased.)
 *
 * To keep this class reasonably small and clear, this is the entire *in memory* portion,
 * and the DAO class will handle persistence.
 */
@Slf4j
@Component
public class PositionCache {

    // THE MOST CRITICAL MAP, the POSITION
    // one position in the fund.. needs to be by PositionMeta as the "position key"
    // use the Builder because we update the Quantity on the fly
    final Map<Long, AtomicLong> perPositionIdQuantityMap = new ConcurrentHashMap<>();

    // for lookup of the order when the fills arrive
    final Map<Long, Order> orderIdToOrderMap = new ConcurrentHashMap<>();

    // the last execution tracks the quantity filled on the Order
    // the fix_cum_qty for the order is here, which is the last known quantity for the order
    final Map<Long, ExecutionReport> orderToLatestFillMap = new ConcurrentHashMap<>();

    // Must keep a map of the fills we have seen, to never de-dup if we are replayed against
    // (because this would duplicate the fills, be very bad.)
    // TODO:  Must load this up on boot from the database from start of the day (or yesterday)
    final Map<String, ExecutionReport> fillsAlreadyCounted = new ConcurrentHashMap<>();

    // lock for new positions.  We won't make one for every position, because we don't want another Hash Map lookup
    // instead, make a small array and modulo.  The amount of collisions will still be tiny, as new positions
    // never seen before are rare enough.  And, a collision is anyway very quick, one CHM put in the critical section.
    public static final int NEW_POSITION_LOCK_ARRAY_SIZE = 128;

    final ReentrantReadWriteLock[] newPositionLockArray = new ReentrantReadWriteLock[NEW_POSITION_LOCK_ARRAY_SIZE];

    public PositionCache() {
        // seed the block of position locks
        for (int i=0; i<newPositionLockArray.length; i++) {
            newPositionLockArray[i] = new ReentrantReadWriteLock();
        }
    }

    /**
     * This is how positions are updated, when ExecutionReports arrive for Orders.
     * We find the position, and increase it with the quantity.
     *
     * Every single ExecutionReport passed to this needs to have the "cumulativeQty" set correctly.
     *
     * @param fill an execution report with a new "cumulativeQuantity" for an order (the latest quantity)
     */
    public void execution(ExecutionReport fill) {
        Order orderForFill = orderIdToOrderMap.get(fill.getHedgeOsOrderId());
        if (orderForFill == null) {
            log.error("Must always have the hedge-os order id for a fill, missing=" + fill.getHedgeOsOrderId());
            return;
        }

        String fillKey = makeFillUniqueKey(fill);
        if (fillsAlreadyCounted.containsKey(fillKey)) {
            // TODO:  Need to unit-test recovery
            log.error("We've seen this fill, must be replay, skipping="+fill);
            return;
        }
        else {
            fillsAlreadyCounted.put(fillKey, fill);
        }

        // We need the old fill to do the delta for the order
        ExecutionReport oldFill = orderToLatestFillMap.put(fill.getHedgeOsOrderId(), fill);

        // this is the new total quantity for the order
        long qtyToAdd = fill.getCumulativeQty().getQuantity();

        // if there was no old fill, then we will be adding this qtyToAdd as the Quantity
        if (oldFill != null) {
            // if there was an old fill, we need to calculate the delta between the old fill and this fill
            // for the "cumulative quantity" (fix_cum_qty = cumulativeQuantity on the ExecutionReport msg.)
            long newQty = fill.getCumulativeQty().getQuantity();
            long oldQty = oldFill.getCumulativeQty().getQuantity();

            // quantity can only go up per order, don't allow it to go down
            if (oldQty > newQty) {
                // this could be a correction but should be rare if ever
                log.error("Old Qty > New Qty (the filled quantity for oder decreased="+
                        fill.getHedgeOsOrderId()+", fill="+fill);
                // you could put a boolean here to stop the system if you see this as it is irrational
                // but you should send an email about it, as it should be rare
                // and Exit, unless it is a correction of some sort
                // (but should not be, a new order can correct it.)

                // TODO:  If this is a correction is it ok, otherwise bad.
                // TODO:  Code and unit test corrections, only allow this to go down if a correction (maybe, later)
                return;
            }

            // we are transitioning the overall quantity from what we knew before, to what we know now
            // we need to alter it by this much = qtyToAdd
            qtyToAdd = newQty - oldQty;
        }

        findPositionAndUpdateQuantity(orderForFill, qtyToAdd);
    }

    private String makeFillUniqueKey(ExecutionReport fill) {
        return ""+fill.getHedgeOsOrderId() + fill.getHedgeOsExecId();
    }

    private void findPositionAndUpdateQuantity(Order orderForFill, long qtyToAdd) {
        // orders are mapped to positions by their Position Meta
        Long positionId = orderForFill.getPositionMeta().getPositionId();

        // look up the current known quantity for this position
        AtomicLong qty = perPositionIdQuantityMap.get(positionId);

        if (qty != null) {
            // if we had a prior quantity (including zero),
            // we update it with the "delta" from the old qty and finished
            // CAS, one operation or as fast as can be.
            qty.getAndAdd(qtyToAdd);
            // great, early exit, we already seeded the positionId
            return;
        }

        // down here, we have yet to put a quantity in the position map (should be rare)
        // as multiple threads can try to create the position at the same time
        // we need to lock on something.

        // a creative solution: instead of having a lock for every position, which would be millions of locks
        // keep a block of locks which reduces the collisions by 30x or more (NEW_POSITION_LOCK_ARRAY_SIZE.)
        // We can performance test the lock count and find the optional count, but 128 is a fine start
        // Because positions usually exist (can be pre-populated as 0 qty),
        //    ... this will be close to zero collisions in practice, with 128 locks.
        // and this block is extremely fast; this should never be a bottleneck.
        // by my math, the lock is one hash map lookup, could be < 10us (10 mics)

        // notice we lock, and release in the finally clause.
        // we use try lock to make it a spin lock
        ReentrantReadWriteLock newPositionLock = newPositionLockArray[ (int) (positionId % NEW_POSITION_LOCK_ARRAY_SIZE)];
        ReentrantReadWriteLock.WriteLock writeLock = newPositionLock.writeLock();
        // do a CAS to try to lock here; faster than getting a lock
        boolean locked = writeLock.tryLock();

        // spin lock, trying to lock if we didn't get it
        // (will be incredibly rare, as there are 32 of these, and only on creation of positions)
        // would need to be same modulo to the lock array selection, in < 10us the critical section time.
        while (!locked) {
            // read "tryLock" code:  it is a CAS, meaning this will spin hot
            // but contention is VERY LOW if ever, and the critical section is tiny
            locked = writeLock.tryLock();
        }

        try {
            // this is why we lock, because, two threads could arrive at the lock above at the same instant
            AtomicLong newQty = perPositionIdQuantityMap.get(positionId);

            if (newQty == null) {
                perPositionIdQuantityMap.put(positionId, new AtomicLong(qtyToAdd));
            } else {
                // case where two Fills came in for the same position at the same time and it was new
                // one of the threads will find a position ID
                // CAS, one operation or as fast as can be.
                newQty.getAndAdd(qtyToAdd);
            }
        }
        finally {
            writeLock.unlock();
        }
    }

    /**
     * Because of the work done above to put a running total Qty in the position map
     * We already know the position, and it is updated in real time.
     * @param request a request to lookup a position
     * @return  A position which holds the Quantity and as of time for the position
     */
    public Position getPositionByPositionId(PositionRequest request) {

        AtomicLong quantity = perPositionIdQuantityMap.get(request.getPositionMeta().getPositionId());

        if (quantity == null) {
            System.err.println("No quantity for secId=" + request.getSecurityId());
            throw new RuntimeException("No quantity fo secId="+request.getSecurityId());
        }

        // TODO:  Question if the AsOf time should be set here, or set when the position is updated.
        // (most likely when the update occurs, but this is not critical.)
        long currentNanos = System.nanoTime();
        long seconds = currentNanos/(1000L^3); // 1 billion nanos in a second
        int nanos = (int) (currentNanos % (1000L^3)); // modulo is the remainder after seconds

        Timestamp.Builder tBuilder = Timestamp.newBuilder().setSeconds(seconds).setNanos(nanos);

        Position position =
                Position.newBuilder()
                        .setPositionId(request.getPositionMeta().getPositionId())
                        .setQty(Quantity.newBuilder().setQuantity(quantity.get()))
                        .setAsOfTime(tBuilder)
                    .build();

        return position;
    }

    /**
     * Simply put every order in the map to get ready
     * - necessary before receiving executions for this order
     * **/
    public void orderPlaced(OrderPlacedRequest request) {
        for (Order order : request.getOrdersList()) {
            orderIdToOrderMap.put(order.getHedgeOsOrderId(), order);
        }
    }

    // on startup, we will seed the position-service from the database
    public void bootstrapQuantities(List<Position> positions) {
        for (Position position : positions) {
            this.perPositionIdQuantityMap.put(position.getPositionId(),
                    new AtomicLong(position.getQty().getQuantity()));
        }
    }


    public void bootstrapExecsSeenToday(List<ExecutionReport> execsSeenToday) {
        for (ExecutionReport executionReport : execsSeenToday) {
            String fillKey = makeFillUniqueKey(executionReport);
            this.fillsAlreadyCounted.put(fillKey, executionReport);
            this.orderToLatestFillMap.put(executionReport.getHedgeOsOrderId(), executionReport);
        }
    }

    public void bootstrapOrders(List<Order> ordersSeenToday) {
        for (Order order : ordersSeenToday) {
            this.orderIdToOrderMap.put(order.getHedgeOsOrderId(), order);
        }
    }

}
