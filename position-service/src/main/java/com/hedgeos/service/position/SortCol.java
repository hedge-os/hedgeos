package com.hedgeos.service.position;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.swing.*;

/**
 What columns to sort, and then what order
 These go in a list to allow for sorting by multiple columns
 */
@Data
@RequiredArgsConstructor
public class SortCol {
    public final String column;
    public final SortOrder sortOrder;
}
