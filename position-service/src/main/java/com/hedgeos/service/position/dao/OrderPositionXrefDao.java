package com.hedgeos.service.position.dao;

import com.hedgeio.trade.OrderPositionXref;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class OrderPositionXrefDao {

    private final PositionJdbcConfig jdbcConfig;

    public OrderPositionXrefDao(PositionJdbcConfig jdbcConfig) {
        this.jdbcConfig = jdbcConfig;
    }

    // we INTENTIONALLY do not join in these boot queries; much quicker to get one entire table
    String SELECT_ORDER_XREF =
            "select * from order_position_xref where create_time > ?";

    public List<OrderPositionXref> getOrderPositionXrefsSeenToday(long sinceMillis)
    {
        long start = System.currentTimeMillis();
        try (Connection conn = jdbcConfig.positionDataSource().getConnection()) {
            PreparedStatement pst = conn.prepareStatement(SELECT_ORDER_XREF);
            pst.setTimestamp(1, new java.sql.Timestamp(sinceMillis));

            List<OrderPositionXref> xrefs = new ArrayList<>();
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                OrderPositionXref.Builder builder = OrderPositionXref.newBuilder();
                builder.setPositionId(rs.getLong("position_id"));
                builder.setHedgeOsOrderId(rs.getLong("hedge_os_order_id"));
                builder.setOrderXrefPositionId(rs.getLong("order_position_xref_id"));
                xrefs.add(builder.build());
            }
            log.info("Xrefs bootstrap took(ms)="+(System.currentTimeMillis()-start));
            return xrefs;
        }
        catch (Exception e) {
            log.error("Exception loading orderXrefs for Positions");
            throw new RuntimeException("Exception loading xrefs", e);
        }
    }
}
