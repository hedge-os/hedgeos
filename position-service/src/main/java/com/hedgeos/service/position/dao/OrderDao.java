package com.hedgeos.service.position.dao;

import com.hedgeio.trade.OrdType;
import com.hedgeio.trade.Order;
import com.hedgeio.trade.OrderPositionXref;
import com.hedgeio.trade.Side;
import com.hedgeos.date.ProtoConverter;
import com.hedgeos.position.PositionMeta;
import com.hedgeos.refdata.Exchange;
import com.hedgeos.thread.Sleep;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Uses spring jdbc template to save orders to the background.
 * Reasonably quick, and is in the background, a million rows in ten minutes (which is fine in the background.)
 */
@Component
@Slf4j
public class OrderDao {

    // we will read from this and save in the background
    final LinkedBlockingQueue<Order> ordersToSaveToDb = new LinkedBlockingQueue<>();

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final PositionJdbcConfig jdbcConfig;

    public OrderDao(PositionJdbcConfig config) {
        jdbcConfig = config;
        jdbcTemplate = new NamedParameterJdbcTemplate(config.positionDataSource());
        startSaveBackgroundThread();
    }

    public void saveOrders(List<Order> ordersList) {
        this.ordersToSaveToDb.addAll(ordersList);
    }

    public void startSaveBackgroundThread() {
        Runnable r = () -> {
            //noinspection InfiniteLoopStatement
            while (true) {
                Order o = null;
                try {
                    // blocks until an order is added to save
                    o = ordersToSaveToDb.take();
                    saveOrderToDb(o);
                    log.info("Saved order="+o.getHedgeOsOrderId());
                }
                catch (Exception e) {
                    log.error("Exception saving order="+
                            (o != null ? o.getHedgeOsOrderId() : 0),e);
                    ordersToSaveToDb.add(o); // try again, can't drop orders
                    // DB had a problem, slow down, hopefully things improve.
                    Sleep.sleep(5_000);
                }
            }
        };

        // spin a thread forever
        new Thread(r, "Save Order Thread").start();
    }

    // understood JPA is what the cool kids do.  This is fast though and we know how it works..
    public static final String INSERT_SQL =
            "insert into order_oms (parent_hedge_os_order_id, " +
                    "portfolio_trade_id," +
                    "fix_client_order_id," +
                    "fix_incoming_seq_number," +
                    "fix_outgoing_seq_number," +
                    "sid,"+
                    "exchange_symbol,"+
                    "side,"+
                    "limit_price,"+
                    "stop_price,"+
                    "order_type,"+
                    "fix_account,"+
                    "exchange,"+
                    "broker,"+
                    "fix_symbol,"+
                    "create_time,"+
                    "create_user,"+
                    "last_update_time,"+
                    "last_update_user,"+
                    "is_allocation_order"+
                    ") "+
                " values (:parent_hedge_os_order_id, " +
                    " :portfolio_trade_id, " +
                    " :fix_client_order_id, " +
                    " :fix_incoming_seq_number, " +
                    " :fix_outgoing_seq_number, " +
                    " :sid, "+
                    " :exchange_symbol, "+
                    " :side, "+
                    " :limit_price, "+
                    " :stop_price, "+
                    " :order_type, "+
                    " :fix_account, "+
                    " :exchange, "+
                    " :broker, "+
                    " :fix_symbol, "+
                    " :create_time, "+
                    " :create_user,"+
                    " :last_update_user,"+
                    " :last_update_time,"+
                    " :is_allocation_order "+
                    ") ";


    public void saveOrderToDb(Order order) {

        Map<String,Object> map = new HashMap<>();
        map.put("parent_hedge_os_order_id",order.getParentHedgeOsOrderId());
        map.put("portfolio_trade_id", order.getPortfolioTradeId());
        map.put("fix_client_order_id", order.getFixClientOrderId());
        map.put("fix_incoming_seq_number", order.getFixIncomingSequenceId());
        map.put("fix_outgoing_seq_number", order.getFixOutgoingSequenceId());
        map.put("sid", order.getSid());
        map.put("exchange_symbol", order.getExchangeSymbol());
        map.put("side", order.getSide());
        map.put("limit_price", order.getLimitPrice());
        map.put("stop_price", order.getStopPrice());
        map.put("order_type", order.getOrderType());
        // todo;  review which field we use, should be the account_id not this
        map.put("fix_account", ""+order.getAccount().getCode());
        map.put("exchange", order.getExchange().getMicValue());
        // todo;  this should be an Int for Broker Id, not the broker
        map.put("broker", ""+order.getAccount().getBrokerId());
        map.put("fix_symbol", order.getExchangeSymbol());
        map.put("create_time", new java.sql.Timestamp(System.currentTimeMillis()));
        map.put("create_user", "pos-service");
        map.put("last_update_user", "pos-service");
        map.put("last_update_time", new java.sql.Timestamp(System.currentTimeMillis()));
        map.put("is_allocation_order", false);

        // map.put("salary",e.getSalary());

        jdbcTemplate.execute(INSERT_SQL,map, new PreparedStatementCallback() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps)
                    throws DataAccessException {
                try {
                    return ps.executeUpdate();
                } catch (Exception e) {
                    // log.error will be configured to send emails
                    log.error("Exception running order insert="+e, e);
                    throw new RuntimeException(e);
                }
            }
        });
    }


    final String SELECT_ORDERS_SEEN_TODAY =
            "select hedge_os_order_id, " +
                    "parent_hedge_os_order_id, "+
                    "portfolio_trade_id, "+
                    "fix_client_order_id, "+
                    "fix_order_id, "+
                    "fix_orig_client_order_id, "+
                    "exec_broker, "+
                    "sid, "+
                    "exchange_symbol, "+
                    "side, "+
                    "limit_price, "+
                    "stop_price, "+
                    "order_type, "+
                    "fix_account, "+
                    "exchange, "+
                    "broker, "+
                    "fix_symbol, "+
                    "create_time, "+
                    "create_user, "+
                    "last_update_user,"+
                    "last_update_time, "+
                    "is_allocation_order " +
                    "from order_oms " +
                    "where create_time >= ? ";


    public List<Order> getOrdersSeenToday(long startBootstrap, List<OrderPositionXref> xrefs) {
        long start = System.currentTimeMillis();

        Map<Long, OrderPositionXref> ordersXrefMap = new HashMap<>();
        for (OrderPositionXref xref : xrefs) {
            ordersXrefMap.put(xref.getHedgeOsOrderId(), xref);
        }

        List<Order> orders = new ArrayList<>();
        try (Connection conn = jdbcConfig.positionDataSource().getConnection()) {
            PreparedStatement pst = conn.prepareStatement(SELECT_ORDERS_SEEN_TODAY);
            pst.setTimestamp(1, new java.sql.Timestamp(startBootstrap));
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Order.Builder ob = Order.newBuilder();
                ob.setHedgeOsOrderId(rs.getLong("hedge_os_order_id"));
                // TODO: (likely) need the positions xref as well,
                //  TODO:  not a huge deal, one more query at 7am.
//                ob.setPositionMeta();

                OrderPositionXref xref = ordersXrefMap.get(ob.getHedgeOsOrderId());
                if (xref != null) {
                    ob.setPositionMeta(
                            PositionMeta.newBuilder()
                                    .setPositionId(xref.getPositionId()).build());
                } else {
                    log.warn("No position xref for Order?="+ob.getHedgeOsOrderId());
                }
//
//                ob.setAccount();
                ob.setSid(rs.getLong("sid"));
                ob.setPortfolioTradeId(rs.getLong("portfolio_trade_id"));
                ob.setFixClientOrderId(rs.getString("fix_client_order_id"));
                ob.setFixOrderId(rs.getString("fix_order_id"));
                ob.setSide(Side.valueOf(rs.getString("side")));
                ob.setLimitPrice(ProtoConverter.priceProtoFromDouble(rs.getDouble("limit_price")));
                ob.setStopPrice(ProtoConverter.priceProtoFromDouble(rs.getDouble("stop_price")));
                ob.setOrderType(OrdType.valueOf(rs.getString("order_type")));
                ob.setExchange(Exchange.newBuilder().setName(rs.getString("exchange")).build());
                ob.setExchangeSymbol(rs.getString("fix_symbol"));
                orders.add(ob.build());
            }

            log.warn("Position query boot up took(ms)="+(System.currentTimeMillis()-start));
            return orders;

        }
        catch (Exception e) {
            log.error("Error getting postions on bootstrap= "+e, e);
            throw new RuntimeException(e);
        }
    }


}
