package com.hedgeos.service.position;

import com.hedgeos.thread.Sleep;
import org.apache.commons.collections4.trie.PatriciaTrie;

import java.util.SortedMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ConcurrentPatriciaTrie<E> extends PatriciaTrie<E> {

    final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public int size() {
        return super.size();
    }

    @Override
    public E put(String key, E value) {
        boolean locked = lock.writeLock().tryLock();

        // we will always get the lock, but if we don't, spin on it
        if (!locked) {
            while (true) {
                locked = lock.writeLock().tryLock();
                if (locked)
                    break;
                // gentle spin lock, this is only for auto-type in the blotter
                Sleep.sleep(1);
            }
        }

        E e =  super.put(key, value);

        lock.writeLock().unlock();
        return e;
    }

    @Override
    public E get(Object k) {
        boolean locked = lock.readLock().tryLock();
        if (!locked) {
            while (true) {
                locked = lock.readLock().tryLock();
                if (locked)
                    break;
                // gentle spin lock, only for auto-complete in the trade blotter
                Sleep.sleep(1);
            }
        }

        E e =  super.get(k);

        lock.readLock().unlock();;
        return e;
    }

    @Override
    public SortedMap<String, E> prefixMap(String key) {
        boolean locked = lock.readLock().tryLock();
        if (!locked) {
            while (true) {
                locked = lock.readLock().tryLock();
                if (locked)
                    break;
                // gentle spin lock, only for auto-complete in the trade blotter
                Sleep.sleep(1);
            }
        }

        SortedMap<String, E> e =  super.prefixMap(key);

        lock.readLock().unlock();

        return e;
    }
}
