package com.hedgeos.service.position.rest;

import com.hazelcast.core.IMap;
import com.hazelcast.map.impl.proxy.MapProxyImpl;
import com.hazelcast.spring.cache.HazelcastCache;
import com.hedgeos.curve.CurveDef;
import com.hedgeos.service.refdata.domain.CurveDefinition;
import com.hedgeos.service.refdata.domain.Portfolio;
import com.hedgeos.service.refdata.repository.CurveDefinitionRepository;
import com.hedgeos.service.refdata.repository.PortfolioRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.sound.sampled.Port;
import java.util.Optional;


@RestController
@RequestMapping("/test")
public class CacheRestTest {

    private final EntityManager entityManager;
    private final CacheManager cacheManager;
    private final CurveDefinitionRepository repo;
    private final PortfolioRepository portRepo;

    public CacheRestTest(
            CacheManager cacheManager,
            @Qualifier("refdataEntityManager") EntityManager entityManager,
            CurveDefinitionRepository repo,
            PortfolioRepository portRepo)
    {
        this.repo = repo;
        this.portRepo = portRepo;
        this.cacheManager = cacheManager;
        this.entityManager = entityManager;
    }

    // TODO:  can make it a more feature rich interface on the cached side like this:
    // https://github.com/hazelcast/spring-data-hazelcast
    // TODO:  But we don't need it, only by the IDs today

    @GetMapping("/portfolio")
    public String portfolio() {
        CurveDefinition cd = entityManager.find(CurveDefinition.class, 1101L);
        Portfolio pd = entityManager.find(Portfolio.class, 1L);

        Portfolio port = portRepo.findById(3L);
        return port.getName();
    }


    @GetMapping("/repo")
    public String repo() {
        CurveDefinition cd = repo.findById(1101L);
        return cd.getName();
    }

    @GetMapping("/cacheTestWorks")
    public String cacheTestWorks() {
            StringBuffer sb = new StringBuffer();
            try {
                // TODO:  Need to do this now in the other apps
                long start = System.currentTimeMillis();
                CurveDefinition cd = entityManager.find(CurveDefinition.class, 1101L);

                System.out.println("Curve=" + cd+", took="+(System.currentTimeMillis()-start));

                return cd.getName() + ", took="+(System.currentTimeMillis()-start);
            }
            catch (Exception e) {
                System.out.println("Exception="+e);
                return ""+e;
            }
    }

    @GetMapping("/cacheManager")
    public String cacheManagerTest() {
        StringBuffer sb = new StringBuffer();

        for (String s : cacheManager.getCacheNames()) {
            System.out.printf("Cache=" + s);
            Cache cache = cacheManager.getCache(s);
            System.out.println("Cache=" + cache.getName());
            sb.append(s + "<br>");

            Object  nativeCache = cache.getNativeCache();
            System.out.println("nativeCache="+nativeCache.getClass()+", nativeCache="+nativeCache);

            MapProxyImpl cacheNative = (MapProxyImpl) nativeCache;

            for (Object o : cacheNative.keySet()) {
                System.out.println("Key="+o);
            }

            CurveDefinition cd3 = cache.get(1101L, CurveDefinition.class);
            sb.append("<br>cd3="+cd3);
        }

        return sb.toString();
    }


}
