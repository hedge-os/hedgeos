package com.hedgeos.service.position.dao;

import com.hedgeio.trade.ExecutionReport;
import com.hedgeos.date.ProtoConverter;
import com.hedgeos.general.Price;
import com.hedgeos.general.Quantity;
import com.hedgeos.thread.Sleep;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * This Singleton Component, keeps two tables updated
 * 1.  execution_report
 * 2.  execution_report_last_fill_per_order
 *
 * The second table by a delete and then insert.  We can do this because,
 * we will run a system in the background to read a "slow channel", which will
 * be Kafka, where we reconcile the slow channel with this fast channel,
 * ensuring every fill which was placed eventually hits Kafka and then quickly
 * is checked against the database (within tens of seconds or less.)
 *
 * This way if the Position Service machine sets fire, we can
 * re-read from the tables and reconcile with what we receive from the system.
 *
 * Using the "fix_cum_qty", enhances this recovery,
 * by always getting the last known good quantity per order.
 *
 * TODO:  Have a slow channel reading from Kafka, which only increases the quantity
 * when the new quantity for an order is > the current quantity.
 *
 * TODO:  This slow channel can also reconcile between Kafka and the database,
 * inserting if it is not present, and moving the last_fill forward if need be,
 * via this class.
 *
 * By adding this slow Kafka listener, we can start up at any time and will
 * have the right position once the offset lag on Kafka goes to zero.
 *
 * TODO:  Report the Offset lag on the consumer post startup.
 * TODO:  And offer an option where we don't report the position until the offset
 * goes to zero post startup.  Perhaps this should be the default, KKutz?
 *
 * TODO:  Need to ignore if we see the same Execution Report twice.
 * TODO:  Need to ignore if we see the same Order twice.
 */
@Component
@Slf4j
public class ExecutionReportDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    // only the last fill per order (saved to a table by itself for speed on startup query)
    private final LinkedBlockingQueue<ExecutionReport> lastFillPerOrder = new LinkedBlockingQueue<>();

    // a log of every execution for every order
    private final LinkedBlockingQueue<ExecutionReport> execReportsPerOrder = new LinkedBlockingQueue<>();
    private final PositionJdbcConfig jdbcConfig;

    public ExecutionReportDao(PositionJdbcConfig config) {
        jdbcConfig = config;
        jdbcTemplate = new NamedParameterJdbcTemplate(config.positionDataSource());
        this.startSaveBackgroundThread();
    }

    public void startSaveBackgroundThread() {
        // save / overwrite, the last fill table
        Runnable r = () -> {
            //noinspection InfiniteLoopStatement
            while (true) {
                ExecutionReport er = null;
                try {
                    // first grab everything, and then only save the LATEST ER per order
                    List<ExecutionReport> currentDrainage = new ArrayList<>();
                    lastFillPerOrder.drainTo(currentDrainage);
                    // filter down to the latest exec per order, as there may be more than one
                    Map<Long, ExecutionReport> lastFillPerOrder = new TreeMap<>();
                    for (ExecutionReport current : currentDrainage) {
                        lastFillPerOrder.put(current.getHedgeOsOrderId(), current);
                    }

                    // in order of the OrderID, save the last Exec Report per order
                    for (ExecutionReport value : lastFillPerOrder.values()) {
                        saveLastExecPerOrder(value);
                    }

                }
                catch (Exception e) {
                    log.error("Exception saving Last Fill, fixExecId="+
                            (er != null ? er.getFixExecId() : "null"),e);
                    lastFillPerOrder.add(er); // try again, can't drop orders
                    // DB had a problem, slow down, hopefully things improve.
                    Sleep.sleep(5_000);
                }
            }
        };

        // spin a thread forever to keep the LAST EXEC updated
        new Thread(r, "Save Last Exec Thread").start();

        // save the entire exec log to a table
        Runnable r2 = () -> {
            //noinspection InfiniteLoopStatement
            while (true) {
                ExecutionReport er = null;
                try {
                    er = execReportsPerOrder.take();
                    saveExecReport(er);
                }
                catch (Exception e) {
                    log.error("Exception saving Last Fill, fixExecId="+
                            (er != null ? er.getFixExecId() : "null"),e);
                    lastFillPerOrder.add(er); // try again, can't drop orders
                    // DB had a problem, slow down, hopefully things improve.
                    Sleep.sleep(5_000);
                }
            }
        };

        // spin a thread forever to save EVERY EXEC to the DB
        new Thread(r2, "Save Every Exec Thread").start();
    }

    // we do a delete and an insert for the "last fill per order".
    // this could be in a transaction, except:
    // - increases database load.
    // - we don't expect these to ever fail or be blocked,
    // - and this is already in the background, memory is what is used!
    // ; also, mitigated by the fact, they can use the full execution log !
    public static final String DELETE_SQL =
            "delete from execution_report_last_fill_per_order " +
                    "where hedge_os_order_id = :hedge_os_order_id; ";

    public static final String INSERT_SQL_LAST_EXEC =
                    // need to delete and insert
                    "insert into execution_report_last_fill_per_order " +
                    "( hedge_os_order_id," +
                    " fix_last_shares," +
                    " fix_last_price," +
                    " fix_cum_qty," +
                    " fix_avg_price," +
                    " fix_last_spot_rate, " +
                    " fix_last_forward_points, " +
                    " create_time, " +
                    " create_user, " +
                    " last_update_time, " +
                    " last_update_user ) " +
                    " values ( " +
                    "  :hedge_os_order_id, " +
                    "  :fix_last_shares," +
                    "  :fix_last_price," +
                    "  :fix_cum_qty," + // important, what is used after reboot
                    "  :fix_avg_price," +
                    "  :fix_last_spot_rate," +
                    "  :fix_last_forward_points," +
                    "  :create_time, " +
                    "  :create_user, " +
                    "  :last_update_time," +
                    "  :last_update_user )";

    /**
     * Cache in another table, only the last execution for an order, for speed of startup.
     * @param er the last known execution report per order
     */
    private void saveLastExecPerOrder(ExecutionReport er) {

        // as this table is only the latest fill, to optimize for system start-up speed
        // we remove the former latest hedge_os_order_id and replace with the latest
        Map<String, Object> deleteMap = new HashMap<>();
        deleteMap.put("hedge_os_order_id", er.getHedgeOsOrderId());
        jdbcTemplate.execute(DELETE_SQL, deleteMap, new PreparedStatementCallback<Object>() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps) throws DataAccessException {
                try {
                    return ps.executeUpdate();
                }
                catch (Exception e) {
                    log.error("Very bad, exception in the delete of latest trans="+e, e);
                    throw new RuntimeException(e);
                }
            }
        });

        Map<String, Object> map = new HashMap<>();
        map.put("hedge_os_order_id", er.getHedgeOsOrderId());
        map.put("fix_last_shares", ProtoConverter.quantityToDouble(er.getLastShares()));
        map.put("fix_exec_id", er.getFixExecId());
        map.put("fix_last_price", ProtoConverter.priceToDouble(er.getLastPrice()));
        map.put("fix_cum_qty", ProtoConverter.quantityToDouble(er.getCumulativeQty())); // always a LONG
        map.put("fix_avg_price", ProtoConverter.priceToDouble(er.getAvgPrice()));
        map.put("fix_last_spot_rate", ProtoConverter.priceToDouble(er.getLastSpotRate()));
        map.put("fix_last_forward_points", ProtoConverter.priceToDouble(er.getLastForwardPoints()));
        // TODO:  Make these defaults
        map.put("create_time", new java.sql.Timestamp(System.currentTimeMillis()));
        map.put("last_update_time", new java.sql.Timestamp(System.currentTimeMillis()));
        map.put("last_update_user", "pos-service");

        jdbcTemplate.execute(INSERT_SQL_LAST_EXEC, map, new PreparedStatementCallback() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps)
                    throws DataAccessException {
                try {
                    return ps.executeUpdate();
                } catch (Exception e) {
                    // log.error will be configured to send emails
                    log.error("Exception running LAST FILL insert="+e, e);
                    throw new RuntimeException(e);
                }
            }
        });
    }


    public static final String INSERT_SQL_EVERY_EXEC =
            "insert into execution_report "+
                    " ( hedge_os_order_id, "+
                    " fix_exec_id, "+
                    " fix_incoming_seq_number, "+
                    " fix_outgoing_seq_number, "+
                    " fix_order_status, "+
                    " quantity, "+
                    " fix_last_shares, "+
                    " fix_last_price, "+
                    " fix_cum_qty, "+
                    " fix_avg_price, "+
                    " fix_last_spot_rate, "+
                    " fix_last_forward_points, "+
                    " create_time, "+
                    " create_user, "+
                    " last_update_time, "+
                    " last_update_user ) " +
                    "values ( "+
                    " :hedge_os_order_id, "+
                    " :fix_exec_id, "+
                    " :fix_incoming_seq_number, "+
                    " :fix_outgoing_seq_number, "+
                    " :fix_order_status, "+
                    " :quantity, "+
                    " :fix_last_shares, "+
                    " :fix_last_price, "+
                    " :fix_cum_qty, "+
                    " :fix_avg_price, "+
                    " :fix_last_spot_rate, "+
                    " :fix_last_forward_points, "+
                    " :create_time, "+
                    " :create_user, "+
                    " :last_update_time, "+
                    " :last_update_user ) ";

    private void saveExecReport(ExecutionReport er) {
        Map<String, Object> map = new HashMap<>();
        map.put("hedge_os_order_id", er.getHedgeOsOrderId());
        map.put("fix_exec_id", er.getFixExecId());
        map.put("fix_incoming_seq_number", er.getFixIncomingSeqNumber());
        map.put("fix_outgoing_seq_number", er.getFixOutgoingSeqNumber());
        map.put("fix_order_status", er.getOrderStatus().toString());
        map.put("fix_last_shares", ProtoConverter.quantityToDouble(er.getLastShares()));
        map.put("fix_last_price", ProtoConverter.priceToDouble(er.getLastPrice()));
        map.put("fix_cum_qty", ProtoConverter.quantityToDouble(er.getCumulativeQty())); // always a LONG
        map.put("fix_avg_price", ProtoConverter.priceToDouble(er.getAvgPrice()));
        map.put("fix_last_spot_rate", ProtoConverter.priceToDouble(er.getLastSpotRate()));
        map.put("fix_last_forward_points", ProtoConverter.priceToDouble(er.getLastForwardPoints()));

        // audit
        map.put("create_time", new java.sql.Timestamp(System.currentTimeMillis()));
        map.put("create_user", "pos-service");
        map.put("last_update_time", new java.sql.Timestamp(System.currentTimeMillis()));
        map.put("last_update_user", "pos-service");

        jdbcTemplate.execute(INSERT_SQL_EVERY_EXEC, map, new PreparedStatementCallback() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps)
                    throws DataAccessException {
                try {
                    return ps.executeUpdate();
                } catch (Exception e) {
                    // log.error will be configured to send emails
                    log.error("Exception running LAST FILL insert="+e, e);
                    throw new RuntimeException(e);
                }
            }
        });
    }

    // One iteration over the fills to make sure we only save the
    // last one in the last fill table, if there are many fills for an order in the message
    public void saveAll(List<ExecutionReport> executionReportsList) {

        // put them to one queue to save to this table
        // (last fill per order, which is where the quantity comes from on restart)
        this.lastFillPerOrder.addAll(executionReportsList);

        // Next, Background save every report, for logging
        this.execReportsPerOrder.addAll(executionReportsList);
    }


    // for loading the current fills into the system, to prevent duplicates on replays to this service
    // (in a catastrophic failure, can't reprocess the same fill twice)
    final String SELECT_FILLS_SEEN_TODAY =
            "select hedge_os_exec_id, " +
                    "hedge_os_order_id, " +
                    "fix_exec_id, " +
                    "fix_cum_qty, " +
                    "fix_avg_price " +
                    "from public.execution_report " +
                    "where create_time >= ? ";


    public List<ExecutionReport> getExecutionsSeenToday(long startTime) {
        long start = System.currentTimeMillis();

        List<ExecutionReport> execs = new ArrayList<>();

        try (Connection conn = jdbcConfig.positionDataSource().getConnection()) {

            PreparedStatement pst = conn.prepareStatement(SELECT_FILLS_SEEN_TODAY );

            pst.setTimestamp(1, new Timestamp(startTime));

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                ExecutionReport.Builder builder = ExecutionReport.newBuilder();
                builder.setFixExecId(rs.getString("fix_exec_id"));
                builder.setCumulativeQty(Quantity.newBuilder().setQuantity(rs.getLong("fix_cum_qty")));
                double price = rs.getDouble("fix_avg_price");
                // TODO: Must unit test
                Price priceProto = ProtoConverter.priceProtoFromDouble(price);
                builder.setAvgPrice(priceProto);

                execs.add(builder.build());
            }

            log.warn("Startup boot of exec reports took(ms)="+(System.currentTimeMillis()-start));
            return execs;
        }
        catch (Exception e) {
            log.error("Exception getting exeuctions seen today on startup="+e, e);
            throw new RuntimeException(e);
        }
    }


}
