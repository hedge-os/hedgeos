package com.hedgeos.service.position.sort;

import com.hedgeio.trade.Order;

public class HedgeOsOrderIdComparator implements java.util.Comparator<Order> {
    @Override
    public int compare(Order o1, Order o2) {
        if (o1.getHedgeOsOrderId() == o2.getHedgeOsOrderId())
            return 0;
        if (o1.getHedgeOsOrderId() < o2.getHedgeOsOrderId())
            return -1;
        return 1;
    }
}
