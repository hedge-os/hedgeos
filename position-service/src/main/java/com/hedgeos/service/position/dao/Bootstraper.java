package com.hedgeos.service.position.dao;

import com.hedgeio.trade.ExecutionReport;
import com.hedgeio.trade.Order;
import com.hedgeio.trade.OrderPositionXref;
import com.hedgeos.position.Position;
import com.hedgeos.service.position.PositionCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class Bootstraper {

    // TODO:  Threaded, load every Portfolio active in RefData, or if one Portfolio, only this ID
    public static int MOCK_PORTFOLIO_ID = -999;

    public Bootstraper(PositionDao positionDao,
                       OrderDao orderDao,
                       ExecutionReportDao execDao,
                       OrderPositionXrefDao xrefDao,
                       PositionCache positionCache)
    {
        try {
            long start = System.currentTimeMillis();

            List<Position> positions = positionDao.bootstrapPositionsFromDb(MOCK_PORTFOLIO_ID);
            for (Position position : positions) {
                log.warn("Position for -999 portfolio=" + position);
            }

            long twentyFourHoursBackMillis = -1 * 1000 * 60 * 60 * 24;

            long startTimeBootstrapQueries = System.currentTimeMillis() - twentyFourHoursBackMillis;

            List<OrderPositionXref> xrefs = xrefDao.getOrderPositionXrefsSeenToday(startTimeBootstrapQueries);

            List<Order> ordersSeenToday = orderDao.getOrdersSeenToday(startTimeBootstrapQueries, xrefs);
            // this will get the execs for every order, since they are created after the order
            List<ExecutionReport> execsSeenToday = execDao.getExecutionsSeenToday(startTimeBootstrapQueries);


            positionCache.bootstrapQuantities(positions);
            positionCache.bootstrapOrders(ordersSeenToday);
            positionCache.bootstrapExecsSeenToday(execsSeenToday);

            log.warn("Full Position bootstrap took(ms)="+(System.currentTimeMillis()-start));
        }
        catch (Exception e) {
            log.error("Exception booting from Position DB="+e, e);
            throw new RuntimeException("Exception booting fro Position DB "+e, e);
        }
    }
}
