package com.hedgeos.service.position.dao;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@EnableConfigurationProperties
@Configuration
@Slf4j
public class PositionJdbcConfig {

    @Autowired
    private Environment environment;

    // much talk on the web about not needing this - but this is only for the JPA usage
    // for standard spring JDBC usage, you need to pull in the properties like this... explicitly

    // NOTE:  NOTE A BEAN, we don't want this to fulfill the JPA data source contract
    @Bean
    public DataSource positionDataSource() {
        String posUrl = environment.getProperty("spring.position-datasource.url");
        log.warn("PosUrl="+posUrl);
        if (posUrl == null) {
            log.error("spring.position-datasource.url is NULL");
            throw new RuntimeException("spring.position-datasource.url is NULL");
        }

        return DataSourceBuilder.create()
                .driverClassName(environment.getProperty("spring.position-datasource.driverClassName"))
                .url(posUrl)
                .username(environment.getProperty("spring.position-datasource.username"))
                .password(environment.getProperty("spring.position-datasource.password"))
                .type(HikariDataSource.class)
                .build();
    }

}
