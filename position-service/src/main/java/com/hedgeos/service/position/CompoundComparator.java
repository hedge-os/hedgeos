package com.hedgeos.service.position;

import com.hedgeio.trade.Order;

import java.util.Comparator;
import java.util.List;

public class CompoundComparator implements Comparator<Order> {
    private final List<Comparator<Order>> orderComparators;

    public CompoundComparator(List<Comparator<Order>> orderComparators) {
        this.orderComparators = orderComparators;
    }

    @Override
    public int compare(Order o1, Order o2) {

        if (o1 == o2) // object optimization
            return 0;

        if (o1.getHedgeOsOrderId() == o2.getHedgeOsOrderId())
            return 0; // business object optimization

        int currentCompare = 0;

        for (Comparator<Order> orderComparator : orderComparators) {
            // run the first, then the second comparator
            currentCompare = orderComparator.compare(o1, o2);
            if (currentCompare != 0) { // if not equal, you are done
                return currentCompare;
            }
            // if they are equal, continue, and down the chain is the result
        }

        // if everything compared, they match
        return 0;
    }
}
