package com.hedgeos.service.position.cache;

import com.hedgeio.trade.Order;
import com.hedgeos.position.PositionMeta;
import com.hedgeos.service.position.PositionMetaResolved;
import com.hedgeos.ui.api.hedgeos.UiOrder;

public interface MetaStringCache {

    PositionMetaResolved getPositionMetaStrings(PositionMeta positionMeta);

    void setOrderMetaStrings(UiOrder uiOrder, Order order);
}
