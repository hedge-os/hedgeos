package com.hedgeos.service.position;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.hedgeio.trade.ExecutionReport;
import com.hedgeio.trade.Order;
import com.hedgeio.trade.OrderPlacedRequest;
import com.hedgeos.service.position.cache.MetaStringCache;
import com.hedgeos.service.position.sort.HedgeOsOrderIdComparator;
import com.hedgeos.service.position.sort.PortfolioOrderComparator;
import lombok.extern.slf4j.Slf4j;
import org.roaringbitmap.RoaringBitmap;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.util.*;
import java.util.concurrent.*;

import static com.hedgeos.service.position.ColumnNames.*;

@Component
@Slf4j
public class OrderCache {

    volatile RoaringBitmap totalOrdersLoaded = new RoaringBitmap();
    ConcurrentSkipListMap<Long, Order> totalOrdersLoadedMap = new ConcurrentSkipListMap<>();

    // for keeping ordered copies of the data
    ConcurrentSkipListMap<Order, Order> portfolioSortedOrderMap = new ConcurrentSkipListMap<>(new PortfolioOrderComparator());
    ConcurrentSkipListMap<Order, Order> orderIdSortedMap = new ConcurrentSkipListMap<>(new HedgeOsOrderIdComparator());

    // for computing matches
    // TODO:  This should also do sorting; mental gynmastics to follow.
    RoaringBitmapTrie portfolioTrie = new RoaringBitmapTrie();
    RoaringBitmapTrie orderIdTrie = new RoaringBitmapTrie();
    RoaringBitmapTrie strategyTrie = new RoaringBitmapTrie();
    RoaringBitmapTrie bbTickerTrie = new RoaringBitmapTrie();
    RoaringBitmapTrie priceTrie = new RoaringBitmapTrie();
    RoaringBitmapTrie orderStatusTrie = new RoaringBitmapTrie();

    public final MetaStringCache metaStringCache;

    // the entire list of fills, for display by the Trade Blotter app
    // (only the last one is needed for position keeping, "fix_cum_qty" = latest order qty)
    final Map<Long, List<ExecutionReport>> orderToExecutions = new ConcurrentHashMap<>();

    @VisibleForTesting
    public final BlockingQueue<Order> ordersToAdd = new LinkedBlockingQueue<>();

    // try to keep this more of a normal Data Structure, more easily tested; not a Spring or JPA thing
    public OrderCache(MetaStringCache metaStringCache)
    {
        this.metaStringCache = metaStringCache;

        Runnable r = () -> {
            while (true) {
                Order order = null;
                try {
                    order = ordersToAdd.take();

                    populateTriesFromOrder(order);
                }
                catch (Exception e) {
                    log.error("Exception adding order="+order+", e="+e, e);
                }
            }
        };

        // spin forever adding orders
        new Thread(r, "Order cache populate thread").start();
    }

    /**
     * Heavy lifting, slow; done in the background;
     * (this is only for the auto-type = eventual consistency)
     **/
    private void populateTriesFromOrder(Order order) {

        // add to our total orders added
        totalOrdersLoaded.add((int) order.getHedgeOsOrderId());
        totalOrdersLoadedMap.put(order.getHedgeOsOrderId(), order);


        PositionMetaResolved metaStrings = metaStringCache.getPositionMetaStrings(order.getPositionMeta());

        // This is only for sorting; we need to keep everything pre-sorted to quickly fetch it sorted

        // the CHMs are sorted
        // TODO:   will keep either the CSLMs or the Concurrent Linked Lists
        portfolioSortedOrderMap.put(order, order);
        orderIdSortedMap.put(order, order);

        // add to the Tries
        portfolioTrie.put(metaStrings.portoflio, (int) order.getHedgeOsOrderId());
        orderIdTrie.put(""+order.getHedgeOsOrderId(), (int) order.getHedgeOsOrderId());
        strategyTrie.put(metaStrings.strategy, (int) order.getHedgeOsOrderId());
    }

    /**
     * This is the entry point to retrieve orders with Prefix matching.
     * (TODO:  expand to infix matches, other functions like not in etc.)
     * TODO:  Could be slightly more efficient dealing with sorting, but is very quick.
     *
     * 1.  Filter on the matches
     * 2.  Run across the first selected sort column, grabbing matches (this way avoiding sorting a million elements)
     *      - e.g. O(n) because pre-sorted instead of O(n*log(n)) removing a log on it.
     * 3.  Then sort the matches by any additional columns specified.
     *
     *
     * @param window how many rows to return
     * @param offset how far shifted in the results
     * @param columnToPrefiMap what prefixes were requested for what columns
     * @return A count size list of matching orders which
     * are "offset" deep into the matches.
     */
    
    public List<Order> getMatchingOrders(List<SortCol> sortCols, int window, int offset, Map<String, String> columnToPrefiMap) {

        // TODO:  Can make a function here
        RoaringBitmap completeMatches = null;

        // no prefixes, use the entire set of matches
        if (columnToPrefiMap == null || columnToPrefiMap.isEmpty()) {
            completeMatches = new RoaringBitmap();
            //
            completeMatches.or(totalOrdersLoaded);
        }
        else {
            // doesn't need to be a sorted map, but doesn't hurt
            SortedMap<String, RoaringBitmap> matchesPerColumn = locateMatchesPerColumn(columnToPrefiMap);

            for (RoaringBitmap value : matchesPerColumn.values()) {
                if (completeMatches == null)
                    completeMatches = value;
                else
                    completeMatches.and(value); // TODO:  Support 'OR' (this is AND on every column)
            }
        }

        if (completeMatches == null) {
            log.warn("No matches for query");
            return Lists.newArrayList();
        }

        if (sortCols.isEmpty()) {
            throw new RuntimeException("Need at least one sort col");
        }

        // get the first sort col and calculate the first "window" size matches
        // this is the only "expensive thing we do" (should be sub 100ms)
        // This can use the sorted LinkedLists, test the speed of both (tested, they are similar)
        SortCol firstSortCol = sortCols.iterator().next();

        List<Order> sortedOrderResults = getOneColumnSortedResults(firstSortCol, completeMatches);

        // if no results we are finished, could be nothing loaded, could also be no matches
        if (sortedOrderResults.size() == 0) {
            return new ArrayList<>();
        }

        // if there are more than one comparator (usually will be),
        // we need to then, compound sort
        if (sortCols.size() > 1) {
            // make a list of comparators
            List<Comparator<Order>> comparatorList = new ArrayList<>();

            // add a comparator for each sort col in the order they are defined
            for (SortCol col : sortCols) {
                Comparator<Order> comparator = getComparatorForColumn(col);
                if (col.sortOrder == SortOrder.DESCENDING) {
                    // this reverses the result if requested
                    comparator = new RerversedOrder(comparator);
                }

                comparatorList.add(comparator);
            }

            // sort with my funky compound comparator (funky is a technical term)
            CompoundComparator compoundComparator = new CompoundComparator(comparatorList);

            sortedOrderResults.sort(compoundComparator);
        }

        // grab the window, offset deep into the results (if the scroll bar was moved.)
        int startResults = offset;
        if (offset >= sortedOrderResults.size())
            startResults = sortedOrderResults.size()-1;

        int endResults = offset + window;
        // TODO:  hack for demo
        endResults = Math.abs(endResults);

        if (endResults >= sortedOrderResults.size())
            endResults = sortedOrderResults.size()-1;


        List<Order> results = sortedOrderResults.subList(startResults, endResults);

        return results;
    }

    /**
     * TODO:  This can be threaded out (if rows > 2_000_000), and use Agrona for a threaded collection result
     *
     * @param sortCol the cols we want to sort on (first one is used)
     * @param completeMatches the matches computed using the Tries and Roaring Bitmaps
     * @return a sorted list of orders by the sortCol column and direction
     */
    private List<Order> getOneColumnSortedResults(SortCol sortCol, RoaringBitmap completeMatches) {

        List<Order> sortedOrderResults = new ArrayList<>();
        ConcurrentSkipListMap<Order, Order> sortedOrderMap = getSortedMapForColumn(sortCol.column);

        // decide which way to iterate this
        Iterator<Order> sortedIterator = null;

        if (sortCol.sortOrder == SortOrder.ASCENDING)
            sortedIterator = sortedOrderMap.navigableKeySet().iterator();
        else
            sortedIterator = sortedOrderMap.descendingKeySet().iterator();

        // TODO:  Even better, can get the first column in order and see if windows size matches are the same,
        // TODO: etc, etc, but not needed because it already flies.

        // TODO: Agrona + threading here, break up the loop

        while (sortedIterator.hasNext()) {
            Order order = sortedIterator.next();
            if (completeMatches.contains((int) order.getHedgeOsOrderId())) {
                sortedOrderResults.add(order);
            }
        }

        return sortedOrderResults;
    }

    private Comparator<Order> getComparatorForColumn(SortCol sortCol) {
        switch (sortCol.column) {
            case PORTOFLIO_COL:
                return new PortfolioOrderComparator();
            case HEDGE_OS_ORDER_ID:
                return new HedgeOsOrderIdComparator();
            default:
                throw new RuntimeException("Can't compare, need to add comparator for column="+sortCol.column);
        }
    }

    private SortedMap<String, RoaringBitmap> locateMatchesPerColumn(Map<String, String> columnToPrefixMap) {

        // no need for this to be a TreeMap but doesn't hurt
        SortedMap<String, RoaringBitmap> matchesPerColumn = new TreeMap<>();

        for (Map.Entry<String, String> colToPrefixEntry : columnToPrefixMap.entrySet()) {
            String columnName = colToPrefixEntry.getKey();
            String prefix = colToPrefixEntry.getValue();

            RoaringBitmapTrie trie = getTrieForColumn(columnName);
            RoaringBitmap matches = trie.get(prefix);
            matchesPerColumn.put(columnName, matches);
        }

        return matchesPerColumn;
    }

    private ConcurrentSkipListMap<Order, Order> getSortedMapForColumn(String column) {
        switch (column) {
            case PORTOFLIO_COL:
                return portfolioSortedOrderMap;
            case HEDGE_OS_ORDER_ID:
                return orderIdSortedMap;
            default:
                throw new RuntimeException("Must add sort column="+column);
        }
    }

    private RoaringBitmapTrie getTrieForColumn(String colName) {
        switch (colName) {
            case PORTOFLIO_COL:
                return portfolioTrie;
            case LIMIT_PRICE_COL:
                return priceTrie;
            default:
                throw new RuntimeException("Must add col to trie map="+colName);
        }
    }

    public void addOrUpdateOrder(OrderPlacedRequest request) {
        // don't do any work, will slow down new order creation
        // simply add to a Queue, and the (small) work is done in the background
        ordersToAdd.addAll(request.getOrdersList());
    }

    public void addExecutionReport(ExecutionReport executionReport) {
        // get it on the orders queue for display
        if (!orderToExecutions.containsKey(executionReport.getHedgeOsOrderId())) {
            // this should always be here by way of the order having been placed
            log.error("No prior order set-up for fill="+executionReport.getHedgeOsOrderId());
            orderToExecutions.put(executionReport.getHedgeOsOrderId(), Collections.synchronizedList(new ArrayList<>()));
        }

        // add it to the list of executions for this order
        orderToExecutions.get(executionReport.getHedgeOsOrderId()).add(executionReport);
    }

    public int size() {
        return orderIdSortedMap.size();
    }

}
