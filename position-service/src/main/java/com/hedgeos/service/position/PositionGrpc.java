package com.hedgeos.service.position;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Small and simple wrapper around gRPC allowing this server to be injected elsewhere
 */
@Component
@RequiredArgsConstructor
public class PositionGrpc {

    // @RequiredArgsConstructor from Lombok, sets this for us.
    // injection allows referencing the same server from Rest controller
    public final PositionServer positionServer;

    @Value("${server.grpcPort}")
    public int grpcPort;

    public void startGrpc() throws IOException {

        // bind to the server.grpcPort in application.yml
        // start hte grpc server "RouterServer" which Spring Boot has injected for us above
        final Server server = ServerBuilder.forPort(grpcPort)
                .addService(positionServer)
                // .addService(ProtoReflectionService.newInstance())
                // .addService(health.getHealthService())
                .build()
                .start();

        System.out.println("********");
        System.out.println("   PositionGRPC is LISTENING, gRPC port --> " + grpcPort);
        System.out.println("********");

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Start graceful shutdown
                server.shutdown();
                try {
                    // Wait for RPCs to complete processing
                    if (!server.awaitTermination(30, TimeUnit.SECONDS)) {
                        // That was plenty of time. Let's cancel the remaining RPCs
                        server.shutdownNow();
                        // shutdownNow isn't instantaneous, so give a bit of time to clean resources up
                        // gracefully. Normally this will be well under a second.
                        server.awaitTermination(5, TimeUnit.SECONDS);
                    }
                } catch (InterruptedException ex) {
                    server.shutdownNow();
                }
            }
        });

        // This would normally be tied to the service's dependencies. For example, if HostnameGreeter
        // used a Channel to contact a required service, then when 'channel.getState() ==
        // TRANSIENT_FAILURE' we'd want to set NOT_SERVING. But HostnameGreeter has no dependencies, so
        // hard-coding SERVING is appropriate.
        // health.setStatus("", ServingStatus.SERVING);


        // inside spring web, don't need to wait
        // server.awaitTermination();
    }

}
