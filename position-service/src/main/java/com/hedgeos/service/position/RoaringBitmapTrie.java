package com.hedgeos.service.position;

import org.roaringbitmap.RoaringBitmap;
import org.springframework.util.Assert;

import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RoaringBitmapTrie {

    ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public static class Node {
        // TODO:  Not clear with the lock if this needs to be a CHM
        volatile boolean isValue = false;
        RoaringBitmap rb = new RoaringBitmap();
        // TODO:  Not clear with the lock if this needs to be a CHM
        ConcurrentHashMap<String, Node> children = new ConcurrentHashMap<>();
    }

    Node root = new Node();

    public void put(String key, int value) {
        key = key.toLowerCase(Locale.ROOT); // is a case insensitive Trie


        // root.rb.add(bitToFlip);

//        if (key.length() == 0) {
//            root.isValue = true;
//            return;
//        }

        String firstChar = ""+key.charAt(0);
        Node child = root.children.get(firstChar);
        if (child == null) {
            child = new Node();
            root.children.put(firstChar, child);
        }

        put(key.substring(1), value, child);
    }

    public void put(String key, int value, Node node) {
        if (key.length() == 0) {
            node.isValue = true;
            node.rb.add(value);
            return;
        }

        key = key.toLowerCase(Locale.ROOT);

        // flip the roaring bitmap in the lock
        boolean locked = lock.writeLock().tryLock();
        if (!locked) {
            while (!locked)
                locked = lock.writeLock().tryLock();
        }

        node.rb.add(value);
        lock.writeLock().unlock();

        // we have a child
        String oneChar = ""+key.charAt(0);
        Node child = node.children.get(oneChar);
        if (child == null) {
            // make a new node and store it
            child = new Node();
            node.children.put(oneChar, child);
        }

        // do the rest of the work on the child
        put(key.substring(1), value, child);
    }

    public RoaringBitmap get(String key) {
        if (key == null)
            return null;
        // notice we go to lower case
        key = key.toLowerCase(Locale.ROOT);
        return get(key, root);
    }

    public RoaringBitmap get(String key, Node node) {
        if (key.length() == 0)
            return null;

        // TODO:  Maybe want to return a copy of the RB
        // TODO: TO avoid mutating it by mistake, which we will do..
        if (key.length() == 1) {
            Node ret = node.children.get(key);
            if (ret == null)
                return null;

            return ret.rb;
        }

        Node firstCharChild = node.children.get(""+key.charAt(0));
        if (firstCharChild == null) {
            return null; // no matches!
        }

        return get(key.substring(1), firstCharChild);
    }

}
