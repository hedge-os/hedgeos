package com.hedgeos.service.cache;

import com.hedgeos.service.position.config.PositionCacheConfiguration;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;


@SpringBootApplication(scanBasePackageClasses =
        { PositionCacheConfiguration.class, HikariDataSource.class })
@RequiredArgsConstructor
public class HazelcastCacheTest  implements CommandLineRunner {

    final CacheManager cacheManager;

    public void printAllCache(){

        for (String s : cacheManager.getCacheNames()) {
            System.out.printf("Cache="+s);
            Cache cache = cacheManager.getCache(s);
            System.out.println("Cache="+cache.getName());
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(HazelcastCacheTest.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.setProperty("spring.datasource.driver-class-name","com.postgres.jdbc.Driver");
        System.setProperty("url", "http://localhost:5532");
        printAllCache();
    }
}
