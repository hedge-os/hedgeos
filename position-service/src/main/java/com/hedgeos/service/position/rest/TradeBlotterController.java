package com.hedgeos.service.position.rest;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.hedgeio.trade.Order;
import com.hedgeos.service.position.ColumnNames;
import com.hedgeos.service.position.OrderCache;
import com.hedgeos.service.position.SortCol;
import com.hedgeos.service.position.cache.MetaStringCache;
import com.hedgeos.ui.api.aggrid.ColumnFilter;
import com.hedgeos.ui.api.aggrid.DataResult;
import com.hedgeos.ui.api.aggrid.ServerSideGetRowsRequest;
import com.hedgeos.ui.api.aggrid.SortModel;
import com.hedgeos.ui.api.hedgeos.UiOrder;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/order")
public class TradeBlotterController {

    // has the orders and the auto-complete
    final OrderCache orderCache;
    // knows how to get the string field names from the Orders
    final MetaStringCache metaStringCache;

    @GetMapping("testGet")
    public String testGet() {
        return "works";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/filterOrdersAgGrid")
    public ResponseEntity<String> filterOrdersString(@RequestBody String request) {
        ServerSideGetRowsRequest ssr = new Gson().fromJson(request, ServerSideGetRowsRequest.class);
        return filterOrdersAgModel(ssr);
    }

    @SneakyThrows
    @RequestMapping(method = RequestMethod.POST, value = "/filterOrdersAgGridModel")
    public ResponseEntity<String> filterOrdersAgModel(@RequestBody ServerSideGetRowsRequest request) {
        long start = System.currentTimeMillis();
        TradeBlotterRequest tbr = new TradeBlotterRequest();
        tbr.offset = request.getStartRow();
        tbr.window = request.getStartRow() - request.getEndRow(); // TODO obob?

        tbr.sortCols = new ArrayList<>();
        // default to order id descending
        if (request.getSortModel() == null || request.getSortModel().isEmpty()) {
            tbr.sortCols.add(new SortCol(ColumnNames.HEDGE_OS_ORDER_ID, SortOrder.DESCENDING));
        } else {
            for (SortModel sortModel : request.getSortModel()) {
                SortOrder sortOrder = SortOrder.ASCENDING;
                if (sortModel.getSort().toLowerCase(Locale.ROOT).contains("desc")) {
                    sortOrder = SortOrder.DESCENDING;
                }

                String col = ColumnNames.HEDGE_OS_ORDER_ID;
                if (sortModel.getColId().toLowerCase(Locale.ROOT).contains("port")) {
                    col = ColumnNames.PORTOFLIO_COL;
                }

                if (sortModel.getColId().toLowerCase(Locale.ROOT).startsWith("price")) {
                    col = ColumnNames.LIMIT_PRICE_COL;
                }

                // TODO:  The other columns..

                // add it to our translated columns
                tbr.sortCols.add(new SortCol(col, sortOrder));
            }
        }

        if (request.getFilterModel() != null && !request.getFilterModel().isEmpty()) {
            tbr.columnToPrefiMap = new HashMap<>();
            for (String column : request.getFilterModel().keySet()) {
                ColumnFilter cf = request.getFilterModel().get(column);
                String filterCol = null;

                // TODO:  make this generic and good
                if (column.toLowerCase(Locale.ROOT).contains("order")) {
                    filterCol = ColumnNames.HEDGE_OS_ORDER_ID;
                }
                else if (column.toLowerCase(Locale.ROOT).contains("port")) {
                    filterCol = ColumnNames.PORTOFLIO_COL;
                }
                else if (column.toLowerCase(Locale.ROOT).startsWith("price")) {
                    filterCol = ColumnNames.LIMIT_PRICE_COL;
                }
                else if (column.toLowerCase(Locale.ROOT).startsWith("strategy")) {
                    filterCol = ColumnNames.STRATEGY;
                }

                // TODO:  Handle numbers, AND/OR conditions, and "Contains"
                tbr.columnToPrefiMap.put(filterCol, ""+cf.getFilter());
            }
        }

        log.info("Start of request orders=" + tbr.requestId);
        List<Order> orders = orderCache.getMatchingOrders(tbr.sortCols, tbr.window, tbr.offset, tbr.columnToPrefiMap);
        log.info("took(ms)=" + (System.currentTimeMillis() - start) + ", Finished order request, ID=" + tbr.requestId);

        // TODO: Translate to Front End orders w/ Reference Data + Security Data
        // Question, should I do this, when I add to the Trie?
        // hmm, doesn't handle a re-name once data is loaded.

        List<String> jsonRows = new ArrayList<>();

        // put this back to test without druid locally
//        for (DruidResultMock mockPosition : mockPositions) {
//            jsonRows.add(new Gson().toJson(mockPosition));
//        }

        // turn each row into JSON
        // TODO: improve this, but is only 200 records
        for (Order order : orders) {
            UiOrder uiOrder = new UiOrder(order);
            // get the strings for the order and set on the the UI Order
            metaStringCache.setOrderMetaStrings(uiOrder, order);
            jsonRows.add(new Gson().toJson(uiOrder));
        }

        DataResult dataResult = new DataResult(jsonRows, orders.size(), new ArrayList<>());
        return new ResponseEntity<>(DataResult.asJsonResponse(dataResult), HttpStatus.OK);


    }


    @GetMapping("/filterOrders")
    public List<Order> getMatchingOrders (TradeBlotterRequest tbr)
    {
        long start = System.currentTimeMillis();
        log.info("Start of request orders=" + tbr.requestId);
        List<Order> orders = orderCache.getMatchingOrders(tbr.sortCols, tbr.window, tbr.offset, tbr.columnToPrefiMap);
        log.info("took(ms)=" + (System.currentTimeMillis() - start) + ", Finished order request, ID=" + tbr.requestId);
        return orders;
    }

    @GetMapping("/test")
    public String testOrderBlotter ()
    {
        TradeBlotterRequest tbr = new TradeBlotterRequest();
        tbr.window = 200;
        tbr.offset = 100;
        tbr.columnToPrefiMap = new HashMap<>();
        tbr.columnToPrefiMap.put(ColumnNames.PORTOFLIO_COL, "c"); // this is from the fake data, chips and another
        tbr.sortCols = Lists.newArrayList(new SortCol(ColumnNames.HEDGE_OS_ORDER_ID, SortOrder.DESCENDING));

        long start = System.currentTimeMillis();

        log.info("Start of request orders=" + tbr.requestId);
        List<Order> orders = orderCache.getMatchingOrders(tbr.sortCols, tbr.window, tbr.offset, tbr.columnToPrefiMap);

        log.info("took(ms)=" + (System.currentTimeMillis() - start) + ", Finished order request, ID=" + tbr.requestId);

        return new Gson().toJson(orders);
    }


}
