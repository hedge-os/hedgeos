package com.hedgeos.service.position;

public class ColumnNames {
    public static final String PORTOFLIO_COL = "PORTFOLIO";
    public static final String HEDGE_OS_ORDER_ID = "HEDGE_OS_ORDER_ID";

    public static final String LIMIT_PRICE_COL = "LIMIT_PRICE";

    public static final String STRATEGY = "STRATEGY";
}
