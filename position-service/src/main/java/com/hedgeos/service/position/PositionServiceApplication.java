package com.hedgeos.service.position;

import com.hedgeos.service.refdata.config.audit.AuditEventConverter;
import com.hedgeos.service.refdata.domain.CurveDefinition;
import com.hedgeos.service.refdata.jpa.RefdataDatabaseConfiguration;
import com.hedgeos.service.refdata.repository.CurveDefinitionRepository;
import com.hedgeos.service.security.jpa.SecurityJpaDatabaseConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Note, we need to scan in the Repositories from RefData here
 * Because we are scanning, need to scan this class (and then sub-packages are scanned)
 * Everything with Spring scanning is package based, and is recursive!
 */
@EnableCaching
@SpringBootApplication(scanBasePackageClasses = {
		PositionServiceApplication.class,
		CurveDefinition.class,
		CurveDefinitionRepository.class,
		AuditEventConverter.class,
		RefdataDatabaseConfiguration.class,
		SecurityJpaDatabaseConfig.class
	}
)

@EnableSwagger2
public class PositionServiceApplication implements CommandLineRunner {

	@Autowired
	PositionGrpc positionGrpc;

	@Autowired
	ApplicationContext applicationContext;

	@Override
	public void run(String... args) throws Exception {

		positionGrpc.startGrpc();

	}

	public static void main(String[] args) {
		SpringApplication.run(PositionServiceApplication.class, args);
	}

}
