package com.hedgeos.service.position.rest;

import com.hedgeio.trade.Order;
import com.hedgeio.trade.OrderPlacedRequest;
import com.hedgeos.service.position.MockOrderGenerator;
import com.hedgeos.service.position.OrderCache;
import com.hedgeos.thread.Sleep;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * For loading up fake orders to performance test the trade blotter code front to back.
 * (can be removed after we go live + demos.)
 */
@Slf4j
@RestController
@RequestMapping("/mock")
public class MockStarter {

    public final OrderCache orderCache;

    public MockStarter(OrderCache orderCache) {
        this.orderCache = orderCache;
        // TODO: remove, is adding fake orders
        // todo, don't wait on startup
        this.mockTwoMillionOrders(false);
    }

    @GetMapping("/startMock")
    public String go() {
        // on this one wait for the order to be created
        return mockTwoMillionOrders(true);
    }

    public String mockTwoMillionOrders(boolean sleep)
    {
        long start = System.currentTimeMillis();

        int size = 1_000;
        try {
            List<Order.Builder> mockOrders = MockOrderGenerator.generateOrders(size);

            for (Order.Builder mockOrder : mockOrders) {
                OrderPlacedRequest opr =
                        OrderPlacedRequest.newBuilder().addOrders(mockOrder).build();
                orderCache.addOrUpdateOrder(opr);
            }

            if (sleep) {
                // need to wait for the cache to clear as it is in the background
                while (orderCache.ordersToAdd.size() > 0) {
                    Sleep.sleepSec(1);
                    System.out.println("Sleeping for size=" + orderCache.ordersToAdd.size());
                }
            }

            long took = (System.currentTimeMillis()-start);
            System.out.println("Loaded orders, " +
                    ", two mil count="+ orderCache.size()+
                    ", took(ms)="+took);

            return "loaded orders="+size+", took(ms)"+took;
        }
        catch (Exception e) {
            log.error("Exception mocking orders", e);
            throw new RuntimeException(e);
        }
    }
}
