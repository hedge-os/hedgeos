package com.hedgeos.service.position.sort;

import com.hedgeio.trade.Order;
import com.hedgeos.service.position.SortCol;

public class PortfolioOrderComparator implements java.util.Comparator<com.hedgeio.trade.Order>
{

    @Override
    public int compare(Order o1, Order o2) {
        if (o1.getHedgeOsOrderId() == o2.getHedgeOsOrderId())
            return 0;

        // TODO: Implement by checking portfolio first on the order
        // TODO: Still need RefData sadly
        return -1;
    }
}
