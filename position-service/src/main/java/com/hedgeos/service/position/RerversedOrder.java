package com.hedgeos.service.position;

import com.hedgeio.trade.Order;

import java.util.Comparator;

public class RerversedOrder implements Comparator<Order> {
    private final Comparator<Order> compare;

    public RerversedOrder(Comparator<Order> comparator) {
        this.compare = comparator;
    }

    @Override
    public int compare(Order o1, Order o2) {
        return -1 * (compare.compare(o1, o2));
    }
}
