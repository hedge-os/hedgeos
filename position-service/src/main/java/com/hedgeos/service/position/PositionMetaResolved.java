package com.hedgeos.service.position;

import lombok.Data;

@Data
public class PositionMetaResolved {
    String portoflio;
    String strategy;
    String bbTicker;
}
