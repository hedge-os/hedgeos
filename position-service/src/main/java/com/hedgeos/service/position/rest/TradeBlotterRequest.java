package com.hedgeos.service.position.rest;

import com.hedgeos.service.position.SortCol;

import java.util.List;
import java.util.Map;

public class TradeBlotterRequest {

    // e.g. fetch 200 rows, from 0 offset (the beginning of the order blotter)
    // e.g. fetch 250 rows from 100 offset (scroll bar has been moved.)
    public int window;
    public int offset;
    // (TODO: handle complex filter expressions etc.)
    public Map<String, String> columnToPrefiMap; // map of what is being filtered
    // what coluns to sort on an in what direction (typically no more than 2 sort columns)
    public List<SortCol> sortCols;

    // for tracking, not used
    public String requestId;
}
