Position Service
----

### Description

Position Service maintains in real-time and at machine speed in memory, the holdings of the fund.

The Position Service ("position-service"), is responsible for maintaining, (in real time),
the current quantity of everything held by the fund, grouped by Security and "Portfolio Meta".

Portfolio Meta defines which Portfolio, Strategy, Sub-Strategy etc, hold the quantity of the Security.

Other services query or subscribe to Position Service to be notified of the current quantity and changes (via gRPC and Protobufs.)

### Summary Flow

- Position Service receives Orders and Execution Reports from your EMS or our EMS Bridge Service reading from your EMS.
- The quantity is then updated in transactional memory, in a map internally at machine speed (Java.)
- In the background, the activity is stored to the Position keeping database, for restart of the service
- This database is used to bootsratp the system, read on startup doing a "sum(fix_cum_qty), position_id" to bootstrap the current position,
- Once running, in memory updates continually update the position, with database writes in the background

![Flow Diagram](docs/OrderFlowHedgeOs.png)

### Key entities defined (and database table)
1.  Position Meta (position_meta):  Defines a position_id for a position which is unique globally.  
    This position is then held by a portfolio, strategy, sub-strategy, etc
1.  Portfolio Trade (portfolio_trade): a *desired* position a PM or desk wants to take
1.  Order (order_oms):  an Order which is sent to achieve all or part of a Portfolio Trade
1.  Execution Report (execution_report):  Notice of an event on an Order like Filled, Cancelled etc.
1.  Order Position Xref (order_position_xref):  Maps an Order into a Position

![DB Design](docs/PositionService-DB-Design.png)

### Notes
1. Position Meta defines a position_id, and this entity is Bi-temporal, meaning, the position can be moved and the table records "valid_from",
"valid_to".
1.  The Orders are for a position_id, and a security id (sid.)
1.  This allows Orders to be run for Securities; however, they can be mapped into any position,
    either at the time the Order is placed, or later by Allocation logic

#### Allocation

1.  Allocations - What if an order is booked and then needs to be split?
    - Execute the Order into an "Execution Desk" portfolio
    - then allocation logic runs to map it into a Portfolio
1.  This can be an order to an Execution Desk and then an Allocation
1.  This is achieved by making orders which are Allocation type, and authoring an Allocator, or using ours

### High Uptime and Disaster Recovery

An astute observer notes the writes to the database occur in the background.
If the machine or program "crashes" (java doesn't crash though, machines can and will crash),
the system needs a way of knowing the Position doesn't reflect the update.

The decision is for the system to be fast, 
and not wait for persistence to disk or otherwise,
when performing the update.  

"Recovery" from this situation, is handled by a replay from Kafka,
post-crash.

If the Position Service crashes, the position server can be restart 
(or the alternate DR service, which is listening to Kafka in real time,
can be used in an automated failover.)

By writing to Kafka in a "slow" persistent channel, with a cluster (Kafka)
we can be guaranteed the message is persisted, and can be 
recovered by the position service on a restart.  
Because we track a unique ID for every Execution Report,
we can play the messages again off of Kafka, after loading
what we have already stored in the databse, and "de-duplicate" (de-dup)
whatever messages we have already seen from the Kafka feed.

Then, we have seen every message published, and can 
continue working off of the existing feed.

(Note: Kafka is slow, on the order of milliseconds
single digit for messages compared to sub-single digit millis for gRPC and protos.
Kafka is "fast" as far as systems and code go, but is
slow in a trading system context.  This is why we go to the trouble of
gRPC + Proto, fast paths.  Kafka is an outstanding accomplishment,
but not ideal for sending trading messages to a position keeping system.)

Each event has a unique "hedge_os_order_id" for Orders, or "hedge_os_exec_id" for Execution Reports.

On start-up, the Position Service will keep track of which of these we have seen in a Map.

When a replay from startup occurs, each event will be checked to see if it has been recorded previously,
and if it has not will be applied to the Position (updating the quantity in memory), and saved to the database.

### Position Service - Internals

Position Service provides the highest speed possible, only incrementing the quantity as a single
"long" operation in Java.

It does this by replacing the current quantity for an order, with the new Quantity, 
as far as the quantity contributes to the position.

This is done with a Java Long.getAndAdd(New Qty - Old Qty),

- The "New Qty" for the Order is known by the incoming message
- The "Old Qty", is
    - A) Null if it is the first Fill for the Order
    - B) The previous fill received if the system was running
    - C) The previous fill, loaded by the Bootstrapper from the Positions Service database, if the system was restarted.
    
By loading from the database, B) and C) above are identical for any case whether the system has been running,
or was started after a crash recovery (via the recovery process, via the Database and the Recovery logic.)

### Corrections

By using the cumulative quantity, if we receive a Cancel Correct with a new Cumulative Quantity,
the update is again a trivial one of replacing the existing quantity in the total with the new quantity.
This argument is additional support for using cumulative quantity; cancel corrects are identical
to the normal order flow.

### Internals Diagram

The position service "internals" diagram, 
explains the  data structures, and flow of Order and Execution Reports into the service.

The most important thing to note is, "fix_cum_qty" on an Execution Report for an Order,
must represent the *total quantity* completed for an order.  
_fix_cum_qty_  is the running total per the FIX spec,
and is easily achieved for manually booked orders by setting it to the total running quantity
for the Order.

![Internals Design](docs/PositionServiceInternals.png)

### HA / DR for Position Service

Position Service will seamlessly fail over to a secondary.
The secondary will maintain parity via a Kafka Topic.
Once this topic is "caught up" (lag = 0), it will be available
for position keeping, which is sub-second fail over.

![HA/DR for Positions](docs/PositionService_HA_DR.png)

## Trade Blotter:  Auto-complete for every column

The trade blotter allows you to auto-type
and sends the searches across any column
to the Position Service.

The "OrderCache" inside the position service,
by using a combination of RoaringBitmaps for 
filtering, Tries for rapid lookup, and
ConcurrentSkipListMap for pre-ordering of the 
column data, can answer most queries in 
sub-100ms up to a hundred million orders.

The system will typically be run with only 
the trades from today, or this week.

But you may choose to also run servers 
with data from an entire year, in addition
to the primary server.

### Order Cache Diagram

![Order Blotter](docs/TradeBlotterInternals.png)


