package com.hedgeos.data;

import com.hedgeos.general.Price;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static com.hedgeos.date.ProtoConverter.priceProtoFromDouble;
import static com.hedgeos.date.ProtoConverter.priceToDouble;

public class ProtoConverterTest {

    @Test
    public void testProtoConverter() {
        // TODO::  Move to a unit test
        double price = 3.33;
        Price pp = priceProtoFromDouble(price);
        System.out.println("pp="+pp);
        double priceToDub = priceToDouble(pp);
        Assertions.assertEquals(priceToDub, price, "Price round trip didn't match");
    }
}
