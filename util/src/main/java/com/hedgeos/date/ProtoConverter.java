package com.hedgeos.date;

import com.hedgeos.general.Price;
import com.hedgeos.general.Quantity;

import java.sql.Date;
import java.time.LocalDate;

public class ProtoConverter {
    public static com.google.type.Date toGoogleDate(Date sqlDate) {
        if (sqlDate == null)
            return null;

        com.google.type.Date.Builder b = com.google.type.Date.newBuilder();
        // time zone less thing
        LocalDate ld = sqlDate.toLocalDate();
        b.setYear(ld.getYear());
        b.setMonth(ld.getMonthValue());
        b.setDay(ld.getDayOfMonth());
        return b.build();
    }

    public static Date toDate(com.google.type.Date startDate) {
        LocalDate ld = LocalDate.of(startDate.getYear(), startDate.getMonth(), startDate.getDay());
        //noinspection UnnecessaryLocalVariable
        Date date = java.sql.Date.valueOf(ld);
        return date;
    }


    public static com.google.type.Date dateFromString(String yyyy_mm_dd) {
        String[] strings = yyyy_mm_dd.split("-");
        com.google.type.Date db = com.google.type.Date.newBuilder()
                .setYear(Integer.parseInt(strings[0]))
                .setMonth(Integer.parseInt(strings[1]))
                .setDay(Integer.parseInt(strings[2]))
                .build();
        return db;
    }

    public static Double priceToDouble(Price price) {
        if (price == null) return null;
        return price.getUnits() +
                price.getNanos() / Math.pow(1000.0,3);
    }

    public static Double quantityToDouble(Quantity lastShares) {
        if (lastShares == null) return null;
        return lastShares.getQuantity() +
                lastShares.getNanoQty() / Math.pow(1000.0, 3);
    }

    // TODO::  Can this be faster with Double.bit operations
    public static Quantity convertDoubleToQty(double lastSharesDouble) {
        // TODO:  Test this

        long whole = (long) lastSharesDouble;
        double fraction = lastSharesDouble - whole;
        int nanos = (int) (fraction * 1_000_000_000);

        Quantity result = Quantity.newBuilder().setQuantity(whole).setNanoQty((int)nanos).build();
        return result;
    }

    public static Price priceProtoFromDouble(double price) {
        long whole = (long) price;
        double fraction = price - whole;
        int nanos = (int) (fraction * 1_000_000_000);
        return Price.newBuilder()
                .setUnits(whole)
                .setNanos(nanos).build();
    }


}
