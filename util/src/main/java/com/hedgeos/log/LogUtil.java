package com.hedgeos.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

public class LogUtil {

    public static void setLogLevelInfo() {
        LoggerContext loggerContext = new LoggerContext();
        ch.qos.logback.classic.Logger root = loggerContext.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.toLevel("info"));
    }
}
