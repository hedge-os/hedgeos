package com.hedgeos.opengamma;

import com.google.protobuf.Timestamp;
import com.google.type.Date;
import com.hedgeos.curve.*;
import com.hedgeos.daycount.DayCount;
import com.hedgeos.general.MarketDataOrBuilder;
import com.hedgeos.general.SecurityType;
import com.hedgeos.security.Equity;
import com.hedgeos.security.FixedFloatLegFlag;
import com.hedgeos.security.InterestRateSwap;
import com.hedgeos.security.Security;
import com.hedgeos.service.curve.Measure;
import com.opengamma.strata.basics.StandardId;
import com.opengamma.strata.basics.currency.Currency;
import com.opengamma.strata.basics.currency.CurrencyAmount;
import com.opengamma.strata.data.FieldName;
import com.opengamma.strata.measure.Measures;
import com.opengamma.strata.product.*;
import com.opengamma.strata.product.common.BuySell;
import com.opengamma.strata.product.swap.SwapTrade;
import com.opengamma.strata.product.swap.type.*;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.ZoneOffset.UTC;

public final class OpenGamma {
    private static final DateTimeFormatter VALUATION_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter MATURITY_DATE_FORMATTER = DateTimeFormatter.ofPattern("MMMyy");
    private static final String SYMBOLOGY = "HedgeOS-Ticker";
    private static final String GROUPS_CSV_HEADERS = "Group Name,Curve Type,Reference,Curve Name\n";
    private static final String SETTINGS_CSV_HEADERS = "Curve Name,Value Type,Day Count,Interpolator,Left Extrapolator,"
            + "Right Extrapolator\n";
    private static final String NODES_CSV_HEADERS = "Curve Name,Label,Symbology,Ticker,Field Name,Type,Convention,Time,"
            + "Clash Action\n";
    private static final String VALUES_CSV_HEADERS = "Valuation Date,Curve Name,Date,Value,Label\n";

    /**
     * Non-instantiable static class
     */
    private OpenGamma() {
    }

    // one-to-one mappings from HedgeOS to OpenGamma
    private static final EnumMap<Measure, com.opengamma.strata.calc.Measure> measureMap = new EnumMap<>(Measure.class);

    // one-to-one bidirectional mappings between HedgeOS and OpenGamma
    private static final EnumMap<DayCount, com.opengamma.strata.basics.date.DayCount> dayCountMap =
            new EnumMap<>(DayCount.class);
    private static final Map<com.opengamma.strata.basics.date.DayCount, DayCount> dayCountMapReverse = new HashMap<>();
    private static final EnumMap<Index, com.opengamma.strata.basics.index.Index> indexMap = new EnumMap<>(Index.class);
    private static final Map<com.opengamma.strata.basics.index.Index, Index> indexMapReverse = new HashMap<>();

    // statically initialize all one-to-one mappings
    static {
        // day counts
        dayCountMap.put(DayCount.ACT_ACT_ISDA, com.opengamma.strata.basics.date.DayCounts.ACT_ACT_ISDA);
        dayCountMap.put(DayCount.ACT_ACT_ICMA, com.opengamma.strata.basics.date.DayCounts.ACT_ACT_ICMA);
        dayCountMap.put(DayCount.ACT_ACT_AFB, com.opengamma.strata.basics.date.DayCounts.ACT_ACT_AFB);
        dayCountMap.put(DayCount.ACT_ACT_YEAR, com.opengamma.strata.basics.date.DayCounts.ACT_ACT_YEAR);
        dayCountMap.put(DayCount.ACT_365_ACTUAL, com.opengamma.strata.basics.date.DayCounts.ACT_365_ACTUAL);
        dayCountMap.put(DayCount.ACT_365L, com.opengamma.strata.basics.date.DayCounts.ACT_365L);
        dayCountMap.put(DayCount.ACT_360, com.opengamma.strata.basics.date.DayCounts.ACT_360);
        dayCountMap.put(DayCount.ACT_365F, com.opengamma.strata.basics.date.DayCounts.ACT_365F);
        dayCountMap.put(DayCount.ACT_365_25, com.opengamma.strata.basics.date.DayCounts.ACT_365_25);
        dayCountMap.put(DayCount.NL_360, com.opengamma.strata.basics.date.DayCounts.NL_360);
        dayCountMap.put(DayCount.NL_365, com.opengamma.strata.basics.date.DayCounts.NL_365);
        dayCountMap.put(DayCount.THIRTY_360_ISDA, com.opengamma.strata.basics.date.DayCounts.THIRTY_360_ISDA);
        dayCountMap.put(DayCount.THIRTY_U_360_EOM, com.opengamma.strata.basics.date.DayCounts.THIRTY_U_360_EOM);
        dayCountMap.put(DayCount.THIRTY_360_PSA, com.opengamma.strata.basics.date.DayCounts.THIRTY_360_PSA);
        dayCountMap.put(DayCount.THIRTY_E_360, com.opengamma.strata.basics.date.DayCounts.THIRTY_E_360);
        dayCountMap.put(DayCount.THIRTY_E_360_ISDA, com.opengamma.strata.basics.date.DayCounts.THIRTY_E_360_ISDA);
        dayCountMap.put(DayCount.THIRTY_EPLUS_360, com.opengamma.strata.basics.date.DayCounts.THIRTY_EPLUS_360);
        dayCountMap.put(DayCount.THIRTY_E_365, com.opengamma.strata.basics.date.DayCounts.THIRTY_E_365);
        dayCountMap.put(DayCount.ONE_ONE, com.opengamma.strata.basics.date.DayCounts.ONE_ONE);
        dayCountMap.put(DayCount.ACT_364, com.opengamma.strata.basics.date.DayCounts.ACT_364);
        dayCountMap.put(DayCount.THIRTY_U_360, com.opengamma.strata.basics.date.DayCounts.THIRTY_U_360);
        dayCountMap.forEach((key, value) -> dayCountMapReverse.put(value, key));

        // measures
        measureMap.put(Measure.PV, Measures.PRESENT_VALUE);
        measureMap.put(Measure.PRICE, Measures.UNIT_PRICE);
        // TODO: correct these test mappings below
        measureMap.put(Measure.DV01, Measures.PV01_CALIBRATED_BUCKETED);
        measureMap.put(Measure.DELTA, Measures.ACCRUED_INTEREST);
        measureMap.put(Measure.GAMMA, Measures.PAR_RATE);
        measureMap.put(Measure.VEGA, Measures.PAR_SPREAD);
        measureMap.put(Measure.THETA, Measures.CASH_FLOWS);
        measureMap.put(Measure.RHO, Measures.CURRENCY_EXPOSURE);
        measureMap.put(Measure.BID, Measures.LEG_INITIAL_NOTIONAL);
        measureMap.put(Measure.ASK, Measures.RESOLVED_TARGET);
        measureMap.put(Measure.LAST, Measures.EXPLAIN_PRESENT_VALUE);
        measureMap.put(Measure.MID, Measures.PV01_MARKET_QUOTE_SUM);

        // indices
        indexMap.put(Index.AUD_AONIA, com.opengamma.strata.basics.index.Index.of("AUD-AONIA"));
//        indexMap.put(Index.AUD_BBSW, com.opengamma.strata.basics.index.Index.of("AUD-BBSW"));
//        indexMap.put(Index.NZ_OCR, com.opengamma.strata.basics.index.Index.of("NZ-OCR"));
        indexMap.put(Index.BRL_CDI, com.opengamma.strata.basics.index.Index.of("BRL-CDI"));
        indexMap.put(Index.CAD_CORRA, com.opengamma.strata.basics.index.Index.of("CAD-CORRA"));
//        indexMap.put(Index.CHF_LIBOR, com.opengamma.strata.basics.index.Index.of("CHF-LIBOR"));
//        indexMap.put(Index.CHF_SAFRON, com.opengamma.strata.basics.index.Index.of("CHF-SAFRON"));
//        indexMap.put(Index.CHF_TOIS, com.opengamma.strata.basics.index.Index.of("CHF-TOIS"));
//        indexMap.put(Index.CLP_CAMARA, com.opengamma.strata.basics.index.Index.of("CLP-CAMARA"));
//        indexMap.put(Index.CNY_CNRR, com.opengamma.strata.basics.index.Index.of("CNY-CNRR"));
//        indexMap.put(Index.COP_IBR, com.opengamma.strata.basics.index.Index.of("COP-IBR"));
//        indexMap.put(Index.CZK_PRIBOR, com.opengamma.strata.basics.index.Index.of("CZK-PRIBOR"));
//        indexMap.put(Index.DKK_CIBOR, com.opengamma.strata.basics.index.Index.of("DKK-CIBOR"));
//        indexMap.put(Index.DKK_DKKOIS, com.opengamma.strata.basics.index.Index.of("DKK-DKKOIS"));
//        indexMap.put(Index.EUR_EURIBOR, com.opengamma.strata.basics.index.Index.of("EUR-EURIBOR"));
//        indexMap.put(Index.EUR_EONIA, com.opengamma.strata.basics.index.Index.of("EUR-EONIA"));
//        indexMap.put(Index.EUR_ESTR, com.opengamma.strata.basics.index.Index.of("EUR-ESTR"));
//        indexMap.put(Index.GBP_LIBOR, com.opengamma.strata.basics.index.Index.of("GBP-LIBOR"));
//        indexMap.put(Index.GBP_SONIA, com.opengamma.strata.basics.index.Index.of("GBP-SONIA"));
//        indexMap.put(Index.HKD_HIBOR, com.opengamma.strata.basics.index.Index.of("HKD-HIBOR"));
//        indexMap.put(Index.HUF_BUBOR, com.opengamma.strata.basics.index.Index.of("HUF-BUBOR"));
//        indexMap.put(Index.ILS_TELBOR, com.opengamma.strata.basics.index.Index.of("ILS-TELBOR"));
//        indexMap.put(Index.INR_MIBOR, com.opengamma.strata.basics.index.Index.of("INR-MIBOR"));
//        indexMap.put(Index.JPY_LIBOR, com.opengamma.strata.basics.index.Index.of("JPY-LIBOR"));
//        indexMap.put(Index.JPY_DTIBOR, com.opengamma.strata.basics.index.Index.of("JPY-DTIBOR"));
//        indexMap.put(Index.JPY_ZTIBOR, com.opengamma.strata.basics.index.Index.of("JPY-ZTIBOR"));
//        indexMap.put(Index.JPY_TONAR, com.opengamma.strata.basics.index.Index.of("JPY-TONAR"));
//        indexMap.put(Index.KRW_CDC, com.opengamma.strata.basics.index.Index.of("KRW-CDC"));
//        indexMap.put(Index.MXN_TIIE, com.opengamma.strata.basics.index.Index.of("MXN-TIIE"));
//        indexMap.put(Index.NOK_NIBOR, com.opengamma.strata.basics.index.Index.of("NOK-NIBOR"));
//        indexMap.put(Index.NZD_BKBM, com.opengamma.strata.basics.index.Index.of("NZD-BKBM"));
//        indexMap.put(Index.PLN_WIBOR, com.opengamma.strata.basics.index.Index.of("PLN-WIBOR"));
//        indexMap.put(Index.RUB_MOSPRIME, com.opengamma.strata.basics.index.Index.of("RUB-MOSPRIME"));
//        indexMap.put(Index.SEK_STIBOR, com.opengamma.strata.basics.index.Index.of("SEK-STIBOR"));
//        indexMap.put(Index.SEK_SIBOR, com.opengamma.strata.basics.index.Index.of("SEK-SIBOR"));
//        indexMap.put(Index.SEK_SIOR, com.opengamma.strata.basics.index.Index.of("SEK-SIOR"));
//        indexMap.put(Index.SGD_SOR, com.opengamma.strata.basics.index.Index.of("SGD-SOR"));
        indexMap.put(Index.USD_FED_FUNDS, com.opengamma.strata.basics.index.Index.of("USD-FED-FUND"));
        indexMap.put(Index.USD_SOFR, com.opengamma.strata.basics.index.Index.of("USD-SOFR"));
//        indexMap.put(Index.USD_LIBOR, com.opengamma.strata.basics.index.Index.of("USD-LIBOR"));
//        indexMap.put(Index.ZAR_JIBAR, com.opengamma.strata.basics.index.Index.of("ZAR-JIBAR"));
//        indexMap.put(Index.AUD_BBSW_3M, com.opengamma.strata.basics.index.Index.of("AUD-BBSW-3M"));
//        indexMap.put(Index.CAD_CDOR_3M, com.opengamma.strata.basics.index.Index.of("CAD-CDOR-3M"));
//        indexMap.put(Index.CHF_LIBOR_3M, com.opengamma.strata.basics.index.Index.of("CHF-LIBOR-3M"));
//        indexMap.put(Index.EUR_EURIBOR_3M, com.opengamma.strata.basics.index.Index.of("EUR-EURIBOR-3M"));
//        indexMap.put(Index.GBP_LIBOR_3M, com.opengamma.strata.basics.index.Index.of("GBP-LIBOR-3M"));
//        indexMap.put(Index.JPY_LIBOR_3M, com.opengamma.strata.basics.index.Index.of("JPY-LIBOR-3M"));
//        indexMap.put(Index.JPY_ZTIBOR_3M, com.opengamma.strata.basics.index.Index.of("JPY-ZTIBOR-3M"));
//        indexMap.put(Index.NZD_NFXI3FRA_3M, com.opengamma.strata.basics.index.Index.of("NZD-NFXI3FRA-3M"));
//        indexMap.put(Index.SEK_STIBOR_3M, com.opengamma.strata.basics.index.Index.of("SEK-STIBOR-3M"));
        indexMap.put(Index.USD_LIBOR_3M, com.opengamma.strata.basics.index.Index.of("USD-LIBOR-3M"));
//        indexMap.put(Index.CHF_LIBOR_1M, com.opengamma.strata.basics.index.Index.of("CHF-LIBOR-1M"));
//        indexMap.put(Index.CHF_LIBOR_6M, com.opengamma.strata.basics.index.Index.of("CHF-LIBOR-6M"));
//        indexMap.put(Index.EUR_EURIBOR_1M, com.opengamma.strata.basics.index.Index.of("EUR-EURIBOR-1M"));
//        indexMap.put(Index.EUR_EURIBOR_6M, com.opengamma.strata.basics.index.Index.of("EUR-EURIBOR-6M"));
//        indexMap.put(Index.GBP_LIBOR_1M, com.opengamma.strata.basics.index.Index.of("GBP-LIBOR-1M"));
//        indexMap.put(Index.GBP_LIBOR_6M, com.opengamma.strata.basics.index.Index.of("GBP-LIBOR-6M"));
//        indexMap.put(Index.HKD_HIBOR_3M, com.opengamma.strata.basics.index.Index.of("HKD-HIBOR-3M"));
//        indexMap.put(Index.HUF_BUBOR_3M, com.opengamma.strata.basics.index.Index.of("HUF-BUBOR-3M"));
//        indexMap.put(Index.ILS_TELBOR_3M, com.opengamma.strata.basics.index.Index.of("ILS-TELBOR-3M"));
//        indexMap.put(Index.JPY_LIBOR_1M, com.opengamma.strata.basics.index.Index.of("JPY-LIBOR-1M"));
//        indexMap.put(Index.JPY_LIBOR_6M, com.opengamma.strata.basics.index.Index.of("JPY-LIBOR-6M"));
//        indexMap.put(Index.JPY_ZTIBOR_6M, com.opengamma.strata.basics.index.Index.of("JPY-ZTIBOR-6M"));
//        indexMap.put(Index.JPY_ZTIBOR_1M, com.opengamma.strata.basics.index.Index.of("JPY-ZTIBOR-1M"));
//        indexMap.put(Index.JPY_DTIBOR_1M, com.opengamma.strata.basics.index.Index.of("JPY-DTIBOR-1M"));
//        indexMap.put(Index.JPY_DTIBOR_3M, com.opengamma.strata.basics.index.Index.of("JPY-DTIBOR-3M"));
//        indexMap.put(Index.JPY_DTIBOR_6M, com.opengamma.strata.basics.index.Index.of("JPY-DTIBOR-6M"));
//        indexMap.put(Index.NOK_NIBOR_3M, com.opengamma.strata.basics.index.Index.of("NOK-NIBOR-3M"));
//        indexMap.put(Index.NZD_BKBM_3M, com.opengamma.strata.basics.index.Index.of("NZD-BKBM-3M"));
//        indexMap.put(Index.PLN_WIBOR_3M, com.opengamma.strata.basics.index.Index.of("PLN-WIBOR-3M"));
//        indexMap.put(Index.PLN_WIBOR_1M, com.opengamma.strata.basics.index.Index.of("PLN-WIBOR-1M"));
//        indexMap.put(Index.PLN_WIBOR_6M, com.opengamma.strata.basics.index.Index.of("PLN-WIBOR-6M"));
//        indexMap.put(Index.RUB_MOSPRIME_3M, com.opengamma.strata.basics.index.Index.of("RUB-MOSPRIME-3M"));
//        indexMap.put(Index.RUB_MOSPRIME_1M, com.opengamma.strata.basics.index.Index.of("RUB-MOSPRIME-1M"));
//        indexMap.put(Index.RUB_MOSPRIME_6M, com.opengamma.strata.basics.index.Index.of("RUB-MOSPRIME-6M"));
//        indexMap.put(Index.SEK_STIBOR_1M, com.opengamma.strata.basics.index.Index.of("SEK-STIBOR-1M"));
//        indexMap.put(Index.SEK_STIBOR_6M, com.opengamma.strata.basics.index.Index.of("SEK-STIBOR-6M"));
//        indexMap.put(Index.NZD_BMBM_1M, com.opengamma.strata.basics.index.Index.of("NZD-BMBM-1M"));
//        indexMap.put(Index.NZD_BMBM_6M, com.opengamma.strata.basics.index.Index.of("NZD-BMBM-6M"));
        indexMap.put(Index.USD_LIBOR_1M, com.opengamma.strata.basics.index.Index.of("USD-LIBOR-1M"));
        indexMap.put(Index.USD_LIBOR_6M, com.opengamma.strata.basics.index.Index.of("USD-LIBOR-6M"));
        indexMap.put(Index.USD_LIBOR_12M, com.opengamma.strata.basics.index.Index.of("USD-LIBOR-12M"));
        indexMap.put(Index.ZAR_JIBAR_3M, com.opengamma.strata.basics.index.Index.of("ZAR-JIBAR-3M"));

        indexMap.forEach((key, value) -> indexMapReverse.put(value, key));
    }

    /**
     * Generates content of an OG quotes CSV, as per the following column format:
     * <p>
     * Valuation Date    Symbology   Ticker      Field Name  Value
     * 2016-08-01        OG-Ticker   GBP-ON      MarketValue 0.0042
     * 2016-08-01        OG-Ticker   GBP-TN      MarketValue 0.005
     * 2016-08-01        OG-Ticker   GBP-OIS-1M  MarketValue 0.0023
     * 2016-08-01        OG-Ticker   GBP-OIS-2M  MarketValue 0.0021
     *
     * @param marketDataList the list of market data points
     * @return the pseudo-CSV content string
     */
    public static String buildMarketDataCsv(Collection<? extends MarketDataOrBuilder> marketDataList) {
        StringBuilder marketDataCsv = new StringBuilder("Valuation Date,Symbology,Ticker,Field Name,Value\n");

        // get rid of duplicates in market data
        marketDataList = marketDataList.stream().distinct().collect(Collectors.toList());

        for (MarketDataOrBuilder md : marketDataList) {
            Timestamp asOfTime = md.getAsOfTime();

            // System.out.println(asOfTime);
            LocalDate ld = Instant
                    .ofEpochSecond(asOfTime.getSeconds(),
                            md.getAsOfTime().getNanos())
                    .atZone(UTC).toLocalDate();

            marketDataCsv.append(ld.format(VALUATION_DATE_FORMATTER))
                    .append(",")
                    .append(SYMBOLOGY)
                    .append(",")
                    .append(md.getSecurityKey().getBbTicker())
                    .append(",")
                    .append(FieldName.MARKET_VALUE)
                    .append(",")
                    .append(md.getUsed())
                    .append("\n");
        }

        return marketDataCsv.toString();
    }

    /**
     * Generates content of an OG curve group CSV, as per the following column format:
     * <p>
     * Group Name              Curve Type  Reference       Curve Name
     * GBP-DSCONOIS-L6MIRS     Discount    GBP             GBP-DSCON-OIS
     * GBP-DSCONOIS-L6MIRS     Forward     GBP-SONIA       GBP-DSCON-OIS
     * GBP-DSCONOIS-L6MIRS     Forward     GBP-LIBOR-6M    GBP-LIBOR6M-IRS
     *
     * @param curveGroup the curve group to calibrate
     * @return the pseudo-CSV content string
     */
    public static String buildCurveGroupCsv(final CurveGroupOrBuilder curveGroup) {
        // add headers as expected by OG (StringBuilder not necessary anymore...)
        StringBuilder curveGroupCsv = new StringBuilder(GROUPS_CSV_HEADERS);

        // set up discount curve
        curveGroupCsv.append(curveGroup.getGroupName())
                .append(",Discount,")
                .append(curveGroup.getCurrencyToDiscountCurve().getCurrency().name())
                .append(",")
                .append(curveGroup.getCurrencyToDiscountCurve().getCurveName())
                .append("\n");

        // set up all forward curves
        for (IndexToForwardCurve indexToForwardCurve : curveGroup.getIndexToForwardCurveList())
            curveGroupCsv.append(curveGroup.getGroupName())
                    .append(",Forward,")
                    .append(getIndex(indexToForwardCurve.getIndex()).getName())
                    .append(",")
                    .append(indexToForwardCurve.getCurveName())
                    .append("\n");

        return curveGroupCsv.toString();
    }

    /**
     * Generates content of an OG curve settings CSV, as per the following column format:
     * <p>
     * Curve Name        Value Type  Day Count   Interpolator    Left Extrapolator   Right Extrapolator
     * GBP-DSCON-OIS     Zero        Act/365F    Linear          Flat                Flat
     * GBP-LIBOR6M-IRS   Zero        Act/365F    Linear          Flat                Flat
     *
     * @param curveDefs iterable of curve definitions
     * @return the pseudo-CSV content string
     */
    public static String buildCurveSettingsCsv(final Iterable<? extends CurveDefOrBuilder> curveDefs) {
        StringBuilder curveSettingsCsv = new StringBuilder(SETTINGS_CSV_HEADERS);

        for (CurveDefOrBuilder curveDef : curveDefs) {
            // again do this for each curve... (note that OG only seems to support one interpolator, so we only use index 0)
            // also, no extrapolation offered as of now
            if (curveDef.getInterpolationDef() == null ||
                    curveDef.getInterpolationDef().getInterpolationNameList().size() == 0)
                throw new RuntimeException("Interpolation name null for curve=" + curveDef.getName());

            // TODO add extrapolation support in protobufs
            curveSettingsCsv.append(curveDef.getName())
                    .append(",Zero,")
                    .append(getDayCount(curveDef.getDayCount()).getName())
                    .append(",")
                    .append(curveDef.getInterpolationDef().getInterpolationName(0))
                    .append(",Flat,Flat\n");
        }

        return curveSettingsCsv.toString();
    }

    /**
     * Generates content of an OG curve nodes CSV, as per the following column format:
     * <p>
     * Curve Name      Label         Symbology Ticker        Field Name  Type Convention             Time  Clash Action
     * GBP-DSCON-OIS   GBP-ON        OG-Ticker GBP-ON        MarketValue DEP  GBP-ShortDeposit-T0    1D
     * GBP-DSCON-OIS   GBP-TN        OG-Ticker GBP-TN        MarketValue DEP  GBP-ShortDeposit-T1    1D
     * GBP-DSCON-OIS   GBP-OIS-1M    OG-Ticker GBP-OIS-1M    MarketValue OIS  GBP-FIXED-1Y-SONIA-OIS 1M
     * GBP-DSCON-OIS   GBP-OIS-2M    OG-Ticker GBP-OIS-2M    MarketValue OIS  GBP-FIXED-1Y-SONIA-OIS 2M
     * GBP-DSCON-OIS   GBP-OIS-3M    OG-Ticker GBP-OIS-3M    MarketValue OIS  GBP-FIXED-1Y-SONIA-OIS 3M
     * ...
     * GBP-LIBOR6M-IRS GBP-FIX-L6M   OG-Ticker GBP-FIX-L6M   MarketValue FIX  GBP-LIBOR-6M
     * GBP-LIBOR6M-IRS GBP-FRA-3Mx9M OG-Ticker GBP-FRA-3Mx9M MarketValue FRA  GBP-LIBOR-6M           3Mx9M
     * GBP-LIBOR6M-IRS GBP-IRS6M-1Y  OG-Ticker GBP-IRS6M-1Y  MarketValue IRS  GBP-FIXED-6M-LIBOR-6M  1Y
     * GBP-LIBOR6M-IRS GBP-IRS6M-2Y  OG-Ticker GBP-IRS6M-2Y  MarketValue IRS  GBP-FIXED-6M-LIBOR-6M  2Y
     * GBP-LIBOR6M-IRS GBP-IRS6M-3Y  OG-Ticker GBP-IRS6M-3Y  MarketValue IRS  GBP-FIXED-6M-LIBOR-6M  3Y
     * ...
     *
     * @param curveDefs iterable of curve definitions
     * @return the pseudo-CSV content string
     */
    public static String buildCurveNodesCsv(final Iterable<? extends CurveDefOrBuilder> curveDefs) {
        StringBuilder curveNodesCsv = new StringBuilder(NODES_CSV_HEADERS);

        for (CurveDefOrBuilder curveDef : curveDefs) {
            // TODO sort all securities by tenor

            // go through all securities
            for (Security security : curveDef.getSecuritiesList()) {
                final SecurityType securityType = security.getSecurityType();
                curveNodesCsv.append(curveDef.getName())
                        .append(",")
                        .append(security.getSecurityKey().getName())
                        .append(",")
                        .append(SYMBOLOGY)
                        .append(",")
                        .append(security.getSecurityKey().getName())
                        .append(",")
                        .append(FieldName.MARKET_VALUE.toString())
                        .append(",")
                        .append(securityType.name())
                        .append(",")
                        .append(security.getSettlementConvention().getName())
                        .append(",");

                if (security.hasTenorDate())
                    curveNodesCsv.append(MATURITY_DATE_FORMATTER.format(LocalDate.of(security.getTenorDate().getYear(),
                            security.getTenorDate().getMonth(), security.getTenorDate().getDay())));
                else if (!security.getTenor().name().equals("NO_TENOR"))
                    curveNodesCsv.append(security.getTenor().name().substring(1));

                if (securityType.equals(SecurityType.FRA)) {
                    curveNodesCsv.append("X");
                    curveNodesCsv.append(security.getFraEnd().name().substring(1));
                }

                // if we encounter an index or a future, always allow it to be dropped in case it conflicts with another
                // security's maturity (per default, always drop the first security of the two)
                if (securityType.equals(SecurityType.FIX) || securityType.equals(SecurityType.IFU)
                        || securityType.equals(SecurityType.ONF))
                    curveNodesCsv.append(",DropThis\n");
                else
                    curveNodesCsv.append(",\n");
            }
        }

        return curveNodesCsv.toString();
    }

    /**
     * Generates content of an OG curve values CSV, as per the following column format:
     * <p>
     * Valuation Date   Curve Name  Date        Value           Label
     * 2014-01-22       USD-Disc    2014-01-23  0.001571524     0D
     * 2014-01-22       USD-Disc    2014-02-24  0.000934099     1M
     * 2014-01-22       USD-Disc    2014-03-24  0.000948444     2M
     * 2014-01-22       USD-Disc    2014-04-24  0.000935866     3M
     * 2014-01-22       USD-Disc    2014-07-24  0.001027672     6M
     * 2014-01-22       USD-Disc    2014-10-24  0.001280398     9M
     * 2014-01-22       USD-Disc    2015-01-26  0.001841773     1Y
     * ...
     * 2014-01-22       USD-3ML     2014-04-24  0.002413351     3M
     * 2014-01-22       USD-3ML     2014-07-24  0.002523692     6M
     * 2014-01-22       USD-3ML     2014-10-24  0.002696588     9M
     * 2014-01-22       USD-3ML     2015-01-26  0.003298585     1Y
     * 2014-01-22       USD-3ML     2016-01-25  0.007112496     2Y
     * ...
     *
     * @param curves iterable of curves
     * @return the pseudo-CSV content string
     */
    public static String buildCurveValuesCsv(Iterable<? extends CurveOrBuilder> curves) {
        StringBuilder result = new StringBuilder(VALUES_CSV_HEADERS);

        for (CurveOrBuilder curve : curves) {
            if (curve.getCurveType().equals(CurveType.DISCOUNT)) continue;

            ZonedDateTime valuationDate = Instant.ofEpochSecond(curve.getAsOfTime().getSeconds(),
                    curve.getAsOfTime().getNanos()).atZone(UTC);

            String front = valuationDate.format(VALUATION_DATE_FORMATTER)
                    + "," + curve.getCurveName() + ",";

            for (CurvePoint curvePoint : curve.getPointsList()) {
                result.append(front);
                ZonedDateTime tenorDate = Instant.ofEpochSecond(curvePoint.getDate().getSeconds()).atZone(UTC);
                result.append(tenorDate.format(VALUATION_DATE_FORMATTER));
                result.append(",");
                result.append(curvePoint.getDiscountFactor());
                result.append(",");
                result.append(Duration.between(valuationDate, tenorDate));
                result.append("\n");
            }
        }

        return result.toString();
    }

    /**
     * Converts HedgeOS DayCount into OpenGamma DayCount
     *
     * @param dayCount the day count to convert
     * @return the OpenGamma day count
     */
    public static com.opengamma.strata.basics.date.DayCount getDayCount(DayCount dayCount) {
        com.opengamma.strata.basics.date.DayCount result = dayCountMap.get(dayCount);
        if (result == null)
            throw new IllegalStateException("DayCount '" + dayCount.name() + "' not supported by OpenGamma");
        return result;
    }

    /**
     * Converts OpenGamma DayCount into HedgeOS DayCount
     *
     * @param dayCount the day count to convert
     * @return the HedgeOS day count
     */
    public static DayCount getDayCount(com.opengamma.strata.basics.date.DayCount dayCount) {
        DayCount result = dayCountMapReverse.get(dayCount);
        if (result == null)
            throw new IllegalStateException("DayCount '" + dayCount.getName() + "' not supported by HedgeOS");
        return result;
    }

    /**
     * Converts HedgeOS Measure into OpenGamma Measure
     *
     * @param measure the measure to convert
     * @return the OpenGamma measure
     */
    public static com.opengamma.strata.calc.Measure getMeasure(Measure measure) {
        com.opengamma.strata.calc.Measure result = measureMap.get(measure);
        if (result == null)
            throw new IllegalStateException("Measure '" + measure.name() + "' not supported by OpenGamma");
        return result;
    }

    /**
     * Converts HedgeOS Index into OpenGamma Index
     *
     * @param index the index to convert
     * @return the OpenGamma index
     */
    public static com.opengamma.strata.basics.index.Index getIndex(Index index) {
        com.opengamma.strata.basics.index.Index result = indexMap.get(index);
        if (result == null)
            throw new IllegalStateException("Index '" + index.name() + "' not supported by OpenGamma");
        return result;
    }

    /**
     * Converts OpenGamma Index into HedgeOS Index
     *
     * @param index the index to convert
     * @return the HedgeOS index
     */
    public static Index getIndex(com.opengamma.strata.basics.index.Index index) {
        Index result = indexMapReverse.get(index);
        if (result == null)
            throw new IllegalStateException("Index '" + index.getName() + "' not supported by HedgeOS");
        return result;
    }

    /**
     * Creates an OpenGamma generic security trade for equities
     *
     * @param quantity      quantity of the position
     * @param equity        the equity in question
     * @param valuationDate the desired valuation date
     * @return an OpenGamma trade object
     */
    public static GenericSecurityTrade getGenericSecurityTrade(long quantity, Equity equity, LocalDate valuationDate) {
        // create all necessary OpenGamma definitions
        SecurityId secId = SecurityId.of("HedgeOS-Equity",
                equity.getSecurity().getBbTicker());

        // TODO provide real info for tick value, currency etc. below
        SecurityPriceInfo securityPriceInfo = SecurityPriceInfo.of(0.01, CurrencyAmount.of(Currency.USD, 0.01));

        // create OpenGamma trade
        return GenericSecurityTrade.of(
                TradeInfo.builder().id(secId.getStandardId()).settlementDate(valuationDate).build(),
                GenericSecurity.of(SecurityInfo.of(secId, securityPriceInfo)),
                quantity,
                0.0
        );
    }

    /**
     * Creates an OpenGamma swap trade for interest rate swaps
     *
     * @param quantity         quantity of the position
     * @param interestRateSwap the interest rate swap in question
     * @param valuationDate    the desired valuation date
     * @return an OpenGamma trade object
     */
    public static SwapTrade getSwapTrade(long quantity, InterestRateSwap interestRateSwap, LocalDate valuationDate) {
        // TODO @Chris: I am assuming the following convention:
        // << long position of a swap pays first leg to receive second leg >>
        Index leg1Index = interestRateSwap.getLeg1Index();
        Index leg2Index = interestRateSwap.getLeg2Index();

        // create common trade info
        TradeInfo tradeInfo = TradeInfo.builder()
                .id(StandardId.of("HedgeOS-Swap", interestRateSwap.getSecurity().getBbTicker()))
                .counterparty(StandardId.of("HedgeOS-Counterparty", interestRateSwap.getCounterparty()))
                .settlementDate(valuationDate)
                .build();

        boolean leg1Fixed = interestRateSwap.getLeg1FixedFloatFlag().equals(FixedFloatLegFlag.FIXED_LEG);
        boolean leg2Fixed = interestRateSwap.getLeg2FixedFloatFlag().equals(FixedFloatLegFlag.FIXED_LEG);

        // some almost superfluous sanity assertions, for -ea only
        assert leg1Fixed || interestRateSwap.getLeg1FixedFloatFlag().equals(FixedFloatLegFlag.FLOAT_LEG)
                : "leg 1 is neither fixed nor floating";
        assert leg2Fixed || interestRateSwap.getLeg2FixedFloatFlag().equals(FixedFloatLegFlag.FLOAT_LEG)
                : "leg 2 is neither fixed nor floating";

        // determine the following two parameters based on the legs we got
        double fixedRateOrSpread;
        SingleCurrencySwapConvention swapConvention;

        if (leg1Fixed) {    // it looks like a fixed overnight swap
            fixedRateOrSpread = interestRateSwap.getLeg1Rate();

            // TODO add SOFR, not yet supported
            if (leg2Index.equals(Index.USD_FED_FUNDS))      // OIS swap
                swapConvention = (interestRateSwap.getSecurity().getTenor().compareTo(Tenor.T1Y) > 0) ?
                        FixedOvernightSwapConventions.USD_FIXED_1Y_FED_FUND_OIS
                        : FixedOvernightSwapConventions.USD_FIXED_TERM_FED_FUND_OIS;
            else if (leg2Index.equals(Index.USD_LIBOR_3M))  // fixed vs. 3M libor
                swapConvention = FixedIborSwapConventions.USD_FIXED_6M_LIBOR_3M;
            else    // other types are currently not supported
                throw new IllegalStateException("unsupported overnight swap: fixed vs. '" + leg2Index.name() + "'");
        } else if (leg2Fixed) {  // it looks like an inverted fixed overnight swap
            quantity = -quantity;
            fixedRateOrSpread = interestRateSwap.getLeg2Rate();

            // TODO add SOFR, not yet supported
            if (leg1Index.equals(Index.USD_FED_FUNDS))      // OIS swap
                swapConvention = (interestRateSwap.getSecurity().getTenor().compareTo(Tenor.T1Y) > 0) ?
                        FixedOvernightSwapConventions.USD_FIXED_1Y_FED_FUND_OIS
                        : FixedOvernightSwapConventions.USD_FIXED_TERM_FED_FUND_OIS;
            else if (leg1Index.equals(Index.USD_LIBOR_3M))  // fixed vs. 3M libor
                swapConvention = FixedIborSwapConventions.USD_FIXED_6M_LIBOR_3M;
            else    // other types are currently not supported
                throw new IllegalStateException("unsupported overnight swap: fixed vs. '" + leg1Index.name() + "'");
        } else {    // it looks like a basis swap
            fixedRateOrSpread = 0.0;    // TODO evaluate when we need to set a market spread here

            if (leg1Index.equals(Index.USD_FED_FUNDS) && leg2Index.equals(Index.USD_LIBOR_3M))
                swapConvention = OvernightIborSwapConventions.USD_FED_FUND_AA_LIBOR_3M;
            else if (leg1Index.equals(Index.USD_LIBOR_1M) && leg2Index.equals(Index.USD_LIBOR_3M))
                swapConvention = IborIborSwapConventions.USD_LIBOR_1M_LIBOR_3M;
            else if (leg1Index.equals(Index.USD_LIBOR_3M)) {
                if (leg2Index.equals(Index.USD_LIBOR_6M))
                    swapConvention = IborIborSwapConventions.USD_LIBOR_3M_LIBOR_6M;
                else {
                    quantity = -quantity;
                    if (leg2Index.equals(Index.USD_FED_FUNDS))
                        swapConvention = OvernightIborSwapConventions.USD_FED_FUND_AA_LIBOR_3M;
                    else if (leg2Index.equals(Index.USD_LIBOR_1M))
                        swapConvention = IborIborSwapConventions.USD_LIBOR_1M_LIBOR_3M;
                    else throw new IllegalStateException("unsupported basis swap: USD 3M LIBOR vs. '"
                                + leg2Index.name() + "'");
                }
            } else if (leg1Index.equals(Index.USD_LIBOR_6M)) {
                quantity = -quantity;
                swapConvention = IborIborSwapConventions.USD_LIBOR_3M_LIBOR_6M;
            } else throw new IllegalStateException("unsupported basis swap: leg 1 is '" + leg1Index.name()
                    + "', leg 2 is '" + leg2Index.name() + "'");
        }

        // extract start and termination dates for clarity
        Date startDate = interestRateSwap.getStartDate();
        Date terminationDate = interestRateSwap.getTerminationDate();

        return swapConvention.toTrade(
                tradeInfo,
                LocalDate.of(startDate.getYear(), startDate.getMonth(), startDate.getDay()),
                LocalDate.of(terminationDate.getYear(), terminationDate.getMonth(), terminationDate.getDay()),
                quantity > 0 ? BuySell.BUY : BuySell.SELL,
                interestRateSwap.getNotional(),
                fixedRateOrSpread
        );
    }
}
