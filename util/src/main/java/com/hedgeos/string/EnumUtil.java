package com.hedgeos.string;

import com.hedgeos.curve.Index;


public class EnumUtil {
    /**
     * Convenience helper function to create more aesthetic index names
     *
     * @param index the index to convert to string
     * @return string representation of the index
     */
    public static String indexToString(Index index) {
        return index.name().replace('_', '-');
    }
}
