package com.hedgeos.thread;

public class Sleep {

    public static void sleepMin(int min) {
        sleep(1000*60*min);
    }

    public static void sleepSec(int sec) {
        sleep(1000*sec);
    }

    public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        }
        catch (Exception e) {
            System.err.println("Interrupted?");
        }
    }
}
