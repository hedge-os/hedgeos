package com.hedgeos.classpath;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.Charset;

public class CpUtil {

    public static String loadFromClasspath(String filePathInClasspath) {
        try {
            InputStream is = CpUtil.class.getResourceAsStream("/"+filePathInClasspath);
            return IOUtils.toString(is, Charset.defaultCharset());
        } catch (Exception e) {
            try {
                InputStream is = CpUtil.class.getResourceAsStream(filePathInClasspath);
                return IOUtils.toString(is, Charset.defaultCharset());
            }
            catch (Exception e2) {
                throw new RuntimeException(e2);
            }
        }
    }
}
