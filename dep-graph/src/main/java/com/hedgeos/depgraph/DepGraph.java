package com.hedgeos.depgraph;

import com.hedgeos.curve.Node;
import com.hedgeos.curve.NodeType;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.position.Position;
import com.hedgeos.security.Security;
import org.apache.commons.collections4.MapUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Dependency graph for Pricing (and possibly reporting)
 * Break down Positions, into the pieces need to price them, e.g.
 * USD IRS Libor 3M Swap --> USD-LIBOR-3M curve -->
 *      Curve Securities + Conventions --> Market Data, Historical Data (fixings)
 *
 * + Sweep the graph, fetch the market data for Leaves
 * + Sweep the graph, fetch the historical data for any leaves
 * + Sweep the graph for yield curves, build them with the market and reference data
 *
 */
public class DepGraph {

    // Note: the root of this Dependency Graph
    public final Node.Builder rootBuilder;

    // pricing a list of positions, is the fundamental job of Dep graph
    // although, reports will be on the DepGraph and these will decompose to fetch Positions
    // e.g. Report = Backtest, PM = KOLJA, will fetch these positions first,
    //  - then run a Backtest over many days
    //  - which means, the Report will need to report dependencies like,
    //  - e.g. Market Data(Positions, Start Data --> End Date)
    //  - this is a worth exercise, now, as it is the "high end" we wish to provide in this system
    public DepGraph(List<Position> positions)
    {
        rootBuilder = Node.newBuilder().setNodeType(NodeType.ROOT);

        Node.Builder posListNode = Node.newBuilder().setNodeType(NodeType.POSITION_COLLECTION);
        posListNode.setPositionsCount(positions.size()); // debug info
        rootBuilder.addChildren(posListNode); // track this waa a list


        // add a node for each Position
        for (Position position : positions) {
            Node.Builder posNode = Node.newBuilder().setNodeType(NodeType.POSITION);
            posNode.setPosition(position);
            posListNode.addChildren(posNode); // add position to the pos list node
            // 0.  Add a security dep. for the Position
            Node.Builder secNode = Node.newBuilder().setNodeType(NodeType.SECURITY);
            secNode.setSecurityKey(posNode.getPosition().getSecurityKey());
            posNode.addChildren(secNode);
            posNode.setIsResolved(true); // nothing more to do for positions list
            Node posWithSec = posNode.build();
            rootBuilder.addChildren(posWithSec);
        }

        // need to build when everything is built
        // root = rootBuilder.build();

        // now we have added the positions,
        // will fan these out, outside this class
    }

}
