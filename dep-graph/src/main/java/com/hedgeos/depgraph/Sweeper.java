package com.hedgeos.depgraph;

import com.hedgeos.curve.Node;
import com.hedgeos.curve.NodeOrBuilder;
import com.hedgeos.curve.NodeType;
import com.hedgeos.curve.ResolutionFailureType;
import com.hedgeos.general.SecurityType;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.security.Security;
import org.apache.commons.collections4.MapUtils;

import java.util.*;
import java.util.function.Function;

public class Sweeper {

    /**
     * Sweep the graph for SecurityKey which we need to fetch
     * .. to make the full fledged, Security
     * @return the collector is populated with Nodes of type SecurityKey
     *  for which we need Securities
     */
    public static List<SecurityKey> unresolvedSecurityKeys(NodeOrBuilder rootNode) {
        List<Node> collector = new ArrayList<>();
        collectUnresolvedNodes(rootNode, NodeType.SECURITY, collector);

        List<SecurityKey> keys = new ArrayList<>();
        for (Node node : collector) {
            if (node.getSecurityKey()  == null)
                continue; // should never happen, because, we only collected security keys
            keys.add(node.getSecurityKey());
        }
        return keys;
    }

    /**
     * Walk the tree recursively, and collect
     *  where we didn't yet resolve this graph node
     * , for the particular NodeType passed.
     *
     * @param node The root node of the graph to walk, or a sub-node using recursion
     * @param nodeType The node type we want to collect
     * @param collector Collection to pick up what we want while doing the recursion
     */
    private static void collectUnresolvedNodes(NodeOrBuilder node, NodeType nodeType, List<Node> collector) {
        for (Node child : node.getChildrenList()) {
            if (!child.getIsResolved() && child.getNodeType().equals(nodeType)) {
                collector.add(child); // collect the child
            }

            // walk down the tree
            if (child.getChildrenList() != null) {
                collectUnresolvedNodes(child, nodeType, collector);
            }
        }
    }

    private static void collectNodes(NodeOrBuilder node, NodeType nodeType, List<Node> collector) {
        for (Node child : node.getChildrenList()) {
            if (child.getNodeType().equals(nodeType)) {
                collector.add(child); // collect the child
            }

            // walk down the tree
            if (child.getChildrenList() != null) {
                collectUnresolvedNodes(child, nodeType, collector);
            }
        }
    }

    public static void addSecuritiesForKeys(Node.Builder root, List<Security> securitiesList) {

        Map<SecurityKey, Security> securityMap = new HashMap<>();
        // thumbs up (Apache Commons)
        MapUtils.populateMap(securityMap, securitiesList, Security::getSecurityKey);

        walkGraphAddSecurities(root, securityMap);
    }

    private static void walkGraphAddSecurities(Node.Builder current,
                                               Map<SecurityKey, Security> securityMap)
    {
        if (current.getNodeType().equals(NodeType.SECURITY)) {
            // do some work
            Security security = securityMap.get(current.getSecurityKey());
            if (security == null) {
                current.setIsResolved(true) // failed but resolved
                        .setFailureType(ResolutionFailureType.MISSING_SECURITY);
            }
            else {
                current.setIsResolved(true).setSecurity(security).build();
            }
        }

        if (current.getChildrenList() == null || current.getChildrenList().isEmpty()) {
            // bottom out
            return;
        }

        // walk the children
        for (Node.Builder builder : current.getChildrenBuilderList()) {
            walkGraphAddSecurities(builder, securityMap);
        }
    }

    private static void replaceCurrent(NodeOrBuilder parent, Node current, Node copy) {
        // TODO:  (may not be, debug)
        //  TODO: This remove may be horrible, as it is O(N), won't be ok for large N
        parent.getChildrenList().remove(current);
        parent.getChildrenList().add(copy);
    }

    public static Map<String, Node> addSecurityToCurveMapping(DepGraph depGraph, Map<SecurityKey, List<String>> curveNamesForSecs)
    {
        // make nodes for the curves (one for each, this way, the graph points at the same curve nodes)

        // collect the unique curve names across securities
        Set<String> curveNames = new HashSet<>();
        for (List<String> value : curveNamesForSecs.values()) {
            curveNames.addAll(value);
        }

        // make the one node per unique curve name
        Map<String, Node> nodeForCurve = new HashMap<>();
        for (String curveName : curveNames) {
            Node curveNode = Node.newBuilder()
                    .setNodeType(NodeType.CURVE)
                    .setCurveName(curveName).build();

            nodeForCurve.put(curveName, curveNode);
        }

        List<Node> collector = new ArrayList<>();
        collectNodes(depGraph.rootBuilder, NodeType.SECURITY, collector);
        for (Node node : collector) {
            List<String> perSecCurves = curveNamesForSecs.get(node.getSecurityKey());
            if (perSecCurves != null) {
                for (String perSecCurve : perSecCurves) {
                    // find the Curve Node we made above (one Node in the graph)
                    Node curveNode = nodeForCurve.get(perSecCurve);
                    // many securities --> one curve node
                    // add the curveNode as a child on this security
                    node.getChildrenList().add(curveNode);
                }
            }
        }

        return nodeForCurve;
    }

    public static List<SecurityKey> getMarketDataSecurities(DepGraph depGraph) {
        List<Node> collector = new ArrayList<>();

        // collect nodes with this determining function
        collectNodes(depGraph.rootBuilder, NodeType.SECURITY, collector,
                Sweeper::isLevelOneMarketDataNode);

        List<SecurityKey> secKeysOnly = new ArrayList<>();
        for (Node node : collector) {
            secKeysOnly.add(node.getSecurityKey());
        }
        return secKeysOnly;
    }

    private static void collectNodes(NodeOrBuilder node, NodeType nodeType,
                                     List<Node> collector,
                                     Function<Node, Boolean> isOtcSecurity) {

        for (Node child : node.getChildrenList()) {
            if (isOtcSecurity.apply(child) && child.getNodeType().equals(nodeType)) {
                collector.add(child); // collect the child
            }

            // walk down the tree
            if (child.getChildrenList() != null) {
                collectUnresolvedNodes(child, nodeType, collector);
            }
        }
    }

    public static boolean isLevelOneMarketDataNode(Node node) {
        if (!node.getNodeType().equals(NodeType.SECURITY))
            return false;

        SecurityType securityType = node.getSecurity().getSecurityType();

        // if it is an OTC security type, it is not level one
        // otherwise it is level one and we need market data observed
        // OTC can be SWAP_OTC SWAPTION_OTC, etc
        if (securityType.toString().endsWith("OTC"))
            return false;

        // everything else not OTC is a "rate" observed in the market, or stock etc.
        return true;
    }


    public static void replaceNode(DepGraph depGraph, Node curveNode, Node newCurveNode) {

    }
}
