package com.hedgeos.depgraph;

import com.hedgeos.curve.CurveGroup;
import com.hedgeos.curve.IndexToForwardCurve;
import com.hedgeos.curve.Node;
import com.hedgeos.curve.NodeType;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility for adding curve info to the graph.
 */
public class CurveGraphUtil {

    /**
     * Add the curve groups to the DepGraph where appropriate, for tracking,
     *
     * @param curveNameToNode Curve names, off of the graph, to their nodes (already graphed)
     * @param curveNameToGroup Curve Names in a group, to the group containing the curve
     * @return Map of Curve Group to the new Curve Group Nodes
     */
    public static Map<CurveGroup, Node> addCurveGroupsToGraph(Map<String, Node> curveNameToNode,
                                                              Map<String, CurveGroup> curveNameToGroup) {

        Map<String, Node> curveGroupNameToCurveGroup = new HashMap<>();
        Map<CurveGroup, Node> curveGroupToNode = new HashMap<>();

        // first make nodes, map them to the name and group
        for (CurveGroup value : curveNameToGroup.values()) {
            Node curveGroupNode =
                    Node.newBuilder().setNodeType(NodeType.CURVE_GROUP).setCurveGroupName(value.getGroupName())
                    .build();

            curveGroupNameToCurveGroup.put(value.getGroupName(), curveGroupNode);
            curveGroupToNode.put(value, curveGroupNode);
        }

        // now map the curve nodes to curveGroup nodes on the graph
        for (Map.Entry<String, CurveGroup> curveNameToGroupEntry : curveNameToGroup.entrySet()) {
            Node curveGroupNode = curveGroupNameToCurveGroup.get(curveNameToGroupEntry.getKey());
            CurveGroup curveGroup = curveNameToGroupEntry.getValue();
            String discountCurve = curveGroup.getCurrencyToDiscountCurve().getCurveName();
            if (curveNameToNode.containsKey(discountCurve)) {
                Node discountCurveNode = curveNameToNode.get(discountCurve);
                // TODO:  May need to clone replace the node on the graph
                discountCurveNode.getChildrenList().add(curveGroupNode);
            }

            for (IndexToForwardCurve indexToForwardCurve : curveGroup.getIndexToForwardCurveList()) {
                String forwardCurveCurveName = indexToForwardCurve.getCurveName();
                if (curveNameToNode.containsKey(forwardCurveCurveName)) {
                    Node forwardCurveNode = curveNameToNode.get(discountCurve);
                    // TODO:  May need to clone replace the node on the graph
                    if (forwardCurveNode != null) {
                        // it is null, if zero securities use it
                        // this projection curve
                        forwardCurveNode.getChildrenList().add(curveGroupNode);
                    }
                }
            }
        }

        // return the group to the nodes, for the graph
        return curveGroupToNode;
    }
}
