package com.hedgeos.service.emsbridge;

import com.google.type.Money;
import com.hedgeio.trade.*;
import com.hedgeos.general.Currency;
import com.hedgeos.general.Price;
import com.hedgeos.general.Quantity;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.position.Position;
import com.hedgeos.position.PositionAndMeta;
import com.hedgeos.position.PositionOrBuilder;
import com.hedgeos.security.Security;
import com.hedgeos.service.position.PositionServiceGrpc;
import com.hedgeos.service.security.SecurityRequest;
import com.hedgeos.service.security.SecurityResponse;
import com.hedgeos.service.security.SecurityServiceGrpc;
import com.hedgeos.thread.Sleep;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static com.hedgeos.api.Ports.*;

@Component
public class EmsBridgePositionClient {

    public static final int FAKE_POSITION_ID = -999;
    private PositionServiceGrpc.PositionServiceBlockingStub posServiceStub;

    // lookup the security, for the test, for now
    private SecurityServiceGrpc.SecurityServiceBlockingStub securityServiceBlockingStub;

    @PostConstruct
    private void init() {

        // TODO:  Fetch from Consul Saturday AM, and with Envoy side cars
        ManagedChannel managedChannelPos = ManagedChannelBuilder
                .forAddress("localhost", POS_SERVICE_GRPC).usePlaintext().build();
        // get a client to the position service
        posServiceStub = PositionServiceGrpc.newBlockingStub(managedChannelPos);

        ManagedChannel managedChannelSec = ManagedChannelBuilder
                .forAddress("localhost", SEC_SERVICE_GRPC).usePlaintext().build();
        // get a client to the position service
        securityServiceBlockingStub = SecurityServiceGrpc.newBlockingStub(managedChannelSec);

        // lookup a fake swap
        SecurityResponse fakeSwapResp = securityServiceBlockingStub.getSecurity(
                SecurityRequest.newBuilder().addSecurityKey(SecurityKey.newBuilder().setName(FAKE_SWAP_NAME)).build()
        );

        Security fakeSwap = fakeSwapResp.getSecurities(0); // the first one

        // fake total swap quantity
        int totalSwapOrderQty = 3_000_000;

        // mock by pushing trades for a PM
        Runnable r = () -> {

            int currentQty = 0;

            // Note:  Important on mapping orders to positions
            // you can send the order and target a position, like this.
            // (alternatively, not done yet, you can map an account on
            // an order to move to a portfolio and strategy)
            PositionAndMeta.Builder pamBuilder = PositionAndMeta.newBuilder();
            // the fake position_id ties this to meta about the position
            // which is fetched from the Ref Data service
            pamBuilder.setPosition(Position.newBuilder().setPositionId(FAKE_POSITION_ID));

            // start sending fills
            while (true) {

                try {
                    // place an order and then fills on it, as far as Position Service is concerned
                    Order order = Order.newBuilder()
                            .setSid(fakeSwap.getSecurityKey().getSecurityId())
                            .setPositionMeta(pamBuilder.build().getMeta())
                            .build();

                    OrderPlacedRequest request = OrderPlacedRequest.newBuilder()
                            .addOrders(order)
                            .build();
                    OrderPlacedResponse resp = posServiceStub.orderPlaced(request);

                    System.out.println("Order Message Status="+resp.getStatus());
                    if (resp.getStatus() != OrderMessageStatus.SUCCESS) {
                        System.err.println("Fake order failed="+resp);
                    }


                    // fill an order
                    ExecutionReport fill = ExecutionReport.newBuilder()
                            .setHedgeOsOrderId(order.getHedgeOsOrderId()) // from the Hedge OS Order
                            // .setC("NMS" + order.getHedgeOsOrderId()) // the brokers, Order ID
                            .setTotalQty(Quantity.newBuilder().setQuantity(totalSwapOrderQty))
                            .setCumulativeQty(Quantity.newBuilder().setQuantity(currentQty).build())
                            .setAvgPrice(Price.newBuilder().setUnits(-999).setNanos(-999))
                        .build();

                    // fake the quantity going up, on the next fill
                    currentQty++;

                    OrderExecutionReportRequest fillRequest =
                            OrderExecutionReportRequest.newBuilder().addExecutionReports(fill).build();
                    OrderExecutionReportResponse fillResponse = posServiceStub.orderFilled(fillRequest);

                    System.out.println("Fill resp Fake IRS="+fillResponse.getStatus());
                    if (fillResponse.getStatus() != OrderMessageStatus.SUCCESS) {
                        System.err.println("Bad, didn't fill order="+fillResponse);
                    }

                    // send a fill every half second, good for demos
                    Sleep.sleep(500);

                } catch (Exception e) {
                    System.out.println("Exception placing order=" + e);
                    Sleep.sleep(10_000);
                }
            }
        };

        // keep sending orders in this bridge, for a fake IRS swap
        new Thread(r, "Fake Order Thread").start();
    }


}
