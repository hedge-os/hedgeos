${AnsiColor.GREEN} ██╗  ██╗███████╗██████╗  ██████╗ ███████╗    ${AnsiColor.RED}  ██████╗ ███████╗
${AnsiColor.GREEN} ██║  ██║██╔════╝██╔══██╗██╔════╝ ██╔════╝    ${AnsiColor.RED} ██╔═══██╗██╔════╝
${AnsiColor.GREEN} ███████║█████╗  ██║  ██║██║  ███╗█████╗█████╗${AnsiColor.RED} ██║   ██║███████╗
${AnsiColor.GREEN} ██╔══██║██╔══╝  ██║  ██║██║   ██║██╔══╝╚════╝${AnsiColor.RED} ██║   ██║╚════██║
${AnsiColor.GREEN} ██║  ██║███████╗██████╔╝╚██████╔╝███████╗    ${AnsiColor.RED} ╚██████╔╝███████║
${AnsiColor.GREEN} ╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚══════╝    ${AnsiColor.RED} ╚═════╝ ╚══════╝

${AnsiColor.BRIGHT_BLUE}:: Hedge-OS 🤓  :: Running Spring Boot ${spring-boot.version} ::
:: https://www.hedge-os.com ::${AnsiColor.DEFAULT}
