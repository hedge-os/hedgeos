package com.hedgeos.service.curve;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharSource;
import com.google.protobuf.Timestamp;
import com.hedgeos.curve.*;
import com.hedgeos.general.Currency;
import com.hedgeos.opengamma.OpenGamma;
import com.opengamma.strata.basics.ReferenceData;
import com.opengamma.strata.data.ImmutableMarketData;
import com.opengamma.strata.loader.csv.QuotesCsvLoader;
import com.opengamma.strata.loader.csv.RatesCalibrationCsvLoader;
import com.opengamma.strata.market.curve.CurveGroupName;
import com.opengamma.strata.market.curve.CurveInfoType;
import com.opengamma.strata.market.curve.InterpolatedNodalCurve;
import com.opengamma.strata.market.curve.RatesCurveGroupDefinition;
import com.opengamma.strata.market.param.DatedParameterMetadata;
import com.opengamma.strata.pricer.curve.RatesCurveCalibrator;
import com.opengamma.strata.pricer.rate.ImmutableRatesProvider;
import io.grpc.stub.StreamObserver;
import org.apache.commons.math3.util.Pair;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.time.ZoneOffset.UTC;

@Component // makes Spring auto-wire a singleton wherever used
public class CurveServer extends CurveServiceGrpc.CurveServiceImplBase {
    @Override
    public void buildCurve(CurveRequest request, StreamObserver<CurveResponse> responseObserver) {
        CurveGroup curveGroup = request.getCurveGroup();

        // build curve group configuration and market data
        String groupsCsv = OpenGamma.buildCurveGroupCsv(curveGroup);
        String settingsCsv = OpenGamma.buildCurveSettingsCsv(request.getCurveDefList());
        String nodesCsv = OpenGamma.buildCurveNodesCsv(request.getCurveDefList());
        String marketDataCsv = OpenGamma.buildMarketDataCsv(request.getMarketDataList());

        // log start time for performance measurement
        long start = System.currentTimeMillis();

        // print the CSVs if Debug = true on the request
        if (request.getDebug()) {
            System.out.println(groupsCsv);
            System.out.println(settingsCsv);
            System.out.println(nodesCsv);
            System.out.println(marketDataCsv);
        }

        // set valuation timestamp
        LocalDate valuationDate = Instant.ofEpochSecond(curveGroup.getAsOfTime().getSeconds(),
                curveGroup.getAsOfTime().getNanos()).atZone(UTC).toLocalDate();

        // use standard rates curve calibrator and reference data in OpenGamma
        RatesCurveCalibrator ratesCurveCalibrator = RatesCurveCalibrator.standard();
        ReferenceData referenceData = ReferenceData.standard();

        // get OpenGamma's curve group definition, using standard reference data
        RatesCurveGroupDefinition ratesCurveGroupDefinition = RatesCalibrationCsvLoader.parse(
                CharSource.wrap(groupsCsv),
                CharSource.wrap(settingsCsv),
                ImmutableList.of(CharSource.wrap(nodesCsv))
        ).get(CurveGroupName.of(curveGroup.getGroupName())).filtered(valuationDate, referenceData);

        // extract market data
        ImmutableMarketData marketData = ImmutableMarketData.builder(valuationDate)
                .values(QuotesCsvLoader.parse(
                        valuationDate::equals, ImmutableList.of(CharSource.wrap(marketDataCsv))
                ).getOrDefault(valuationDate, ImmutableMap.of())).build();

        // calibrate the curve
        ImmutableRatesProvider ratesProvider = ratesCurveCalibrator.calibrate(ratesCurveGroupDefinition, marketData,
                referenceData);

        // prepare response
        CurveResponse.Builder responseBuilder = CurveResponse.newBuilder();

        // prepare curve builder and curve pairs
        List<Pair<com.opengamma.strata.market.curve.Curve, Curve.Builder>> curves = ratesProvider.getDiscountCurves()
                .entrySet().stream()
                .map(e -> new Pair<>(e.getValue(), Curve.newBuilder()
                        .setCurveType(CurveType.DISCOUNT)
                        .setCurrency(Currency.valueOf(e.getKey().getCode())))).collect(Collectors.toList());

        curves.addAll(ratesProvider.getIndexCurves().entrySet().stream()
                .map(e -> new Pair<>(e.getValue(), Curve.newBuilder()
                        .setCurveType(CurveType.PROJECTION)
                        .setIndex(OpenGamma.getIndex(e.getKey()))))
                .collect(Collectors.toList()));

        // transform each calibrated curve into HedgeOS format
        for (Pair<com.opengamma.strata.market.curve.Curve, Curve.Builder> c : curves) {
            com.opengamma.strata.market.curve.Curve openGammaCurve = c.getFirst();

            // for now, we only know how to deal with interpolated nodal curves
            if (!(openGammaCurve instanceof InterpolatedNodalCurve))
                throw new RuntimeException("Returned OpenGamma curve must be an InterpolatedNodalCurve");

            // cast it
            InterpolatedNodalCurve interpolatedCurve = (InterpolatedNodalCurve) openGammaCurve;

            // create curve points for our result
            double[] xValues = interpolatedCurve.getXValues().toArrayUnsafe();
            double[] yValues = interpolatedCurve.getYValues().toArrayUnsafe();

            // pedantic check if OG is working properly
            if (xValues.length != yValues.length)
                throw new RuntimeException("received inconsistent curve result from OpenGamma");

            // create our curve points
            List<CurvePoint> curvePoints = IntStream.range(0, xValues.length).mapToObj(
                    i -> CurvePoint.newBuilder()
                            .setFractionalYear(xValues[i])
                            .setDate(Timestamp.newBuilder()
                                    .setSeconds(((DatedParameterMetadata) interpolatedCurve.getParameterMetadata(i))
                                            .getDate().toEpochSecond(LocalTime.MIDNIGHT, UTC)))
                            .setDiscountFactor(yValues[i])
                            .build())
                    .collect(Collectors.toList());

            // build new curve
            Curve.Builder curveBuilder = c.getSecond();

            curveBuilder.setCurveName(interpolatedCurve.getName().getName())
                    .setAsOfTime(curveGroup.getAsOfTime())
                    .setDayCount(OpenGamma.getDayCount(
                            interpolatedCurve.getMetadata().getInfo(CurveInfoType.DAY_COUNT)))
                    .addAllPoints(curvePoints)
                    .setInterpolationDef(InterpolationDef.newBuilder()
                            .addInterpolationName(interpolatedCurve.getInterpolator().getName())
                            .addStartPoint(0));
        }

        // measure time
        int timePerCurve = (int) (System.currentTimeMillis() - start) / curves.size();

        // and add all to the response builder
        curves.stream().map(p -> p.getSecond().setMillisToBuild(timePerCurve)).forEach(responseBuilder::addCurve);

        // respond to observer
        responseObserver.onNext(responseBuilder.build());
        responseObserver.onCompleted();
    }
}
