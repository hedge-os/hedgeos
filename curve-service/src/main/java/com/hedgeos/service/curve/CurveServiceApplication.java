package com.hedgeos.service.curve;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableSwagger2
public class CurveServiceApplication  implements CommandLineRunner  {

	@Autowired
	CurveServerGrpc curveServerGrpc;

	@Override
	public void run(String... args) throws Exception {

		curveServerGrpc.startGrpc();

	}


	public static void main(String[] args) throws Exception {
		SpringApplication.run(CurveServiceApplication.class, args);

	}
}
