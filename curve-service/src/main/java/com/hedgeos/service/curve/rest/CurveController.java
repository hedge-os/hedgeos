package com.hedgeos.service.curve.rest;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.hedgeos.curve.Curve;
import com.hedgeos.curve.CurveResponse;
import com.hedgeos.service.curve.CurveDemo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@RequiredArgsConstructor
@RestController("/curve")
public class CurveController {

    CurveDemo curveDemo = new CurveDemo();

    ExecutorService executor = Executors.newFixedThreadPool(8);

    @GetMapping("/go/{times}")
    public String runDemo(@PathVariable("times") int times) throws InvalidProtocolBufferException {
        List<Curve> curves = new ArrayList<>();

        List<Future<Curve>> futures = new ArrayList<>();

        CurveResponse.Builder crb = CurveResponse.newBuilder();

        for (int i=0; i<times; i++) {
            Future<Curve> f = executor.submit(() -> curveDemo.runDemo());
            futures.add(f);
        }

        for (Future<Curve> future : futures) {
            // get result from Future
            try {
                Curve result = future.get();

                crb.addCurve(result);

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }


        JsonFormat.Printer jPrinter = JsonFormat.printer();
        String result = jPrinter.print(crb.build());

        return result;
    }


    @GetMapping("demo")
    public String demoCurveBuilds() throws InvalidProtocolBufferException {
        Curve curve = curveDemo.runDemo();
        JsonFormat.Printer jPrinter = JsonFormat.printer();
        String result = jPrinter.print(curve);
        return result;
    }
}
