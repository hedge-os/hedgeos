package com.hedgeos.service.curve;

import com.google.protobuf.Timestamp;
import com.hedgeos.curve.*;
import com.hedgeos.daycount.DayCount;
import com.hedgeos.general.Currency;
import com.hedgeos.general.MarketData;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.mock.curve.TestData;
import io.grpc.stub.StreamObserver;
import org.apache.commons.math3.util.Pair;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.hedgeos.mock.curve.TestData.*;

public class CurveDemo {


    private static final String STAR_LINE = "************************************************";

    static long DEPO_SID = 1;
    static long FUT_SID = 2;
    static long SWAP_SID = 3;

    // nice touch in proto java, the setters, take builders themselves (need not call "build")
    static SecurityKey.Builder DEPO_SECURITY_KEY = SecurityKey.newBuilder().setSecurityId(DEPO_SID);
    static SecurityKey.Builder FUT_SECURITY_KEY = SecurityKey.newBuilder().setSecurityId(FUT_SID);
    static SecurityKey.Builder SWAP_SECURITY_KEY = SecurityKey.newBuilder().setSecurityId(SWAP_SID);


    CurveStreamObserverInProcess inProcessStreamObserver = new CurveStreamObserverInProcess();


    public Curve runDemo() {

        CurveRequest usCurveRequest = makeCurveRequestDemo();

        // TODO:  Shock market data random
//        double random = Math.random() / 10000;
//        for (MarketData marketData : usCurveRequest.getMarketDataList()) {
//
//        }

        long start = System.currentTimeMillis();

        CurveServer curveServer = new CurveServer();
        curveServer.buildCurve(usCurveRequest, inProcessStreamObserver);

        long end = System.currentTimeMillis();
        long took = end - start;

        if (usCurveRequest.getDebug()) {
            System.out.println(STAR_LINE + "\nCurve took(ms)=" + took + "\n" + STAR_LINE);
        }

        // List<CurvePoint> curvePoints = inProcessStreamObserver.curveResponse.getCurve().getPointsList();

        Curve curve = inProcessStreamObserver.curveResponse.getCurve(0);

        return curve;
    }

    private CurveRequest makeCurveRequestDemo() {
        // set reference data snapshot time
        Instant refTime = Instant.parse("2020-09-30T19:48:41.418Z");

        // define discount curve index
        Index discountIndex = Index.USD_FED_FUNDS;

        // helper function for aesthetics
        Function<Index, String> indexToString = s -> s.name().replace('_', '-');

        // start defining the curve group for this request and set desired discount curve
        CurveGroup.Builder curveGroupBuilder = CurveGroup.newBuilder()
                .setGroupName("USD-OIS-TEST")
                .setAsOfTime(Timestamp.newBuilder().setSeconds(refTime.getEpochSecond()).setNanos(refTime.getNano()))
                .setCurrencyToDiscountCurve(CurrencyToDiscountCurve.newBuilder()
                        .setCurrency(Currency.USD)
                        .setCurveName(indexToString.apply(discountIndex)));

        // start defining the curve pillars and reference data for this request
        Map<Index, Pair<SecurityWithMark[], RawCurveReference[]>> curves = new HashMap<>();
        curves.put(Index.USD_FED_FUNDS, new Pair<>(getUsdOisSecurities(), getUsdOisReferenceCurve()));
//        curves.put(Index.USD_SOFR, new Pair<>(getUsdSofrSecurities(), getUsdSofrReferenceCurve()));
        curves.put(Index.USD_LIBOR_1M, new Pair<>(getUsdLibor1MSecurities(), getUsdLibor1MReferenceCurve()));
        curves.put(Index.USD_LIBOR_3M, new Pair<>(getUsdLibor3MSecurities(), getUsdLibor3MReferenceCurve()));
        curves.put(Index.USD_LIBOR_6M, new Pair<>(getUsdLibor6MSecurities(), getUsdLibor6MReferenceCurve()));

        // define day count convention to use
        DayCount dayCount = DayCount.ACT_365F;

        // set maturity/settlement time... equal to the snapshot time for now (TODO an issue discuss with you / Chris!)
//        LocalTime settlementTime = LocalTime.MIDNIGHT;
        LocalTime settlementTime = refTime.atZone(ZoneOffset.UTC).toLocalTime();
//        LocalTime settlementTime = LocalTime.MAX;

        // prepare CurveDef builder to use for each curve as they have common values
        CurveDef.Builder curveDefBuilder = CurveDef.newBuilder()
                .setDayCount(dayCount)
                .setInterpolationDef(InterpolationDef.newBuilder().addInterpolationName("Linear").addStartPoint(0));

        // prepare curve and market data definitions
        List<CurveDef> curveDefs = new ArrayList<>(curves.size());
        List<MarketData> marketData = new ArrayList<>();
//        Map<Index, CurveReference[]> refs = new HashMap<>();

        // prepare CurveReference factory
        Function<RawCurveReference, CurveReference> curveReferenceFactory = CurveReference.getFactory(settlementTime,
                refTime, dayCount);

        for (Map.Entry<Index, Pair<SecurityWithMark[], RawCurveReference[]>> curve : curves.entrySet()) {
            Index index = curve.getKey();
            SecurityWithMark[] pillars = curve.getValue().getFirst();

            // include each into the curve group
            curveGroupBuilder.addIndexToForwardCurve(IndexToForwardCurve.newBuilder()
                    .setIndex(index)
                    .setCurveName(indexToString.apply(index)));

            // build the curve def and add it to the list
            curveDefBuilder.setName(indexToString.apply(index));
            curveDefBuilder.addAllSecurities(
                    Stream.of(pillars).map(SecurityWithMark::getSecurity).collect(Collectors.toList())
            );
            curveDefs.add(curveDefBuilder.build());

            // reset builder for next use
            curveDefBuilder.clearSecurities();

            // collate market data
            marketData.addAll(Stream.of(pillars).map(s -> s.getMarketData(refTime)).collect(Collectors.toList()));

            // prepare reference data by creating fractional year curve references
//            refs.put(index, Stream.of(curve.getValue().getSecond())
//                    .map(curveReferenceFactory)
//                    .toArray(CurveReference[]::new));
        }

        // TODO:  Convexity Adjustments, are critical, support them as interesting Market Data, flagged as such

        // finally put together the curve request
        CurveRequest usCurveRequest = CurveRequest.newBuilder()
                .setCurveGroup(curveGroupBuilder)
                .addAllCurveDef(curveDefs)
                .addAllMarketData(marketData)
                .setDebug(true)
                .build();

        return usCurveRequest;
    }


    // we make our own, non-network callback
    // this is also a technique by which you may freely run servers, In Process..
    public static class CurveStreamObserverInProcess implements StreamObserver<CurveResponse> {
        public CurveResponse curveResponse;
        public Throwable error;
        boolean completed = false;

        @Override
        public void onNext(CurveResponse curveResponse) {
            this.curveResponse = curveResponse;
        }

        @Override
        public void onError(Throwable throwable) {
            this.error = throwable;
        }

        @Override
        public void onCompleted() {
            completed = true;
        }
    }


}
