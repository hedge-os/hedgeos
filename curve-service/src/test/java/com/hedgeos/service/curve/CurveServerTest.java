package com.hedgeos.service.curve;

import com.google.protobuf.Timestamp;
import com.hedgeos.curve.*;
import com.hedgeos.daycount.DayCount;
import com.hedgeos.general.Currency;
import com.hedgeos.general.MarketData;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.opengamma.OpenGamma;
import com.hedgeos.string.EnumUtil;
import io.grpc.stub.StreamObserver;
import org.apache.commons.math3.util.Pair;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.hedgeos.mock.curve.TestData.*;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CurveServerTest {
    private static final String STAR_LINE = "************************************************";

    static long DEPO_SID = 1;
    static long FUT_SID = 2;
    static long SWAP_SID = 3;

    // nice touch in proto java, the setters, take builders themselves (need not call "build")
    static SecurityKey.Builder DEPO_SECURITY_KEY = SecurityKey.newBuilder().setSecurityId(DEPO_SID);
    static SecurityKey.Builder FUT_SECURITY_KEY = SecurityKey.newBuilder().setSecurityId(FUT_SID);
    static SecurityKey.Builder SWAP_SECURITY_KEY = SecurityKey.newBuilder().setSecurityId(SWAP_SID);

    // we make our own, non-network callback
    // this is also a technique by which you may freely run servers, In Process..
    public static class CurveStreamObserverInProcess implements StreamObserver<CurveResponse> {
        public CurveResponse curveResponse;
        public Throwable error;
        boolean completed = false;

        @Override
        public void onNext(CurveResponse curveResponse) {
            this.curveResponse = curveResponse;
        }

        @Override
        public void onError(Throwable throwable) {
            this.error = throwable;
        }

        @Override
        public void onCompleted() {
            completed = true;
        }
    }

    @Test
    public void USD_OIS_TEST() {
        // set reference data snapshot time
        Instant refTime = Instant.parse("2020-09-30T19:48:41.418Z");

        // define discount curve index
        Index discountIndex = Index.USD_FED_FUNDS;

        // start defining the curve group for this request and set desired discount curve
        CurveGroup.Builder curveGroupBuilder = CurveGroup.newBuilder()
                .setGroupName("USD-OIS-TEST")
                .setAsOfTime(Timestamp.newBuilder().setSeconds(refTime.getEpochSecond()).setNanos(refTime.getNano()))
                .setCurrencyToDiscountCurve(CurrencyToDiscountCurve.newBuilder()
                        .setCurrency(Currency.USD)
                        .setCurveName(EnumUtil.indexToString(discountIndex)));

        // start defining the curve pillars and reference data for this request
        Map<Index, Pair<SecurityWithMark[], RawCurveReference[]>> refCurves = new HashMap<>();
        refCurves.put(Index.USD_FED_FUNDS, new Pair<>(getUsdOisSecurities(), getUsdOisReferenceCurve()));
//        curves.put(Index.USD_SOFR, new Pair<>(getUsdSofrSecurities(), getUsdSofrReferenceCurve()));
        refCurves.put(Index.USD_LIBOR_1M, new Pair<>(getUsdLibor1MSecurities(), getUsdLibor1MReferenceCurve()));
        refCurves.put(Index.USD_LIBOR_3M, new Pair<>(getUsdLibor3MSecurities(), getUsdLibor3MReferenceCurve()));
        refCurves.put(Index.USD_LIBOR_6M, new Pair<>(getUsdLibor6MSecurities(), getUsdLibor6MReferenceCurve()));

        // define day count convention to use
        DayCount dayCount = DayCount.ACT_365F;

        // set maturity/settlement time... equal to the snapshot time for now (TODO an issue discuss with you / Chris!)
//        LocalTime settlementTime = LocalTime.MIDNIGHT;
        LocalTime settlementTime = refTime.atZone(ZoneOffset.UTC).toLocalTime();
//        LocalTime settlementTime = LocalTime.MAX;

        // prepare CurveDef builder to use for each curve as they have common values
        CurveDef.Builder curveDefBuilder = CurveDef.newBuilder()
                .setDayCount(dayCount)
                .setInterpolationDef(InterpolationDef.newBuilder().addInterpolationName("Linear").addStartPoint(0));

        // prepare curve and market data definitions
        List<CurveDef> curveDefs = new ArrayList<>(refCurves.size());
        List<MarketData> marketData = new ArrayList<>();
        Map<Index, CurveReference[]> curveReferences = new HashMap<>();

        // prepare CurveReference factory
        Function<RawCurveReference, CurveReference> curveReferenceFactory = CurveReference.getFactory(settlementTime,
                refTime, dayCount);

        for (Map.Entry<Index, Pair<SecurityWithMark[], RawCurveReference[]>> curve : refCurves.entrySet()) {
            Index index = curve.getKey();
            SecurityWithMark[] pillars = curve.getValue().getFirst();

            // include each into the curve group
            curveGroupBuilder.addIndexToForwardCurve(IndexToForwardCurve.newBuilder()
                    .setIndex(index)
                    .setCurveName(EnumUtil.indexToString(index)));

            // build the curve def and add it to the list
            curveDefBuilder.setName(EnumUtil.indexToString(index));
            curveDefBuilder.addAllSecurities(
                    Stream.of(pillars).map(SecurityWithMark::getSecurity).collect(Collectors.toList())
            );
            curveDefs.add(curveDefBuilder.build());

            // reset builder for next use
            curveDefBuilder.clearSecurities();

            // collate market data
            marketData.addAll(Stream.of(pillars).map(s -> s.getMarketData(refTime)).collect(Collectors.toList()));

            // prepare reference data by creating fractional year curve references
            curveReferences.put(index, Stream.of(curve.getValue().getSecond())
                    .map(curveReferenceFactory)
                    .toArray(CurveReference[]::new));
        }

        // TODO:  Convexity Adjustments, are critical, support them as interesting Market Data, flagged as such

        // finally put together the curve request
        CurveRequest usCurveRequest = CurveRequest.newBuilder()
                .setCurveGroup(curveGroupBuilder)
                .addAllCurveDef(curveDefs)
                .addAllMarketData(marketData)
                .setDebug(true)
                .build();

        CurveStreamObserverInProcess inProcessStreamObserver = new CurveStreamObserverInProcess();

        long totalAfterJIT = 0;
        long totalTries = 20;
        boolean debug = usCurveRequest.getDebug();

        for (int i = 0; i < totalTries; i++) {
            long start = System.currentTimeMillis();

            CurveServer curveServer = new CurveServer();
            curveServer.buildCurve(usCurveRequest, inProcessStreamObserver);

            long end = System.currentTimeMillis();
            long took = end - start;

            if (i > 0) {
                totalAfterJIT += took;
            }

            // switch off curve building debug mode after first iteration
            if (usCurveRequest.getDebug()) usCurveRequest = usCurveRequest.toBuilder().setDebug(false).build();

            if (debug) {
                if (i == 0)
                    System.out.println("JIT and curve took(ms)=" + took);
                else
                    System.out.println(STAR_LINE + "\nCurve took(ms)=" + took + "\n" + STAR_LINE);
            }
            if (i > 0) {
                assertTrue("Curve building USD OIS too slow, took(ms)=" + took, took < 1000);
                assertTrue("Curve building USD OIS too fast", took > 10); // maybe nothing happened
            } else {
                // the JIT compiler runs the first (only once per JVM), curve build.
                assertTrue("Curve building USD OIS too slow w JIT, took(ms)=" + took, took < 4000);
            }
        }

        // make sure the average of the non JIT runs, is good
        double avg = totalAfterJIT / (1.0d * totalTries - 1);

        System.out.println(STAR_LINE + "\nAverage curve build time avg(ms)=" + avg + "\n" + STAR_LINE);

        // is 21.1ms on my i7 from 5 years aga.  150 is for GitLab which is slower.
        assertTrue("Average Curve time, too slow, too slow, avg(ms)=" + avg, avg < 500);

        // the last run, will be what is on the inProcessObserver, curveResponse
        assertNotNull("Missing curve response", inProcessStreamObserver.curveResponse);

        // prepare mapping result curves back to reference curves
        Map<Index, Curve> curves = new EnumMap<>(Index.class);
        for (Curve curve : inProcessStreamObserver.curveResponse.getCurveList()) {
            // only compare forward curves for now
            if (curve.getCurveType().equals(CurveType.DISCOUNT)) continue;
            curves.put(curve.getIndex(), curve);
        }
        assertEquals("Missing curve", curveReferences.size(), curves.size());

        // check each curve against its reference curve
        for (Map.Entry<Index, CurveReference[]> indexReference : curveReferences.entrySet()) {
            // get corresponding result curve points
            Curve curve = curves.get(indexReference.getKey());
            List<CurvePoint> curvePoints = curve.getPointsList();
            assertNotNull("Missing points", curvePoints);
            assertTrue("Not enough points returned", curvePoints.size() >= 1);

            System.out.println();
            System.out.println(EnumUtil.indexToString(indexReference.getKey()) + " (" + curve.getMillisToBuild() + "ms)"
                    + " curve comparison (h = HedgeOS, r = reference):");

            // to collect statistics
            double mean = 0.0, var = 0.0;

            // makeshift in-place interpolation: j is the first curve point with a fractional year greater than that of
            // the current reference point
            int j = 0;

            for (CurveReference ref : indexReference.getValue()) {
                // linearly interpolate from our points
                while (j < curvePoints.size() && curvePoints.get(j).getFractionalYear() < ref.fractionalYear)
                    j++;

                // get zero rate from our curve
                double z;

                if (curvePoints.isEmpty())
                    z = 0.0;
                else if (j == 0) // flat extrapolation to the left
                    z = curvePoints.get(j).getDiscountFactor();
                else if (j == curvePoints.size()) // flat extrapolation to the right
                    z = curvePoints.get(j - 1).getDiscountFactor();
                else {
                    // linear interpolation between two points
                    CurvePoint l = curvePoints.get(j - 1);
                    CurvePoint r = curvePoints.get(j);

                    // normalized fraction of distance between left and right (a == 0 ==> @ left, a == 1 ==> @ right)
                    double a = (ref.fractionalYear - l.getFractionalYear())
                            / (r.getFractionalYear() - l.getFractionalYear());

                    // the actual interpolation
                    z = (1.0 - a) * l.getDiscountFactor() + a * r.getDiscountFactor();
                }

                // transform zero rate into discount factor
                double df = Math.exp(-z * ref.fractionalYear);

                // express difference in basis points
                double delta = (df - ref.value) * 10_000;
                mean += delta;
                var += delta * delta;
                System.out.printf("\u03c4 = %+7.3f, h = %18.16f, r = %18.16f, \u0394 = %+11.5f bps\n",
                        ref.fractionalYear, df, ref.value, delta);
            }

            // calculate and display matching statistics
            mean /= curvePoints.size();
            var /= curvePoints.size();
            var -= mean * mean;
            System.out.printf("                                                             \u03bc = %+11.5f bps\n",
                    mean);
            System.out.printf("                                                             \u03c3 = %+11.5f bps\n",
                    Math.sqrt(var));

//            for (int i = 0; i < curvePoints.size(); i++) {
//                double absDiff = Math.abs(refs[i].value - curvePoints.get(i).getDiscountFactor());
//                System.out.println("Diff=" + absDiff + ", point=" + curvePoints.get(i).getFractionalYear());
//                assertTrue("DF is off by more than .01 bps", absDiff < .00001);
//            }
        }
        // performance stats again
        System.out.println(STAR_LINE + "\nAverage curve build time avg(ms)=" + avg + "\n" + STAR_LINE);
    }
}
