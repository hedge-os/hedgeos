# Building is very simple (intentionally)
# 1.  Build the Maven project pom.xml which builds all pom.xml in every folder
# 2.  Build every docker image with the spring-boot jars for every java service (docker-compose build)
# 3.  'npm' on the react project which generates the front end result for deployment

# let druid write data
chmod a+rwx druid/storage

# must liquibase the database into place?
# builds every java project
mvn -DskipTests clean install

# builds every docker file with Java projects
docker-compose build

# build the react user interface
cd user-interface && ./build.sh
