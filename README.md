# Hedge-OS

#### "Your hedge fund technical co-founder, in a box."

### Getting Started

Five minutes
1.  Start the system in docker-compose
1.  Review the system
1.  Move the data storage (volumes), to permanent storage.
1.  Restart the system.  

Two hours
1.  (Optional) Connect market data

Half Day
1.  Connect and test trading with your prime broker


### Architecture diagram

(see individual Component README.md for more extensive diagramming and explanation)

![Arch diagram](docs/ArchDiagram.png)

### Summary Instructions
1.  Run Docker Compose in the root of the hedge-os project
1.  ```docker-compose up --build```
1.  Navigate to the homepage
1.  Review the real time PnL

If Docker is not installed, pls. follow these instructions.

Windows and Linux development is supported.
Docker on Windows was buggy but has improved! 
(Docker Desktop 3.0.0, 12/27/2020)

### Linux Startup
1.  Install Docker service
1.  Install docker-compose, add to bin folder or $PATH in .bashrc
1.  sudo add yourself to the "docker" group (if not already done)
1.  ```sudo groupadd docker```
1.  ```sudo usermod -aG docker $USER```
1.  Run docker compose against the docker-compose.yml in the root
cd hedge-os; 
1. ```docker-compose up```

### Windows Startup
1.  Enable Hyper-V for Windows, and install Hyper-V if not done
    https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v

1.  Install Docker Desktop for Windows
https://docs.docker.com/docker-for-windows/install/

1.  Install docker-compose for Windows
https://docs.docker.com/compose/install/

1.  Docker Settings (right click windows tasks "whale" icon)
![Docker Windows Settings](docs/readme/right-click-docker-windows-settings.PNG)

1.  Enable unauthenticated connections in Docker Desktop
  ![Docker Windows](docs/readme/docker-enable-tls-localhost.PNG)

1.  Enable local disk storage:  C:\Coding\hedge-os (for dev databases)
![Docker Windows Enable Storage](docs/readme/enable-disk-storage-windows-docker.PNG)

