package com.hedgeos.service.security.domain;


import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Stock Equity Spring JPA domain class.
 *
 * What little we want to know about an equity which is not
 * on the base security table.
 */
@Data
@Entity
@Cacheable
@Table(name = "equity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EquityJpa implements Serializable {

    @Id
    @Column(name="sid")
    private Long sid;

    public void setSid(Long sid) { this.sid = sid; }

    @Id
    public Long getSid() { return sid; }

    Boolean isEtf;
    Integer isEtn; // TODO:  Make a boolean?
    Long originalSid;
    Double adjustmentFactor;
    String creationReasonCode;
    String deathReasonCode;
    String primaryCountryIso;
    Integer parentCompanyId;
    String parentCompanyIso;
}
