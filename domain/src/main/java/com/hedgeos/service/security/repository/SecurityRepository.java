package com.hedgeos.service.security.repository;

import com.hedgeos.service.security.domain.SecurityJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import javax.persistence.Table;

@Repository
public interface SecurityRepository extends JpaRepository<SecurityJpa, Long> {

    SecurityJpa findBySid(long sid);
}
