package com.hedgeos.service.security.jpa;

import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
// NOTE:  Enable Default = false is critical for the Hazelcast READERS (many bothans!!)
// @EnableJpaRepositories(value = "com.hedgeos.service.security", enableDefaultTransactions = false)
// @EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
@EnableTransactionManagement // TODO:  Turn off writes here
@EntityScan(basePackages = {"com.hedgeos.service.security"})
// TODO: May need
// @PropertySource({"classpath:persistence-multiple-db-boot.properties"})
@EnableJpaRepositories(
        enableDefaultTransactions = false,
        basePackages = "com.hedgeos.service.security.repository",
        entityManagerFactoryRef = "securityEntityManager",
        transactionManagerRef = "securityTransactionManager")
public class SecurityJpaDatabaseConfig {


    @Bean(name = "securityDataSource")
    @ConfigurationProperties(prefix="security-datasource")
    public DataSource securityDataSource(Environment env) {
        return DataSourceBuilder.create()
                .url(env.getProperty("spring.security-datasource.url"))
                .username(env.getProperty("spring.security-datasource.username"))
                .password(env.getProperty("spring.security-datasource.password"))
                .type(HikariDataSource.class)
                .build();
    }

    @Bean(name = "securityEntityManager")
    public LocalContainerEntityManagerFactoryBean securityEntityManager(@Qualifier("securityDataSource") DataSource securityDataSource)
    {
        // EntityManagerFactoryBuilder builder can be passed above

        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        em.setDataSource(securityDataSource);
        em.setPackagesToScan("com.hedgeos.service.security");

        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        final HashMap<String, Object> properties = new HashMap<String, Object>();
        /*
        This is important for the underscore naming in the DB to match.

        hibernate:
            ddl-auto: none
            naming:
                physical-strategy: org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy
                implicit-strategy: org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy

         */
        properties.put("hibernate.ddl-auto", "none");

        // the web does not tell you this, boo.  nor do any docs?!
        // from "AvailableSettings.java" in hibernate-orm, 5.4 branch
        properties.put("hibernate.physical_naming_strategy", SpringPhysicalNamingStrategy.class.getName());
        properties.put("hibernate.implicit_naming_strategy", SpringImplicitNamingStrategy.class.getName());
        properties.put("hibernate.cache.use_query_cache", "true");


        properties.put("hibernate.dialect", "io.github.jhipster.domain.util.FixedPostgreSQL10Dialect");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.format_sql", "true");
        properties.put("spring.jpa.show-sql", true);

        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean(name = "securityTransactionManager")
    public JpaTransactionManager transactionManager(@Qualifier("securityEntityManager") EntityManagerFactory refDataEntityManager){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(refDataEntityManager);

        return transactionManager;
    }

}
