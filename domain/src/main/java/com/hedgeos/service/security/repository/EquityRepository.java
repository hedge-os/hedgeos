package com.hedgeos.service.security.repository;

import com.hedgeos.service.security.domain.EquityJpa;
import com.hedgeos.service.security.domain.SecurityJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquityRepository extends JpaRepository<EquityJpa, Long> {

    EquityJpa findBySid(long sid);
}
