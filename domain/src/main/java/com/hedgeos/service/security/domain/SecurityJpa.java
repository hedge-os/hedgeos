package com.hedgeos.service.security.domain;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * The base table from which every security is derived
 *      (sid = security id)
 * - Also contains every symbology we could reasonably support
 * - as well as securityTypeId which is the 'discriminator'
 *      for which sub-table to use
 */
@Data
@Entity
@Table(name = "security")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SecurityJpa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // security_id shorthand = sid
    @Column(name = "sid", columnDefinition = "serial")
    private Long sid;

    public void setSid(Long sid) { this.sid = sid; }

    @Id
    public Long getSid() { return sid; }

    private Long securityTypeId;
    private Long securitySubType;
    private String name;
    private String bbTicker;
    private String figi;
    private String isin;
    private String cusip;
    private String sedol;
    private String ric;
    private String occCode;
    private String primaryExchangeMic;
    private String primaryExchangeTicker;
    private String description;
    private LocalDate startTradingDate;
    private LocalDate deathDate;

}
