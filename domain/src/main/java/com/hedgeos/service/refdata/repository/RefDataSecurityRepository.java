package com.hedgeos.service.refdata.repository;

import com.hedgeos.service.refdata.domain.Security;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Security entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RefDataSecurityRepository extends JpaRepository<Security, Long> {
}
