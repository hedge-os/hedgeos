package com.hedgeos.service.refdata.jpa;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;


@Configuration
@EnableJpaRepositories(
    entityManagerFactoryRef = "refdataEntityManager",
    transactionManagerRef = "refdataTransactionManager",
    basePackages = {"com.hedgeos.service.refdata.repository"})
@EnableTransactionManagement
// TODO:  Turn auditing back on; the other projects need to load this bean
// @EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
@Slf4j
public class RefdataDatabaseConfiguration {

    private final Environment env;

    public RefdataDatabaseConfiguration(Environment env) {
        this.env = env;
    }

    // @Primary
    @Bean(name = "refdataDataSource")
    @ConfigurationProperties(prefix="refdata-datasource")
    public DataSource refdataDataSource(Environment env) {
        return DataSourceBuilder.create()
            .url(env.getProperty("spring.refdata-datasource.url"))
            .username(env.getProperty("spring.refdata-datasource.username"))
            .password(env.getProperty("spring.refdata-datasource.password"))
            .type(HikariDataSource.class)
            .build();
    }


    @Bean(name = "refdataEntityManager")
    public LocalContainerEntityManagerFactoryBean refDataEntityManager(@Qualifier("refdataDataSource") DataSource refdataDataSource)
    {
        // EntityManagerFactoryBuilder builder can be passed above

        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        em.setDataSource(refdataDataSource);
        em.setPackagesToScan("com.hedgeos.service.refdata");

        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        final HashMap<String, Object> properties = new HashMap<String, Object>();
        /*
        This is important for the underscore naming in the DB to match.

        hibernate:
            ddl-auto: none
            naming:
                physical-strategy: org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy
                implicit-strategy: org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy

         */
        properties.put("hibernate.ddl-auto", "none");

        // the web does not tell you this, boo.  nor do any docs?!
        // from "AvailableSettings.java" in hibernate-orm, 5.4 branch
        properties.put("hibernate.physical_naming_strategy", SpringPhysicalNamingStrategy.class.getName());
        properties.put("hibernate.implicit_naming_strategy", SpringImplicitNamingStrategy.class.getName());
        properties.put("hibernate.cache.use_query_cache", "true");


        properties.put("hibernate.dialect", "io.github.jhipster.domain.util.FixedPostgreSQL10Dialect");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.format_sql", "true");
        properties.put("spring.jpa.show-sql", true);

        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean(name = "refdataTransactionManager")
    public JpaTransactionManager transactionManager(@Qualifier("refdataEntityManager") EntityManagerFactory refDataEntityManager){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(refDataEntityManager);

        return transactionManager;
    }
}
