package com.hedgeos.service.refdata.repository;

import com.hedgeos.service.refdata.domain.CurveDefinition;
import com.hedgeos.service.refdata.domain.Portfolio;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Portfolio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PortfolioRepository extends JpaRepository<Portfolio, Long> {
    Portfolio findById(long id);
}
