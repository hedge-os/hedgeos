package com.hedgeos.service.refdata.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A CurveSecurity.
 */
@Entity
@Table(name = "curve_security")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CurveSecurity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "version")
    private Long version;

    @Column(name = "security_id")
    private Integer securityId;

    @ManyToOne
    @JsonIgnoreProperties(value = "curveSecurities", allowSetters = true)
    private CurveDefinition curveDefinition;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public CurveSecurity version(Long version) {
        this.version = version;
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Integer getSecurityId() {
        return securityId;
    }

    public CurveSecurity securityId(Integer securityId) {
        this.securityId = securityId;
        return this;
    }

    public void setSecurityId(Integer securityId) {
        this.securityId = securityId;
    }

    public CurveDefinition getCurveDefinition() {
        return curveDefinition;
    }

    public CurveSecurity curveDefinition(CurveDefinition curveDefinition) {
        this.curveDefinition = curveDefinition;
        return this;
    }

    public void setCurveDefinition(CurveDefinition curveDefinition) {
        this.curveDefinition = curveDefinition;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CurveSecurity)) {
            return false;
        }
        return id != null && id.equals(((CurveSecurity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CurveSecurity{" +
            "id=" + getId() +
            ", version=" + getVersion() +
            ", securityId=" + getSecurityId() +
            "}";
    }
}
