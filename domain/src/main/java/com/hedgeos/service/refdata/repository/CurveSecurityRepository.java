package com.hedgeos.service.refdata.repository;

import com.hedgeos.service.refdata.domain.CurveSecurity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CurveSecurity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CurveSecurityRepository extends JpaRepository<CurveSecurity, Long> {
}
