package com.hedgeos.service.refdata.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Security.
 */
@Entity
@Table(name = "security")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Security implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "security_id")
    private Integer securityId;

    @Column(name = "name")
    private String name;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSecurityId() {
        return securityId;
    }

    public Security securityId(Integer securityId) {
        this.securityId = securityId;
        return this;
    }

    public void setSecurityId(Integer securityId) {
        this.securityId = securityId;
    }

    public String getName() {
        return name;
    }

    public Security name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Security)) {
            return false;
        }
        return id != null && id.equals(((Security) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Security{" +
            "id=" + getId() +
            ", securityId=" + getSecurityId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
