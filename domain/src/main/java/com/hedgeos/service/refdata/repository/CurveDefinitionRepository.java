package com.hedgeos.service.refdata.repository;

import com.hedgeos.service.refdata.domain.CurveDefinition;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CurveDefinition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CurveDefinitionRepository extends JpaRepository<CurveDefinition, Long> {

    CurveDefinition findById(long id);
}
