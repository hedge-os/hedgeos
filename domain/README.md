## Domain objets (JPA)

Reference data is cached everywhere, locally
in Hazelcast, NearCache (in process.)

We don't need it to move around at high speed
like orders or fills, as it sits in memory and is slow moving.
Same for Security objects, for the most part.

While both are modeled as Protobufs,
we load them using JPA into Java Beans and they are avaialble.
And convienient until such a time or if we decide everything will be a 
protobuf, gRPC call which it may be in the future.

Until then, the Hazecast cache is connected to everything
and the JPA objects are loaded from there.

To do this, we share these beans and the JPA "repositories".

Note, a beautiful accident, but truly notable:

- When you debug, one service, this service uses the Databases to fetch everything.
- But in Prod, everything will be in a warmed Hazelcast cluster

This way, when you debug, you don't need to spin up the services which manage
the entities (ref-data and security), merely spin up 
position-service by itself, and it will use databases.

This reduces the burden of debugging, not needing to run
more than the service you are working on, a beautiful thing.
