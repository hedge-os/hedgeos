package com.hedgeos.ui.api.hedgeos;

import com.hedgeio.trade.Order;
import com.hedgeio.trade.Side;
import com.hedgeos.date.ProtoConverter;
import lombok.Data;

@Data
public class UiOrder {

    // for debugging, keep the order on the UI Order but don't serialize it
    private transient Order order;

    private final long hedgeOsOrderId;
    private final Double price;
    private final Double limitPrice;
    private final String accountCode;
    private final long accountId;
    private final String fixOrderId;
    private final long sid;
    private final String side;
    private final Double stopPrice;
    private String portfolio;
    private String strategy;
    private String bbTicker;

    public UiOrder(Order order) {
        this.order = order;

        // TODO:  Builder pattern?
        this.hedgeOsOrderId = order.getHedgeOsOrderId();

        // TODO: remove one of these
        this.price = ProtoConverter.priceToDouble(order.getLimitPrice());
        this.limitPrice = ProtoConverter.priceToDouble(order.getLimitPrice());
        this.accountCode = order.getAccount().getCode();
        this.accountId = order.getAccount().getAccountId();
        this.fixOrderId = order.getFixOrderId();
        this.sid = order.getSid();
        this.side = order.getSide().name();
        this.stopPrice = ProtoConverter.priceToDouble(order.getStopPrice());
    }



}
