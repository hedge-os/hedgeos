package com.hedgeos.ui.api.aggrid;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import java.util.List;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "filterType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = NumberColumnFilter.class, name = "number"),
        @JsonSubTypes.Type(value = SetColumnFilter.class, name = "set") })
@Data
public class ColumnFilter {
    String filterType;
    String type;

    // will be an integer for number column filters
    Object filter;

    // if a Set Column filter, this is populated
    List<String> values;

    // if a Number Column Filter, this is populated
    Integer filterTo;
}