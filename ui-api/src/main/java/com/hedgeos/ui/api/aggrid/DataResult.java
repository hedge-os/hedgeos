package com.hedgeos.ui.api.aggrid;

import java.util.List;

public class DataResult {
    private List<String> data;
    private long lastRow;
    private List<String> secondaryColumns;

    public DataResult(List<String> data, long rowCount, List<String> secondaryColumns) {
        this.data = data;
        this.lastRow = rowCount;
        this.secondaryColumns = secondaryColumns;
    }

    // this is AG code I believe
    public static String asJsonResponse(DataResult result) {
        String secondaryColumns = result.getSecondaryColumns().isEmpty() ? "" :
                "\"" + String.join("\", \"", result.getSecondaryColumns()) + "\"";

        return "{" +
                "\"data\": [" + String.join(",", result.getData()) + "], " +
                "\"lastRow\":" + result.getLastRow() + ", " +
                "\"secondaryColumns\": [" + secondaryColumns + "] " +
                "}";
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public long getLastRow() {
        return lastRow;
    }

    public void setLastRow(long lastRow) {
        this.lastRow = lastRow;
    }

    public List<String> getSecondaryColumns() {
        return secondaryColumns;
    }

    public void setSecondaryColumns(List<String> secondaryColumns) {
        this.secondaryColumns = secondaryColumns;
    }
}