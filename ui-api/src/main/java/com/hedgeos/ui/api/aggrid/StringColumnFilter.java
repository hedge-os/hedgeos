package com.hedgeos.ui.api.aggrid;

import lombok.Data;

@Data
public class StringColumnFilter extends ColumnFilter {

    private String type;
    private String filter;

    public StringColumnFilter() {}

    public StringColumnFilter(String type, String filter) {
        this.filter = filter;
        this.type = type;
    }

}
