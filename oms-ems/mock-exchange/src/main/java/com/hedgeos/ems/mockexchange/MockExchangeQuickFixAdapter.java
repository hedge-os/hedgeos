package com.hedgeos.ems.mockexchange;

import com.hedgeos.ems.util.PrecisionConstants;
import com.hedgeos.test.IntegrationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;
import quickfix.Message;
import quickfix.MessageCracker;
import quickfix.field.*;
import quickfix.fix44.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

import static java.time.ZoneOffset.UTC;

public class MockExchangeQuickFixAdapter extends MessageCracker implements quickfix.Application {

    private static final String DEFAULT_MARKET_PRICE_KEY = "DefaultMarketPrice";
    private static final String ALWAYS_FILL_LIMIT_KEY = "AlwaysFillLimitOrders";
    private static final String VALID_ORDER_TYPES_KEY = "ValidOrderTypes";
    public static final int MsgTypeField = 35;
    public static final String REJECT_MSG_TYPE_FIELD_VALUE = "j";

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final boolean alwaysFillLimitOrders;
    private final HashSet<String> validOrderTypes = new HashSet<>();
    private MarketDataProvider marketDataProvider;

    final MockExchangeCsvSymbology symbology = new MockExchangeCsvSymbology();


    public MockExchangeQuickFixAdapter(SessionSettings settings) throws ConfigError, FieldConvertError {
        initializeValidOrderTypes(settings);
        initializeMarketDataProvider(settings);

        alwaysFillLimitOrders = settings.isSetting(ALWAYS_FILL_LIMIT_KEY) && settings.getBool(ALWAYS_FILL_LIMIT_KEY);
    }


    private void initializeMarketDataProvider(SessionSettings settings) throws ConfigError, FieldConvertError {
        if (settings.isSetting(DEFAULT_MARKET_PRICE_KEY)) {
            if (marketDataProvider == null) {
                final double defaultMarketPrice = settings.getDouble(DEFAULT_MARKET_PRICE_KEY);
                marketDataProvider = new MarketDataProvider() {
                    public double getAsk(String symbol) {
                        return defaultMarketPrice;
                    }

                    public double getBid(String symbol) {
                        return defaultMarketPrice;
                    }
                };
            } else {
                log.warn("Ignoring {} since provider is already defined.", DEFAULT_MARKET_PRICE_KEY);
            }
        }
    }

    private void initializeValidOrderTypes(SessionSettings settings) throws ConfigError, FieldConvertError {
        if (settings.isSetting(VALID_ORDER_TYPES_KEY)) {
            List<String> orderTypes = Arrays
                    .asList(settings.getString(VALID_ORDER_TYPES_KEY).trim().split("\\s*,\\s*"));
            validOrderTypes.addAll(orderTypes);
        } else {
            validOrderTypes.add(OrdType.LIMIT + "");
        }
    }

    @Override
    public void onCreate(SessionID sessionId) {
        System.out.println("onCreate");
        Session.lookupSession(sessionId).getLog().onEvent("Valid order types: " + validOrderTypes);
    }

    @Override
    public void onLogon(SessionID sessionId) {
        System.out.println("onLogon");
    }

    @Override
    public void onLogout(SessionID sessionId) {
        System.out.println("onLogout");
    }

    @Override
    public void toAdmin(Message message, SessionID sessionId) {

    }

    @Override
    public void fromAdmin(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {

    }

    @Override
    public void toApp(Message message, SessionID sessionId) throws DoNotSend {
        log.warn("toApp, sessionID: "+sessionId+", message-->"+message);
    }

    @Override
    public void fromApp(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        log.warn("From app, session:"+sessionId+", message-->"+message);

        try {
            MsgType msgType = new MsgType();
            // CharField msgType = new CharField(MsgTypeField);
            StringField value = message.getField(msgType);
            if (value.getValue().equals(REJECT_MSG_TYPE_FIELD_VALUE)) {
                log.error("Business message reject, dropping=" + message);
                return;
            }

        } catch (Exception e) {
            log.error("Exception getting msgType="+e);
        }

        crack(message, sessionId);
    }


    public void onMessage(quickfix.fix44.BusinessMessageReject reject, SessionID session) {
        // without this the business message rejects go back and forth.
        log.error("Business message reject from EMS, response was not supported.");
    }


    private boolean isOrderExecutable(Message order) throws FieldNotFound {
        if (order.getChar(OrdType.FIELD) == OrdType.LIMIT) {
            BigDecimal limitPrice = new BigDecimal(order.getString(Price.FIELD));
            return (PrecisionConstants.isEqual(limitPrice.doubleValue(),IntegrationConstants.MAGIC_FILL_PRICE));
        }

        return true;
    }

    private Price getPrice(Message message) throws FieldNotFound {
        Price price;
        if (message.getChar(OrdType.FIELD) == OrdType.LIMIT && alwaysFillLimitOrders) {
            price = new Price(message.getDouble(Price.FIELD));
        } else {
            if (marketDataProvider == null) {
                throw new RuntimeException("No market data provider specified for market order");
            }
            char side = message.getChar(Side.FIELD);
            if (side == Side.BUY) {
                price = new Price(marketDataProvider.getAsk(message.getString(Symbol.FIELD)));
            } else if (side == Side.SELL || side == Side.SELL_SHORT) {
                price = new Price(marketDataProvider.getBid(message.getString(Symbol.FIELD)));
            } else {
                throw new RuntimeException("Invalid order side: " + side);
            }
        }
        return price;
    }

    private void sendMessage(SessionID sessionID, Message message) {
        try {
            Session session = Session.lookupSession(sessionID);
            if (session == null) {
                throw new SessionNotFound(sessionID.toString());
            }

            DataDictionaryProvider dataDictionaryProvider = session.getDataDictionaryProvider();
            if (dataDictionaryProvider != null) {
                try {
                    dataDictionaryProvider.getApplicationDataDictionary(
                            getApplVerID(session, message)).validate(message, true);
                } catch (Exception e) {
                    LogUtil.logThrowable(sessionID, "Outgoing message failed validation: "
                            + e.getMessage(), e);
                    return;
                }
            }

            session.send(message);
        } catch (SessionNotFound e) {
            log.error(e.getMessage(), e);
        }
    }

    private ApplVerID getApplVerID(Session session, Message message) {
        String beginString = session.getSessionID().getBeginString();
        if (FixVersions.BEGINSTRING_FIXT11.equals(beginString)) {
            return new ApplVerID(ApplVerID.FIX50);
        } else {
            return MessageUtils.toApplVerID(beginString);
        }
    }

    final Map<String, Map<String, NewOrderSingle>> buyOrdersResting = new ConcurrentSkipListMap<>();
    final Map<String, Map<String, NewOrderSingle>> sellOrdersResting = new ConcurrentSkipListMap<>();

    // TODO: Load the Symbology CSVs on startup, this links them

    public void onMessage(quickfix.fix44.OrderCancelReplaceRequest replace, SessionID sessionID) throws FieldNotFound {
        try {
            // TODO:  Test Replace Price, Stop, Limit
            // if (replace.getOrderQty() == null)

            // testing, auto-cancel reject on a specific symbol
            if (replace.getSymbol().getValue().equals(symbology.orderCancelRejectSymbol)) {
                // always cancel reject, as a mock test
                sendCancelReject("planned", CxlRejReason.BROKER_EXCHANGE_OPTION,
                        genOrderID(), replace.getClOrdID(), replace.getOrigClOrdID(), sessionID);
                return;
            }

            // TODO: invalid cancel handling; overall scripting by symbol
            if (isBuy(replace.getSide().getValue())) {

                Map<String, NewOrderSingle> buyOrdersForSymbol = buyOrdersResting.get(replace.getSymbol().getValue());

                NewOrderSingle original = buyOrdersForSymbol.get(replace.getOrigClOrdID().getValue());
                // TODO:  Send a cancel reject; then Trades..
                // https://www.onixs.biz/fix-dictionary/4.4/app_dD.1.b.html

                if (original != null) {
                    double quantity = replace.getOrderQty().getValue();
                    // https://www.onixs.biz/fix-dictionary/4.4/app_dC.1.a.html
                    ExecutionReport replacePending =
                            makePendingReplace(quantity, replace, original);
                    sendMessage(sessionID, replacePending);
                    log.warn("SENT Pending Replace.");
                    ExecutionReport replaceReport =
                            makeReplace(quantity, replace, original);
                    sendMessage(sessionID, replaceReport);
                    // TODO:  Send another fill after
                    return;
                }
            }
            else {
                Map<String, NewOrderSingle> sellOrdersForSymbol = sellOrdersResting.get(replace.getSymbol().getValue());
                NewOrderSingle original = sellOrdersForSymbol.get(replace.getOrigClOrdID().getValue());
                if (original != null) {
                    double quantity = replace.getOrderQty().getValue();
                    ExecutionReport replacePending =
                            makePendingReplace(quantity, replace, original);
                    sendMessage(sessionID, replacePending);
                    log.warn("SENT Pending Replace.");
                    ExecutionReport replaceReport =
                            makeReplace(quantity, replace, original);
                    sendMessage(sessionID, replaceReport);
                    log.warn("SENT Pending Replace.");
                    return;
                }
            }

            sendCancelReject("unknown original",CxlRejReason.UNKNOWN_ORDER,
                    genOrderID(), replace.getClOrdID(), replace.getOrigClOrdID(), sessionID);
        }
        catch (Exception e) {
            log.error("Excepting sending message, likely missing a field="+e, e);
            throw e;
        }
    }

    private void sendCancelReject(String text, int cxlRejReason, OrderID orderID, ClOrdID replace, OrigClOrdID replace1, SessionID sessionID) throws FieldNotFound {
        OrderCancelReject cancelReject =
                new OrderCancelReject(orderID,
                        replace,
                        replace1,
                        new OrdStatus(OrdStatus.REJECTED),
                        new CxlRejResponseTo(CxlRejResponseTo.ORDER_CANCEL_REQUEST));

        cancelReject.set(new CxlRejReason(cxlRejReason));
        cancelReject.set(new Text(text));
        log.warn("Sending cancel REJECT: " + cancelReject);
        sendMessage(sessionID, cancelReject);
    }

    private ExecutionReport makePendingReplace(double quantity,
                                               OrderCancelReplaceRequest replace,
                                               NewOrderSingle original)
            throws FieldNotFound
    {
        ExecutionReport replacePending =
                new ExecutionReport(
                        genOrderID(),
                        genExecID(),
                        new ExecType(ExecType.PENDING_REPLACE),
                        new OrdStatus(OrdStatus.PENDING_REPLACE),
                        replace.getSide(),
                        new LeavesQty(original.getOrderQty().getValue()),
                        new CumQty(0),
                        new AvgPx(0));
        // OrderQty required
        replacePending.set(new OrderQty(quantity));
        // symbol required on exec report; "Instrument Block"
        replacePending.set(original.getSymbol());
        replacePending.set(replace.getClOrdID());
        replacePending.set(replace.getOrigClOrdID());
        return replacePending;
    }

    private ExecutionReport makeReplace(double quantity,
                                        OrderCancelReplaceRequest replace,
                                        NewOrderSingle original)
            throws FieldNotFound
    {
        CumQty cumQty = new CumQty(replace.getOrderQty().getValue());
        LeavesQty leavesQty = new LeavesQty(original.getOrderQty().getValue());
        OrdStatus ordStatus = new OrdStatus(OrdStatus.PARTIALLY_FILLED);
        // TODO:  Improve to do real quantity, decide if filled
        if (replace.getOrderQty().getValue() >= original.getOrderQty().getValue()) {
            ordStatus = new OrdStatus(OrdStatus.FILLED);
            leavesQty = new LeavesQty(0);
        }

        ExecutionReport replacePending =
                new ExecutionReport(
                        genOrderID(),
                        genExecID(),
                        new ExecType(ExecType.REPLACED),
                        ordStatus,
                        replace.getSide(),
                        leavesQty,
                        cumQty,
                        new AvgPx(0));
        // OrderQty required
        replacePending.set(new OrderQty(quantity));

        // symbol required on exec report; "Instrument Block"
        replacePending.set(original.getSymbol());
        replacePending.set(replace.getClOrdID());
        replacePending.set(replace.getOrigClOrdID());
        return replacePending;
    }


    public void onMessage(quickfix.fix44.OrderCancelRequest cancel, SessionID sessionID) throws FieldNotFound {
        try {
            // testing, auto-cancel reject on a specific symbol
            if (cancel.getSymbol().getValue().equals(symbology.orderCancelRejectSymbol)) {
                // always cancel reject, as a mock test
                sendCancelReject("planned", CxlRejReason.BROKER_EXCHANGE_OPTION,
                        genOrderID(), cancel.getClOrdID(), cancel.getOrigClOrdID(), sessionID);
                return;
            }

            // TODO: invalid cancel handling; overall scripting by symbol
            if (isBuy(cancel.getSide().getValue())) {
                if (foundOriginalOrder(buyOrdersResting, cancel, sessionID)) {
                    log.info("Found original order for cancel");
                    return;
                }
            }
            else {
                if (foundOriginalOrder(sellOrdersResting, cancel, sessionID)) {
                    log.info("Found original order for cancel");
                    return;
                }
            }

            // send Cancel Reject
            sendCancelReject("unknown original",CxlRejReason.UNKNOWN_ORDER,
                    genOrderID(), cancel.getClOrdID(), cancel.getOrigClOrdID(), sessionID);
        }
        catch (Exception e) {
            log.error("Excepting sending message, likely missing a field="+e, e);
        }
        catch (Error e) {
            log.error("ERROR sending message", e);
        }
    }

    private boolean foundOriginalOrder(Map<String, Map<String, NewOrderSingle>> orderRestingMap, OrderCancelRequest cancel, SessionID sessionID) throws FieldNotFound {
        Map<String, NewOrderSingle> ordersForSymbol =
                orderRestingMap.get(cancel.getSymbol().getValue());

        if (ordersForSymbol != null) {
            NewOrderSingle newOrderSingle = ordersForSymbol.get(cancel.getOrigClOrdID().getValue());
            if (newOrderSingle != null) {
                makeAndSendCancelSuccess(cancel, sessionID, newOrderSingle);
                return true;
            }
        }
        return false;
    }

    private void makeAndSendCancelSuccess(OrderCancelRequest cancel,
                                          SessionID sessionID,
                                          NewOrderSingle newOrderSingle)
            throws FieldNotFound
    {
        ExecutionReport cancelSuccess = new ExecutionReport(
                genOrderID(),
                genExecID(),
                new ExecType(ExecType.CANCELED),
                new OrdStatus(OrdStatus.CANCELED),
                cancel.getSide(),
                new LeavesQty(newOrderSingle.getOrderQty().getValue()),
                new CumQty(0), new AvgPx(0));

        cancelSuccess.set(new OrderQty(newOrderSingle.getOrderQty().getValue()));

        // symbol required on exec report; "Instrument Block"
        cancelSuccess.set(newOrderSingle.getSymbol());
        cancelSuccess.set(cancel.getClOrdID());
        cancelSuccess.set(cancel.getOrigClOrdID());
        cancelSuccess.set(new TransactTime(LocalDateTime.now(UTC)));

        sendMessage(sessionID, cancelSuccess);
        log.warn("SENT CANCEL SUCCESS.");
    }

    public void onMessage(quickfix.fix44.ResendRequest resendRequest, SessionID sessionID) {
        log.info("Resend request handled");
    }

    public void onMessage(quickfix.fix44.NewOrderSingle order, SessionID sessionID) throws FieldNotFound
    {
        try {
            int rejectReason = isValidOrder(order);
            if (rejectReason != 0) {
                // reject
                quickfix.fix44.ExecutionReport reject = new quickfix.fix44.ExecutionReport(
                        genOrderID(), genExecID(), new ExecType(ExecType.REJECTED), new OrdStatus(
                        OrdStatus.REJECTED), order.getSide(),
                        new LeavesQty(order.getOrderQty().getValue()),
                        new CumQty(0), new AvgPx(0));
                reject.set(new OrdRejReason(rejectReason));
                reject.set(order.getSymbol());
                reject.set(order.getClOrdID());
                sendMessage(sessionID, reject);
            }

            OrderQty orderQty = order.getOrderQty();

            // Price price = getPrice(order);
            Price price = order.getPrice();

            quickfix.fix44.ExecutionReport accept = new quickfix.fix44.ExecutionReport(
                    genOrderID(), genExecID(), new ExecType(ExecType.NEW), new OrdStatus(
                    OrdStatus.NEW), order.getSide(), new LeavesQty(order.getOrderQty()
                    .getValue()), new CumQty(0), new AvgPx(0));

            accept.set(order.getClOrdID());
            accept.set(order.getSymbol());
            // NEW state of order starts
            sendMessage(sessionID, accept);

            char side = order.getChar(Side.FIELD);

            if (isOrderExecutable(order)) {
                quickfix.fix44.ExecutionReport executionReport = new quickfix.fix44.ExecutionReport(genOrderID(),
                        genExecID(), new ExecType(ExecType.TRADE), new OrdStatus(OrdStatus.FILLED), order.getSide(),
                        new LeavesQty(0), new CumQty(orderQty.getValue()), new AvgPx(price.getValue()));

                executionReport.set(order.getClOrdID());
                executionReport.set(order.getSymbol());
                executionReport.set(orderQty);
                executionReport.set(new LastQty(orderQty.getValue()));
                executionReport.set(new LastPx(price.getValue()));
                executionReport.set(new OrderQty(orderQty.getValue()));
                executionReport.set(new CumQty(orderQty.getValue()));
                executionReport.set(new TransactTime(LocalDateTime.now(UTC)));
                // fills the entire order
                sendMessage(sessionID, executionReport);
            }
            else {
                // add to a buy or sell orders
                if (isBuy(side)) {
                    buyOrdersResting.putIfAbsent(order.getSymbol().getValue(), new ConcurrentHashMap<>());
                    buyOrdersResting.get(order.getSymbol().getValue())
                            .put(order.getClOrdID().getValue(), order);
                }
                else {
                    sellOrdersResting.putIfAbsent(order.getSymbol().getValue(), new ConcurrentHashMap<>());
                    sellOrdersResting.get(order.getSymbol().getValue())
                            .put(order.getClOrdID().getValue(), order);
                }
            }
        } catch (RuntimeException e) {
            LogUtil.logThrowable(sessionID, e.getMessage(), e);
        }
        catch (Exception e) {
            log.error("Exception on new order", e);
        }
        catch (Error e) {
            e.printStackTrace();
            System.out.println("exception on new order="+e);
            log.error("Error on new order", e);
        }
    }

    private int isValidOrder(NewOrderSingle order) throws FieldNotFound {
        if (order.isSetSide() == false)
            return OrdRejReason.OTHER;
        if (order.isSetSymbol() == false)
            return OrdRejReason.UNKNOWN_SYMBOL;
        if (order.isSetOrderQty() == false)
            return OrdRejReason.INCORRECT_QUANTITY;
        if (order.getOrderQty().getValue() <= 0.0) {
            return OrdRejReason.INCORRECT_QUANTITY;
        }
        if (order.isSetOrdType() == false) {
            return OrdRejReason.OTHER;
        }
        if (order.getOrdType().valueEquals(OrdType.LIMIT)) {
            if (false == order.isSetPrice()) {
                return OrdRejReason.INVALID_PRICE_INCREMENT;
            }
        }
        return 0;
    }

    private boolean isBuy(char side) {
        return side == Side.BUY;
    }


    public OrderID genOrderID() {
        return new OrderID(Integer.toString(++m_orderID));
    }

    public ExecID genExecID() {
        return new ExecID(Integer.toString(++m_execID));
    }

    /**
     * Allows a custom market data provider to be specified.
     *
     * @param marketDataProvider
     */
    public void setMarketDataProvider(MarketDataProvider marketDataProvider) {
        this.marketDataProvider = marketDataProvider;
    }

    private volatile int m_orderID = 0;
    private volatile int m_execID = 0;
}
