package com.hedgeos.ems.mockexchange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import quickfix.SessionSettings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@SpringBootApplication
public class MockExchangeApplication {

    private final static Logger log = LoggerFactory.getLogger(MockExchange.class);

    public static void main(String[] args) {
        try {
            // SpringApplication.run(MockExchange.class, args);
            SessionSettings settings = loadSettings(args);

            MockExchange executor = new MockExchange(settings);
            executor.start();

            while (true) {
                Thread.sleep(100);
                // System.out.println("Mock exchange is running.");
            }
            // executor.stop();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    static InputStream getSettingsInputStream(String[] args) throws FileNotFoundException {
        InputStream inputStream = null;
        if (args.length == 0) {
            inputStream = MockExchange.class.getResourceAsStream("executor.cfg");
        } else if (args.length == 1) {
            inputStream = new FileInputStream(args[0]);
        }

        if (inputStream == null) {
            System.out.println("Input stream not found in classpath: executor.cfg (will look for a file)");
            System.out.println("usage: " + MockExchange.class.getName() + " [configFile].");

            File f = new File(".");
            System.out.println("Root dir in:"+f.getAbsolutePath());
            // look in 'oms-ems' package first
            String settingsFileRoot = "./mock-exchange/src/main/resources/com/hegeos/ems/mockexchange/executor.cfg";
            System.out.println("looking for settings file="+settingsFileRoot);
            File mockExchangeDir = new File("./logs/fix/mock-exchange");
            boolean madeDirs = mockExchangeDir.mkdirs();
            System.out.println("Made dirs for mock exchange?"+madeDirs);
            File relativeToOmsEms = new File(settingsFileRoot);
            if (relativeToOmsEms.exists() == false) {
                settingsFileRoot = "./oms-ems/mock-exchange/src/main/resources/com/hegeos/ems/mockexchange/executor.cfg";
                relativeToOmsEms = new File(settingsFileRoot);
                if (relativeToOmsEms.exists() == false) {
                    System.out.println("Can't find file: "+settingsFileRoot);
                }
            }
            inputStream = new FileInputStream(settingsFileRoot);
        }
        return inputStream;
    }

    private static SessionSettings loadSettings(String[] args) {
        try {
            InputStream inputStream = getSettingsInputStream(args);
            SessionSettings settings = new SessionSettings(inputStream);
            inputStream.close();
            return settings;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
