package com.hedgeos.ems.mockexchange;

import com.hedgeos.test.IntegrationConstants;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MockExchangeCsvSymbology {
    public String SECURITY_ID_COLUMN_NAME = "sid";

    static String MOCK_BLOOMBERG_SYMBOLOGY_FILE = "./ems-config-mock-files/symbology/bloomberg_20230520.csv";

    public Map<String, Long> symbolToSid = new ConcurrentHashMap<>();

    public String orderCancelRejectSymbol;

    public MockExchangeCsvSymbology() {

        File currentFile = new File(MOCK_BLOOMBERG_SYMBOLOGY_FILE);
        try (Reader reader = Files.newBufferedReader(currentFile.toPath(), StandardCharsets.UTF_8)) {
            CSVFormat format = CSVFormat.Builder.create().setHeader().build();
            CSVParser parser = CSVParser.parse(reader, format);
            // load up the map
            for (CSVRecord csvRecord : parser) {
                if (csvRecord.size() == 0) {
                    // skip blank lines
                    continue;
                }

                if (csvRecord.values()[0].startsWith("#")) {
                    // treat this as comments
                    continue;
                }

                String sidString = csvRecord.get(SECURITY_ID_COLUMN_NAME);
                String symbol = csvRecord.get("symbol");
                long sid = Long.parseLong(sidString);
                symbolToSid.put(symbol, sid);
                // set up the mapping between the EMS test for Cancel Reject and the Mock Exchange here
                if (sid == IntegrationConstants.CANCEL_REJECT_TEST_SECURITY_ID) {
                    orderCancelRejectSymbol = symbol;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
