package com.hedgeos.ems.mockexchange;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

// tests if spring loads properly, the MockExchangeQuickFixApplication
// too magical for my taste, but this is how Spring 3.0 does it.
@SpringBootTest
class MockExchangeApplicationTests {

    @Test
    void contextLoads() {
    }

}
