### Mock Exchange

QuickFix J Mock Exchange for testing.

Goals of the mock exchange:
1. Certification and re-certification plans for all counterparties
2. Test harness for cancel completed order and other boundary conditions
3. Performance testing of the ems-service