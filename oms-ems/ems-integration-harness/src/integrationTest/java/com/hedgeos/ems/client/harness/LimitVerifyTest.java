package com.hedgeos.ems.client.harness;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hedgeos.ems.order.EmsRequestBuilder;
import order.*;

import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.emsClient;
import static com.hedgeos.ems.client.harness.EmsIntegrationTestHarnessIT.makeRequestId;
import static com.hedgeos.test.TestConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LimitVerifyTest {

    private static final double UNIMPORTANT_LIMIT_PRICE = 1.23;
    public static final int ONE_TOO_MANY_QTY = 201_000;
    public static final int ACCEPTABLE_QTY = 200;

    @BeforeAll
    public static void beforeAll() {
        BeforeAllSetupHarness.setupHarness();
    }

    @Test
    public void testBadSymbol() {
        long hedgeOsOrderId = emsClient.getNextOrderId();
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        makeRequestId(),
                        PM_GROUP_CODE, ACCOUNT_1,
                        hedgeOsOrderId,
                        ONE_TOO_MANY_QTY,
                        Side.SELL,
                        MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        UNKNOWN_SID,
                        UNIMPORTANT_LIMIT_PRICE);

        order.LimitVerifyResponse response = emsClient.sendLimitVerify(request);
        assertEquals(1, response.limitVerifyResponsesLength());
        assertEquals(OrderSubmitStatus.SYMBOL_MAPPING_FAILED, response.overallResult());
        LimitVerifyOrderResponse oneResponse = response.limitVerifyResponses(0);
        assertEquals(hedgeOsOrderId, oneResponse.hedgeOsOrderId());
    }


    @Test
    public void testOverTradingShortLimit() {

        long hedgeOsOrderId = emsClient.getNextOrderId();
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        makeRequestId(),
                        PM_GROUP_CODE, ACCOUNT_1,
                        hedgeOsOrderId,
                        ONE_TOO_MANY_QTY,
                        Side.SELL,
                        MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        FAKE_REPEATED_SHORT_SELL_THEN_CANCEL_ID,
                        UNIMPORTANT_LIMIT_PRICE);

        order.LimitVerifyResponse response = emsClient.sendLimitVerify(request);
        assertEquals(1, response.limitVerifyResponsesLength());
        LimitVerifyOrderResponse oneResponse = response.limitVerifyResponses(0);
        assertEquals(OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_ONE_SECOND, response.overallResult());
        assertEquals(OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_ONE_SECOND, oneResponse.tradingLimitStatus());
        assertEquals(hedgeOsOrderId, oneResponse.hedgeOsOrderId());
    }

    @Test
    public void testShortLimitSuccess() {

        long hedgeOsOrderId = emsClient.getNextOrderId();
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        makeRequestId(),
                        PM_GROUP_CODE, ACCOUNT_1,
                        hedgeOsOrderId,
                        ACCEPTABLE_QTY,
                        Side.SELL,
                        MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        FAKE_REPEATED_SHORT_SELL_THEN_CANCEL_ID,
                        UNIMPORTANT_LIMIT_PRICE);

        order.LimitVerifyResponse response = emsClient.sendLimitVerify(request);
        assertEquals(1, response.limitVerifyResponsesLength());
        LimitVerifyOrderResponse oneResponse = response.limitVerifyResponses(0);
        assertEquals(OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_ONE_SECOND, oneResponse.tradingLimitStatus());
        assertEquals(hedgeOsOrderId, oneResponse.hedgeOsOrderId());
    }


}
