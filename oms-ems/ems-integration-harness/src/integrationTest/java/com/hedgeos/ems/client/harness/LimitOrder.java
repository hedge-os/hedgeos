package com.hedgeos.ems.client.harness;

import com.hedgeos.client.ems.EmsClient;
import com.hedgeos.client.ems.EmsClientCallback;
import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.test.IntegrationConstants;
import com.hedgeos.test.TestConstants;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class LimitOrder {


    public static final long BIG_QTY = 999;

    public static long sendLimitOrder(int side,
                                         long requestId,
                                         EmsClient emsClient,
                                         EmsClientCallback callback) {

        long orderId = emsClient.getNextOrderId();
        boolean worked = sendLimitOrder(TestConstants.FAKE_SECURITY_ID,
                emsClient, requestId, orderId, side, BIG_QTY,
                IntegrationConstants.BUY_LIMIT_PRICE_RESTS);

        assertTrue(worked, "Failed to send basic limit order");

        // wait for the report
        EmsReport emsReportLimit = callback.takeFirstEmsReport(60);
        assertNotNull(emsReportLimit, "Missing EmsReport of limit in NEW state");

        assertEquals(orderId, emsReportLimit.hedgeOsOrderId());

        return orderId;
    }


    public static boolean sendLimitOrder(long sid,
                                      EmsClient emsClient,
                                      long requestId,
                                      long hedgeOsOrderId,
                                      int side,
                                      long quantity,
                                      double price)
    {
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        requestId,
                        TestConstants.PM_GROUP_CODE,
                        TestConstants.ACCOUNT_1,
                        hedgeOsOrderId,
                        quantity,
                        side,
                        TestConstants.MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        sid,
                        price);

        List<OrderResponse> statusList = emsClient.sendOrder(request);
        log.warn("Orders sent!");
        for (OrderResponse response : statusList) {
            if (response.status() != OrderSubmitStatus.SUCCESSFUL_ORDER) {
                log.error("Failed to send a new limit order");
                return false;
            }

            log.warn("     status=SUCCESS,"+
                    ", orderId="+response.hedgeosOrderId()+
                    ", statusCode="+response.status());
            return true;
        }

        return false;
    }

}
