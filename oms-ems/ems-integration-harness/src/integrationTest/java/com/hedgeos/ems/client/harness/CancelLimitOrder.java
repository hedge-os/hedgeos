package com.hedgeos.ems.client.harness;

import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.client.ems.EmsClient;
import com.hedgeos.client.ems.EmsClientCallback;
import com.hedgeos.test.IntegrationConstants;
import com.hedgeos.test.TestConstants;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.util.List;

import static com.hedgeos.ems.client.harness.EmsIntegrationTestHarnessIT.makeRequestId;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class CancelLimitOrder {


    public static boolean cancelLimitOrder(
            long sid,
            int side,
            long requestId,
            EmsClient emsClient,
            EmsClientCallback callback) {

        long orderId = emsClient.getNextOrderId();
        sendLimitOrder(sid, emsClient, requestId, orderId, side, 99,
                IntegrationConstants.BUY_LIMIT_PRICE_RESTS);

        // wait for the report
        EmsReport emsReportLimit = callback.takeFirstEmsReport(60);
        assertNotNull(emsReportLimit, "Missing EmsReport of limit in NEW state");

        Order order = emsClient.getOrderForId(emsReportLimit.hedgeOsOrderId());
        if (order.hedgeOsOrderId() != orderId) {
            log.error("Different order returned, request was="+order.sid());
        }

        assertEquals(orderId, emsReportLimit.hedgeOsOrderId(), "wrong emsReport returned on Buy Limit");

        EmsReport emsReportCancel = cancelEmsReport(emsClient, callback, orderId);
        assertEquals(orderId, emsReportCancel.hedgeOsOriginalOrderId());
        assertEquals(EmsExecType.CANCEL_SUCCESS_EXEC_TYPE, emsReportCancel.emsExecType(),
                "Incorrect EMS ExecType for Cancel Success.");

        return true;
    }

    public static EmsReport cancelEmsReport(EmsClient emsClient, EmsClientCallback callback, long orderId) {
        // now cancel
        long cancelOrderId = cancelOrder(emsClient, makeRequestId(), orderId);
        EmsReport emsReportCancel = callback.takeFirstEmsReport();
        assertNotNull(emsReportCancel, "EmsReport for cancel failed to return in a second");
        if (cancelOrderId != emsReportCancel.hedgeOsOrderId()) {
            log.info("Strange unmatched orderId");
        }
        assertEquals(cancelOrderId, emsReportCancel.hedgeOsOrderId(), "order id incorrect on cancel returned");
        assertTrue(callback.isEmpty());

        return emsReportCancel;
    }

    public static void sendLimitOrder(long sid,
                                      EmsClient emsClient,
                                      long requestId,
                                      long hedgeOsOrderId,
                                      int side,
                                      long quantity,
                                      double price)
    {
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        requestId,
                        TestConstants.PM_GROUP_CODE, TestConstants.ACCOUNT_1,
                        hedgeOsOrderId,
                        quantity,
                        side,
                        TestConstants.MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        sid,
                        price);

        List<OrderResponse> statusList = emsClient.sendOrder(request);
        log.warn("Orders sent!");
        for (OrderResponse response : statusList) {
            String status = "FAILED";
            // TODO:  Translate to messages for each status code
            if (response.status() == OrderSubmitStatus.SUCCESSFUL_ORDER) {
                status = "SUCCESS";
            }
            log.warn("     status="+status+
                    ", orderId="+response.hedgeosOrderId()+
                    ", statusCode="+response.status());
            if (response.status() != OrderSubmitStatus.SUCCESSFUL_ORDER) {
                log.error("Failed to send a new limit order");
                throw new RuntimeException("Failed to send a new limit order");
            }
        }
    }



    public static long cancelOrder(EmsClient emsClient,
                                    long requestId,
                             long originalOrderIdToCancel) {
        long hedgeOsCancelId = emsClient.getNextOrderId();

        CancelRequest request =
                EmsRequestBuilder.createCancelRequest(emsClient.getConnection(),
                        requestId, hedgeOsCancelId, originalOrderIdToCancel);

        List<order.CancelResponse> statusList = emsClient.sendCancel(request);
        log.warn("Cancel sent!");
        for (CancelResponse cancelResponse : statusList) {
            // TODO:  Translate to messages for each status code
            log.warn("     status=" + CancelSubmitStatus.name(cancelResponse.status()) +
                    ", cancelId=" + cancelResponse.hedgeOsOrderId() +
                    ", statusCode=" + cancelResponse.status());

            if (cancelResponse.status() != CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS) {
                throw new RuntimeException("Submitting the cancel failed!");
            }
        }

        return hedgeOsCancelId;
    }

}
