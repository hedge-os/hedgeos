package com.hedgeos.ems.client.harness;

import org.junit.jupiter.api.Test;

import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.callback;
import static com.hedgeos.ems.client.harness.TestUtil.*;
import static com.hedgeos.test.TestConstants.POSITION_LONG_SECURITY_ID;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PositionShortingTest {

    // @RepeatedTest(10)
    @Test
    public void testShortingPosition() {
        BeforeAllSetupHarness.setupHarness();

        assertTrue(callback.isEmpty());
        // # long position 100 tests
        // 9223372036854775803,CMR,100,12.34

        // we cant sell 101; we only own 100; and no locates
        sellFailExpected(POSITION_LONG_SECURITY_ID, 101);
        // we CAN sell 100; we own 100
        long orderId = sellResting(POSITION_LONG_SECURITY_ID, 100);
        // we FAIL selling 1; we sold our position
        sellFailExpected(POSITION_LONG_SECURITY_ID, 1);
        // we buy 100
        buyExecutes(POSITION_LONG_SECURITY_ID, 100);

        // we FAIL sending 101
        sellFailExpected(POSITION_LONG_SECURITY_ID, 101);
        // we can now SELL 100; the position we bought
        sellExpectFilled(POSITION_LONG_SECURITY_ID, 100);

        // cleanup the limit we places
        cancelResting(orderId);
        assertTrue(callback.isEmpty());
    }
}
