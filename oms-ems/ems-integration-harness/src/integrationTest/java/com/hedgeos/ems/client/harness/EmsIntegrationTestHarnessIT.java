package com.hedgeos.ems.client.harness;


import com.hedgeos.test.TestConstants;
import lombok.extern.slf4j.Slf4j;
import order.EmsReport;
import order.Side;
import org.HdrHistogram.Histogram;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicLong;

import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.callback;
import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.emsClient;
import static com.hedgeos.ems.client.harness.CancelLimitOrder.cancelEmsReport;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class EmsIntegrationTestHarnessIT {



    // this is merely for reporting on server side; client should invent new ones
    public static AtomicLong requestId = new AtomicLong(0);


    public EmsIntegrationTestHarnessIT() {
        BeforeAllSetupHarness.setupHarness();
    }

    @Test
    public void testLimit() {
        assertTrue(callback.isEmpty());
        long orderId =
                LimitOrder.sendLimitOrder(Side.BUY, makeRequestId(), emsClient, callback);
        EmsReport emsReportCancel = cancelEmsReport(emsClient, callback, orderId);
        assertEquals(orderId, emsReportCancel.hedgeOsOriginalOrderId());
        assertTrue(callback.isEmpty());
    }

    static long makeRequestId() {
        return requestId.incrementAndGet();
    }


    @Test
    @Tag("integration")
    public void testCancelReject() {
        assertTrue(callback.isEmpty());
        Histogram histogram = new Histogram(1000L, 3);

        for (int i=0; i<20; i++) {
            long start = System.currentTimeMillis();
            boolean worked =
                    ExpectCancelReject.expectCancelRejectFromEms(makeRequestId(), emsClient);
            // log.warn("Took to cancel reject=" + (System.currentTimeMillis() - start));
            if (i > 10) {
                histogram.recordValue(System.currentTimeMillis() - start);
            }
            assertTrue(worked, "Canceling a non-existent order must produce a Cancel Reject");
        }

        histogram.outputPercentileDistribution(System.out, 1.0);
        long threshold = 3;
        double underMillis = histogram.getPercentileAtOrBelowValue(threshold);
        System.out.println("Percent of 20 cancel rejects under 3 millis="+underMillis);
        log.warn("Under {} millis={}",threshold, underMillis);
        assertTrue(callback.isEmpty());
    }


    @RepeatedTest(3)
    public void testLimitThenCancelBuy() {
        assertTrue(callback.isEmpty());
        boolean worked =
                CancelLimitOrder.cancelLimitOrder(TestConstants.FAKE_SECURITY_ID,
                        Side.BUY, makeRequestId(), emsClient, callback);
        assertTrue(worked, "Failed to cancel a limit order.");
        assertTrue(callback.isEmpty());
    }

    @RepeatedTest(3)
    public void testLimitThenCancelSellShort() {
        assertTrue(callback.isEmpty());
        boolean worked =
                CancelLimitOrder.cancelLimitOrder(TestConstants.FAKE_SECURITY_ID,
                        Side.SELL_SHORT, makeRequestId(), emsClient, callback);
        assertTrue(worked, "Failed to cancel a limit order.");
        assertTrue(callback.isEmpty());

    }


    /**
     * Decent test of returning limits, locates, when cancelling a short.
     */
    @Test
    public void testRepeatedLimitThenCancelSellShort() {
        assertTrue(callback.isEmpty());
        for (int i=0; i<100; i++) {
            boolean worked =
                    CancelLimitOrder.cancelLimitOrder(TestConstants.FAKE_REPEATED_SHORT_SELL_THEN_CANCEL_ID,
                            Side.SELL_SHORT, makeRequestId(), emsClient, callback);
            assertTrue(worked, "Failed to cancel a limit order.");
            log.info("Cancel Limit Sell completed={}", i);
        }
        assertTrue(callback.isEmpty());
    }

    @Test
    public void testCancelLimitOrderSellShort() {
        boolean worked =
                CancelSellLimitOrderReturnLocates.cancelLimitOrder(
                        makeRequestId(), emsClient, callback);
        assertTrue(worked, "Failed to cancel a sell short limit order.");
        assertTrue(callback.isEmpty());

    }

    @Test
    public void testLimitOrderViolateShortLimit() {
        boolean worked =
                TradeLimits.violateShortLimit(
                        makeRequestId(), emsClient, callback);
        assertTrue(worked, "Failed to prevent sellling a massive amount.");
        assertTrue(callback.isEmpty());

    }

    @Test
    public void testCancelRejectFromMockExchange() {
        boolean worked =
                ExpectCancelRejectFromMockExchange.
                        expectCancelRejectFromMockExchange(
                                makeRequestId(),emsClient, callback);
        assertTrue(worked, "Failed to cancel reject");
        assertTrue(callback.isEmpty());

    }


    @Test
    public void testReplaceFromMockExchange() {
        assertTrue(callback.isEmpty());
        boolean worked =
                ReplaceFromMockExchange.expectReplaceFromMockExchange(
                        makeRequestId(),emsClient, callback);
        assertTrue(worked, "failed to replace");
        assertTrue(callback.isEmpty());

    }

    @Test
    @Tag("integration")
    public void tradeAllNamesOneMessage() {
        assertTrue(callback.isEmpty());
        TradeAllNasdaqNamesOneSecondLimit.tradeAllNamesOneMessage(emsClient);
        assertTrue(callback.isEmpty());
    }


    @Test
    @Tag("integration")
    public void testTradeAllNasdaqNames() {
        TradeAllNasdaqNamesOneSecondLimit.tradeAllNames(emsClient);
    }
}
