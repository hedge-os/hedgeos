package com.hedgeos.ems.client.harness;

import com.hedgeos.client.ems.EmsClient;
import com.hedgeos.client.ems.EmsClientCallback;
import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.test.IntegrationConstants;
import com.hedgeos.test.TestConstants;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
public class CancelSellLimitOrderReturnLocates {


    public static boolean cancelLimitOrder(long requestId,
                                           EmsClient emsClient,
                                           EmsClientCallback callback) {

        long orderId = emsClient.getNextOrderId();
        sendLimitOrder(emsClient, requestId, orderId,
                Side.SELL_SHORT, 99, IntegrationConstants.SELL_PRICE_LIMIT_RESTS);

        // wait for the report
        EmsReport emsReportLimit = callback.takeFirstEmsReport(60);
        assertNotNull(emsReportLimit, "Missing EmsReport of limit in NEW state");
        assertTrue(callback.isEmpty());

        // now cancel
        long cancelOrderId = cancelOrder(emsClient, requestId, orderId);
        EmsReport emsReportCancel = callback.takeFirstEmsReport(60);
        assertNotNull(emsReportCancel, "EmsReport for cancel failed to return in a second");
        assertEquals(cancelOrderId, emsReportCancel.hedgeOsOrderId(), "order id incorrect on cancel returned");
        assertEquals(EmsExecType.CANCEL_SUCCESS_EXEC_TYPE, emsReportCancel.emsExecType(),
                "Incorrect EMS ExecType for Cancel Success.");
        assertTrue(callback.isEmpty());

        return true;
    }
    public static void sendLimitOrder(EmsClient emsClient,
                                      long requestId,
                                      long hedgeOsOrderId,
                                      int side,
                                      long quantity,
                                      double price)
    {
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        requestId,
                        TestConstants.PM_GROUP_CODE, TestConstants.ACCOUNT_1,
                        hedgeOsOrderId,
                        quantity,
                        side,
                        TestConstants.MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        TestConstants.FAKE_SECURITY_ID,
                        price);

        List<OrderResponse> statusList = emsClient.sendOrder(request);
        log.warn("Orders sent!");
        for (OrderResponse response : statusList) {
            String status = "FAILED";
            // TODO:  Translate to messages for each status code
            if (response.status() == OrderSubmitStatus.SUCCESSFUL_ORDER) {
                status = "SUCCESS";
            }
            log.warn("     status="+status+
                    ", orderId="+response.hedgeosOrderId()+
                    ", statusCode="+response.status());
            if (response.status() != OrderSubmitStatus.SUCCESSFUL_ORDER) {
                log.error("Failed to send a new limit order");
                throw new RuntimeException("Failed to send a new limit order");
            }
        }
    }



    private static long cancelOrder(EmsClient emsClient,
                                    long requestId,
                             long originalOrderIdToCancel) {
        long hedgeOsCancelId = emsClient.getNextOrderId();

        CancelRequest request =
                EmsRequestBuilder.createCancelRequest(emsClient.getConnection(),
                        requestId, hedgeOsCancelId, originalOrderIdToCancel);

        List<CancelResponse> statusList = emsClient.sendCancel(request);
        log.warn("Cancel sent!");
        for (CancelResponse cancelResponse : statusList) {
            // TODO:  Translate to messages for each status code
            log.warn("     status=" + CancelSubmitStatus.name(cancelResponse.status()) +
                    ", cancelId=" + cancelResponse.hedgeOsOrderId() +
                    ", statusCode=" + cancelResponse.status());

            if (cancelResponse.status() != CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS) {
                throw new RuntimeException("Submitting the cancel failed!");
            }
        }

        return hedgeOsCancelId;
    }

}
