package com.hedgeos.ems.client.harness;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class IncludedTest {

    @Test
    public void testIncludeGradle() {
        assertEquals(1, 1, "test");
    }
}
