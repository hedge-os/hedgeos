package com.hedgeos.ems.client.harness;

import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.test.IntegrationConstants;
import order.*;

import java.util.List;

import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.callback;
import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.emsClient;
import static com.hedgeos.ems.client.harness.EmsIntegrationTestHarnessIT.makeRequestId;
import static org.junit.jupiter.api.Assertions.*;

public class TestUtil {


    public static long sellResting(long sid, long qty) {
        long firstLimit = emsClient.getNextOrderId();
        boolean worked1 = LimitOrder.sendLimitOrder(sid,
                emsClient, makeRequestId(), firstLimit, Side.SELL, qty,
                IntegrationConstants.SELL_PRICE_LIMIT_RESTS);

        assertTrue(worked1, "failed to send FIRST limit order, qty="+qty);
        // wait for the report
        EmsReport emsReportLimit = callback.takeFirstEmsReport();
        assertNotNull(emsReportLimit, "Missing EmsReport of limit in NEW state");
        return firstLimit;
    }


    public static void buyExecutes(long sid, long qty) {
        // TODO:  Then trade, executed, wait for result, then another short.
        boolean buyWorks = LimitOrder.sendLimitOrder(sid,
                emsClient, makeRequestId(), emsClient.getNextOrderId(), Side.BUY, qty,
                IntegrationConstants.FILL_BUY_PRICE);
        assertTrue(buyWorks, "Failed to send a buy of="+qty);

        // wait for the report
        EmsReport buyNewReport = callback.takeFirstEmsReport();
        assertNotNull(buyNewReport, "Missing EmsReport of buy in NEW state.");
        // wait for another report
        EmsReport buyFilledReport = callback.takeFirstEmsReport();
        assertNotNull(buyFilledReport, "Missing EmsReport of filled buy.");
    }


    public static void sellExpectFilled(long sid, long sellFilledQty) {
        boolean sellTheBuy =
                LimitOrder.sendLimitOrder(sid,
                        emsClient, makeRequestId(), emsClient.getNextOrderId(), Side.SELL, sellFilledQty,
                        IntegrationConstants.FILL_SELL_PRICE);

        assertTrue(sellTheBuy, "Failed to sell the buy");
        EmsReport sellTheBuyReportNew = callback.takeFirstEmsReport(5);
        assertNotNull(sellTheBuyReportNew, "Missing EmsReport of Sell NEW.");
        EmsReport sellFilled = callback.takeFirstEmsReport(5);
        assertNotNull(sellFilled, "Missing EmsReport of Sell Filled.");
    }

    public static void sellFailExpected(long sid, long qty) {
        boolean expectedFail = LimitOrder.sendLimitOrder(sid,
                emsClient, makeRequestId(), emsClient.getNextOrderId(),
                Side.SELL, qty,
                IntegrationConstants.SELL_PRICE_LIMIT_RESTS);

        assertFalse(expectedFail);
    }

    public static void cancelResting(long orderId) {
        long cancelId = emsClient.getNextOrderId();
        CancelRequest request =
                EmsRequestBuilder.createCancelRequest(emsClient.getConnection(),
                        makeRequestId(), cancelId, orderId);

        List<CancelResponse> statusList = emsClient.sendCancel(request);
        for (CancelResponse cancelResponse : statusList) {

            if (cancelResponse.status() != CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS) {
                throw new RuntimeException("Submitting the cancel failed!");
            }
        }
    }
}
