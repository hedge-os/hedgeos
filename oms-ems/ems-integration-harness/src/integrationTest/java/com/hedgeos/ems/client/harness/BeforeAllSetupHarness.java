package com.hedgeos.ems.client.harness;

import com.hedgeos.client.ems.EmsClient;
import com.hedgeos.client.ems.EmsClientCallback;
import com.hedgeos.test.TestConstants;

import java.util.Arrays;
import java.util.List;

public class BeforeAllSetupHarness {

    static EmsClientCallback callback = new EmsClientCallback();


    static EmsClient emsClient;
    @org.junit.jupiter.api.BeforeAll
    public static void setupHarness() {
        String[] accountIds = new String[]{TestConstants.ACCOUNT_1, TestConstants.ACCOUNT_2};
        List<String> accountiDList = Arrays.asList(accountIds);

        String login = "testHarness";

        // note the "silly" security; to prevent accidental connections to Prod
        String passwordHash = login+"WORKSTATION";
        passwordHash = passwordHash + passwordHash.length();

        // construct the client
        if (emsClient == null) {
            emsClient = new EmsClient("127.0.0.1", 9090, -1, login, passwordHash, accountiDList);
            emsClient.networkStart();
            emsClient.connect(callback);
        }
    }
}
