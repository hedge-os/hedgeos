package com.hedgeos.ems.client.harness;

import com.hedgeos.ems.util.QuantityConverter;
import com.hedgeos.test.IntegrationConstants;
import com.hedgeos.test.TestConstants;
import lombok.SneakyThrows;
import order.EmsExecType;
import order.EmsReport;
import order.OrderStatus;
import order.Side;
import org.junit.jupiter.api.RepeatedTest;

import java.util.ArrayList;
import java.util.List;

import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.callback;
import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.emsClient;
import static com.hedgeos.ems.client.harness.EmsIntegrationTestHarnessIT.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class LocatesTest {

    @org.junit.jupiter.api.BeforeAll
    public static void start() {
        BeforeAllSetupHarness.setupHarness();
    }

    @RepeatedTest(3)
    public void testLocateBuyThenSell() {
        assertTrue(callback.isEmpty());
        int waitTime = 120;
        int qty = 10;
        int orderCount = 30;

        List<Long> toCancel = new ArrayList<>();
        for (int i=0; i< orderCount; i++) {
            long limitOrder = sellResting(qty);
            toCancel.add(limitOrder);
            buyExecutes(qty);
        }

        for (Long orderId : toCancel) {
            cancelRestingOrder(orderId);
        }

        // clean up
        // sell everything we bought; to clean up, in one order
        sellExpectFilled(orderCount * qty);
        assertTrue(callback.isEmpty());
    }

    @RepeatedTest(100)
    public void testLocateCancelReturnsTheLocates() {
        int qty = 100;

        assertTrue(callback.isEmpty());

        long orderId = sellResting(qty);
        long orderId2 = sellResting(qty);

        cancelRestingOrder(orderId);
        cancelRestingOrder(orderId2);
        assertTrue(callback.isEmpty());
    }

    private static void cancelRestingOrder(long orderId) {
        CancelLimitOrder.cancelEmsReport(emsClient, callback, orderId);
    }

    /**
     // starts with 200: locates, 100 position
     // # small locates for fail locates test
     //9223372036854775804,MORGAN_STANLEY,CMR,200
     // starts with 100 position
     // 9223372036854775804,CMR,100,12.34
     // means we can trade 300
     */
    // TODO:  This test needs to be repeatable; is not
    @RepeatedTest(3)
    public void testLocatesBasics() {
        BeforeAllSetupHarness.setupHarness();

        // sell 100
        int side = Side.SELL;

        sellFailExpected(side, 1000, "failed to prevent trading sell " + (long) 1000);

        // sell 200; this will take the position + 100 locates
        long firstLimit = sellResting(200);
        // sell 100; this will take the next 100 locates
        long secondLimit = sellResting(100);

        sellFailExpected(side, 101, "did not prevent trading 101");
        sellFailExpected(side, 1, "Failed to prevent trading 1");

        // buy 100, creates a 100 long traded
        buyExecutes(100);
        // sell 100, against the 100
        sellExpectFilled(100);

        sellFailExpected(side, 1, "Failed to prevent trading 1 again");

        cancelRestingOrder(firstLimit);
        cancelRestingOrder(secondLimit);

        // sell 200; this will take the position + 100 locates
        long startOver = sellResting(200);
        // sell 100; this will take the next 100 locates
        long startOverTwo = sellResting(100);
        cancelRestingOrder(startOver);
        cancelRestingOrder(startOverTwo);
    }

    private static void sellExpectFilled(long sellFilledQty) {
        boolean sellSuccess =
                LimitOrder.sendLimitOrder(TestConstants.FAKE_TEST_LOCATES_SECURITY_ID,
                    emsClient, makeRequestId(), emsClient.getNextOrderId(), Side.SELL, sellFilledQty,
                    IntegrationConstants.FILL_SELL_PRICE);

        assertTrue(sellSuccess, "Failed to sell: +"+sellFilledQty);
        EmsReport sellTheBuyReportNew = callback.takeFirstEmsReport(5);
        assertNotNull(sellTheBuyReportNew, "Missing EmsReport of Sell NEW.");
        EmsReport sellFilled = callback.takeFirstEmsReport(5);
        assertNotNull(sellFilled, "Missing EmsReport of Sell Filled.");
        assertEquals(sellFilledQty, QuantityConverter.quantityToDouble(sellFilled.cumulativeQty()).longValue());
    }

    private static void sellFailExpected(int side, long qty, String qty1) {
        boolean expectedFail =
                LimitOrder.sendLimitOrder(TestConstants.FAKE_TEST_LOCATES_SECURITY_ID,
                emsClient, makeRequestId(), emsClient.getNextOrderId(), side, qty,
                IntegrationConstants.SELL_PRICE_LIMIT_RESTS);

        assertFalse(expectedFail, qty1);
    }

    private static long sellResting(long qty) {
        long sellOrderId = emsClient.getNextOrderId();
        boolean sellRestingSuccess = LimitOrder.sendLimitOrder(
                TestConstants.FAKE_TEST_LOCATES_SECURITY_ID,
                emsClient, makeRequestId(), sellOrderId, Side.SELL, qty,
                IntegrationConstants.SELL_PRICE_LIMIT_RESTS);

        assertTrue(sellRestingSuccess, "failed to send FIRST limit order, qty="+qty);
        // wait for the report
        EmsReport emsReportLimit = callback.takeFirstEmsReport();
        assertNotNull(emsReportLimit, "Missing EmsReport of limit in NEW state");
        return sellOrderId;
    }

    @SneakyThrows
    private static void buyExecutes(long qty) {
        // TODO:  Then trade, executed, wait for result, then another short.
        boolean buyWorks = LimitOrder.sendLimitOrder(TestConstants.FAKE_TEST_LOCATES_SECURITY_ID,
                emsClient, makeRequestId(), emsClient.getNextOrderId(), Side.BUY, qty,
                IntegrationConstants.FILL_BUY_PRICE);
        assertTrue(buyWorks, "Failed to send a buy of 77");

        // wait for the report
        EmsReport buyNewReport = callback.takeFirstEmsReport();
        assertNotNull(buyNewReport, "Missing EmsReport of buy in NEW state.");
        assertEquals(OrderStatus.NEW_ORDER_STATUS, buyNewReport.orderStatus());
        assertEquals(0, buyNewReport.cumulativeQty().quantity());
        // wait for another report
        EmsReport buyFilledReport = callback.takeFirstEmsReport();
        assertNotNull(buyFilledReport, "Missing EmsReport of filled buy.");
        assertEquals(qty, buyFilledReport.cumulativeQty().quantity());
        assertEquals(OrderStatus.FILLED_ORDER_STATUS, buyFilledReport.orderStatus());
        assertTrue(buyFilledReport.emsExecType() == EmsExecType.TRADE_EXEC_TYPE ||
                    buyFilledReport.emsExecType() == EmsExecType.FILL_EXEC_TYPE);
        assertTrue(callback.isEmpty());

    }
}
