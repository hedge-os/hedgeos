package com.hedgeos.ems.client.harness;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.client.ems.EmsClient;
import com.hedgeos.ems.util.QuantityConverter;
import com.hedgeos.test.IntegrationConstants;
import com.hedgeos.test.TestConstants;
import general.Currency;
import general.Quantity;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.hedgeos.ems.order.EmsRequestBuilder.*;
import static org.junit.jupiter.api.Assertions.*;

import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.callback;
import static com.hedgeos.ems.client.harness.EmsIntegrationTestHarnessIT.makeRequestId;

@Slf4j
public class TradeAllNasdaqNamesOneSecondLimit {

    public static boolean tradeAllNamesOneMessage(EmsClient emsClient) {
        long start = System.currentTimeMillis();

        int orderType = OrdType.LIMIT;
        double limitPrice = IntegrationConstants.BUY_LIMIT_PRICE_RESTS;
        String pmGroupCode = "CMR";
        String account = "account1";
        long quantity = 3;
        int venueId = 7; // Morgan Stanley
        FlatBufferBuilder builder = new FlatBufferBuilder();

        int limitPriceOffset =
                QuantityConverter.doubleToPriceBuffer(builder, limitPrice, Currency.USD);

        int pmGroupOffset = builder.createString(pmGroupCode);
        int accountCodeOffset = builder.createString(account);

        ref_data.Account.startAccount(builder);
        ref_data.Account.addCode(builder, accountCodeOffset);
        int accountOffset = ref_data.Account.endAccount(builder);

        int evNameOffset = builder.createString("unused_name");

        int orderDateOffset = setLocalDateOnBuilder(builder);

        Quantity.startQuantity(builder);
        Quantity.addQuantity(builder, quantity);
        Quantity.addNanoQty(builder, 0);
        int quantityOffset = Quantity.endQuantity(builder);

        ExecutionVenue.startExecutionVenue(builder);
        ExecutionVenue.addName(builder, evNameOffset);
        ExecutionVenue.addId(builder, venueId);
        ExecutionVenue.addVenueType(builder, ExecutionVenueType.EQUITY);
        // name is not important
        // ExecutionVenue.addName(builder, "US_STOCK");
        int evOffset = ExecutionVenue.endExecutionVenue(builder);
        ArrayList<Integer> ordersArray = new ArrayList<>();
        for (int i=100; i<4500; i++) {

            addOrderToVector(emsClient, orderType, builder, limitPriceOffset,
                    pmGroupOffset, accountOffset, orderDateOffset, quantityOffset,
                    evOffset, ordersArray, i);

            // NOTICE:  You must add the Orders to a Vector, then create the vector
            // add the order above to the flatbuffer, must happen before startOrderRequest
        }

        int[] orderArrayInt = new int[ordersArray.size()];
        int i=0;
        for (Integer orderOffset : ordersArray) {
            orderArrayInt[i++] = orderOffset;
        }

        int ordersVector = OrderRequest.createOrdersVector(builder, orderArrayInt);

        int connOffset = copyConnection(emsClient.getConnection(), builder);

        OrderRequest.startOrderRequest(builder);
        OrderRequest.addConnection(builder, connOffset);
        OrderRequest.addOrders(builder, ordersVector);
        int oprOffset = OrderRequest.endOrderRequest(builder);

        builder.finish(oprOffset);

        OrderRequest request = OrderRequest.getRootAsOrderRequest(builder.dataBuffer());

        Iterator<OrderResponse> statusList = emsClient.sendOrderAsync(request);
        List<OrderResponse> orderResponseList = new ArrayList<>();
        while (statusList.hasNext()) {
            OrderResponse response = statusList.next();
            if (response.status() != OrderSubmitStatus.SUCCESSFUL_ORDER) {
                log.error("Failed to submit order. "+response.hedgeosOrderId());
                throw new RuntimeException("Failed to submit order="+response.status()+", orderId="+response.hedgeosOrderId());
            }
            orderResponseList.add(response);
        }

        if (orderResponseList.size() != orderArrayInt.length) {
            throw new RuntimeException("Not enough responses!");
        }

        long end = System.currentTimeMillis();
        long took = (end-start);
        System.out.println("NASDAQ 4400 round trip in one message.  Took(ms)="+took);

        if (took > 2000) {
            throw new RuntimeException("Took more than 2 seconds to send one request with 4400 orders, took ms="+took);
        }

        List<EmsReport> statusNewEmsReports = new ArrayList<>();
        for (int j=100; j<4500; j++) {
            EmsReport emsReportNew = callback.takeFirstEmsReport();
            assertEquals(OrderStatus.NEW_ORDER_STATUS, emsReportNew.orderStatus());
            statusNewEmsReports.add(emsReportNew);
        }

        // clear the one message off the queue, but is it one?
        List<EmsReport> garbage = callback.pollUntilEmpty();
        assertTrue(garbage.isEmpty());

        // TODO:  Cancel all those orders
        for (EmsReport report : statusNewEmsReports) {
            long cancelId = CancelLimitOrder.cancelOrder(emsClient, makeRequestId(), report.hedgeOsOrderId());
            EmsReport cancelReport = callback.takeFirstEmsReport();
            assertEquals(OrderStatus.CANCELLED_ORDER_STATUS, cancelReport.orderStatus());
            assertEquals(cancelReport.hedgeOsOriginalOrderId(), report.hedgeOsOrderId());
            assertEquals(cancelId, cancelReport.hedgeOsOrderId());
        }

        // System.out.println("EmsReports="+emsReports.size());
        assertTrue(callback.isEmpty(), "Did not retrieve all callbacks!");
        return true;
    }

    private static void addOrderToVector(EmsClient emsClient, int orderType, FlatBufferBuilder builder,
                                         int limitPriceOffset,
                                         int pmGroupOffset,
                                         int accountOffset,
                                         int orderDateOffset,
                                         int quantityOffset,
                                         int evOffset,
                                         ArrayList<Integer> ordersArray,
                                         int i)
    {
        long securityId = i;
        Order.startOrder(builder);
        Order.addSubmitTimestampMillisUtc(builder, System.currentTimeMillis());
        Order.addSid(builder, securityId);
        Order.addSide(builder, Side.BUY);
        Order.addOrderType(builder, orderType); // market order
        Order.addLimitPrice(builder, limitPriceOffset);
        Order.addAccount(builder, accountOffset);
        Order.addTradeDate(builder, orderDateOffset);
        // important for limits checks; must be base62, 3 chars max
        Order.addPmGroup(builder, pmGroupOffset);

        // TODO:  Make sure hedgeOsOrderid is not re-used, in EmsClient
        long hedgeOsOrderId = emsClient.getNextOrderId();

        Order.addHedgeOsOrderId(builder, hedgeOsOrderId);

        // created above.
        Order.addVenue(builder, evOffset);

        // created above.
        Order.addQuantity(builder, quantityOffset);
        // standard finish flat buffer message
        int orderOffset = Order.endOrder(builder);
        ordersArray.add(orderOffset);
    }

    public static boolean tradeAllNames(EmsClient emsClient) {
        long start = System.currentTimeMillis();

        List<OrderResponse> orderResponseList = new ArrayList<>();

        for (int i=100; i<4500; i++) {
            long hedgeOsOrderId = emsClient.getNextOrderId();
            OrderRequest request =
                    EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                            makeRequestId(),
                            TestConstants.PM_GROUP_CODE, TestConstants.ACCOUNT_1,
                            hedgeOsOrderId,
                            (i % 10)+1,
                            Side.BUY,
                            TestConstants.MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                            i,
                            IntegrationConstants.FILL_BUY_PRICE);

            List<OrderResponse> response = emsClient.sendOrder(request);
            orderResponseList.addAll(response);
        }

        long end = System.currentTimeMillis();
        long took = (end-start);

        System.out.println("Nasdaq, one trade each for 4400, took(ms)="+took);
        if (orderResponseList.size() != 4500-100) {
            throw new RuntimeException("Not enough responses!"+orderResponseList.size());
        }

        if (took > 10_000) {
            throw new RuntimeException("More than 10 seconds for 5k orders, took ms="+took);
        }

        for (OrderResponse response : orderResponseList) {
            EmsReport emsReportNew = callback.takeFirstEmsReport();
            assertEquals(OrderStatus.NEW_ORDER_STATUS, emsReportNew.orderStatus());
            assertEquals(response.hedgeosOrderId(), emsReportNew.hedgeOsOrderId());
            EmsReport emsReportFilled = callback.takeFirstEmsReport();
            assertEquals(OrderStatus.FILLED_ORDER_STATUS, emsReportFilled.orderStatus());
            assertEquals(response.hedgeosOrderId(), emsReportFilled.hedgeOsOrderId());
        }

        assertTrue(callback.isEmpty(), "Did not retrieve all callbacks!");

        // clear the one message off the queue, but is it one?
        List<EmsReport> emsReports = callback.pollUntilEmpty();
        assertEquals(0, emsReports.size());

        System.out.println("EmsReports="+emsReports.size());

        // System.out.println("EmsReports="+emsReports.size());
        // callback.clear();

        return true;
    }
}
