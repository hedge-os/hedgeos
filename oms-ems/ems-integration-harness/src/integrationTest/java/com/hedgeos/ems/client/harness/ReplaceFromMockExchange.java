package com.hedgeos.ems.client.harness;

import com.hedgeos.client.ems.EmsClient;
import com.hedgeos.client.ems.EmsClientCallback;
import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.test.IntegrationConstants;
import com.hedgeos.test.TestConstants;
import order.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ReplaceFromMockExchange {

    public static boolean expectReplaceFromMockExchange(long requestId,
                                                        EmsClient emsClient,
                                                        EmsClientCallback callback) {

        long limitOrderId = emsClient.getNextOrderId();
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        requestId,
                        TestConstants.PM_GROUP_CODE, TestConstants.ACCOUNT_1,
                        limitOrderId,
                        99,
                        Side.BUY,
                        TestConstants.MORGAN_STANLEY_MOCK_VENUE,
                        // important number used in MockExchange
                        TestConstants.FAKE_SECURITY_ID,
                        IntegrationConstants.BUY_LIMIT_PRICE_RESTS);

        List<OrderResponse> statusList = emsClient.sendOrder(request);
        assertEquals(1, statusList.size(),
                "No return value from order submit");
        assertEquals(OrderSubmitStatus.SUCCESSFUL_ORDER,
                statusList.get(0).status());

        // report will say "NEW" order status
        EmsReport report = callback.takeFirstEmsReport(60);
        assertEquals(limitOrderId, report.hedgeOsOrderId());
        assertEquals(OrderStatus.NEW_ORDER_STATUS, report.orderStatus());

        // now cancel the order
        long replaceId = emsClient.getNextOrderId();

        long quantity = 77;

        CancelReplaceRequest replaceRequest =
                EmsRequestBuilder.createReplaceRequest(quantity,
                        emsClient.getConnection(),
                        requestId,
                        replaceId,
                        limitOrderId);

        List<CancelReplaceResponse> cancelStatusList = emsClient.sendReplace(replaceRequest);

        assertEquals(1, cancelStatusList.size());
        assertEquals(OrderSubmitStatus.SUCCESSFUL_ORDER,
                cancelStatusList.get(0).status());

        // TODO:  Do we push pendings?  processReport in Multiplexer, removes them.
        //  Decision doc, discuss with friends.
//        EmsReport pendingReplaceReport = callback.pollForNextEmsReport(60);
//        if (pendingReplaceReport == null) {
//            fail("Failed to get nextEms report.");
//        }
//        assertEquals(replaceId, pendingReplaceReport.hedgeOsOrderId());
//        boolean isPending = OrderStatus.PENDING_REPLACE_ORDER_STATUS == pendingReplaceReport.orderStatus();


        // the replace itself
        EmsReport replaceReport = callback.takeFirstEmsReport(60);

        boolean isFilled = OrderStatus.FILLED_ORDER_STATUS == replaceReport.orderStatus();
        boolean isPartFilled = OrderStatus.PART_FILLED_ORDER_STATUS == replaceReport.orderStatus();
        assertTrue(isFilled || isPartFilled,
                "status is="+OrderStatus.name(replaceReport.orderStatus()));

        return true;
    }
}
