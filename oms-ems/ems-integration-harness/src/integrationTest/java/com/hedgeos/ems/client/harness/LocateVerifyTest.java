package com.hedgeos.ems.client.harness;

import com.hedgeos.ems.order.EmsRequestBuilder;
import order.*;
import org.junit.jupiter.api.Test;

import static com.hedgeos.ems.client.harness.BeforeAllSetupHarness.emsClient;
import static com.hedgeos.ems.client.harness.EmsIntegrationTestHarnessIT.makeRequestId;
import static com.hedgeos.test.TestConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LocateVerifyTest {

    public static final double PRICE_UNIMPORTANT_FOR_LOCATE_VERIFY = 1.23;
    private static final long THREE_HUNDRED_AND_ONE_TOO_MANY = 301;
    private static final long THREE_HUNDRED_ACCEPTIBLE = 30;


    /**
     * # small locates for fail locates test
     * 9223372036854775804,MORGAN_STANLEY,CMR,200
     *
     *# locates test, start long 100
     * 9223372036854775804,CMR,100,12.34
     */
    @Test
    public void testLocateVerifyTooMany() {
        BeforeAllSetupHarness.setupHarness();

        long qty = THREE_HUNDRED_AND_ONE_TOO_MANY;

        long hedgeOsOrderId = emsClient.getNextOrderId();
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        makeRequestId(),
                        PM_GROUP_CODE, ACCOUNT_1,
                        hedgeOsOrderId,
                        qty,
                        Side.SELL,
                        MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        FAKE_TEST_LOCATES_SECURITY_ID,
                        PRICE_UNIMPORTANT_FOR_LOCATE_VERIFY);

        LocateVerifyResponse response = emsClient.sendLocateVerify(request);
        assertEquals(OrderSubmitStatus.LOCATE_VERIFY_FAILURE, response.overallResult());
        LocateVerifyOrderResponse oneOrderResponse = response.responses(0);
        assertEquals(hedgeOsOrderId, oneOrderResponse.hedgeOsOrderId());
        assertEquals(OrderSubmitStatus.NOT_ENOUGH_LOCATES, oneOrderResponse.status());
    }

    // same test, but 300 = 200 locates + 100 start of day position; is allowed.
    @Test
    public void testLocateVerifySuccess() {
        BeforeAllSetupHarness.setupHarness();

        long qty = THREE_HUNDRED_ACCEPTIBLE;

        long hedgeOsOrderId = emsClient.getNextOrderId();
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        makeRequestId(),
                        PM_GROUP_CODE, ACCOUNT_1,
                        hedgeOsOrderId,
                        qty,
                        Side.SELL,
                        MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        FAKE_TEST_LOCATES_SECURITY_ID,
                        PRICE_UNIMPORTANT_FOR_LOCATE_VERIFY);

        LocateVerifyResponse response = emsClient.sendLocateVerify(request);
        assertEquals(OrderSubmitStatus.SUCCESSFUL_ORDER, response.overallResult());
        LocateVerifyOrderResponse oneOrderResponse = response.responses(0);
        assertEquals(hedgeOsOrderId, oneOrderResponse.hedgeOsOrderId());
        assertEquals(OrderSubmitStatus.SUCCESSFUL_ORDER, oneOrderResponse.status());
    }
}
