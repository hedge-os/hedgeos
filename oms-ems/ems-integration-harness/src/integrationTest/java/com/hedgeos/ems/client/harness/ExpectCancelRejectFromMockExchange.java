package com.hedgeos.ems.client.harness;

import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.client.ems.EmsClient;
import com.hedgeos.client.ems.EmsClientCallback;
import com.hedgeos.test.IntegrationConstants;
import com.hedgeos.test.TestConstants;
import order.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExpectCancelRejectFromMockExchange {

    public static boolean expectCancelRejectFromMockExchange(long requestId,
                                                             EmsClient emsClient,
                                                             EmsClientCallback callback) {

        long limitOrderId = emsClient.getNextOrderId();
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        requestId,
                        TestConstants.PM_GROUP_CODE, TestConstants.ACCOUNT_1,
                        limitOrderId,
                        99,
                        Side.BUY,
                        TestConstants.MORGAN_STANLEY_MOCK_VENUE,
                        // important number used in MockExchange
                        IntegrationConstants.CANCEL_REJECT_TEST_SECURITY_ID,
                        // important to make a buy limit rest in MockExchange
                        IntegrationConstants.BUY_LIMIT_PRICE_RESTS);

        List<OrderResponse> statusList = emsClient.sendOrder(request);
        assertEquals(1, statusList.size(), "No return value from order submit");
        assertEquals(OrderSubmitStatus.SUCCESSFUL_ORDER, statusList.get(0).status());

        // report will say "NEW" order status
        EmsReport report = callback.takeFirstEmsReport();
        assertEquals(limitOrderId, report.hedgeOsOrderId());
        assertEquals(OrderStatus.NEW_ORDER_STATUS, report.orderStatus());

        // now cancel the order
        long cancelId = emsClient.getNextOrderId();

        CancelRequest cancelRequest =
                EmsRequestBuilder.createCancelRequest(
                        emsClient.getConnection(),
                        requestId,
                        cancelId,
                        limitOrderId);

        List<CancelResponse> cancelStatusList = emsClient.sendCancel(cancelRequest);
        // log.warn("CANCEL sent!");
        assertEquals(1, cancelStatusList.size());
        assertEquals(CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS, cancelStatusList.get(0).status());
        EmsReport cancelReport = callback.takeFirstEmsReport();
        assertEquals(OrderStatus.REJECTED_ORDER_STATUS, cancelReport.orderStatus());
        assertEquals(cancelId, cancelReport.hedgeOsOrderId());

        return true;
    }
}
