package com.hedgeos.ems.client.harness;

import com.hedgeos.client.ems.EmsClient;
import com.hedgeos.client.ems.EmsClientCallback;
import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.test.IntegrationConstants;
import com.hedgeos.test.TestConstants;
import lombok.extern.slf4j.Slf4j;
import order.*;
import org.junit.jupiter.api.Assertions;

import java.util.List;

@Slf4j
public class TradeLimits {
    public static boolean violateShortLimit(long requestId,
                                            EmsClient emsClient,
                                            EmsClientCallback callback) {
        long hedgeOsOrderId = emsClient.getNextOrderId();

        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(emsClient.getConnection(),
                        requestId,
                        TestConstants.PM_GROUP_CODE, TestConstants.ACCOUNT_1,
                        hedgeOsOrderId,
                        100_000_000,
                        Side.SELL_SHORT,
                        TestConstants.MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        TestConstants.FAKE_SECURITY_ID,
                        IntegrationConstants.SELL_PRICE_LIMIT_RESTS);

        List<OrderResponse> statusList = emsClient.sendOrder(request);
        for (OrderResponse response : statusList) {
            Assertions.assertEquals(
                    OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_ONE_DAY,
                    response.status());
        }
        return true;
    }
}
