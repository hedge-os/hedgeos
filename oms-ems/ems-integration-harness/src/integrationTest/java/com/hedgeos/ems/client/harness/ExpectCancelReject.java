package com.hedgeos.ems.client.harness;

import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.client.ems.EmsClient;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.util.List;

@Slf4j
public class ExpectCancelReject {

    public static boolean expectCancelRejectFromEms(long requestId, EmsClient emsClient) {

        long cancelId = emsClient.getNextOrderId();
        long orderNeverPlaced = emsClient.getNextOrderId();

        CancelRequest request =
                EmsRequestBuilder.createCancelRequest(
                        emsClient.getConnection(),
                        requestId,
                        cancelId,
                        orderNeverPlaced);

        List<CancelResponse> statusList = emsClient.sendCancel(request);
        // log.warn("CANCEL sent!");
        for (CancelResponse response : statusList) {
//            log.warn("     status="+ CancelSubmitStatus.name(response.cancelSubmitStatus())+
//                    ", cancelId="+response.hedgeOsCancelId()+", statusCode="+response.cancelSubmitStatus());

            if (response.status() == CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS) {
                // we expect a CANCEL REJECT since it was a random order ID we tried to cancel,
                // (without ever sending an order;)
                return false;
            }

            // EmsService will short circuit the messaging, say it knows nothing about the original order
            // TODO:  Ponder, should EMS send it to market anyway?
            if (response.status() == CancelSubmitStatus.NO_ORIGINAL_ORDER_CANCEL_STATUS) {
                return true;
            }
        }

        return false;
    }

}
