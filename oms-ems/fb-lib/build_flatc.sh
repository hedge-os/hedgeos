# Easy and small binary:  https://flatbuffers.dev/flatbuffers_guide_building.html
mkdir -p build/flat23.3.3
cp flatbuffers-23.3.3.tar.gz build/flat23.3.3
cd build/flat23.3.3/
tar -xvf flatbuffers-23.3.3.tar.gz
cd flatbuffers-23.3.3/
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
make -j4
cp flatc ../../../flatc_23_3_3