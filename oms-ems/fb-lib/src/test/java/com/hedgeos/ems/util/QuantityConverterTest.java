package com.hedgeos.ems.util;

import general.Quantity;
import org.apache.commons.math3.util.Precision;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class QuantityConverterTest {
    @Test
    public void testProtoConverter() {

        Quantity quantityObj = QuantityConverter.makeQuantityFromFields(12, 333);

        Double quantity = QuantityConverter.quantityToDouble(quantityObj);
        double normally = 12 + 333/1_000_000_000.0;
        System.out.println("12="+quantity);
        Assertions.assertEquals(quantity, normally, "Quantity round trip failed");
        System.out.println(normally == quantity);
        System.out.println(normally);
        System.out.println(quantity);
        System.out.println("Diff="+(normally-quantity));

        Quantity newQty = QuantityConverter.makeQuantityFromFields(3, 100_000_000);
        Double threeTen = QuantityConverter.quantityToDouble(newQty);
        System.out.println("Three ten="+threeTen);
        double dub = 3.10;
        System.out.println(dub == threeTen);
        System.out.println(dub);
        Assertions.assertEquals(dub, threeTen, "Quantity round trip failed.");
    }

    @Test
    public void testQuantityFromDouble() {
        double price = 3.444;
        Quantity quantity = QuantityConverter.convertDoubleToQty(price);
        System.out.println("quantity="+quantity);
        Assertions.assertEquals(3L, quantity.quantity(), "busted quantity");
        Assertions.assertEquals( 444_000_000, quantity.nanoQty(), "busted nanoQty");
    }

    @Test
    public void testBigDecimalFromDouble() {
        double lastSharesDouble = 3.444;
        BigDecimal bd = new BigDecimal(String.valueOf(lastSharesDouble));
        BigDecimal fractionalPart = bd.remainder(BigDecimal.ONE);

        int nanos = fractionalPart.multiply(new BigDecimal(1_000_000_000)).intValue();
        // int nanos2 = fractionalPart.multiply(ONE_BILLION_BIG_DECIMAL).intValue();

        Quantity bdResult =
                QuantityConverter.makeQuantityFromFields(bd.intValue(), nanos);

        System.out.println("bdResult="+bdResult.quantity()+", nano:"+bdResult.nanoQty());
    }
}
