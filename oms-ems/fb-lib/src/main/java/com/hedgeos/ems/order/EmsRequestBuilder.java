package com.hedgeos.ems.order;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.util.QuantityConverter;
import com.hedgeos.ems.util.TradingDayUtil;
import general.Currency;
import general.Quantity;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.nio.ByteBuffer;
import java.time.LocalDate;
import java.util.Objects;

import static java.time.ZoneOffset.UTC;

@Slf4j
public class EmsRequestBuilder {

    public static CancelReplaceRequest createReplaceRequest(long quantity,
                                                            Connection connection,
                                                    long requestId,
                                                    long replaceOrderId,
                                                    long origHedgeOsOrderId)
    {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int connOffset = copyConnection(connection, builder);
        int tradeDateOffset = setLocalDateOnBuilder(builder);
        int quantityOffset = setQuantityOnBuilder(builder, quantity);
        CancelReplace.startCancelReplace(builder);
        CancelReplace.addTradeDate(builder, tradeDateOffset);
        CancelReplace.addQuantity(builder, quantityOffset);
        CancelReplace.addHedgeOsOrderId(builder, replaceOrderId);
        CancelReplace.addHedgeOsOrigOrderId(builder, origHedgeOsOrderId);
        int replaceOffset = CancelReplace.endCancelReplace(builder);
        int cancelVectorOffset = CancelReplaceRequest.createCancelReplacesVector(builder, new int[] {replaceOffset});
        int requestOffset = CancelReplaceRequest.createCancelReplaceRequest(builder, connOffset, requestId, cancelVectorOffset);
        builder.finish(requestOffset);
        CancelReplaceRequest cancelRequest =
                CancelReplaceRequest.getRootAsCancelReplaceRequest(builder.dataBuffer());
        return cancelRequest;
    }

    private static int setQuantityOnBuilder(FlatBufferBuilder builder, long quantity) {
        Quantity.startQuantity(builder);
        Quantity.addQuantity(builder, quantity);
        Quantity.addNanoQty(builder, 0);
        int quantityOffset = Quantity.endQuantity(builder);
        return quantityOffset;
    }


    public static CancelRequest createCancelRequest(Connection connection,
                                                    long requestId,
                                                    long cancelHedgeOsOrderId,
                                                    long origHedgeOsOrderId)
    {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int connOffset = copyConnection(connection, builder);
        int tradeDateOffset = setLocalDateOnBuilder(builder);
        int cancelOffset = Cancel.createCancel(builder, System.currentTimeMillis(),
                tradeDateOffset, cancelHedgeOsOrderId, origHedgeOsOrderId);
        int cancelVectorOffset = CancelRequest.createCancelsVector(builder, new int[] {cancelOffset});
        int requestOffset = CancelRequest.createCancelRequest(builder, connOffset, requestId, cancelVectorOffset);
        builder.finish(requestOffset);
        CancelRequest cancelRequest = CancelRequest.getRootAsCancelRequest(builder.dataBuffer());
        return cancelRequest;
    }

    // TODO:  Builder pattern here.
    public static OrderRequest createOrderRequest(Connection connection,
                                                        long requestId,
                                                        String pmGroupCode,
                                                        String account,
                                                        long hedgeOsOrderId,
                                                        long quantity,
                                                        int side,
                                                        int venueId,
                                                        long sid,
                                                        Double limitPrice)
    {
        long now = System.currentTimeMillis();

        int orderType = OrdType.MARKET;
        if (limitPrice != null) {
            orderType = OrdType.LIMIT;
        }

        FlatBufferBuilder builder = new FlatBufferBuilder();

        int limitPriceOffset = Integer.MIN_VALUE;
        if (limitPrice != null) {
            limitPriceOffset = QuantityConverter.doubleToPriceBuffer(builder, limitPrice, Currency.USD);
        }

        if (Objects.requireNonNull(pmGroupCode).length() > 3 ||
                pmGroupCode.length() == 0)
        {
            log.error("pmGroup can be at most three chars, pmGroup="+pmGroupCode);
            throw new RuntimeException("pmGroup must be 1-3 chars.");
        }

        int pmGroupOffset = builder.createString(pmGroupCode);
        int accountCodeOffset = builder.createString(account);

        ref_data.Account.startAccount(builder);
        ref_data.Account.addCode(builder, accountCodeOffset);
        int accountOffset = ref_data.Account.endAccount(builder);

        int evNameOffset = builder.createString("unused_name");

        int orderDateOffset = setLocalDateOnBuilder(builder);

        Quantity.startQuantity(builder);
        Quantity.addQuantity(builder, quantity);
        Quantity.addNanoQty(builder, 0);
        int quantityOffset = Quantity.endQuantity(builder);

        ExecutionVenue.startExecutionVenue(builder);
        ExecutionVenue.addName(builder, evNameOffset);
        ExecutionVenue.addId(builder, venueId);
        ExecutionVenue.addVenueType(builder, ExecutionVenueType.EQUITY);
        // name is not important
        // ExecutionVenue.addName(builder, "US_STOCK");
        int evOffset = ExecutionVenue.endExecutionVenue(builder);

        Order.startOrder(builder);
        Order.addSubmitTimestampMillisUtc(builder, now);
        Order.addSid(builder, sid);
        Order.addSide(builder, side);
        Order.addOrderType(builder, orderType); // market order
        if (limitPrice != null) {
            Order.addLimitPrice(builder, limitPriceOffset);
        }
        Order.addAccount(builder, accountOffset);
        Order.addTradeDate(builder, orderDateOffset);
        // important for limits checks; must be base62, 3 chars max
        Order.addPmGroup(builder, pmGroupOffset);

        Order.addHedgeOsOrderId(builder, hedgeOsOrderId);

        // created above.
        Order.addVenue(builder, evOffset);

        // created above.
        Order.addQuantity(builder, quantityOffset);
        // standard finish flat buffer message
        int orderOffset = Order.endOrder(builder);

        int connOffset = copyConnection(connection, builder);

        // NOTICE:  You must add the Orders to a Vector, then create the vector
        // add the order above to the flatbuffer, must happen before startOrderRequest
        int ordersVector = OrderRequest.createOrdersVector(builder, new int[] {orderOffset});

        OrderRequest.startOrderRequest(builder);
        OrderRequest.addConnection(builder, connOffset);
        OrderRequest.addOrders(builder, ordersVector);

        OrderRequest.addRequestId(builder, requestId);

        int oprOffset = OrderRequest.endOrderRequest(builder);

        builder.finish(oprOffset);
        OrderRequest request = OrderRequest.getRootAsOrderRequest(builder.dataBuffer());

        testDeserRequest(request, venueId);
        return request;
    }

    public static int setLocalDateOnBuilder(FlatBufferBuilder builder) {
        LocalDate nowUtc = TradingDayUtil.getTradeDate();
        google.type.Date.startDate(builder);
        google.type.Date.addYear(builder, nowUtc.getYear());
        google.type.Date.addMonth(builder, nowUtc.getMonthValue());
        google.type.Date.addDay(builder, nowUtc.getDayOfMonth());
        int orderDateOffset = google.type.Date.endDate(builder);
        return orderDateOffset;
    }

    public static int copyConnection(Connection connection, FlatBufferBuilder builder) {
        // Notice, required connectionId; is only an integer; however, explicit and type safe
        int connOffset =
                Connection.createConnection(builder, connection.emsServerId(),
                        connection.connectionId(), connection.reconnectCount(),
                        connection.status(), connection.reason());
        return connOffset;
    }

    // verify you added the orders correctly..
    private static void testDeserRequest(OrderRequest in, int venueId) {
        ByteBuffer bb = in.getByteBuffer();
        OrderRequest request = OrderRequest.getRootAsOrderRequest(bb);
        assert request.ordersLength() == 1;
        assert request.orders(0).venue().id() == venueId;
    }
}
