package com.hedgeos.ems.order;

import com.hedgeos.ems.util.PrecisionConstants;
import general.Quantity;
import lombok.extern.slf4j.Slf4j;
import order.Order;
import order.Side;

import static com.hedgeos.ems.util.QuantityConverter.quantityToDouble;

@Slf4j
public class OrderUtil {

    public static final double ZERO_DOUBLE = 0.0;

    public static double getDirectionalQuantity(Quantity quantity, int side)
    {
        if (isSell(side) && isNegativeQuantity(quantity)) {
            log.error("Negative quantity on short order, qty={}",
                    quantityToDouble(quantity)+", side="+side);
        }

        if (isBuy(side)) {
            return quantityToDouble(quantity);
        }
        else if (isSell(side))  {
            return -1 * Math.abs(quantityToDouble(quantity));
        }

        String msg = "Unknown side="+side;
        log.error(msg);
        throw new Error(msg);
    }


    public static boolean isNegativeQuantity(Quantity quantity) {
        return PrecisionConstants.isGreaterThan(ZERO_DOUBLE, quantityToDouble(quantity));
    }

    public static boolean isBuy(int side) {
        return side == Side.BUY || side == Side.BUY_MINUS;
    }

    public static boolean isSell(int side) {
        return side == Side.SELL || side == Side.SELL_SHORT || side == Side.SELL_SHORT_EXEMPT;
    }

    public static String toString(google.type.Date tradeDate) {
        StringBuilder sb = new StringBuilder();
        sb.append(tradeDate.month());
        sb.append("/");
        sb.append(tradeDate.day());
        sb.append("/");
        sb.append(tradeDate.year());
        return sb.toString();
    }
}
