package com.hedgeos.ems.service;

import com.hedgeos.ems.util.WorkstationDevelopmentConstants;
import environment.EmsEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@Slf4j
// basic prevention for accidental connections from test code to prod
public class EmsEnvironmentManager {

    // must be set to PRODUCTION for Prod usage.
    public static final String EMS_ENVIRONMENT = "EMS_ENVIRONMENT";
    public static final String STALE_LIMITS_ENABLED = "STALE_LIMITS_ENABLED";
    public static final String STALE_SYMBOLS_ENABLED = "STALE_SYMBOLS_ENABLED";
    public static final String STALE_LOCATES_ENABLED = "STALE_LOCATES_ENABLED";

    private final boolean isWorkstation;
    private final int emsEnvironment;

    private final boolean isStaleLimitsEnabled;
    private final boolean isStaleSymbolsEnabled;

    private final boolean isStaleLocatesEnabled;

    public EmsEnvironmentManager() {

        isStaleLimitsEnabled = setStaleSymbolsFromEnvVariable(STALE_LIMITS_ENABLED);
        isStaleSymbolsEnabled = setStaleSymbolsFromEnvVariable(STALE_SYMBOLS_ENABLED);
        isStaleLocatesEnabled = setStaleSymbolsFromEnvVariable(STALE_LOCATES_ENABLED);
        // set a convenience flag if running from the source tree
        // if a developer workstation, load what we find locally
        for (String potentialWorkstationFileLocation : WorkstationDevelopmentConstants.POTENTIAL_WORKSTATION_FILE_LOCATIONS) {
            File devWorkstationFile = new File(potentialWorkstationFileLocation);
            if (devWorkstationFile.exists()) {
                log.warn("Dev workstation file found, loading emsServerId from: "+potentialWorkstationFileLocation);
                emsEnvironment = EmsEnvironment.WORKSTATION;
                isWorkstation = true;
                return;
            }
        }
        // get the critical emsEnvironment, which must exist for non-workstation usage
        emsEnvironment = getEmsEnvironmentFromEnvVariable();
        isWorkstation = false;
    }

    public String getEnvName() {
        return EmsEnvironment.names[emsEnvironment];
    }

    public boolean isWorkstation() {
        return isWorkstation;
    }

    public int getEmsEnvironment() {
        return emsEnvironment;
    }

    public boolean isStaleLocatesEnabled() { return isWorkstation || isStaleLocatesEnabled; }
    public boolean isStaleLimitsEnabled() {
        return isWorkstation || isStaleLimitsEnabled;
    }

    public boolean isStaleSymbolsEnabled() {
        return isWorkstation || isStaleSymbolsEnabled;
    }

    // note: you can never set stale positions enabled in prod.
    public boolean isStalePositionsEnabled() { return isWorkstation; }

    private boolean setStaleSymbolsFromEnvVariable(String envVariable) {
        if (System.getenv(envVariable) != null) {
            String staleLimitsEnv = System.getenv(envVariable);
            if (Boolean.parseBoolean(staleLimitsEnv)) {
                log.warn("Enabled "+envVariable+" because \"STALE_SYMBOLS_ENABLED\"={}", staleLimitsEnv);
                return true;
            }
            else {
                log.warn("DISABLED "+envVariable+" because \"STALE_SYMBOLS_ENABLED\"={}", staleLimitsEnv);
                return false;
            }
        }
        else {
            log.warn(envVariable+" not allowed; must provide recent, every trading date");
            return false;
        }
    }

    private int getEmsEnvironmentFromEnvVariable() {
        // always first look for EMS_ENVIRONMENT variable set on the account
        String emsEnv = System.getenv(EMS_ENVIRONMENT);
        if (emsEnv != null) {
            emsEnv = emsEnv.toUpperCase().trim();

            // this is how Flatbuffer Enums are used with strings.
            if (emsEnv.equals(EmsEnvironment.names[EmsEnvironment.PRODUCTION])) {
                return EmsEnvironment.PRODUCTION;
            }
            if (emsEnv.equals(EmsEnvironment.names[EmsEnvironment.WORKSTATION])) {
                return EmsEnvironment.WORKSTATION;
            }
            if (emsEnv.equals(EmsEnvironment.names[EmsEnvironment.DEVEL])) {
                return EmsEnvironment.DEVEL;
            }
            if (emsEnv.equals(EmsEnvironment.names[EmsEnvironment.QA])) {
                return EmsEnvironment.QA;
            }
            if (emsEnv.equals(EmsEnvironment.names[EmsEnvironment.UAT])) {
                return EmsEnvironment.UAT;
            }

            log.error("Invalid value of {}}={}",EMS_ENVIRONMENT,emsEnv);
            // will cause startup to fail (fail-fast) intentionally
            throw new Error("Invalid value of EMS_ENVIRONMENT="+emsEnv);
        }

        // special case, where defaults to WORKSTATION if in the source tree
        if (isWorkstation) {
            return EmsEnvironment.WORKSTATION;
        }

        // in Devel and all the others, require this variable to be set on the machine
        String msg = "EMS_ENVIRONMENT unset in non-workstation env.";
        log.error(msg);
        throw new Error(msg);

    }
}
