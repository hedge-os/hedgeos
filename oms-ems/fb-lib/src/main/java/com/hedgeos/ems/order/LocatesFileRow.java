package com.hedgeos.ems.order;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.File;

@RequiredArgsConstructor
@Getter
public class LocatesFileRow {
    final EmsExecutionVenue venue; // may be null, see docs
    final long sid;
    final String pmGroup; // may be null, see docs
    final long locates;
    final File file;
    final int rowNumber;


}
