package com.hedgeos.ems.util;

import org.apache.commons.math3.util.Precision;

public class PrecisionConstants {
    // quantities of up to 100 billion should not fail a limit check due to double precision
    // ULP = 1.52587890625E-5 as Epsilon
    public static final double ULP = Math.ulp(100_000_000_000.0);

    public static boolean isGreaterThan(double first, double second) {
        int comparison = Precision.compareTo(first, second, ULP);
        return comparison > 0;
    }

    public static boolean isGreaterThanOrEqual(double first, double second) {
        int comparison = Precision.compareTo(first, second, ULP);
        return comparison >= 0;
    }

    public static boolean isLessThanOrEqual(double first, double second) {
        int comparison = Precision.compareTo(first, second, ULP);
        return comparison <= 0;
    }

    public static boolean isEqual(double price, double magicFillPrice) {
        int comparison = Precision.compareTo(price, magicFillPrice, ULP);
        return comparison == 0;
    }
}
