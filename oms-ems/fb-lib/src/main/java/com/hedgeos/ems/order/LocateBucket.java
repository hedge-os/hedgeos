package com.hedgeos.ems.order;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class LocateBucket {
    final LocatesFileRow row; // the row which provided this amount of locates
    // final AtomicLong runningTotal; // the running total available
}
