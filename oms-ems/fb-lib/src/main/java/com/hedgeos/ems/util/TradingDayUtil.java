package com.hedgeos.ems.util;

import com.google.flatbuffers.FlatBufferBuilder;
import google.type.Date;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;

import static java.time.ZoneOffset.UTC;

/**
 * Trading date may be, a command line param on startup of the EMS;
 * but only if it is provided, and it need not be (typically.)
 *
 * TODO:  Extensive unit testing, and design of trading day.
 */
@Slf4j
public class TradingDayUtil {
    
    LocalDate cachedTradingDate = null;

    public static final String EMS_TRADE_DATE = "EMS_TRADE_DATE";

    public static final String EMS_TIMEZONE = "EMS_TIMEZONE";

    static {
        try {
            String envVar = System.getenv(EMS_TRADE_DATE);
            String timezone = System.getenv(EMS_TIMEZONE);
            // TODO:
            //  if these are set, seed with TRADE_DATE and roll at timezone
        }
        catch (Exception e) {
            log.error("Exception looking for trading date property", e);
            throw new Error(e);
        }
    }
    public static int getTradingDay() {
        return LocalDate.now(UTC).getDayOfMonth();
    }

    public static LocalDate getTradeDate() {
        return LocalDate.now(UTC);
    }

    public static LocalDate googleDateToLocalDate(google.type.Date googleDate) {
        // TODO:  can do this pooled, no construction of memory
        return LocalDate.of(googleDate.year(), googleDate.month(), googleDate.day());
    }

    public static LocalDate plusTradeDays(LocalDate tradeDate) {
        // Taking into account global trading calendar (skip Jan 1, the only global trading holiday.)
        LocalDate nextDay = tradeDate.plusDays(1);
        if (nextDay.getDayOfMonth() == 1 && nextDay.getMonth() == Month.JANUARY) {
            // push to next day; no trading on Jan 1
            return nextDay.plusDays(1);
        }
        return nextDay;
    }

    public static LocalDate timeToLocalDate(long transactTime) {
        // this pushes us forward a time zone.
        return LocalDate.ofInstant(Instant.ofEpochMilli(transactTime), UTC);
    }

    public static int getProtoCurrentTradeDate(FlatBufferBuilder builder) {
        LocalDate date = getTradeDate();
        return Date.createDate(builder, date.getYear(),date.getMonthValue(), date.getDayOfMonth());
    }
}
