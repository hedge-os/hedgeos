package com.hedgeos.ems.order;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j
@Getter
public class LocateCheckResult {

    public final boolean isOrderUsingLocates;
    public final long locatesReserved;
    public final int orderStatusCode;
    final EmsPosition locatesPosition;
    final PmGroupSidKey key;
    private final LocatesFileRow row;
    private final File locatesFile;
    private final String message;
    private final long totalLocates;
    private final long currentPosition;
    private final long orderQty;


    public LocateCheckResult(
            boolean areLocatesUsed,
            long locatesReserved,
            int returnCode,
            PmGroupSidKey key,
            EmsPosition locatesPosition,
            String message,
            File file,
            LocatesFileRow row,
            long locatesTotal,
            long currentPosition,
            long orderQty)
    {
        isOrderUsingLocates = areLocatesUsed;
        this.locatesReserved = locatesReserved;
        this.orderStatusCode = returnCode;
        this.key = key;
        this.locatesPosition = locatesPosition;
        this.message = message;
        this.row = row;
        this.locatesFile = file;
        this.totalLocates = locatesTotal;
        this.currentPosition = currentPosition;
        this.orderQty = orderQty;
    }

    /**
     * Parent locates is a pointer to the AtomicLong bucket of locates this was drawn from.
     * Now we can simply increment this back by the order quantity.
     */
//    public void returnLocatesDueToFailedOrder() {
//        // if the order was not a success, return
//        if (orderStatusCode != OrderSubmitStatus.SUCCESSFUL_ORDER)
//            return;
//
//        if (locatesBucketDrawnFrom != null) {
//            // give this amount of locates back to the pool
//            locatesBucketDrawnFrom.runningTotal.addAndGet(locatesReserved);
//        }
//
//        // reduce the number of locates taken on the position
//        if (locatesPosition != null) {
//            locatesPosition.returnLocates(locatesReserved);
//        }
//    }

    public String errorMessage() {
        return message;
    }
}
