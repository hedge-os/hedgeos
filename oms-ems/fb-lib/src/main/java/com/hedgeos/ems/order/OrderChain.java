package com.hedgeos.ems.order;

import general.Quantity;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

/**
 * The chain is the first Order, then any cancel replaces, and cancels.
 * The side, symbol, pmGroup of an order never change.
 * We use the order chain to cache these values, and the "key" of the order.
 */
@Getter
@Slf4j
public class OrderChain {

    private static final double ZERO_QUANTITY = 0.0d;
    final OrderUnion unionLastOrder = new OrderUnion();
    final Order order;
    private final PmGroupSidKey key;
    private volatile String symbol;
    final List<CancelReplace> replaceChain = new CopyOnWriteArrayList<>();
    final List<Cancel> cancels = new CopyOnWriteArrayList<>();
    private final LocalDate orderTradeDate;

    private final AtomicLong maxOrderQty;

    private Map<Long, Long> orderIdTimes = new ConcurrentHashMap<>();
    private final EmsExecutionVenue venue;

    // ultra important for calculating; how much new amount was completed against this order chain
    volatile Double executedCumQty;

    public OrderChain(Order order, LocalDate tradeDate, EmsExecutionVenue venue) {
        this.order = order;
        unionLastOrder.setOrder(order);
        this.key = new PmGroupSidKey(order.sid(), order.pmGroup());
        this.orderTradeDate = tradeDate;
        this.maxOrderQty = new AtomicLong(order.quantity().quantity());
        orderIdTimes.put(order.hedgeOsOrderId(), order.submitTimestampMillisUtc());

        this.venue = venue;
    }


    public double addReplaceAndCalcPreTradeIncrease(CancelReplace replace) {
        orderIdTimes.put(replace.hedgeOsOrderId(), replace.hedgeOsOrderId());
        replaceChain.add(replace);
        unionLastOrder.setCancelReplace(replace);
        // replaces can be for limit prices; typically are not quantity changes
        if (replace.quantity() != null && replace.quantity().quantity() > maxOrderQty.get()) {
            double temp = replace.quantity().quantity() - maxOrderQty.get();
            maxOrderQty.set(replace.quantity().quantity());
            return temp;
        }
        return ZERO_QUANTITY;
    }

    public void addCancel(Cancel cancel) {
        this.cancels.add(cancel);
        this.unionLastOrder.setCancel(cancel);
        orderIdTimes.put(cancel.hedgeOsOrderId(), cancel.submitTimestampMillisUtc());
    }

    // sid and pmGroup from the key for minor speed
    public long sid() {
        return key.sid;
    }
    public String pmGroup() {
        return key.pmGroup;
    }

    public google.type.Date tradeDate() {
        return order.tradeDate();
    }


    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public long submitTimestampMillisUtc(long hedgeOsOrderId) {
        return orderIdTimes.get(hedgeOsOrderId);
    }


    public long submitTimestampMillisUtc() {
        return unionLastOrder.submitTimestampMillisUtc();
    }

//    public int orderStatus(long origOrdId) {
//
//    }

//    public int originalOrderStatus() {
//        return order.orderStatus();
//    }

    public EmsExecutionVenue venue() {
        return venue;
    }

    public int side() {
        return order.side();
    }

    public Quantity quantity() {
        return unionLastOrder.quantity();
    }

    public int orderType() {
        return order.orderType();
    }



    public synchronized double increaseCumQuantityReturnDiff(double cumQtyDouble) {
        if (executedCumQty == null) {
            executedCumQty = cumQtyDouble;
            return executedCumQty;
        }
        else if (cumQtyDouble == executedCumQty) {
            return ZERO_QUANTITY;
        }
        else if (cumQtyDouble > executedCumQty) {
            double diff = cumQtyDouble - executedCumQty;
            executedCumQty = cumQtyDouble;
            return diff;
        }

        // never happens
        log.error("QTY decrease, can never happen. lastQty={}, cumQty={}," +
                "  hedgeOsOrderId={}", executedCumQty, cumQtyDouble, unionLastOrder.hedgeOsOrderId());
        return executedCumQty;
    }
}
