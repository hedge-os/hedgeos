package com.hedgeos.ems.util;


// convenience for determining if running in the source tree
public class WorkstationDevelopmentConstants {
    public static final String DEVELOPER_EMS_SERVER_ID_NUMBER = "./src/main/resources/developerEmsServerId.number";
    public static final String ALT_DEVELOPER_EMS_SERVER_ID_NUMBER = "./ems-service/src/main/resources/developerEmsServerId.number";
    public static final String ALT2_DEVELOPER_EMS_SERVER_ID_NUMBER = "../ems-service/src/main/resources/developerEmsServerId.number";

    public static String[] POTENTIAL_WORKSTATION_FILE_LOCATIONS = { DEVELOPER_EMS_SERVER_ID_NUMBER, ALT_DEVELOPER_EMS_SERVER_ID_NUMBER, ALT2_DEVELOPER_EMS_SERVER_ID_NUMBER };
}
