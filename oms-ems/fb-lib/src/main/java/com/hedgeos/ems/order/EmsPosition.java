package com.hedgeos.ems.order;

import com.google.common.util.concurrent.AtomicDouble;
import com.hedgeos.ems.util.CompareAndSwapLock;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static com.hedgeos.ems.order.OrderUtil.*;

@RequiredArgsConstructor
@ToString
@Slf4j
public class EmsPosition {

    // when locates are involved, we need to lock the position, calculate the locate quantity,
    // and then unlock the position
    // THIS IS THAT LOCK
    public final CompareAndSwapLock lock = new CompareAndSwapLock();

    // for tracking the total amount traded since a given moment
    public static final double RESET_TO_ZERO_AMOUNT = 0.0d;

    // key field for locates, trading limits
    private final PmGroupSidKey key;
    private final AtomicDouble position;

    private final AtomicDouble originalPosition = new AtomicDouble(RESET_TO_ZERO_AMOUNT);

    // total amounts requested to trade; for this position
    private final AtomicDouble longAmountRequestedSinceReset = new AtomicDouble(RESET_TO_ZERO_AMOUNT);
    private final AtomicDouble shortAmountRequestedSinceReset = new AtomicDouble(RESET_TO_ZERO_AMOUNT);

    // amounts TRADED (executed, back from the market); for this position
    private final AtomicDouble longAmountTradedSinceReset = new AtomicDouble(RESET_TO_ZERO_AMOUNT);
    private final AtomicDouble shortAmountTradedSinceReset = new AtomicDouble(RESET_TO_ZERO_AMOUNT);

    private long resetTime;
    private PositionFileRow originalRow;

    // locates are part of position keeping

    // There really is no locates; locates are, the amount ahead of longs that your shorts can go
    // When more locates arrive, need to increase the total locates; when postions are changed..
    // .. perhaps flush locates

    // private volatile RunningTotalLocateBucket locatesBucketDrawnFrom;
    private final AtomicLong totalLocatesReserved = new AtomicLong(0);
    // we may get more than one call to return locates which have not been used;
    // don't do this more than once
    private final AtomicBoolean locatesReturnedOnCancel = new AtomicBoolean(false);

    /**
     *  Available to short = start of day + long amount TRADED - short amounts requested.
     *      - now I'm thinking needs to also include locates given out.
     *
     *  Note:  can't include Long amounts traded, until it actually trades
     *         Short amounts, should be considered immediately traded
     *  It is the "short amount requested vs. long amount actually traded."
     * @return the amount the position will let you short against
     */
    public long calcNetPositionWithShortRequestsAndLongTradesSinceStartOfDay() {
        // if the start of day was negative; use zero as the amount we can short against
        double startOfDayToShortAgainst = Math.max(0, originalPosition.get());

        // get the Actual traded (executed) inside the lock
        double longAmountTraded = longAmountTradedSinceReset.get();
        // REQUESTED short amount
        double shortAmountRequested = shortAmountRequestedSinceReset.get();

        // increase the available position with any amount traded long
        // but decrease it by the amount of shorting which was requested today
        //noinspection UnnecessaryLocalVariable
        double netPositionLongTradedVsShortRequested =
                (startOfDayToShortAgainst + longAmountTraded - shortAmountRequested);

        return (long) netPositionLongTradedVsShortRequested;

        // locates NEUTRALIZE shorted amounts; thus, we add them back
        // double withLocates = availableShortPosition + totalLocatesReserved.get();

        // return Math.max(0, withLocates);

        // hmm..
        // //return Math.max(0, availableShortPosition);
        // return withLocates;
    }


    /**
     * Add the total amount long and short requested; could be gamed, but the trades should not stay open
     */
    public double getOpenPosition() {
        double original = originalPosition.get();
        return original + longAmountRequestedSinceReset.get() - shortAmountRequestedSinceReset.get();
    }

    public void resetAmountTraded(long resetTime, PositionFileRow positionFileRow) {
        this.resetTime = resetTime;
        if (positionFileRow != null) {
            this.originalPosition.set(positionFileRow.getPosition());
        }
        this.originalRow = positionFileRow;
        // actual amounts traded
        this.longAmountTradedSinceReset.set(RESET_TO_ZERO_AMOUNT);
        this.shortAmountTradedSinceReset.set(RESET_TO_ZERO_AMOUNT);
        // requested amounts reset
        this.longAmountRequestedSinceReset.set(RESET_TO_ZERO_AMOUNT);
        this.shortAmountRequestedSinceReset.set(RESET_TO_ZERO_AMOUNT);
    }


    public void increaseRequestedQuantity(int side, double positiveTradeQty) {
        if (isBuy(side)) {
            longAmountRequestedSinceReset.addAndGet(positiveTradeQty);
        }
        else {
            shortAmountRequestedSinceReset.addAndGet(positiveTradeQty);
        }
    }

    public void reduceRequestedQuantity(int side, double positiveQuantity) {
        // these go up and down; we negate them when we need to
        if  (isBuy(side)) {
            longAmountRequestedSinceReset.addAndGet(-1 * positiveQuantity);
        }
        else {
            shortAmountRequestedSinceReset.addAndGet(-1 * positiveQuantity);
        }
    }

    public void tradeCompletedAgainstPosition(int side, double signedQuantity) {

        // increment the position by how much we have done
        position.addAndGet(signedQuantity);

        // these go up and down; we negate them when we need to
        if  (isBuy(side)) {
            longAmountTradedSinceReset.addAndGet(signedQuantity);
        }
        else {
            shortAmountTradedSinceReset.addAndGet(signedQuantity);
        }
    }

    public double getLongAmountTradedSinceReset() {
        return longAmountTradedSinceReset.get();
    }

    public double getShortAmountRequestedSinceReset() {
        return shortAmountRequestedSinceReset.get();
    }

    public double getPosition() {
        return position.get();
    }

    public void setPosition(double position) {
        this.position.set(position);
    }

}
