package com.hedgeos.ems.order;

import lombok.extern.slf4j.Slf4j;
import order.Connection;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import static com.hedgeos.ems.order.OrderIdConstants.*;

/**
 * A critical system of generating orderIds on the client side;
 * must be used by all clients to get a hedgeOs orderId.
 * This uses the reconnect counter to generate a unique orderId per connection to the EMS.
 * See the docs/decisions/0005-order-id-generation-using-connection-count.md
**/
 @Slf4j
public class OrderIdGenerator {

    // TODO:  Change to arrays for speed, size 64

    AtomicLong orderIdCounter = new AtomicLong();

    long connectionTime = Long.MIN_VALUE;
    private final long dateRollOrderIdLimit;

    private final long catastropheRecoveryOffset;

    public static final long ONE_HUNDRED_BILLION_ORDERS_DATE_ROLL = 100_000_000_000L;

    public OrderIdGenerator() {
        this(ONE_HUNDRED_BILLION_ORDERS_DATE_ROLL);
    }

    public OrderIdGenerator(long dateRollOrderIdLimit) {
        this.dateRollOrderIdLimit = dateRollOrderIdLimit;

        boolean isCatastrophicRecovery = Boolean.getBoolean("recovery");
        if (isCatastrophicRecovery) {
            log.error("CATASTROPHIC RECOVERY MODE: ADDING 500 billion as an orderId offset");
            catastropheRecoveryOffset = 500_000_000_000L;
        }
        else {
            catastropheRecoveryOffset = 0L;
        }
    }

    /**
     * On new connection, EmsClient lets OrderIdGenerator know the time of connection
     * @param connectedTime the time connected
     */
    public synchronized void setConnectionTime(Date connectedTime) {
        connectionTime = connectedTime.getTime();
    }

    /**
     * A critical method, must be used by all clients to get a hedgeOs orderId.
     * This uses the reconnect counter to generate a unique per day, orderId.
     * See the docs/decisions/0005-order-id-generation-using-connection-count.md
     * @return long the next order id
     */
    public long getNextHedgeOsOrderId(Connection connection)
    {
        long nextPerConnectionOrderId = resetOrderIdCounterIfDateRoll();

        long connectionOrderIdOffset = ONE_TRILLION_CONNECTION_ID_OFFSET * connection.connectionId();

        long remainder = connection.reconnectCount() % RECONNECTS_PER_TRILLION_OFFSETS;
        // the offset we will jump to per reconnect;
        // increase order count by one billion per reconnect
        long reconnectCountOffset = ONE_BILLION_ORDERS_PER_RECONNECT_OFFSET * remainder;

        if (connection.reconnectCount() >= RECONNECTS_PER_TRILLION_OFFSETS) {
            // if we've reconnected more than 1000 times, can't jump by 1 trillion,
            // as it would collide with the next connectionId order number space
            // need to jump more than 62 trillion (connections per EMS)...
            // ... and 100 is a nice round number.
            long multipleOfTrillions = connection.reconnectCount() / RECONNECTS_PER_TRILLION_OFFSETS;

            connectionOrderIdOffset += SIXTY_TWO_TRILLION * multipleOfTrillions;
        }

        // each connection offset orderId wise by 1 trillion, plus a billion per reconnect
        long perConnectionOffsetOrderId =
                connectionOrderIdOffset + reconnectCountOffset + nextPerConnectionOrderId;

        // if we are in catastrophic recovery mode (should never occur in practice), add a giant offset
        perConnectionOffsetOrderId += catastropheRecoveryOffset;

        // client needs to reconnect occasionally to avoid sending a trillion orders;
        if (perConnectionOffsetOrderId > MAX_ORDER_IDS_ALLOWED_IN_CLORDID_STUFFING) {
            // don't panic; this is over 3 trillion orderIds
            String msg = "Exceeded max orderIds allowed in ClOrdIdStuffing:  VERY BAD";
            log.error(msg);
            throw new Error(msg);
        }

        return perConnectionOffsetOrderId;
    }

    /**
     *  While connected, we have sent over a hundred billion orders
     *   check to see if more than a day has passed, and once it has, reset the orderId counter
     *
     *   synchronized:  prevents two threads from setting orderIdCounter to a new AtomicLong
     */
    private synchronized long resetOrderIdCounterIfDateRoll() {

        long nextPerConnectionOrderId = orderIdCounter.incrementAndGet();

        if (nextPerConnectionOrderId > dateRollOrderIdLimit) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - connectionTime > ONE_DAY_MILLIS) {
                // we have been connected for more than 24 hours, and generated a hundred billion orders
                // we can safely reset the orderId counter, as it is a new trading day.
                orderIdCounter = new AtomicLong(0); // back to 1 for the next orderId
            }
        }
        return nextPerConnectionOrderId;
    }

}
