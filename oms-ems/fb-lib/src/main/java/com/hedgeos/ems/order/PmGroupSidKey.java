package com.hedgeos.ems.order;

import com.google.common.primitives.Longs;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class PmGroupSidKey implements Comparable<PmGroupSidKey> {
    // hunch that using sid first is quicker for key calculations
    public final long sid;
    public final String pmGroup;

    @Override
    public int compareTo(PmGroupSidKey other) {
        if (this == other)
            return 0;
        if (this.sid == other.sid) {
            return this.pmGroup.compareTo(other.pmGroup);
        }

        return Longs.compare(this.sid, other.sid);
    }
}
