package com.hedgeos.ems.order;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
public class PositionFileRow {
    public final long sid;
    public final String account;

    // TODO: needs to be exact?  Precision 12 here.
    // TODO:  Look for usages, make sure can be imprecise.
    public final double position;

    // need not be precise, risk checks at higher than you should trade
    public final Double price;

    public final long rowNumber;
}
