package com.hedgeos.ems.order;

import com.google.flatbuffers.FlatBufferBuilder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import order.ExecutionVenue;

@EqualsAndHashCode
@NonNull
@Data
public class EmsExecutionVenue implements Comparable<EmsExecutionVenue>{

    int id;
    String name;

    @Override
    public int compareTo(@NonNull EmsExecutionVenue other) {
        if (other == this)
            return 0;

        return Integer.compare(other.id, this.id);
    }

    public int asExecutionVenueFlatbuffer(FlatBufferBuilder envBuilder) {
        int venueNameOffset = envBuilder.createString(name);
        int venueOffset =
                ExecutionVenue.createExecutionVenue(envBuilder, 0, id, venueNameOffset);
        return venueOffset;
    }
}
