package com.hedgeos.ems.util;

import com.google.flatbuffers.FlatBufferBuilder;
import general.Price;
import general.Quantity;

import java.math.BigDecimal;

import static com.hedgeos.ems.order.OrderUtil.isBuy;

public class QuantityConverter {

    public static final BigDecimal ONE_BILLION_BIG_DECIMAL = new BigDecimal(1_000_000_000);
    public static final double ONE_BILLION_DOUBLE = Math.pow(1000.0, 3.0);

    public static Double quantityToDouble(Quantity lastShares) {
        return lastShares == null ? null :
                (double) lastShares.quantity() +
                        (double) lastShares.nanoQty() / ONE_BILLION_DOUBLE;
    }

    public static Quantity convertDoubleToQty(double lastSharesDouble) {
//        long whole = (long)lastSharesDouble;
//        double fraction = lastSharesDouble - (whole * 1.0d);
//        int nanos = (int)(fraction * 1.0E9);
//
//        Quantity result = QuantityConverter.makeQuantityFromFields(whole, nanos);
//
//        return result;

        // TODO:  Speed this up
        // no other way to work
        BigDecimal bd = new BigDecimal(String.valueOf(lastSharesDouble));
        BigDecimal fractionalPart = bd.remainder(BigDecimal.ONE);
        int nanos = fractionalPart.multiply(ONE_BILLION_BIG_DECIMAL).intValue();
        Quantity bdResult =
                QuantityConverter.makeQuantityFromFields(bd.intValue(), nanos);
        return bdResult;
    }

    public static Quantity makeQuantityFromFields(long wholeAmount, int fraction) {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int quantityOffset = Quantity.createQuantity(builder, wholeAmount, fraction);
        builder.finish(quantityOffset);
        Quantity quantityObj = Quantity.getRootAsQuantity(builder.dataBuffer());
        return quantityObj;
    }

    public static Double priceToDouble(Price price) {
        return price == null ? null :
                (double) price.units() +
                        (double) price.nanos() / ONE_BILLION_DOUBLE;
    }

    public static int doubleToPriceBuffer(FlatBufferBuilder builder, double limitPrice, int currency) {
        BigDecimal bd = new BigDecimal(String.valueOf(limitPrice));
        BigDecimal fractionalPart = bd.remainder(BigDecimal.ONE);
        int nanos = fractionalPart.multiply(ONE_BILLION_BIG_DECIMAL).intValue();
        int priceOffset = Price.createPrice(builder, currency, bd.intValue(), nanos);
        return priceOffset;
    }

    public static double getSignedQuantity(int side, double unsignedIncrementalQuantity) {
        double signedQuantity = unsignedIncrementalQuantity;
        if (false == isBuy(side)) {
            signedQuantity = -1 * signedQuantity;
        }
        return signedQuantity;
    }
}
