package com.hedgeos.ems.order;

public class OrderIdConstants {
    // this offset is used for orderIds for each connectionId
    public static final long ONE_TRILLION_CONNECTION_ID_OFFSET = 1_000_000_000_000L;
    // this offset is added for each reconnect by each connection
    public static final long ONE_BILLION_ORDERS_PER_RECONNECT_OFFSET = 1_000_000_000L;
    // the system supports 218 trillion orders total.  while in practice never happens, check.
    public static final int DIGITS_IN_CLORID_FOR_ORDERID = 8;
    public static final int BASE_62_FOR_CLORDID_STUFFING = 62;

    // 218_340_105_584_896L; 218 trillion orderIds is the max we can stuff in the ClOrdId
    // using base62 and 8 digits; see the docs/decisions
    public static final long MAX_ORDER_IDS_ALLOWED_IN_CLORDID_STUFFING =
            (long) Math.pow(BASE_62_FOR_CLORDID_STUFFING, DIGITS_IN_CLORID_FOR_ORDERID);

    // connectionId is one char of the ClOrdId stuffing; can only go to 62.
    public static final int MAX_CONNECTION_IDS = 62;

    // this offset is added if you reconnect more than ten times
    public static final long SIXTY_TWO_TRILLION = MAX_CONNECTION_IDS * ONE_TRILLION_CONNECTION_ID_OFFSET;

    // around three trillion max orders per connection (per day)
    public static final long MAX_TRILLIONS_PER_CONNECTION =
            MAX_ORDER_IDS_ALLOWED_IN_CLORDID_STUFFING / MAX_CONNECTION_IDS / ONE_TRILLION_CONNECTION_ID_OFFSET;

    // 1000 reconnects
    public static final long RECONNECTS_PER_TRILLION_OFFSETS =
            (ONE_TRILLION_CONNECTION_ID_OFFSET / ONE_BILLION_ORDERS_PER_RECONNECT_OFFSET);

    // 3000 reconnects for each trillion offsets (per day)
    public static final long MAX_RECONNECTS_PER_CONNECTION =
            MAX_TRILLIONS_PER_CONNECTION * RECONNECTS_PER_TRILLION_OFFSETS;
    public static final long ONE_DAY_MILLIS = 1000 * 60 * 60 * 24;
}
