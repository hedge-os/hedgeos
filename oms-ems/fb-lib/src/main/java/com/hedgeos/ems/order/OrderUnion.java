package com.hedgeos.ems.order;

import general.Quantity;

import javax.lang.model.type.UnionType;

public class OrderUnion {

    public enum UnionType { ORDER, REPLACE, CANCEL }

    private order.Order order;
    private order.CancelReplace replace;
    private order.Cancel cancel;
    private UnionType unionType;

    public void setOrder(order.Order order) {
        this.order = order;
        this.unionType = UnionType.ORDER;
    }

    public void setCancelReplace (order.CancelReplace cancelReplace) {
        this.replace = cancelReplace;
        this.unionType = UnionType.REPLACE;
    }

    public void setCancel(order.Cancel cancel) {
        this.cancel = cancel;
        this.unionType = UnionType.CANCEL;
    }

    public long hedgeOsOrderId() {
        return switch (unionType) {
            case ORDER -> order.hedgeOsOrderId();
            case REPLACE -> replace.hedgeOsOrderId();
            case CANCEL -> cancel.hedgeOsOrderId();
        };
    }


    public long submitTimestampMillisUtc() {
        return switch (unionType) {
            case REPLACE -> replace.submitTimestampMillisUtc();
            case ORDER -> order.submitTimestampMillisUtc();
            case CANCEL -> cancel.submitTimestampMillisUtc();
        };
    }

    public Quantity quantity() {
        return switch (unionType) {
            case REPLACE -> replace.quantity();
            case ORDER -> order.quantity();
            case CANCEL -> order.quantity();
        };
    }

// letting the venue handle the order status problem (for now.)
//    public int orderStatus() {
//        return switch (unionType) {
//            case ORDER -> order.orderStatus();
//            case REPLACE -> replace.orderStatus();
//            case CANCEL -> order.orderStatus();
//        };
//    }


}
