package com.hedgeos.ems.util;

import java.util.concurrent.atomic.AtomicBoolean;

public class CompareAndSwapLock {

    // https://jenkov.com/tutorials/java-concurrency/compare-and-swap.html
    private AtomicBoolean locked = new AtomicBoolean(false);

    public void unlock() {
        this.locked.set(false);
    }

    public void lock() {
        while(!this.locked.compareAndSet(false, true)) {
            // busy wait - until compareAndSet() succeeds
        }
    }

}