from random import randint
import time


class Ravine:
    playerHearts = 0
    stoneCount = 0
    woodCount = 0
    fiberCount = 0
    oneFoodCount = 0
    twoFoodCount = 0
    nightCount = 0
    hasSpear = None
    hasBasket = None
    hasShelter = None
    isFireLit = None

    def __init__(self, hp, stc, woc, fic, one, two, nic, spe, bas, she, fir):
        self.playerHearts = hp
        self.stoneCount = stc
        self.woodCount = woc
        self.fiberCount = fic
        self.oneFoodCount = one
        self.twoFoodCount = two
        self.nightCount = nic
        self.hasSpear = spe
        self.hasBasket = bas
        self.hasShelter = she
        self.hasFireLit = fir

    def tutorial(self):
        global start
        print("You have crash landed on an island")
        time.sleep(2)
        print("You have to survive for 8 nights to be rescued")
        time.sleep(2)
        print("There will be a day and night cycle")
        time.sleep(2)
        print("During the day you can forage, light a fire, eat, or craft")
        time.sleep(3)
        print("To forage for 1 item it will cost you a heart, which you have a total 6 of")
        time.sleep(4)
        print("When you forage you can find berries and apples which restore hearts")
        time.sleep(3)
        print("You can also find wood, stone, and fiber which are used in crafting")
        time.sleep(5)
        print("You can craft a spear, basket, or shelter")
        time.sleep(2)
        print("Spears can prevent animal attacks and cost 1 wood and 1 stone")
        time.sleep(3)
        print("Baskets allow you to get an extra item when foraging and cost 1 wood and 2 fiber")
        time.sleep(3)
        print("Shelters shield you from weather events and cost 2 wood, 2 stone, and 2 fiber")
        time.sleep(3)
        print("You can also make a fire which can prevent smaller animal attacks and psychological effects of the "
              "isolation")
        time.sleep(4)
        print("After the day you will have to endure the night")
        time.sleep(2)
        while True:
            try:
                start = int(input("Press 1 to begin\n"))
            except ValueError:
                print("Sorry, I didn't understand")
                continue
            else:
                break
        if start == 1:
            self.day()

    def day(self):
        global df, dc, de, dl, nd
        self.isFireLit = False
        while True:
            print("You have " + str(self.playerHearts) + " Hearts")
            try:
                df = int(input("Would you like to forage?(Type 1 for Yes, Type 2 for No)\n"))
            except ValueError:
                print("Sorry, I didn't understand")
                continue
            else:
                break
        while True:
            if df == 1:
                self.forage()
                break
            if df == 2:
                break
        while True:
            print(
                "You have " + str(self.woodCount) + " pieces of wood, " + str(self.stoneCount) + " stones, and " + str(
                    self.fiberCount) + " pieces of fiber")
            time.sleep(3)
            try:
                dc = int(input("Would you like to craft?(Type 1 for Yes, Type 2 for No)\n"))
            except ValueError:
                print("Sorry, I didn't understand")
                continue
            else:
                break
        while True:
            if dc == 1:
                if self.stoneCount < 1 and self.woodCount < 1 and self.fiberCount < 1:
                    print("You have no materials")
                    break
                else:
                    self.craft()
                    break
            if dc == 2:
                break
        while True:
            print("You have " + str(self.oneFoodCount) + " berries and " + str(self.twoFoodCount) + " apples")
            time.sleep(2)
            try:
                de = int(input("Would you like to eat?(Type 1 for Yes, Type 2 for No)\n"))
            except ValueError:
                print("Sorry, I didn't understand")
                continue
            else:
                break
        while True:
            if de == 1:
                self.eat()
                break
            if de == 2:
                break
        while True:
            print("You have " + str(self.woodCount) + " pieces of wood")
            time.sleep(1)
            try:
                dl = int(input("Would you like to light the fire?(Type 1 for Yes, Type 2 for No)\n"))
            except ValueError:
                print("Sorry, I didn't understand")
                continue
            else:
                break
        while True:
            if dl == 1:
                self.lightFire()
                break
            if dl == 2:
                break
        while True:
            try:
                nd = int(input("Would you like to continue into the night?(Type 1 to continue)\n"))
            except ValueError:
                print("Sorry, I didn't understand")
                continue
            else:
                break
        while True:
            if nd == 1:
                self.newNight()
                break

    def forage(self):
        if self.playerHearts < 2:
            print("You do not have enough hearts\n")
        elif self.hasBasket:
            i = randint(0, 4)
            if i == 0:
                self.woodCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.woodCount) + " pieces of wood")
            if i == 1:
                self.stoneCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.stoneCount) + " stones")
            if i == 2:
                self.fiberCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.fiberCount) + " pieces of fiber")
            if i == 3:
                self.oneFoodCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.oneFoodCount) + " berries")
            if i == 4:
                self.twoFoodCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.twoFoodCount) + " apples")
            j = randint(0, 4)
            if j == 0:
                self.woodCount += 1
                print("You have " + str(self.woodCount) + " pieces of wood")
                print("You have " + str(self.playerHearts) + " Hearts")
            if j == 1:
                self.stoneCount += 1
                print("You have " + str(self.stoneCount) + " stones")
                print("You have " + str(self.playerHearts) + " Hearts")
            if j == 2:
                self.fiberCount += 1
                print("You have " + str(self.fiberCount) + " pieces of fiber")
                print("You have " + str(self.playerHearts) + " Hearts")
            if j == 3:
                self.oneFoodCount += 1
                print("You have " + str(self.oneFoodCount) + " berries")
                print("You have " + str(self.playerHearts) + " Hearts")
            if j == 4:
                self.twoFoodCount += 1
                print("You have " + str(self.twoFoodCount) + " apples")
                print("You have " + str(self.playerHearts) + " Hearts")
        else:
            i = randint(0, 4)
            if i == 0:
                self.woodCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.woodCount) + " pieces of wood")
                print("You have " + str(self.playerHearts) + " Hearts")
            if i == 1:
                self.stoneCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.stoneCount) + " stones")
                print("You have " + str(self.playerHearts) + " Hearts")
            if i == 2:
                self.fiberCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.fiberCount) + " pieces of fiber")
                print("You have " + str(self.playerHearts) + " Hearts")
            if i == 3:
                self.oneFoodCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.oneFoodCount) + " berries")
                print("You have " + str(self.playerHearts) + " Hearts")
            if i == 4:
                self.twoFoodCount += 1
                self.playerHearts -= 1
                print("You have " + str(self.twoFoodCount) + " apples")
                print("You have " + str(self.playerHearts) + " Hearts")

    def craft(self):
        global craftOption
        while True:
            try:
                craftOption = int(input(
                    "What would you like to craft, Spear [1 wood, 1 stone] (Type 1), Basket [1 wood, 2 fiber] (Type "
                    "2), Shelter [2 wood, 2 stone, 2 fiber] (Type 3), (Type 4 for No)\n"))
            except ValueError:
                print("Sorry, I didn't understand")
                continue
            else:
                break
        while True:
            if craftOption == 1:
                self.craftSpear()
                break
            elif craftOption == 2:
                self.craftBasket()
                break
            elif craftOption == 3:
                self.craftShelter()
                break
            elif craftOption == 4:
                break

    def craftSpear(self):
        if self.hasSpear:
            print("You already have a spear")
            self.craft()
        elif self.woodCount < 1:
            print("You don't have enough wood")
            self.craft()
        elif self.stoneCount < 1:
            print("You don't have enough stone")
            self.craft()
        else:
            self.hasSpear = True
            self.stoneCount -= 1
            self.woodCount -= 1
            print("Spear Crafted")

    def craftBasket(self):
        if self.hasBasket:
            print("You already have a basket")
            self.craft()
        elif self.woodCount < 1:
            print("You don't have enough wood")
            self.craft()
        elif self.fiberCount < 2:
            print("You don't have enough fiber")
            self.craft()
        else:
            print("Basket Crafted")
            self.hasBasket = True
            self.woodCount -= 1
            self.fiberCount -= 2

    def craftShelter(self):
        if self.hasShelter:
            print("You already have a shelter")
            self.craft()
        elif self.woodCount < 2:
            print("You don't have enough wood")
            self.craft()
        elif self.stoneCount < 2:
            print("You don't have enough stone")
            self.craft()
        elif self.fiberCount < 2:
            print("You don't have enough fiber")
            self.craft()
        else:
            print("Shelter Crafted")
            self.hasShelter = True
            self.woodCount -= 2
            self.stoneCount -= 2
            self.fiberCount -= 2

    def eat(self):
        global bd, ed
        if self.playerHearts == 6:
            print("You are full on hearts")
        elif self.oneFoodCount == 0 and self.twoFoodCount == 0:
            print("You have no food")
        elif self.playerHearts == 5:
            while True:
                try:
                    bd = int(input("Would you like to eat berries?(Restores 1 Heart)(Type 1 for yes, Type 2 for no)\n"))
                except ValueError:
                    print("Sorry, I didn't understand")
                    continue
                else:
                    break
            while True:
                if bd == 1:
                    self.playerHearts += 1
                    self.oneFoodCount -= 1
                    self.eat()
                    break
                if bd == 2:
                    break
        else:
            while True:
                try:
                    ed = int(input(
                        "Would you like to eat berries,(Restores 1 Heart) apples, or noting?(Restores 2 Hearts)(Type "
                        "1 for Berries, Type 2 for Apples, Type 3 for Nothing)\n"))
                except ValueError:
                    print("Sorry, I didn't understand")
                    continue
                else:
                    break
            while True:
                if ed == 1:
                    self.playerHearts += 1
                    self.oneFoodCount -= 1
                    print("You have restored 1 Heart")
                    self.eat()
                    break
                if ed == 2:
                    self.playerHearts += 2
                    self.twoFoodCount -= 1
                    print("You have restored 2 Hearts")
                    self.eat()
                    break
                if ed == 3:
                    break
                else:
                    print("Sorry I don't understand")

    def lightFire(self):
        if self.woodCount < 1:
            print("You don't have enough wood")
        else:
            self.isFireLit = True
            self.woodCount -= 1

    # nothingNight = [The Calm, The Fog, The Quiet]
    # weatherNight = [The Flood, The Mountain(Rock Slide), The Rain, The Wind]
    # animalNight = [The Bats, The Wolves, The Bear, The Cougar]
    # psychoNight = [The Voices, The Fear, The Guilt]
    # positiveNight = [The Owl]

    def newNight(self):
        self.nightCount += 1
        if self.nightCount >= 8:
            self.rescue()
        else:
            n = randint(0, 14)
            if n == 0:
                print("The Calm")
                time.sleep(1)
                print("Is anybody else nervous about how quiet it is tonight? No effects tonight")
                time.sleep(4)
            if n == 1:
                print("The Fog")
                time.sleep(1)
                print(
                    "A blanket of thick, misty gloom envelops the camp. But at least it's a blanket right? No effects "
                    "tonight")
                time.sleep(7)
            if n == 2:
                print("The Quiet")
                time.sleep(1)
                print(
                    "You stare into the darkness, but tonight, nothing stares back. It is quiet, and in a way, "
                    "beautiful. no effects tonight")
                time.sleep(8)
            if n == 3:
                print("The Flood")
                time.sleep(1)
                print("Distant rainstorms send their water roaring through camp.")
                time.sleep(4)
                if self.hasShelter:
                    print("You were sheltered")
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 4:
                print("The Mountain")
                time.sleep(1)
                print(
                    "A massive wave of rocks, mud, and debris storms down the mountain and into camp. Uninvited, "
                    "no less.")
                time.sleep(6)
                if self.hasShelter:
                    print("You were sheltered")
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 5:
                print("The Rain")
                time.sleep(1)
                print("Damp, Cold, and Starving")
                time.sleep(3)
                if self.hasShelter:
                    print("You were sheltered")
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 6:
                print("The Wind")
                time.sleep(1)
                print("It howls through the tress and chills you to your very marrow.")
                time.sleep(4)
                if self.hasShelter:
                    print("You were sheltered")
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 7:
                print("The Bats")
                time.sleep(1)
                print("A cloud of bloodthirsty creatures comes swarming through camp.")
                time.sleep(4)
                if self.isFireLit:
                    print("The fire scared away the animals")
                elif self.hasSpear:
                    print("You used a spear and it broke")
                    self.hasSpear = False
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 8:
                print("The Wolves")
                time.sleep(1)
                print("They came to welcome you to the neighborhood, but they're not leaving without dinner.")
                time.sleep(5)
                if self.hasSpear:
                    print("You used a spear and it broke")
                    self.hasSpear = False
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 9:
                print("The Bear")
                time.sleep(1)
                print("Starved for food, a huge bear wanders into your camp hoping to find a fresh meal. And it does.")
                time.sleep(5)
                if self.hasSpear:
                    print("You used a spear and it broke")
                    self.hasSpear = False
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 10:
                print("The Cougar")
                time.sleep(1)
                print("Bad Kitty")
                time.sleep(1)
                if self.hasSpear:
                    print("You used a spear and it broke")
                    self.hasSpear = False
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 11:
                print("The Voices")
                time.sleep(1)
                print("Every time you close your eyes, you hear dead people talking. Best to just keep your eyes open.")
                time.sleep(5)
                if self.isFireLit:
                    print("The fire was able to comfort you")
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 12:
                print("The Fear")
                time.sleep(1)
                print("It's difficult to sleep when the fear of impending death is constantly looming over you.")
                time.sleep(6)
                if self.isFireLit:
                    print("The fire was able to comfort you")
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 13:
                print("The Guilt")
                time.sleep(1)
                print("Maybe the people who died in the crash were actually the lucky ones...")
                time.sleep(4)
                if self.isFireLit:
                    print("The fire was able to comfort you")
                else:
                    self.playerHearts -= 1
                    print("You lost 1 heart")
            if n == 14:
                print("The Owl")
                time.sleep(1)
                print("An Owl takes roost over your camp, and for some reason, everyone feels a lot better.")
                time.sleep(6)
                if self.playerHearts == 6:
                    print("You would've gained a heart but you are full")
                else:
                    self.playerHearts += 1
                    print("You gained 1 heart")
        if self.playerHearts > 0:
            self.day()
        else:
            self.death()

    def rescue(self):
        print("You have been rescued!")
        quit()

    def death(self):
        print("You have died, you survived " + str(self.nightCount) + " nights, better luck next time")
        quit()


guide = Ravine(6, 0, 0, 0, 0, 0, 0, None, None, None, None)

letsPlay = Ravine(6, 0, 0, 0, 0, 0, 0, None, None, None, None)
while True:
    try:
        isNew = int(input("Do you know how to play Ravine?(Type 1 for Yes, Type 2 for No)\n"))
    except ValueError:
        print("Sorry, I didn't understand")
        continue
    else:
        break
while True:
    if isNew == 1:
        letsPlay.day()
        break
    if isNew == 2:
        guide.tutorial()
        break