# Flat files for symbology

### Background
**Symbology** is known as the problem of different symbols to trade at 
different exchanges and vendors.  For example IBM via "Refinitiv" (formerly Reuters)
is known as IBM.N because it trades on NYSE, and .N is the NYSE extension.
Bloomberg however, tracks this as "IBM US" or "IBM US Stock".  Yet on NYSE,
IBM trades as symbol "IBM".  The connection to the exchange needs to send IBM.
The EMS service will not likely be connected to the NYSE, but instead, to
a bank who will route the order to the NYSE.  Yet, the banks may support
Bloomberg, Reuters, or other symbol sets (symbologies); because NYSE only covers 
certain US stocks.

## Problem statement

#### Need for security_id (sid)
Any financial institution needs to track symbols using one single identifier.
This should be a "long" or can be mapped to a long, and are 
managed in a **'security bank'**.  Generally
the security bank is a set of database tables, wrapped by a service
and local caching layer for fast lookup.

This long, *security_id*,
or **"sid"** allows the firm to track exposure and positions to the 
security across different exposures at institutions which refer to it 
using alternate symbol sets.  We refer to it as **sid** for brevity in the "api"
package.

#### EMS usage of 'sid'
The traders must not be asked to manage and send the correct symbol
for the counterparties.  If they do, then each of the N desks would need
a system to do this.  Instead, it is simpler to have the systems operate
using security_id (sid), and have the EMS translate to the necessary 
symbology as it generates the FIX message outgoing.  In this way
the EMS speaks Refinitiv, Bloomberg, NYSE, and any other necessary symbology,
while the users of the system are isolated from this complexity.

#### Mapping from 'sid' to FIX venue symbology
Therefore, the problem is how to map a set of 'long' numbers which
are the securities known in the *'security bank'* at the buying institution
to the symbol sets known by the FIX trading venues.

## Flat files loaded from the ems-service working folder

#### Format of the files
The ems-service system will load flat files of this format.
```text
sid,symbol,priority
1,USD
2,SPY,1
3,USD/JPY,1
4,IBM
```

#### Column definitions
**sid**:  The security_id which will be sent by the ems-clients to the ems-service
to trade.

**symbol**: The symbol to send to the FIX venue.

**priority**: If priority is set, a small cache is loaded and checked
before the primary cache; to decrease latency.  Priority can only be '1'
in the first iteration.  The small cache will fit in a cache line or L2,
compared to the overall symbol cache which will likely be in main memory;
however this is the JVM optimizing.

#### Location of the files

Default:

```text
${EMS_CONFIG_FOLDER}/symbology
```

The files will be checked for in a sub-folder **/symbology** 
of the **EMS_CONFIG_FOLDER**,
unless an environment variable is set **EMS_SYMBOL_FOLDER**.

#### File names
The file names will be of this format.
```text
bloomberg_20230525.csv
nyse_20230525.csv
```
Every five minutes, the folder will be checked for symbology which should take
effect on a particular trading day.

A file for a future date, e.g. 20230526 fill NOT be loaded if the trading date is 20230525.
This allows operators to push new files for future dates at any time.

#### Replace a file
If a mistake is made and a correction is necessary, corrections can be
made in a file **with only the corrections**, in a file named with "underscore 1",
like this:
```text
bloomberg_20230525_1.csv
```
This file may have only one or two tickers and sids, or the entire set.
These will replace the existing symbols, when the five-minute file check
scheduled job runs.  *Generally it is expected corrections are not needed
but an operator could create this file in an emergency to correct one symbol.*

### Symbology per FIX Adapter
Each FIX Adapter YAML config file loaded from **EMS_CONFIG_FOLDER**
requires a property:  **symbology**

**symbology** values is generally one of the following (case-insensitive):
- Bloomberg
- Refinitiv
- NYSE
- NASDAQ
- OCC
- LSE
- DAX
- (...) any others needed.

Any value can be used as the "symbology" property in the config file;
but it must be matched by a symbology file of the same name in this folder.

The symbology will be loaded from
the folder **EMS_CONFIG_FOLDER**, sub-folder **/symbology**.

If **EMS_SYMBOL_FOLDER** is set, then this variable can be used to override this default.

The symbology named file must exist, for at least one day, in this folder.
If the next day is not present, *the prior days symbology will be used to trade.*

It is the responsibility of the operator, to verify and raise alarms
if the symbology files are not loaded, and to take quick action.
In a catastrophe, any commonly traded symbols which have been renamed
could be manually authored in a symbol file, and dropped in this folder.

If none of these files are found for a given symbology, 
**the FIX Adapter will fail to start**
and the ems-service will fail if the system is not configured to
ignore FIX Adapter failures.

### Reasons this is preferred

1. **robustness:**  No external dependencies once the system is started (not calling databases.)
2. **simplicity:**  Trivial for operators to manually update in a catastrophe or emergency.
3. **co-location concerns:**  This is important - do not need additional machines in a co-location 
facility to store the symbology on a database, or other cores
on a systematic trading machine which may be take up by a database, in a co-located setup in an 
expensive rented co-location facility.  (The ems-service is designed for sub-50 microsecond latency, 
suited for low latency co-located trading.)

## Alternatives considered

### Database
A database could not be run on the same machine co-located facility.
The database could cause severe jitter on the system for trading.
Furthermore, a database adds unnecessary risk and complexity to the trading system.
It is expected the file will be generated by a daily export job from a security-bank
backed by a database, with a query and a secure copy, on a cron or a scheduled task
in a job scheduler.

### H2 or local database

Similar concerns to above, a file is simple, easy to edit, trivial to look for.

### Another service

A security-bank should be used to vend the security_ids at any firm.
The security-bank can run and also run the export to the ems-service
filesystem, as one cron job a day.