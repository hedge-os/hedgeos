## Locates tracking: preventing short trading without locates

### Context and problem statement

### Background

Locates are provided by banks; representing shares they guarantee to loan you 
in the event you want to short a security.  They are provided to institutional 
clients at the start of trading day, generally in a file of ticker to the quantity
of locates provided for the buy side firm (hedge fund.)  Generally locates are provided as 
part of brokerage for a small fee; the rate to "borrow" the shares to short them, 
but may be a higher fee for locating certain names which are 'hard to locate.'

Positions in securities, are tracked at a portfolio manager group (pmGroup) or firm wide level.

Once trading opens, buying and selling begins.  If you have a long position 
in a name, then when you sell, you simply sell the quantity you own as a firm,
detracting from your held position amount in the name.  This can continue
until the fund owns zero of the name.  If you purchase more shares, you
sell those shares you purchased.  

This continues until you are 'net short';
either as a portfolio team or as a group / firm; the level at which you 
choose to track locates.

If you wish to be 'net short' as a group, locates must be available.  
The total short position of a buy side, after subtracting the longs achieved, must not exceed this
amount of locates.  

### Formula to test shorts

A 'Sell' order must be rejected if this comparison is false:

`(Longs Executed + Locates Provided) - (Shorts executed + Shorts requested) > 0`

![Visually](../locates/locatesTheory.drawio.png)

This can be thought of simply as this; *you need
enough long position and locates to cover the 
total amount of selling throughout the day.*

The bank handling the trade will also track the number of locates available,
the buy side firm must track it and provide a means for traders to determine
if there are enough locates to achieve a given short position in a name.

## Decision drivers

1. Position File load at Start of Day
   1. A position file is loaded at the start of day with the known positions in a name.
   2. The position is tracked; however, the position is not updated until orders are *filled*.
   3. This means the position is not a reflection of how many locates are available. 
      1. e.g. Multiple orders could be placed for a security, and not affect the position, 
      yet once executed, they would require locates.
      1. Therefore, the running position must not be used for locate generation purposes.
      2. **Only the executed long trades will contribute to the long position, not the requested long trades.**
1. Position keeping system to track trading
   1. Both the **requested** (open) and completed trades long and short directionally are tracked to determine
      the need for locates; along side the position.  This is in "EmsPosition".
   2. The EmsPosition is by the combination of pmGroup and sid: Porfolio Manager group, and security id.
       1. The portoflio manager group is an arbitrary grouping of portfolio managers.
       1. The security id is from the security bank system, exported into the symbology files.
   2. **Return cancelled quantities**
      3. When a trade is cancelled, the system **reverses the cancelled quantity** for
      the amount of a trade which is not completed.
2. Locates are loaded as start of day file
   1. The locate quantity is known, generally start of day with a file being loaded.
   2. Loading as a file follows prior decisions regarding an EMS and file loading (stability, simplicity)

## Considered options

Several amounts need to be tracked to calculate locates:
1.  Use the traded amounts 
1.  start of day position
1. Requested trades 
1. Completed trades
1. Locates per ticker provided by the bank.
                
to determine if locates are available.

## Decision outcome

Use start of day position and then track open and completed orders, as the total against
which locates are calculated; using the formula provided above.

The PositionManager, using EmsPosition tracks the amount of trading 
from start of day.

The locate total is held in memory; it does not change, and compared against the total
of the long and shorting, to verify the net total is always 
greater than or equal to zero.

Note that the EmsPosition must be locked during the comparison;
this avoids a data race where the position is changed with
a long or short trade during the calculation.

### Collaboration diagram

![collaboration diagram](../locates/locatesDesign.drawio.png)

#### Unused locates back to pool

If the order is cancelled, the quantity of outstanding 
short trades is reversed by the quantity not traded.
This opens up the ability to trade this amount short on 
another opening short trade.

```LocatesManager``` and associated code handles the locates
loading in memory; and the ability to fetch it using
a `PmGroupSidKey` (pmGroup + sid) and `EmsExecutionVenue`
which is the EMS internal representation of the api `ExecutionVenue`
object.  (Note that locates are provided on a per-execution venue basis.)

### Boundary conditions:  Positions file reloaded

If the position file is reloaded intraday, the start positions will be reset 
to the current amounts in the file.  If this happens, the exact amount
of trades as of the new position file load must be re-calculated.

Next, the amount of available locates must be re-calculated; according to 
the trading activity, and any reservations which are already out for trades
which are in-flight.

#### Order of loading positions

1.  Load the positions; from the file into "row" objects.
2.  For each position (positions by date)
   1. Lock the "locate position"
   1. Reset the trading value to zero, for this name, trade date
      2. Amount traded per pmGroup+sid since "position load" time
   1. Set the new position value
   1. Unlock the "locate position"