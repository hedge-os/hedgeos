# ClOrdId tag 11 stuffing using base62

## Problem statement

ClOrdId is used to match Fills to orders
which were sent, FIX tag 11.

FIX clients generate a unique tag11, for each
order they send to a counterparty. This could
be a simple counter if there were only one 
EMS running.  It must though support 
multiple fix servers.  A day token
is convenient to verify you are getting
the right fills for the right day 
from the counterparty (not yesterday for example.)
This happened with one counterparty, during
FIX certification with them (testing recovery.)

Every ClOrdId must be unique *every day*
from each counterparty
according to the FIX spec, and this makes
sense, otherwise you would have a conflict.

Therefore, you must construct a tag11
clientOrderID value which is unique every day
and is different for each EMS installation
you run, in the event two of them connect
to the same FIX venue.

Furthermore, as is seen in decision #3:
[Message routing using tag11 connectionID](./0003-message-routing-using-tag-11-client-order-id-stuffing.md)
we also know we need the connectionID in 
every ClOrdID replayed back to us,
to handle rapid routing back to the clients.

We need a counter which increments
for every orderId in the system.
It is preferable for this to be 
unique every day *across* venues,
this way each message can easily 
be identified independent of the venue
for debugging, emergencies, 
and for safety as well as simplicity sake.
                                    
Finally, some ClOrdIds at some venues
is limited to 17 chars or less
(it may be 11 chars at one venue.)

This does not leave much space to 
handle these concerns, considering
that a large number of orders may be generated.

## Decision:  base62 encode with specific slots

We will base62 encode to save space.

Each spot will have an assigned value.

The character encoding will be as follows

### Why base encode 62, why not base64?

It can be dangerous to send special chars to certain venues, and while 
the FIX spec calls for any ascii128 values including symbols which are non-end of message symbol, 
venues can break on it.  Also, it can make it more difficult to diagnose in an emergency
when you have a string with percent signs, and other charachters which on a command
line have special meaning.

We can base62 encode with no special symbols, using, the standard base62 encoding

https://en.wikipedia.org/wiki/Base62

#### **0-9,A-Z,a-z**

#### 10 + 26 + 26 = **62**

### tag11 char encoding:  base62

We will use this definition of each character in the tag11 string, base62 encoded.

**ClientOrderIdStuffer** will be a class which handles the stuffing and cracking of tag11.

#### Tag 11 encoding:


1. emsServerId:  allows for 62 EMS servers with uniqueness across them.
2. connectionId:  allows for 62 connections to each EMS server, more than sufficient as multiple EMS servers may be run.
3. dayOfMonth:  1-31, day of month, trading date.  Useful to observe logs and debugging.
4. hedgeOsOrderId: **8 digits** for base62 encoded ID, 62 to the power of 8 = **62^8** = 218.3 trillion = **218,340,105,584,896**
5. hedgeOsOrderId:  continued
6. hedgeOsOrderId:  continued
7. hedgeOsOrderId:  continued
8. hedgeOsOrderId:  continued
9. hedgeOsOrderId:  continued
10. hedgeOsOrderId:  continued
11. hedgeOsOrderId:  continued
12. pmGroupId:  **A 3 char moniker for the PM desk**  
13. pmGroupId:  second char of three char pmGroup
14. pmGroupId:  third char of three char pmGroup
15. securityId:  **10 digits** If the alternate FIX venue goes above 11 chars length for ClOrdId, we will encode our own security bank identifier in chars 12-18, for **3.5 trillion** ids (3,521,614,606,208)
16. securityId:
17. securityId:
18. securityId:
19. securityId:
20. securityId:
21. securityId:
22. securityId:
23. securityId:
24. securityId:
25. securityId:

------------------------------
### Exceptions to the rule

#### Tag11 is less than 18 chars long for a Venue

In the case tag11 is less than 18 chars; if the securityID fits in the remaining chars
then we will encode it in the remaining chars.  If the securityId is too large for 
whatever remains, then the securityId will be omitted.

FX securities will be the first securities, with securityIDs in the single digits or tens.
Stocks will come after this, in the hundreds of thousands (in the security bank.)

Only for Options trading is there a real risk of exceeding the securityId length.
If the options securityID can't fit, then when tag11 is returned,
we can't crack the securityID from the tag, and can lookup the symbol in a symbolMap.
This is not a problem as the symbolMap must exist in the EMS anyway, to generate
the symbol outgoing to the street in tag48.

In almost all cases the securityId will be in those characters, but care 
must be taken to unit test if it does not fit in the tag11 size of the venue.
