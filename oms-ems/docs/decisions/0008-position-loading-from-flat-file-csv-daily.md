# Flat files for Position Tracking 
### (and locates and risk-checks)

### Background
Positions are needed in the system, to determine
when you are short selling a security, vs. selling a
security you own.

#### Locates

https://centerpointsecurities.com/short-locates-guide/

Locates are required and the system must not trade
once a set of locates provided by a broker are exhausted.
You only need to use a locate when you are **short selling**
a security (you are not selling a position you currently hold.)

When locates are necessary, tag 114 in FIX must be set to Y:

https://www.onixs.biz/fix-dictionary/4.4/tagNum_114.html

Locates for each day available are FTPed from a broker (typically.)
Once the locates are loaded, you need to determine
if you need to use locates when a stock is sold.
To make this determination, you need to know your position
in the stock.

#### Optimization for multiple desks at a fund
When multiple desks are involved, there is an optimization
where if one desk is long a security and antoher wants to
go short, then in aggregate you do not need a locate.
This is a small win as you do not need to pay the cost of the
locates, but at the cost of added system complexity.

### Trade a security with one broker

If locates are needed, one stock should be traded
against a broker providing those locates.  
This way, the total position in the stock can be tracked
in one EMS (ems-service), and the locates are used
against the total daily available locates for a security
at this broker.  Trading without sufficient locates
can incur fines and penalties trading.  Using locates
is provided by brokerages for a daily cost of the locates,
and may be included in overall brokerage, or a specific fee.

## Problem statement

The EMS needs to perform two critical activities:
1.  Risk checks
2.  LocateReqd flag setting; tag 114.

### 1. Risk checks
Risk checks are necessary to avoid over-trading.
Knight trading incident is an example where risk checks
would have prevented a catastrophe.  One of the risk checks
is the total amount of a security held should not be above
a threshold.  To perform this check, the EMS must 
be aware of the total amount held at the start of the day,
and maintain this 'position' internally.

Risk checks will be covered in another decision document.

### 2.  Locates

If you do not own any of a stock (hold a position),
you must set a flag locates required tag 114 at many
FIX venues.  Penalties exist for not setting this flag,
or the trades will simply fail if the broker thinks
you don't have a position and have failed to set the 
locates flag.  

To know if you must set the locates flag, you must
test to see if you hold a position, and how much 
of a position you hold.  e.g. if you hold 50
and the ems-client sends an order to sell 100,
then you will be short 50 shares at the end of the trade
and therefore require locates.

In a day, a broker provides a limited amount of locates
in a stock.  Certain small cap stocks and certain 
heavily shorted names (e.g. Game Stop), may be 
difficult for the broker to provide locates,
as not many customers are maintaining a long position
in the security.  In this case, there may be no locates
from the broker, or there may be limited locates.

The EMS must only allow you to sell as many locates
as provided by the broker, and prevent you,
reporting a specific error "locates unavailable."

## Decision: use flat files to load positions

Default folder:

```text
${EMS_CONFIG_FOLDER}/positions
```

The EMS system loads a daily flat file of CSV,
from a folder **/positions** by default
using the **EMS_CONFIG_FOLDER** as a location.

This default can be overridden with an environment variable:
**EMS_POSITION_FOLDER**.

There will be a position file for every day, similar to the 
symbology files; however, a file is required for the 
trading day to start the ems-service, unless a flag is set
to override this check.  

### Position file

There must be a start of day position file for the trading day in question.
The name of the file contains the trading date, allowing
each trading date file to remain in the folder for audit:
```text
positions/positions_20230523.csv
```

### Overload file
An overload file names with **_1** may be used

```text
positions/positions_20230523_1.csv
```

This may be added in an emergency by an operator to force the 
EMS to adjust a position at the time the file is loaded.
For each security in the file, the position will be set 
at the time the file is loaded to the exact quantity of position in the
overload file.  This allows operators to adjust a position up or 
down in an emergency to reflect a phone trade done outside the EMS for 
example.

### Decision criteria

Similar to the decision 0007 on loading flat files of stocks,
we follow similar reasoning for flat file loading of positions.
The positions will not change until the open of a market
where a security is traded.  The most robust 
and lowest risk to having the EMS available (high availability)
is to load a flat file from S3 or a SAN, and we will follow
this approach to favor uptime, and no external dependencies
of the EMS system on another system being available.
This is the 'push' model, where a batch system pushes
the positions to the EMS as a flat csv file.

### Format of the file

```text
sid,account,position,price
3,A1534343,-30,117.12
1,A1457809,1000,54.1234
```

The file has the security Id (sid), and the trading account (tag 1.)

Each order places is for a trading account.
The trading accounts map to portfolio managers at the fund.

Certain securities are traded in certain trading accounts.
This is the account string in column 2.
e.g. US stocks for portfolio manager X are in account = Y.
This relationship must also be managed, and is outside
of the EMS system.  The ems-client must source this
mapping, and use the correct account when calling the EMS.

Column 1 is the security_id (sid).

Column 3 is the current position held in this account.

Column 4 is the price of the security.  The start of day exposure
is the position held times the price of the security.
This is needed for risk checks, which will be covered in another decision.

## Important notes on loading Positions

After the ems-service boots, with a position file
for the given trade date; it takes these
as the current positions, as it starts.

#### Load positions before trading opens
The ems-service expects to start before trading 
opens, but at any time it is started,
when it finds a file for the given trade date,
when it starts, it then assumes these are 
the current best known positions.  This allows
for setting the positions and restarting
in the middle of the day if necessary in an emergency
with a file of the current positions.

### Timestamp checking of the current trade date 
Every minute the system keeps looking for position files,
including the current trade date.  This allows
a new file to be dropped for the current trade date
while the system is running if need be to correct the
positions or update the positions the ems-service
knows, from an external system for example.
The file is checked every 60 seconds for a newwer file.
Once that file is found, any knowledge about a position
for the given trade date will be replaced with the
current csv file contents for the trade date found
on the filesystem.

### Days after the initial trade date
After the initial trade date are loaded
at midnight of the local system date
or when the TradeDateUtils.tradeDate()
decides the trade date has rolled.

At that time the PositionLoader thread
stops looking for the old trade date
and starts looking for a new trade date.

If this file doesn't arrive, the ems-service
will continue using the previous days position.

It is expected that a new file will be dropped
shortly after the close, and this could be dropped
again in the positions S3 or SAN folder
to update the positions for the upcoming trade date.

A restart is not necessary and the new positions
file will be picked up.  Once it is loaded,
this is loaded into memory as the positions for
the start of day.  Every minute this file can 
be reloaded.


-----------------------------------------
## Alternatives evaluated

See the symbology 0007 decision for alternatives
to this decision.

In summary a flat file is the lowest risk technology,
and allows for an operator to intervene, by 
altering providing a position overlay
similar to the EMS overlay.

This kind of operational simplicity is critical for an EMS
which may be critical to the survival of the firm.
