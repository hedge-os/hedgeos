# Locates per venue from flat files

### Background
Locates are discussed in the position loading
decision 0008.

#### Locates

https://centerpointsecurities.com/short-locates-guide/

## Decision: use flat files

The system will load csv files, by default from:
```text
${EMS_CONFIG_FOLDER}/locates
```

The EMS system loads a daily flat file of CSV,
from a folder **/locates** by default
using the **EMS_CONFIG_FOLDER** as a location.

This default can be overridden with an environment variable:
**EMS_LOCATES_FOLDER**.

Similar to positions, there should
be a locates file provided every day.

The system will continue to check every
minute for new locates files.

Similar to positions, once the locates
are loaded for a date, they become 
the setting in the system as of the 
time they are loaded.  Locates
are tracked against this file.

### Fallback to yesterday

If the system doesn't find a locates file
for the current date, it will use
the locates file for the prior date.

The operator is responsible for monitoring
and ensuring a new file is secure copied
to the S3 folder or SAN folder /locates.

#### File names
The file names will be of this format.
```text
locates_20230525.csv
```

### Overload file
An overload file names with **_1** may be used

```text
locates_20230523_1.csv
```

This may be added in an emergency by an operator to force the 
EMS to adjust locates at the time the file is loaded.

This is the standard mechanism in the ems system
allowing for emergency loading of data
into a running ems-service without a restart.

### Decision criteria

This follows other decisions for loading flat files.

The note here is, the files act as the API
for ingress into the EMS.

The reason we want flat files as the API,
is that it only has a dependency on the
SAN or S3 filesystem,
and once it is loaded, there is no dependency
during the trading day.

This files bases approach also offers 
trivial audit, if there are any issues
with locates, positions, risk checks
or otherwise.

### Format of the file

The locates file can take two formats.

#### Per account locates format
The first format alternative is per trading account, per execution venue:

```text
sid,venue,account,locates
3,MORGAN_STANLEY,A1534343,123
1,MORGAN_STANLEY,A1457809,456
```

The file has the security Id (sid), and the trading account (tag 1.)

Each order places is for a trading account.
The trading accounts map to portfolio managers at the fund.

**Column 3** is the amount of locates
made available to the trading account.

#### Total locates per venue

In this format, the system
is provided a total number of locates
for a security by a broker.  This
total amount of provided locates
is shared by the trading accounts
using this ems-service.

**Example**
```test
sid,venue,locates
1,MORGAN_STANLEY,456
3,LMAX,123
```

## Alternatives

Locates could be sourced from an API.

Again as in similar decisions,
the EMS will depend on only flat files
as a kind of API with zero external 
dependencies other than the filesystem.
