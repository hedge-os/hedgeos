# Risk Limits and Restrictions from flat files

## Context and Problem Statement

The ems-service must prevent over trading as occurred in the infamous [Knight trading incident](https://www.henricodolfing.com/2019/06/project-failure-case-study-knight-capital.html).
Limits must be available for higher than defaults.
Typical limits are:

1. Total position size / total exposure 
1. Quantity / exposure of trades per (second/minute/hour/day)
1. Restricted securities, long and short

## Considered Options

1. Use 'account' for tag1 to set limits
2. Use 'desk' as a name, and manage limits by 'desk'
3. Use 'pmGroup' as a name, allowing for flexible limits.

#### Note on flat files

Similar to prior decisions, flat files are preferred for stability during the trading day.
Reloading should exclusively occur after trading hours.

Flat files are trivial to edit on a filesystem in an emergency, by 
an operator of the system, is the most important concern 
for simplicity in an emergency.

Otherwise, a CSV file is like any other system messages
you may exchange.  File are preferable in trading
because the contents are persistent and stable, and can easily be audited.

## Decision Outcome

### pmGroup - for flexible limits and locates

A `pmGroup` is an *arbitrary* group, which can be one
porfolio manager, or a group of managers, bound by limits.
As well this, group can share locates, which are provided
for a given `pmGroup`.

The `pmGroup` is a three char code 
which must be present on an order.
This pmGroup is used to verify limit checks pass before
sending orders to the market.

Limits are based on `pmGroup` and sid (security id.)

The combination of these two, or one without the other
and wildcards are used with flat files to define limtis.

#### pmGroup usage in the system
pmGroup is embedded in tag11.  This is a sub-decision.
Without embedding it in tag11, the only way on an 
ExecutionReport from the market would be to keep 
a map of every Order in memory, by orderId.
Then the execReport could fetch the order, from this map.
Orders in memory, would imply that every Order be recovered into memory
when the system is restarted; to handle fills after the restart.

At time of this decision, we are trying to avoid
keeping every order in memory; as it is not necessary
for limits checks, or otherwise.

### pmGroup Syntax

Therefore, the syntax for pmGroup is:
1.  pmGroup must be **exactly three characters**
1.  pmGroup must **NOT** contain special chars; must be only letters and numbers.

### Flat files for positions and trade limits

#### Directory
The default directory flat files is named `risklimits`:

     ${EMS_CONFIG_FOLDER}/risklimits

The default location is the `EMS_CONFIG_FOLDER` environment variable value.

#### File names
Flat files will be loaded with the following formats:

        1. position_limits_20230525.csv: pmGroup,sid,qtyLong,qtyShort,exposureLong,exposureShort
        2. trade_limits_20230525.csv:  pmGroup,sid,timeUnit,numTimeUnits,qtyLong,qtyShort,exposureLong,exposureShort
        
## File syntax

Two important syntax decisions are used to make two files cover all three concerns:

The **pmGroup** and **sid (security_id)** columns may contain a dash, be blank, or a star

### Star syntax means wildcard
A "star" **(*)** character means wildcard for that column.

#### Wildcard sid
A star in the sid row sets a limit for an pmGroup, **for each security** 

e.g.:  pmGroup=abc, sid=*, qtyLong=1000

This means every security in pmGroup 'abc' has the limits of 1000 qtyLong.

#### Wildcard pmGroup
A star in the pmGroup row sets a limit for a sid, **for each pmGroup**

e.g. pmGroup=*, sid=33

#### Hyphen syntax means pmGroup or sid is not considered.

A "hyphen" **"-"** character, *or empty*, means a column is not considered.

#### Two wildcard rows must start the limits file: one row for pmGroups, and one for sid

The system will not start without the first two rows in the example.

This verifies the operator has set a sensible default for any one security 
or any one pmGroup.  This alone would have avoided Knight Trading.

The sid wildcard row takes precedence, and *this limit must be lower than the pmGroup wildcard row.*

*Additionally, these two wildcard rows **<u>must</u>** be the first two rows in the file.*

#### Underscores in limit amounts

To make it easy to edit and review the limits files in an emergency,
instead of counting zeros, you may use the Java underscore 
syntax for numbers.  This allows you to insert an underscore
at any position in the number, anywhere, and it is ignored by the system.

This underscore allows for a convenient separator which 
makes it easy to see the size of the number in the file.

*Commas must not be used*, as the format of the file itself is CSV.

For example:  **10_000_000** may be used as ten million.

Details on this format:

https://docs.oracle.com/javase/7/docs/technotes/guides/language/underscores-literals.html

### Example

The position limits file is in the "risklimts" folder, named like:
```text
position_limits_20230525.csv
```

The first row states that the total long or short quantity held in any pmGroup is *9999*.
This limit will take precedence unless a rule is provided for a security or pmGroup.

The second row states that the maximum quantity long or short is **333**.

The third row states that **every pmGroup** is limited to 1000 total held in position long, or 99 short
for *sid=33*.  The star means apply this rule to every trading pmGroup, for sid=33.

The fourth row states that for pmGroup, **pmGroup=CMR**, **sid=33**, a specific limit, not the wildcard, is applied
for the pmGroup, *higher than the default*; **2000** for qtyLong and **777** for qtyShort.

The last row provides a limit for the pmGroup (**pmGroup=CMR**), stating that 
any security (wildcard) can be 1500 qtyLong and short.

### position_limits_20230525.csv

| pmGroup | sid  | qtyLong  | qtyShort | exposureLong | exposureShort |
|---------|------|----------|----------|--------------|---------------|
| *       | -    | *9999*   | *9999*   |              |               |
| -       | *    | **333**  | **333**  |              |               |
| *       | *33* | 1000     | 99       | 9_888_777    | 8_777_333     |
| **CMR** | 33   | **2000** | **777**  | 100_000_000  | 10_000_000    |
| CMR     | *    | 1500     | 1500     | 100_000_000  | 10_000_000    |


## Order of rules (important to understand)

There are three tiers of rules.  Once a match
for risk checks is found at any tier, the risk checks
are complete, and *more tiers are not checked.*
The rationale is that a limit was put in place,
and this limit was not breached. Otherwise, all
three tiers would need to be checked.  This would cause
more latency, as well as be potentially complicated.

The system looks for matches for position limits
using the pmGroup and sid rows, in the following 
tiered order of limit checks.

### Three tiers of position checks (first found is used)

1. **Tier1**: *Both specific match: both sid and pmGroup matched*:  The most specific rule is the first one applied, if a rule exists for both pmGroup and sid.
   - If a specific rule is found, then it is enforced, and the wildcard rules **ARE NOT** applied.
   - This allows you to specify some general rules, but rely on specific rules per name, 
     - *without a complex interaction of rules.*
3. **Tier2** *One Specific, One Wildcard:  Sid and pmGroup rules*:  Both rules applying for one pmGroup and sid are applied, if no specific check was found
   1. *specific sid third (wildcard pmGroup)*:  If no specific rule was found, the wildcard rule for an pmGroup, with a specific security, is applied third
   1. *specific pmGroup forth (wildcard sid)*:  If no specific rule was found, the wildcard pmGroup rule for all sids, and one pmGroup applied next
4. **Tier3** *One Wildcard, one Blank:  Catch-all limits applied*:  If no rule has yet matched, the match everything pmGroup and sid rules are checked
   5. **All sids row:** one row is required, the second row in the example; the 333 qtyLong wildcard sid, blank pmGroup row.
        This is applied.
   5. **All pmGroups row**:  The first example row, with a wildcard pmGroup and blank or hypen sid row, is applied.

#### Syntax for wildcards and hyphens

Two wildcards is invalid, and two hyphens also invalid.

The system will not accept double wildcard or double dashes.

Therefore, the operator can note that at a minimum, the file must be provided
with one row for each file, with a wildcard for **both pmGroup and sid.**

### Risk limit file is REQUIRED

The system will not start without any of the three risk files in place.

### Reloading

#### Overlay file
Similar to the other files, **overlays** may be added to amend a row.

These overlay files use a syntax like:

      position_limits_20230525_1.csv

The contents of these files are appended to the existing file,
but do not replace any of the other values.

#### Replacing limits file

If the same named file is provided, the limits are **replaced.**
The limits from the old file are eliminated, and the contents
of the new file are used instead.

#### Reloading every one minute

The entire file will be reloaded if a newer file is provided, and take effect immediately upon loading,
every minute.

#### 99 reloads

There may be up to 99 reload files, more than this are ignored.

### Restrictions

To restrict trading in a security, 
set the qtyLong and qtyShort to zero 
in the trading limits file.

If there are restrictions as to percentage ownership,
similarly, publish these into this limits file,
as position limits.  

This way the risk limits checks can be used
for both restrictions and risk limits checking.

## Trade limits file format

The trading limits file is in the "risklimits" folder, named:
```text
trade_limits_20230525.csv
```

The trading limits file follows the same format of hyphens and wildcards, and 
the same loading order as the position file.

An additional trading concern is the "timeUnit."

The trading file allows multiple rows for a particular entry, with a different value
for timeUnit.  For example below we specify a per **SECOND**, 
per *5* **MINUTE** risk limit check
for each of the pmGroup and sid hyphen rows; followed by
the same rows but for a **DAY** limit.

### Example file

| pmGroup    | sid | timeUnit   | numTimeUnits | qtyLong  | qtyShort  | exposureLong | exposureShort |
|------------|-----|------------|--------------|----------|-----------|--------------|---------------|
| *          | -   | **SECOND** | **1**        | 50_000   | 50_000    |              |               |
| *          | -   | **MINUTE** | **5**        | 100_000  | 100_000   | 5_000_000    | 2_000_000     |
| *          | -   | **DAY**    | **1**        | 9_000_000 | 8_000_000 | 10_000_000   | 10_000_000    |
| -          | *   | MINUTE     | 5            | 10_000   | 10_000    | 1_000_000    | 1_000_000     |
| -          | *   | DAY        | 1            | 10_000   | 10_000    | 1_000_000    | 1_000_000     |
| pmGroup | CMR | *   | DAY        | 1            | 9876     |           |              |               |
| *          | 33  | DAY        | 1            | 8_888    | 8_888          |              |               |
| pmGroup| CMR | 1   | DAY        | 1            | 777      | 777       |              |               |
| pmGroup| CMR | 34  | DAY        | 1            | **0**    | **0**     |              |               |

### NumTime Units supported

#### 1 and 5 for Seconds and Minutes

Only certain combinations of numTimeUnits and timeUnit are supported, to achieve low latench:

1. 1 second
2. 5 seconds
3. 1 minute
4. 5 minutes
5. 1 hour
6. 1 day

Other times are easily added, but these are the most common.  The system uses a ring buffer
for each of above, to calculate each time period.  More can be allocared as necessary.

See `TradeLevelManager` for details.

---------------------------------------------

TODO:  This is work in progress..

## Exposure calculations and Trading P&L calculation

### Exposure calculation: trades


The price is returned on every trade.  Any time a price is received for any
trade by the ems-service, the current price of this security is updated for 
purposes of exposure checks.  In this way the EMS uses the trades it is seeing
as the most current price information for exposure checks.

### Exposure calculation:  trades into new positions

The exposure calculation requires a price.

When a trade is made into a new position, without
any existing price information, *only the qtyLong 
and qtyShort checks are used for trading risk checks.*

If prices are desired for securities which will 
be traded, a position of zero
can be loaded in the positions file.

### Exposure calculation:  positions

For existing positions without trades, the 
start of day positions file prices per position are used
as prices for the securities until a trade is made.

These same prices are used to bootstrap the trading
exposure limits until a price is known for each
of the securities.



## More information

### Future enhancements:  Risk limits UX

A database will store the risk information, and a Java client will be provided
with a database to extract, which pushes the files to a /tmp folder, or to an S3 bucket.

As well, a web UX will be provided to amend the risk levels, and audit changes to risk levels.

A risk limits checker will be provided to verify the new risk limits would not have been
breached by any risk limit updates, given a set of log files.