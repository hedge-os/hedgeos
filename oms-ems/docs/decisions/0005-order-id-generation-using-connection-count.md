# Decision:  Use connection counting for Order ID generation

### Problem

1.  The ems-client needs to generate orderIds on the client side, for latency.
2.  These orderIds need to be unique for the ems-server
3. The client may reconnect, and then can't collide with order ids generated on previous connections 
   1. (can't start over or repeat.)
4.  If the ems-service restarts, it needs to remember the connection count, per day.

---------------------
## OrderId assignment based on connectionId

This clientOrderId stuffing leaves us with 218 trillion orderIds total, PER EMS SERVER.
it is PER EMS SERVER, because the emsServerId is part of the ClOrdId stuffing, making each
EMS Server an independent unit; **the ClOrd IDs for two ems servers will always be different
based on the emsServerId being part of the ClOrdID.**

The ems-servers will call a central database to get their assigned **emsServerId**.

This can be done later, for now; it is manually assigned, and **must be unique**.

If the emsServerId is **not** assigned, the ems-service will fail to start.
If there is a collision, *and two ems-service are connected to the same venue*, then and only then, will you fail to trade.
The failure will occur when a ClOrdId is re-used, at the venue; and it is not globally unique.

### One trillion orderIds per connection, per day .

Each connection will be assigned *1 trillion* orderIds
per day, as the top offset used to generate orderId.
e.g. connectionId=2 will start with orderId=2 trillion.

On each reconnect to the EMS, to avoid colliding with
prior connections, the offset is further increased 
by 100 billion.  (We also prevent more than 100 billion
orderIds per connection; and once there, the system
can reconnect itself, to get another 100 billion.)

This way, there is no collision when the connection
starts to generate orderIds.  

OrderID generation
is on the **client side** via the EMS client;
this facilitates no round trip anywhere to get the
next orderId in sequence; merely an AtomicLong
counted upwards by 1 AtomicLong.incrementAndGet().

### 100 billion orderIds per RECONNECT

It is likely a client has moved machines, if there
is a reconnect.  In this type of emergency, we don't
want the client to be worrying about what the last orderId
which was used.  Yet, we need to make the generation of
the ids live on the client side for speed and simplicity.

To allow for this, we use a large offset of 100 billion orderIds
for each time the client connects to the server.

#### Simulated reconnect on 100 billionth orderId

The client can keep counting above 100 billion,
for orderIds.  When the server detects the 100 billionth
order, it will increase the reconnect count for this connection.

This way, if the client disconnects and reconnects, the
offset for orderIds will be increased by another hundred billion,
preventing a collision.

This way, we allow for three trillion orders per day, 
per connection to each EMS server.

#### Process for reconnect

Each time a connection is made for a login,
the ems-service counts how many reconnects have
been made, and returns the next counter value.  

The number of reconnects determines an offset
into the orderId generation; 100 billion
will be assigned for each "reconnect".

#### More than 10 reconnects
More than 10 reconnects use an offset of 
62 trillion * connectionId.

This is because there are up to 62 connections to an EMS,
and the orderId needs to jump above this 62 trillion space
used by the connections.  on the tenth reconnect,
a trillion is added to the offset; this
will start at 62 trillion.  And ten more reconnects
each allow for 100 billion Ids.

If ten more reconnects occur, **62*2** = 124 trillion is added
as an offset, and similarly **62*3** = 186 trillion is added
as an offset, plus the 100 billion for the single digit
remainder amount of reconnects.

As we have 8 digits in the ClOrderId, base62 encoded,
we can support 218 trillion connections:
```shell
62^8 = 218_340_105_584_896L
```

Practically this means connections can reconnect to the EMS
up to 30 times *per day* before this orderId counter is exceeded.

30 reconnects to a system is a large number of restarts 
which should never practically occur.  

#### Prevent 30 reconnects a day
To make sure 30 reconnects are not exhausted,
the 10th and following reconnects will slow down
by 10 seconds, and log an error on the server and client.

## Clean up the reconnect count

The system will not be able to run forever, without
resetting the reconnectCount on a connection.

Via reconnect attempts, and using more than 100 billion
orderIds, the reconnect value will increase, and
is capabable of supporting hundreds of billions of 
orders.  These need only be uniqueIds, each day.

Therefore:
1.  If we see a connection with reconnect count > 10 (more than a Trillion orders.)
2.  AND, the connection has been open for more than 48 hours
3.  This means - it is safe to reset the reconnectCount to zero.

The next value will be back to orderId = 0, plus the trillions offset
for the connection.  This is ok, because, a trillion orders may
only be submitted every 24 hours.

When there is time, we could handle the pathological case
of more than a trillion orders in 24 hours.

## Diagram: connection counting
![orderIdGeneration.drawio.png](..%2ForderIdGeneration%2ForderIdGeneration.drawio.png)