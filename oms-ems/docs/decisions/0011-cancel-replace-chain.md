

### Limits for Cancel Replace

Limits are always checked on the
order quantity of the cancel replace.
This is because limits use a rolling
window of time, which may have elapsed.
Therefore, we need the entire quantity
within the timeframe.

In theory, we could try 
to return the original order limit,
if it were inside the timeframe.

If the user wishes to avoid this,
the limits may be set higher;
or order w/ cancel can be used.

### Locates for Cancel Replace

Locates are added to a chain 
of orders.  If you
have a cancel replace
then the max of all the cancel
replace is used as the 'total'
number of locates you need.

The system will reserve
locates from the existing
amount reserved for the order
chain up to the max 
of the cancel replace chain.

Meaning, if you have locates,
and then cancel replace to
increase the order quantity,
the system will take only 
the additional amount of 
locates needed.
