# Catastrophic Recovery

## Problem statement

In the world of physical machines; about once
every 200 'machine years' a machine will die,
is an expected and observed statistic.  
With the advent of the cloud, a machine
in theory dies less often, but the cloud
itself will take outages, up to once a year.
For example, the famous US-East outage
at Amazon in 2021.

[Amazon US-East outage](https://www.cnbc.com/2021/12/10/aws-explains-outage-and-will-make-it-easier-to-track-future-ones.html)

In summary, you now need to start on a new physical machine,or
in a new region of your cloud provider.  Once restarted
you need to instruct the FIX counterparty (venue), to 
resend messages.

#### Details

When the server which is lost is the server where you are 
running ems-service, you have a serious problem.  

While a rare occurrence, this is one of the biggest 
risks to your trading business.  Without
receiving the fills and knowing the status
of the orders in the market, you are "flying blind"
from a risk perspective; you will not know
your positions in anything you trade, 
and during this time, the market could move 
against you.  Algorithms running
will be halted, for example VWAP and TWAP
orders, which may need to complete to
provide proper hedges against large block 
trades already completed, leaving you 
with risk in your portfolio, and financial losses.

When this situation arises, you need
to start the system on a new machine.
This machine *may* (very likely), will
lack the connection details.

Importantly, the sequence numbers used to connect to the 
venue will be missing. 
While your system was down, you likely
missed Execution Reports; fills on your orders, which
are queued up at the FIX venue.

### Messaging challenges

The messaging problem is two-fold; 

1.  **Sequence number mismatch:**  You need to
reconnect using a sequence number expected by the Venue.
2. **Message recovery**:  You must instruct
 the venue to replay messages to recover any missed
messages.

## Alternatives

#### Detect and update sequence number

There are two ways you can reconnect to the FIX Venue
and sequence numbers will agree.

### Alternative: Use ResetSeqNumFlag:  

#### **Never use ResetSeqNum in production**

One way to agree with the FIX Venue is to send 
a ResetSeqNumFlag (tag 141) = Y, 
which QuickFix supports and is added to the ems-service
to pass to QuickFix.  this is for Development only.  

You must never use ResetSeqNumFlag do this in production.
The reason this can never be done in production is
that this instructs the FIX Venue to restart, 
as if the messages which were missed are not important.

The FIX Venue merely resets to the same start = 1
sequence number as your FIX engine.  Once you have done
this reset, there is no way to replay the missing messages
while the system was down.

### Alternative:  Detect sequence number from "Logout" message

This is the preferred and expected alternative
in the event of a catastrophe.  This must be
tested as part of certification with every
FIX Venue counterparty.

First ems-service determines the "expected" sequence number.
The exchange informs us of this, on the **error Text** (tag 58)
of a **"Logout"** message.  

The logout message in response to a "Login" message from the
Venue.  For example:

``` Expecting sequence number 14157 received 1```

The venue needs to communicate the mismatch,
which the Logout message is doing.  The venue
also needs to express the expected message
number.  Unfortunately, there was a major
miss in the design of the FIX protocol, which
is for this exact case.  Both of these facts
are communicated to the FIX client in 
an ambiguous manner.  The "Logout" message is used,
instead of a specific message regarding, missing
messages, was the first mistake.  The
second mistake in the FIX design was, the tag 58 "Text"
field is the only wan the expected sequence
number is communicated.

#### How FIX could be this wildly insufficient

It is likely the FIX spec developers expected
a catastrophic loss of a machine to be 'rare'; however, happening once a year
is not rare, and in some years, may happen 1% of the
days you are in business (2x a year.)  

Also, the FIX protocol maintainers may have expected when
this does occur, phone calls would be made between
the operators and the venues.  This was a naive
approach and assumption.  The FIX protocol
sits between a session and application level protocol;
and at the application layer, is unspecific as 
far as what message order should be used, and
what fields should be sent in each message, leaving
this open to interpretation by the FIX venues, the NYSE
the CME, etc.  Further, there is no way to know 
what NYSE expects in a certification, leaving this
as private, somewhat protected knowledge.  The only
way to certify with NYSE, is to own a trading business
and certify with NYSE.  This is one of the 
problems the ems-service addresses.  Each buy-side
firm repeatedly performs this same certification,
and learning about these bespoke rules.

A new effort exists called **FIX Orchestrator**,
which appears to both specify the exact exchange
of messages and also perform code generation
to accomplish this task.  The ems-service
is essentially a handwritten, version of what
this FIX Orchestra code generation would seek to 
generate.  It is debatable if it is more effort
to write a code generation machine to generate
the ems-service, or to implement the ems-service itself.
We here take the belief the latter is more effective.
In doing so, we must be opinionated about 
the correct exchange of messages.  The 
decisions here in the recovery design represent the buy-side
opinions in the optimal handling of a recovery.

Also, a new 'recommendation' for how this 
should occur has evolved, called NextExpectedSeqNum:

https://www.fixtrading.org/standards/fix-session-layer-online/#using-nextexpectedmsgseqnum789-to-synchronize-a-fix-session

This alternative to use tag 789 on the Login is available in ems-service.
This is enabled with a switch, Tag789RecoveryStyle=true
in the FIX Config for each Venue where it is available.

If this alternative is provided by the Venue, 
then the problem is solved, by setting this
one flag on the FIX Venue config in ems-service.
Part of the decision is to provide this flag, and test
it with the **mock-exchange** system providing this same
facility.

### Options for recovering messages

When ems-service is restarted after a catastrophe
it has no knowledge of orders placed, fills and
cancels received; it has no state.  The ems-service
system needs to rebuild this state.  The
state is determined by the sequence numbers,
and the necessary truth that the ems-service
has seen each message associated with each sequence
number.

If the system is restarted after a catastrophe,
there are two places the state can be "rebuilt"
from.

#### Your own storage

One is from your own storage, e.g.
rewinding a Kafka topic you have been writing.

#### Ask the venue for entire history

The second alternative to recover the messages,
is to request a replay from the FIX Venue.

Two major alternatives exist to ask the venue
to replay messages.  A second
decision, is which messages to replay.
You can either read the history you know
locally, and then request the rest from the venue
or you can request the entire history from
the venue.

#### Hybrid approach

The best alternative is to try to recover
the messages from Kafka, by doing a rewind on 
a specific topic assigned to this FIX Venue.
This provides for a rapid recovery of up 
to millions and tens of millions of messages.
Once the Kafka topic has been read to the end,
and messages recovered, the remainder
of the messages should be recovered from the 
FIX Venue.  If Kafka fails, this is treated
as receiving zero messages from Kafka,
and the entirety of message history is replayed
from the venue.  It is important to be able 
to recover without Kafka, because
a kafka outage could coincide with a trading outage.

The Kafka cluster must always be a separate cluster.
Kafka is free and open source, and it is trivial
to stand up a cluster to handle low volume.  
An existing cluster could be used in a small team;
however, a Kafka cluster in Docker is strongly preferred
and recommended.

### Alternative:  (fail-safe) Phone call to the venue and manually set the sequence numbers

*Set the expected sequence number manually.*

The auto-detection of the expected sequence number
should be tested, but in theory could fail.  For 
example, if the logout message failed to state
the expected sequence number, due to a change
at the FIX Venue.  This should never happen,
and 'certification' with the FIX Venue should 
always verify that recovery works, by reading 
the Logout "Text" (tag 58) field on the Logout
message (type=5 Logout.)

In the event of failure, a fail-safe is provided.

The operator can phone the venue support, and
be read the expected sender sequence number,
(or have it emailed.)  Once it is emailed,
the ems-service operator on the buy-side
can then manually force the ems-service
to use this number, by dropping a file in a folder.

In the future, a UX will likely be provided as a priority, 
to drop this file, in a future decision.  This
will be an internal web-page served by ems-service
which writes the file to the correct location.

#### GapFill.txt *file* details

If a file is dropped named **GapFill.txt**,
in the "connection" folder.

The connection folder for development is:

```logs/fix/us-stock-mock-ems-service-connection```

This is where QuickfixJ maintains one
FIX Venue, the us-stock-mock-ems-exchange.

Therefore, for this FIX connection, to request 
a GapFill, a file named GapFill.txt should be placed here:

#### File:  GapFill.txt
```text
from=1
to=14156
```

A file named GapFill.txt in 
this connection folder will result in 
the ems-service sending a matching GapFill
message to the FIX Venue for this range of messages.

#### Admin web page

A web-page on ems-service allows you to send GapFill messages.
ems-service operators should request a GapFill for a test trade
to develop comfort with the process.  As the result
will be a replay with PosDup=Y, this is a safe operation
to practice in production on a test trade.

The page is linked from a central index admin page in ems-service.

http://localhost:8080/index.html

This page should only be used in extreme emergencies.  EMS 
operators should review it during certification and post
launching their first ems-service; but it should not 
be used as the automated-recovery of Kafka and the Venue
is tested extensively.  

In this way, the web page should be considered, "break glass"
capabilities, after normal recovery has been attempted,
and even then, used with extreme caution.

## Sub-decision - handling GapFill replays

1. Don't love this:  Put the sequence numbers on the messages
2. Flag for "CumQty available", defaults to true.
   3. If CumQty is available; then, it can only go up.
 
### Alternatives for CumQty  
1. Always get all messages; prior to allowing new trading
   2. This makes sure, we have everything in order.
      3. To do this, we will go to Kafka first; this is simple and easy, can play everything fast
      4. After playing Kafka, GapFill to the exchange.
      5. Make sure works with Kafka down as well.
1. ignore prior messages, using SequenceNumber, or if CumQty goes down

### Flag to "CancelOnDisconnect"

#### Cancel on disconnect - *not* recommended, but available as a startup flag.

Cancel on disconnect is a philosophical decision, without
any perfect answer, for a highly undesirable situation.  
Disconnecting should be a (very) rare occurrence.

Based on pros/cons below, the decision is to avoid cancel on disconnect, as a default.

It is better to leave the algo orders running, and to recover where
you left off (quickly), which is an important ems-service capability.
ems-service will recovery quickly from any venue (after recovering
from Kafka, in a catastrophe, if needed.)

### Pros and cons of CancelOnDisconnect

#### Pros

1.  **Fewer lost messages**:  If you don't cancel, you could
    have tens or hundreds of thousands of fills waiting for you
    when you reconnect.  If anything goes wrong reconnecting,
    this could become an operational nightmare to determine
    your true position in the market.
 1. Lack of visibility is mitigated by
    the ability to likely see your position via interfaces at the
    banks you are connected to.
 2. **ems-service is built to recover**  The ems-service is built
    and extensively tested to recover when in this scenario.  The 'GapFill'
    once reconnected acts as an ultimate backstop for an operator
    to replay messages.  The ems-service will request missing messages
    when reconnection occurs, on its own; and then these alternatives
    are available.


#### Cons

1. **Day algo orders:**  Algo orders like TWAP and VWAP should continue; it will
 be more difficult to restart these from where they left off,
 and could potentially be massive operational load to do so.
2. **Risk position hurt:**  Your risk position may be kept
 where you need or want it by running algos; and if you cancel,
 then until you are back up, you are out of position.




