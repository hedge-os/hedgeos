## Kafka design for FIX messaging storage, broadcast, and recovery

#### Alternatives considered
1.  Topic per Venue, per Day, per ems-service
1.  Certainly per ems-service per Venue.
1.  Both, per ems-service system startup per venue, and, per trade date

#### Selected alternative

1.  Topic per ems-service, per-venue, per restart.
    1. This makes Kafka a distributed redundant log file per ems-service.
    2. With the kafka special sauce of rewind and replay as an API.

On startup, the system will create a new topic for each ems-service,
for each venue.  This will have the date of the startup.  The
topics will be queried on startup, and if there is a topic for
the current business date, it will be read, and all messages loaded.

#### Date roll for Kafka Topics

The kafka topic will contain a **LOCAL** date.

```prod_ems_service_01_us_morgan_stanely_razor_20230629```

This is the ems_service numbered 01, 
for the FIX Venue US Morgan Stanley Razor, current trade date.

### Startup Sequence for ems-service
1.  For each Venue
1.  The system tries to Logon to the Venue with Sequence = 1
   1.  If the Logon succeeds, then it is 'normal startup'
       1. **Normal Startup**
           1. Make a Kafka Topic for the current **LOCAL** *business date*
           2. Local business dates used for simplicity, as these topics are only ever needed locally.
           3. This is a departure from the "everything UTC plan", but seems reasonable.
   1.  Logon fails with sequence number mismatch; **Recovery Startup**
       1. **Recovery Startup**
           1. Look for a Kafka topic with this environment, ems-service number, venue (for today)
               1. Then look for yesterday in the event this is a multi-day session
           1. Read this entire Kafka queue as binary messages into a java concurrent queue
           1. While this is happening, start a thread to push the queue onto a Callback which
              should be provided as an implementation of an interface.

This key to the recovery is captured in `EmsReportEnvelope` in the api.

This will be built by each FixAdapter and returned to EmsMultiplexer
in the callback.

EmsMultiplexer will push the EmsReport to the connections,
and async push the report to a Kafka topic for recovery.

### Failure to set the ems-server id on a recovery.

Note:  If we Logon, and we need to recover, but we can't find a topic for our ems-service,
then this must mean the operator has failed to set the ems-service id, on the new machine,
after a catastrophe where a machine was lost.  

At this point, the system will not start; and an error will instruct the admin to set it 
correctly, or to provide an override flag, which will state "if you know exactly what you are doing,
then override this." (which should never be used.)

#### Note on async, non-guaranteed Kafka
Every venue supports FIX recovery; this is the entire point of FIX.
Therefore, Kafka messages can be sent async.
We use the Kafka system as an optimization for recovery, and
to broadcast to risk monitoring systems.  We can reasonably
assume the messages will arrive at Kafka; there will be a dedicated
cluster to FIX trading and this exact purpose, with redundant brokers.

The trade-off is one between sync and async write, and speed to Kafka.
As Kafka is a secondary channel (not the gRPC ems-service connections.)
and also because the messages can be recovered from the venue,
we decide to make the Kafka delivery default to async; **however**, this option
of Kafka write mode, can be *configured* to be a sync write to Kafka for more pessimistic deployments.

In the event of Kafka failure, messages are queued to disk in `/tmp`
or another configurable disk location.  Messages are recoverable from
the venue, and this is the primary source.  Kafka is a perfect
fast secondary source, but need not pay performance penalties to write,
when there is a robust backup, the venue itself.  With sub-100usec (micros)
as an SLA, there is not much wiggle room.

### Problem design statement
1. Design an interface for generic binary message storage where each message has a sequence number.
1. As well, each message should be stored by a
    1. LocalDate object,
    1. a number called 'ems-service id',
    1. an environment enum where the environment can be PROD,UAT,DEV,WORKSTATION
    1. A String 'fixVenueName'
    1. A sequence number
1. The interface should allow for retrieval of the messages by passing
   another interface which is a callback which each message will be pushed onto,
   along with the LocalDate, 'ems-service id', environment, and fixVenueName.
1. The query parameters for the interface should be made a Java Bean called EmsMessage

