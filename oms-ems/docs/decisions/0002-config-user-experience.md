# EMS fix connection management - operator experience

Two ***environment variables***
are used by the EMS (ems-server) to determine which FIX Connections
to spin up.

1. ```EMS_CONFIG_FOLDER```: Config folder referenced on command line
2. ```EMS_CONFIG_FILE```: Config file in this folder with a list of connections to run. 
   1.  The config for these is located in EMS_CONFIG_FOLDER.

These folders will be set one and only one way.
This means there is never any doubt about how the system
was configured.  These environment variables are available
to review in /proc/*pid*/environ.

This can be reviewed with for example:
```shell
xargs -n 1 -0 < /proc/4793/environ
```

Using ```/proc/pid/environ``` makes it trivial to locate easily.

## Correct Usage + Benefits

### Env variables set on hosts in ```.bashrc```

The user in production, should have these variables set,
as part of the ```.bashrc```.  This way, each machine
has a defined setting, which is set on the machine.
This simplifies deployment.  After setting these variables in .bashrc,
You push the software to a machine, and you start it.

The system finds these settings on the host where
you are starting EMS, and away you go.
New deployments are to push the software, or to 
push the entire folder of GIT config out to the system.

### Benefits / comparison
The approach of choosing the file with an environment variable
is less error-prone than pushing out a subset of files.
With a subset of config files, you need to 
push only a certain set.  Then every time a fix connection 
is added, there is a second "deploy" step, to copy it 
to only the one machine you want it on, which can be painful.
If the file is not copied, you could be left with stale
fix connection config.

### GIT as the preferred deployment method

The deployment process can be simply doing a ```git pull```
to of the config folder directly onto the host **(from a RELEASE branch!)**

The added benefit of doing a ```git pull``` is you can trivially
do a ```git status``` to see what config is on each host.

**Furthermore**, if you change the config, you can 
use ```git status``` and ```git diff``` to push any changes
made in production config, back to your GIT repo.

### Folder in GIT
This means two simple variables control the config
First is a folder which can 
be shared in GIT across all EMS installations.
This is the config folder, where all connections
can be committed.  
This is the ```EMS_CONFIG_FOLDER```
which is a critical attribute.  The 
shared git folder should be pushed to every machine.

### Config files in GIT
Second is one file within the folder.
This is called ```EMS_CONFIG_FILE```.

### Everything checked into GIT in one folder

The entire EMS config can be in one flat folder, trivially,
or two folders like
1. ```my-hedge-fund-fix-config/config/connections```
2. ```my-hedge-fund-fix-config/config/environments```

Variable wise, these are two simple and easy to remember names.

These must be set on a machine (preferable), 
or in a shell script which starts the ems-server.

**If both variables are not found, the ems-server will not start.**

This has the added benefit where we know these variables
are properly set, and it is intentional.

### Problems solved

1. Committing an external folder of config to git
   1. We want one external folder, outside the codebase,
        to be a git project where the config is checked in by 
        a hedge fund deploying the EMS. ```EMS_CONFIG_FOLDER```
   2. Inside this folder, will be the set of connections
        for multiple EMS installations, and we need to run only one
        of these installations.  ```EMS_CONFIG_FILE```

### Alternatives considered

The ```EMS_CONFIG_FILE``` could be on the command line,
or as a Java -D parameter.  We could also allow this as an override.
It is possible in the future we allow this as an override.
It is however preferable to only have one way of configuring
the system.

## Notes on design

Tremendous care must be taken as EMS configuration
is one of the most risky set of configuration settings
which are deployed to production.

For this reason, each FIX Gateway has a 
separate configuration file which is pushed to git.  
This way changes to each fix connection file
are separately tracked and audited, and can be 
code reviewed and pushed out separately.

This is critical configuration, and *must*
be stored in GIT.  The configuation is stored
externally and deployed separately from the 
EMS server system code itself.

### Class diagram

Class diagram of key configuration class in ems-service

![class diagram of config](../config/ems-service-packages.drawio.png)

#### Data (Java beans)

1. **EmsConfigVariables**:  A bean with the two variables, used to allow for flexible sourcing
    of the two critical settings ```EMS_CONFIG_FOLDER``` and ```EMS_CONFIG_FILE```.
   (particulary useful for testing Spring from Unit and integration tests.)
2. **EmsConfigFiles**:  Used to hold java.io.File for each of the environment variables 
    for ```EMS_CONFIG_FOLDER``` and ```EMS_CONFIG_FILE```
2.  **ActiveFixConnections**:  This is the config file listed in ```EMS_CONFIG_FILE``` loaded into memory
    This contains a list of fixGatewayConfigFiles files to load, one for 
    each fix connection this EMS installation will open.
3.  **FixAdapterConfig**:  The configuration for one FixAdapter (one fix connection.)
    from the list of fixGatewayConfigFiles in ActiveFixConnections
4.  **CompleteEmsConfig**:  This is merely single bean storing the three config settings from above
    , where everything is loaded as type safe Java beans.

#### Config loader classes

1. **EmsServiceConfiguration**:  **ems-service** class which reads the System environment, finds ```EMS_CONFIG_FOLDER and FILE```
    and creates an EmsConfigVariables bean, sending it to the **ems-config** system.
1. **EmsConfiguration**:  Loads these variables form the System environment variables.
2. **EmsConfigLoader**:  The entry point for loading EMS config.
3. **EmsConfigFileLocationUtil**:  Loads the Linux system environment variables ```EMS_CONFIG_FOLDER``` and ```EMS_CONFIG_FILE```,
        then finds the folder, and the file in the folder.
3. **FixAdapterConfigLoader**:  Loads the collection of FixAdapterConfig files, one at a time
    from the list of ActiveFixConnections. 

Running the EmsConfigLoader results in a CompleteEmsConfig bean
being returned with a List of FixAdapterConfig beans.

Once this config is returned to the ems-server, it 
then starts up these fix connections, connecting to counterparties
using the config loaded by this config loading process.