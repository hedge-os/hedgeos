# Decision:  Use basic environment and password as a sanity check

## Problem Statement

1.  If a systematic trader connects to the wrong environment, e.g. running
    a test system but connected to prod; a catastrophe can occur, 
    perhaps better referred to as death of a firm (see Knight trading incident.)
1.  We therefore need basic protection for a trader not connecting 
    a test system (or an engineer), to the production EMS for trading.
2.  We need something independent and trivial to diagnose, as an inability
    to trade, is a different kind of catastrophe.  Inability to trade
    is marginally acceptable, because brokers can be phoned to place trades
    and other manual backups to exit positions or take new positions; however,
    still needs to be avoided.

---------------------

### Simple validation with 'ems environment'

The api contains **environment.proto**, with an enum which defines the environment.
These are **WORKSTATION, DEVEL, QA, UAT and PRODUCTION.**

#### ems-client usage

The ems-client (EmsClient.java) uses the Environment Manager (EmsEnvironmentManager)
to load a critical environment variable on the client:

e.g.:
```shell
EMS_ENVIRONMENT=DEVEL
```

If you clone the code and run, it defaults to WORKSTATION, by detecting the
source tree as a relative path.  If you are not running from a clone
of the source tree, this environment variable must be set on the Unix account
where you are running the client.

#### ems-service usage
The ems-service, similarly, uses the shared class EmsEnvironmentManager
to load the same environment variable, and load the value of the 'environment'
on the server.

If the **EMS_ENVIRONMENT** is not set, the ems-service won't start, by design.
Again, on ems-service in a cloned source tree start of ems-service, 
the environment defaults to **WORKSTATION**
for ease of use when developing the system.

### Environments must match

If the environments don't match, the EMS Client will fail to connect,
with a specific failure returned to the client from **connect.proto**

```shell
ConnectionStatusReason:
  WRONG_EMS_ENVIRONMENT = 9;
```

The environment provides a reasonably strong protection against connecting
to the wrong EMS.  **Networking** in the cloud environment or otherwise,
is the **primary means** of protection against development connecting to 
production.  But given the financial risk of trading systems, 
the system requires this secondary token, as an additional sanity check.

### Secondary simple validation with 'password'

The EMS requires a field 'password' be set when connecting.
This password is a trivial string, which is the concatenation of:
1. the login
2. The EMS_ENVIRONMENT in all caps
3. The length of the two strings above appended together.

e.g. 
- login = 'chris'
- **password = 'chrisWORKSTATION15'**

(because chrisWORKSTATION is 15 characters long.)

This provides a second sanity check, where, users should hand-code
this password value, and not create it using code; by loading it from a config file.

This way, clients can be certain they have loaded the correct client configuration,
and are connected to the correct EMS service.

### Conclusion

The combination of these two trivial checks, act as additional sanity
checking, to prevent developers and traders from connecting to the wrong EMS.
In a systematic trading environment, Knight trading serves as an example
of what can go wrong.  We need to be careful, yet not prevent straightforward
connection to the EMS for actual trading purposes.  These two checks
serve as enough basic protection, where if both checks failed,
**(in addition to networking)** it is a case
of clear negligence by the operators.

The checks also importantly, force the engineering team
to be highly deliberate when connecting to 
the production EMS systems, as these developers may have networking access
to every machine.

## Alternatives and future iterations


### Kerberos

Kerberos could be integrated with the system.  This is expensive to 
integrate when installing the EMS system, and fairly complicated.
Failures can take hours to diagnose, without obvious error messages
when using Kerberos.  The expense and the difficulty to operate 
a Kerberos security system, are two reasons this is not the initial choice
for security for the EMS connections, and instead trivial security checks
are preferred.

### JWT

JWT could be integrated, or many other security mechanisms.
These are not a substitute for networking, and ultimately, 
provide a check of token exchange.  The EMS system
fundamentally is a protected exchange on a private network.

Therefore, basic authentication is as powerful, at a fraction 
of the complexity of JWT or alternatives.

### Future iterations

Either JWT or Kerberos could be added, in specific deployment scenarios if 
desired or necessary.  The concern of EMS security is basic sanity checking;
not real security.  Once the trades reach the exchange, they are public
and on the tape forever.
