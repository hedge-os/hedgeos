## Risk Limits Loading

Files with names like:

`position_limits_20230525.csv`

are loaded, ignoring the file times and
checking for the date string in the name yyyyMMdd.

The other two files (for risk purposes) are:

`trading_limits_20230525.csv`
and
`positions_20230525.csv`

These files are loaded and set on the
maps in the `RiskLimitsManager`

The date in the maps **is always** 
the date yyyyMMdd from the filename
itself.

A loop runs for the current trade date
and for the next trade date,
reloading any files which are found
if the timestamp is updated, or if 
an overlay file is found with a newer timestamp.

The preferred way of running ems-service
is to load these files every day,
prior to any trading; and to have 
these files pushed to the filesystem
before the trading date rolls to the next date
(which is currently at midnight in the 
timezone the ems-service is running.)

#### Diagram:  Limits and Position file loading

![Risk Limits Loading](./riskLimitsLoading.drawio.png) (./riskLimitsLoading.drawio.png)


### Startup
The ems-service will not start 
without current limits files and 
current position files.

#### Post startup
Once ems-service is started, 
it will use these limits and positions
for the current trading day.

#### Limits usage, post first day
Once the date rolls, ems-service
first checks to see if a limits file was
loaded for the next specific trade date.

#### Used once date rolls

This way, limits, positions, symbology
and locates files may be set for the next
trade date in the future, and these
will be used when the date rolls; *but not before
the date rolls.*

#### Fallback to prior days

ems-service will fall back to prior days limits if it can't
locate any limits loaded for the current
trade date.  Using a prior day file will incur 
a small latency penalty to walk balk to find a prior
day in the maps.

#### Positions usage, post first day
Positions will be updated after
the first day, keeping the current
positions known in memory
until or unless a flat file is loaded
for the next position date.

This is akin to the system being 
aware of all trading in a position for
an account.

### More recent file takes effect

If a file timestamp is touched,
with a specific date like:
`positions_20230525.csv`

(or an overlay is loaded.)

Then the positions will be instantly
set to these values.




