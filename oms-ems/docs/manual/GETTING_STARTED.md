## EMS:  Getting Started

### ems environment

The ems-service relies on an environment variable:

**EMS_ENVIRONMENT**

This should be set on the unix account to be one of the following:

1. DEVEL
1. QA
2. UAT
3. PRODUCTION

The client unix accounts must set this same environment variable,
and it must match with the ems-service system.  This is 
used to verify when a connection is made, that the ems-client
is connected to the correct environment, as a critical protection
to avoid a development ems-client from connecting to a production
ems-service.

### Configuration

The EMS server (ems-service) relies on two critical environment variables:
1. **EMS_CONFIG_FOLDER**
2. **EMS_CONFIG_FILE**

These should be set as environment variables in the container
or machine you wish to start the ems-service.

In here you configure the FIX Adapters, similar to the 
example files in the **ems-config-mock-files** folder.

#### Symbology
The ems requires a mapping of long--> string, for each
symbology you wish to trade.  This is loaded from CSV files.

Create a sub-folder of the config folder for symbology: 

**${EMS_CONFIG_FOLDER}/symbology**

In this folder an SCP job must push CSV files of long id
to symbol mappings, where the long represents the symbol.

#### Positions

The ems requires start of day positions for 
every trading day.  This is also loaded from CSV files.

Create a sub-folder in the config folder for positions: 

**${EMS_CONFIG_FOLDER}/positions**  

In this folder, an SCP job must push a file with a securityId,
the trading account, and the position in the trading account.

This file is checked every minute for new files, after
the ems-service is started, for days after the first trading date.
For the date the ems-service is started, *this positions file must exist.*
(otherwise, the ems-service will stop, to prevent trading.)

## Start up ems-service

ems-service is deployed as a shaded-jar (one jar file.)

Build the code, and this jar is then deployed in a container.

Use java to start this jar, and the main classpath in a shell script.

The main class is **EmsServerGrpcSpringBootApplication**.

One started, clients can connect on port **9090** by default.

## Connect a client

**EmsTestApp** is a sample application which may be used
to connect to the EMS and send test orders in development.

To connect to development, a username and matching password
must be sent in the connection request to ems-service.

The username must be unique for each ems-client connected,
or orders may be routed incorrectly, as the login is used
to connect fills from the market to the EMS clients.

See the decisions documents on password checking for
basic checking of the correct password.

The password must be a string combination like this:

1. login
2. EMS_ENVIRONMENT value
3. length of login+environment

e.g. login=test, env=DEVEL, password=**testDEVEL9**

This password acts an additional sanity check for connecting to 
an ems-service from an ems-client.  Networking should prevent
any development system from connecting to production; however,
this is an additional software check, because an EMS is a sensitive 
and critical trading system.

## Use the EmsClient to trade

TODO:  Details on how to use EMS Client.

The EmsTestApp has an example of connecting, attaching a callback
to listen for EmsReports from the ems-service (fills and cancels, etc.)

These must be done in sequence, of:
1. connect
2. subscribe with a callback
3. send orders

### Flatbuffers messages

The messaging schema for EMS messages is defined in protobuf schema
in the API module, **ems.proto** file.

Flatbuffers are used for reduced latency.  Convenience functions
exist in the EmsClient package for creating common messages
using Java flatbuffers.
