# Disaster Recovery handling

## EmsClient restart or machine failure

Any EmsClient can trivially fail, and restart
on the current machine or on a new machine.  
The ems-service which is connected, keeps 
a reconnectCount on the "connection", and 
every reconnect, this is incremented on the server side.
Further, the server maintains this in a file, per connection.
The connections are defined by the "login".
Logins must not be re-used and should be protected, and unique.
This is critical; as if there is a login collision between users
of the EMS, you will have all orders routed to the second connection
which uses the login.  

**TODO:**  In a future iteration, passwords are given per login
and not shared, and the system runs with passwords by default.
The storage of the passwords today is trivial encryption
into a file with a salt which does not change and base64 encoded.

This is then given to the ems client, and they use it to connect.

The orderIds for the client are increased by 1 billion each 
time a client reconnects, and this is persisted by the EMS server.
Therefore, there will never be an orderId collision, 
as a billion orders are not sent in one day.  If a client
is connected for more than 24 hours, the EMS client will reset the
orderID counter if it is over 100 billion.  Orders of more than
a billion will cause the 'reconnectCounter' to increase, 
which also acts as a counter of how many billions of orders
have been sent.  The ems-service has an @Scheduled cron job
using Spring scheduling, which at 6pm will reset the 
reconnectCount to 0 on the server, for any client
which has been connected for 24 hours.

A restart of EmsClient on a new machine is identical to a restart on the same
machine.  There is importantly one EmClient for each ems-service
which is connected to.

## ems-service

Each ems-service has a file or environment variable which
must be a unique counter of the number of ems-services which 
are run in a trading organization.  The overall system supports 62 ems-service
installations.

This is stored in a file generally in the folder where the ems-service
is startted, called **emsServer.number**

This value is loaded using these constants in the class ***EmsServerIdManager**
The system looks first for a file named **emsServer.number**, using the one number contained int he file
, then for an environment variable named **EMS_SERVER_ID**;

```java
// env variable, property, or emsServerId.number file must be set in working directory of EMS
public static final String EMS_SERVER_ID = "EMS_SERVER_ID";
public static final String PATH_TO_EMS_SERVER_ID_NUMBER_FILE = "./emsServerId.number";
public static final String PATH_TO_EMS_SERVER_ID = "PATH_TO_EMS_SERVER_ID";
```

It is critical this never be re-used across two EMS installations.
The value is sent to every counterparty, as the first charachter
of the FIX tag 11 ClOrdId field in FIX.  If the number is not unique
it is almost certain there will be a collision of ClOrdIds across the system.
While this is not an immediate problem, it will be difficult to
audit which server handled the orders.  If two ems-service instances
are connected to the same venue, and use the same ID, there will
be ClOrdId collisions, and failure to trade.  This is a pathological case;
however, the operators must be certain and use CI/CD to guarantee
this file is unique across installations.

A later iteration of the system may verify this by speaking
to a central service to verify they are never re-used, but
this is a TODO and not implemented today.

### ems-service restarted

A restart of ems-service is a simple event, a restart.
The reconnectCount and connection info is reloaded for 
each connection, which is in a file in the /tmp folder on the
machine where ems-service is running.  These files 
can also easily be stored on S3; *and should be in production.*
The current storage uses a /tmp folder.  

When the ems-service restarts, these files are loaded.

The clients will re-connect automatically, and the reconnectCount
increased by one.  When this happens, an offset of 1 billion orders
is added by the system.  The emsClient uses this reconnectCount
to add a billion orders for each reconnect.  If the emsClient
uses a billion orders (however unlikely), the server 
synthetically adds to this reconnectCount.

The combination of these two incrementing "reconnectCount" systems
results in this offset going up by a billion each time a client
reconnects.  Clients should connect once a day.  But if they
reconnect, this counter goes up by a billion.  This way
the orderIds will never collide after a reconnect.  

And it simplifies the system, because, we don't need to keep the
current orderId for each client on a disk anywhere.  It is only 
in main memory **on the EMS Client** intentionally; allowing for speed.
We avoid a round trip to the server to fetch the next orderId,
and the latency cost, by putting this client side.  Further,
it is not stored on disk; each reconnect gets another billion
added to the orderId offset.

Finally, if the EmsClient is connected for many days, every 24 hours
if more than a hundred billion orders have been used, the 
counter is reset to zero by the client.  The server will 
reset the reconnectCount to zero every 24 hours.  
This allows the orderId counter to return to zero after a long 
time of usage, if the systems remain up for days or weeks.

The ems-service can therefore run for days or weeks, and
be connected to 24/7 markets, and restarted as necessary.

## ems-service machine catastrophic failure

The reconnectCount files used to track the number of reconnects
of each client must be recovered and loaded on a new machine.
Using S3 is a trivial solution, as the S3 bucket can be connected
to the new machine on startup, and the files loaded by the new machine.

If S3 is not used or available, then a SAN can be used. 
These files are only written when the client connects, or
once when more than a billion orders are used by an EmsClient
(the files are incremented, as if a reconnect occurred.)

Therefore, the file storage can be slow, and S3 or a SAN is 
most appropriate.

In a rare situation where co-location is in use and there is no S3
or SAN, and a machine is lost, there is a workaround.

The workaround is to keep a copy of the files, and FTP them back
when they change to a campus disk, for recovery purposes.

If these files are not created, and a list ditch effort is needed
there will be a flag **-Drecovery=true**, which can be set on startup
of ems-service.  When this is set, the offset is increased by 500
billion, which does not *strickly* guarantee you will not 
have an orderId collisoin (as a client *could have* sent 500 billion) 
orders; however, it very reasonably assumes that no client 
has sent 500 billion orders.
1.  It will set the reconnectCount to 500; this will set the 
    orderId offset to 500 billion, per connection.  It is unlikely
    that any client has sent 500 billion orders, and therefore,
    orderIds will not be re-used.

