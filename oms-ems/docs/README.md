## Documentation

**docs/decisions** is from MADR approach to documentation in markdown files.

Reading all decisions (ignore the first decision, which 
is part of the MADR distribution.)

You can then see the design decisions which were made.

The docs/manual folder, GETTING_STARTED.md, is a reasonable
place to get a high level overview for an operator.

## Developing: Required background reading

### Spring Boot 3.0 

Read the entire Spring Boot 3.0 reference.
https://docs.spring.io/spring-boot/docs/current/reference/html/index.html

1. Skip 'Messaging' but read 'Kafka', and Caching but read 'Hazelcast'
1. We use the Spring Boot 3.0 Kafka connector, and testing facilities.
1. We also use Spring Boot @Configuration, and some limited use of Spring Boot Configuration.
1. The testing is useful in Spring, and the IOC container.
1. We will use the Docker image deploy; the layered docker jars Spring helps you produce.

### Flatbuffers Java docs

#### Example

Flatbuffers docs on the web are outdated, some broken links.

The best example is here:

https://github.com/google/flatbuffers/blob/master/samples/SampleBinary.java

#### Docs

Read the Tutorial:  https://flatbuffers.dev/flatbuffers_guide_use_java.html

(note the tutorial link to Java code does not work, thus the "Sample Binary" file above.)

2nd tutorial link: https://flatbuffers.dev/flatbuffers_guide_tutorial.html

#### OnixS FIX state transitions:

https://www.onixs.biz/fix-dictionary/4.2/app_d.html






