package com.hedgeos.fix.adapter;

import com.hedgeos.fix.adapter.base62.EmsBase62;
import org.unbrokendome.base62.Base62;

import java.nio.LongBuffer;

/**
 * Designed in one hour, written in two hours.. needs a latency rewrite, but is not slow.
 * (currently p50 of 6usecs to make a ClOrdId)
 * TODO:  Spend a week making this completely optimal, latency wise.
 *
 * ------------------------
 #### Tag 11 encoding:

 1. emsServerId:  allows for 62 EMS servers with uniqueness across them.
 2. connectionId:  allows for 62 connections to each EMS server, more than sufficient as multiple EMS servers may be run.
 3. dayOfMonth:  1-31, day of month, trading date.  Useful to observe logs and debugging.
 4. hedgeOsOrderId: **8 digits** for base62 encoded ID, 62 to the power of 8 = **62^8** = 218.3 trillion = **218,340,105,584,896**
 5. hedgeOsOrderId:  continued
 6. hedgeOsOrderId:  continued
 7. hedgeOsOrderId:  continued
 8. hedgeOsOrderId:  continued
 9. hedgeOsOrderId:  continued
 10. hedgeOsOrderId:  continued
 11. hedgeOsOrderId:  continued
 12. pmGroupId:  **A 3 char moniker for the PM desk**
 13. pmGroupId:  second char of three char pmGroup
 14. pmGroupId:  third char of three char pmGroup
 15. securityId:  **10 digits** If the alternate FIX venue allows 25 chars length for ClOrdId, we will encode our own security bank identifier, in these 10 chars
 16. securityId:
 17. securityId:
 18. securityId:
 19. securityId:
 20. securityId:
 21. securityId:
 22. securityId:
 23. securityId:
 24. securityId:
 25. securityId:
 -------------------------
 */
public class ClientOrderIdStuffer {

    public static int EMS_SERVER_ID_POSITION = 0;
    public static int CONNECTION_ID_POSITION = 1;
    public static int DAY_OF_MONTH_POSITION = 2;

    // HedgeOs Order ID is 8 chars long to support 200 trillion orders
    public static int HEDGEOS_ORDER_ID_LENGTH = 8;
    public static final int ORDER_ID_START_POSITION = 3;
    public static final int ORDER_ID_END_POSITION =
            ORDER_ID_START_POSITION + HEDGEOS_ORDER_ID_LENGTH;
    // the base62 parser assumes an 11 char string for longs
    public static final String TO_BE_REMOVED_ORDER_ID_PADDING_FOR_ENCODING = "000"; // 11-8 = 3 zeros.

    public static final int PM_GROUP_ID_LENGTH = 3;

    // SecurityId is 7 chars long to support 2 trillion unique securities.
    public static int SECURITY_ID_LENGTH = 11;
    public static int SECURITY_ID_START_POSITION = 3+PM_GROUP_ID_LENGTH+HEDGEOS_ORDER_ID_LENGTH;
    public static int SECURITY_ID_END_POSITION =
            SECURITY_ID_START_POSITION + SECURITY_ID_LENGTH;
    // public static final String SECURITY_ID_PADDING = "00"; // 11-9 = 4 zeros
    public static final String PM_GROUP_ID_PADDING = "000000000"; // 11-2 = 9 zeros


    public static final int EMS_SERVER_ID_LIMIT_FOR_BASE62_ENCODING = 61;
    public static final int CONNECTION_ID_LIMIT_FOR_BASE62_ENCODING = 61;


    public static String stuffClientOrderID(
            int emsServerId,
            int connectionId,
            int dayOfMonth,
            long hedgeOsOrderId,
            String pmGroupId,
            long securityId)
    {
        if (emsServerId < 1 || connectionId < 1 || dayOfMonth < 1 || hedgeOsOrderId < 1 || securityId < 1) {
            throw new RuntimeException("We don't use zero or negative values");
        }

        if (pmGroupId.length() > 3) {
            throw new RuntimeException("pmGroup must be at most three chars.");
        }

        if (emsServerId > EMS_SERVER_ID_LIMIT_FOR_BASE62_ENCODING)
            throw new RuntimeException("emsServerID can't be > "+EMS_SERVER_ID_LIMIT_FOR_BASE62_ENCODING);
        if (connectionId > CONNECTION_ID_LIMIT_FOR_BASE62_ENCODING)
            throw new RuntimeException("connectionId cant be > "+CONNECTION_ID_LIMIT_FOR_BASE62_ENCODING);
        if (dayOfMonth > 31)
            throw new RuntimeException("dayOfMonth should never be > 31");

        if (emsServerId >= 62)
            throw new RuntimeException("Only allow 61 emsServerIds, because of stuffing.");
        if (connectionId >= 62)
            throw new RuntimeException("only allow 61 connectionId, because of stuffing.");

        StringBuilder sb = new StringBuilder();

        String emsServerChar = EmsBase62.encode(1, emsServerId);
        // trim to one char, remove the padding from the encoder
        sb.append(emsServerChar);

        String connectionIdChar = EmsBase62.encode(1, connectionId);
        // trim to one char, remove the padding from the encoder
        // char lastConnectionIdChar = connectionIdChar.charAt(connectionIdChar.length()-1);
        sb.append(connectionIdChar);

        String dayOfMonthString = EmsBase62.encode(1, dayOfMonth);
        // trim to one char, remove the padding from the encoder
        // char dayOfMonthLastChar = dayOfMonthString.charAt(dayOfMonthString.length()-1);
        sb.append(dayOfMonthString);

        // Need to pad the hedgeOsOrderId
        String orderIdString = EmsBase62.encode(HEDGEOS_ORDER_ID_LENGTH, hedgeOsOrderId);


        sb.append(orderIdString);

        // we simply tack on the pmGroupID
        sb.append(pmGroupId);

        String secIdString = Base62.encode(securityId);
        // secIdString = secIdString.substring(secIdString.length()-SECURITY_ID_LENGTH, secIdString.length());
        sb.append(secIdString);

        String result = sb.toString();
        // System.out.println("ClOrdId="+result);

        return result;
    }

    /*
    ------------------------------------------
    Start of the parser methods
    ------------------------------------------
     */

    public static long emsServerIdFromClOrdId(String clientOrderId) {
        String emsServerId = clientOrderId.substring(EMS_SERVER_ID_POSITION, EMS_SERVER_ID_POSITION+1);
        long[] longArray = new long[1];
        LongBuffer longBuffer = LongBuffer.wrap(longArray);
        EmsBase62.decode(emsServerId, longBuffer);
        long valueBack = longArray[0];
        return valueBack;
    }

    public static long connectionIdFromClOrdId(String clientOrderId) {
        String connectionIdSubstring = clientOrderId.substring(CONNECTION_ID_POSITION,CONNECTION_ID_POSITION+1);
        // String repadded = "0000000000"+connectionIdSubstring;
        long[] longArray = new long[1];
        LongBuffer longBuffer = LongBuffer.wrap(longArray);
        EmsBase62.decode(connectionIdSubstring, longBuffer);
        long valueBack = longArray[0];
        return valueBack;
    }


    public static long dayOfMonthFromClOrdId(String clientOrderId) {
        StringBuilder sb = new StringBuilder(clientOrderId.substring(DAY_OF_MONTH_POSITION,DAY_OF_MONTH_POSITION+1));
        // String repadded = "0000000000"+connectionIdSubstring;
        long[] longArray = new long[1];
        LongBuffer longBuffer = LongBuffer.wrap(longArray);
        EmsBase62.decode(sb, longBuffer);
        long valueBack = longArray[0];
        return valueBack;
    }

    public static long orderIdFromClOrdID(String clientOrderId)
    {
        // TODO: Work in progress to use EmsBase62..
        StringBuilder orderIdSubstring =
                new StringBuilder(TO_BE_REMOVED_ORDER_ID_PADDING_FOR_ENCODING);
        orderIdSubstring.append(clientOrderId.substring(ORDER_ID_START_POSITION, ORDER_ID_END_POSITION));
        long[] longArray = new long[1];
        LongBuffer longBuffer = LongBuffer.wrap(longArray);
        Base62.decode(orderIdSubstring, longBuffer);
        long valueBack = longArray[0];
        return valueBack;
    }


    public static String pmGroupIdFromClOrdId(String clientOrderId) {
        return clientOrderId.substring(ORDER_ID_END_POSITION,ORDER_ID_END_POSITION+PM_GROUP_ID_LENGTH);
    }

    public static long securityIdFromClOrdId(String clientOrderId) {
        StringBuilder sb = new StringBuilder(11);
        sb.append(clientOrderId.substring(SECURITY_ID_START_POSITION,SECURITY_ID_END_POSITION));
        long[] longArray = new long[1];
        LongBuffer longBuffer = LongBuffer.wrap(longArray);
        Base62.decode(sb, longBuffer);
        long valueBack = longArray[0];
        return valueBack;
    }

}
