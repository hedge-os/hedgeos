package com.hedgeos.fix.adapter;

import order.EmsReport;
import order.EmsReportEnvelope;

public interface EmsCallback {
    void callback(EmsReportEnvelope envelope, EmsReport er);
}
