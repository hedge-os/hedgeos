package com.hedgeos.fix.adapter;

import lombok.extern.slf4j.Slf4j;
import order.EmsExecType;
import order.OrderStatus;

@Slf4j
public class EmsFixTranslator {
    public static char sideToFixSide(int side) {
        return switch (side) {
            case order.Side.BUY -> '1';
            case order.Side.SELL -> '2';
            case order.Side.BUY_MINUS -> '3';
            case order.Side.SELL_PLUS -> '4';
            case order.Side.SELL_SHORT -> '5';
            case order.Side.SELL_SHORT_EXEMPT -> '6';
            case order.Side.UNDISCLOSED -> '7';
            case order.Side.CROSS -> '8';
            case order.Side.CROSS_SHORT -> '9';
            case order.Side.CROSS_SHORT_EXEMPT -> 'A';
            case order.Side.AS_DEFINED -> 'B';
            case order.Side.OPPOSITE -> 'C';
            case order.Side.SUBSCRIBE -> 'D';
            case order.Side.REDEEM -> 'E';
            case order.Side.LEND -> 'F';
            case order.Side.BORROW -> 'G';
            default -> throw new RuntimeException("Unknown side=" + side);
        };
    }

    public static int getEmsSide(char sideChar) {
        //noinspection EnhancedSwitchMigration
        switch (sideChar) {
            case '1': return order.Side.BUY;
            case '2': return order.Side.SELL;
            case '3': return order.Side.BUY_MINUS;
            case '4': return order.Side.SELL_PLUS;
            case '5': return order.Side.SELL_SHORT;
            case '6': return order.Side.SELL_SHORT_EXEMPT;
            case '7': return order.Side.UNDISCLOSED;
            case '8': return order.Side.CROSS;
            case '9': return order.Side.CROSS_SHORT;
            case 'A': return order.Side.CROSS_SHORT_EXEMPT;
            case 'B': return order.Side.AS_DEFINED;
            case 'C': return order.Side.OPPOSITE;
            case 'D': return order.Side.SUBSCRIBE;
            case 'E': return order.Side.REDEEM;
            case 'F': return order.Side.LEND;
            case 'G': return order.Side.BORROW;
            default: {
                log.error("unknown side passed={}", sideChar);
                throw new Error("unknown side=" + sideChar);
            }
        }
    }

    public static int getEmsReportTypeForExecReport(char fixExecType) {
        switch (fixExecType) {
            case '0': return EmsExecType.NEW_EXEC_TYPE;
            case '1': return EmsExecType.PARTIAL_FILL_EXEC_TYPE;
            case '2': return EmsExecType.FILL_EXEC_TYPE;
            case '3': return EmsExecType.DONE_FOR_DAY_EXEC_TYPE;
            case '4': return EmsExecType.CANCEL_SUCCESS_EXEC_TYPE;
            case '5': return EmsExecType.REPLACE_SUCCESS_EXEC_TYPE;
            case '6': return EmsExecType.PENDING_CANCEL_EXEC_TYPE;
            case '7': return EmsExecType.STOPPED_EXEC_TYPE;
            case '8': return EmsExecType.REJECT_EXEC_TYPE;
            case '9': return EmsExecType.SUSPENDED_EXEC_TYPE;
            case 'A': return EmsExecType.PENDING_NEW_EXEC_TYPE;
            case 'B': return EmsExecType.CALCULATED_EXEC_TYPE;
            case 'C': return EmsExecType.EXPIRED_EXEC_TYPE;
            case 'D': return EmsExecType.RESTATED_EXEC_TYPE;
            case 'E': return EmsExecType.PENDING_REPLACE_EXEC_TYPE;
            case 'F': return EmsExecType.TRADE_EXEC_TYPE; // this is the new standards
            case 'G': return EmsExecType.TRADE_CORRECT_EXEC_TYPE;
            case 'H': return EmsExecType.TRADE_CANCEL_EXEC_TYPE;
            case 'I': return EmsExecType.ORDER_STATUS_EXEC_TYPE;
            default:
                log.error("Unknown fixExecType="+fixExecType);
                throw new Error("Unknown fixExecType="+fixExecType);
        }


    }

    public static int getEmsOrderStatusForOrdStatus(char fixOrdStatus) {
        switch (fixOrdStatus) {
            case '0': return OrderStatus.NEW_ORDER_STATUS;
            case '1': return OrderStatus.PART_FILLED_ORDER_STATUS;
            case '2': return OrderStatus.FILLED_ORDER_STATUS;
            case '3': return OrderStatus.DONE_FOR_DAY_ORDER_STATUS;
            case '4': return OrderStatus.CANCELLED_ORDER_STATUS;
            case '5': return OrderStatus.REPLACED_ORDER_STATUS;
            case '6': return OrderStatus.PENDING_CANCEL_ORDER_STATUS;
            case '7': return OrderStatus.STOPPED_ORDER_STATUS;
            case '8': return OrderStatus.REJECTED_ORDER_STATUS;
            case '9': return OrderStatus.SUSPENDED_ORDER_STATUS;
            case 'A': return OrderStatus.PENDING_NEW_ORDER_STATUS;
            case 'B': return OrderStatus.CALCULATED_ORDER_STATUS;
            case 'C': return OrderStatus.EXPIRED_ORDER_STATUS;
            case 'D': return OrderStatus.ACCEPTED_FOR_BIDDING_ORDER_STATUS;
            case 'E': return OrderStatus.PENDING_REPLACE_ORDER_STATUS;
            default:
                throw new Error("Unknown OrderStatus FIX char="+fixOrdStatus);
        }
    }
}
