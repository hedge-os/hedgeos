package com.hedgeos.fix.adapter.base62;

import javax.annotation.MatchesPattern;
import javax.annotation.Nonnull;
import java.nio.LongBuffer;
import java.util.UUID;


public class EmsBase62 {

    /**
     * Private constructor, to prevent instantiation
     */
    private EmsBase62() {
    }


    /**
     * Converts a number of {@code long} values to a Base62-encoded string.
     *
     * The length of the resulting string will be 11 times the number of arguments.
     * Calling the method with an empty argument array (i.e. `encode()`)
     * will return an empty string.
     *
     * @param value the {@code long} values to encode
     * @return the Base62-encoded string
     */

    @Nonnull
    @MatchesPattern("([0-9A-Za-z]{11})*")
    public static String encode(int stringLength, long value) {
        Base62Encoder encoder = new Base62Encoder(stringLength);
        encoder.accept(value);
        return encoder.toString();
    }

    /**
     * Converts the remaining contents of a {@link LongBuffer} to a Base62-encoded string.
     *
     * @param buffer the {@code LongBuffer} to read values from
     * @return the Base62-encoded string
     */
    @Nonnull
    @MatchesPattern("([0-9A-Za-z]{11})*")
    public static String encode(int stringLength, LongBuffer buffer) {
        Base62Encoder encoder = new Base62Encoder(stringLength);
        while (buffer.hasRemaining()) {
            encoder.accept(buffer.get());
        }
        return encoder.toString();
    }


    /**
     * Decodes a Base62-encoded string into a {@link LongBuffer}.
     *
     * The length of the input string must be a multiple of 11, and must only contain
     * alphanumeric ASCII characters. Passing an empty string will result in no modification
     * to the output buffer.
     *
     * @param input a Base62-encoded input string
     * @param output a {@link LongBuffer} that will receive the decoded values
     * @throws IllegalArgumentException if the input is not a valid Base62-encoded string
     * @throws java.nio.BufferOverflowException if the {@code output} buffer has not enough capacity to hold
     *         all decoded values
     */
    public static void decode(
            @MatchesPattern("([0-9A-Za-z]{11})*") CharSequence input,
            LongBuffer output) {
        Base62Decoder decoder = new Base62Decoder(input);
        decoder.decode(output);
    }

}
