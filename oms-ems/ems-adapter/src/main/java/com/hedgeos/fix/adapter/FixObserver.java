package com.hedgeos.fix.adapter;

import order.EmsReport;
import order.EmsReportEnvelope;

public interface FixObserver {

    void onMessage(EmsReportEnvelope envelope, EmsReport emsReport);

}
