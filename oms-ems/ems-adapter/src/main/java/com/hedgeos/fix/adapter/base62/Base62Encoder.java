package com.hedgeos.fix.adapter.base62;

import java.nio.CharBuffer;
import java.util.function.LongConsumer;


final class Base62Encoder implements LongConsumer {

    private final StringBuilder builder = new StringBuilder();
    private final CharBuffer charBuffer;
    private final int stringLength;

    public Base62Encoder(int stringLength) {
        this.stringLength = stringLength;
        charBuffer = CharBuffer.allocate(stringLength);
    }


    @Override
    public void accept(long value) {

        long v = value;
        if (stringLength == 1) {
            putDigit(0, (int) (v % 62));
        }
        else {
            for (int i = stringLength-1; i > 0; i--) {
                putDigit(i, (int) (v % 62));
                v /= 62;
            }
            putDigit(0, (int) v);
        }

        // No need to call flip() on the charBuffer because we use absolute put() operations
        // which don't change the buffer's position

        builder.append(charBuffer);
        charBuffer.clear();
    }


    private void putDigit(int index, int charIndex) {
        charBuffer.put(index, Base62Digits.getDigit(charIndex));
    }


    @Override
    public String toString() {
        return builder.toString();
    }

    public StringBuilder getBuilder() {
        return builder;
    }
}
