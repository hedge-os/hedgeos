package com.hedgeos.fix.adapter;

import com.hedgeos.ems.config.beans.EmsConfigFiles;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import com.hedgeos.ems.csvfiles.symbology.SymbolAdapter;
import com.hedgeos.ems.order.OrderChain;
import order.CancelReplace;
import order.Order;

public interface FixAdapter {

    FixAdapterConfig getConfig();

    void loadAndConnect(int emsServerId,
                        EmsConfigFiles emsConfig,
                        FixAdapterConfig config,
                        SymbolAdapter symbolAdapter);

    void subscribe(EmsCallback callback);




    int submitNewOrder(String symbol, Order order, String clientOrderIdStuffed, boolean isOrderUsingLocates);

    int cancelOrder(String symbol,
                           long hedgeOsOrderId,
                           int side,
                           long submitTimeUtcMillis,
                           String clientOrderIdStuffed,
                           String originalClientOrderIdStuffed);

    int cancelReplaceOrder(CancelReplace replace, String symbol, OrderChain order,
                           String clientOrderIdStuffed, String originalClientOrderIdStuffed,
                           boolean isOrderUsingLocates);

    void stop();

    String getName();

    int sendGapFill(int from, int to);

    // admin
    // void validateConfig();
    // void checkConnectionStatus();
}
