package com.hedgeos.fix.adapter.base62;

import java.nio.CharBuffer;
import java.nio.LongBuffer;


final class Base62Decoder {

    private final CharBuffer charBuffer;


    Base62Decoder(CharSequence input) {
//        if (input.length() % 11 != 0) {
//            throw new IllegalArgumentException("input length must be a multiple of 11");
//        }

        this.charBuffer = CharBuffer.wrap(input);
    }


    private long decodeLong() {
        boolean negative = false;
        long value;

        char ch = charBuffer.get();
        int digit = Base62Digits.getDigitIndex(ch);

        value = (long) digit;

        for (int j = 1; j < charBuffer.length(); j++) {
            ch = charBuffer.get();
            digit = Base62Digits.getDigitIndex(ch);
            value = value * 62 + digit;
        }

        return value;
    }


    void decode(LongBuffer output) {
        while (charBuffer.hasRemaining()) {
            output.put(decodeLong());
        }
    }
}
