package com.hedgeos.fix.adapter;

import com.hedgeos.ems.config.beans.FixAdapterConfig;
import com.hedgeos.ems.config.beans.FixAdapterType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class FixAdapterFactory {

    public static final String QUICKFIX_ADAPTER_EMS = "quickfixAdapterEms";

    private final BeanFactory springBeanFactory;

    // Note:  Could make this more spring magical.
    public FixAdapter createAdapter(FixAdapterConfig config) {

        // TODO:  May need the config, fix version, etc
        FixAdapterType fixAdapterType = config.getAdapter_type();

        switch (fixAdapterType) {
            case QUICKFIX -> {
                return createNamedBeanAdapter(QUICKFIX_ADAPTER_EMS);
            }
            case CHRONICLE -> throw new RuntimeException("Support pending for Chronicle");
            case ONIXS -> throw new RuntimeException("Support pending for ONIXS");
            default -> throw new RuntimeException("Unknown FIX Adapter Type"+fixAdapterType);
        }
    }


    private FixAdapter createNamedBeanAdapter(String beanName)
    {

        // here I use the bean factory, allowing QuickfixAdapter to be in another package
        // which this package does not depend on; allowing for different adapters to be
        // added to the system, without direct dependencies.
        FixAdapter fixAdapter =
                springBeanFactory.getBean(beanName, FixAdapter.class);

        return fixAdapter;
    }
}
