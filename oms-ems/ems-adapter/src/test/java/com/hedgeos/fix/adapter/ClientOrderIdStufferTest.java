package com.hedgeos.fix.adapter;

import org.HdrHistogram.Histogram;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ClientOrderIdStufferTest {

    // TODO:  50 more unit tests here for boundary conditions
    // TODO:  10 more unit tests for latency

    @Test
    public void testBasicStuffing() {
        int emsServerId = 1;
        int connectionId = 3;
        // 61 = 'z'
        long hedgeOsOrderId = 61;
        // 123 = '1' + 'y' = "1y"
        long hedgeOsSecurityId = Long.MAX_VALUE;
        int dayOfMonth = 31;
        String pmGroup = "CMR";

        String clientOrderId =
                ClientOrderIdStuffer.stuffClientOrderID
                        (1, connectionId, dayOfMonth
                                ,hedgeOsOrderId,pmGroup, hedgeOsSecurityId);
        System.out.println("clientOrderID="+clientOrderId);

        long emsServerIdBack = ClientOrderIdStuffer.emsServerIdFromClOrdId(clientOrderId);
        assertEquals(emsServerId, emsServerIdBack, "Failed to retrieve emsServerId");

        long connectionIdBack = ClientOrderIdStuffer.connectionIdFromClOrdId(clientOrderId);
        assertEquals(connectionId, connectionIdBack, "Failed to retrieve connectionId");

        long dayOfMonthBack = ClientOrderIdStuffer.dayOfMonthFromClOrdId(clientOrderId);
        assertEquals(dayOfMonth, dayOfMonthBack, "Failed to retrieve dayOfMonth from ClientOrderId");

        // get the order_id back.
        long orderIdBack = ClientOrderIdStuffer.orderIdFromClOrdID(clientOrderId);
        assertEquals(hedgeOsOrderId, orderIdBack, "Failed to crack hedgeOsOrderId from clientOrderId");

        String pmGroupIdBack = ClientOrderIdStuffer.pmGroupIdFromClOrdId(clientOrderId);
        assertEquals(pmGroupIdBack, pmGroupIdBack, "failed to crack pmGroupId");

        // get the security_id back.
        long securityIdBack = ClientOrderIdStuffer.securityIdFromClOrdId(clientOrderId);
        assertEquals(hedgeOsSecurityId, securityIdBack, "Failed to crack securityId from clientOrderId");

        assertEquals("13V0000000zCMRAzL8n0Y58m7", clientOrderId);
        assertEquals(25, clientOrderId.length(), "wrong length for expected ClOrdId stuffing");

    }

    @Test
    public void latencyTestStuffing() {

        Histogram histogram =
                new Histogram(10*1000*1000L, 3);

        String clientOrderId =
                ClientOrderIdStuffer
                        .stuffClientOrderID(1, 3, 31
                                ,61,"CMR", 122);

        int size = 1000;
        List<String> ids = new ArrayList<>(size);
        long millisStart = System.currentTimeMillis();

        runExperiments(ids, size, histogram);

        long millsEnd = System.currentTimeMillis();
        long took = millsEnd - millisStart;

        int num = (int) (Math.random()*1000) % 100;
        String oneId = ids.get(num);
        System.out.println("one Id in latency test="+oneId);

        assertTrue(took < 100, "took a long time");
        System.out.println("1000 Stuffs took="+took);

        // output in mics
        // 6 mics p50, is too slow
        // TODO: make it sub-1 mic
        histogram.outputPercentileDistribution(System.out, 1000.0);
        long threshold = 10_000;
        double percentUnderSixMics = histogram.getPercentileAtOrBelowValue(threshold);
        // assertTrue(percentUnderSixMics > 50, "p50 is > 10 mics");
        System.out.println("percent under "+(threshold/1000)+" usecs = "+percentUnderSixMics);
    }

    private static void runExperiments(List<String> ids, int size, Histogram histogram) {
        for (int i=0; i<size; i++) {
            long startTime = System.nanoTime();
            String id2 = ClientOrderIdStuffer
                            .stuffClientOrderID(i % 61+1, i % 61+1, i % 30+1
                                    ,61+i,"CMR", 122+i);
            long endTime = System.nanoTime();
            histogram.recordValue(endTime-startTime);
            ids.add(id2);
        }
    }
}
