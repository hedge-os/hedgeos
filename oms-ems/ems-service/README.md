# ems-service

**ems-service** starts up an EMS using configuration defined
and loaded by the ems-config project.  Environment variables
are defined and point the ems-config system to these external files.

**ems-service** uses **ems-config** to load up the config.

Next, ems-service uses **ems-adapter** to make FIX connections
to any FIX environments configured, and construct FIX Adapters
to translate from the ems-service ingress messages to the FIX connetions.

The ems-service ingress is in flatbuffers; with the object model
defined by the "api" project protobufs, and one EmsService interface
defined in grpc flatbuffers, in the fb-lib module.

This is the entry point:
```shell
oms-ems/fb-lib/src/main/flatbuffers/EmsService.fbs
```

The messages are defined in files in **order.proto** in the **'api'** module
of hedge-os, which is external to the EMS module.