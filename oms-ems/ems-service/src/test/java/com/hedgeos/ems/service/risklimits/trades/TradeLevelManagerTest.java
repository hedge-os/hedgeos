package com.hedgeos.ems.service.risklimits.trades;


import lombok.SneakyThrows;
import org.HdrHistogram.Histogram;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicIntegerArray;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TradeLevelManagerTest {

    public static final TimeZone UTC = TimeZone.getTimeZone("UTC");

    @Test
    public void testTenths() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(Calendar.SECOND, 1);
        cal.set(Calendar.MILLISECOND, 0);
        long time = cal.getTimeInMillis();
        int tenthOffset = TradeAmountTracker.twoSecondTenthsOffset(time);
        assertEquals(10, tenthOffset);
    }

    @Test
    public void testTenthsAgain() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(Calendar.SECOND, 12);
        cal.set(Calendar.MILLISECOND, 405);
        long timeTwo = cal.getTimeInMillis();
        int tenthOffsetTwo = TradeAmountTracker.twoSecondTenthsOffset(timeTwo);
        assertEquals(4, tenthOffsetTwo);
    }

    @Test
    public void testTenthsAgain2() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(Calendar.SECOND, 11);
        cal.set(Calendar.MILLISECOND, 405);
        long timeTwo = cal.getTimeInMillis();
        int tenthOffsetTwo = TradeAmountTracker.twoSecondTenthsOffset(timeTwo);
        assertEquals(14, tenthOffsetTwo);
    }


    @Test
    public void testTenSecondTenths() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023, 1, 1, 7, 10, 0);
        cal.set(Calendar.MILLISECOND, 405);
        long timeTwo = cal.getTimeInMillis();
        int tenthOffsetTwo = TradeAmountTracker.tenSecondTenthsOffset(timeTwo);
        assertEquals(4, tenthOffsetTwo);
    }

    @Test
    public void testTenSecondTenthsAgain() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023, 1, 1, 7, 10, 1);
        cal.set(Calendar.MILLISECOND, 405);
        long timeTwo = cal.getTimeInMillis();
        int tenthOffsetTwo = TradeAmountTracker.tenSecondTenthsOffset(timeTwo);
        assertEquals(14, tenthOffsetTwo);
    }
    @Test
    public void testTenSecondThirdTry() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023, 1, 1, 7, 10, 5);
        cal.set(Calendar.MILLISECOND, 405);
        long time = cal.getTimeInMillis();
        int tenthOffset = TradeAmountTracker.tenSecondTenthsOffset(time);
        assertEquals(54, tenthOffset);
    }

    @SneakyThrows
    @Test
    public void testFiveSecondIntervals() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023, 1, 1, 7, 11, 2);
        long time = cal.getTimeInMillis();
        int fiveSecondOffset = TradeAmountTracker.tenMinutesFiveSecondOffsets(time);
        assertEquals(12, fiveSecondOffset);

        Thread.sleep(10_000);
        cal.set(2023, 1, 1, 7, 11, 2);
        time = cal.getTimeInMillis();
        fiveSecondOffset = TradeAmountTracker.tenMinutesFiveSecondOffsets(time);
        assertEquals(12, fiveSecondOffset);


        cal.set(2023, 1, 1, 7, 11, 12);
        time = cal.getTimeInMillis();
        fiveSecondOffset = TradeAmountTracker.tenMinutesFiveSecondOffsets(time);
        assertEquals(14, fiveSecondOffset);
    }

    @Test
    public void testFiveSecondAgain() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023,1,1,7,0,5);
        long time = cal.getTimeInMillis();
        int fiveSecondOffset = TradeAmountTracker.tenMinutesFiveSecondOffsets(time);
        assertEquals(1, fiveSecondOffset);
    }

    @Test
    public void testFiveSecondThree() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023,1,1,7,1,5);
        long time = cal.getTimeInMillis();
        int fiveSecondOffset = TradeAmountTracker.tenMinutesFiveSecondOffsets(time);
        assertEquals(13, fiveSecondOffset);
    }

    @Test
    public void testMillisToTwoHoursMinutes() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023,1,1,8,11,2);
        long time = cal.getTimeInMillis();
        int minute = TradeAmountTracker.minuteOffset(time);
        assertEquals(11, minute);
    }

    @Test
    public void testHoursInTwoDays() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023,1,1,8,11,2);
        long time = cal.getTimeInMillis();
        int hour = TradeAmountTracker.hoursOffset(time);
        assertEquals(8+24, hour);
    }


    @Test
    public void testHoursInTwoDaysAgain() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023,1,2,8,11,2);
        long time = cal.getTimeInMillis();
        int hour = TradeAmountTracker.hoursOffset(time);
        assertEquals(8, hour);
    }

    @Test
    public void testMillisToMinute() {
        Calendar cal = Calendar.getInstance(UTC);
        cal.set(2023,1,1,7,10,2);
        long time = cal.getTimeInMillis();
        int minute = TradeAmountTracker.minuteOffset(time);
        assertEquals(70, minute);

        int second = TradeAmountTracker.secondOffset(time);
        assertEquals(2, second);

        cal.set(2023,1,1,7,11,2);
        int secondAgain = TradeAmountTracker.secondOffset(cal.getTimeInMillis());
        assertEquals(62, secondAgain);
    }

    @Test
    public void testArrayAggSpeed() {
        AtomicIntegerArray array = new AtomicIntegerArray(60);
        for (int i=0; i<60; i++) {
            array.set(i, (int) (Math.random() * 10000));
        }

        Histogram histogram = new Histogram(1000*1000L, 3);

        int grandTotal = 0;

        runExperiment(array, histogram, grandTotal, false);
        runExperiment(array, histogram, grandTotal, false);
        runExperiment(array, histogram, grandTotal, false);
        runExperiment(array, histogram, grandTotal, false);

        for (int j=0; j<500; j++) {
            runExperiment(array, histogram, grandTotal, true);
        }
        // output in mics
        // 6 mics p50, is too slow
        // TODO: make it sub-1 mic
        histogram.outputPercentileDistribution(System.out, 1000.0);
        // assertTrue(percentUnderSixMics > 50, "p50 is > 10 mics");

        System.out.println("percent under 3usecs = "+ histogram.getPercentileAtOrBelowValue(3_000));
        System.out.println("percent under 5usecs = "+ histogram.getPercentileAtOrBelowValue(5_000));
        System.out.println("percent under 10usecs = "+ histogram.getPercentileAtOrBelowValue(10_000));
    }

    private static void runExperiment(AtomicIntegerArray array, Histogram histogram, int grandTotal, boolean record) {
        long nanoStart = System.nanoTime();
        int total = 0;
        for (int i = 0; i< array.length(); i++) {
            total += array.get(i);
        }
        long nanoEnd = System.nanoTime();
        if (record) {
            histogram.recordValue(nanoEnd - nanoStart);
        }
        grandTotal += total;
    }
}
