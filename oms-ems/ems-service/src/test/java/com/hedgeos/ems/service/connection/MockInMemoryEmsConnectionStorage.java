package com.hedgeos.ems.service.connection;

import java.util.ArrayList;
import java.util.List;

public class MockInMemoryEmsConnectionStorage implements EmsConnectionStorage {
    @Override
    public void writeConnectionCountFile(int emsServerId, ConnectionAndReconnectCount connectionInfo) {

    }

    @Override
    public void updateConnectionCountFile(int emsServerId, ConnectionAndReconnectCount connectionInfo) {

    }

    @Override
    public List<ConnectionAndReconnectCount> loadConnectionInfo(int emsServerId) {
        return new ArrayList<>();
    }
}
