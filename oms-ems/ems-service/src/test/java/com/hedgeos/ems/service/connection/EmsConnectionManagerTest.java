package com.hedgeos.ems.service.connection;

import com.hedgeos.ems.service.EmsServerIdManager;
import com.hedgeos.ems.service.exception.NoRemainingConnections;
import order.ConnectRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EmsConnectionManagerTest {

    @Test
    public void testConnectRequest() throws NoRemainingConnections {
        EmsConnectionStorage fakeStorage = new MockInMemoryEmsConnectionStorage();
        EmsConnectionManager connectionManager =
                new EmsConnectionManager(new EmsServerIdManager(), fakeStorage);

        int stickyConnId = 0;

        ConnectRequest request =
                EmsConnectionMockFactory.buildConnectRequest("test", "testDEV7", stickyConnId);

        ConnectionAndReconnectCount reconnectCount =
                connectionManager.getNextConnectionId(request);

        assertEquals(0, reconnectCount.getReconnectCounter().get());

        ConnectRequest request2 =
                EmsConnectionMockFactory.buildConnectRequest("test", "testDEV7", reconnectCount.getConnectionId());
        ConnectionAndReconnectCount reconn2 =
                connectionManager.getNextConnectionId(request2);
        assertEquals(1, reconn2.getReconnectCounter().get());
        assertEquals(reconnectCount.getConnectionId(), reconn2.getConnectionId());
    }

    @Test
    public void testGetNextEmsConnection() throws NoRemainingConnections {
        EmsConnectionStorage fakeStorage = new MockInMemoryEmsConnectionStorage();
        EmsConnectionManager connectionManager =
                new EmsConnectionManager(new EmsServerIdManager(), fakeStorage);

        int nextId = connectionManager.getNextAvailableConnectionIdFromArray("chris");
        int nextIdAgain = connectionManager.getNextAvailableConnectionIdFromArray("chris");

        assertEquals(53, nextId);
        assertEquals(nextId, nextIdAgain);

        for (int i = -1; i<65; i++) {
            try {
                String fakeLogin = String.valueOf(i);

                int anId =
                        connectionManager.getNextAvailableConnectionIdFromArray(fakeLogin);

                ConnectionAndReconnectCount newConn = new ConnectionAndReconnectCount(fakeLogin, System.currentTimeMillis(), anId, new AtomicInteger(0));
                        connectionManager.loginToConnectionId.putIfAbsent(fakeLogin,newConn);
                connectionManager.connections[anId] = newConn;
                System.out.println("anId=" + anId);
                int totalConnected = connectionManager.loginToConnectionId.size();

                assertTrue(totalConnected <= 60, "Too many connections="+totalConnected);
            }
            catch (Exception e) {
                Assertions.assertTrue(e instanceof NoRemainingConnections);
                assertEquals(60, connectionManager.loginToConnectionId.size(), "Should be 60 connections when full.");
            }
        }
    }
}
