package com.hedgeos.ems;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Test verifies the application context for the ems-service loads and starts.
 * Start on a random port to avoid conflict with a workstation ems-service.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties =
        {"test.ems.config.folder=../ems-config-mock-files/",
        "test.ems.config.file=active_mock_fix_connections.yml",
                "grpc.port=12121" // random port to see if we can load the system
        })
@ActiveProfiles("test")
class EmsServiceApplicationTests {

    public static final String EMS_CONFIG_FOLDER="../ems-config-mock-files/";
    public static final String EMS_CONFIG_FILE = "active_mock_fix_connections.yml";

    // this is how you provide a mock bean when testing spring boot..
//    @TestConfiguration
//    static class MockEmsConfigVariables {
//        @Bean
//        public EmsConfigVariables emsConfigVariables() {
//            return new EmsConfigVariables(EMS_CONFIG_FOLDER, EMS_CONFIG_FILE);
//        }
//    }

    @Test
    void contextLoads() {

    }

}
