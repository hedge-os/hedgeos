package com.hedgeos.ems.service.risklimits.trades;

import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitLoad;
import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitsFileRow;
import com.hedgeos.ems.util.TradingDayUtil;
import io.micrometer.core.instrument.Timer;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
public class TradeAmountTrackerLoadTest {


    // 50k takes max 30ms on my machine,
    // meaning 500k expected to take 300ms (perhaps 500ms max)
    @Test
    @Tag("load")
    public void testFiftyThousandSize() {
        testTradeAmountScale(50_000);
    }

    // also 30ms max on my machine;
    // means we could handle 1_000_000 one second limits in 300ms
    // needs:  -Xmx10g
//    @Test
//    @Tag("load")
//    public void testHundredThousdandSecuritiesOneSecondLimit() {
//        testTradeAmountScale(100_000);
//    }


    @SneakyThrows
    public void testTradeAmountScale(int testSize) {
        PrometheusMeterRegistry registry =
                new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);

        Map<TradeLimitsFileRow, TradeLimitsFileRow> tradeLimitsMap = new ConcurrentHashMap<>();
        TradeAmountTracker tradeAmountTracker = new TradeAmountTracker(registry);

        File f = new File(".");
        for (int i=0; i<testSize; i++) {
            TradeLimitsFileRow row =
                    new TradeLimitsFileRow(1, f, "CMR",
                            false, (long) i, false,
                            "SECONDS", 1L,
                            Duration.of(1, ChronoUnit.SECONDS), 5000L,
                            4000L, 0L, 0L );
            tradeLimitsMap.put(row, row);
            // fake a trade for every security
            tradeAmountTracker.updateAmountTraded(System.currentTimeMillis(), "CMR", i, 100);
        }

        TradeLimitLoad fakeLimits = new TradeLimitLoad(tradeLimitsMap, System.currentTimeMillis(), TradingDayUtil.getTradeDate());
        TradeLimitChecker checker = new TradeLimitChecker(tradeAmountTracker, fakeLimits);

        Timer timer = tradeAmountTracker.secondCleaner.getCleanTimer();

        assertTrue(tradeAmountTracker.tradingPerSecurity.size() == testSize);

        for (int i=0; i<5; i++) {
            Thread.sleep(1_000);
//            RequiredSearch search = meterRegistry.get(LIMITS_CLEANER_SECOND_LIMIT_REGISTRY_NAME);
//            Timer timer = search.timer();
            log.warn("max=" + timer.max(TimeUnit.MILLISECONDS)
                    + ", mean=" + timer.mean(TimeUnit.MILLISECONDS)
                    + ", count=" + timer.mean(TimeUnit.MILLISECONDS));
        }
    }
}
