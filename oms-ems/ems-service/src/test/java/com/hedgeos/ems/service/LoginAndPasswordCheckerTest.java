package com.hedgeos.ems.service;

import com.hedgeos.ems.service.connection.EmsConnectionMockFactory;
import lombok.extern.slf4j.Slf4j;
import order.ConnectRequest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class LoginAndPasswordCheckerTest {

    EmsEnvironmentManager envManager = new EmsEnvironmentManager();
    LoginAndPasswordChecker checker = new LoginAndPasswordChecker(envManager);

    @Test
    public void testLoginAndSillyPassword() {
        String login = "chris";
        String password = "chrisWORKSTATION";
        String length = ""+password.length();
        log.info("length="+length);
        password += length;
        log.info("passowrd="+password);

        ConnectRequest request = EmsConnectionMockFactory.buildConnectRequest(login, password, 1);
        int result = checker.verifyLogin(request);

        assertEquals(0, result);
    }
}
