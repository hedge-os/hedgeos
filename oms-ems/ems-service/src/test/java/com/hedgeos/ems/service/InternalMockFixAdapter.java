package com.hedgeos.ems.service;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.config.beans.EmsConfigFiles;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import com.hedgeos.ems.csvfiles.symbology.SymbolAdapter;
import com.hedgeos.ems.order.OrderChain;
import com.hedgeos.ems.util.PrecisionConstants;
import com.hedgeos.ems.util.QuantityConverter;
import com.hedgeos.ems.util.TradingDayUtil;
import com.hedgeos.fix.adapter.EmsCallback;
import com.hedgeos.fix.adapter.FixAdapter;
import environment.EmsEnvironment;
import general.Price;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.hedgeos.ems.service.EmsReportTestBuilder.*;
import static com.hedgeos.test.IntegrationConstants.*;

@Slf4j
public class InternalMockFixAdapter implements FixAdapter {

    EmsCallback emsCallback;

    FixAdapterConfig fixAdapterConfig;
    Map<String, Order> restingOrdersByClOrderId = new ConcurrentHashMap<>();
    private int emsServerId;
    private FixAdapterConfig config;
    private EmsReportEnvelope envelope;

    @Override
    public FixAdapterConfig getConfig() {
        return fixAdapterConfig;
    }

    @Override
    public void loadAndConnect(int emsServerId, EmsConfigFiles emsConfig, FixAdapterConfig config, SymbolAdapter symbolAdapter) {
        log.info("load and connect called on mock");
        this.fixAdapterConfig = config;
        this.emsServerId = emsServerId;
        this.config = config;

        FlatBufferBuilder builder = new FlatBufferBuilder();
        int tradeDate = TradingDayUtil.getProtoCurrentTradeDate(builder);
        int venueOffset = config.getExecutionVenue().asExecutionVenueFlatbuffer(builder);
        int envelopeOffset = EmsReportEnvelope.createEmsReportEnvelope(builder, EmsEnvironment.WORKSTATION,
                this.emsServerId, venueOffset, tradeDate);
        builder.finish(envelopeOffset);
        this.envelope = EmsReportEnvelope.getRootAsEmsReportEnvelope(builder.dataBuffer());
    }

    @Override
    public void subscribe(EmsCallback callback) {
        //noinspection NonAtomicOperationOnVolatileField
        this.emsCallback = callback;
    }

    // ExecutorService es = Executors.newFixedThreadPool(1);

    @Override
    public int submitNewOrder(String symbol, Order order, String clientOrderIdStuffed, boolean isOrderUsingLocates) {

        EmsReport emsReport = buildNewStatus(order, clientOrderIdStuffed);


        emsCallback.callback(envelope, emsReport);
        if (isMagicLimitPrice(order.limitPrice()) ||
                order.orderType() == OrdType.MARKET)
        {
            // respond immediately
            EmsReport filledReport = buildFill(order, clientOrderIdStuffed);
            emsCallback.callback(envelope, filledReport);
        }

        return OrderSubmitStatus.SUCCESSFUL_ORDER;
    }


    @Override
    public int cancelOrder(String symbol, long hedgeOsOrderId, int side, long submitTimeUtcMillis, String clientOrderIdStuffed, String originalClientOrderIdStuffed) {
        if (restingOrdersByClOrderId.containsKey(originalClientOrderIdStuffed)) {
            Order origOrder = restingOrdersByClOrderId.get(originalClientOrderIdStuffed);
            EmsReport cancelReport = buildCancelReport(origOrder, hedgeOsOrderId, side, clientOrderIdStuffed, originalClientOrderIdStuffed);
            emsCallback.callback(envelope, cancelReport);
        }
        else {
            log.error("No originalOrderId in InternalAdapter="+originalClientOrderIdStuffed);
        }

        return CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS;
    }


    @Override
    public int cancelReplaceOrder(CancelReplace replace, String symbol, OrderChain order, String clientOrderIdStuffed, String originalClientOrderIdStuffed, boolean isOrderUsingLocates) {
        return 0;
    }


    private boolean isMagicLimitPrice(Price limitPrice) {
        double price =QuantityConverter.priceToDouble(limitPrice);
        //noinspection ConditionCoveredByFurtherCondition
        boolean isMagic = PrecisionConstants.isEqual(price, MAGIC_FILL_PRICE);
        return isMagic;
    }

    @Override
    public void stop() { }

    @Override
    public String getName() {
        return "InternalMockFixAdapter";
    }

    @Override
    public int sendGapFill(int from, int to) {
        throw new RuntimeException("Must implement mock send gapFill");
    }
}
