package com.hedgeos.ems.service;

import java.util.concurrent.atomic.AtomicLong;

public class RequestIdMock {

    static AtomicLong requestId = new AtomicLong(100000000);
    public static long nextRequestId() {
        return requestId.incrementAndGet();
    }
}
