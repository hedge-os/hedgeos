package com.hedgeos.ems.service.connection;

import com.google.flatbuffers.FlatBufferBuilder;
import order.ConnectRequest;

public class EmsConnectionMockFactory {
    public static ConnectRequest buildConnectRequest(String login, String password,
                                                     int stickyConnId) {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int loginOffset = builder.createString(login);
        int passwordOffset = builder.createString(password);
        ConnectRequest.startConnectRequest(builder);
        ConnectRequest.addLogin(builder, loginOffset);
        ConnectRequest.addPassword(builder, passwordOffset);
        // connections request the same connectionId (sticky), when they reconnect.
        // this is not essential,
        //  UNLESS: The EMS machine has a catastrophic failure.
        //     AND:  Storage is lost (not on S3)
        ConnectRequest.addStickyConnectionIdRequest(builder, stickyConnId);
        int offset = ConnectRequest.endConnectRequest(builder);
        builder.finish(offset);
        ConnectRequest request = ConnectRequest.getRootAsConnectRequest(builder.dataBuffer());
        return request;
    }
}
