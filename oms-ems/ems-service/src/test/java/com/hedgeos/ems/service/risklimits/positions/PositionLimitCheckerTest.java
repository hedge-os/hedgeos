package com.hedgeos.ems.service.risklimits.positions;

import org.junit.jupiter.api.Test;

import static com.hedgeos.ems.util.PrecisionConstants.ULP;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PositionLimitCheckerTest {

    @Test
    public void testLimitLong() {
        System.out.println("ULP="+ULP);
        boolean isViolated = PositionLimitChecker.violatedLongLimit(100.0, 100);
        assertFalse(isViolated);
        boolean isViolatedAgain = PositionLimitChecker.violatedLongLimit(100.0001, 100);
        assertTrue(isViolatedAgain);
    }

    @Test
    public void testPrecisionLimitLong() {
        boolean isViolatedAgain =
                PositionLimitChecker.violatedLongLimit(100.0000000000001, 100);
        assertFalse(isViolatedAgain);
    }

    @Test
    public void testLimitShort() {
        boolean isViolated = PositionLimitChecker.violatedShortLimit(-100.0001, 100);
        assertTrue(isViolated);

        boolean isViolatedAgain = PositionLimitChecker.violatedShortLimit(-99.99999, 100);
        assertFalse(isViolatedAgain);
    }

    @Test
    public void testOperatorErrorLimitShort() {
        long neg100 = -100; // operator misunderstood, set negative values
        boolean isViolatedAg = PositionLimitChecker.violatedShortLimit(-100.0001, neg100);
        assertTrue(isViolatedAg);

        boolean didNotViolate = PositionLimitChecker.violatedShortLimit(-99.999999999, neg100);
        assertFalse(didNotViolate);
    }
}
