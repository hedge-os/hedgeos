package com.hedgeos.ems;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class LocksTest {


    static final ReentrantLock lock = new ReentrantLock();
    static final Condition reload = lock.newCondition();


    @BeforeAll
    public static void setupTestLogging() {

    }

    @SneakyThrows
//    @Test
    public void test() {

        // logger.warn("Hello IntelliJ logging");
        Chris chris = new Chris();
        Thread t = new Thread(chris);
        t.start();
        lock.lock();
        reload.signalAll();
        lock.unlock();

        Thread.sleep(1000);
        lock.lock();
        reload.signalAll();
        lock.unlock();

    }

    public static class Chris implements  Runnable {
        @Override
        public void run() {
            System.out.println("Here");
            log.error("HERE");
            int i=0;
            while (true) {
                System.out.println("Locking: "+i);
                lock.lock();
                try {
                    System.out.println("awaiting: "+i);

                    reload.await();
                    log.warn("Fired await at="+System.currentTimeMillis());
                    System.out.println("NOTIFIED: "+i);
                    lock.unlock();
                    if (i++ >= 2) {
                        System.out.println("made it="+i);
                        return;
                    } else {
                        System.out.println("Not > 2: "+i);
                    }
                } catch (InterruptedException e) {
                    System.out.println("ERROR: "+e);
                    throw new RuntimeException(e);
                }
            }

            // System.out.println("Exiting");
        }
    }

    public static void main(String[] args) {
        LocksTest lockTest = new LocksTest();
        Thread t = new Thread(new Chris());
        t.start();


        try {
            Thread.sleep(1000);
            System.out.println("Locking again =1");

            lock.lock();
            reload.signalAll();
            lock.unlock();

            System.out.println("Locking again 2");
            Thread.sleep(100);
            lock.lock();
            reload.signalAll();
            lock.unlock();

            System.out.println("Locking again 3");

            Thread.sleep(100);
            lock.lock();
            reload.signalAll();
            lock.unlock();

        }
        catch (Exception e) {
            System.err.println("Exception: "+e);
            log.error("Exception", e);
        }
    }
}
