package com.hedgeos.ems.service;


import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.ems.order.OrderIdGenerator;
import com.hedgeos.ems.util.PrecisionConstants;
import com.hedgeos.ems.util.QuantityConverter;
import com.hedgeos.fix.adapter.FixAdapter;
import com.hedgeos.fix.adapter.FixAdapterFactory;
import com.hedgeos.test.IntegrationConstants;
import com.hedgeos.test.TestConstants;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import order.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import static com.hedgeos.ems.service.RequestIdMock.nextRequestId;
import static com.hedgeos.test.TestConstants.POSITION_LONG_SECURITY_ID;
import static org.junit.jupiter.api.Assertions.*;

/**
 * This test loads the config, but does not boot up a fix connection,
 * and allows entry directly into the EmsMultiplexer; which is where
 * most of the important coordination happens.
 *
 * By adding a FixAdapter, the unit tests run without leaving the JVM,
 * allowing you to debug the system easily in one JVM.
 *
 * As well, it spins up fresh; avoid situations where messages from the MockExchange
 * are left over from the last test; you get a fresh start every run of this.
 * -----------------------------------------
 * Uses these two Spring Boot tricks, combined
 * -----------------------------------------
 * Not well documented in Spring/Spring Boot.
 * First:
 * @AcitiveProfiles set the profile to be "test"
 *
 * Second: in application.yml in ems-service, we "allow-bean-definition-overriding"
 *  as part of the "test" profile.
 * # Snippet of application.yml
 * # For the tests, we need to replace beans; never set this in prod
 * spring:
 *   application:
 *     name: ems-service
 *   config:
 *     activate:
 *       on-profile: test
 *   main:
 *     allow-bean-definition-overriding: true
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties =
        {"test.ems.config.folder=../ems-config-mock-files/",
                "test.ems.config.file=active_mock_fix_connections.yml",
                "grpc.port=12124" // random port to see if we can load the system
        })
@ActiveProfiles("test")
public class EmsMultiplexerTest {

//    @Autowired
//    ApplicationContext context;

    private final EmsMultiplexer multiplexer;
    private final FixAdapter fixAdapter;

    private static final BlockingDeque<EmsReport> messages = new LinkedBlockingDeque<>();
    private final StreamObserver<EmsReport> myObserver = new StreamObserver<>() {
        @Override
        public void onNext(EmsReport value) {
            messages.add(value);
        }

        @Override
        public void onError(Throwable t) {

        }

        @Override
        public void onCompleted() {

        }
    };

    private Connection conn = fakeConnection();

    public EmsMultiplexerTest(@Autowired  ApplicationContext context) {
        multiplexer = context.getBean(EmsMultiplexer.class);
        fixAdapter = context.getBean(FixAdapter.class);

        List<StreamObserver<EmsReport>> streamObserverList = new ArrayList<>();
        streamObserverList.add(myObserver);
        multiplexer.connectionMap.put(conn.connectionId(), streamObserverList);
        // no need to subscribe
        // fixAdapter.subscribe(callback);
    }

    private final OrderIdGenerator orderIdGenerator = new OrderIdGenerator();

    // attempt to make a fake FixAdapter, to respond, not
    // the mock Exchange
    @TestConfiguration
    static class FixAdapterTextConfiguration {
        @Bean(FixAdapterFactory.QUICKFIX_ADAPTER_EMS)
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public FixAdapter fixAdapter() {
            return new InternalMockFixAdapter();
        }
    }

    @SneakyThrows
    @Test
    public void testLongShortRepeated() {
        // position file contents to setup this test
        // # long position 100 tests
        // 9223372036854775803,CMR,100,12.34

        OrderResponse tooHighStatus = limitRequest(POSITION_LONG_SECURITY_ID, 101, Side.SELL);
        // position is 100; we can't trade, no locates in the file for this name
        // (but a locate file exists for the Venue, thus the specific error code.)
        assertEquals(OrderSubmitStatus.LOCATES_ABSENT_FROM_FILE, tooHighStatus.status());
        // no need to clean up; it never made it
        assertTrue(messages.isEmpty());

        // we CAN sell 100; we own 100
        OrderResponse limitSuccess = limitRequest(POSITION_LONG_SECURITY_ID, 100, Side.SELL);
        assertEquals(OrderSubmitStatus.SUCCESSFUL_ORDER, limitSuccess.status());
        EmsReport limitNewReport = messages.take();
        assertEquals(limitSuccess.hedgeosOrderId(), limitNewReport.hedgeOsOrderId());
        assertEquals(OrderStatus.NEW_ORDER_STATUS, limitNewReport.orderStatus());
        assertTrue(messages.isEmpty());

//        // we FAIL selling 1; we sold our position of 100 which was start of day
        OrderResponse cantSell = limitRequest(POSITION_LONG_SECURITY_ID, 1, Side.SELL);
        assertEquals(OrderSubmitStatus.LOCATES_ABSENT_FROM_FILE, cantSell.status());
        // no need to clean up, it never made it
        assertTrue(messages.isEmpty());

        //        // we buy 100
        OrderResponse response = orderIsFilled(POSITION_LONG_SECURITY_ID, 100, Side.BUY);
        assertEquals(OrderSubmitStatus.SUCCESSFUL_ORDER, response.status());

        // wait for the report
        EmsReport buyNewReport = messages.take();
        assertEquals(response.hedgeosOrderId(), buyNewReport.hedgeOsOrderId());
        assertNotNull(buyNewReport, "Missing EmsReport of buy in NEW state.");
        assertEquals(OrderStatus.NEW_ORDER_STATUS, buyNewReport.orderStatus());

        // wait for another report
        EmsReport buyFilledReport = messages.take();
        assertEquals(response.hedgeosOrderId(), buyFilledReport.hedgeOsOrderId());
        assertNotNull(buyFilledReport, "Missing EmsReport of filled buy.");
        assertEquals(OrderStatus.FILLED_ORDER_STATUS, buyFilledReport.orderStatus());
        double cumQty = QuantityConverter.quantityToDouble(buyFilledReport.cumulativeQty());
        assertTrue(PrecisionConstants.isEqual(100.0, cumQty));

        // TODO:  Continue here, fix the PositionLongShort test, then
        // fix it in the actual integration test.
        OrderResponse failSellAgain =
                limitRequest(POSITION_LONG_SECURITY_ID, 101, Side.SELL);
        assertEquals(OrderSubmitStatus.LOCATES_ABSENT_FROM_FILE, failSellAgain.status());
        assertTrue(messages.isEmpty());


        OrderResponse sellExpectFilled =
                orderIsFilled(POSITION_LONG_SECURITY_ID, 100, Side.SELL);
        assertEquals(OrderSubmitStatus.SUCCESSFUL_ORDER, sellExpectFilled.status());
        // wait for the report
        EmsReport sellFilledNew = messages.take();
        assertNotNull(sellFilledNew, "Missing EmsReport of buy in NEW state.");
        // wait for another report
        EmsReport sellFilledDone = messages.take();
        assertNotNull(sellFilledDone, "Missing EmsReport of filled buy.");
        assertTrue(messages.isEmpty());

        cancelOrder(limitSuccess);
        assertTrue(messages.isEmpty());
    }

    private void cancelOrder(OrderResponse limitSuccess) {
        CancelRequest request =
                EmsRequestBuilder.createCancelRequest(conn, nextRequestId(), nextOrderId(), limitSuccess.hedgeosOrderId());
        CancelResponse response = multiplexer.cancelOrder(request.cancels(0), conn);
        assertEquals(CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS, response.status());
    }

    private OrderResponse orderIsFilled(long sid, int qty, int side) {
        // we cant sell 101; we only own 100; and no locates
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(conn,
                        nextRequestId(),
                        TestConstants.PM_GROUP_CODE,
                        TestConstants.ACCOUNT_1,
                        nextOrderId(),
                        qty,
                        side,
                        TestConstants.MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        sid,
                        IntegrationConstants.MAGIC_FILL_PRICE);

        // sellFailExpected(POSITION_LONG_SECURITY_ID, 101);
        OrderResponse orderPlaced = multiplexer.submitOrder(request.orders(0), conn);
        return orderPlaced;
    }


    private long nextOrderId() {
        long hedgeOsOrderId = orderIdGenerator.getNextHedgeOsOrderId(conn);
        return hedgeOsOrderId;
    }


    private OrderResponse limitRequest(long sid, long quantity, int side) {
        // we cant sell 101; we only own 100; and no locates
        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(conn,
                        nextRequestId(),
                        TestConstants.PM_GROUP_CODE,
                        TestConstants.ACCOUNT_1,
                        nextOrderId(),
                        quantity,
                        side,
                        TestConstants.MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        sid,
                        IntegrationConstants.RESTS_EITHER_BUY_OR_SELL);

        // sellFailExpected(POSITION_LONG_SECURITY_ID, 101);
        OrderResponse tooHighFailResponse = multiplexer.submitOrder(request.orders(0), conn);
        return tooHighFailResponse;
    }


    private static Connection fakeConnection() {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int connOffset =
                Connection.createConnection(builder, 11, 55, 77,
                        ConnectionStatus.SUCCESS, ConnectionStatusReason.SUCCESS_REASON);
        builder.finish(connOffset);
        Connection conn = Connection.getRootAsConnection(builder.dataBuffer());
        return conn;
    }

}
