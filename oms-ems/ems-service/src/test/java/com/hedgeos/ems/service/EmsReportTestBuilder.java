package com.hedgeos.ems.service;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.fix.adapter.ClientOrderIdStuffer;
import general.Quantity;
import order.EmsReport;
import order.Order;
import order.OrderStatus;
import order.EmsExecType;

public class EmsReportTestBuilder {


    public static EmsReport buildCancelReport(
            Order orgiginalOrder,
            long hedgeOsOrderId,
            int side,
            String clientOrderIdStuffed,
            String originalClientOrderIdStuffed) {

        // note: using originalClOrdId here
        long emsServerId = ClientOrderIdStuffer.emsServerIdFromClOrdId(originalClientOrderIdStuffed);
        long hedgeOsConnectionId = ClientOrderIdStuffer.connectionIdFromClOrdId(originalClientOrderIdStuffed);
        String pmGroup = ClientOrderIdStuffer.pmGroupIdFromClOrdId(originalClientOrderIdStuffed);

        FlatBufferBuilder builder = new FlatBufferBuilder();
        // add the pmGroup to the builder
        int pmGroupOffset = builder.createString(pmGroup);

        int orderQtyOffset = Quantity.createQuantity(builder, orgiginalOrder.quantity().quantity(), orgiginalOrder.quantity().nanoQty());
        int zeroQtyOffset = Quantity.createQuantity(builder, 0L, 0);

        // start making the ExecutionReport flatbuffer to respond with to the client
        // note: Must start the execution report LAST after other objects added
        // to the flatbuffer (quantity, price etc, compound objects.)
        EmsReport.startEmsReport(builder);
        EmsReport.addHedgeOsEmsServerId(builder, (int) emsServerId);
        EmsReport.addHedgeOsConnectionId(builder, (int) hedgeOsConnectionId);
        EmsReport.addPmGroup(builder, pmGroupOffset);
        EmsReport.addSid(builder, orgiginalOrder.sid());
        EmsReport.addHedgeOsOrderId(builder, hedgeOsOrderId);
        EmsReport.addHedgeOsOriginalOrderId(builder, orgiginalOrder.hedgeOsOrderId());
        EmsReport.addSide(builder, orgiginalOrder.side());
        EmsReport.addOrderStatus(builder, OrderStatus.FILLED_ORDER_STATUS);
        EmsReport.addEmsExecType(builder, EmsExecType.TRADE_EXEC_TYPE);
        EmsReport.addTransactTime(builder, System.currentTimeMillis());
        EmsReport.addOrderQty(builder, orderQtyOffset);
        // cum and last qty of the entire order
        EmsReport.addCumulativeQty(builder, zeroQtyOffset);
        EmsReport.addLeavesQty(builder, zeroQtyOffset);
        EmsReport.addLastQty(builder, zeroQtyOffset);
        int offset = EmsReport.endEmsReport(builder);
        builder.finish(offset);
        EmsReport report = EmsReport.getRootAsEmsReport(builder.dataBuffer());
        return report;
    }

    public static EmsReport buildFill(Order order, String clientOrderIdStuffed) {
        // send a new message to callback
        long emsServerId = ClientOrderIdStuffer.emsServerIdFromClOrdId(clientOrderIdStuffed);
        // critical for routing, the ems connection to return the message
        long hedgeOsConnectionId = ClientOrderIdStuffer.connectionIdFromClOrdId(clientOrderIdStuffed);

        // needed to match our orderId tracking for positions, risk.
        long hedgeOsOrderId = ClientOrderIdStuffer.orderIdFromClOrdID(clientOrderIdStuffed);

        String pmGroup = ClientOrderIdStuffer.pmGroupIdFromClOrdId(clientOrderIdStuffed);

        FlatBufferBuilder builder = new FlatBufferBuilder();
        // add the pmGroup to the builder
        int pmGroupOffset = builder.createString(pmGroup);

        int orderQtyOffset = Quantity.createQuantity(builder, order.quantity().quantity(), order.quantity().nanoQty());
        int cumQtyOffset = Quantity.createQuantity(builder, order.quantity().quantity(), order.quantity().nanoQty());
        int lastQtyOffset = Quantity.createQuantity(builder, order.quantity().quantity(), order.quantity().nanoQty());

        // start making the ExecutionReport flatbuffer to respond with to the client
        // note: Must start the execution report LAST after other objects added
        // to the flatbuffer (quantity, price etc, compound objects.)
        EmsReport.startEmsReport(builder);
        EmsReport.addHedgeOsEmsServerId(builder, (int) emsServerId);
        EmsReport.addHedgeOsConnectionId(builder, (int) hedgeOsConnectionId);
        EmsReport.addPmGroup(builder, pmGroupOffset);
        EmsReport.addSid(builder, order.sid());
        EmsReport.addHedgeOsOrderId(builder, hedgeOsOrderId);
        EmsReport.addSide(builder, order.side());
        EmsReport.addOrderStatus(builder, OrderStatus.FILLED_ORDER_STATUS);
        EmsReport.addEmsExecType(builder, EmsExecType.TRADE_EXEC_TYPE);
        EmsReport.addTransactTime(builder, System.currentTimeMillis());
        EmsReport.addOrderQty(builder, orderQtyOffset);
        // cum and last qty of the entire order
        EmsReport.addCumulativeQty(builder, cumQtyOffset);
        EmsReport.addLastQty(builder, lastQtyOffset);
        int offset = EmsReport.endEmsReport(builder);
        builder.finish(offset);
        EmsReport report = EmsReport.getRootAsEmsReport(builder.dataBuffer());
        return report;
    }

    public static EmsReport buildNewStatus(Order order, String clientOrderIdStuffed) {
        // send a new message to callback
        long emsServerId = ClientOrderIdStuffer.emsServerIdFromClOrdId(clientOrderIdStuffed);
        // critical for routing, the ems connection to return the message
        long hedgeOsConnectionId = ClientOrderIdStuffer.connectionIdFromClOrdId(clientOrderIdStuffed);

        // needed to match our orderId tracking for positions, risk.
        long hedgeOsOrderId = ClientOrderIdStuffer.orderIdFromClOrdID(clientOrderIdStuffed);

        String pmGroup = ClientOrderIdStuffer.pmGroupIdFromClOrdId(clientOrderIdStuffed);

        FlatBufferBuilder builder = new FlatBufferBuilder();
        // add the pmGroup to the builder
        int pmGroupOffset = builder.createString(pmGroup);

        int orderQtyOffset = Quantity.createQuantity(builder, order.quantity().quantity(), order.quantity().nanoQty());

        // start making the ExecutionReport flatbuffer to respond with to the client
        // note: Must start the execution report LAST after other objects added
        // to the flatbuffer (quantity, price etc, compound objects.)
        EmsReport.startEmsReport(builder);
        EmsReport.addHedgeOsOrderId(builder, hedgeOsOrderId);
        EmsReport.addOrderStatus(builder, OrderStatus.NEW_ORDER_STATUS);
        EmsReport.addHedgeOsEmsServerId(builder, (int) emsServerId);
        EmsReport.addHedgeOsConnectionId(builder, (int) hedgeOsConnectionId);
        EmsReport.addPmGroup(builder, pmGroupOffset);
        EmsReport.addSid(builder, order.sid());
        EmsReport.addSide(builder, order.side());
        EmsReport.addTransactTime(builder, System.currentTimeMillis());
        EmsReport.addOrderQty(builder, orderQtyOffset);
        int offset = EmsReport.endEmsReport(builder);
        builder.finish(offset);
        EmsReport report = EmsReport.getRootAsEmsReport(builder.dataBuffer());
        return report;
    }
}
