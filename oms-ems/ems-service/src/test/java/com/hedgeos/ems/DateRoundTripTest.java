package com.hedgeos.ems;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.util.TradingDayUtil;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static com.hedgeos.ems.util.TradingDayUtil.googleDateToLocalDate;
import static java.time.ZoneOffset.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DateRoundTripTest {


    @Test
    public void testDateRoundTrip() {
        LocalDate localDate = TradingDayUtil.getTradeDate();
        FlatBufferBuilder builder = new FlatBufferBuilder();
        google.type.Date.startDate(builder);
        google.type.Date.addYear(builder, localDate.getYear());
        google.type.Date.addMonth(builder, localDate.getMonthValue());
        google.type.Date.addDay(builder, localDate.getDayOfMonth());
        int orderDateOffset = google.type.Date.endDate(builder);
        builder.finish(orderDateOffset);
        google.type.Date date = google.type.Date.getRootAsDate(builder.dataBuffer());

        LocalDate localDateBack = googleDateToLocalDate(date);

        assertEquals(localDate.getDayOfMonth(), localDateBack.getDayOfMonth());
        assertEquals(localDate.getMonthValue(), localDateBack.getMonthValue());
        assertEquals(localDate.getYear(), localDateBack.getYear());
    }
}
