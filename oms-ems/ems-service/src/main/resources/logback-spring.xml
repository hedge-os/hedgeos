<?xml version="1.0" encoding="UTF-8"?>
<configuration debug="false"
               scan="true" scanPeriod="10 seconds">

    <import class="ch.qos.logback.classic.encoder.PatternLayoutEncoder"/>
    <import class="ch.qos.logback.core.rolling.RollingFileAppender"/>
    <import class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy"/>
    <import class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy"/>

    <!-- use Spring default values -->
    <include resource="org/springframework/boot/logging/logback/defaults.xml"/>
    <include resource="org/springframework/boot/logging/logback/console-appender.xml" />

    <property name="LOGS" value="./logs" />
    <property name="US_STOCK_FIX_LOGS" value="./logs/fix/us-stock-mock-ems-service-fixlogs" />

    <property name="us.stock.senderCompID" value="SENDER"/>
    <property name="us.stock.targetCompID" value="VENUE"/>


    <appender name="Async-AGGREGATE_ROLLINGFILE" class="ch.qos.logback.classic.AsyncAppender">
        <appender-ref ref="AGGREGATE_ROLLINGFILE"/>
    </appender>

    <appender name="AGGREGATE_ROLLINGFILE" class="RollingFileAppender">
        <file>${US_STOCK_FIX_LOGS}/${us.stock.senderCompID}.${us.stock.targetCompID}.agg</file>
        <!-- daily rollover -->
        <fileNamePattern>${US_STOCK_FIX_LOGS}/%d{yyyy-MM-dd}.${us.stock.senderCompID}.${us.stock.targetCompID}.agg</fileNamePattern>

        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- daily rollover -->
            <fileNamePattern>${US_STOCK_FIX_LOGS}/%d{yyyy-MM-dd}.${us.stock.senderCompID}.${us.stock.targetCompID}.agg</fileNamePattern>
            <maxHistory>60</maxHistory>
            <totalSizeCap>3GB</totalSizeCap>
        </rollingPolicy>

        <encoder class="PatternLayoutEncoder">
            <pattern>%d{"yyyy-MM-dd'T'HH:mm:ss,SSSXXX", UTC}|%msg%n</pattern>
        </encoder>
    </appender>

    <appender name="Async-Events_ROLLINGFILE" class="ch.qos.logback.classic.AsyncAppender">
        <appender-ref ref="Events_ROLLINGFILE"/>
    </appender>

    <appender name="Events_ROLLINGFILE" class="RollingFileAppender">
        <file>${US_STOCK_FIX_LOGS}/${us.stock.senderCompID}.${us.stock.targetCompID}.events</file>
        <!-- daily rollover -->
        <fileNamePattern>${US_STOCK_FIX_LOGS}/%d{yyyy-MM-dd}.${us.stock.senderCompID}.${us.stock.targetCompID}.events</fileNamePattern>

        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- daily rollover -->
            <fileNamePattern>${US_STOCK_FIX_LOGS}/%d{yyyy-MM-dd}.${us.stock.senderCompID}.${us.stock.targetCompID}.events</fileNamePattern>
            <maxHistory>60</maxHistory>
            <totalSizeCap>3GB</totalSizeCap>
        </rollingPolicy>

        <encoder class="PatternLayoutEncoder">
            <pattern>%d{"yyyy-MM-dd'T'HH:mm:ss,SSSXXX", UTC}|%msg%n</pattern>
        </encoder>
    </appender>

    <appender name="Async-Incoming_ROLLINGFILE" class="ch.qos.logback.classic.AsyncAppender">
        <appender-ref ref="Incoming_ROLLINGFILE"/>
    </appender>

    <appender name="Incoming_ROLLINGFILE"
              class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${US_STOCK_FIX_LOGS}/${us.stock.senderCompID}.${us.stock.targetCompID}.incoming</file>

        <!-- daily rollover -->
        <fileNamePattern>${US_STOCK_FIX_LOGS}/%d{yyyy-MM-dd}.${us.stock.senderCompID}.${us.stock.targetCompID}.incoming</fileNamePattern>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- daily rollover -->
            <fileNamePattern>${US_STOCK_FIX_LOGS}/%d{yyyy-MM-dd}.${us.stock.senderCompID}.${us.stock.targetCompID}.incoming</fileNamePattern>
            <maxHistory>60</maxHistory>
            <totalSizeCap>3GB</totalSizeCap>
        </rollingPolicy>

        <encoder>
            <pattern>%d{"yyyy-MM-dd'T'HH:mm:ss,SSSXXX", UTC}|%msg%n</pattern>
        </encoder>
    </appender>

    <appender name="Async-Outgoing_ROLLINGFILE" class="ch.qos.logback.classic.AsyncAppender">
        <appender-ref ref="Outgoing_ROLLINGFILE"/>
    </appender>

    <appender name="Outgoing_ROLLINGFILE"
              class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${US_STOCK_FIX_LOGS}/${us.stock.senderCompID}.${us.stock.targetCompID}.outgoing</file>
        <!-- daily rollover -->
        <fileNamePattern>${US_STOCK_FIX_LOGS}/%d{yyyy-MM-dd}.${us.stock.senderCompID}.${us.stock.targetCompID}.outgoing</fileNamePattern>

        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- daily rollover -->
            <fileNamePattern>${US_STOCK_FIX_LOGS}/%d{yyyy-MM-dd}.${us.stock.senderCompID}.${us.stock.targetCompID}.outgoing</fileNamePattern>
            <maxHistory>60</maxHistory>
            <totalSizeCap>3GB</totalSizeCap>
        </rollingPolicy>

        <encoder>
            <pattern>%d{"yyyy-MM-dd'T'HH:mm:ss,SSSXXX", UTC}|%msg%n</pattern>
        </encoder>
    </appender>

    <!-- TODO:  Async or not on these FIX loggers; debate, prefer async
        reason being; we can always get the tape from the bank, and the exchange -->

    <!-- The 'name' attribute here MUST match the value of the config file's SLF4JLogEventCategory setting! -->
    <logger name="quickfixj.events.us.stock" level="info" additivity="false">
        <appender-ref ref="Async-Events_ROLLINGFILE"/>
        <appender-ref ref="Async-AGGREGATE_ROLLINGFILE"/>
    </logger>

    <!-- The 'name' attribute here MUST match the value of the config file's SLF4JLogIncomingMessageCategory setting! -->
    <logger name="quickfixj.msg.incoming.us.stock" level="info" additivity="false">
        <appender-ref ref="Async-Incoming_ROLLINGFILE"/>
        <appender-ref ref="Async-AGGREGATE_ROLLINGFILE"/>
    </logger>

    <!-- The 'name' attribute here MUST match the value of the config file's SLF4JLogOutgoingMessageCategory setting! -->
    <logger name="quickfixj.msg.outgoing.us.stock" level="info" additivity="false">
        <appender-ref ref="Async-Outgoing_ROLLINGFILE"/>
        <appender-ref ref="Async-AGGREGATE_ROLLINGFILE"/>
    </logger>


    <appender name="service-RollingFile" class="RollingFileAppender">
        <file>${LOGS}/ems-service.log</file>

        <encoder class="PatternLayoutEncoder">
            <pattern>%-4relative [%thread] %-5level %logger{35} -%kvp- %msg%n</pattern>
        </encoder>

<!--        <encoder class="PatternLayoutEncoder">-->
<!--            <Pattern>%d %p %C{1.} [%t] %m%n</Pattern>-->
<!--        </encoder>-->

        <fileNamePattern>${LOGS}/archived/ems-service-%d{yyyy-MM-dd}.%i.log</fileNamePattern>

        <rollingPolicy class="SizeAndTimeBasedRollingPolicy">
            <!-- rollover daily and when the file reaches 10 MegaBytes -->
            <fileNamePattern>${LOGS}/archived/ems-service-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
            <maxFileSize>100MB</maxFileSize>
            <maxHistory>30</maxHistory>
            <totalSizeCap>3GB</totalSizeCap>
        </rollingPolicy>
    </appender>

    <!-- TODO:  Maybe I have this appender.. in a utils project -->
    
    <!-- TODO: wrap the Email Appender to prevent spamming -->
    <appender name="EMAIL" class="ch.qos.logback.classic.net.SMTPAppender">
        <smtpHost>ADDRESS-OF-YOUR-SMTP-HOST</smtpHost>
        <to>EMAIL-DESTINATION</to>
        <to>ANOTHER_EMAIL_DESTINATION</to> <!-- additional destinations are possible -->
        <from>SENDER-EMAIL</from>
        <subject>TESTING: %logger{20} - %m</subject>
        <layout class="ch.qos.logback.classic.PatternLayout">
            <pattern>%date %-5level %logger{35} - %message%n%throwable</pattern>
        </layout>
    </appender>

    <appender name="service-Async-Appender" class="ch.qos.logback.classic.AsyncAppender">
        <appender-ref ref="service-RollingFile"/>
    </appender>

    <!-- LOG "com.hedgeos*" at INFO level -->
    <logger name="com.hedgeos" level="INFO" additivity="false">
        <appender-ref ref="service-Async-Appender" />
        <appender-ref ref="CONSOLE"/>
    </logger>

    <!-- LOG everything at INFO level -->
    <root level="WARN">
        <appender-ref ref="service-Async-Appender" />
        <appender-ref ref="CONSOLE"/>
    </root>


</configuration>