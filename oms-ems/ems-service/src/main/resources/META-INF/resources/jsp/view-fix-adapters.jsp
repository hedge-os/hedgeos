<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.hedgeos.ems.service.web.GapFillController" %>
<%@ page import="com.hedgeos.fix.adapter.FixAdapter" %>
<%@ page import="static com.hedgeos.ems.service.web.GapFillController.EMS_MULTIPLEXER" %>
<%@ page import="static com.hedgeos.ems.service.web.GapFillController.FIX_ADAPTER_MAP" %>
<%@ page import="static com.hedgeos.ems.service.web.GapFillController.*" %>

<html>
<head>
    <title>View FIX Adapters</title>
    <link rel="stylesheet" href="/styles.css" />
</head>
<body>
<h3>Fix Adapters as of <% out.print(new Date());%></h3>

GapFill can be used to replay messages; reserved for emergencies.
<h2>
<%
    String result = (String) request.getAttribute(GapFillController.STATUS);
    if (result != null && result.equals(GapFillController.GAP_FILL_SENT)) {
        out.println("Gap Fill sent.  Result=<b>"+result+"</b>");
    }
%>
</h2>

<hr/>

<div id="errorMessage" class="error-message-fix-page" style="display: none;"></div>

<table>
    <tr>
        <td>FIX Adapter Name</td>
        <td>GapFill:  from</td>
        <td>GapFill:  to</td>
        <td>Submit Gap Fill</td>
    </tr>

<%
    Map<String, FixAdapter> fixAdapterMap =
            (Map<String, FixAdapter>) request.getAttribute(FIX_ADAPTER_MAP);
    for (Map.Entry<String, FixAdapter> entry : fixAdapterMap.entrySet()) {
        out.println("<tr>");
            out.println("<td>" +
                    "<span id=\"adapterName\" >"+entry.getValue().getName()+"</span></td>");
            out.println("<td><input type=\"text\" name=\"from\" /></td>");
            out.println("<td><input type=\"text\" name=\"to\"/></td>");
            out.println("<td><button type=\"button\" onclick=\"submitForm(this)\">Send GapFill</button></td>");
        out.println("</tr>");
    }
%>
</table>

<script>
    function submitForm(button) {
        var row = button.parentNode.parentNode; // Get the parent <tr> element
        var adapterName = row.querySelector('#adapterName').textContent;
        var fromValue = row.querySelector('input[name="from"]').value;
        var toValue = row.querySelector('input[name="to"]').value;

        // Do something with the field values or submit the form
        console.log("Gap Fill Triggered")
        console.log('fixAdapter name:', adapterName);
        console.log('from field value:', fromValue);
        console.log('to field value:', toValue);

        if (!adapterName || !fromValue || !toValue) {
            errorMessage.textContent = "Please fill in all required fields.";
            errorMessage.style.display = "block";
            return; // Stop form submission
        }

        if (toValue < fromValue) {
            errorMessage.textContent = "'from' must be less or equal 'to'.";
            errorMessage.style.display = "block";
            return; // Stop form submission
        }

        var url = '/fix/gapFill';

        // Append the form data to the URL
        url += '?<% out.print(FIX_ADAPTER_NAME_KEY); %>=' + encodeURIComponent(adapterName);
        url += '&<% out.print(FIX_ADAPTER_GAP_FILL_FROM_KEY); %>=' + encodeURIComponent(fromValue);
        url += '&<% out.print(FIX_ADAPTER_GAP_FILL_TO_KEY); %>=' + encodeURIComponent(toValue);

        // Redirect the browser to the specified URL
        window.location.href = url;
    }
</script>

</body>
</html>
<%!

%>