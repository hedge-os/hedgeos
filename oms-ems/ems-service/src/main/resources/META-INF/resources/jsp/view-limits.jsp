<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import="com.hedgeos.ems.order.PmGroupSidKey" %>
<%@ page import="com.hedgeos.ems.service.risklimits.trades.TradeAmountTracker" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.hedgeos.ems.service.web.LimitsController" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.apache.catalina.Manager" %>
<%@ page import="com.hedgeos.ems.service.risklimits.trades.TradeLimitsManager" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.io.IOException" %>

<html>
<head>
    <title>View Limits 2</title>
    <link rel="stylesheet" href="/styles.css" />
</head>
<body>
<h3>Limits:  Amounts traded per period as of <% out.print(new Date());%></h3>

In an emergency, set the traded amounts to zero for all securities:
<h2>
<%
    String result = (String) request.getAttribute(LimitsController.STATUS);
    if (result != null && result.equals(LimitsController.FULL_CLEAR)) {
        out.println("All traded amounts set to zero.");
    }
    else if (result != null && result.equals(LimitsController.ONE_SECURITY)) {
        out.println("Cleared one security");
    }
%>
</h2>

<form method="get"
      action="${pageContext.request.contextPath}/limits/clearAmounts">
    <input class="button-6"
           type="submit"
           value="Emergency Clear ALL Amounts Traded"/>
</form>

<hr/>
<table>
    <tr>
        <td>pmGroup</td>
        <td>sid</td>
        <td>SECOND</td>
        <td>5 SECOND</td>
        <td>1 MINUTE</td>
        <td>5 MINUTE</td>
        <td>1 HOUR</td>
        <td>DAY</td>
        <td>CLEAR LIMITS</td>
    </tr>

<%
    TradeAmountTracker manager =
            (TradeAmountTracker)
                    request.getAttribute(LimitsController.TRADE_TRACKER);

    Map<PmGroupSidKey, TradeAmountTracker.TradeActivityBlocks> limitsMap =
            (Map<PmGroupSidKey, TradeAmountTracker.TradeActivityBlocks>)
                    request.getAttribute(LimitsController.LIMITS_CONTEXT);

    // print my key out first
    PmGroupSidKey ewingKey = new PmGroupSidKey(33, "CMR");
    printOneKey(out, manager, request, ewingKey);

    // rest of the portfolio
    for (PmGroupSidKey pmGroupSidKey : limitsMap.keySet()) {
        printOneKey(out, manager, request, pmGroupSidKey);
    }
%>
</table>



</body>
</html>
<%!
    private void printOneKey(JspWriter out, TradeAmountTracker manager, HttpServletRequest request, PmGroupSidKey pmGroupSidKey) throws IOException {
        out.print("<tr>");
        out.print("<td>"+pmGroupSidKey.pmGroup+"</td>" +
                "<td>"+pmGroupSidKey.sid+"</td>");

        Collection<Integer> limits = manager.getAllLimits(pmGroupSidKey, System.currentTimeMillis());
        for (Integer limit : limits) {
            out.print("<td>"+limit+"</td>");
        }
        out.println("<td>" +
                "<a href=\""+request.getContextPath()+
                "/limits/clearOneSecurity?sid="+pmGroupSidKey.sid+
                "&pmGroup="+pmGroupSidKey.pmGroup+
                "\">CLEAR</a>" +
                "<td>");

        out.println("</tr>");
    }
%>