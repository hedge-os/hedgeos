package com.hedgeos.grpc.utils;

import io.netty.channel.epoll.Epoll;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;

@Slf4j
@Component
public class NettyEpollNativeLoader {
    public boolean loadNettyEpoll() {
        try {
            // TODO:  Add libnetty_transport_native_epoll_x86_64.so to PATH
            File nettyEpoll = new File("./libnetty_transport_native_epoll_x86_64.so");

            String epollLibMsg = "Epoll library location: "+nettyEpoll;
            System.out.println(epollLibMsg);
            log.warn(epollLibMsg);
            // System.load is easier than java.library.path
            System.load(nettyEpoll.getAbsolutePath());
//
            // System.load("/home/crodier/coding/flatbuffers-grpc-java-example/grpc-server/libnetty_transport_native_epoll_x86_64.so");
//            File workingDir2 = new File("./libnetty_transport_native_epoll.so");
//            System.load(workingDir2.getAbsolutePath());

            boolean isEpollAvailNow = Epoll.isAvailable();
            if (isEpollAvailNow == false) {
                Epoll.unavailabilityCause().printStackTrace();
                throw new Exception(Epoll.unavailabilityCause());
                // System.err.println("No EPOLL, need the native transport for Netty performance.");
            }

            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            // System.exit(-1);
            log.error("Failed to load Netty EPOLL", e);
            return false;
        }

    }
}
