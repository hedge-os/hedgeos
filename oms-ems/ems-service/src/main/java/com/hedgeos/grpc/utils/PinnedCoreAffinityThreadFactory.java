package com.hedgeos.grpc.utils;

import lombok.extern.slf4j.Slf4j;
import net.openhft.affinity.AffinityLock;

import java.util.concurrent.ThreadFactory;

@Slf4j
public class PinnedCoreAffinityThreadFactory implements ThreadFactory {

    private final int[] cpus;
    String name = "pinned_";
    private final int core;

    public PinnedCoreAffinityThreadFactory(String name, int core) {
        this.name = name;
        this.core = core;
        this.cpus = new int[]{core};
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(() -> {
            try (AffinityLock lock = AffinityLock.acquireLock(cpus)) {
                try {
                    r.run();
                } catch (Throwable th) {
                    log.error("VERY BAD:  throwable in the run method of Netty", th);
                }
            }
        }, name);
        thread.setDaemon(true);
        return thread;
    }
}
