package com.hedgeos.ems.loader;

import com.hedgeos.ems.config.beans.CompleteEmsConfig;
import com.hedgeos.ems.config.beans.EmsVenueMap;
import com.hedgeos.ems.csvfiles.locates.LocatesLoader;
import com.hedgeos.ems.csvfiles.locates.LocatesMapAndLatestTimestamp;
import com.hedgeos.ems.service.EmsMultiplexer;
import com.hedgeos.ems.service.locates.LocatesManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.datetime.standard.DateTimeFormatterFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.hedgeos.ems.csvfiles.locates.LocatesLoader.LOCATES_FILE_PREFIX;
import static com.hedgeos.ems.csvfiles.util.FlatFileUtil.*;

@RequiredArgsConstructor
@Component
@Slf4j
public class LocatesReloader {
    public static final String EMS_FAIL_WITHOUT_LOCATES = "EMS_FAIL_WITHOUT_LOCATES";

    // everyone calls back to the Multiplexer; files are reloaded to it
    final EmsMultiplexer multiplexer;
    final LocatesManager locatesManager;

    public Reload loadLocatesAndReloadThread(CompleteEmsConfig completeEmsConfig,
                                             EmsVenueMap emsVenueMap,
                                             LocalDate tradeDate) {
        LocatesLoader loader = new LocatesLoader(completeEmsConfig.getEmsConfigFolder());

        // if it does not find the file, throws missing file ERROR (which is good.)
        // can't trade with no locates file whatsoever
        File currentFile =
                findMostRecentFile(tradeDate, loader.getLocatesFolder(), LOCATES_FILE_PREFIX);

        DateTimeFormatter formatter = new DateTimeFormatterFactory(YYYYMMDD).createDateTimeFormatter();
        String localDateString = formatter.format(tradeDate);
        // we must have locates for the start date; fail-fast if no positions loaded
        // as it means we have not configured the ems-service
        //noinspection PointlessBooleanExpression
        if (false == currentFile.getName().contains(localDateString)) {
            // let operators fail-fast on lack of locates by setting this property
            if (Boolean.parseBoolean(System.getenv(EMS_FAIL_WITHOUT_LOCATES))) {
                String msg = "Must provide locates file named= [locates_"+ localDateString + ".csv] " +
                        "in folder=[" + loader.getLocatesFolder() + "]";
                log.error(msg);
                throw new Error(msg);
            }
            // you can survive with locates from yesterday; it is not good though
            // generally locates are similar, can run the EMS; however, this is not good
            log.error("Using OLD LOCATES FILE="+currentFile.getAbsolutePath());
        }

        LocatesMapAndLatestTimestamp locates = loader.loadLocates(emsVenueMap, currentFile, true);

        // set up the first day locates
        locatesManager.setLocatesForTradeDate(locates);

        LocalDate dateForFile = getDateForFile(currentFile);
        // reload positions when they arrive, either for today (emergency reload)
        // or future position dates, once available
        return new LocatesReload(emsVenueMap, multiplexer, locatesManager, loader, dateForFile, locates.getLatestTimestamp());
    }

    public static class LocatesReload implements Reload {

        private final LocatesLoader loader;
        private final EmsMultiplexer multiplexer;
        private final LocatesManager locatesManager;
        private final EmsVenueMap emsVenueMap;
        private Map<LocalDate, Long> locatesForDate = new ConcurrentHashMap<>();

        public LocatesReload(EmsVenueMap emsVenueMap,
                             EmsMultiplexer multiplexer,
                             LocatesManager locatesManager,
                             LocatesLoader loader,
                             LocalDate locatesDate,
                             long latestTimestamp)
        {
            this.emsVenueMap = emsVenueMap;
            this.locatesForDate.put(locatesDate, latestTimestamp);
            this.loader = loader;
            this.multiplexer = multiplexer;
            this.locatesManager = locatesManager;
        }

        @Override
        public void reloadIfNewer(LocalDate tradeDate) {
            File currentLocatesFile =
                    findMostRecentFile(tradeDate,
                            loader.getLocatesFolder(), LOCATES_FILE_PREFIX);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(YYYYMMDD);
            String tradeDateString = tradeDate.format(formatter);
            // Don't load new locates; but old locates can be utilized by the system
            //noinspection PointlessBooleanExpression
            if (false == currentLocatesFile.toString().contains(tradeDateString)) {
                log.debug("Locates file has not arrived for tradeDate="+tradeDateString);
            }
            else {
                // false = check the file time, don't load anything
                LocatesMapAndLatestTimestamp mostRecentLocates =
                        loader.loadLocates(emsVenueMap, currentLocatesFile, false);

                Long latestLoadedFile = locatesForDate.get(mostRecentLocates.getFileDate());

                if (latestLoadedFile != null && mostRecentLocates.getLatestTimestamp() <= latestLoadedFile) {
                    log.debug("Locates file has not changed=" + mostRecentLocates.getLatestTimestamp());
                }
                else {
                    // new file, load it
                    mostRecentLocates = loader.loadLocates(emsVenueMap, currentLocatesFile, true);
                    log.warn("Found new locates file=" + mostRecentLocates.getLatestTimestamp() +
                            ", loaded locates=" + mostRecentLocates.getLocatesFileRowMap().size());
                    // update the latest load time
                    locatesForDate.put(tradeDate, mostRecentLocates.getLatestTimestamp());
                    // set the locates on locates manager
                    locatesManager.setLocatesForTradeDate(mostRecentLocates);
                    // clean out any old positions
                    locatesManager.cleanOldLocates(tradeDate);
                }
            }
        }
    }
}
