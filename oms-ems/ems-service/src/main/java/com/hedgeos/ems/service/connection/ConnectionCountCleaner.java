package com.hedgeos.ems.service.connection;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@RequiredArgsConstructor
@Slf4j
public class ConnectionCountCleaner {

    private final EmsConnectionManager connectionManager;

    /**
     * Critical: At 6pm every day, if there were more than a trillion orderIds used
     * (tracked by the reconnectCount on the connection)
     * We can reset this reconnectCounter to zero.
     * These IDs may be safely re-used during the next 24 hour trading window.
     *
     * It is critical this fires only every 24 hours, and at the same time UTC.
     */
    @Scheduled(cron = "0 0 18 * * *")
    public void cleanConnectionCounts() {
        log.warn("Cleaning connection reconnectCounts.");
        Set<String> currentLogins = connectionManager.loginToConnectionId.keySet();
        for (String login : currentLogins) {
            // reset one login at a time
            connectionManager.resetReconnectCounts(login);
        }
    }
}
