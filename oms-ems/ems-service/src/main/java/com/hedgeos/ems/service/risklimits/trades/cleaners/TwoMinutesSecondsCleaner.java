package com.hedgeos.ems.service.risklimits.trades.cleaners;

import com.hedgeos.ems.service.risklimits.trades.TradeAmountTracker;
import com.hedgeos.ems.service.risklimits.trades.cleaners.OneArrayCleaner;

public class TwoMinutesSecondsCleaner implements OneArrayCleaner {
    @Override
    public void cleanFunction(TradeAmountTracker.TradeActivityBlocks blocks) {
        long now = System.currentTimeMillis();
        int currentIndex = TradeAmountTracker.secondOffset(now);
        blocks.cleanHalfAheadOfNow(blocks.lastTwoMinutesSeconds, currentIndex);
    }
}
