package com.hedgeos.ems.service.risklimits.trades.cleaners;

import com.hedgeos.ems.service.risklimits.trades.TradeAmountTracker;

public class TwoSecondsTenthsCleaner implements OneArrayCleaner {

    @Override
    public void cleanFunction(TradeAmountTracker.TradeActivityBlocks blocks) {
        long now = System.currentTimeMillis();
        int currentIndex = TradeAmountTracker.twoSecondTenthsOffset(now);
        blocks.cleanHalfAheadOfNow(blocks.lastTwoSecondsTenths, currentIndex);
    }
}
