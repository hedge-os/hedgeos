package com.hedgeos.ems.loader;

import java.time.LocalDate;

public interface Reload {
    void reloadIfNewer(LocalDate tradeDate);
}
