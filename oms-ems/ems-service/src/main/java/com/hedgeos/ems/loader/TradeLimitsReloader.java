package com.hedgeos.ems.loader;

import com.hedgeos.ems.config.beans.CompleteEmsConfig;
import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitLoader;
import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitLoad;
import com.hedgeos.ems.service.EmsEnvironmentManager;
import com.hedgeos.ems.service.risklimits.trades.TradeLimitsManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.datetime.standard.DateTimeFormatterFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitLoader.TRADE_LIMIT_FILE_PREFIX;
import static com.hedgeos.ems.csvfiles.util.FlatFileUtil.*;

@Component
@RequiredArgsConstructor
@Slf4j
public class TradeLimitsReloader {

    private final TradeLimitsManager limitManager;
    private final EmsEnvironmentManager emsEnvironmentManager;

    public TradeLimitReload loadAndStartReload(CompleteEmsConfig completeEmsConfig, LocalDate tradeDate) {
        // load positions on startup; and need to check for positions every minute
        TradeLimitLoader loader = new TradeLimitLoader(completeEmsConfig.getEmsConfigFolder());

        // if it does not find the file, throws missing file ERROR (which is good.)
        // can't trade with stale positions
        File currentFile =
                findMostRecentFile(tradeDate, loader.getRiskLimitsFolder(), TRADE_LIMIT_FILE_PREFIX);

        DateTimeFormatter formatter = new DateTimeFormatterFactory(YYYYMMDD).createDateTimeFormatter();
        String localDateString = formatter.format(tradeDate);
        // we must have positions for the start date; fail-fast if no positions loaded
        // as it means we have not configured the ems-service
        //noinspection PointlessBooleanExpression
        if (false == currentFile.getName().contains(localDateString)) {
            if (emsEnvironmentManager.isWorkstation()) {
                log.warn("Continuing with old trade LIMITS file="+currentFile+
                        ", due to developer workstation.");
            }
            else {
                // in Development, or Prod, fail if the positions file doesn't exist for the trade date starting on.
                // If starting on Sunday, is a position file necessary?
                // FAQ:  The operators can provide an empty file
                // or the file from Friday, if Sunday is the day the system is started.
                String msg = "Must provide a trading LIMITS file named= [trade_limits_"
                                + localDateString + ".csv] in folder=[" + loader.getRiskLimitsFolder() + "]";
                log.error(msg);
                // can't start ems-service without position limits
                throw new Error(msg);
            }
        }

        // load the positions one time to bootstrap; we need them for the current trade date
        TradeLimitLoad tradeLimits = loader.loadTradeLimits(currentFile, true);

        LocalDate dateFromFile = getDateForFile(currentFile);
        // set us up for success on the first trading day
        limitManager.setTradeLimits(tradeLimits);

        // reload positions when they arrive, either for today (emergency reload)
        // or future position dates, once available
        return new TradeLimitReload(limitManager, dateFromFile, tradeLimits.getLatestTimestamp(), loader);
    }

    public static class TradeLimitReload implements Reload {

        private final TradeLimitLoader loader;
        private final Map<LocalDate, Long> lastLoadMap = new ConcurrentHashMap<>();
        private final TradeLimitsManager limitManager;

        public TradeLimitReload(TradeLimitsManager limitsManager, LocalDate firstLoadDate,
                                long firstLoad, TradeLimitLoader loader) {
            this.lastLoadMap.put(firstLoadDate, firstLoad);
            this.loader = loader;
            this.limitManager = limitsManager;
        }

        public void reloadIfNewer(LocalDate tradeDate) {
            try {
                // TODO:  This should look ahead to the future date one day, and load it as well
                // TODO:  Along with all limits check files

                // the file contents may not be the tradeDate; it is the most recent file
                File mostRecentTradeLimitsFile =
                        findMostRecentFile(tradeDate,
                                loader.getRiskLimitsFolder(), TRADE_LIMIT_FILE_PREFIX);

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(YYYYMMDD);
                String tradeDateString = tradeDate.format(formatter);
                // NEVER load trade limits into memory if not for the current trade date
                //noinspection PointlessBooleanExpression
                if (false == mostRecentTradeLimitsFile.toString().contains(tradeDateString)) {
                    log.debug("TRADE LIMITS file has not arrived for tradeDate="+tradeDateString);
                }
                else {
                    TradeLimitLoad latestCheck =
                            loader.loadTradeLimits(mostRecentTradeLimitsFile, false);

                    Long lastLoadTime = lastLoadMap.get(latestCheck.getFileDate());
                    if (lastLoadTime != null && latestCheck.getLatestTimestamp() <= lastLoadTime) {
                        log.debug("TRADE LIMITS file has not changed=" + latestCheck.getLatestTimestamp());
                    }
                    else {
                        LocalDate dateFromFile = getDateForFile(mostRecentTradeLimitsFile);
                        // now loadThePositions is = true
                        latestCheck = loader.loadTradeLimits(mostRecentTradeLimitsFile, true);
                        log.warn("Found new TRADE LIMITS file=" + latestCheck.getLatestTimestamp() +
                                ", loaded TRADE LIMITS=" + latestCheck.getTradeLimitsMap().size());
                        // once a new file is loaded; this becomes the current known positions
                        // see the Decision 0008 in the decision docs folder.
                        limitManager.setTradeLimits(latestCheck);
                        // update the latest load time
                        lastLoadMap.put(dateFromFile, latestCheck.getLatestTimestamp());
                    }
                }
            }
            catch (Exception e) {
                // TODO: this needs to produce an email, and UX alert
                log.error("Error loading position LIMITS file, will sleep and retry in a minute.", e);
            }
        }
    }
}
