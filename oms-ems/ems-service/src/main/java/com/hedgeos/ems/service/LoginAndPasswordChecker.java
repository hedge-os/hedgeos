package com.hedgeos.ems.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import order.ConnectRequest;
import order.ConnectionStatusReason;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Component
@RequiredArgsConstructor
@Slf4j
public class LoginAndPasswordChecker {
    // prevents too many logins from any one client
    final Map<String, AtomicLong> connectionsPerLogin = new ConcurrentHashMap<>();

    private final EmsEnvironmentManager environmentManager;

    public int verifyLogin(ConnectRequest request) {
        if (request.emsEnvironment() != environmentManager.getEmsEnvironment()) {
            // this can cause you to trade in prod; Knight trading incident, very bad.
            log.error("VERY BAD: Attempt to connect to the wrong ems environment="+request.emsEnvironment()+
                    ", tried to connect to="+environmentManager.getEmsEnvironment());
            return ConnectionStatusReason.WRONG_EMS_ENVIRONMENT;
        }

        int reason = checkLogin(request, environmentManager.getEnvName());
        if (reason != 0) {
            return reason;
        }

        return protectAgainstRepeatedLogins(request);
    }

    public static int checkLogin(ConnectRequest request, String envName) {
        if (request == null || request.login() == null || request.login().isEmpty())
        {
            return ConnectionStatusReason.NULL_LOGIN;
        }

        if (request.password() == null) {
            return ConnectionStatusReason.PASSWORD_EMPTY;
        }

        String sillyHash = request.login()+envName;
        int length = sillyHash.length();
        sillyHash = sillyHash + length;
        if (Objects.equals(request.password(), sillyHash) == false) {
            log.error("Password sent does not match the hash 'algorithm'.");
            return ConnectionStatusReason.INVALID_PASSWORD;
        }

        return 0;
    }

    /**
     * If someone has a run-away system which keeps reconnecting, stop them at 1000.
     * We could run out of memory.  This is a severe production issue on the client,
     * and they can wait to trade until tomorrow.
     *
     * @param request
     */
    int protectAgainstRepeatedLogins(ConnectRequest request) {
        if (connectionsPerLogin.get(request.login()) == null) {
            // put in how many connections this login has
            connectionsPerLogin.put(request.login(), new AtomicLong(1));
            return ConnectionStatusReason.SUCCESS_REASON;
        }
        else {
            AtomicLong loginCount = connectionsPerLogin.get(request.login());
            long currentCount = loginCount.incrementAndGet();
            // protect against nefarious logins thousands of times
            if (currentCount > 1000) {
                log.error("More than 1000 connections from: "+ request.login());
                return ConnectionStatusReason.TOO_MANY_LOGINS;
            }

            return ConnectionStatusReason.SUCCESS_REASON;
        }
    }
}
