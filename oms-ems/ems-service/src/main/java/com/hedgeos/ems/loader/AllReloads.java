package com.hedgeos.ems.loader;

import com.hedgeos.ems.util.TradingDayUtil;

import java.time.LocalDate;
import java.util.List;

public class AllReloads implements Runnable {

    private final List<Reload> reloadList;

    public AllReloads(List<Reload> reloadList) {
        this.reloadList = reloadList;
    }

    public void run() {
        // reload each of the files in the sequence they were added
        for (Reload runnable : reloadList) {
            LocalDate tradeDate = TradingDayUtil.getTradeDate();
            runnable.reloadIfNewer(tradeDate);
            // look into the future one day, to prepare for trading
            LocalDate nextTradeDate = TradingDayUtil.plusTradeDays(tradeDate);
            runnable.reloadIfNewer(nextTradeDate);
        }

    }
}
