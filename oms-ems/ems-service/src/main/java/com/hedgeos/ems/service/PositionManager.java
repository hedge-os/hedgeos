package com.hedgeos.ems.service;

import com.google.common.util.concurrent.AtomicDouble;
import com.hedgeos.ems.order.EmsPosition;
import com.hedgeos.ems.order.PmGroupSidKey;
import com.hedgeos.ems.util.CompareAndSwapLock;
import com.hedgeos.ems.util.QuantityConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Component
@Slf4j
public class PositionManager {
    public static final double ZERO_START_QUANTITY = 0.0d;
    // public final Map<PmGroupSidKey, EmsPosition> currentPosition = new ConcurrentHashMap<>();

    public final Map<LocalDate, Map<PmGroupSidKey, EmsPosition>> currentPositionByDate = new ConcurrentHashMap<>();

    // this is an array of locks, hashed by SID to lock when we need to create a new position; to avoid a race
    // as creating new positions is rare (they can be created by inserting a row in a position file=0)
    // we could lock on one single lock; but 8 reduces contention in the event the system is not setup
    // optimally for new positions with the 0 rows in the position file
    final CompareAndSwapLock[] newPositionLockArray = new CompareAndSwapLock[8];

    public PositionManager() {
        // boot up with the array of new position locks ready
        for (int i=0; i<newPositionLockArray.length; i++) {
            newPositionLockArray[i] = new CompareAndSwapLock();
        }
    }

    public EmsPosition getPosition(LocalDate tradeDate, PmGroupSidKey key) {
        Map<PmGroupSidKey, EmsPosition> positionForDate = safeGetPositionsForDate(tradeDate);

        return positionForDate.get(key);
    }

    private Map<PmGroupSidKey, EmsPosition> safeGetPositionsForDate(LocalDate tradeDate) {
        Map<PmGroupSidKey, EmsPosition> positionForDate = currentPositionByDate.get(tradeDate);
        if (positionForDate == null) {
            // ordered for now; review if CHM is faster
            positionForDate = new ConcurrentSkipListMap<>();
            // TODO:  A little jitter; use a ring-buffer
            currentPositionByDate.put(tradeDate, positionForDate);
        }
        return positionForDate;
    }


    public EmsPosition getPositionOrMakeNew(LocalDate tradeDate, PmGroupSidKey key) {
        Map<PmGroupSidKey, EmsPosition> positionsForDate = currentPositionByDate.get(tradeDate);
        EmsPosition position = positionsForDate.get(key);
        if (position != null)
            return position;

        return atomicMakeNewPosition(ZERO_START_QUANTITY, positionsForDate, key);
    }


    public void tradeCompletedAgainstPosition(LocalDate tradeDate,
                                              int side,
                                              PmGroupSidKey key,
                                              double unsignedIncrementalQuantity)
    {
        Map<PmGroupSidKey, EmsPosition> positionsForDate = safeGetPositionsForDate(tradeDate);

        // we only need to lock if locates are being done
        // got a lock, lock the position
        EmsPosition position = positionsForDate.get(key);

        double signedQuantity = QuantityConverter.getSignedQuantity(side, unsignedIncrementalQuantity);

        if (position == null) {
            // this can never happen, is bad
            log.error("VERY BAD: Position null for a completed trade, key={}", key);
            atomicMakeNewPosition(signedQuantity, positionsForDate, key);
        }
        else {
            // position is not null; happy path, 99.99% of the time

            // need to lock the position;
            // ... locates may be trying to read the position, and we can't adjust the locate bucket
            position.lock.lock();
            position.tradeCompletedAgainstPosition(side, signedQuantity);
            position.lock.unlock();
        }
    }

    private EmsPosition atomicMakeNewPosition(double signedQuantity,
                                       Map<PmGroupSidKey, EmsPosition> positionsForDate,
                                       PmGroupSidKey key) {

        // There was no position previously; make one, must be thread safe.

        // We lack a position; this can be avoided with a zero value entry in the position file
        // reducing a tiny amount of jitter (estimated at 2 usecs)
        int sidHashed = (int) (key.sid % newPositionLockArray.length);
        CompareAndSwapLock newPositionLock = newPositionLockArray[sidHashed];
        newPositionLock.lock();
        try {
            // double check; after getting the position
            // - is it still null, inside the lock
            EmsPosition position = positionsForDate.get(key);
            if (position == null) {
                // locked, still null, make a new position key and position for this trade
                position = new EmsPosition(key, new AtomicDouble(ZERO_START_QUANTITY));
                // and add to the map
                positionsForDate.put(key, position);
            }
            else {
                // someone beat us to making a new position, unlikely but could happen
                position.setPosition(signedQuantity);
            }
            return position;
        }
        finally {
            // done creating a new position key and value
            newPositionLock.unlock();
        }
    }

    public EmsPosition getOrCreateNewPosition(LocalDate orderTradeDate, PmGroupSidKey key) {
        Map<PmGroupSidKey, EmsPosition> positionsForDate = safeGetPositionsForDate(orderTradeDate);

        // we only need to lock if locates are being done
        // got a lock, lock the position
        EmsPosition position = positionsForDate.get(key);

        if (position != null) {
            return position;
        }

        return atomicMakeNewPosition(ZERO_START_QUANTITY, positionsForDate, key);
    }
}
