package com.hedgeos.ems.service;

import com.hedgeos.ems.util.TradingDayUtil;
import lombok.extern.slf4j.Slf4j;
import order.Connection;
import order.Order;
import order.OrderResponse;
import order.OrderSubmitStatus;

import java.text.MessageFormat;
import java.time.LocalDate;

import static com.hedgeos.ems.order.OrderUtil.isNegativeQuantity;
import static com.hedgeos.ems.order.OrderUtil.isSell;
import static com.hedgeos.ems.service.Base62Checker.isPmGroupBase62Chars;
import static com.hedgeos.ems.util.QuantityConverter.quantityToDouble;

@Slf4j
public class DataIntegrityChecker {
    static OrderResponse dataIntegrityCheck(Order order, Connection connection)
    {
        // must have a trade date on every order (TODO: move this to the client.)
        if (order.tradeDate() == null) {
            String msg = MessageFormat.format("Missing tradeDate for order={0}, connectionId={1}",
                    order.hedgeOsOrderId(), connection.connectionId());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.MISSING_TRADE_DATE, msg);
        }

        // TODO: these checks can be client side
        // the trade date must be on or after this systems trade date
        if (isGoogleDateBeforeTradeDate(order.tradeDate())) {
            String msg = MessageFormat.format(
                    "Order has a tradeDate={0} prior to system tradeDate={1}",
                    order.tradeDate(), TradingDayUtil.getTradeDate());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.INVALID_TRADE_DATE, msg);
        }

        if (order.submitTimestampMillisUtc() == 0) {
            String msg =
                    MessageFormat.format("Missing submitTimeMillisUTC. " +
                            "hedgeOsOrderId={0}",order.hedgeOsOrderId());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.MISSING_ORDER_TIMESTAMP, msg);
        }

        if (order.pmGroup() == null || order.pmGroup().length() == 0) {
            String msg =
                MessageFormat.format("Null or blank pmGroup passed, needed for limits checks. " +
                        "hedgeOsOrderId={0}", order.hedgeOsOrderId());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.PM_GROUP_NULL, msg);
        }

        // client also does this
        if (order.pmGroup().length() > 3) {
            String msg = MessageFormat.format(
                    "pmGroup can be at most three chars, {0}", order.hedgeOsOrderId());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order,
                    OrderSubmitStatus.PM_GROUP_GREATER_THAN_THREE_CHARS, msg);
        }

        if (false == isPmGroupBase62Chars(order.pmGroup())) {
            // pmGroup goes into FIX messages tag 11; therefore, must prevent special chars
            String msg =
                MessageFormat.format("pmGroup must be all base62 chars, pmGroup={0}, orderId={1}",
                        order.pmGroup(), order.hedgeOsOrderId());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.PM_GROUP_NOT_BASE_62, msg);
        }

        if (order.quantity().quantity() == 0 && order.quantity().nanoQty() == 0) {
            String msg = "Zero quantity on order=" + order.hedgeOsOrderId() +
                    ", qty=" + quantityToDouble(order.quantity());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.ZERO_QUANTITY, msg);
        }

        if (isNegativeQuantity(order.quantity())) {
            String msg = "Negative quantity on order=" + order.hedgeOsOrderId() +
                            ", qty=" + quantityToDouble(order.quantity());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.NEGATIVE_QUANTITY, msg);
        }

        // note;  don't need an account with pmGroup in use
        // must have an account code (should be an ID for speed !)
        //        if (order.account() == null || order.account().code() == null) {
        //            log.error("Missing account code for order={}, connectionId={}", order.hedgeOsOrderId(), connection.connectionId());
        //            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.MISSING_ACCOUNT_CODE);
        //        }

        if (order.venue() == null) {
            String msg = "Venue is null; must be a valid venueID.  orderId=" + order.hedgeOsOrderId();
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.NULL_VENUE, msg);
        }

        return null;
    }

    private static boolean isGoogleDateBeforeTradeDate(google.type.Date orderApiTradeDate) {
        LocalDate tradeDate = TradingDayUtil.getTradeDate();
        if (orderApiTradeDate.year() < tradeDate.getYear())
            return true;
        if (orderApiTradeDate.month() < tradeDate.getMonthValue())
            return true;
        if (orderApiTradeDate.day() < tradeDate.getDayOfMonth())
            return true;

        return false;
    }
}
