package com.hedgeos.ems.service;

import com.google.common.util.concurrent.AtomicDouble;
import com.hedgeos.ems.config.beans.EmsVenueMap;
import com.hedgeos.ems.order.PositionFileRow;
import com.hedgeos.ems.csvfiles.positions.PositionLoadMap;
import com.hedgeos.ems.order.*;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import com.hedgeos.ems.csvfiles.symbology.SymbolAdapter;
import com.hedgeos.ems.csvfiles.symbology.SymbolManager;
import com.hedgeos.ems.order.EmsExecutionVenue;
import com.hedgeos.ems.service.locates.LocatesManager;
import com.hedgeos.ems.service.risklimits.positions.PositionCheckRowResult;
import com.hedgeos.ems.service.risklimits.positions.PositionLimitsManager;
import com.hedgeos.ems.service.risklimits.trades.TradingLimitCheckRowResult;
import com.hedgeos.ems.service.risklimits.trades.TradeAmountTracker;
import com.hedgeos.ems.service.risklimits.trades.TradeLimitsManager;
import com.hedgeos.ems.util.QuantityConverter;
import com.hedgeos.ems.util.TradingDayUtil;
import com.hedgeos.fix.adapter.ClientOrderIdStuffer;
import com.hedgeos.fix.adapter.FixAdapter;
import general.Quantity;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import order.*;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

import static com.hedgeos.ems.order.OrderUtil.*;
import static com.hedgeos.ems.service.risklimits.trades.TradeLimitChecker.LIMIT_CHECK_SUCCESS;
import static com.hedgeos.ems.util.QuantityConverter.quantityToDouble;
import static order.OrderStatus.PENDING_REPLACE_ORDER_STATUS;

@Component
@RequiredArgsConstructor
@Slf4j
public class EmsMultiplexer {

    public static final int REVERSE_MULT = -1;
    private final EmsServerIdManager serverIdManager;
    private final SymbolManager symbolManager;

    private final PositionLimitsManager positionLimitsManager;
    private final TradeLimitsManager tradeLimitsManager;

    public final TradeAmountTracker tradeAmountTracker;

    private final PositionManager positionManager;
    private final LocatesManager locatesManager;

    private final OrderCache orderCache;

    // most important map, holds the connectionId to the observers, since this goes
    // out in ClientOrderId, we can simply grab it, and get the stream observer to write back to!
    // Ems Client --> Ems Server (connectionId --> Observer) -->
    //  Tag11 2nd char --> Response (w/ tag 11) --> read 2nd char --> Observer --> EmsClient
    // Diagram here:
    final Map<Integer, List<StreamObserver<EmsReport>>> connectionMap = new ConcurrentHashMap<>();

    // the fix Adapters started by FixServerLifecycle
    final Map<EmsExecutionVenue, FixAdapter> venueToFixAdapterMap = new ConcurrentSkipListMap<>();
    // symbol adapter for each venue
    final Map<EmsExecutionVenue, SymbolAdapter> venueToSymbolAdapter = new ConcurrentSkipListMap<>();

    private EmsVenueMap venueMap;

    // used for admin web-jsp pages (see GapFillController)
    private final Map<String, FixAdapterConfig> fixConfigMap = new HashMap<>();
    private final Map<String, FixAdapter> fixAdapterMap = new HashMap<>();

    public Map<String, FixAdapter> getFixAdapterMap() { return fixAdapterMap; }
    public Map<String, FixAdapterConfig> getFixConfigMap() { return fixConfigMap; }

    public void setVenueMap(EmsVenueMap venueMap) {
        this.venueMap = venueMap;
    }

    public void addFixAdapter(FixAdapterConfig config, FixAdapter fixAdapter) {

        // this is a second check; we prevent this in ems-config
        if (venueToFixAdapterMap.containsKey(config.getExecutionVenue())) {
            FixAdapter otherAdapter = venueToFixAdapterMap.get(config.getExecutionVenue());
            log.error("(VERY BAD) Two identical execution venues configured="+config.getExecutionVenue());
            throw new Error("(VERY BAD) Two identical execution venues configured="+config.getExecutionVenue()+
                    ", fixAdapter="+config.getName()+", the other config is="+otherAdapter.getName());
        }

        // record the FixAdapter to use for the venue
        this.venueToFixAdapterMap.put(config.getExecutionVenue(), fixAdapter);

        // these are used by the web config
        this.fixConfigMap.put(config.getName(), config);
        this.fixAdapterMap.put(fixAdapter.getName(), fixAdapter);

        // each venue uses a particular set of symbols (bloomberg, NYSE, etc.)
        SymbolAdapter symbolAdapter = symbolManager.buildSymbolAdapter(config.getSymbology());
        this.venueToSymbolAdapter.put(config.getExecutionVenue(), symbolAdapter);
    }

    /**
     * Callback from the FixAdapter, with FIX translated to an EmsReport.
     * @param er emsReport from the FixAdapter;
     *           message from a Venue; e.g. NEW, PART_FILLED, CANCELLED.
     */
    public void processAndRoute(EmsReport er) {
        try {

            // TODO: We are dropping Pending; less complexity.
            if (isPendingOrderStatus(er)) {
                log.debug("Ignoring pending, hedgeOsOrderId={}, status={}", er.hedgeOsOrderId(), er.orderStatus());
                return;
            }

            PmGroupSidKey key = new PmGroupSidKey(er.sid(), er.pmGroup());

            if (isExecTypeForReturnOfQuantity(er)) {
                returnLocatesAndLimitsOnCancels(er, key);
            }
            else if (isPotentialIncreaseOfCumQty(er)) {
                increaseActualTradedAmountsPerPosition(er, key);
            }

            // get the list of observers for this connection
            //    (connectionId from cracked ClientOrderIdStuffer)
            List<StreamObserver<EmsReport>> observerList = connectionMap.get(er.hedgeOsConnectionId());

            if (observerList == null) {
                // on restart; messages can be played; do we hold them?
                // TODO:  Probably need to hold the messages, and play them
                // TODO:  Once someone connects to us, if replay = true.
                log.warn("Client is not connected=" + er.hedgeOsConnectionId());
                return;
            }

            // typically only one connection, push out to each connection
            // note: the first subscription should be the most latency critical client
            for (StreamObserver<EmsReport> orderStatusStreamObserver : observerList) {
                try {
                    // pump the message back to the client subscription
                    orderStatusStreamObserver.onNext(er);
                } catch (Throwable t) {
                    // connections could be broken, and not yet removed on an execution report
                    log.debug("Connection may be lost", t);
                }
            }
        }
        catch (Error e) {
            log.error("Error in processReport, hedgeOsOrderId="+er.hedgeOsOrderId());
        }
    }

    private void increaseActualTradedAmountsPerPosition(EmsReport er, PmGroupSidKey key) {
        LocalDate tradeDate = getEmsReportTradeDate(er);
        OrderChain orderChain = orderCache.getOrder(tradeDate, er.hedgeOsOrderId());
        // TODO:  Hard problem, updating position given an execution report.
        // TODO:  Tricky; no order chain for a cancel; should we set it, this way a
        // chain exists?  Probably not a bad thing to do
        // alternately, could check if it is a cancel
        if (orderChain != null) {
            // see if the cum-qty went up, if it did, we traded more
            Quantity cumQty = er.cumulativeQty();
            double cumQtyDouble = QuantityConverter.quantityToDouble(cumQty);
            double quantityIncrease = orderChain.increaseCumQuantityReturnDiff(cumQtyDouble);
            if (quantityIncrease != 0.0d) {
                // if we traded more, increase the amounts used to compute positions, locates
                positionManager.tradeCompletedAgainstPosition(tradeDate, er.side(), key, quantityIncrease);
            }
            else {
                log.warn("Increase quantity is zero, er="+ er.hedgeOsOrderId());
            }
        } else {
            log.error("VERY BAD: Order Chain is null for orderId=" + er.hedgeOsOrderId());
        }
    }

    /**
     * The CumQty for an order can only increase for FILLED or PART FILLED order status.
     * @param er the exec report from the venue
     * @return if it is filled or part filled
     */
    private static boolean isPotentialIncreaseOfCumQty(EmsReport er) {
        return (er.orderStatus() == OrderStatus.PART_FILLED_ORDER_STATUS ||
                er.orderStatus() == OrderStatus.FILLED_ORDER_STATUS);
    }

    private static boolean isPendingOrderStatus(EmsReport er) {
        return er.orderStatus() == OrderStatus.PENDING_NEW_ORDER_STATUS ||
                er.orderStatus() == PENDING_REPLACE_ORDER_STATUS ||
                er.orderStatus() == OrderStatus.PENDING_CANCEL_ORDER_STATUS;
    }

    /**
     * If an order is cancelled, there may be unused locates we can return.
     *
     * @param er
     * @param key
     */
    private void returnLocatesAndLimitsOnCancels(EmsReport er, PmGroupSidKey key) {


        LocalDate tradeDate = getEmsReportTradeDate(er);
        // long orderId = er.hedgeOsOrderId();

        EmsPosition position = positionManager.getOrCreateNewPosition(tradeDate, key);

        // if it was a cancel, we want to return the leaves quantity on the cancel
        if (isOrderCancelled(er)) {

            // prefer the OrderQty - CumQty, as the "untraded" quantity
            double positiveQuantityToReturn = getPositiveQuantityToReturn(er);

            if (positiveQuantityToReturn > 0.0) {
                // TODO:  Interesting question; in a cancel replace chain could we return twice
                long origOrderId = er.hedgeOsOriginalOrderId();

                if (quantityWasNotPreviouslyReturned(origOrderId)) {
                    // return difference AS OF the ORIGINAL TIME the order was placed

                    long orderTimestamp = getOriginalOrderTimestamp(origOrderId, tradeDate);

                    // if a sell is cancelled, we want to return this quantity, as we no longer need locates for it.
                    // note: returning a used quantity, do not need to lock
                    position.reduceRequestedQuantity(er.side(), positiveQuantityToReturn);

                    // return the PRE TRADED amount
                    // this only returns the trading limits, for the exact time of the order timestamp

                    // Both trading limits and position limits checks passed; reserve the limits
                    // CRITICAL; reserve the new trade in the limit space
                    tradeLimitsManager.recordNewTradePlacedInTracker(orderTimestamp, key, origOrderId, tradeDate, positiveQuantityToReturn);
                    // change the position by this much; as if the trade was executed
                } else {
                    log.warn("Quantity was returned for limits, hedgeOsOrderId={}, quantity={}"
                            ,er.hedgeOsOrderId(),positiveQuantityToReturn);
                }

//                OrderChain orderChain =
//                        orderCache.getOrder(tradeDate, er.hedgeOsOrignalOrderId());
//
//                if (orderChain != null && position != null) {
//                    // if a qty difference (unused), return it
//                    // return the locates; also protects against reentrancy
//
//                    // position.returnLocatesOnCancel((long) positiveQuantityToReturn, orderChain.side());
//                }
//                else {
//                    log.error("Could not find originalOrderId={}",er.hedgeOsOrignalOrderId());
//                }
            }
            else {
                log.error("Trying to return a negative locate quantity");
                throw new Error("Trying to return a negative locate quantity");
            }
        }

    }

    private static double getPositiveQuantityToReturn(EmsReport er) {

        if (er.orderQty() != null && er.cumulativeQty() != null) {
            double orderQty = quantityToDouble(er.orderQty());
            double cumQtyDouble = quantityToDouble(er.cumulativeQty());
            return orderQty - cumQtyDouble;
        }
        else if (er.leavesQty() != null) {
            // use leavesQty on a cancel, if orderQty or cumQty are not available
            double leavesDouble =  quantityToDouble(er.leavesQty());
            log.warn("Using leavesQty to return limits and locates, hedgeOsOrderId="+ er.hedgeOsOrderId());
            return leavesDouble;
        }
        else {
            log.error("failed to return trade limits; " +
                            "no orderQty and cumQty on cancelled ExecReport. " +
                            "hedgeOsOrderId={}, orderQty={}, cumQty={}",
                    er.hedgeOsOrderId(), er.orderQty(), er.cumulativeQty());
            return 0.0d;
        }
    }

    private boolean isExecTypeForReturnOfQuantity(EmsReport er) {
        return er.emsExecType() == EmsExecType.CANCEL_SUCCESS_EXEC_TYPE;
    }

    private boolean quantityWasNotPreviouslyReturned(long orderId) {
        return false == orderCache.getAndSetLimitsReturned(orderId);
    }


    private boolean isOrderCancelled(EmsReport er) {
        return er.orderStatus() == OrderStatus.CANCELLED_ORDER_STATUS;
    }

    private LocalDate getEmsReportTradeDate(EmsReport er) {
        // TransactTime is not required; therefore, do our best.
        long transactTime = er.transactTime();
        if (transactTime == 0) {
            log.warn("ER Transact time was not set, setting to now.");
            transactTime = System.currentTimeMillis();
        }
        return TradingDayUtil.timeToLocalDate(transactTime);
    }

    private static boolean isOrderNoLongerRunning(EmsReport er) {
        return  er.orderStatus() == OrderStatus.CANCELLED_ORDER_STATUS ||
                er.orderStatus() == OrderStatus.DONE_FOR_DAY_ORDER_STATUS ||
                er.orderStatus() == OrderStatus.EXPIRED_ORDER_STATUS ||
                er.orderStatus() == OrderStatus.STOPPED_ORDER_STATUS ||
                er.orderStatus() == OrderStatus.REPLACED_ORDER_STATUS;
    }

    /**
    * Important:  return the limits as of the time the order was placed.
    * otherwise, a sliding window will lose the order but have a negative quantity
    * for the reversed quantity here; a serious defect.
     */
    private long getOriginalOrderTimestamp(long orderId, LocalDate orderTradeDate) {
        OrderChain order = orderCache.getOrder(orderTradeDate, orderId);
        if (order == null) {
            order = orderCache.getOrder(TradingDayUtil.getTradeDate(), orderId);
        }

        if (order != null) {
            return order.submitTimestampMillisUtc(orderId);
        }
        else {
            log.error("Could not find order in orderCache on cancel="+ orderId);
            // this should never happen; but will back out a negative quantity as of now,
            // effectively increasing the available limit, allowing trading to continue
            return System.currentTimeMillis();
        }
    }

    public CancelReplaceResponse cancelReplace(CancelReplace replace, Connection connection) {

        // need trade date to find the original order; orderId is (potentially) not unique across days
        LocalDate orderTradeDate = TradingDayUtil.googleDateToLocalDate(replace.tradeDate());

        long origOrdId = replace.hedgeOsOrigOrderId();
        log.debug("Looking in orderCache for originalOrder={}", origOrdId);
        OrderChain chain = orderCache.getOrder(orderTradeDate, origOrdId);
        if (chain == null) {
            String msg = MessageFormat.format(
                    "Attempt to cancel an order with no orderId={0}, original={1}, tradeDate={2}",
                            replace.hedgeOsOrderId(), origOrdId,
                            OrderUtil.toString(replace.tradeDate()));
            log.error(msg);
            return ResponseFactory.createCancelReplaceResponse(replace,
                    OrderSubmitStatus.NO_ORIGINAL_ORDER_TO_CANCEL,
                    msg);
        }

        // let the venue handle these cases; TODO: could add and test these
//        if (chain.orderStatus(origOrdId) == OrderStatus.CANCELLED_ORDER_STATUS) {
//            log.error("Attempt to replace a cancelled order. orderId={}", replace.hedgeOsOrigOrderId());
//            return ResponseFactory.createCancelReplaceResponse(replace, CancelReplaceSubmitStatus.ORDER_ALREADY_REPLACED_STATUS);
//        }
//
//        if (chain.orderStatus(origOrdId) == OrderStatus.REJECTED_ORDER_STATUS) {
//            log.error("Attempt to replace a rejected order. orderId={}", replace.hedgeOsOrigOrderId());
//            return ResponseFactory.createCancelReplaceResponse(replace, CancelReplaceSubmitStatus.ORDER_PREVIOUSLY_REJECTED_REPLACE_STATUS);
//        }

        // order is likely going to the market..

        // find the right venue
        FixAdapter fixAdapter = venueToFixAdapterMap.get(chain.venue());
        if (fixAdapter == null) {
            // this should never happen, means venues changed are making up venues
            String msg = "No venue mapping exists FOR CANCEL. hedgeOsOrderId="+replace.hedgeOsOrderId()+
                    ", order="+replace.hedgeOsOrderId()+", venue="+chain.venue().getId();
            log.error(msg);
            return ResponseFactory.createCancelReplaceResponse(replace,
                    OrderSubmitStatus.NO_SUCH_VENUE_EXISTS, msg);
        }

        // unique trading day token for every clientOrderID
        int tradingDayOfMonth = TradingDayUtil.getTradingDay();

        // need care to make the same one every time
        String originalClientOrderIdStuffed =
                ClientOrderIdStuffer.stuffClientOrderID(
                        serverIdManager.getEmsServerId(), // the emsServerId is first in ClientOrderID
                        connection.connectionId(), // the connection is the second char
                        tradingDayOfMonth, // day of month is the third char
                        replace.hedgeOsOrigOrderId(), // origOrdId passed on the replace
                        chain.pmGroup(), // // pmGroup of the original order; never changes
                        chain.sid()); // sid of the original order; never changes

        String clientOrderIdStuffed =
                ClientOrderIdStuffer.stuffClientOrderID(
                        serverIdManager.getEmsServerId(), // the emsServerId is first in ClientOrderID
                        connection.connectionId(), // the connection is the second char
                        tradingDayOfMonth, // day of month is the third char
                        replace.hedgeOsOrderId(), // orderId of the replace
                        chain.pmGroup(), // pmGroup of the original order
                        chain.sid()); // sid of the original order

        double orderQty = QuantityConverter.quantityToDouble(replace.quantity());
        double orderQtySigned = getDirectionalQuantity(replace.quantity(), chain.side());
        double signedPreTradeIncrease = orderQtySigned;


        // we need to know the pre-trade quantity increase, on a cancel replace;
        // impacts the locate calculations
        double quantityIncreaseThisReplace = chain.addReplaceAndCalcPreTradeIncrease(replace);
        double signedQuantityIncreaseThisReplace = quantityIncreaseThisReplace;
        Quantity increasedQuantityThisReplace = QuantityConverter.convertDoubleToQty(quantityIncreaseThisReplace);

        if (isSell(chain.side())) {
            signedPreTradeIncrease *= -1;
            signedQuantityIncreaseThisReplace *= -1;
        }

        PmGroupSidKey key = chain.getKey();
        // get or make a new position, needed for locates
        EmsPosition position = positionManager.getOrCreateNewPosition(orderTradeDate, key);

        int returnCode = 0;
        LocateCheckResult locateCheckResult = null;

        // this is actual trading; check the limits.
        boolean isPreCheck = false;

        // run risk checks using position_limits and trade_limits; must not over-trade
        TradingLimitCheckRowResult tradingLimitsCheck =
                tradeLimitsManager.checkTradingLimit(isPreCheck, chain.getKey(),
                    chain.side(), increasedQuantityThisReplace, replace.submitTimestampMillisUtc(),
                    orderTradeDate, replace.hedgeOsOrigOrderId());

        // if trading limits failed, return
        if (tradingLimitsCheck != LIMIT_CHECK_SUCCESS)
        {
            String msg = MessageFormat.format("Order failed trading limits check.  error={0}, hedgeOsOrderId={1}, quantity={2}",
                    tradingLimitsCheck, replace.hedgeOsOrderId(), quantityToDouble(replace.quantity()));
            log.error(msg);
            return ResponseFactory.createCancelReplaceResponse(replace,
                    tradingLimitsCheck.getOrderSubmitStatus(),
                    tradingLimitsCheck.getMessage());
        }

        // position limit checks
        PositionCheckRowResult positionLimitsCheck =
                positionLimitsManager.checkPositionLimits(
                    isPreCheck, chain.getKey(), chain.side(),
                        increasedQuantityThisReplace,
                        replace.hedgeOsOrderId(), orderTradeDate, position);

        // if position limits failed, return
        if (positionLimitsCheck.getOrderSubmitStatus() !=
                OrderSubmitStatus.SUCCESSFUL_ORDER)
        {
            log.error("Order failed position limits check.  error={}, hedgeOsOrderId={}, quantity={}",
                    positionLimitsCheck, replace.hedgeOsOrderId(), quantityToDouble(replace.quantity()));
            return ResponseFactory.createCancelReplaceResponse(replace,
                    positionLimitsCheck.getOrderSubmitStatus(),
                    positionLimitsCheck.getMessage());
        }

        try {
            // TODO:  Get limits and locates
            // TODO:  Limits is the net increase in the order from the last order

            // TODO: ----------------------------------------------------------------
            // TODO:  Data race here between limit checks, and then reserved
            // TODO:  one statement later in "updateTradingLimits"
            // TODO:  Could try to get a lock; but expensive
            // TODO: ----------------------------------------------------------------

            // Both trading limits and position limits checks passed; reserve the limits
            // CRITICAL; reserve the new trade in the limit space

            // Note:  You always need the order quantity; because, as time passes,
            // the original order is not being applied to the trading limits,
            // which are sliding windows
            long timeOfTradeMillis = replace.submitTimestampMillisUtc();

            // Both trading limits and position limits checks passed; reserve the limits
            // reserve the new trade in the limit space
            tradeLimitsManager.recordNewTradePlacedInTracker(timeOfTradeMillis,
                    key, replace.hedgeOsOrderId(), orderTradeDate, signedPreTradeIncrease);

            // Do we need locates to complete this order, and, are there enough?
            locateCheckResult =
                    locatesManager.atomicCheckLocateAndIncrementPreTradedAmounts(position, replace.hedgeOsOrderId(),
                            chain, isPreCheck, (long) quantityIncreaseThisReplace);

            // did we get the locates we needed?
            if (locateCheckFailed(locateCheckResult)) {
                String msg =
                        MessageFormat.format("Failed to obtain LOCATES, " +
                                        "orderId={0}, quantity={1}, venue={2}",
                                replace.hedgeOsOrderId(),
                                quantityToDouble(replace.quantity()),
                                chain.venue().getId());
                log.error(msg);
                return ResponseFactory.createCancelReplaceResponse(replace, locateCheckResult.orderStatusCode, msg);
            }

            boolean isOrderUsingLocates = false;
            if (locateCheckResult != null) {
                isOrderUsingLocates = locateCheckResult.isOrderUsingLocates;
            }

            // send it off to the FIX Adapter; convert to a FIX message
            returnCode =
                    fixAdapter.cancelReplaceOrder(
                            replace,
                            chain.getSymbol(),
                            chain,
                            clientOrderIdStuffed,
                            originalClientOrderIdStuffed,
                            isOrderUsingLocates);

            return ResponseFactory.createCancelReplaceResponse(replace, returnCode, null);
        }
        finally {

            if (returnCode == OrderSubmitStatus.SUCCESSFUL_ORDER) {
                // put in a pointer from the replaced to the order chain
                // allowing us to replace another replace.
                orderCache.addOrder(replace.hedgeOsOrderId(), chain);
            }
            else {
                // if we fail to send the fixMessage, reverse the limits taken
                returnLimitsAndPreTradeAmount(
                        replace.hedgeOsOrderId(),
                        replace.submitTimestampMillisUtc(),
                        chain, signedQuantityIncreaseThisReplace);
            }
        }
    }


    private static boolean locateCheckFailed(LocateCheckResult locateCheckResult) {
        return locateCheckResult != null &&
                locateCheckResult.orderStatusCode != OrderSubmitStatus.SUCCESSFUL_ORDER;
    }

    public CancelResponse cancelOrder(Cancel cancel, Connection connection) {
        LocalDate orderTradeDate = TradingDayUtil.googleDateToLocalDate(cancel.tradeDate());

        long origOrdId = cancel.hedgeOsOrigOrderId();
        log.debug("Looking in orderCache for originalOrder={}",origOrdId);
        OrderChain chain = orderCache.getOrder(orderTradeDate, origOrdId);
        if (chain == null) {
           log.error("Attempt to cancel an order with no original, " +
                           "orderId={}, original={}, tradeDate={}",
                   cancel.hedgeOsOrderId(), origOrdId, cancel.tradeDate());
           return ResponseFactory.createCancelResponse(cancel,
                   CancelSubmitStatus.NO_ORIGINAL_ORDER_CANCEL_STATUS);
        }

        // letting the venue handle this complex case for the time being
//        if (chain.originalOrderStatus() == OrderStatus.CANCELLED_ORDER_STATUS) {
//            log.error("Attempt to cancel a cancelled order={}, cancelId={}",cancel.hedgeOsOrigOrderId(), cancel.hedgeOsOrderId());
//            return ResponseFactory.createCancelResponse(cancel, CancelSubmitStatus.ORDER_ALREADY_CANCELLED);
//        }
//        if (chain.originalOrderStatus() == OrderStatus.REJECTED_ORDER_STATUS) {
//            log.error("Attempt to cancel a rejected order={}, cancelId={}",cancel.hedgeOsOrigOrderId(), cancel.hedgeOsOrderId());
//            return ResponseFactory.createCancelResponse(cancel, CancelSubmitStatus.ORDER_PREVIOUSLY_REJECTED);
//        }

        chain.addCancel(cancel);

        // make a key for the venue map lookup
        EmsExecutionVenue emsVenue = new EmsExecutionVenue();
        emsVenue.setId(chain.venue().getId());

        // find the right venue
        FixAdapter fixAdapter = venueToFixAdapterMap.get(emsVenue);
        if (fixAdapter == null) {
            // this should never happen, means venues changed are making up venues
            log.error("No venue mapping exists FOR CANCEL. hedgeOsOrderId="+cancel.hedgeOsOrderId()+
                    ", originalOrderId="+cancel.hedgeOsOrigOrderId()+", venue="+chain.venue().getId());
            return ResponseFactory.createCancelResponse(cancel, CancelSubmitStatus.NO_SUCH_VENUE_EXISTS_CANCEL);
        }

        // TODO:  Should be from ORIGINAL ORDER:  unique trading day token for every clientOrderID
        int tradingDayOfMonth = chain.getOrderTradeDate().getDayOfMonth();
        // need care to make the same one every time
        // TODO:  Do we need the original order id in the clientOrderId
        String originalClientOrderIdStuffed =
                ClientOrderIdStuffer.stuffClientOrderID(
                        serverIdManager.getEmsServerId(), // the emsServerId is first in ClientOrderID
                        connection.connectionId(), // the connection is the second char
                        tradingDayOfMonth, // day of month is the third char
                        cancel.hedgeOsOrigOrderId(), // hedgeOS original ID
                        chain.pmGroup(), // pmGroup never changes
                        chain.sid()); // sid never changes

        String clientOrderIdStuffed =
                ClientOrderIdStuffer.stuffClientOrderID(
                        serverIdManager.getEmsServerId(), // the emsServerId is first in ClientOrderID
                        connection.connectionId(), // the connection is the second char
                        tradingDayOfMonth, // day of month is the third char
                        cancel.hedgeOsOrderId(), // hedgeOS Order ID is next X chars
                        chain.pmGroup(), // pmGroup never changes
                        chain.sid()); // sid never changes

        // send it off to the FIX Adapter; convert to a FIX message
        int returnCode = fixAdapter.cancelOrder(chain.getSymbol(),
                cancel.hedgeOsOrderId(), chain.side(), cancel.submitTimestampMillisUtc(),
                clientOrderIdStuffed, originalClientOrderIdStuffed);

        if (returnCode == CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS) {
            // success canceling; add it to the chain
            orderCache.addOrder(cancel.hedgeOsOrigOrderId(), chain);
        }

        return ResponseFactory.createCancelResponse(cancel, returnCode);
    }


    public OrderResponse submitOrder(Order order, Connection connection) {

        OrderResponse orderResponse = DataIntegrityChecker.dataIntegrityCheck(order, connection);
        if (orderResponse != null) {
            log.error("Failed basic checks={}", order.hedgeOsOrderId());
            return orderResponse;
        }

        // convert trade date to local date
        LocalDate tradeDate = TradingDayUtil.googleDateToLocalDate(order.tradeDate());

        EmsExecutionVenue venue = findExecutionVenueForOrder(order);

        if (venue == null) {
            String msg = MessageFormat.format("no venue mapped for either on hedgeOsOrderId={}, venue id={}, venue ame={}", order.hedgeOsOrderId(), order.venue().id(), order.venue().name());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.NO_SUCH_VENUE_EXISTS, msg);
        }

        // wrap the order in an OrderChain; with cached trade date
        OrderChain chain = new OrderChain(order, tradeDate, venue);
        // add to the order cache; could be done in the-finally; however, makes it easier to test, same cost.
        // if it is in the finally, then there are data races
        orderCache.addOrder(order.hedgeOsOrderId(), chain);

        // find the right venue
        FixAdapter fixAdapter = venueToFixAdapterMap.get(chain.venue());
        if (fixAdapter == null) {
            // this should never happen, means users are making up venues
            String msg =
                    MessageFormat.format("No venue mapping exists. hedgeOsOrderId={0}",order.hedgeOsOrderId() + ", venue=" + order.venue().id());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.NO_SUCH_VENUE_EXISTS, msg);
        }

        // find the symbol adapter for the venue, and the symbol for the sid
        SymbolAdapter symbolAdapter = venueToSymbolAdapter.get(chain.venue());
        String symbol = symbolAdapter.getSymbolForSid(order.sid());
        // send failure if we lack a symbol for this sid
        if (symbol == null) {
            String msg = MessageFormat.format("Symbol mapping failed for orderId={0}, sid={1}, symbology={2}",
                    order.hedgeOsOrderId(), order.sid(), symbolAdapter.getSymbology());
            log.error(msg);
            return ResponseFactory.createOrderResponse(order, OrderSubmitStatus.SYMBOL_MAPPING_FAILED, msg);
        }

        // symbol never changes, once set here, on the OrderChain
        chain.setSymbol(symbol);

        // TODO: ----------------------------------------------------------------
        // TODO:  Data race here between limit checks, and then reserved
        // TODO:  one statement later in "updateTradingLimits"
        // TODO:  Could try to get a lock; but expensive
        // TODO: ----------------------------------------------------------------

        // we are not testing limits, we need the limits.
        boolean isPreCheck = false;

        // run risk checks using position_limits and trade_limits; must not over-trade
        TradingLimitCheckRowResult tradingLimitsCheck =
                tradeLimitsManager.checkTradingLimit(isPreCheck, chain.getKey(), chain.side(),
                        order.quantity(), order.submitTimestampMillisUtc(),
                        chain.getOrderTradeDate(),
                        order.hedgeOsOrderId());

        // if we performed trading limit checks, and not successful, return it
        if (tradingLimitsCheck != null && tradingLimitsCheck != LIMIT_CHECK_SUCCESS)
        {
            log.error("Order failed trading limits check.  " +
                            "error={}, hedgeOsOrderId={}, quantity={}",
                    tradingLimitsCheck, order.hedgeOsOrderId(),
                    quantityToDouble(order.quantity()));
            return ResponseFactory.createOrderResponse(order,
                    tradingLimitsCheck.getOrderSubmitStatus(),
                    tradingLimitsCheck.getMessage());
        }

        PmGroupSidKey key = chain.getKey();
        LocalDate orderTradeDate = chain.getOrderTradeDate();
        EmsPosition position = positionManager.getOrCreateNewPosition(orderTradeDate, key);

        // run position risk limit checks
        PositionCheckRowResult positionLimitResult =
                positionLimitsManager.checkPositionLimits(isPreCheck, chain.getKey(),
                        order.side(), order.quantity(),order.hedgeOsOrderId(),
                        chain.getOrderTradeDate(), position);

        // if we checked, and not success, return the failure
        if (positionLimitResult != null &&
                positionLimitResult.getOrderSubmitStatus() != OrderSubmitStatus.SUCCESSFUL_ORDER)
        {
            String msg =
                    MessageFormat.format(
                            "Order failed position limits check.  error={0}, hedgeOsOrderId={1}, quantity={2}",
                    positionLimitResult.getOrderSubmitStatus(), order.hedgeOsOrderId(), quantityToDouble(order.quantity()));
            log.error(msg);
            return ResponseFactory.createOrderResponse(order,
                    positionLimitResult.getOrderSubmitStatus(),
                    positionLimitResult.getMessage());
        }


        long timeOfTradeMillis = order.submitTimestampMillisUtc();

        // for the finally block where these are cleaned,
        // need to make variables
        int returnCode = 0;
        LocateCheckResult locateCheckResult = null;
        boolean areLocatesUsed = false;

        double orderQty = QuantityConverter.quantityToDouble(order.quantity());
        double orderQtySigned = getDirectionalQuantity(order.quantity(), order.side());

        try {
            // Both trading limits and position limits checks passed; reserve the limits
            // CRITICAL; reserve the new trade in the limit space
            tradeLimitsManager.recordNewTradePlacedInTracker(timeOfTradeMillis, key, order.hedgeOsOrderId(), orderTradeDate, orderQtySigned);

            // try to get locates for this venue (if necessary)
            locateCheckResult =
                    locatesManager.atomicCheckLocateAndIncrementPreTradedAmounts(
                            position, order.hedgeOsOrderId(), chain, isPreCheck, (long) orderQty);

            // did we get the locates we needed?
            if (locateCheckFailed(locateCheckResult)) {
                String msg =
                        MessageFormat.format("Failed to obtain LOCATES, sid={0}, orderId={1}, quantity={2}",
                                order.sid(), order.hedgeOsOrderId(), quantityToDouble(order.quantity()));
                log.error(msg);
                return ResponseFactory.createOrderResponse(order, locateCheckResult.orderStatusCode, msg);
            }

            // carry the flag of did we "use locates", for pushing to FIX
            if (locateCheckResult != null) {
                areLocatesUsed = locateCheckResult.isOrderUsingLocates;
            }

            // TODO:  End of position limits and trade limits data race
            // TODO: ----------------------------------------------------------------

            // unique trading day token for every clientOrderID
            int tradingDayOfMonth = TradingDayUtil.getTradingDay();
            // need care to make the same one every time
            // TODO:  Do we need the original order id in the clientOrderId
            String clientOrderIdStuffed =
                    ClientOrderIdStuffer.stuffClientOrderID(
                            serverIdManager.getEmsServerId(), // the emsServerId is first in ClientOrderID
                            connection.connectionId(), // the connection is the second char
                            tradingDayOfMonth, // day of month is the third char
                            order.hedgeOsOrderId(), // hedgeOS Order ID is next X chars
                            order.pmGroup(),
                            order.sid()); // sid is next Y chars


            // send it off to the FIX Adapter; convert to a FIX message
            returnCode = fixAdapter.submitNewOrder(symbol, order, clientOrderIdStuffed, areLocatesUsed);

            OrderResponse status = ResponseFactory.createOrderResponse(order, returnCode, null);

            log.debug("Returning SUCCESS for hedgeOsOrderId={}, clOrdId={}",
                    order.hedgeOsOrderId(), clientOrderIdStuffed);

            return status;
        }
        catch (Throwable t) {
            log.error("Error handling orderId={}", order.hedgeOsOrderId(), t);
            throw t;
        }
        finally {

            if (returnCode != OrderSubmitStatus.SUCCESSFUL_ORDER) {
                 // Order DID NOT GO TO MARKET
                orderCache.tryDeleteOrder(order.hedgeOsOrderId(), chain);
                // Note:  Important
                // if the order failed to go to market, give back the position limits and trade limit space
                returnLimitsAndPreTradeAmount(
                        order.hedgeOsOrderId(),
                        order.submitTimestampMillisUtc(),
                        chain,
                        orderQtySigned);
            }
        }
    }

    /**
     * Using name or id, find the EMS internal configured venue from the collection of
     * EmsExecutionVenues this EMS has started up.
     * The EmsExecutionVenue is how we map to FixAdapters, and to Locates
     *
     * @param order order for which we need a Venue
     * @return the mapped ExecutionVenue to trade on
     */
    private EmsExecutionVenue findExecutionVenueForOrder(Order order) {
        EmsExecutionVenue venue = venueMap.getById(order.venue().id());
        if (venue == null) {
            venue = venueMap.getByName(order.venue().name());
        }
        return venue;
    }

    private void returnLimitsAndPreTradeAmount(long hedgeOsOrderId,
                                               long orderSubmitTime,
                                               OrderChain chain,
                                               double orderQtySigned) {
        double reversedOrderQtySigned = -1.0d * orderQtySigned;
        // prevent accidental duplicate return of limits
        orderCache.getAndSetLimitsReturned(hedgeOsOrderId);
        // return the limits which we reserved above
        LocalDate orderTradeDate = chain.getOrderTradeDate();
        PmGroupSidKey key = chain.getKey();
        // CRITICAL; return the trade which failed to the trade limits
        tradeLimitsManager.recordNewTradePlacedInTracker(orderSubmitTime, key, hedgeOsOrderId,
                orderTradeDate, reversedOrderQtySigned);
    }

    public InternalLimitVerifyResult limitVerifyOrder(Order order, Connection connection) {

        // if the order fails data integrity checks, tell the user
        OrderResponse orderResponse = DataIntegrityChecker.dataIntegrityCheck(order, connection);
        if (orderResponse != null && orderResponse.status() != OrderSubmitStatus.SUCCESSFUL_ORDER) {
            // return ResponseFactory.createLimitVerifyResponse(builder, order, orderResponse.status(), orderResponse.errorMessage());
            return new InternalLimitVerifyResult(null, null, order,
                    orderResponse.status(), orderResponse.errorMessage());
        }

        // make a key for the venue map lookup
        EmsExecutionVenue venue = new EmsExecutionVenue();
        venue.setId(order.venue().id());

        // find the right venue
        FixAdapter fixAdapter = venueToFixAdapterMap.get(venue);
        if (fixAdapter == null) {
            // this should never happen, means users are making up venues
            String msg =
                    MessageFormat.format("No venue mapping exists. hedgeOsOrderId={0}",order.hedgeOsOrderId() + ", venue=" + order.venue().id());
            log.error(msg);
            // return ResponseFactory.createLimitVerifyResponse(builder, order, OrderSubmitStatus.NO_SUCH_VENUE_EXISTS, msg);
            return new InternalLimitVerifyResult(null, null, order,
                    OrderSubmitStatus.NO_SUCH_VENUE_EXISTS, msg);
        }

        // find the symbol adapter for the venue, and the symbol for the sid
        SymbolAdapter symbolAdapter = venueToSymbolAdapter.get(venue);
        String symbol = symbolAdapter.getSymbolForSid(order.sid());
        // send failure if we lack a symbol for this sid
        if (symbol == null) {
            String msg = MessageFormat.format("Symbol mapping failed for orderId={0}, sid={1}, symbology={2}",
                    order.hedgeOsOrderId(), order.sid(), symbolAdapter.getSymbology());
            log.error(msg);
            // return ResponseFactory.createLimitVerifyResponse(builder, order, OrderSubmitStatus.SYMBOL_MAPPING_FAILED, msg);
            return new InternalLimitVerifyResult(null, null, order,
                    OrderSubmitStatus.SYMBOL_MAPPING_FAILED, msg);
        }

        LocalDate tradeDate = TradingDayUtil.googleDateToLocalDate(order.tradeDate());
        OrderChain chain = new OrderChain(order, tradeDate, venue);

        // prevents error messages to the logs on this limit test
        boolean preCheck = true;

        // run risk checks using position_limits and trade_limits; must not over-trade
        TradingLimitCheckRowResult tradingLimitsCheck =
                tradeLimitsManager.checkTradingLimit(preCheck, chain.getKey(), chain.side(),
                        order.quantity(), order.submitTimestampMillisUtc(),
                        chain.getOrderTradeDate(),
                        order.hedgeOsOrderId());

        // if there is no position, make a fake temporary zero here
        EmsPosition position = positionManager.getPosition(tradeDate, chain.getKey());
        if (position == null) {
            position = new EmsPosition(chain.getKey(), new AtomicDouble(0.0d));
        }

        // run position risk limit checks
        PositionCheckRowResult positionLimitResult =
                positionLimitsManager.checkPositionLimits(preCheck, chain.getKey(),
                        order.side(), order.quantity(),order.hedgeOsOrderId(),
                        chain.getOrderTradeDate(), position);

        // return ResponseFactory.createLimitVerifyResult(builder, order, tradingLimitsCheck, positionLimitResult);
        return new InternalLimitVerifyResult(tradingLimitsCheck, positionLimitResult, order,
                OrderSubmitStatus.UNSET_ORDER_MESSAGE_STATUS, null);
    }

    public LocateCheckResult locateVerifyOrder(Order order) {

        LocalDate tradeDate = TradingDayUtil.googleDateToLocalDate(order.tradeDate());
        EmsExecutionVenue venue = findExecutionVenueForOrder(order);
        OrderChain chain = new OrderChain(order, tradeDate, venue);

        // get a position, but don't lock it; this is only verification
        EmsPosition emsPosition = positionManager.getPosition(tradeDate, chain.getKey());

        boolean isPreCheck = true;

        if (isSell(chain.side())) {

            double quantity = QuantityConverter.quantityToDouble(order.quantity());

            // try to get locates for this venue (if necessary)
            LocateCheckResult locateCheckResult =
                    locatesManager.atomicCheckLocateAndIncrementPreTradedAmounts(emsPosition,
                            order.hedgeOsOrderId(),
                            chain, isPreCheck, (long) quantity);

            return locateCheckResult;
            // need to put it in the Builder.
            // TODO:  Finish this method
        }
        return null;

    }

    private void stopFixAdapters() {
        // TODO:  Need to tear down all the FIX connections
        for (FixAdapter fixAdapter : venueToFixAdapterMap.values())
        {
            try {
                fixAdapter.stop();
            }
            catch (Exception e) {
                log.error("Error stopping fixAdapter:"+fixAdapter, e);
            }
        }
    }

    public void removeSubscriber(Connection connection, StreamObserver<EmsReport> responseObserver) {
        try {
            List<StreamObserver<EmsReport>> observerList = connectionMap.get(connection.connectionId());
            observerList.remove(responseObserver);
        }
        catch (Throwable t) {
            log.error("Throwable trying to remove broken connection, connId="+connection.connectionId(), t);
        }
    }

    public void stop() {
        // TODO:  Close connections in gRPC

        // TODO:  Then stop fix adapters
        stopFixAdapters();

    }

    /**
     * See locatesDesign.drawio, and the decision document.
     * @param positions positions loaded from the dated csv file.
     */
    public void setFileLoadedPositions(PositionLoadMap positions) {
        // TODO:  Careful about how this date is computed; should TRULY BE LOCAL
        LocalDate tradeDate = positions.getFileDate();

        // add a date to position manager for the file if not there
        positionManager.currentPositionByDate.putIfAbsent(tradeDate, new ConcurrentHashMap<>());

        // publish to the maps
        for (PositionFileRow positionFileRow : positions.getPositionFileRowMap().keySet()) {
            var key = new PmGroupSidKey(positionFileRow.getSid(), positionFileRow.getAccount());

            // get, then lock the position
            //    Need to prevent trades until we are done with this row
            EmsPosition knownPosition = positionManager.getPositionOrMakeNew(tradeDate, key);

            try {
                long resetTime = System.currentTimeMillis();
                // CAS lock the position
                knownPosition.lock.lock();
                // rest the trade amount on the position
                knownPosition.resetAmountTraded(resetTime, positionFileRow);
                // reset the position, logging the change for operational clarity
                double currentPosition = knownPosition.getPosition();
                knownPosition.setPosition(positionFileRow.getPosition());
                if (currentPosition != 0.0d) {
                    log.warn("Reset position for key={}, formerly={}, reset to={}", key, currentPosition, positionFileRow.getPosition());
                }
            }
            finally {
                knownPosition.lock.unlock();
            }
        }
    }

}
