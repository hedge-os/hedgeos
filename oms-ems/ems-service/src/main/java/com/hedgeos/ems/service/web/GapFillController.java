package com.hedgeos.ems.service.web;

import com.hedgeos.ems.service.EmsMultiplexer;
import com.hedgeos.fix.adapter.FixAdapter;
import lombok.extern.slf4j.Slf4j;
import order.OrderSubmitStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;

@Controller
@RequestMapping("/fix")
@Slf4j
public class GapFillController {

    public static final String EMS_MULTIPLEXER = "EMS_MULTIPLEXER_KEY";
    public static final String FIX_ADAPTER_MAP = "FIX_ADAPTER_MAP_KEY";
    public static final String FIX_CONFIG_MAP = "FIX_CONFIG_MAP_KEY";

    public static final String GAP_FILL_SENT = "GAP_FILL_SENT";
    public static final String STATUS = "FIX_EMS_VIEW_STATUS";

    // fields of the GapFill
    public static final String FIX_ADAPTER_NAME_KEY = "FIX_ADAPTER_NAME_KEY";
    public static final String FIX_ADAPTER_GAP_FILL_FROM_KEY = "FIX_ADAPTER_GAP_FILL_FROM_KEY";
    public static final String FIX_ADAPTER_GAP_FILL_TO_KEY = "FIX_ADAPTER_GAP_FILL_TO_KEY";
    private static final String ERROR_GAP_FILL_STATUS = "ERROR_GAP_FILL_STATUS";
    private static final String ERROR_FROM_MORE_THAN_TO = "ERROR_GAP_FILL_FROM_GREATER_THAN_TO";
    private static final String FAILED_SEND_GAP_FILL_STATUS = "FAILED_SEND_GAP_FILL_STATUS";
    private static final String ERROR_GAP_FILL_NO_NAME = "ERROR_GAP_FILL_NO_NAME";


    public final ApplicationContext context;
    public final EmsMultiplexer emsMultiplexer;

    public GapFillController(@Autowired ApplicationContext context) {
        this.context = context;
        this.emsMultiplexer = context.getBean(EmsMultiplexer.class);
    }

    // http://localhost:8080/fix/view
    @GetMapping(value = {"/view"})
    public String viewLimits(Model model) {
        model.addAttribute(EMS_MULTIPLEXER, emsMultiplexer);
        model.addAttribute(FIX_ADAPTER_MAP, emsMultiplexer.getFixAdapterMap());
        model.addAttribute(FIX_CONFIG_MAP, emsMultiplexer.getFixConfigMap());
        return "view-fix-adapters"; // links to view-limits.jsp
    }

    @GetMapping(value = {"/gapFill"})
    public String gapFill(Model model,
                          @ModelAttribute(FIX_ADAPTER_NAME_KEY) String name,
                          @ModelAttribute(FIX_ADAPTER_GAP_FILL_FROM_KEY) String fromString,
                          @ModelAttribute(FIX_ADAPTER_GAP_FILL_TO_KEY) String toString)
    {
        if (name == null) {
            log.warn("/fix/gapFill called without the name of the FixAdapter to gapFill.");
            model.addAttribute(STATUS, ERROR_GAP_FILL_NO_NAME);
            return viewLimits(model);
        }

        FixAdapter fixAdapter = emsMultiplexer.getFixAdapterMap().get(name);

        if (fromString == null || toString == null || fixAdapter == null) {
            log.error("Error sending GapFill; form not populated");
            model.addAttribute(STATUS, ERROR_GAP_FILL_STATUS);
            return viewLimits(model);
        }

        int from = Integer.parseInt(fromString);
        int to = Integer.parseInt(toString);
        if (from > to) {
            log.error("Error sending GapFill; from is > to");
            model.addAttribute(STATUS, ERROR_FROM_MORE_THAN_TO);
            return viewLimits(model);
        }

        log.error("GapFill triggered from web page, name="+fixAdapter.getName()+", from="+fromString+", to="+toString);

        // send the gap fill
        int result = fixAdapter.sendGapFill(from, to);

        if (result == OrderSubmitStatus.SUCCESSFUL_ORDER) {
            log.warn("Success; Gap fill sent for FixAdapter="+name+", from="+from+", to="+to);
            model.addAttribute(STATUS, GAP_FILL_SENT+", result="+result);
        }
        else {
            log.error("Failed to send GapFill, status="+result);
            model.addAttribute(STATUS, FAILED_SEND_GAP_FILL_STATUS+"="+result);
        }
        return viewLimits(model);
    }
}
