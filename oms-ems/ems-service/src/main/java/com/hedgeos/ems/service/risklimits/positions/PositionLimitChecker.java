package com.hedgeos.ems.service.risklimits.positions;

import com.hedgeos.ems.csvfiles.risklimits.position.PositionLimitLoad;
import com.hedgeos.ems.csvfiles.risklimits.position.PositionLimitsFileRow;
import com.hedgeos.ems.order.OrderUtil;
import com.hedgeos.ems.order.EmsPosition;
import com.hedgeos.ems.service.PositionManager;
import com.hedgeos.ems.order.PmGroupSidKey;
import com.hedgeos.ems.util.TradingDayUtil;
import general.Quantity;
import lombok.extern.slf4j.Slf4j;
import order.OrderSubmitStatus;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.hedgeos.ems.util.PrecisionConstants.isGreaterThan;
import static com.hedgeos.ems.util.QuantityConverter.quantityToDouble;

/**
 * The PositionChecker holds an optimized view of the position limits and positions for a given day,
 * to verify limits are not breached when an order is placed.
 */
@Slf4j
public class PositionLimitChecker {

    public static final PositionCheckRowResult POSITION_CHECK_SUCCESS =
            new PositionCheckRowResult(null, OrderSubmitStatus.SUCCESSFUL_ORDER, null,
                    0L, 0L, 0L);
    // when these limits were loaded
    private final LocalDate createDate;
    public LocalDate fileDate;

    // the rows which start the position limits file
    public final PositionLimitsFileRow pmGroupWildcardRowNoSid;
    public final PositionLimitsFileRow sidWildcardRowNoAccount;
    public final Map<String, PositionLimitsFileRow> perPmGroupLimitsWildcardSid;
    public final Map<Long, PositionLimitsFileRow> perSidLimitsWildcardPmGroup;
    public final Map<PmGroupSidKey, PositionLimitsFileRow> specificPmGroupAndSid;

    public final PositionManager positionManager;

    public PositionLimitChecker(PositionLimitLoad positionLimits,
                                PositionManager positionManager) {

        this.fileDate = positionLimits.getFileDate();

        // clear the maps, any old limits
        Map<PmGroupSidKey, PositionLimitsFileRow> specificAccountAndSid1 = new ConcurrentHashMap<>();
        Map<Long, PositionLimitsFileRow> perSidLimitsWildcardAccount1 = new ConcurrentHashMap<>();
        Map<String, PositionLimitsFileRow> perAccountLimitsWildcardSid1 = new ConcurrentHashMap<>();
        PositionLimitsFileRow accountWildcardRowNoSid1 = null;
        PositionLimitsFileRow sidWildcardRowNoAccount1 = null;

        // load the maps with the limits file contents
        for (PositionLimitsFileRow row : positionLimits.getPositionLimitsMap().keySet()) {
            if (row.isPmGroupWildcard() && row.isSidWildcard()) {
                log.error("Should never be a row with wildcard sid and account");
            }
            else if (row.isPmGroupWildcard() && row.getSid() == null) {
                accountWildcardRowNoSid1 = row;
            }
            else if (row.isSidWildcard() && row.getPmGroup() == null) {
                sidWildcardRowNoAccount1 = row;
            }
            else if (row.isPmGroupWildcard() && row.getSid() != null) {
                perSidLimitsWildcardAccount1.put(row.getSid(), row);
            }
            else if (row.isSidWildcard() && row.getPmGroup() != null) {
                perAccountLimitsWildcardSid1.put(row.getPmGroup(), row);
            }
            else {
                PmGroupSidKey pmGroupSidKey = new PmGroupSidKey(row.getSid(), row.getPmGroup());
                specificAccountAndSid1.put(pmGroupSidKey, row);
            }
        }

        this.specificPmGroupAndSid = specificAccountAndSid1;
        this.perPmGroupLimitsWildcardSid = perAccountLimitsWildcardSid1;
        this.perSidLimitsWildcardPmGroup = perSidLimitsWildcardAccount1;
        this.pmGroupWildcardRowNoSid = accountWildcardRowNoSid1;
        this.sidWildcardRowNoAccount = sidWildcardRowNoAccount1;
        this.positionManager = positionManager;
        this.createDate = TradingDayUtil.getTradeDate();
        // a freeze fence is inserted here for any file variables, and their contents
    }

    public PositionCheckRowResult checkPositionLimit(boolean preCheck,
                                                     LocalDate tradeDate,
                                                     PmGroupSidKey key,
                                                     Quantity quantity,
                                                     long hedgeOsOrderId,
                                                     int side,
                                                     EmsPosition position)
    {

        // check for a match of both account and sid from the limits file
        PositionLimitsFileRow specificLimitRow = specificPmGroupAndSid.get(key);

        // if a position, use it; otherwise, use 0.0
        double currentPosition = 0.0d;
        if (position != null) {
            currentPosition = position.getOpenPosition();
        }

        // TODO:  Exposure checks using order price, trading and position price data.
        // Double price = null;

        double directionalQuantity = OrderUtil.getDirectionalQuantity(quantity, side);
        double newPositionQty = currentPosition + directionalQuantity;

        if (specificLimitRow != null) {
            // This is the Tier 1 check in the decision doc 0010
            // if a specific limit exists, then only this check is performed
            return tierOneSpecificLimitCheck(preCheck, specificLimitRow, position, newPositionQty, quantity, hedgeOsOrderId);
        }

        PositionCheckRowResult tierTwoCheckResult =
                tierTwoChecks(preCheck, quantity, key, newPositionQty, currentPosition, hedgeOsOrderId);
        // if any limit tests were run, we are complete
        if (tierTwoCheckResult != null) {
            return tierTwoCheckResult;
        }

        // if no limit tests at tier1 or tier2, run tier3
        return tierThreeChecks(preCheck, quantity, hedgeOsOrderId, newPositionQty, currentPosition);
    }


    /**
     * Check the short limit was not violated; with a nicety in the event
     * operators set the value to be negative, consider it the quantity short
     * which is what the system expects.
     *
     * @param positionQty the order qty requested
     * @param qtyShort the short limit
     * @return if the limit was violated
     */
    static boolean violatedShortLimit(double positionQty, long qtyShort) {
        long positiveShortQty = Math.abs(qtyShort);
        double inversePosQty = -1 * positionQty;
        return isGreaterThan(inversePosQty, positiveShortQty);
    }

    static boolean violatedLongLimit(double positionQty, long limitQtyLong) {
        return isGreaterThan(positionQty, limitQtyLong);
    }

    private PositionCheckRowResult tierOneSpecificLimitCheck(
            boolean preCheck,
            PositionLimitsFileRow specificLimitRow,
            EmsPosition position,
            Double newPositionQty,
            Quantity quantity,
            long hedgeOsOrderId)
    {
        if (violatedLongLimit(newPositionQty, specificLimitRow.getQtyLong()))
        {
            // note:  we need a highly detailed error message; as this is typically an emergency
            double orderQty = quantityToDouble(quantity);
            // note: logging the row prints the exact file which generate the row, a base limit or overlay file.
            // TODO:  We may want the login for this orderId, purely for these errors
            String msg = MessageFormat.format("Violated specific qtyLong position check, " +
                            "row={0}, newPositionQty={1}, positionQty={2}, " +
                            "orderQty={3}, orderId={4}",
                    specificLimitRow, newPositionQty, position.getPosition(),
                    orderQty, hedgeOsOrderId);
            if (!preCheck)
                log.error(msg);
            return new PositionCheckRowResult(specificLimitRow,
                    OrderSubmitStatus.POSITION_LIMIT_BREACHED_QTY_LONG, msg,
                    specificLimitRow.getQtyLong(), position.getPosition(), newPositionQty.longValue());
        }

        if (violatedShortLimit(newPositionQty, specificLimitRow.getQtyShort())) {
            double orderQty = quantityToDouble(quantity);
            String msg = MessageFormat.format(
                    "Violated qtyShort position check for " +
                            "row={0}, newPositionQty={1}, positionQty={2}, " +
                    "orderQty={3}, orderId={4}",
                    specificLimitRow, newPositionQty, position, orderQty, hedgeOsOrderId);
            if (!preCheck)
                log.error(msg);
            return new PositionCheckRowResult(specificLimitRow,
                    OrderSubmitStatus.POSITION_LIMIT_BREACHED_QTY_SHORT, msg,
                    specificLimitRow.getQtyShort(), position.getPosition(), newPositionQty.longValue());
        }

        if (preCheck) {
            // return success with additional messaging
            return new PositionCheckRowResult(specificLimitRow, OrderSubmitStatus.SUCCESSFUL_ORDER, "success checking position limit",
                    specificLimitRow.getQtyLong(), position.getPosition(), newPositionQty);
        }

        // if the specific limit row exists, then it is the only limit check performed
        return POSITION_CHECK_SUCCESS;
    }

    /**
     * This is the Tier2 checks in the decision doc 0010
     *
     * @param preCheck
     * @param newPositionQty the orderQty, not re-converted to save latency
     * @param position       the current position
     * @return the Integer result if we know the result, otherwise null means, need to keep checking limits
     */
    private PositionCheckRowResult tierTwoChecks(
            boolean preCheck,
            Quantity quantity,
            PmGroupSidKey key,
            Double newPositionQty,
            double position,
            long hedgeOsOrderId)
    {
        PositionLimitsFileRow specificSidAccountWildcard = perSidLimitsWildcardPmGroup.get(key.sid);
        PositionLimitsFileRow specificAccountWildcardSid = perPmGroupLimitsWildcardSid.get(key.pmGroup);

        if (specificSidAccountWildcard != null) {
            if (violatedLongLimit(newPositionQty, specificSidAccountWildcard.getQtyLong())) {
                double orderQty = quantityToDouble(quantity);
                String msg =
                        MessageFormat.format("Violated specific SID, account=*, " +
                                "qtyLong position check, row={0}, " +
                                        "newPositionQty={1}, " +
                                        "positionQty={2}, orderQty={3}, orderId={4}",
                        specificSidAccountWildcard, newPositionQty, position,
                                orderQty, hedgeOsOrderId);
                if (!preCheck)
                    log.error(msg);
                return new PositionCheckRowResult(specificSidAccountWildcard,
                        OrderSubmitStatus.ACCOUNT_WILDCARD_POSITION_LIMIT_BREACHED_QTY_LONG, msg,
                        specificSidAccountWildcard.getQtyLong(), position,
                        newPositionQty.longValue());
            }

            if (violatedShortLimit(newPositionQty, specificSidAccountWildcard.getQtyShort())) {
                double orderQty = quantityToDouble(quantity);
                String msg = MessageFormat.format("Violated specific SID, account=*, " +
                        "qtyShort position check, row={0}, newPositionQty={1}, positionQty={2}, orderQty={3}," +
                                " orderId={4}",
                        specificSidAccountWildcard, newPositionQty, position, orderQty, hedgeOsOrderId);
                if (!preCheck)
                    log.error(msg);
                return new PositionCheckRowResult(specificSidAccountWildcard,
                        OrderSubmitStatus.ACCOUNT_WILDCARD_POSITION_LIMIT_BREACHED_QTY_LONG, msg,
                        specificAccountWildcardSid.getQtyShort(), position, newPositionQty);
            }
        }

        if (specificAccountWildcardSid != null) {
            if (violatedLongLimit(newPositionQty, specificAccountWildcardSid.getQtyLong())) {
                double orderQty = quantityToDouble(quantity);
                String msg = MessageFormat.format("Violated qtyLong, specific account sid=*, " +
                                "position check, row={0}, newPositionQty={1}, positionQty={2}, orderQty={3}, " +
                                "orderId={4}",
                        specificAccountWildcardSid, newPositionQty, position, orderQty, hedgeOsOrderId);
                if (!preCheck)
                    log.error(msg);
                return new PositionCheckRowResult(specificAccountWildcardSid,
                        OrderSubmitStatus.SID_WILDCARD_POSITION_LIMIT_BREACHED_QTY_LONG, msg,
                        specificAccountWildcardSid.getQtyLong(), position, newPositionQty);
            }

            if (violatedShortLimit(newPositionQty, specificAccountWildcardSid.getQtyShort())) {
                double orderQty = quantityToDouble(quantity);
                String msg = MessageFormat.format("Violated qtyShort, specific account,wildcard sid=*, " +
                        "account qtyShort position check, " +
                                "row={0}, newPositionQty={1}, positionQty={2}, orderQty={3}, orderId={4}",
                        specificAccountWildcardSid, newPositionQty, position, orderQty, hedgeOsOrderId);
                if (!preCheck)
                    log.error(msg);
                return new PositionCheckRowResult(specificAccountWildcardSid,
                        OrderSubmitStatus.SID_WILDCARD_POSITION_LIMIT_BREACHED_QTY_LONG, msg,
                        specificSidAccountWildcard.getQtyShort(), position, newPositionQty);
            }
        }

        // per account tier 2 check, both passed, return success; otherwise, keep checking
        if (specificSidAccountWildcard != null && specificAccountWildcardSid != null) {
            if (preCheck) {
                // checked multiple rows; return success, the current and new quantity
               return new PositionCheckRowResult(specificSidAccountWildcard,
                       OrderSubmitStatus.SUCCESSFUL_ORDER,
                       "SUCCESS", 0L, position, newPositionQty);
            }

            // cache the success when not 'preCheck' mode, for speed
            return POSITION_CHECK_SUCCESS;
        }

        // null = no checks performed, will cause the tier three checks to run
        return null;
    }

    /**
     * The third and final tier of checks, if nothing has matched, check against fail-safe rows for any account
     * and any SID.
     *
     * @param preCheck
     * @param newPositionQty the quantity which results if the trade is executed
     * @param position       the current position the system knows
     * @return an integer OrderSubmitStatus code value from the ems.proto api, sent to the client if not success.
     */
    private PositionCheckRowResult tierThreeChecks(boolean preCheck, Quantity quantity,
                                                   long hedgeOsOrderId, double newPositionQty,
                                                   double position) {

        if (sidWildcardRowNoAccount != null) {
            if (violatedLongLimit(newPositionQty, sidWildcardRowNoAccount.getQtyLong())) {
                double orderQty = quantityToDouble(quantity);
                String msg = MessageFormat.format("Violated qtyLong sid wildcard, account=blank, " +
                                "position check, row={0}, newPositionQty={1}, positionQty={2}, orderQty={3}, orderId={4}",
                        sidWildcardRowNoAccount, newPositionQty, position, orderQty, hedgeOsOrderId);
                if (!preCheck)
                    log.error(msg);
                return new PositionCheckRowResult(sidWildcardRowNoAccount,
                        OrderSubmitStatus.SID_WILDCARD_BLANK_ACCOUNT_POSITION_LIMIT_BREACHED_QTY_LONG, msg,
                        sidWildcardRowNoAccount.getQtyLong(), position, newPositionQty);
            }

            if (violatedShortLimit(newPositionQty, sidWildcardRowNoAccount.getQtyShort())) {
                double orderQty = quantityToDouble(quantity);
                String msg = MessageFormat.format("Violated qtyShort sid wildcard, account=blank, " +
                                "position check, row={0}, newPositionQty={1}, positionQty={2}, orderQty={3}, orderId={4}",
                        sidWildcardRowNoAccount, newPositionQty, position, orderQty, hedgeOsOrderId);
                if (!preCheck)
                    log.error(msg);

                return new PositionCheckRowResult(sidWildcardRowNoAccount,
                        OrderSubmitStatus.SID_WILDCARD_BLANK_ACCOUNT_POSITION_LIMIT_BREACHED_QTY_SHORT, msg,
                        sidWildcardRowNoAccount.getQtyShort(), position, newPositionQty);
            }
        }

        if (pmGroupWildcardRowNoSid != null) {
            if (violatedLongLimit(newPositionQty, pmGroupWildcardRowNoSid.getQtyLong())) {
                String msg = MessageFormat.format("Violated qtyLong account wildcard, sid=blank, " +
                        "position check, row={0}, orderQty={1}, orderId={2}",
                        pmGroupWildcardRowNoSid, newPositionQty, hedgeOsOrderId);
                if (!preCheck)
                    log.error(msg);

                return new PositionCheckRowResult(pmGroupWildcardRowNoSid,
                        OrderSubmitStatus.ACCOUNT_WILDCARD_BLANK_SID_POSITION_LIMIT_BREACHED_QTY_LONG, msg,
                        pmGroupWildcardRowNoSid.getQtyLong(), position, newPositionQty);
            }

            if (violatedShortLimit(newPositionQty, pmGroupWildcardRowNoSid.getQtyShort())) {
                String msg = MessageFormat.format("Violated qtyShort account wildcard, " +
                                "sid=blank, position check, row={0}, orderQty={1}, orderId={2}",
                        pmGroupWildcardRowNoSid, newPositionQty, hedgeOsOrderId);
                if (!preCheck)
                    log.error(msg);
                return new PositionCheckRowResult(pmGroupWildcardRowNoSid,
                        OrderSubmitStatus.ACCOUNT_WILDCARD_BLANK_SID_POSITION_LIMIT_BREACHED_QTY_SHORT, msg,
                        pmGroupWildcardRowNoSid.getQtyShort(), position, newPositionQty);
            }
        }

        return POSITION_CHECK_SUCCESS;
    }


}
