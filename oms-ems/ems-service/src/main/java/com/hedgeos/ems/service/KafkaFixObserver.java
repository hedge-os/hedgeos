package com.hedgeos.ems.service;

import com.hedgeos.fix.adapter.FixObserver;
import order.EmsReport;
import order.EmsReportEnvelope;

public class KafkaFixObserver implements FixObserver {

    @Override
    public void onMessage(EmsReportEnvelope envelope, EmsReport emsReport) {
        // TODO:  Bootstrap to Kafka on campus, push proto messages or JSON
    }
}
