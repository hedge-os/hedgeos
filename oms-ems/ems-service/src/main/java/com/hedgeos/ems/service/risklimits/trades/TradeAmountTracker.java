package com.hedgeos.ems.service.risklimits.trades;

import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitDurations;
import com.hedgeos.ems.order.PmGroupSidKey;
import com.hedgeos.ems.service.risklimits.trades.cleaners.*;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLongArray;

import static com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitDurations.*;

/**
 * Important class, which maintains a "block" of ring buffers (AtomicIntegerArray)
 * for each of the time periods for which limits are supported on the system.
 *
 * You need an area of the ring buffer which is not read; to allow for cleaning
 * when the buffer wraps.
 *
 * The ring buffers are 2x the period of the limit (for simplicity.)
 *
 * Namely the following six ring buffers:
 * 1.  1 second:  tenths of a second array (size 20)
 * 2.  5 seconds:  tenths of a second array (size 100)
 * 3.  1 minute:  seconds array (size 120)
 * 4.  5 minutes
 * 6.  1 hour
 * 7.  1 day
 *
 * The "checker", finds the current offset in the ring, and looks backwards, summing
 * one period worth of the sub-periods.  e.g. for tenths of second array, 10 of them are
 * summed to determine how many trades occurred in the last one-second window.
 *
 * While this is happening, a cleaner thread is zeroing the other half of the array
 * which is not being looked at.  This cleaner thread, runs every half second.
 */
@Slf4j
@Component
public class TradeAmountTracker {
    public static final double NO_TRADES_YET = 0.0;

    // lengths of the ring buffers

    static final int TWO_SECONDS_IN_TENTHS_ARRAY_LENGTH = 10*2;
    static final int TEN_SECONDS_TENTHS_ARRAY_LENGTH = 10 * 10;
    static final int MINUTES_ARRAY_LENGTH = 60;
    static final int TWO_HOURS_MINUTES_ARRAY_LENGTH = MINUTES_ARRAY_LENGTH * 2;
    static int MINUTES_IN_HOUR = 60;
    static final int SECONDS_ARRAY_LENGTH = 60;
    static int TWO_MINUTES_SECONDS_ARRAY_LENGTH = 2 * SECONDS_ARRAY_LENGTH;

    static final int HOURS_IN_DAY = 24;
    static final int TWO_DAYS_HOURS_ARRAY_LENGTH = HOURS_IN_DAY * 2;
    static final int SECONDS_IN_MINUTE = 60;
    static final int FIVE_SECOND_PERIODS_IN_FIVE_MIN = 60; //5*60/5
    static final int TEN_MINUTES_FIVE_SECONDS_SIZE = (60 / 5) * 10;

    // for each pmGroup and sid --> 60 seconds, divide up to tenths of a second
    // --> One minute, 60 seconds; need to add the last 60 seconds
    // --> One hour, 60 minutes, need to add the last 60 minutes
    // --> One day, 24 hours, need to add the last 24 hours

    // clean thresholds
    public static final int HALF_SECOND_MILLIS_CLEAN_TWO_SECOND_BUFFER = 500;
    public static final int THREE_SECOND_MILLIS_CLEAN_TEN_SECOND_BUFFER = 3000;
    public static final Duration TWENTY_SECONDS = Duration.ofSeconds(20);
    private static final Duration THREE_MINUTES = Duration.ofMinutes(3);
    private static final Duration TWENTY_MINUTES = Duration.ofMinutes(20);
    private static final Duration THREE_HOURS_MILLIS = Duration.ofHours(3);

    // for a specific problem; where we try to give back trading limits
    // we need to know how far the lookback window is, based on bucket size.
    public static final long TWO_SECONDS_BUCKETS_TO_CLEAN_COUNT = getCleanAreaSize(TWO_SECONDS_IN_TENTHS_ARRAY_LENGTH);
    public static final long TENTHS_BUCKET_SIZE_MILLIS = 100L;

    // will always be 1000ms; calculated in the event the function to clean changes
    private static final long ONE_SECOND_LOOKBACK_MILLIS =
            TENTHS_BUCKET_SIZE_MILLIS * TWO_SECONDS_BUCKETS_TO_CLEAN_COUNT;

    // will always be 5000ms; calculated in the event the clean function changes
    public static final long TEN_SECONDS_BUCKETS_TO_CLEAN_COUNT = getCleanAreaSize(TEN_SECONDS_TENTHS_ARRAY_LENGTH);
    private static final long FIVE_SECOND_LOOKBACK_MILLIS =
            TENTHS_BUCKET_SIZE_MILLIS * TEN_SECONDS_BUCKETS_TO_CLEAN_COUNT;

    public static final long SECONDS_BUCKET_SIZE_MILLIS = 1000L;

    public static final long TWO_MIN_BUCKETS_TO_CLEAN_COUNLT =
            getCleanAreaSize(TWO_MINUTES_SECONDS_ARRAY_LENGTH);
    private static final long ONE_MIN_LOOKBACK_MILLIS =
            SECONDS_BUCKET_SIZE_MILLIS * TWO_MIN_BUCKETS_TO_CLEAN_COUNLT;

    private final static long FIVE_SECOND_BUCKET_SIZE_MILLIS = 5 * 1000;

    private final static long TEN_MIN_BUCKET_TO_CLEAN_COUNT =
            getCleanAreaSize(TEN_MINUTES_FIVE_SECONDS_SIZE);
    private static final long FIVE_MIN_LOOKBACK_MILLIS =
            FIVE_SECOND_BUCKET_SIZE_MILLIS * TEN_MIN_BUCKET_TO_CLEAN_COUNT;

    private static final long ONE_MINUTE_BUCKET_SIZE_MILLIS = 60*1000;
    private static final long ONE_HOURS_MINUTE_BUCKET_TO_CLEAN_COUNT =
            getCleanAreaSize(TWO_HOURS_MINUTES_ARRAY_LENGTH);
    private static final long ONE_HOUR_LOOKBACK_MILLIS =
            ONE_MINUTE_BUCKET_SIZE_MILLIS * ONE_HOURS_MINUTE_BUCKET_TO_CLEAN_COUNT;
    private static final long ONE_HOUR_BUCKET_SIZE_MILLIS = 60*60*1000;
    private static final long TWELVE_HOURS_BUCKET_TO_CLEAN_COUNT =
            getCleanAreaSize(TWO_DAYS_HOURS_ARRAY_LENGTH);
    private static final long ONE_DAY_LOOKBACK_MILLIS =
            ONE_HOUR_BUCKET_SIZE_MILLIS * TWELVE_HOURS_BUCKET_TO_CLEAN_COUNT;

//    static {
//        log.warn("ONE_SECOND_LOOKBACK_MILLIS={}",ONE_SECOND_LOOKBACK_MILLIS);
//        log.warn("FIVE_SECOND_LOOKBACK_MILLIS={}",FIVE_SECOND_LOOKBACK_MILLIS);
//        log.warn("ONE_MIN_LOOKBACK_MILLIS={}",ONE_MIN_LOOKBACK_MILLIS);
//        log.warn("FIVE_MIN_LOOKBACK_MILLIS={}",FIVE_MIN_LOOKBACK_MILLIS);
//        log.warn("ONE_HOUR_LOOKBACK_MILLIS={}",ONE_HOUR_LOOKBACK_MILLIS);
//        log.warn("ONE_DAY_LOOKBACK_MILLIS={}",ONE_DAY_LOOKBACK_MILLIS);
//    }

    // this map holds all the trading per pmGroup, per security

    // TODO:  Change back to a CHM for speed; ordered for now
    public final Map<PmGroupSidKey, TradeActivityBlocks> tradingPerSecurity =
            new ConcurrentSkipListMap<>();

    // micro-meter observe how long the limits cleaner takes.
    private final MeterRegistry meterRegistry;

    // ----------------------------------------------------------------------
    // We need to sweep every security,
    // and then clean one particular ring for the security
    // these are the six cleaners which are cleaning each ring buffer across
    // all securities.
    // --------------------------------------------------------------------
    public final TradeBlockCleaner secondCleaner;
    private final TradeBlockCleaner fiveSecondCleaner;
    private final TradeBlockCleaner minuteCleaner;
    private final TradeBlockCleaner fiveMinuteCleaner;
    private final TradeBlockCleaner hourCleaner;
    private final TradeBlockCleaner dayCleaner;

    public TradeAmountTracker(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;

        // cleaner runners
        secondCleaner = new TradeBlockCleaner("second", HALF_SECOND_MILLIS_CLEAN_TWO_SECOND_BUFFER,
                this.tradingPerSecurity, new TwoSecondsTenthsCleaner(), meterRegistry);
        fiveSecondCleaner = new TradeBlockCleaner("five-second", TradeAmountTracker.THREE_SECOND_MILLIS_CLEAN_TEN_SECOND_BUFFER, this.tradingPerSecurity,
                new TenSecondsTenthsCleaner(), meterRegistry);
        minuteCleaner = new TradeBlockCleaner("minute",TWENTY_SECONDS.toMillis(), tradingPerSecurity,
                new TwoMinutesSecondsCleaner(), meterRegistry);
        fiveMinuteCleaner = new TradeBlockCleaner("five-minute",THREE_MINUTES.toMillis(), tradingPerSecurity,
                new TenMinutesFiveSecondsCleaner(), meterRegistry);
        hourCleaner = new TradeBlockCleaner("hour",TWENTY_MINUTES.toMillis(), tradingPerSecurity,
                new TwoHoursOneMinuteCleaner(), meterRegistry);
        dayCleaner = new TradeBlockCleaner("day",THREE_HOURS_MILLIS.toMillis(), tradingPerSecurity,
                new TwoDaysOneHourCleaner(), meterRegistry);

        startLimitRingBufferCleanerThreads();
    }

    private void startLimitRingBufferCleanerThreads() {
        // TODO:  Need to shut these down on shutdown

        // Note:  Don't put this on a schedule, we want it to have a thread, and run forever
        // one thread is cleaning seconds
        ExecutorService fastPerSecondCleaner = Executors.newFixedThreadPool(1);
        fastPerSecondCleaner.submit(secondCleaner);

        // This thread is fairly time sensitive; has five seconds to complete, gets a thread pool
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.submit(fiveSecondCleaner);

        // the remaining four cleaners, run slowly; can run serially on one thread pool
        ExecutorService executorServiceSlowCleaners = Executors.newFixedThreadPool(1);
        executorServiceSlowCleaners.submit(minuteCleaner);
        executorServiceSlowCleaners.submit(fiveMinuteCleaner);
        executorServiceSlowCleaners.submit(hourCleaner);
        executorServiceSlowCleaners.submit(dayCleaner);
    }

    public void clearAllLimits() {
        for (PmGroupSidKey pmGroupSidKey : tradingPerSecurity.keySet()) {
            clearOneLimit(pmGroupSidKey);
        }
    }

    public void clearOneLimit(PmGroupSidKey pmGroupSidKey) {
        TradeActivityBlocks blocks = this.tradingPerSecurity.get(pmGroupSidKey);
        blocks.clean();
    }


    public static class TradeActivityBlocks {
        public static final long CLEAR_THREAD_VALUE = 0;
        // used to measure one-second periods
        public final AtomicLongArray lastTwoSecondsTenths = new AtomicLongArray(TWO_SECONDS_IN_TENTHS_ARRAY_LENGTH);
        // used to measure five-second buckets
        public final AtomicLongArray lastTenSecondsTenths = new AtomicLongArray(TEN_SECONDS_TENTHS_ARRAY_LENGTH);
        // use to measure minutes
        public final AtomicLongArray lastTwoMinutesSeconds = new AtomicLongArray(TWO_MINUTES_SECONDS_ARRAY_LENGTH);
        // used to measure five minutes in five second intervals
        public final AtomicLongArray lastTenMinutesFiveSeconds = new AtomicLongArray(TEN_MINUTES_FIVE_SECONDS_SIZE);
        // used to measure one hour; the last hour
        public final AtomicLongArray lastTwoHoursMinutes = new AtomicLongArray(TWO_HOURS_MINUTES_ARRAY_LENGTH);
        // used to measure days
        public final AtomicLongArray lastTwoDaysInHours = new AtomicLongArray(TWO_DAYS_HOURS_ARRAY_LENGTH);

        private final PmGroupSidKey key;

        public TradeActivityBlocks(PmGroupSidKey key) {
            this.key = key;
        }

        public void cleanHalfAheadOfNow(AtomicLongArray arrayToClean, int currentIndex) {
            int toClean = getCleanAreaSize(arrayToClean.length());
            // Offset ahead by 2 because, in case the millisecond rolls while we clean
            // (avoids cleaning a tenths of second during which we are trading)
            int aheadOneTenth = currentIndex+2;
            // clean half the array ahead of us
            for (int i=0; i<toClean; i++) {
                int loop = (aheadOneTenth + i) % arrayToClean.length();
                arrayToClean.set(loop, CLEAR_THREAD_VALUE);
            }
        }

        public void clean(AtomicLongArray atomicLongArray) {
            for (int i=0; i<atomicLongArray.length(); i++) {
                atomicLongArray.set(i, CLEAR_THREAD_VALUE);
            }
        }

        public void clean() {
            clean(lastTwoSecondsTenths);
            clean(lastTenSecondsTenths);
            clean(lastTwoMinutesSeconds);
            clean(lastTenMinutesFiveSeconds);
            clean(lastTwoHoursMinutes);
            clean(lastTwoDaysInHours);
        }
    }

    /**
     * We will clean half the array every time the cleaner runs.
     * Also, need to know the number of buckets to calculate the look back window size.
     * @param arrayToCleanSize the size of the array being cleaned
     * @return how many buckets are cleaned
     */
    private static int getCleanAreaSize(int arrayToCleanSize) {
        int toClean = arrayToCleanSize / 2;
        return toClean;
    }

    public Collection<Integer> getAllLimits(PmGroupSidKey key, long now) {

        Map<Duration, Integer> amountsTraded =
                getAmountsTraded(Arrays.asList(TradeLimitDurations.DURATIONS), key, now);
        return amountsTraded.values();
    }


    public Map<Duration, Integer> getAmountsTraded(List<Duration> durationList,
                                                   PmGroupSidKey key,
                                                   long timestamp)
    {
        // only one hash map lookup
        TradeActivityBlocks blocks = tradingPerSecurity.get(key);

        Map<Duration, Integer> resultMap = new LinkedHashMap<>();

        for (Duration duration : durationList) {
            // walk across the array requested
            if (duration.equals(ONE_SECOND_LIMIT)) {
                int totalQty = 0;
                if (blocks != null) {
                    // find where we should start in the two seconds ring buffer
                    int start = twoSecondTenthsOffset(timestamp);
                    // move ten spots in the tenths range = 1 second
                    for (int i=0; i < 10; i++) {
                        int loop = betterModulo(start-i, blocks.lastTwoSecondsTenths.length());
                        totalQty += blocks.lastTwoSecondsTenths.get(loop);
                    }
                }
                resultMap.put(duration, totalQty);
            }
            else if (duration.equals(FIVE_SECOND_LIMIT)) {
                int totalQty = 0;
                if (blocks != null) {
                    int start = tenSecondTenthsOffset(timestamp);
                    // 50 iterations on the tenths right buffer will get us five seconds worth
                    for (int i = 0; i < 50; i++) {
                        int loop = betterModulo(start-i, blocks.lastTenSecondsTenths.length());
                        totalQty += blocks.lastTenSecondsTenths.get(loop);
                    }
                }
                resultMap.put(duration, totalQty);
            }
            else if (duration.equals(ONE_MINUTE_LIMIT)) {
                int totalQty = 0;
                if (blocks != null) {
                    int start = secondOffset(timestamp);
                    for (int i = 0; i < SECONDS_IN_MINUTE; i++) {
                        int loop = betterModulo(start-i, blocks.lastTwoMinutesSeconds.length());
                        totalQty += blocks.lastTwoMinutesSeconds.get(loop);
                    }
                }
                resultMap.put(duration, totalQty);
            }
            else if (duration.equals(FIVE_MINUTE_LIMIT)) {
                int totalQty = 0;
                if (blocks != null) {
                    int start = tenMinutesFiveSecondOffsets(timestamp);
                    for (int i = 0; i < FIVE_SECOND_PERIODS_IN_FIVE_MIN; i++) {
                        int loop = betterModulo(start-i, blocks.lastTenMinutesFiveSeconds.length());
                        totalQty += blocks.lastTenMinutesFiveSeconds.get(loop);
                    }
                }
                resultMap.put(duration, totalQty);
            }
            else if (duration.equals(ONE_HOUR_LIMIT)) {
                // Note:  This takes a mean of 1.8 usecs, to iterate the array
                int totalQty = 0;
                if (blocks != null) {
                    int start = minuteOffset(timestamp);
                    for (int i = 0; i < start+MINUTES_IN_HOUR; i++) {
                        int loop = betterModulo(start - i, blocks.lastTwoHoursMinutes.length());
                        totalQty += blocks.lastTwoHoursMinutes.get(loop);
                    }
                }
                resultMap.put(duration, totalQty);
            }
            else if (duration.equals(ONE_DAY_LIMIT)) {
                int totalQty = 0;
                if (blocks != null) {
                    int start = hoursOffset(timestamp);
                    for (int i = 0; i < 24; i++) {
                        int loop = betterModulo(start - i, blocks.lastTwoDaysInHours.length());
                        totalQty += blocks.lastTwoDaysInHours.get(loop);
                    }
                }
                resultMap.put(duration, totalQty);
            }
            else {
                String msg = "Unsupported Duration: " + duration;
                log.error(msg);
                throw new Error(msg);
            }
        }

        return resultMap;
    }

    public static int betterModulo(int i, int N)
    {
        return (i % N + N) % N;
    }

    /**
     * Update the amount traded at a specific moment; which is then used to calculate
     * trading limits.
     *
     * Only update the limit, if the trade time falls in the trading
     * window.  Cancels, will provide negative updates (increasing the allowed limit,
     * trying to counterbalance a prior trade); but these must be ignored if the
     * original trade is no longer in the window.
     *
     * @param tradeTimeMillis the millis UTC the trade was made
     * @param pmGroup the PM group to apply the trade to
     * @param sid security id of the fill
     * @param signedAmount the negative or positive amount traded
     */
    // TODO:  Could populate some maps on startup; for securities we know we trade,
    //  based on limit files and positions
    public void updateAmountTraded(long tradeTimeMillis, String pmGroup, long sid, int signedAmount) {
        PmGroupSidKey key = new PmGroupSidKey(sid, pmGroup);

        TradeActivityBlocks blocks = tradingPerSecurity.get(key);
        if (blocks == null) {
            blocks = new TradeActivityBlocks(key);
            tradingPerSecurity.put(key, blocks);
        }

        long now = System.currentTimeMillis();
        long millisDiff = (now - tradeTimeMillis);

        // needed when we "return" trading limits for cancelled orders; prevent returning an old order to limits

        // this will always be 1000ms; don't take the limit out of this bucket,
        // if the trade time is less than one second OLD (not an old cancel being returned)
        if (millisDiff < ONE_SECOND_LOOKBACK_MILLIS) {
            // add tenths of seconds to counting system
            int tenthsOffset = twoSecondTenthsOffset(tradeTimeMillis);
            long perTenthValue = blocks.lastTwoSecondsTenths.get(tenthsOffset);
            perTenthValue += signedAmount;
            blocks.lastTwoSecondsTenths.set(tenthsOffset, perTenthValue);
        } else {
            log.debug("Trade limits is prior to the ONE SECOND limit window.  " +
                    "sid={}, pmGroup={}, signedQty={}",sid,pmGroup,signedAmount);
        }

        if (millisDiff < FIVE_SECOND_LOOKBACK_MILLIS) {
            int fiveSecondsTenths = tenSecondTenthsOffset(tradeTimeMillis);
            long perFiveTenths = blocks.lastTenSecondsTenths.get(fiveSecondsTenths);
            perFiveTenths += signedAmount;
            blocks.lastTenSecondsTenths.set(fiveSecondsTenths, perFiveTenths);
        }
        else {
            log.debug("(normal for a cancel) " +
                    "Trade limits is prior to the FIVE SECOND limit window.  " +
                    "sid={}, pmGroup={}, signedQty={}",sid,pmGroup, signedAmount);
        }

        if (millisDiff < ONE_MIN_LOOKBACK_MILLIS) {
            // add to the Seconds counting system
            int secondOffset = secondOffset(tradeTimeMillis);
            long perSecondValue = blocks.lastTwoMinutesSeconds.get(secondOffset);
            perSecondValue += signedAmount;
            blocks.lastTwoMinutesSeconds.set(secondOffset, perSecondValue);
        }
        else {
            log.debug("(normal for a cancel) " +
                    "Trade limits is prior to the ONE MIN limit window.  " +
                    "sid={}, pmGroup={}, signedQty={}",sid,pmGroup, signedAmount);
        }

        if (millisDiff < FIVE_MIN_LOOKBACK_MILLIS) {
            int fiveSecondOffset = tenMinutesFiveSecondOffsets(tradeTimeMillis);
            long perFiveSecondValue = blocks.lastTenMinutesFiveSeconds.get(fiveSecondOffset);
            perFiveSecondValue += signedAmount;
            blocks.lastTenMinutesFiveSeconds.set(fiveSecondOffset, perFiveSecondValue);
        }
        else {
            log.debug("(normal for a cancel) " +
                    "Trade limits is prior to the FIVE MIN limit window.  " +
                    "sid={}, pmGroup={}, signedQty={}",sid,pmGroup, signedAmount);
        }

        if (millisDiff < ONE_HOUR_LOOKBACK_MILLIS) {
            // add to the Minutes counting system
            int minuteOffset = minuteOffset(tradeTimeMillis);
            long perMinuteValue = blocks.lastTwoHoursMinutes.get(minuteOffset);
            perMinuteValue += signedAmount;
            blocks.lastTwoHoursMinutes.set(minuteOffset, perMinuteValue);
        }

        if (millisDiff < ONE_DAY_LOOKBACK_MILLIS) {
            // add to the hours counting system
            int hoursOffset = hoursOffset(tradeTimeMillis);
            long perHourValue = blocks.lastTwoDaysInHours.get(hoursOffset);
            perHourValue += signedAmount;
            blocks.lastTwoDaysInHours.set(hoursOffset, perHourValue);
        }
    }

    public static int tenSecondTenthsOffset(long timestamp) {
        int tenths = tenthOffset(timestamp);
        int seconds = (int) (timestamp/1000);
        int secondsModFive = seconds % 10;
        return tenths + secondsModFive * 10;
    }

    public static int tenthOffset(long tradetimeMillis) {
        long tenthOfSecond = tradetimeMillis/100;
        int onlyTenth = (int) (tenthOfSecond % 10);
        return onlyTenth;
    }

    public static int twoSecondTenthsOffset(long timestamp) {
        long tenthOfSecond = timestamp/100;
        int onTwenty = (int) (tenthOfSecond % 20);
        return onTwenty;
    }

    public static int secondOffset(long tradeTimeMillis) {
        long second = tradeTimeMillis/1000;
        int onlySeconds = (int) (second % 120);
        return onlySeconds;
    }

    // 60 buckets
    public static int tenMinutesFiveSecondOffsets(long tradeTimeMillis) {
        long second = tradeTimeMillis/1000;
        int secondsInTenMinutes = (int) (second % (60*10));
        return secondsInTenMinutes / 5;
    }

    public static int minuteOffset(long tradeTimeMillis) {
        long second = tradeTimeMillis/1000;
        long minutes = second/60;
        int oneMinute = (int) (minutes % 120);
        return oneMinute;
    }

    public static int hoursOffset(long tradeTimeMillis) {
        long second = tradeTimeMillis/1000;
        long minutes = second/60;
        long hours = minutes/60;
        int oneHour = (int) (hours % 48);
        return oneHour;
    }

}
