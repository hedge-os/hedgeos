package com.hedgeos.ems.service.locates;

import com.hedgeos.ems.csvfiles.locates.LocatesMapAndLatestTimestamp;
import com.hedgeos.ems.order.*;
import com.hedgeos.ems.service.ArraySearcher;
import com.hedgeos.ems.service.EmsEnvironmentManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import order.OrderSubmitStatus;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.hedgeos.ems.order.OrderUtil.isSell;

/**
 * Do we have enough locates to trade short?
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class LocatesManager {

    public static final int HOLD_31_DAYS_SIZE = 32;
    public static final int MAX_STALE_DAYS = 31;

    // ring buffer for speed;
    // in production, should always be set for the trade date, prior to trading day opening
    // keeping this a buffer of object per date, *should* keep this in a cache line
    // TODO:  Measure against CHM, see if it makes any difference
    private final List<LocatesForDate> locatesPerDayOfMonth =
            new CopyOnWriteArrayList<>(new LocatesForDate[HOLD_31_DAYS_SIZE]);

    public final EmsEnvironmentManager environmentManager;

    public final static LocateCheckResult RESERVE_LOCATE_SUCCESS_UNUSED =
            new LocateCheckResult(false, 0L,
                    OrderSubmitStatus.SUCCESSFUL_ORDER,
                    null, null, null, null, null,
                    0L, 0L, 0L);

    public final static LocateCheckResult RESERVE_LOCATE_SUCCESS_USED_LOCATES =
            new LocateCheckResult(true, 0L,
                    OrderSubmitStatus.SUCCESSFUL_ORDER,
                    null, null, null, null, null,
                    0L, 0L, 0L);



    public LocateCheckResult atomicCheckLocateAndIncrementPreTradedAmounts(EmsPosition emsPosition,
                                                                           long hedgeOsOrderId,
                                                                           OrderChain chain,
                                                                           boolean isPreCheck,
                                                                           long shortOrderQty) {
        LocateCheckResult locates = null;
        try {
            // lock the position
            if (!isPreCheck) {
                emsPosition.lock.lock();
            }

            // calculate [real long position]
            long netPositionStartOfDayWithOpenShortTrades =
                    emsPosition.calcNetPositionWithShortRequestsAndLongTradesSinceStartOfDay();

            if (isSell(chain.side())) {

                // if we have less position traded than orderQty, we need locates
                if (netPositionStartOfDayWithOpenShortTrades < shortOrderQty) {

                    // this is how many we need; needs to lock the position
                    // these need to be reserved per position; inside the position lock
                    locates = reserveLocates(
                            isPreCheck,
                            chain.venue(),
                            shortOrderQty,
                            netPositionStartOfDayWithOpenShortTrades,
                            chain,
                            emsPosition,
                            hedgeOsOrderId);

                    return locates;
                }

                if (isPreCheck) {
                    // preCheck returns an informative result
                    return new LocateCheckResult(false, 0L, OrderSubmitStatus.SUCCESSFUL_ORDER,
                            chain.getKey(), emsPosition, "short covered by position", null, null, 0L,
                            netPositionStartOfDayWithOpenShortTrades, shortOrderQty);
                }

                // null means we do not need locates
                return null;
            }

            if (isPreCheck) {
                // preCheck returns an informative result
                return new LocateCheckResult(false, 0L, OrderSubmitStatus.SUCCESSFUL_ORDER,
                        chain.getKey(), emsPosition, "buy does not require locates",
                        null, null, 0L,netPositionStartOfDayWithOpenShortTrades, shortOrderQty);
            }


            // null means we do not need locates
            return null;
        }
        finally {
            // amend the amount traded
            if (!isPreCheck) {
                // if locates were reserved, update the preTraded amounts
                if (notLocatesFailure(locates)) {
                    // always update the amount preTraded; whether reserved the locates or not
                    emsPosition.increaseRequestedQuantity(chain.side(), shortOrderQty);
                }

                emsPosition.lock.unlock();
            }
        }
    }

    private static boolean notLocatesFailure(LocateCheckResult locates) {
        return locates == null || locates.orderStatusCode == OrderSubmitStatus.SUCCESSFUL_ORDER;
    }


    /**
     * Need to hold a certain amount of locates;
     * removing them out of the pool of locates, with a pointer to return the
     * very same quantity if the order is canceled.
     **/
    public LocateCheckResult reserveLocates(boolean isPreCheck,
                                            EmsExecutionVenue venue,
                                            long shortOrderQty,
                                            long netPositionLongShortSinceStartOfDay,
                                            OrderChain chain,
                                            EmsPosition emsPosition,
                                            long hedgeOsOrderIdForLogging) {
        // since this is a Sell, we subtract from the total quantity
        // to determine, where this short order will put us at if we execute
        // (subtract because shorting)
        long positionIncludingNewShort = netPositionLongShortSinceStartOfDay - shortOrderQty;

        // if we are still long after this order, then we don't need locates.
        if (positionIncludingNewShort >= 0) {

            if (false == isPreCheck) {
                // return a canned answer for speed; less info
                return RESERVE_LOCATE_SUCCESS_UNUSED;
            }

            // we have locates; for pre-check return more info.
            return new LocateCheckResult(false,0L,
                    OrderSubmitStatus.SUCCESSFUL_ORDER,
                    chain.getKey(), emsPosition, null, null, null,
                    0L, netPositionLongShortSinceStartOfDay,
                    shortOrderQty);
        }

        // find the locates collection in a ring buffer
        LocatesForDate locatesForDate = locatesPerDayOfMonth.get(chain.getOrderTradeDate().getDayOfMonth());

        if (locatesForDate == null && environmentManager.isStaleLocatesEnabled()) {
            locatesForDate = ArraySearcher.findPriorDateValue(chain.getOrderTradeDate(), locatesPerDayOfMonth);
        }

        if (locatesForDate == null) {
            if (environmentManager.isStaleLocatesEnabled()) {
                // state that NO_VIABLE_LOCATES; means we could not find them, nothing existed.
                String msg = MessageFormat.format("No viable stale LOCATES for tradeDate={0}, for hedgeOsOrderId={1}",
                        chain.getOrderTradeDate(), hedgeOsOrderIdForLogging);
                log.error(msg);
                return new LocateCheckResult(false,0L,
                        OrderSubmitStatus.NO_VIABLE_LOCATES_FILE_FOR_TRADE_DATE,
                        null, emsPosition, msg, null, null, 0L,
                        netPositionLongShortSinceStartOfDay,
                        shortOrderQty);
            }

            String msg = MessageFormat.format("Missing LOCATES for tradeDate={0}, for hedgeOsOrderId={1}",chain.tradeDate(), hedgeOsOrderIdForLogging);
            log.error(msg);
            return new LocateCheckResult(false,0L,
                    OrderSubmitStatus.MISSING_LOCATES_FILE_FOR_TRADE_DATE,
                    chain.getKey(), emsPosition, msg, null, null,
                    0L,
                    netPositionLongShortSinceStartOfDay,
                    shortOrderQty);
        }

        // find any locates mapped, per ExecutionVenue for this key
        LocatesFileRow specificLocateBucket =
                locatesForDate.getByKeyAndVenue(chain.getKey(), venue);

        if (specificLocateBucket == null) {
            // early exit, there are no locates for this symbol; however, there ARE locates for the Venue
            if (locatesForDate.locatesExistForVenue(venue)) {
                // there are locates; but not for this key, specific error
                String msg = "Locates loaded; not for this symbol="+ chain.getSymbol()+
                        ", locatesRequested=" + positionIncludingNewShort +", key="+ chain.getKey();
                return new LocateCheckResult(false, 0L,
                        OrderSubmitStatus.LOCATES_ABSENT_FROM_FILE, chain.getKey(),
                        emsPosition, msg,
                        locatesForDate.originalLoad.getFile(), null, 0L,
                        netPositionLongShortSinceStartOfDay,
                        shortOrderQty);
            }

            String msg = "NO LOCATES. NONE LOADED. symbol=" + chain.getSymbol() +
                    ", locatesRequested=" + positionIncludingNewShort + ", key=" + chain.getKey();
            return new LocateCheckResult(false, 0L,
                    OrderSubmitStatus.NO_LOCATES_EXIST_FOR_VENUE, chain.getKey(),
                    emsPosition, msg,
                    locatesForDate.originalLoad.getFile(), null, 0L,
                    netPositionLongShortSinceStartOfDay,
                    shortOrderQty);
        } else {

            // this is the critical question
            // Between the net position, and the locates; are we above zero?
            // this means, between the
            //  1. short requested,
            //  2. the current outstanding shorts,
            //  3. longs which executed
            // we have enough locates to cove the new order.
            long positionWithLocates =
                    specificLocateBucket.getLocates() + positionIncludingNewShort;

            if (positionWithLocates >= 0) {
                // success; the locates gave us enough room to execute this short
                if (!isPreCheck) {
                    // for speed, don't construct a result with additional info
                    return RESERVE_LOCATE_SUCCESS_USED_LOCATES;
                }

                // more detailed answer for "preCheck" call
                return new LocateCheckResult(true,
                        positionIncludingNewShort,
                        OrderSubmitStatus.SUCCESSFUL_ORDER,
                        chain.getKey(), emsPosition, null,
                        locatesForDate.originalLoad.getFile(),
                        specificLocateBucket,
                        specificLocateBucket.getLocates(),
                        netPositionLongShortSinceStartOfDay,
                        shortOrderQty);
            }
            else {
                // not enough locates to cover the overall short position
                return new LocateCheckResult(false,
                        0L,
                        OrderSubmitStatus.NOT_ENOUGH_LOCATES,
                        chain.getKey(), emsPosition, null,
                        locatesForDate.originalLoad.getFile(),
                        specificLocateBucket,
                        specificLocateBucket.getLocates(),
                        netPositionLongShortSinceStartOfDay,
                        shortOrderQty);
            }

        }
    }


    public void setLocatesForTradeDate(LocatesMapAndLatestTimestamp locates) {
        int dayOfMonth = locates.getFileDate().getDayOfMonth();
        LocatesForDate checker = new LocatesForDate(locates);
        // copy on write, will swap out the array list for the next read
        locatesPerDayOfMonth.set(dayOfMonth, checker);

        // cleanOldLocates();
    }

    public void cleanOldLocates(LocalDate tradeDate) {
        // clean out any (very) old position limit checkers (unless in dev mode, and allowing stale limits)
        //noinspection PointlessBooleanExpression
        if (false == environmentManager.isStaleLocatesEnabled()) {
            LocalDate thirtyDaysPrior = tradeDate.minusDays(MAX_STALE_DAYS);
            Set<LocatesForDate> toRemove = new HashSet<>();
            for (LocatesForDate locatesForDate : locatesPerDayOfMonth) {
                if (locatesForDate.isBefore(thirtyDaysPrior)) {
                    log.error("Removing ancient locates > 31 days="+locatesForDate.getFileDate());
                    toRemove.add(locatesForDate);
                }
            }
            locatesPerDayOfMonth.removeAll(toRemove);
        }
    }

}
