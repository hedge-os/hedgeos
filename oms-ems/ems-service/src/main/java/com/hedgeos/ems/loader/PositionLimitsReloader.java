package com.hedgeos.ems.loader;

import com.hedgeos.ems.config.beans.CompleteEmsConfig;
import com.hedgeos.ems.csvfiles.risklimits.position.PositionLimitLoader;
import com.hedgeos.ems.csvfiles.risklimits.position.PositionLimitLoad;
import com.hedgeos.ems.service.EmsEnvironmentManager;
import com.hedgeos.ems.service.risklimits.positions.PositionLimitsManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.datetime.standard.DateTimeFormatterFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.hedgeos.ems.csvfiles.risklimits.position.PositionLimitLoader.POSITION_LIMITS_FILE_PREFIX;
import static com.hedgeos.ems.csvfiles.util.FlatFileUtil.*;

@Component
@RequiredArgsConstructor
@Slf4j
public class PositionLimitsReloader {

    private final PositionLimitsManager positionLimitsManager;
    private final EmsEnvironmentManager emsEnvironmentManager;

    public Reloader loadAndStartReload(CompleteEmsConfig completeEmsConfig, LocalDate tradeDate) {
        // load positions on startup; and need to check for positions every minute
        PositionLimitLoader loader = new PositionLimitLoader(completeEmsConfig.getEmsConfigFolder());

        // if it does not find the file, throws missing file ERROR (which is good.)
        // can't trade with stale positions
        File currentPositionFile =
                findMostRecentFile(tradeDate, loader.getRiskLimitsFolder(), POSITION_LIMITS_FILE_PREFIX);

        DateTimeFormatter formatter = new DateTimeFormatterFactory(YYYYMMDD).createDateTimeFormatter();
        String localDateString = formatter.format(tradeDate);
        // we must have positions for the start date; fail-fast if no positions loaded
        // as it means we have not configured the ems-service
        //noinspection PointlessBooleanExpression
        if (false == currentPositionFile.getName().contains(localDateString)) {
            if (emsEnvironmentManager.isWorkstation()) {
                log.warn("Continuing with old positions LIMITS file="+currentPositionFile+
                        ", due to developer workstation.");
            }
            else {
                // in Development, or Prod, fail if the positions file doesn't exist for the trade date starting on.
                // If starting on Sunday, is a position file necessary?
                // FAQ:  The operators can provide an empty file
                // or the file from Friday, if Sunday is the day the system is started.
                String msg =
                        "Must provide a risk LIMITS file named= [positions_limits_"
                                + localDateString + ".csv] in folder=[" + loader.getRiskLimitsFolder() + "]";
                log.error(msg);
                // can't start ems-service without position limits
                throw new Error(msg);
            }
        }

        // load the positions one time to bootstrap; we need them for the current trade date
        PositionLimitLoad positionLimits =
                loader.loadPositionLimits(currentPositionFile, true);

        // set us up for success on the first trading day
        positionLimitsManager.setPositionLimits(positionLimits);

        // reload positions when they arrive, either for today (emergency reload)
        // or future position dates, once available
        return new Reloader(positionLimitsManager, positionLimits.getFileDate(), positionLimits.getLatestTimestamp(), loader);
    }

    public static class Reloader implements Reload {

        private final PositionLimitLoader loader;
        private final Map<LocalDate,Long> lastLoadMap = new ConcurrentHashMap<>();
        private final PositionLimitsManager limitsManager;

        public Reloader(PositionLimitsManager limitsManager, LocalDate firstTradeDate, long firstLoad, PositionLimitLoader loader) {
            this.lastLoadMap.put(firstTradeDate, firstLoad);
            this.limitsManager = limitsManager;
            this.loader = loader;
        }

        public void reloadIfNewer(LocalDate tradeDate) {
            try {
                File mostRecentLimitsFile =
                        findMostRecentFile(tradeDate,
                                loader.getRiskLimitsFolder(), POSITION_LIMITS_FILE_PREFIX);

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
                String tradeDateString = tradeDate.format(formatter);
                // NEVER load the limits for any day other than the file date
                //noinspection PointlessBooleanExpression
                if (false == mostRecentLimitsFile.toString().contains(tradeDateString)) {
                    log.debug("Position LIMITS file has not arrived for tradeDate="+tradeDateString);
                }
                else {
                    PositionLimitLoad latestCheck =
                            loader.loadPositionLimits(mostRecentLimitsFile, false);

                    Long lastLoadTime = lastLoadMap.get(latestCheck.getFileDate());
                    if (lastLoadTime != null && latestCheck.getLatestTimestamp() <= lastLoadTime) {
                        log.debug("Position LIMITS file has not changed=" + latestCheck.getLatestTimestamp());
                    }
                    else {
                        // now loadThePositions is = true
                        latestCheck = loader.loadPositionLimits(mostRecentLimitsFile, true);
                        log.warn("Found new position LIMITS file=" + latestCheck.getLatestTimestamp() +
                                ", loaded positions LIMITS=" + latestCheck.getPositionLimitsMap().size());
                        // once a new file is loaded; this becomes the current known positions
                        // see the Decision 0008 in the decision docs folder.
                        limitsManager.setPositionLimits(latestCheck);
                        // update the latest load time
                        lastLoadMap.put(latestCheck.getFileDate(), latestCheck.getLatestTimestamp());
                    }
                }
            }
            catch (Exception e) {
                // TODO: this needs to produce an email, and UX alert
                log.error("Error loading position LIMITS file, will sleep and retry in a minute.", e);
            }
        }
    }
}
