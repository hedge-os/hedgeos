package com.hedgeos.ems.service;

import com.hedgeos.ems.EmsServerGrpcSpringBootApplication;
import com.hedgeos.ems.config.EmsConfigLoader;
import com.hedgeos.ems.config.EmsConfigVariables;
import com.hedgeos.ems.config.beans.CompleteEmsConfig;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import com.hedgeos.ems.csvfiles.symbology.SymbolAdapter;
import com.hedgeos.ems.csvfiles.symbology.SymbolManager;
import com.hedgeos.ems.loader.*;
import com.hedgeos.ems.service.exception.FixAdapterFailedToStartException;
import com.hedgeos.ems.util.TradingDayUtil;
import com.hedgeos.fix.adapter.EmsCallback;
import com.hedgeos.fix.adapter.FixAdapter;
import com.hedgeos.fix.adapter.FixAdapterFactory;
import com.hedgeos.fix.adapter.FixObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import order.EmsReport;
import order.EmsReportEnvelope;
import org.springframework.boot.SpringApplication;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Excessive logging and care of the config setup for EMS, as it is critical
 * to the operations of the EMS system, followed by starting the FIX Adapters.
 *
 * Lifecycles are started by Spring Boot, in the "phase" order.  See the spring docs.
 */
@RequiredArgsConstructor
@Slf4j
@Component
public class FixServerLifecycle implements SmartLifecycle {

    public static final String STOP_ON_CONNECTION_LOAD_FAILURE = "stopOnConnectionLoadFailure";

    volatile boolean isRunning = false;

    // loads the environment variables EMS_CONFIG_FOLDER and EMS_CONFIG_FILE
    // (set these in .bashrc ON THE MACHINE where you run EMS)
    // then loads each of the configs referenced by the primary config file
    private final EmsConfigLoader configLoader;
    // this loads the QuickfixJ Fix Adapter (and any others we add in the future.)
    private final FixAdapterFactory fixAdapterFactory;
    // pushes data from the Adapters to the EMS itself, and Kafka (two observers)
    private final EmsFixObserver emsFixObserver;
    // the important config
    private final EmsConfigVariables emsConfigVariables;

    // we load up the FixAdapters on the Multiplexer
    private final EmsMultiplexer multiplexer;

    private final SymbolManager symbolManager;

    // loads a file once, then reloads periodically
    private final PositionReloader positionReloader;
    private final SymbolReloader symbolReloader;
    private final LocatesReloader locatesReloader;
    private final PositionLimitsReloader positionLimitsReloader;
    private final TradeLimitsReloader tradeLimitsReloader;

    // this is the trigger, for once S3 has been copied locally
    private final FileReloadScheduler fileReloadScheduler;

    private final EmsServerIdManager emsServerIdManager;

    @Override
    public int getPhase() {
        return EmsServerGrpcSpringBootApplication.FIX_SERVER_LIFECYCLE_STARTUP_PHASE; // startup first, before gRPC is loaded for connections from clients to the ems
    }

    @Override
    public boolean isAutoStartup() {
        return true;
    }

    @Override
    public void start() {

        try {
            boolean stopOnConnectionLoadFailure = isStopOnConnectionLoadFailure();

            // First, config loading; relies on EMS_CONFIG_FOLDER and EMS_CONFIG_FILE env. variables.
            // Fetch the entire set of EMS Config, before we start to load anything
            // this also verifies the config is valid, and the EMS won't start with invalid config.
            CompleteEmsConfig completeEmsConfig = configLoader.loadConfig(emsConfigVariables);

            // give the venue map to the multiplexer; early, required for loading locates
            multiplexer.setVenueMap(completeEmsConfig.getEmsVenueMap());

            LocalDate tradeDate = TradingDayUtil.getTradeDate();

            List<Reload> reloadList = new ArrayList<>();

            // need symbols to trade
            List<Reload> symbolReload =
                    symbolReloader.loadSymbolsForEachFixAdapter(completeEmsConfig);
            reloadList.addAll(symbolReload);

            // need positions for risk checks, and short selling
            Reload positionReload =
                    positionReloader.loadPositionsAndStartReloadThread(completeEmsConfig, tradeDate);
            reloadList.add(positionReload);

            // need locates to sell short (when not in position)
            Reload locatesReload =
                    locatesReloader.loadLocatesAndReloadThread(completeEmsConfig, completeEmsConfig.getEmsVenueMap(), tradeDate);
            reloadList.add(locatesReload);

            // need position limits to avoid taking too much risk (exposure)
            Reload positionLimits =
                    positionLimitsReloader.loadAndStartReload(completeEmsConfig, tradeDate);
            reloadList.add(positionLimits);

            // need trading limits to avoid over-trading
            Reload tradingLimits =
                    tradeLimitsReloader.loadAndStartReload(completeEmsConfig, tradeDate);
            reloadList.add(tradingLimits);

            // start one refresh loop across all reloaders
            AllReloads allReloads = new AllReloads(reloadList);
            fileReloadScheduler.addReloads(allReloads);

            // TODO:  A thread which waits a minute, then goes over all the condition runnables above.
            // TODO:  Serially, or in parallel, and could be after S3 is copied locally.
            // like this;  https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/examples-s3-transfermanager.html#tranfermanager-download-directory
            // TODO:  Run like this:  https://github.com/localstack/localstack

            // start the FIX connections to the venues
            startFixConnections(emsFixObserver, stopOnConnectionLoadFailure, completeEmsConfig);

            isRunning = true;
        }
        catch (Error e) {
            String msg = "Failed to start FixServerLifecycle="+e.getMessage();
            log.error(msg,e);
            throw e;
        }
    }

    /**
     *  Prefer to fail-fast on startup of ems-service
     *  when a FIX connection fails; this way operations is woken up..
     *  instead of a trading failure at the open.
     * @return
     */
    private static boolean isStopOnConnectionLoadFailure() {
        boolean stopOnConnectionLoadFailure = true;
        // TODO:  Ability to reconfigure and restart a connection
        String isStopPropSet = System.getProperty(STOP_ON_CONNECTION_LOAD_FAILURE);
        if (isStopPropSet != null && Boolean.getBoolean(isStopPropSet) == false) {
            stopOnConnectionLoadFailure = false;
        }
        return stopOnConnectionLoadFailure;
    }

    @Override
    public void stop() {
        log.warn("Stop runnable called in FixServerLifecycle");
        // fileReloadScheduler.stop();
        multiplexer.stop();
        log.warn("Finished stop on FixServerLifecycle");
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }



    /**
     * @param emsFixObserver observer the EMS passes to the FIX Adapters to be notified of fills
     * @param completeEmsConfig the value of the critical variables EMS_CONFIG_FOLDER and EMS_CONFIG_FILE
     */
    private void startFixConnections(EmsFixObserver emsFixObserver,
                                     boolean stopOnConnectionLoadFailure,
                                     CompleteEmsConfig completeEmsConfig) {

        log.warn("Success loading config, now starting FIX connections.");

        // subscribe for messages (including connection),
        // to keep positions, risk, pass back on gRPC
        final CopyOnWriteArrayList<FixObserver> observers = new CopyOnWriteArrayList<>();

        // TODO:  Auto-wire Kafka Observer and invent it
        final FixObserver kafkaObserver = new KafkaFixObserver();

        // wire up the callback to push Execution Reports
        // 1) back into the Multiplexer
        //      1.1) which pushes to all EMS gRPC subscriptions
        // 2) to Kafka for risk and reporting

        // write to the EmsObserver first
        observers.add(emsFixObserver);
        // also drop a copy onto Kafka for Campus, GUI, risk-management
        observers.add(kafkaObserver);

        // the ems-callback is passed to all FIX Adapters
        // to observe messages, and route back to subscriptions to EMS,
        // as well as Kafka, any other locations
        EmsCallback emsCallback = new EmsCallback() {
            @Override
            public void callback(EmsReportEnvelope envelope, EmsReport er) {
                for (FixObserver fixObserver : observers) {
                    fixObserver.onMessage(envelope, er);
                }
            }
        };

        startFixAdapters(stopOnConnectionLoadFailure, completeEmsConfig, emsCallback);
    }

    private void startFixAdapters(boolean stopOnConnectionLoadFailure,
                                  CompleteEmsConfig completeEmsConfig,
                                  EmsCallback emsCallback) {
        // iterate the list of adapters to start, start them up
        for (FixAdapterConfig fixAdapterConfig : completeEmsConfig.getFixAdapterConfigList()) {

            log.warn("Creating FIX Adapter for config:"+fixAdapterConfig.getName()+
                    ", type="+fixAdapterConfig.getConfigFileName());

            FixAdapter fixAdapter = fixAdapterFactory.createAdapter(fixAdapterConfig);

            try {
                // send the callback, to observe any activity on the fixAdapter
                fixAdapter.subscribe(emsCallback);

                // ties the FIXAdapter to a specific symbology in symbology manager
                SymbolAdapter perSymbologyAdapter =
                        symbolManager.buildSymbolAdapter(fixAdapterConfig.getSymbology());

                // load this fix adapter, connect to the venue
                fixAdapter.loadAndConnect(
                        emsServerIdManager.getEmsServerId(),
                        completeEmsConfig.getEmsConfigSetting(),
                        fixAdapterConfig,
                        perSymbologyAdapter);

                // inform the multiplexer about this FixAdapter
                multiplexer.addFixAdapter(fixAdapterConfig, fixAdapter);

            } catch (Exception e) {
                log.error("Exception loading FIX Adapter, name="+fixAdapterConfig.getName(), e);
                // fail-fast is typically better, preferred; however, in certain situations
                // you could route away to somewhere else, if one connection failed to start
                // however; in practice, a connection failure usually
                // means your config is broken, and you should not continue
                if (stopOnConnectionLoadFailure) {
                    throw new FixAdapterFailedToStartException(
                            "Stopping on FIX Connection failure="+fixAdapterConfig.getName(), e);
                }
            }
        }
    }


}
