package com.hedgeos.ems.service.risklimits.trades;

import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitDurations;
import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitLoad;
import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitsFileRow;
import com.hedgeos.ems.order.PmGroupSidKey;
import general.Quantity;
import lombok.extern.slf4j.Slf4j;
import order.OrderSubmitStatus;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.hedgeos.ems.order.OrderUtil.getDirectionalQuantity;
import static com.hedgeos.ems.util.PrecisionConstants.*;

/**
 * Holds the contents of one load of trade limit files with given dates like:
 *  trade_limit_20230525.csv (see ems-config-mock-files folder.)
 * This file and any override files are loaded (see decisions docs on overrides, decision 0010 for limits files.)
 * Importantly, the trade checker is initialized from a trade limits file; and then is never altered.
 * This allows us to use assign final standard collections and the JVM inserts a freeze fence
 * after the construction is complete.
 * Good article:  // good read:  https://dzone.com/articles/final-keyword-and-jvm-memory-impact
 */
@Slf4j
public class TradeLimitChecker {

    // avoid constructing the success message more than once
    public static final TradingLimitCheckRowResult LIMIT_CHECK_SUCCESS =
            new TradingLimitCheckRowResult(null,
                    "limit check success", OrderSubmitStatus.SUCCESSFUL_ORDER,
                    0L, 0d, 0d);
    public final TradeAmountTracker tradeAmountTracker;

    private final List<TradeLimitsFileRow> pmGroupWildcardRowNoSid;
    private final List<TradeLimitsFileRow> sidWildcardRowNoPmGroup;
    private final Map<Long, List<TradeLimitsFileRow>> perSidLimitsWildcardPmGroup;

    private final Map<String, List<TradeLimitsFileRow>> perPmGroupLimitsWildcardSid;

    private final Map<PmGroupSidKey, List<TradeLimitsFileRow>> specificPmGroupAndSid;

    public TradeLimitChecker(TradeAmountTracker tradeManager, TradeLimitLoad tradeLimits) {
        this.tradeAmountTracker = tradeManager;

        List<TradeLimitsFileRow> pmGroupWildcardRowNoSid1 = new ArrayList<>();
        List<TradeLimitsFileRow> sidWildcardRowNoAccount1 = new ArrayList<>();
        Map<PmGroupSidKey, List<TradeLimitsFileRow>> specificPmGroupAndSid = new ConcurrentHashMap<>();
        Map<Long, List<TradeLimitsFileRow>> perSidLimitsWildcardPmGroup = new ConcurrentHashMap<>();
        Map<String, List<TradeLimitsFileRow>> perPmGroupLimitsWildcardSid1 = new ConcurrentHashMap<>();

        // load the maps with the limits file rows
        for (TradeLimitsFileRow row : tradeLimits.getTradeLimitsMap().keySet()) {
            if (row.isPmGroupWildcard() && row.isSidWildcard()) {
                log.error("Should never be a row with wildcard sid and account");
            }
            else if (row.isPmGroupWildcard() && row.getSid() == null) {
                pmGroupWildcardRowNoSid1.add(row);
            }
            else if (row.isSidWildcard() && row.getPmGroup() == null) {
                sidWildcardRowNoAccount1.add(row);
            }
            else if (row.isPmGroupWildcard() && row.getSid() != null) {
                perSidLimitsWildcardPmGroup.putIfAbsent(row.getSid(), new ArrayList<>());
                perSidLimitsWildcardPmGroup.get(row.getSid()).add(row);
            }
            else if (row.isSidWildcard() && row.getPmGroup() != null) {
                perPmGroupLimitsWildcardSid1.putIfAbsent(row.getPmGroup(), new ArrayList<>());
                perPmGroupLimitsWildcardSid1.get(row.getPmGroup()).add(row);
            }
            else {
                PmGroupSidKey pmGroupSidKey = new PmGroupSidKey(row.getSid(), row.getPmGroup());
                // there may be multiple specific rows
                specificPmGroupAndSid.putIfAbsent(pmGroupSidKey, new CopyOnWriteArrayList<>());
                List<TradeLimitsFileRow> list = specificPmGroupAndSid.get(pmGroupSidKey);
                list.add(row);
            }
        }

        this.pmGroupWildcardRowNoSid = pmGroupWildcardRowNoSid1;
        this.sidWildcardRowNoPmGroup = sidWildcardRowNoAccount1;
        this.perSidLimitsWildcardPmGroup = perSidLimitsWildcardPmGroup;
        this.perPmGroupLimitsWildcardSid = perPmGroupLimitsWildcardSid1;
        this.specificPmGroupAndSid = specificPmGroupAndSid;
        // a freeze fence is inserted here for any file variables, and their contents
    }

    public TradingLimitCheckRowResult checkTradeLimit(boolean preCheck,
                                                      long utcOrderMillisTime,
                                                      int side, Quantity quantity, long hedgeOsOrderId, PmGroupSidKey key) {
        // check for a match of both account and sid from the limits file
        List<TradeLimitsFileRow> specificLimitRowList = specificPmGroupAndSid.get(key);

        double directionalQuantity = getDirectionalQuantity(quantity, side);

        // if a specific limit row exists; this is the only limit checked.
        if (specificLimitRowList != null) {
            // This is the Tier 1 check in the decision doc 0010
            // if a specific limit exists, then only this check is performed
            return checkTradingLimits(preCheck, directionalQuantity, hedgeOsOrderId, specificLimitRowList,utcOrderMillisTime,key);
        }

        TradingLimitCheckRowResult tierTwoCheckResult = tierTwoChecks(preCheck, directionalQuantity, hedgeOsOrderId, utcOrderMillisTime, key);
        if (tierTwoCheckResult != null) {
            // non-null means a tier two check was performed; return it
            return tierTwoCheckResult;
        }

        TradingLimitCheckRowResult tierThreeResult = tierThreeChecks(preCheck, directionalQuantity, hedgeOsOrderId, utcOrderMillisTime, key);
        if (tierThreeResult != null) {
            // non-null means a tier three check was performed; return it
            return tierThreeResult;
        }

        return LIMIT_CHECK_SUCCESS;
    }

    private TradingLimitCheckRowResult checkTradingLimits(boolean preCheck,
                                                          double directionalQuantity,
                                                          long hedgeOsOrderId,
                                                          List<TradeLimitsFileRow> rowsBeingChecked,
                                                          long utcOrderSubmitTime,
                                                          PmGroupSidKey key) {

        // TODO:  Get the time windows for the limit
        List<Duration> durations = getDurationsForLimits(rowsBeingChecked);
        // TimeUnit.SECONDS, 1
        Map<Duration, Integer> amountsTradePerDuration =
                tradeAmountTracker.getAmountsTraded(durations, key,
                        utcOrderSubmitTime);

        for (TradeLimitsFileRow tradeLimitsFileRow : rowsBeingChecked) {
            // how much have we traded in this period (one second, one minute, one hour, one day.)
            long amountAlreadyTraded = amountsTradePerDuration.get(tradeLimitsFileRow.getDuration());
            // what would the new total be with this trade
            double totalWithNewOrder = amountAlreadyTraded + directionalQuantity;

            // does the new total pass the long limit on this "row" from the file?
            TradingLimitCheckRowResult longStatusCheck =
                    checkLongLimitStatus(hedgeOsOrderId, tradeLimitsFileRow,
                            amountAlreadyTraded, directionalQuantity, totalWithNewOrder, preCheck);

            // if we didn't pass, return the failure
            if (longStatusCheck.getOrderSubmitStatus() != OrderSubmitStatus.SUCCESSFUL_ORDER) {
                return longStatusCheck;
            }

            // does tne new total pass the short limit on this row from the file?
            TradingLimitCheckRowResult shortStatusCheck =
                    checkShortLimitStatus(preCheck, hedgeOsOrderId, tradeLimitsFileRow,
                            amountAlreadyTraded, directionalQuantity, totalWithNewOrder);

            // early exit if not preCheck limits call
            if (shortStatusCheck.getOrderSubmitStatus() != OrderSubmitStatus.SUCCESSFUL_ORDER) {
                return shortStatusCheck;
            }
        }

        return LIMIT_CHECK_SUCCESS;
    }

    /**
     * Record this order is in the market; add it to the list of trades done.
     * (for trading limit purposes we assume it is in the market until canceled.)
     */
    public void recordNewTradePlacedInTracker(long timestampMillis, String pmGroup, long sid, int signedOrderQty) {
        tradeAmountTracker.updateAmountTraded(timestampMillis, pmGroup, sid, signedOrderQty);
    }

    static TradingLimitCheckRowResult checkLongLimitStatus(long hedgeOsOrderId,
                                                           TradeLimitsFileRow tradeLimitsFileRow,
                                                           double amountAlreadyTraded,
                                                           double directionalQuantity,
                                                           double totalWithNewOrder,
                                                           boolean preCheck)
    {
        if (isGreaterThan(totalWithNewOrder, tradeLimitsFileRow.getQtyLong())) {
            String message =
                    "Order violated qtyLong trading limit"+
                            ", hedgeOsOrderId="+ hedgeOsOrderId+
                            ", orderQty="+ directionalQuantity +
                            ", alreadyTraded="+ amountAlreadyTraded +
                            ", newQty="+ totalWithNewOrder +
                            ", limitViolated="+ tradeLimitsFileRow;
            if (preCheck == false) {
                log.error(message);
            }

            if (tradeLimitsFileRow.getDuration().equals(TradeLimitDurations.ONE_SECOND_LIMIT)) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message, OrderSubmitStatus.TRADE_LIMIT_BREACHED_EXPOSURE_LONG_ONE_SECOND,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(TradeLimitDurations.FIVE_SECOND_LIMIT)) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_LONG_FIVE_SECOND,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(TradeLimitDurations.ONE_MINUTE_LIMIT)) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_LONG_ONE_MINUTE,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(TradeLimitDurations.FIVE_MINUTE_LIMIT)) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_LONG_FIVE_MINUTE,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(TradeLimitDurations.ONE_HOUR_LIMIT)) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_LONG_ONE_HOUR,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(TradeLimitDurations.ONE_DAY_LIMIT)) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_LONG_ONE_DAY,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }

            String msg = "Unrecognized duration in limits="+ tradeLimitsFileRow.getDuration();
            // this is always an error; the Duration from the file was unknown
            log.error(msg);

            if (preCheck) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, msg,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_LONG_UNKNOWN_PERIOD,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }

            // if a new duration is added, and we don't understand it; err with caution, allow the trade
            return LIMIT_CHECK_SUCCESS;
        }

        if (preCheck) {
            // if it is the trading limits check, return a more detailed response
            return new TradingLimitCheckRowResult(tradeLimitsFileRow, "SUCCESS",
                    OrderSubmitStatus.SUCCESSFUL_ORDER,
                    tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
        }

        // if it is not the pre-check, avoid constructing a new response for success
        return LIMIT_CHECK_SUCCESS;
    }

    public static boolean isShortLimitViolated(double newTotalQty, TradeLimitsFileRow row) {
        long positiveShortQty = Math.abs(row.getQtyShort());
        double inversePosQty = -1 * positiveShortQty;
        return isLessThanOrEqual(newTotalQty, inversePosQty);
    }

    static TradingLimitCheckRowResult checkShortLimitStatus(boolean preCheck,
                                                            long hedgeOsOrderId,
                                                            TradeLimitsFileRow tradeLimitsFileRow,
                                                            long amountAlreadyTraded,
                                                            double directionalQuantity,
                                                            double totalWithNewOrder)
    {
        if (isShortLimitViolated(totalWithNewOrder, tradeLimitsFileRow)) {
            String message = "Order violated qtyShort trading limit"+
                    ", hedgeOsOrderId="+ hedgeOsOrderId+
                    ", orderQty="+ directionalQuantity +
                    ", alreadyTraded="+ amountAlreadyTraded +
                    ", newQty="+ totalWithNewOrder +
                    ", limitViolated="+ tradeLimitsFileRow;
            if (preCheck == false) {
                log.error(message);
            }

            if (tradeLimitsFileRow.getDuration().equals(Duration.of(1, ChronoUnit.SECONDS))) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_ONE_SECOND,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(Duration.of(5, ChronoUnit.SECONDS))) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_FIVE_SECOND,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(Duration.of(1, ChronoUnit.MINUTES))) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_ONE_MINUTE,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(Duration.of(5, ChronoUnit.MINUTES))) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_FIVE_MINUTE,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(Duration.of(1, ChronoUnit.HOURS))) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_ONE_HOUR,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            else if (tradeLimitsFileRow.getDuration().equals(Duration.of(1, ChronoUnit.DAYS))) {
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, message,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_ONE_DAY,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }

            String msg = "Unrecognized duration in limits="+ tradeLimitsFileRow.getDuration();
            log.error(msg);

            if (preCheck) {
                // in the event pre-check is run and bad limit row is loaded, arrives here (never should)
                return new TradingLimitCheckRowResult(tradeLimitsFileRow, msg,
                        OrderSubmitStatus.TRADE_LIMIT_BREACHED_QTY_SHORT_UNKNOWN_PERIOD,
                        tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
            }
            // err on caution here; but this never happens and generates an error message
            return LIMIT_CHECK_SUCCESS;
        }

        if (preCheck) {
            // if we are pre-checking, return a more detailed result, at the cost of speed
            return new TradingLimitCheckRowResult(tradeLimitsFileRow, "SUCCESS",
                    OrderSubmitStatus.SUCCESSFUL_ORDER,
                    tradeLimitsFileRow.getQtyLong(), amountAlreadyTraded, totalWithNewOrder);
        }

        return LIMIT_CHECK_SUCCESS;
    }


    private List<Duration> getDurationsForLimits(List<TradeLimitsFileRow> specificLimitRow) {
        List<Duration> results = new ArrayList<>();
        for (TradeLimitsFileRow tradeLimitsFileRow : specificLimitRow) {
            results.add(tradeLimitsFileRow.getDuration());
        }
        return results;
    }

    private TradingLimitCheckRowResult tierTwoChecks(boolean preCheck,
                                                     double directionalQuantity,
                                                     long hedgeOsOrderId,
                                                     long utcOrderSubmitTime,
                                                     PmGroupSidKey key) {
        // sid with * account
        List<TradeLimitsFileRow> specificSidAccountWildcardTradeLimits =
                perSidLimitsWildcardPmGroup.get(key.sid);

        // sid check first
        TradingLimitCheckRowResult sidChecks =
                checkLimit(preCheck, directionalQuantity,
                    hedgeOsOrderId, utcOrderSubmitTime, key, specificSidAccountWildcardTradeLimits);

        // if it is not null, and not success, return this check
        if (sidChecks != null && sidChecks.getOrderSubmitStatus() != OrderSubmitStatus.SUCCESSFUL_ORDER)
            return sidChecks;

        // second, account with sid wildcard (*)
        List<TradeLimitsFileRow> specificAccountWildcardSidTradeLimits = perPmGroupLimitsWildcardSid.get(key.pmGroup);
        TradingLimitCheckRowResult accountChecks =
                checkLimit(preCheck, directionalQuantity, hedgeOsOrderId, utcOrderSubmitTime, key, specificAccountWildcardSidTradeLimits);

        if (sidChecks != null && sidChecks.getOrderSubmitStatus() != OrderSubmitStatus.SUCCESSFUL_ORDER)
            return sidChecks;

        // if any check was performed;
        // return SUCCESS to signal we are finished checking limits at tierTwo.
        if (sidChecks != null || accountChecks != null) {
            return LIMIT_CHECK_SUCCESS;
        }

        return null;
    }

    private TradingLimitCheckRowResult checkLimit(boolean preCheck,
                                                  double directionalQuantity, long hedgeOsOrderId,
                                                  long utcOrderSubmitTime, PmGroupSidKey key,
                                                  List<TradeLimitsFileRow> specificAccountWildcardSidTradeLimits)
    {
        if (specificAccountWildcardSidTradeLimits != null && specificAccountWildcardSidTradeLimits.size() > 0) {
            TradingLimitCheckRowResult accountChecks =
                    checkTradingLimits(preCheck,
                            directionalQuantity,
                            hedgeOsOrderId,
                            specificAccountWildcardSidTradeLimits,
                            utcOrderSubmitTime, key);

            // check performed, may be success
            return accountChecks;
        }

        // nothing checked
        return null;
    }

    private TradingLimitCheckRowResult tierThreeChecks(boolean preCheck,
                                                       double directionalQuantity, long hedgeOsOrderId,
                                                       long utcOrderSubmitTime, PmGroupSidKey key) {

        // sid *, blank account (-)
        TradingLimitCheckRowResult sidChecks =
                checkLimit(preCheck, directionalQuantity, hedgeOsOrderId, utcOrderSubmitTime, key, sidWildcardRowNoPmGroup);
        if (sidChecks != null && sidChecks != LIMIT_CHECK_SUCCESS)
            return sidChecks;

        // account *, sid = blank (-)
        TradingLimitCheckRowResult accountChecks =
                checkLimit(preCheck, directionalQuantity, hedgeOsOrderId, utcOrderSubmitTime, key, pmGroupWildcardRowNoSid);
        if (accountChecks != null && accountChecks != LIMIT_CHECK_SUCCESS)
            return accountChecks;

        if (accountChecks != null || sidChecks != null) {
            return LIMIT_CHECK_SUCCESS;
        }

        return null;
    }

}
