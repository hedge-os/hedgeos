package com.hedgeos.ems;

import com.hedgeos.ems.service.CheckPortLifecycle;
import com.hedgeos.ems.service.EmsServiceFactoryConfig;
import com.hedgeos.ems.service.GrpcServerLifecycle;
import io.grpc.BindableService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.io.File;
import java.util.List;
import java.util.Set;

@ComponentScan(
        {"com.hedgeos.ems.config", // external, config package
        "com.hedgeos.ems", // this package
        "com.hedgeos.fix.adapter"} // external, for Fix Adapter Factory
)
@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class EmsServerGrpcSpringBootApplication implements CommandLineRunner {

    // these phase values control the order of startup (and shutdown in reverse)
    public static final int CHECK_PORT_LIFECYCLE_PHASE = -1;
    public static final int FIX_SERVER_LIFECYCLE_STARTUP_PHASE = 0;
    public static final int GRPC_SERVER_LIFECYCLE_PHASE = 1;

    // exit codes for common failures here
    public static final int PORT_UNAVAILABLE_EXIT_CODE = 9;


    @Bean
    CheckPortLifecycle provideCheckPort(@Value("${grpc.port}") int port) {
        log.warn("Checking port open:"+port);
        return new CheckPortLifecycle(port);
    }

    // FixServerLifecycle is the other ServerLifecycle, prior to grpc server startup

    @Bean
    GrpcServerLifecycle provideGrpcSL(List<BindableService> services,
                                      @Value("${grpc.port}") int port)
    {
        // now with Config loaded, start the gRPC services listening
        if (services.isEmpty()) {
            log.error("Need a Configuration bean which returns EmsService, and is magically injected here.");
            throw new RuntimeException(EmsServiceFactoryConfig.class.getName()+
                    " provides services.");
        }

        return new GrpcServerLifecycle(services, port);
    }

    public void run(String... args) { }

    public static void main(String[] args) {
        // kick off a Spring Boot 3.0 Command line application.

        // Clean up the test login before we start testing
        // TODO:: Move to a test spring boot starter
        File file = new File("/tmp/emsServerId_7_login_testHarness.ems");
//        if (file.exists()) {
//            file.delete();
//        }

        if (System.getenv("EMS_CONFIG_FILE") == null &&
            System.getenv("EMS_CONFIG_FOLDER") == null)
        {
            if (new File("oms-ems/ems-config-mock-files").exists())
            {
                System.out.println("Using Developer mode - In source tree with no props set.");
                System.setProperty("EMS_CONFIG_FOLDER", "oms-ems/ems-config-mock-files");
                System.setProperty("EMS_CONFIG_FILE", "active_mock_fix_connections.yml");
            }
        }

        // defect in spring
        // https://stackoverflow.com/questions/14375673/how-to-fix-jsp-compiler-warning-one-jar-was-scanned-for-tlds-yet-contained-no-t
        System.setProperty("tomcat.util.scan.StandardJarScanFilter.jarsToSkip", "*");

        SpringApplicationBuilder app =
                new SpringApplicationBuilder(EmsServerGrpcSpringBootApplication.class);

        // turns off spring boot web, if desired
        // .web(WebApplicationType.NONE);

        // write a pid file with our Process ID; can be killed in bash
        // kill $(cat /tmp/ems-service.pid)
        app.build().addListeners(new ApplicationPidFileWriter("/tmp/ems-service.pid"));

        try {
            ConfigurableApplicationContext ctx = app.run();
            // SpringApplication.run(EmsServerGrpcSpringBootApplication.class, args);
            System.out.println("Finished main of ems-service");
            log.warn("SUCCESS, started ems-service");
        }
        catch (ApplicationContextException ace) {
            log.error("Spring Boot Application Failed to run:"+ace, ace);

            Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
            for (Thread t : threadSet) {
                log.error("Thread alive being killed="+t.getName() + " (daemon=" + t.isDaemon() + ")");
            }

            System.exit(-1);
        }
        catch (Error e) {
            log.error("ERROR< Failed to run app="+e.getMessage());

            Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
            for (Thread t : threadSet) {
                log.error(t.getName() + " (daemon=" + t.isDaemon() + ")");
            }

            System.exit(-99);
        }
        // continues to run with background threads..
        // TODO: ctx.close() does a safe shutdown

    }



}
