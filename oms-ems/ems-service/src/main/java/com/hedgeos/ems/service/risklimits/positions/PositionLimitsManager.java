package com.hedgeos.ems.service.risklimits.positions;


import com.hedgeos.ems.csvfiles.risklimits.position.PositionLimitLoad;
import com.hedgeos.ems.order.EmsPosition;
import com.hedgeos.ems.order.PmGroupSidKey;
import com.hedgeos.ems.service.ArraySearcher;
import com.hedgeos.ems.service.EmsEnvironmentManager;
import com.hedgeos.ems.service.PositionManager;
import com.hedgeos.ems.util.TradingDayUtil;
import general.Quantity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import order.OrderSubmitStatus;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

@RequiredArgsConstructor
@Component
@Slf4j
public class PositionLimitsManager {

    public static final int MAX_STALE_DAYS = 31;
    public final EmsEnvironmentManager emsEnvironmentManager;
    public final PositionManager positionManager;

    // maps for risk checking
    public static final int HOLD_31_DAYS_SIZE = 32;

    // positions, per day of month, ring buffer for speed;
    //  in production, should always be set for the trade date, prior to trading day opening
    // keeping this a buffer of object per date, should keep this in a cache line
    private final List<PositionLimitChecker> positionLimitCheckerList =
            new CopyOnWriteArrayList<>(new PositionLimitChecker[HOLD_31_DAYS_SIZE]);

    public void setPositionLimits(PositionLimitLoad positionLimits) {
        int dayOfMonth = positionLimits.getFileDate().getDayOfMonth();
        PositionLimitChecker checker = new PositionLimitChecker(positionLimits, positionManager);
        // copy on write, will swap out the array list for the next read
        positionLimitCheckerList.set(dayOfMonth, checker);

        cleanStalePositionLimits();
    }

    private void cleanStalePositionLimits() {
        // clean out any (very) old position limit checkers (unless in dev mode, and allowing stale limits)
        //noinspection PointlessBooleanExpression
        if (false == emsEnvironmentManager.isStaleLimitsEnabled()) {
            LocalDate tradedate = TradingDayUtil.getTradeDate();
            LocalDate thirtyDaysPrior = tradedate.minusDays(MAX_STALE_DAYS);
            Set<PositionLimitChecker> toRemove = new HashSet<>();
            for (PositionLimitChecker positionLimitChecker : positionLimitCheckerList) {
                if (positionLimitChecker.fileDate.isBefore(thirtyDaysPrior)) {
                    log.error("Removing ancient positionLimit > 31 days="+positionLimitChecker.fileDate);
                    toRemove.add(positionLimitChecker);
                }
            }
            positionLimitCheckerList.removeAll(toRemove);
        }
    }

    public PositionCheckRowResult checkPositionLimits(boolean preCheck,
                                                      PmGroupSidKey key,
                                                      int side,
                                                      Quantity quantity,
                                                      long hedgeOsOrderId,
                                                      LocalDate tradeDate,
                                                      EmsPosition position) {
        // look for both an account and sid match
        // first find the positions for the date
        // TODO:  We only care about current positions..
        PositionLimitChecker checker = positionLimitCheckerList.get(tradeDate.getDayOfMonth());
        if (checker == null) {
            if (emsEnvironmentManager.isStaleLimitsEnabled()) {
                checker = ArraySearcher.findPriorDateValue(tradeDate, positionLimitCheckerList);

                if (checker == null) {
                    String msg =
                            MessageFormat.format(
                                    "No viable stale position LIMITS for tradeDate={0}, " +
                                            "for orderId={1}", tradeDate, hedgeOsOrderId);
                    if (preCheck == false)
                        log.error(msg);
                    return new PositionCheckRowResult(null,
                            OrderSubmitStatus.NO_VIABLE_POSITION_LIMITS_FOR_TRADE_DATE, msg,
                            0L, 0L, 0L);
                }
            }
            else {
                String msg = MessageFormat.format("Missing POSITION LIMITS for " +
                        "tradeDate={0}, for orderId={1}", tradeDate, hedgeOsOrderId);
                if (preCheck == false)
                    log.error(msg);
                return new PositionCheckRowResult(null,
                        OrderSubmitStatus.MISSING_POSITION_LIMITS_FOR_TRADE_DATE, msg,
                        0L, 0L, 0L);
            }
        }

        // we have limits and positions, time to check
        PositionCheckRowResult result =
                checker.checkPositionLimit(preCheck, tradeDate, key, quantity, hedgeOsOrderId, side, position);

        return result;
    }


}
