package com.hedgeos.ems.service.connection;

import com.hedgeos.ems.service.EmsServerIdManager;
import com.hedgeos.ems.service.exception.NoRemainingConnections;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import order.ConnectRequest;
import order.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import static com.hedgeos.ems.order.OrderIdConstants.*;

/**
 * A critical class; be wary changing, needs more unit tests.
 */
@Slf4j
@Component
public class EmsConnectionManager {


    public static final int ZERO_INVALID_CONNECTION_ID = 0;
    final EmsConnectionStorage emsConnectionStorage;
    private final int emsServerId;

    public static final int SLOW_RECONNECT_AT_LEVEL = 500;
    public static final int SLOW_RECONNECT_MILLIS = 5_000;

    public static final int SYSTEM_CONNECTION_ID_ONE = 1;
    public static final int NON_SYSTEM_FIRST_CONNECTION_ID_TWO = SYSTEM_CONNECTION_ID_ONE +1;

    public static final String SYSTEM_LOGIN = "system";

    // start at 1, important; 0 is not a valid connectionId,
    // as it is the default value for an int
    final Map<String, ConnectionAndReconnectCount> loginToConnectionId = new ConcurrentSkipListMap<>();

    final Map<Integer, ConnectionAndReconnectCount> connectionIdMap = new ConcurrentHashMap<>();

    // TODO:  If login ever changes, is an exception.
    final Map<Integer, String> connectionIdToLogin = new ConcurrentHashMap<>();

    final Map<String, ReentrantLock> perLoginLock = new ConcurrentHashMap<>();

    // use hasing of the login with collisions to determine the connectionId
    final ConnectionAndReconnectCount[] connections = new ConnectionAndReconnectCount[62];

    public EmsConnectionManager(@Autowired  EmsServerIdManager emsServerIdManager,
                                @Autowired EmsConnectionStorage storage) {

        log.info("Reconnect limit={}", MAX_RECONNECTS_PER_CONNECTION);

        this.emsConnectionStorage = storage;
        this.emsServerId = emsServerIdManager.getEmsServerId();

        // load up the connection map from storage; to keep the reconnection count increasing
        List<ConnectionAndReconnectCount> counterFiles =
                emsConnectionStorage.loadConnectionInfo(emsServerIdManager.getEmsServerId());

        for (ConnectionAndReconnectCount counterFile : counterFiles) {
            loginToConnectionId.put(counterFile.getLogin(), counterFile);
            connectionIdMap.put(counterFile.getConnectionId(), counterFile);

            // take over the slot in the array for this connection post loading
            // makes connections sticky across ems-service restarts.
            synchronized (connections) {
                connections[counterFile.getConnectionId()] = counterFile;
            }
        }
    }

    // connecting does not need to be at light speed, and this is quick..
    public ConnectionAndReconnectCount getNextConnectionId(ConnectRequest request)
    throws NoRemainingConnections
    {
        // make a lock for each EMS connection user (systematic desk.)
        perLoginLock.putIfAbsent(request.login(), new ReentrantLock());
        ReentrantLock oneLoginLock = perLoginLock.get(request.login());

        try {
            // synchronized per login, to avoid data races managing connections
            boolean locked = oneLoginLock.tryLock(10, TimeUnit.SECONDS);

            if (locked == false) {
                String msg = "Could not obtain one login lock, more than 5 second lock on login=" + request.login();
                log.error(msg);
                // this will prevent login, but we have a severe problem accessing a disk for a tiny file
                throw new RuntimeException(msg);
            }

            ConnectionAndReconnectCount firstConnection =
                    makeOrFetchConnectionForLogin(request);

            ConnectionAndReconnectCount existing =
                    loginToConnectionId.putIfAbsent(request.login(), firstConnection);

            // if we've never seen this, write it to the connection file
            if (existing == null) {
                log.warn("First login from={}, returning connectionId=1", request.login());
                // here, it is the 2nd or greater login from this connection
                // this map is necessary for locking the system, for certain orderId increases
                connectionIdToLogin.put(firstConnection.getConnectionId(), request.login());
                emsConnectionStorage.writeConnectionCountFile(emsServerId, firstConnection);
                return firstConnection;
            }

            // increase the reconnect count, in place in the map
            int reconnectCount = existing.getReconnectCounter().incrementAndGet();

            // here, it is the 2nd or greater login from this connection
            // this map is necessary for locking the system, for certain orderId increases
            connectionIdToLogin.put(existing.getConnectionId(), request.login());
            // this MUST happen after the reconnect count increment above
            emsConnectionStorage.updateConnectionCountFile(emsServerId, existing);

            sleepIfRapidReconnect(request, reconnectCount);

            // copy for immutability
            AtomicInteger reconnect = new AtomicInteger(reconnectCount);

            // copy for immutability
            ConnectionAndReconnectCount result =
                    ConnectionAndReconnectCount.builder()
                            .connectionId(existing.getConnectionId())
                            .login(request.login())
                            .reconnectCounter(reconnect).build();

            log.warn("Returning connection="+existing.getConnectionId()+
                    ", reconnect=" + reconnectCount + ", for login=" + request.login());

            return result;

        } catch (InterruptedException e) {
            // try lock, but not failure, interrupted.
            throw new RuntimeException(e);
        } finally {
            // let another connection (from my own login), into this block, should never happen.
            oneLoginLock.unlock();
        }

    }

    /**
     * If a client makes an erroneous tight loop of reconnects; slow them down for many
     * hours and throw errors (for > 100 reconnects.)
     * @param request user request to connect
     * @param reconnectCount current count of reconnects
     */
    private static void sleepIfRapidReconnect(ConnectRequest request, int reconnectCount) {
        if (reconnectCount > SLOW_RECONNECT_AT_LEVEL) {
            log.error("(slowing connection speed) More than 100 reconnects from login=" + request.login());
            // this is helpful because a rogue re-connector will take more than 4 hours to burn
            // through 3000 reconnects, which is 3 trillion / 1 billion = # of reconnects max
            try {
                Thread.sleep(SLOW_RECONNECT_MILLIS);
            } catch (InterruptedException e) {}
        }
    }

    /**
     * This is sticky on connectionId
     *  as long as the hash codes for login does not collide with any other hash code;
     *  therefore, we can easily vend logins which don't collide.
     *
     *  Furthermore, clients try to reconnect and are given preference for
     *  the requested connectionId.  This means the system is sticky
     *  except for a contrived situation;
     *      during an outage - a new connection without
     *      any sticky reconnects from a client.
     *
     * @return ConnectAndReconnectCount the connection info for the login
     */
    synchronized ConnectionAndReconnectCount makeOrFetchConnectionForLogin(ConnectRequest request) throws NoRemainingConnections {
        String login = request.login();

        // if we've been connected, return the same connection as before
        if (loginToConnectionId.containsKey(login)) {
            ConnectionAndReconnectCount counter = loginToConnectionId.get(login);
            synchronized (connections) {
                connections[counter.getConnectionId()] = counter;
            }
            // the same connectionId as last time
            return counter;
        }

        // "system" login always receives connectionId = 1
        if (login.trim().equalsIgnoreCase(SYSTEM_LOGIN)) {
            ConnectionAndReconnectCount systemConnection =
                    new ConnectionAndReconnectCount(login, System.currentTimeMillis(),
                            SYSTEM_CONNECTION_ID_ONE, new AtomicInteger(0));
            synchronized (connections) {
                connections[SYSTEM_CONNECTION_ID_ONE] = systemConnection;
            }
            return systemConnection;
        }

        // if we were connected before, and the connection is available, give it back
        // this solves a problem with 'stickyness' below; where a collision can give an alternateId
        if (request.stickyConnectionIdRequest() > SYSTEM_CONNECTION_ID_ONE) {
            ConnectionAndReconnectCount oldConnection = connections[request.stickyConnectionIdRequest()];
            if (oldConnection.getLogin().equals(request.login())) {
                log.warn("Found the oldConnection for this login="+login+", re-using connectionId="+
                        request.stickyConnectionIdRequest());

                return oldConnection;
            }
            else {
                log.warn("Didn't find the oldConnection, but respecting the sticky request.");
                ConnectionAndReconnectCount newConnection =
                        new ConnectionAndReconnectCount(login, System.currentTimeMillis(),
                                request.stickyConnectionIdRequest(), new AtomicInteger(0));
                synchronized (connections) {
                    connections[request.stickyConnectionIdRequest()] = newConnection;
                }
                return newConnection;
            }
        }

        int hashOfLoginOnConnections = getNextAvailableConnectionIdFromArray(login);

        log.warn("Determined hash location for login, connectionId="+hashOfLoginOnConnections);

        long currentTimeMillisUtc = System.currentTimeMillis();

        ConnectionAndReconnectCount newConnection =
                new ConnectionAndReconnectCount(login, currentTimeMillisUtc,
                    hashOfLoginOnConnections, new AtomicInteger(0));

        // important, take the slot over for new connections
        synchronized (connections) {
            connections[hashOfLoginOnConnections] = newConnection;
        }

        return newConnection;
    }

    int getNextAvailableConnectionIdFromArray(String login) throws NoRemainingConnections {

        // we don't know until now, when we would try to find a slot in the connections array
        // therefore, if we are full, give up; don't search the array.
        // 60 = 62 minus 0=invalid, 1=system connection = 60 available.
        if (loginToConnectionId.size() == 60) {
            // no connections remain, don't attempt this.
            String msg = "No connections left on EMS; FAILURE TO CONNECT.";
            log.error(msg);
            throw new NoRemainingConnections();
        }

        // use the login to hash on the 62 connection count; to get a 'sticky' connectionId.
        int hashOfLoginOnConnections = login.hashCode() % connections.length;
        // advance to the next available connection

        // keep looking until we find one; guaranteed because we are not "full" per above
        while (connections[hashOfLoginOnConnections] != null)
        {
            // 0 is not a valid connectionId, and 1= system reserved
            if (hashOfLoginOnConnections == ZERO_INVALID_CONNECTION_ID ||
                    hashOfLoginOnConnections == SYSTEM_CONNECTION_ID_ONE) {
                continue;
            }

            hashOfLoginOnConnections++;
            // if we reach the end, go back to the 2nd (the 1st is for 'system')
            if (hashOfLoginOnConnections == connections.length) {
                hashOfLoginOnConnections = NON_SYSTEM_FIRST_CONNECTION_ID_TWO;
            }
        }
        return hashOfLoginOnConnections;
    }


    /**
     * Every day, reconnect count is a function of
     * a) reconnects
     * b) One billion orders (auto-incremented per reconnect)
     *  If one connection has
     *        1) grown to greater than one trillion orders (1000 reconnects),
     *        2) Connection has remained for > 24 hours.
     *      --> We can then recycle back to zero at 6pm UTC every day.
     *  This implies; indeed, we support MAX of  ONE TRILLION orders a day (per connectionId.)
     *  If a client needs more than a trillion orders; connect again to the EMS, with a new login.
     *
     * @param login login for the connectionId to check
     */
    @SneakyThrows
    public void resetReconnectCounts(String login) {
        ReentrantLock oneLoginLock = perLoginLock.get(login);
        if (oneLoginLock == null) {
            log.error("One login lock should never be null here in resetConnectCounts, login="+login);
            oneLoginLock = new ReentrantLock();
            perLoginLock.put(login, oneLoginLock);
        }

        try {
            boolean locked = oneLoginLock.tryLock(10, TimeUnit.SECONDS);
            if (locked == false) {
                log.error("Could not lock the perLoginLock for login=" + login);
                // throw new Error("Could not lock the perLoginLock for login="+login);
                return; // better to try again tomorrow than to stop the scheduled thread
            }

            ConnectionAndReconnectCount counter = loginToConnectionId.get(login);
            if (counter == null) {
                log.error("Counter should not be null for a login="+login);
                return;
            }

            if (counter.getReconnectCounter().get() > RECONNECTS_PER_TRILLION_OFFSETS) {
                // more than a thousand reconnects; have we been connected 24 hours on the last attempt?
                long now = System.currentTimeMillis();
                long timeConnectedMillis = now - counter.getConnectionTimeUtcMillis();
                // we support a trillion orders a day;
                // therefore, can reset to zero once we see a trillion orders, if you've been connected for a day
                if (timeConnectedMillis >= ONE_DAY_MILLIS) {
                    log.warn("24 hour connection, with " +
                            "one thousand reconnects="+login+", reconnects="+
                            counter.getReconnectCounter().get()+", resetting to 0");
                    // reset it to zero
                    counter.getReconnectCounter().set(0);
                }

                // write this new connection info out to the connection storage file.
                emsConnectionStorage.updateConnectionCountFile(emsServerId, counter);
            }
            else {
                log.info("No reconnect reset required, connection="+counter);
            }
        }
        finally {
            oneLoginLock.unlock();
        }
    }

    /**
     * @param hedgeOsOrderId unique order id from the EmsClient
     * @param connection the value the client knows as connection
     */
    @SneakyThrows
    public void checkOrIncreaseReconnectCountOffset(long hedgeOsOrderId, Connection connection)
    {
        long removeTrillions = hedgeOsOrderId % ONE_TRILLION_CONNECTION_ID_OFFSET;
        // calculate how high the reconnectCount needs to be, for this many orders
        long neededReconnectOffset = removeTrillions / ONE_BILLION_ORDERS_PER_RECONNECT_OFFSET;

        // if we need a reconnectOffset at a certain level (due to the high order count)
        // but reconnectOffset is detected as not high enough
        // 1) artificially increase the reconnect offset,
        // 2) save it in case of EMS Server restart
        int currentClientReconnectCount = connection.reconnectCount();

        // see if the client failed to set reconnect count; it is almost certainly not zero here
        // however, this test is still cheap, one hash map lookup
        if (currentClientReconnectCount == 0) {
            // fetch what the reconnectCount is today, the one we know (which is authoritative)
            ConnectionAndReconnectCount serverConnectionCopy = connectionIdMap.get(connection.connectionId());
            if (serverConnectionCopy == null) {
                log.error("No server connection for connectionId=" + connection.connectionId());
                return;
            }
            currentClientReconnectCount = serverConnectionCopy.getReconnectCounter().get();
        }

        // if what we need in the event of a disconnect is greater than the current value of reconnectCount
        // we need to increase it and save it to disk
        if (neededReconnectOffset > currentClientReconnectCount) {
            String login = connectionIdToLogin.get(connection.connectionId());
            if (login == null) {
                log.warn("No login for connectionId="+connection.connectionId());
                return;
            }

            ReentrantLock oneLoginLock = perLoginLock.get(login);
            if (oneLoginLock == null) {
                log.error("One login lock should never be null here, login="+login);
                oneLoginLock = new ReentrantLock();
                perLoginLock.put(login, oneLoginLock);
            }

            try {
                boolean locked = oneLoginLock.tryLock(5, TimeUnit.SECONDS);
                if (locked == false) {
                    log.error("Could not lock the perLoginLock for login=" + login);
                    throw new Error("Could not lock the perLoginLock for login="+login);
                }

                // fetch what the reconnectCount is today
                ConnectionAndReconnectCount serverConnectionCopy = connectionIdMap.get(connection.connectionId());
                if (serverConnectionCopy == null) {
                    log.error("No server connection for connectionId="+connection.connectionId());
                    return;
                }

                // increase the reconnect counter by one; which produces the client side offset
                serverConnectionCopy.getReconnectCounter().incrementAndGet();
                // Note, every billion orders, this call here to save will cause jitter on the next order.
                emsConnectionStorage.updateConnectionCountFile(emsServerId, serverConnectionCopy);
            }
            finally {
                oneLoginLock.unlock();
            }
        }
    }

    public boolean verifyReconnectCount(ConnectRequest request) {
        ConnectionAndReconnectCount counter = loginToConnectionId.get(request.login());
        // if never connected before, continue
        if (counter == null) {
            log.info("No previous connection for login={}", request.login());
            return true;
        }

        // prevent more than 3000 reconnects, due to orderId overflow
        // for ClientOrderIdStuffer
        // don't panic; this is 3000 reconnects, every day
        if (counter.getReconnectCounter().get() > MAX_RECONNECTS_PER_CONNECTION) {
            log.error("No more reconnects allowed for request={}", request.login());
            return false;
        }

        return true;
    }
}
