package com.hedgeos.ems.service;

import com.hedgeos.ems.service.risklimits.positions.PositionCheckRowResult;
import com.hedgeos.ems.service.risklimits.trades.TradingLimitCheckRowResult;
import lombok.RequiredArgsConstructor;
import order.Order;

@RequiredArgsConstructor
public class InternalLimitVerifyResult {
    final TradingLimitCheckRowResult tradingResult;
    final PositionCheckRowResult positionCheckRowResult;
    final Order order;

    final int status;

    final String message;
}
