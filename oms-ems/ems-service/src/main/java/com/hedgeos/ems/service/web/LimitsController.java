package com.hedgeos.ems.service.web;

import com.hedgeos.ems.order.PmGroupSidKey;
import com.hedgeos.ems.service.risklimits.trades.TradeAmountTracker;
import com.hedgeos.ems.service.risklimits.trades.TradeLimitsManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
@RequestMapping("/limits")
@Slf4j
public class LimitsController {

    public static final String LIMITS_CONTEXT = "limits";
    public static final String TRADE_TRACKER = "trade_tracker";
    public static final String STATUS = "status";
    public static final String ONE_SECURITY = "oneSecurity";
    public static final String FULL_CLEAR = "fullClear";

    private final TradeLimitsManager limitsManager;
    public LimitsController(@Autowired TradeLimitsManager manager) {
        this.limitsManager = manager;
    }

    // http://localhost:8080/limits/view
    @GetMapping(value = {"/view"})
    public String viewLimits(Model model) {
        model.addAttribute(LIMITS_CONTEXT, limitsManager.tradeAmountTracker.tradingPerSecurity);
        model.addAttribute(TRADE_TRACKER, limitsManager.tradeAmountTracker);

        return "view-limits"; // links to view-limits.jsp
    }

    @GetMapping(value = {"clearAmounts"})
    public String clearTradingAmounts(Model model) {
        log.error("CLEAR TRADING AMOUNTS CALLED - START");
        limitsManager.tradeAmountTracker.clearAllLimits();
        log.error("CLEAR TRADING AMOUNTS CALLED - END");
        model.addAttribute(STATUS, FULL_CLEAR);
        viewLimits(model);
        return "view-limits";
    }

    @GetMapping(value = {"clearOneSecurity"})
    public String clearLimitsOneSecurity(
            @ModelAttribute("pmGroup") String pmGroup,
            @ModelAttribute("sid") String sid,
            Model model)
    {
        PmGroupSidKey key = new PmGroupSidKey(Long.parseLong(sid), pmGroup);
        log.error("CLEAR ONE LIMIT CALLED - START: "+key);
        limitsManager.tradeAmountTracker.clearOneLimit(key);
        log.error("CLEAR ONE LIMIT CALLED - END: "+key);
        model.addAttribute(STATUS, ONE_SECURITY);
        viewLimits(model);
        return "view-limits";
    }
}
