package com.hedgeos.ems.service;

import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class TerminateBean {

    @Autowired
    private ApplicationContext context;

    @PreDestroy
    public void onDestroy() throws Exception {
         System.out.println("Spring Container is destroyed!");
    }


}
