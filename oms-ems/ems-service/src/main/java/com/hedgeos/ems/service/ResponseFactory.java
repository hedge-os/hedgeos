package com.hedgeos.ems.service;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.csvfiles.risklimits.position.PositionLimitsFileRow;
import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitsFileRow;
import com.hedgeos.ems.service.risklimits.positions.PositionCheckRowResult;
import com.hedgeos.ems.service.risklimits.trades.TradingLimitCheckRowResult;
import order.*;

public class ResponseFactory {
    public static CancelResponse createCancelResponse(Cancel cancel, int status) {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        CancelResponse.startCancelResponse(builder);
        CancelResponse.addHedgeOsOrderId(builder, cancel.hedgeOsOrderId());
        CancelResponse.addHedgeOsOrigOrderId(builder, cancel.hedgeOsOrigOrderId());
        CancelResponse.addStatus(builder, status);
        int offset = CancelResponse.endCancelResponse(builder);
        builder.finish(offset);
        CancelResponse cancelResponse = CancelResponse.getRootAsCancelResponse(builder.dataBuffer());
        return cancelResponse;
    }

    public static CancelReplaceResponse createCancelReplaceResponse(CancelReplace cancel,
                                                                    int statusCode,
                                                                    String message) {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int messageOffset = 0;
        if (statusCode != OrderSubmitStatus.SUCCESSFUL_ORDER) {
            builder.createString(message);
        }
        CancelReplaceResponse.startCancelReplaceResponse(builder);
        CancelReplaceResponse.addHedgeOsOrderId(builder, cancel.hedgeOsOrderId());
        CancelReplaceResponse.addHedgeOsOrigOrderId(builder, cancel.hedgeOsOrigOrderId());
        CancelReplaceResponse.addStatus(builder, statusCode);
        if (statusCode != OrderSubmitStatus.SUCCESSFUL_ORDER) {
            CancelReplaceResponse.addErrorMessage(builder, messageOffset);
        }
        int offset = CancelReplaceResponse.endCancelReplaceResponse(builder);
        builder.finish(offset);
        CancelReplaceResponse response = CancelReplaceResponse.getRootAsCancelReplaceResponse(builder.dataBuffer());
        return response;
    }

    public static OrderResponse createOrderResponse(Order order, int orderStatus, String errorMessage) {
        FlatBufferBuilder builder = new FlatBufferBuilder();

        int messageOffset = 0;
        if (errorMessage != null) {
            messageOffset = builder.createString(errorMessage);
        }
        OrderResponse.startOrderResponse(builder);
        OrderResponse.addHedgeosOrderId(builder, order.hedgeOsOrderId());
        OrderResponse.addStatus(builder, orderStatus);
        OrderResponse.addErrorMessage(builder, messageOffset);
        int offset = OrderResponse.endOrderResponse(builder);
        builder.finish(offset);
        OrderResponse response = OrderResponse.getRootAsOrderResponse(builder.dataBuffer());
        return response;
    }

    static int createLimitVerifyResponse(FlatBufferBuilder builder,
                                         Order order,
                                         int status,
                                         String errorMessage) {
        int errorOffset = 0;
        if (errorMessage != null) {
            errorOffset = builder.createString(errorMessage);
        }

        LimitVerifyOrderResponse.startLimitVerifyOrderResponse(builder);
        LimitVerifyOrderResponse.addHedgeOsOrderId(builder, order.hedgeOsOrderId());
        LimitVerifyOrderResponse.addTradingLimitStatus(builder, status);
        if (errorMessage != null)
            LimitVerifyOrderResponse.addTradingLimitErrorMessage(builder, errorOffset);

        int offset = LimitVerifyOrderResponse.endLimitVerifyOrderResponse(builder);
        return offset;
    }

    public static int createLimitVerifyResult(FlatBufferBuilder builder,
                                              Order order,
                                              TradingLimitCheckRowResult tradingLimitResult,
                                              PositionCheckRowResult positionLimitResult)
    {
        int tradingErrorOffset = 0;
        if (tradingLimitResult != null && tradingLimitResult.getMessage() != null) {
            tradingErrorOffset = builder.createString(tradingLimitResult.getMessage());
        }

        int positionErrorOffset = 0;
        if (positionLimitResult != null && positionLimitResult.getMessage() != null) {
            positionErrorOffset = builder.createString(positionLimitResult.getMessage());
        }

        int posFileNameOffset = 0;
        if (positionLimitResult != null && positionLimitResult.getRow() != null) {
            posFileNameOffset = builder.createString(positionLimitResult.getRow().getLimitsFile().getName());
        }
        int pmGroupOffset = 0;
        if (positionLimitResult != null && positionLimitResult.getRow() != null) {
            pmGroupOffset = builder.createString(positionLimitResult.getRow().getPmGroup());
        }

        int tradingLimitRowOffset = 0;
        if (tradingLimitResult != null && tradingLimitResult.getRow() != null) {
            TradeLimitsFileRow row = tradingLimitResult.getRow();

            int timeUnitOffset = 0;
            if (tradingLimitResult.getRow().getTimeUnit() != null)
                timeUnitOffset = builder.createString(tradingLimitResult.getRow().getTimeUnit());

            int fileNameOffset = 0;
            if (row.getFileOfRow() != null) {
                fileNameOffset = builder.createString(row.getFileOfRow().getName());
            }

            int rowPmGroupOffset = 0;
            if (row.getPmGroup() != null) {
                rowPmGroupOffset = builder.createString(row.getPmGroup());
            }

            TradingLimitFileRow.startTradingLimitFileRow(builder);

            if (fileNameOffset != 0) {
                TradingLimitFileRow.addFileName(builder, fileNameOffset);
            }

            if (pmGroupOffset != 0) {
                TradingLimitFileRow.addPmGroup(builder, rowPmGroupOffset);
            }

            if (timeUnitOffset != 0) {
                TradingLimitFileRow.addTimeUnit(builder, timeUnitOffset);
            }

            TradingLimitFileRow.addIsPmGroupWildcard(builder, row.isPmGroupWildcard());

            if (row.isSidWildcard() == false)
                TradingLimitFileRow.addSid(builder, row.getSid());

            TradingLimitFileRow.addIsSidWildcard(builder, row.isSidWildcard());
            TradingLimitFileRow.addQtyLong(builder, row.getQtyLong());
            TradingLimitFileRow.addQtyShort(builder, row.getQtyShort());
            if (row.getExposureLong() != null)
                TradingLimitFileRow.addExposureLong(builder, row.getExposureLong());
            if (row.getExposureShort() != null)
                TradingLimitFileRow.addExposureShort(builder, row.getExposureShort());

            TradingLimitFileRow.addNumTimeUnits(builder, row.getNumTimeUnits());
            tradingLimitRowOffset = TradingLimitFileRow.endTradingLimitFileRow(builder);

        }

        int positionLimitRowOffset = 0;
        if (positionLimitResult != null) {
            PositionLimitsFileRow posFileRow = positionLimitResult.getRow();

            PositionLimitFileRow.startPositionLimitFileRow(builder);
            if (posFileNameOffset != 0)
                PositionLimitFileRow.addFileName(builder, posFileNameOffset);
            if (pmGroupOffset != 0)
                PositionLimitFileRow.addPmGroup(builder, pmGroupOffset);

            PositionLimitFileRow.addQtyLong(builder, posFileRow.getQtyLong());
            PositionLimitFileRow.addQtyShort(builder, posFileRow.getQtyShort());
            PositionLimitFileRow.addIsSidWildcard(builder, posFileRow.isSidWildcard());
            PositionLimitFileRow.addIsPmGroupWildcard(builder, posFileRow.isPmGroupWildcard());
            PositionLimitFileRow.addRowNumber(builder, posFileRow.getFileRowNumber());
            if (posFileRow.getExposureLong() != null) {
                PositionLimitFileRow.addExposureLong(builder, posFileRow.getExposureLong().longValue());
            }
            if (posFileRow.getExposureShort() != null) {
                PositionLimitFileRow.addExposureShort(builder, posFileRow.getExposureShort().longValue());
            }

            positionLimitRowOffset = PositionLimitFileRow.endPositionLimitFileRow(builder);
        }

        // start the overall result
        LimitVerifyOrderResponse.startLimitVerifyOrderResponse(builder);
        LimitVerifyOrderResponse.addHedgeOsOrderId(builder, order.hedgeOsOrderId());

        // add trading limit if it was checked (always is)
        if (tradingLimitResult != null) {
            LimitVerifyOrderResponse.addTradingLimitStatus(builder, tradingLimitResult.getOrderSubmitStatus());
            if (tradingLimitResult.getMessage() != null) {
                LimitVerifyOrderResponse.addTradingLimitErrorMessage(builder, tradingErrorOffset);
            }
            if (tradingLimitRowOffset != 0) {
                LimitVerifyOrderResponse.addTradingFileRow(builder, tradingLimitRowOffset);
            }
        }

        // add position limit check result if it exists (always does)
        if (positionLimitResult != null) {
            LimitVerifyOrderResponse.addPositionLimitStatus(builder, positionLimitResult.getOrderSubmitStatus());
            if (positionLimitResult.getMessage() != null) {
                LimitVerifyOrderResponse.addPositionLimitErrorMessage(builder, positionErrorOffset);
            }
            if (positionLimitRowOffset != 0) {
                LimitVerifyOrderResponse.addPositionFileRow(builder, positionLimitRowOffset);
            }
        }

        // finish
        int offset = LimitVerifyOrderResponse.endLimitVerifyOrderResponse(builder);
        return offset;
    }
}
