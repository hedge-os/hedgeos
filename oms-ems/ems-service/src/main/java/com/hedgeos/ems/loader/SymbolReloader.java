package com.hedgeos.ems.loader;

import com.hedgeos.ems.config.beans.CompleteEmsConfig;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import com.hedgeos.ems.csvfiles.symbology.SymbolLoaderFactory;
import com.hedgeos.ems.csvfiles.symbology.SymbolManager;
import com.hedgeos.ems.csvfiles.symbology.SymbolMaps;
import com.hedgeos.ems.util.TradingDayUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@RequiredArgsConstructor
@Slf4j
@Component
public class SymbolReloader {

    public final SymbolManager symbolManager;

    public List<Reload> loadSymbolsForEachFixAdapter(CompleteEmsConfig completeEmsConfig) {
        Set<String> symbolsLoaded = new HashSet<>();

        List<Reload> reloaders = new ArrayList<>();
        for (FixAdapterConfig fixAdapterConfig : completeEmsConfig.getFixAdapterConfigList()) {
            LocalDate tradeDate = TradingDayUtil.getTradeDate();

            // reload the symbol file and look for overlays, every five minutes recurring
            // we only need the symbols loaded once, if two FixAdapters use the same symbols
            //noinspection PointlessBooleanExpression
            if (false == symbolsLoaded.contains(fixAdapterConfig.getSymbology())) {
                log.warn("Looking for symbols files for symbology="+fixAdapterConfig.getSymbology());

                // load the symbols for this fix adapter at startup
                SymbolMaps symbolMaps =
                        SymbolLoaderFactory.getPriorityAndFullSymbolMap(tradeDate,
                                fixAdapterConfig.getSymbology(),
                                completeEmsConfig.getEmsConfigFolder());

                // send the first update to symbology manager
                symbolManager.updateSymbolMaps(fixAdapterConfig.getSymbology(), symbolMaps);

                Reload reloader =
                        new SymbolReload(fixAdapterConfig.getSymbology(),
                                symbolManager,
                                symbolMaps.symbolsDate,
                                symbolMaps.latestSymbolFileTime,
                                completeEmsConfig.getEmsConfigFolder());

                reloaders.add(reloader);

                // we only need one reload thread per symbology; which may be shared on many FixAdapters
                symbolsLoaded.add(fixAdapterConfig.getSymbology());
            }
        }

        return reloaders;
    }

    public static class SymbolReload implements Reload {

        private final String symbology;
        private final File emsConfigFolder;
        public SymbolManager symbolManager;
        public final Map<LocalDate, Long> lastReloadMap = new ConcurrentHashMap<>();

        public SymbolReload(String symbology, SymbolManager manager,
                            LocalDate fileDate, long lastReloadTime,
                              File emsConfigFolder) {
            symbolManager = manager;
            lastReloadMap.put(fileDate,lastReloadTime);
            this.symbology = symbology;
            this.emsConfigFolder = emsConfigFolder;
        }

        @Override
        public void reloadIfNewer(LocalDate tradeDate) {
            try {
                // note:  the system will run with old symbol maps, but they should be refreshed daily.
                SymbolMaps reloadMaps =
                        SymbolLoaderFactory.getPriorityAndFullSymbolMap(tradeDate,
                                symbology,
                                emsConfigFolder);

                Long lastReloadTime = lastReloadMap.get(reloadMaps.symbolsDate);
                // Only update Symbols this if the file changes, to avoid jitter
                if (lastReloadTime != null && lastReloadTime == reloadMaps.latestSymbolFileTime) {
                    log.debug("Symbol map file timestamp did not change for symbology=" + symbology);
                } else {
                    log.warn("Found newer file for symbology={}, time={}", symbology, reloadMaps.latestSymbolFileTime);
                    symbolManager.updateSymbolMaps(symbology, reloadMaps);
                    // update the current check to the reloaded maps
                    lastReloadMap.put(tradeDate, reloadMaps.latestSymbolFileTime);
                    // now with a new symbolMap, likely we can clean out a prior tradeDate
                    symbolManager.cleanOldSymbols(symbology, tradeDate);
                }

            } catch (Exception e) {
                // keep trying, if the file is malformed, can't reach the filesystem, etc
                log.error("Exception loading symbol map, will retry in 5 minutes.", e);
            }

        }
    }
}
