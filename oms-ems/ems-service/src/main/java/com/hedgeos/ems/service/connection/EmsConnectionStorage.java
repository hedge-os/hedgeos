package com.hedgeos.ems.service.connection;

import java.util.List;

public interface EmsConnectionStorage {
    void writeConnectionCountFile(int emsServerId, ConnectionAndReconnectCount connectionInfo);

    void updateConnectionCountFile(int emsServerId, ConnectionAndReconnectCount connectionInfo);

    List<ConnectionAndReconnectCount> loadConnectionInfo(int emsServerId);

}
