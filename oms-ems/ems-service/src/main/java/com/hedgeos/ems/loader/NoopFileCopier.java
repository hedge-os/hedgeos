package com.hedgeos.ems.loader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NoopFileCopier implements FileCopier {
    @Override
    public void copyFilesIntoPlace() {
        log.warn("Copy the files into the ${EMS_CONFIG_FOLDER}/positions and other sub-folders");
    }
}
