package com.hedgeos.ems.service.connection;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.concurrent.atomic.AtomicInteger;

@RequiredArgsConstructor
@Getter
@ToString
@Builder
public class ConnectionAndReconnectCount implements Comparable<ConnectionAndReconnectCount> {

    private final String login;

    // only sweep the reconnectCounter after 24 hours of operation (orderIds unique per day)
    private final long connectionTimeUtcMillis;

    private final int connectionId;
    private final AtomicInteger reconnectCounter;


    @Override
    public int compareTo(ConnectionAndReconnectCount other) {
        int loginCompare = login.compareTo(other.login);
        if (loginCompare != 0)
            return loginCompare;

        return Integer.compare(connectionId, other.connectionId);
    }
}
