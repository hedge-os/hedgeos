package com.hedgeos.ems.service;

import com.hedgeos.ems.EmsServerGrpcSpringBootApplication;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.SmartLifecycle;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

import static com.hedgeos.ems.EmsServerGrpcSpringBootApplication.PORT_UNAVAILABLE_EXIT_CODE;

/**
 * We start the grpcServer after the FIX connections are started.
 * This is a hack to check that the port is open for the grpcServer
 * , prior to starting the FIX Connections in FixServerLifecycle.
 */
@Slf4j
public class CheckPortLifecycle implements SmartLifecycle  {
    private final int port;

    private volatile boolean isRunning;

    @Autowired
    ApplicationContext context;

    public CheckPortLifecycle(int port) {
        this.port = port;
    }

    // CHECK THE PORT IS OPEN FIRST
    @Override
    public int getPhase() {
        return EmsServerGrpcSpringBootApplication.CHECK_PORT_LIFECYCLE_PHASE;
    }

    @Override
    public void start() {

        boolean isSpringPortAvailable = available(port);
        if (isSpringPortAvailable == false) {
            String msg = "Port "+port+" is NOT AVAILABLE";
            log.error(msg);

            int exitCode = SpringApplication.exit(context, new ExitCodeGenerator() {
                @Override
                public int getExitCode() {
                    // return the error code
                    return PORT_UNAVAILABLE_EXIT_CODE;
                }
            });

            log.error(msg);
            System.exit(exitCode);
        }

        isRunning = true;
    }


    private static boolean available(int port) throws IllegalStateException {
        try (Socket ignored = new Socket("localhost", port)) {
            return false;
        } catch (ConnectException e) {
            return true;
        } catch (IOException e) {
            throw new IllegalStateException("Error while trying to check open port", e);
        }
    }

    @Override
    public void stop() {
        log.warn("Stop called on CheckPortLifecycle");
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public boolean isAutoStartup() {
        return true;
    }
}
