package com.hedgeos.ems.service;

import com.hedgeos.fix.adapter.FixObserver;
import lombok.extern.slf4j.Slf4j;
import order.EmsReport;
import order.EmsReportEnvelope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EmsFixObserver implements FixObserver {

    private final EmsMultiplexer emsMultiplexer;

    @Autowired
    public EmsFixObserver(EmsMultiplexer multiplexer) {
        this.emsMultiplexer = multiplexer;
    }

    @Override
    public void onMessage(EmsReportEnvelope envelope, EmsReport emsReport) {

        // log.warn("exec report in callback=" + emsReport);
        emsMultiplexer.processAndRoute(emsReport);
    }


}
