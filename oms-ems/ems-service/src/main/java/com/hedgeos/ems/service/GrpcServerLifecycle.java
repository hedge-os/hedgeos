package com.hedgeos.ems.service;

import com.hedgeos.ems.EmsServerGrpcSpringBootApplication;
import com.hedgeos.ems.service.connection.RemoveDisconnectedStreamObserverFilter;
import com.hedgeos.grpc.utils.NettyEpollNativeLoader;
import com.hedgeos.grpc.utils.PinnedCoreAffinityThreadFactory;
import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerTransportFilter;
import io.grpc.netty.NettyServerBuilder;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.ReflectiveChannelFactory;
import io.netty.channel.ServerChannel;
import io.netty.channel.epoll.EpollEventLoopGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.SmartLifecycle;

import java.io.IOException;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Slf4j
public class GrpcServerLifecycle implements SmartLifecycle {
    private final List<BindableService> services;
    private final int port;
    private final NettyEpollNativeLoader nettyEpollNativeLoader;
    private Server grpcServer;

    @Autowired
    ConfigurableApplicationContext ctx;

    private ServerTransportFilter removeClosedConnectionServerFilter =
            new RemoveDisconnectedStreamObserverFilter();

    public GrpcServerLifecycle(List<BindableService> services, int port) {
        this.services = requireNonNull(services);
        this.port = port;
        this.nettyEpollNativeLoader = new NettyEpollNativeLoader();
    }

    @Override
    public int getPhase() {
        // int min value --> int max value, things are started in this order by Spring.
        return EmsServerGrpcSpringBootApplication.GRPC_SERVER_LIFECYCLE_PHASE; // startup gRPC after everything else has started
    }

    // GrpcServer can be auto-started
    @Override
    public boolean isAutoStartup() {
        return true;
    }

    @Override
    public synchronized void start() {
        log.warn("Starting *** ems-server ** (flatbuffers gRPC server)");
        if (!isRunning()) {
            createServer();

            try {
                grpcServer.start();
                // Prevent the JVM from shutting down while the server is running
                final Thread awaitThread = new Thread(() -> {
                    try {
                        log.warn("grpcServer is listening port {}", grpcServer.getPort());
                        grpcServer.awaitTermination();
                    } catch (final InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                });
                awaitThread.setName("grpc-server-awaiter");
                awaitThread.setDaemon(false);
                awaitThread.start();
            } catch (IOException e) {
                ctx.getBean(TerminateBean.class);
                ctx.close();
                // TODO:  This needs to exit the JVM, port 9090 in use
                log.error("Failed to start gRPC server, very bad.",e);
                throw new Error(e);
            }
        }
    }

    private void createServer() {

        boolean loadedOpimizedEpollNetty = tryLoadingOptimizedNettyEpoll();

        if (loadedOpimizedEpollNetty == false) {
            log.warn("Loading Netty NIO server.");
            NettyServerBuilder nettyServerBuilder =
                    NettyServerBuilder.forPort(port)
                            .directExecutor();
            services.forEach(nettyServerBuilder::addService);
            grpcServer = nettyServerBuilder.build();
        }

        log.warn("Netty load complete.");
    }

    private boolean tryLoadingOptimizedNettyEpoll() {

        // to disable:
        //        if (true)
        //            return false;

        try {

            // TODO:  Optimize such that these are to cores on one socket, and re-test
            // one acceptor thread, is enough indeed
            final int acceptorThreads = 1;
            // for my test, one worker thread
            final int workerThreads = 1;
            // this will put workers on different cores, with affinity
            // ThreadFactory threadFactory = new AffinityThreadFactory("atf_wrk", AffinityStrategies.DIFFERENT_SOCKET);

            // pin the acceptor to core 3, which is 'isolcpu' isolated
            PinnedCoreAffinityThreadFactory pinnedThree = new PinnedCoreAffinityThreadFactory("acceptor", 4);
            EventLoopGroup acceptorGroup = new EpollEventLoopGroup(acceptorThreads, pinnedThree);

            // pin the worker to core 4, which is 'isolcpu' isolated
            PinnedCoreAffinityThreadFactory pinnedFour = new PinnedCoreAffinityThreadFactory("worker", 3);
            EventLoopGroup workerGroup = new EpollEventLoopGroup(workerThreads, pinnedFour);

            // try to load the netty native library, needed for EpollServerSocketChannel
            boolean loaded = nettyEpollNativeLoader.loadNettyEpoll();

            Class<? extends ServerChannel> serverSocketChannel =
                    Class.forName("io.netty.channel.epoll.EpollServerSocketChannel")
                            .asSubclass(ServerChannel.class);

            NettyServerBuilder nettyServerBuilder =
                    NettyServerBuilder.forPort(port)
                            .channelFactory(new ReflectiveChannelFactory<>(serverSocketChannel))
                            // direct executor with 1 thread should be quickest
                            .directExecutor()
                            .addTransportFilter(removeClosedConnectionServerFilter)
                            .workerEventLoopGroup(workerGroup)
                            .bossEventLoopGroup(acceptorGroup);
            // .withOption(ChannelOption.MAX_MESSAGES_PER_WRITE, 64)
            // TODO:  Further tunings, carefully, on a big machine
//                            .withOption(ChannelOption.SO_KEEPALIVE, true)
//                            .withOption(ChannelOption.TCP_NODELAY, true);

            // Direct Executor makes sense, single threaded
            services.forEach(nettyServerBuilder::addService);

            grpcServer = nettyServerBuilder.build();

            return true;

        } catch (ClassNotFoundException e) {
            log.error("Class not found for io.netty.channel.epoll.EpollServerSocketChannel?"+e.getMessage(), e);
            log.warn("Reverting to NIO, which is reasonable.");
            return false;
        }
        catch (Error error) {
            log.warn("Exception loading Netty Epoll: "+error.getMessage());
            log.warn("Reverting to NIO, which is reasonable.");
            return false;
        }
    }

    @Override
    public synchronized void stop() {
        log.warn("Stopping ems-service grpcServer");
        if (grpcServer != null) {
            grpcServer.shutdownNow();
            log.warn("STOPPED ems-service grpcServer");
            this.grpcServer = null;
        }
    }

    @Override
    public synchronized boolean isRunning() {
        if (grpcServer != null) {
            return !grpcServer.isShutdown() && !grpcServer.isTerminated();
        }
        return false;
    }

}
