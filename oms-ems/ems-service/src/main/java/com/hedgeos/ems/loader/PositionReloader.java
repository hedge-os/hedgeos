package com.hedgeos.ems.loader;

import com.hedgeos.ems.config.beans.CompleteEmsConfig;
import com.hedgeos.ems.csvfiles.positions.PositionLoader;
import com.hedgeos.ems.csvfiles.positions.PositionLoadMap;
import com.hedgeos.ems.csvfiles.util.FlatFileUtil;
import com.hedgeos.ems.service.EmsEnvironmentManager;
import com.hedgeos.ems.service.EmsMultiplexer;
import com.hedgeos.ems.util.TradingDayUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.datetime.standard.DateTimeFormatterFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.hedgeos.ems.csvfiles.positions.PositionLoader.POSITIONS_FILES_PREFIX;
import static com.hedgeos.ems.csvfiles.util.FlatFileUtil.*;

@Component
@RequiredArgsConstructor
@Slf4j
public class PositionReloader {

    private final EmsMultiplexer emsMultiplexer;
    private final EmsEnvironmentManager emsEnvironmentManager;
    private static final String FAKE_DEV_POSITIONS_FILE = "positions_DEV_ROLL_TO_TODAY.csv";

    public Reloader loadPositionsAndStartReloadThread(CompleteEmsConfig completeEmsConfig, LocalDate tradeDate) {
        // load positions on startup; and need to check for positions every minute
        PositionLoader loader = new PositionLoader(completeEmsConfig.getEmsConfigFolder());

        // if it does not find the file, throws missing file ERROR (which is good.)
        // can't trade with stale positions
        File currentPositionFile =
                findMostRecentFile(tradeDate, loader.getPositionsFolder(), POSITIONS_FILES_PREFIX);
        LocalDate dateFromFile = FlatFileUtil.getDateForFile(currentPositionFile);

        // TODO:  Use spring to inject this, from test code, not like this
        if (emsEnvironmentManager.isStalePositionsEnabled()) {
            File devFile =
                    new File(loader.getPositionsFolder()+File.separator+ FAKE_DEV_POSITIONS_FILE);
            if (currentPositionFile.exists()) {
                currentPositionFile = devFile;
            }
            dateFromFile = TradingDayUtil.getTradeDate();
        }

        DateTimeFormatter formatter = new DateTimeFormatterFactory(YYYYMMDD).createDateTimeFormatter();
        String localDateString = formatter.format(tradeDate);
        // we must have positions for the start date; fail-fast if no positions loaded
        // as it means we have not configured the ems-service
        //noinspection PointlessBooleanExpression
        if (false == currentPositionFile.getName().contains(localDateString)) {
            if (emsEnvironmentManager.isWorkstation()) {
                log.warn("Continuing with old positions file="+currentPositionFile+", due to developer workstation.");
            }
            else {
                // in Development, or Prod, fail if the positions file doesn't exist for the trade date starting on.
                // If starting on Sunday, is a position file necessary?
                // FAQ:  The operators can provide an empty file
                // or the file from Friday, if Sunday is the day the system is started.
                String msg = "Must provide positions file named= [positions_"
                                + localDateString + ".csv] in folder=[" + loader.getPositionsFolder() + "]";
                log.error(msg);
                throw new Error(msg);
            }
        }

        // load the positions one time to bootstrap; we need them for the current trade date
        PositionLoadMap positions =
                loader.loadPositions(currentPositionFile, true, dateFromFile);

        // set us up for success on the first trading day
        emsMultiplexer.setFileLoadedPositions(positions);

        // reload positions when they arrive, either for today (emergency reload)
        // or future position dates, once available
        return new Reloader(emsMultiplexer, positions.getFileDate(), positions.getLatestTimestamp(), loader);
    }



    private static class Reloader implements Reload {

        private final PositionLoader loader;
        private Map<LocalDate, Long> lastLoadMap = new ConcurrentHashMap<>();
        private final EmsMultiplexer emsMultiplexer;

        public Reloader(EmsMultiplexer emsMultiplexer,
                        LocalDate tradeDate, long firstLoad, PositionLoader loader) {
            this.lastLoadMap.put(tradeDate, firstLoad);
            this.emsMultiplexer = emsMultiplexer;
            this.loader = loader;
        }

        @Override
        public void reloadIfNewer(LocalDate tradeDate) {
            try {
                File currentPositionFile =
                        findMostRecentFile(tradeDate,
                                loader.getPositionsFolder(), POSITIONS_FILES_PREFIX);

                LocalDate dateFromFile = FlatFileUtil.getDateForFile(currentPositionFile);

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(YYYYMMDD);
                String tradeDateString = tradeDate.format(formatter);

                // NEVER LOAD POSITIONS unless match this trade date; wait until tradeDate
                // (We do not want any data in the maps in memory to reflect falsehoods for POSITIONS)

                //noinspection PointlessBooleanExpression
                if (false == currentPositionFile.toString().contains(tradeDateString)) {
                    log.debug("Positions file has not arrived for tradeDate="+tradeDateString);
                }
                else {
                    PositionLoadMap positionsForTradeDate =
                            loader.loadPositions(currentPositionFile, false, dateFromFile);

                    Long lastLoadTime = lastLoadMap.get(positionsForTradeDate.getFileDate());
                    if (lastLoadTime != null && positionsForTradeDate.getLatestTimestamp() <= lastLoadTime) {
                        log.debug("Positions file have not changed=" + positionsForTradeDate.getLatestTimestamp());
                    }
                    else {
                        // now loadThePositions is = true
                        positionsForTradeDate =
                                loader.loadPositions(currentPositionFile, true, dateFromFile);
                        log.warn("Found new position file=" + positionsForTradeDate.getLatestTimestamp() +
                                ", loaded positions=" + positionsForTradeDate.getPositionFileRowMap().size());
                        // once a new file is loaded; this becomes the current known positions
                        // see the Decision 0008 in the decision docs folder.
                        emsMultiplexer.setFileLoadedPositions(positionsForTradeDate);
                        // update the latest load time
                        lastLoadMap.put(tradeDate, positionsForTradeDate.getLatestTimestamp());
                    }
                }
            }
            catch (Exception e) {
                // TODO:  Needs to generate an email and UX error (likely via Kafka)
                log.error("Error loading position files, will sleep and retry in a minute.", e);
            }
        }
    }
}
