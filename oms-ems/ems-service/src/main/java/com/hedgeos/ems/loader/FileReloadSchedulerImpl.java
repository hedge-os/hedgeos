package com.hedgeos.ems.loader;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;

@RequiredArgsConstructor
@Slf4j
public class FileReloadSchedulerImpl implements FileReloadScheduler {

    private Runnable allReloads;

    private final FileCopier fileCopier;

    @Override
    public void addReloads(Runnable allReloads) {
        this.allReloads = allReloads;
    }

    // check for new files every minute; wait for a minute before the reloads fire
    @Override
    @Scheduled(initialDelay = 60000, fixedDelayString = "${reloadFileCheckTimeMillis:60000}")
    public void scheduledRun() {
        try {
            fileCopier.copyFilesIntoPlace();
            if (allReloads == null) {
                log.error("All reloads has not been set!");
                return;
            }

            allReloads.run();

            log.debug("FINISHED RELOADING ALL FILES");
        }
        catch (Exception e) {
            log.error("Exception on scheduled run of all file reloads", e);
            // TODO:  This may be terrible; might send an email on error
            // TODO:  Need to send emails on errors
        }
    }
}
