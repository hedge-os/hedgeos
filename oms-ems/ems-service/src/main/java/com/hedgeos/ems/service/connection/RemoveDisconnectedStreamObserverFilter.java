package com.hedgeos.ems.service.connection;

import io.grpc.Attributes;
import io.grpc.ServerTransportFilter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RemoveDisconnectedStreamObserverFilter extends ServerTransportFilter {
    @Override
    public void transportTerminated(Attributes transportAttrs) {
        super.transportTerminated(transportAttrs);
        Object value = transportAttrs.get(io.grpc.Grpc.TRANSPORT_ATTR_REMOTE_ADDR);
        // good to know when we lost a connection
        log.warn("Client disconnected abruptly: "+value);
    }

    @Override
    public Attributes transportReady(Attributes transportAttrs) {
        Attributes atts = super.transportReady(transportAttrs);
        // log.warn("Client connected");
        return atts;
    }
}
