package com.hedgeos.ems.service.risklimits.trades.cleaners;

import com.hedgeos.ems.service.risklimits.trades.TradeAmountTracker;

import java.util.concurrent.atomic.AtomicIntegerArray;

public interface OneArrayCleaner {
    void cleanFunction(TradeAmountTracker.TradeActivityBlocks blocks);
}
