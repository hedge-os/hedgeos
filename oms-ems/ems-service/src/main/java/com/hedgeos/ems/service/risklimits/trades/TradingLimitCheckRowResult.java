package com.hedgeos.ems.service.risklimits.trades;

import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitsFileRow;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
public class TradingLimitCheckRowResult {

    private final TradeLimitsFileRow row;
    private final String message;
    private final int orderSubmitStatus;

    private final long limitLevel;
    private final double currentLevel;
    private final double requestedLevel;
}
