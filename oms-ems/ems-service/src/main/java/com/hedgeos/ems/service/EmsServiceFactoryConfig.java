package com.hedgeos.ems.service;

import com.hedgeos.ems.config.EmsConfigVariables;
import com.hedgeos.ems.loader.FileReloadScheduler;
import com.hedgeos.ems.loader.FileReloadSchedulerImpl;
import com.hedgeos.ems.loader.NoopFileCopier;
import com.hedgeos.ems.service.connection.EmsConnectionManager;
import com.hedgeos.ems.service.connection.EmsConnectionStorage;
import com.hedgeos.ems.service.connection.TmpFileEmsConnectionStorage;
import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.context.LifecycleProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import static com.hedgeos.ems.config.EmsConfigFileLocationUtil.EMS_CONFIG_FILE;
import static com.hedgeos.ems.config.EmsConfigFileLocationUtil.EMS_CONFIG_FOLDER;

/**
 * This is the equivalent of a Spring XML to configure an abstract factory.
 *  yet, it is better because it is at compile time.
 *
 * TODO: Move this to test, and the GettingStarted is to author one of these; partially as XML
 *  to provide an S3 file loader instead of the NoopFileCopier
 *  The file copier puts the files in place; this can copy the files locally from S3 for example
 *
 * Without this, there are no services in the GrpcLifecycle.
 *  TODO:  This is too magical.
 */
@Configuration
@ConfigurationPropertiesScan(basePackageClasses = EmsLifecycleProperties.class)
@EnableScheduling
@Slf4j
public class EmsServiceFactoryConfig {

    // hack to allow this to be overridden by tests
    @Value("${test.ems.config.folder:null}")
    private String testEmsConfigFolder;

    @Value("${test.ems.config.file:null}")
    private String testEmsConfigFile;

//    @Bean(name = QUICKFIX_ADAPTER_EMS)
//    public FixAdapter quickfixAdapterEms() {
//        return new QuickfixAdapter();
//    }

    @Bean
    @Primary
    public LifecycleProperties provideLifecycleProperties() {
        return new EmsLifecycleProperties();
    }

    @Bean
    public EmsConfigVariables emsConfigVariables() {
        // grab these two most critical variables,
        // log them to System out for good measure
        String emsConfigFolderName = System.getenv(EMS_CONFIG_FOLDER);
        String emsConfigFileName = System.getenv(EMS_CONFIG_FILE);

        if (emsConfigFolderName == null) {
            log.warn("EMS_CONFIG_FOLDER environment variable is null, checking for java system property");
            emsConfigFolderName = System.getProperty(EMS_CONFIG_FOLDER);
        }

        if (emsConfigFileName == null)
        {
            log.warn("EMS_CONFIG_FILE environment variable is null, checking for java system property");
            emsConfigFileName = System.getProperty(EMS_CONFIG_FILE);
        }

        // a hack for testing in Spring 3.0
        if (emsConfigFolderName == null && testEmsConfigFolder != null) {
            log.warn("Using TEST ems config folder="+testEmsConfigFolder);
            emsConfigFolderName = testEmsConfigFolder;
        }
        if (emsConfigFileName == null && testEmsConfigFile != null) {
            log.warn("Using TEST ems config file ="+testEmsConfigFile);
            emsConfigFileName = testEmsConfigFile;
        }

        return new EmsConfigVariables(emsConfigFolderName, emsConfigFileName);
    }

    /**
     * This is strictly necessary, otherwise, will be no services in GrpcStartLifecycle
     */
    @Bean
    EmsService provideEmsService(@Autowired EmsMultiplexer emsMultiplexer,
                                 @Autowired EmsConnectionManager emsConnectionManager,
                                 @Autowired LoginAndPasswordChecker loginCountChecker,
                                 @Autowired EmsServerIdManager emsServerIdManager,
                                 @Autowired MeterRegistry meterRegistry)
    {
        int emsServerId = emsServerIdManager.getEmsServerId();
        return new EmsService(emsServerId, emsConnectionManager, emsMultiplexer, loginCountChecker,meterRegistry);
    }

    @Bean
    public TimedAspect timedAspect(MeterRegistry registry) {
        return new TimedAspect(registry);
    }

    @Bean
    FileReloadScheduler provideReloadScheduler() {
        return new FileReloadSchedulerImpl(new NoopFileCopier());
    }

    // TODO:  S3 storage, and a way to easily switch between tmp and S3 storage (env variable)
    @Bean EmsConnectionStorage provideEmsStorage() {
        return new TmpFileEmsConnectionStorage();
    }


    @Bean
    public TaskScheduler taskScheduler() {
        final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(5);
        return scheduler;
    }
}
