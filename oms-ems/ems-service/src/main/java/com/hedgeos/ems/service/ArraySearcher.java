package com.hedgeos.ems.service;

import java.time.LocalDate;
import java.util.List;

public class ArraySearcher {
    /** Go from the day of month down to 1, and then loop back around, looking for positions */
    public static <T> T findPriorDateValue(LocalDate tradeDate, List<T> arrayList) {
        int current = tradeDate.getDayOfMonth();
        // search from tradeDate --> 1
        for (int i = current -1; i>=1; i--) {
            T checker = arrayList.get(i);
            if (checker != null) {
                return checker;
            }
        }

        // search from 31 --> tradeDate
        for (int i = 31; i>= current; i--) {
            T checker = arrayList.get(i);
            if (checker != null) {
                return checker ;
            }
        }

        return null;
    }
}
