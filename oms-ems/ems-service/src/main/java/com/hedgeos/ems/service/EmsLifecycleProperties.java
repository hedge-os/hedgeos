package com.hedgeos.ems.service;

import org.springframework.boot.autoconfigure.context.LifecycleProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@ConfigurationProperties(prefix = "ems.lifecycle")
public class EmsLifecycleProperties extends LifecycleProperties {
    @Override
    public Duration getTimeoutPerShutdownPhase() {
        return Duration.ofSeconds(30);
    }

    @Override
    public void setTimeoutPerShutdownPhase(Duration timeoutPerShutdownPhase) {
        super.setTimeoutPerShutdownPhase(timeoutPerShutdownPhase);
    }
}
