package com.hedgeos.ems.service;

public class Base62Checker {
    public static boolean isPmGroupBase62Chars(String pmGroup) {
        for (int i=0; i<pmGroup.length(); i++) {
            char c = pmGroup.charAt(i);
            if (c < 48 || (c >57 && c<65) || (c > 90 && c < 97) || c > 122) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        boolean isCmr = Base62Checker.isPmGroupBase62Chars("cM9");
        boolean isFail = Base62Checker.isPmGroupBase62Chars("%^u");
        System.out.println("Cmr="+isCmr+", isFail="+isFail);
    }
}
