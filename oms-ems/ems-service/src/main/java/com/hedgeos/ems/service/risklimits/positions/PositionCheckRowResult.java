package com.hedgeos.ems.service.risklimits.positions;

import com.hedgeos.ems.csvfiles.risklimits.position.PositionLimitsFileRow;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class PositionCheckRowResult {

    final PositionLimitsFileRow row;
    final int orderSubmitStatus;
    final String message;

    final long limitLevel;
    final double currentPosition;
    final double necessaryPosition;

}
