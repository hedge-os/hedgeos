package com.hedgeos.ems.service.risklimits.trades.cleaners;

import com.hedgeos.ems.order.PmGroupSidKey;
import com.hedgeos.ems.service.risklimits.trades.TradeAmountTracker;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.Map;

/**
 * One of these runs for each AtomicIntegerArray in the "TradeBlock" class.
 * A function is attached which cleans the correct portion of the ring buffer,
 * given the current time, cleans the half of the ring in front of current time,
 * which represents half a period of the future.
 */
@Slf4j
@Getter
public class TradeBlockCleaner implements Runnable {

    private final Map<PmGroupSidKey, TradeAmountTracker.TradeActivityBlocks> tradingPerSecurity;
    private final OneArrayCleaner cleanerFunction;
    private final long loopRuntimeMs;

    private final Timer cleanTimer;

    public TradeBlockCleaner(String name,
                             long loopRuntimeMs,
                             Map<PmGroupSidKey, TradeAmountTracker.TradeActivityBlocks> tradingPerSecurity,
                             OneArrayCleaner cleaner, MeterRegistry meterRegistry)
    {
        this.tradingPerSecurity = tradingPerSecurity;
        this.cleanerFunction = cleaner;
        this.loopRuntimeMs = loopRuntimeMs;

        // TODO: Confirm if this timer is visible in the actuator
        cleanTimer =
                Timer.builder("limits.cleaner."+name)
                        .maximumExpectedValue(Duration.ofMillis(2L * loopRuntimeMs))
                        .sla(Duration.ofMillis(this.loopRuntimeMs))
                        .publishPercentileHistogram()
                        .percentilePrecision(2)
                        .tags("limits.cleaner", name)
                        .description("time for the " +name+" limit cleaner to clean per security limit entries")
                        .register(meterRegistry);
    }


    @Override
    public void run() {
        long count = 0;

        while (true) {

            try {
                // don't spin through this until there is any trading
                if (tradingPerSecurity.isEmpty()) {
                    Thread.sleep(10);
                    continue;
                }

                count++;

                long start = System.currentTimeMillis();
                // zero out half of the array referenced in cleanFunction
                // one of these needs to run for each AtomicIntegerArray
                for (TradeAmountTracker.TradeActivityBlocks blocks : tradingPerSecurity.values()) {
                    cleanerFunction.cleanFunction(blocks);
                }
                long end = System.currentTimeMillis();

                long tookMillis = end - start;

                cleanTimer.record(Duration.ofMillis(tookMillis));

                if ((count % (60 * 5)) == 0) {
                    // every five minutes, log an info about this
                    log.info("limits clean, size=" + tradingPerSecurity.size() +
                            ", took (ms)= [" + tookMillis + "] (must be < 900 ms.)");
                }

                // if we didn't finish in under 500ms, error message
                if (tookMillis > loopRuntimeMs) {
                    // This will never be hit; however,
                    // if you ever see this, trading could halt,
                    // as the thread clearing limits is not clearing all securities in time
                    log.error("Cleaning trading limits: STALE LIMITS - cleaning " +
                                    "took MORE THAN ALLOTTED time={}; took ms={}",
                            loopRuntimeMs, tookMillis);
                }

                // sleep for the remainder of a second
                // because, the window is two seconds; only need to clean half of it
                long maxSleep = Math.max(0, loopRuntimeMs - tookMillis);

                Thread.sleep(maxSleep); // sleep to the loopRuntime

            } catch (Error e) {
                log.error("Error cleaning", e); // keep going
            } catch (InterruptedException e) {
                log.error("Interrupted cleaner", e); // keep going
            }

        }
    }


}
