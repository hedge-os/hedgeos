package com.hedgeos.ems.service;

import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import order.*;

@Slf4j
public class ConnectionChecker {
    static boolean isValidConnection(Connection connection,
                                     StreamObserver responseObserver) {
        // null check for connectionId
        if (connection == null) {
            log.error("No connectionId on request.");
            // TODO:  Learn grpc Metadata
//            Metadata meta = new Metadata();
//            meta.put(new Metadata.Key<String>("msgKey", false), "No connectionId on request.");
            String errorMsg = "No CONNECTION on request.";
            responseObserver.onError(new StatusException(Status.ABORTED.withDescription(errorMsg)));
            return false;
        }

        // verify connectionId not set or garbage value
        // TODO:  Could verify the password here, and/or connectionId matches
        if (connection.connectionId() <= 0 ||
            connection.connectionId() > 61)
        {
            log.error("Invalid or unset connectionId" +
                    ", connectionId="+ connection.connectionId());
            String errorMsg = "ConnectionId <= 0 on request="+ connection.connectionId();
            responseObserver.onError(new StatusException(Status.ABORTED.withDescription(errorMsg)));
            return false;
        }

        return true;
    }

}
