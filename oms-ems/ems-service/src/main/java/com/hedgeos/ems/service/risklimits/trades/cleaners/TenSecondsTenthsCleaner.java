package com.hedgeos.ems.service.risklimits.trades.cleaners;

import com.hedgeos.ems.service.risklimits.trades.TradeAmountTracker;

public class TenSecondsTenthsCleaner implements OneArrayCleaner {

    @Override
    public void cleanFunction(TradeAmountTracker.TradeActivityBlocks blocks) {
        long now = System.currentTimeMillis();
        int currentIndex = TradeAmountTracker.tenSecondTenthsOffset(now);
        blocks.cleanHalfAheadOfNow(blocks.lastTenSecondsTenths, currentIndex);
    }
}
