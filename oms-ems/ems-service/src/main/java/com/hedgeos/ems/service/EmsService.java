package com.hedgeos.ems.service;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.order.LocateCheckResult;
import com.hedgeos.ems.service.connection.ConnectionAndReconnectCount;
import com.hedgeos.ems.service.connection.EmsConnectionManager;
import com.hedgeos.ems.service.exception.NoRemainingConnections;
import io.grpc.stub.ServerCallStreamObserver;
import io.grpc.stub.StreamObserver;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.hedgeos.ems.order.OrderIdConstants.ONE_BILLION_ORDERS_PER_RECONNECT_OFFSET;
import static com.hedgeos.ems.order.OrderIdConstants.ONE_TRILLION_CONNECTION_ID_OFFSET;

@RequiredArgsConstructor
@Slf4j
public class EmsService extends EmsServiceGrpc.EmsServiceImplBase {

    public static final int FAILED_CONNECTION_ID = Integer.MIN_VALUE;
    private static final int FAILED_RECONNECT_COUNT = Integer.MIN_VALUE;
    private static final int FAILED_EMS_SERVER_ID = Integer.MIN_VALUE;

    private final int emsServerId;
    private final EmsConnectionManager connectionManager;
    private final EmsMultiplexer multiplexer;
    private final LoginAndPasswordChecker loginAndPasswordChecker;

    private final MeterRegistry meterRegistry;


    @Override
    @Timed
    public void connect(ConnectRequest request, StreamObserver<Connection> responseObserver) {
        meterRegistry.counter("connect",
                "emsServerId",String.valueOf(emsServerId)).increment();

        int connectionStatusReason = loginAndPasswordChecker.verifyLogin(request);

        if (connectionStatusReason != ConnectionStatusReason.SUCCESS_REASON) {
            sendFailure(request, responseObserver, connectionStatusReason);
            return;
        }

        log.warn("Connect from login: {}", request.login());
        meterRegistry.counter("connect",
                "emsServerId", String.valueOf(emsServerId),
                "login", request.login()).increment();

        // each client can reconnect 3000 times, per day; more than enough.
        if (connectionManager.verifyReconnectCount(request) == false) {
            sendFailure(request, responseObserver, ConnectionStatusReason.TOO_MANY_RECONNECTS);
            // note; an alarm on this
            meterRegistry.counter("connect",
                    "emsServerId",String.valueOf(emsServerId),
                    "login", request.login(),
                    "status", "failed")
                    .increment();
            return;
        }

        try {
            // get the current or new connectionId and reconnectCount, and send it
            ConnectionAndReconnectCount counter =
                    connectionManager.getNextConnectionId(request);

            meterRegistry.counter("connect", "emsServerId", String.valueOf(emsServerId), "login", request.login(), "status", "success").increment();

            returnConnectionSuccess(responseObserver, counter);
        }
        catch (NoRemainingConnections e) {
            meterRegistry.counter("connect",
                    ""+emsServerId, "login", request.login(),
                    "status", "failed",
                    "reason", "no_remaining_connections")
                    .increment();

            // we have 62 connections per ems-service; if more are needed, start another ems-service
            sendFailure(request, responseObserver, ConnectionStatusReason.NO_CONNECTIONS_LEFT);
            return;
        }
    }

    private void returnConnectionSuccess(StreamObserver<Connection> responseObserver,
                                         ConnectionAndReconnectCount counter)
    {
        // build the response FlatBuffer
        FlatBufferBuilder builder = new FlatBufferBuilder();

        int offset =
                Connection.createConnection(builder, emsServerId,
                        counter.getConnectionId(),
                        counter.getReconnectCounter().get(),
                        ConnectionStatus.SUCCESS,
                        ConnectionStatusReason.SUCCESS_REASON);

        builder.finish(offset);
        Connection connection = Connection.getRootAsConnection(builder.dataBuffer());

        // push reponse to gRPC
        responseObserver.onNext(connection);
        responseObserver.onCompleted();
    }

    private static void sendFailure(ConnectRequest request,
                                    StreamObserver<Connection> responseObserver,
                                    int connectionStatusReason)
    {
        log.error("Failed login from: {}", request.login());
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int offset =
                Connection.createConnection(builder, FAILED_EMS_SERVER_ID, FAILED_CONNECTION_ID, FAILED_RECONNECT_COUNT,
                        ConnectionStatus.FAILED, connectionStatusReason);
        builder.finish(offset);
        Connection connection = Connection.getRootAsConnection(builder.dataBuffer());
        responseObserver.onNext(connection);
        responseObserver.onCompleted();
    }

    @Override
    @Timed
    public void subscribe(Connection connection, StreamObserver<EmsReport> responseObserver) {
        System.out.println("Subscribe from connectId: "+connection.connectionId());
        log.warn("Subscribe from connectionId: {}", connection.connectionId());

        // critical to clean up routing when clients disconnect from ems-service
        //noinspection rawtypes
        ServerCallStreamObserver serverCallStreamObserver = (ServerCallStreamObserver) responseObserver;
        serverCallStreamObserver.setOnCloseHandler(new Runnable() {
            @Override
            public void run() {
                // TODO:  Need to test this, proper disconnect from client
                log.warn("Close handler for observer, connectionId={}", connection.connectionId());
                multiplexer.removeSubscriber(connection, responseObserver);
            }
        });
        serverCallStreamObserver.setOnCancelHandler(new Runnable() {
            @Override
            public void run() {
                log.warn("Cancel handler for stream, connectionId={}", connection.connectionId());
                multiplexer.removeSubscriber(connection, responseObserver);
            }
        });

        multiplexer.connectionMap.putIfAbsent(connection.connectionId(), new CopyOnWriteArrayList<>());
        List<StreamObserver<EmsReport>> streamObserverList = multiplexer.connectionMap.get(connection.connectionId());
        log.warn("Total connections for id="+connection.connectionId()+", count="+streamObserverList.size());
        streamObserverList.add(responseObserver);
    }

    @Override
    @Timed
    public void submitOrders(OrderRequest request,
                             StreamObserver<OrderResponse> responseObserver) {

        if (false == ConnectionChecker.isValidConnection(request.connection(), responseObserver)) {
            log.error("No order connection, requestId={}", request.requestId());
            return;
        }

        log.debug("Submit orders called, size={}", request.ordersLength());

        // submit each order and return a response
        try {
            for (int i = 0; i < request.ordersLength(); i++) {
                Order order = request.orders(i);
                increaseReconnectOffsetPerHundredBillionOrderIds(order.hedgeOsOrderId(), request.connection());
                OrderResponse status = multiplexer.submitOrder(order, request.connection());
                responseObserver.onNext(status);
            }
            responseObserver.onCompleted();
        }
        catch (Exception e) {
            log.error("Exception on submitOrders, requestId={}", request.requestId(), e);
            responseObserver.onError(e);
        }
    }

    @Override
    @Timed
    public void cancelOrders(CancelRequest request, StreamObserver<CancelResponse> responseObserver) {

        if (false == ConnectionChecker.isValidConnection(request.connection(), responseObserver)) {
            log.error("Missing connection on cancel orders!, requestId={}", request.requestId());
            return;
        }

        log.debug("Cancel orders called, size={}", request.cancelsLength());

        // submit each cancel and return a response
        try {
            for (int i = 0; i < request.cancelsLength(); i++) {
                Cancel cancel = request.cancels(i);
                increaseReconnectOffsetPerHundredBillionOrderIds(cancel.hedgeOsOrderId(), request.connection());
                CancelResponse status = multiplexer.cancelOrder(cancel, request.connection());
                responseObserver.onNext(status);
            }
            responseObserver.onCompleted();
        }
        catch (Exception e) {
            log.error("Exception on cancelOrders, requestId={}", request.requestId(), e);
            responseObserver.onError(e);
        }
    }

    @Override
    @Timed
    public void cancelReplaceOrders(CancelReplaceRequest request, StreamObserver<CancelReplaceResponse> responseObserver) {

        if (false == ConnectionChecker.isValidConnection(request.connection(), responseObserver)) {
            log.error("Missing connection on REPLACE orders! requestId={}", request.requestId());
            return;
        }

        log.debug("REPLACE orders called, size={}", request.cancelReplacesLength());

        // submit each replace and stream responses
        try {
            for (int i = 0; i < request.cancelReplacesLength(); i++) {
                CancelReplace replace = request.cancelReplaces(i);
                increaseReconnectOffsetPerHundredBillionOrderIds(replace.hedgeOsOrderId(), request.connection());
                CancelReplaceResponse status = multiplexer.cancelReplace(replace, request.connection());
                responseObserver.onNext(status);
            }
            responseObserver.onCompleted();
        }
        catch (Exception e) {
            log.error("Exception on cancelReplace, requestId={}", request.requestId(), e);
            responseObserver.onError(e);
        }
    }


    @Override
    public void limitVerifyPreTrade(OrderRequest request, StreamObserver<LimitVerifyResponse> responseObserver) {
        if (false == ConnectionChecker.isValidConnection(request.connection(), responseObserver)) {
            log.error("Missing connection on LIMIT VERIFY orders! requestId={}", request.requestId());
            return;
        }

        log.debug("LIMIT VERIFY orders called, size={}", request.ordersLength());

        // submit each replace and stream responses
        try {
            FlatBufferBuilder builder = new FlatBufferBuilder();

            int[] responseVector = new int[request.ordersLength()];

            InternalLimitVerifyResult[] results = new InternalLimitVerifyResult[request.ordersLength()];

            int overallResult = OrderSubmitStatus.SUCCESSFUL_ORDER;

            for (int i = 0; i < request.ordersLength(); i++) {
                Order order = request.orders(i);
                increaseReconnectOffsetPerHundredBillionOrderIds(order.hedgeOsOrderId(), request.connection());
                InternalLimitVerifyResult result = multiplexer.limitVerifyOrder(order, request.connection());
                results[i] = result;
                // if any data integrity checks fail (symbol mapping or venue mapping)
                // make these the overall result
                if (result.status > 1) {
                    // this means we couldn't perform status checks;
                    // symbol or venue lookup or other serious issue with the order
                    overallResult = result.status;
                }

                int orderResponseOffset =
                        ResponseFactory.createLimitVerifyResult(builder, order,
                                result.tradingResult, result.positionCheckRowResult);

                responseVector[i] = orderResponseOffset;
            }

            // if the result is still unset, use any non-success result
            if (overallResult == OrderSubmitStatus.SUCCESSFUL_ORDER) {
                for (InternalLimitVerifyResult result : results) {
                    if (result.tradingResult.getOrderSubmitStatus() > 1) {
                        overallResult = result.tradingResult.getOrderSubmitStatus();
                        break;
                    }
                    if (result.positionCheckRowResult.getOrderSubmitStatus() > 1) {
                        overallResult = result.positionCheckRowResult.getOrderSubmitStatus();
                        break;
                    }
                }
            }

            int vectorOffset = LimitVerifyResponse.createLimitVerifyResponsesVector(builder, responseVector);
            int offset = LimitVerifyResponse.createLimitVerifyResponse(builder, overallResult, vectorOffset);
            builder.finish(offset);
            LimitVerifyResponse response = LimitVerifyResponse.getRootAsLimitVerifyResponse(builder.dataBuffer());

            // 1 response with the sub-responses
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
        catch (Exception e) {
            log.error("Exception on LIMIT VERIFY, requestId={}", request.requestId(), e);
            responseObserver.onError(e);
        }
    }

    @Override
    public void locatesVerifyPreTrade(OrderRequest request, StreamObserver<LocateVerifyResponse> responseObserver) {
        if (false == ConnectionChecker.isValidConnection(request.connection(), responseObserver)) {
            log.error("Missing connection on VERIFY LOCATES orders! requestId={}", request.requestId());
            return;
        }

        log.debug("LOCATES VERIFY orders called, size={}", request.ordersLength());

        // submit each replace and stream responses
        try {
            // build one response across all orders
            FlatBufferBuilder builder = new FlatBufferBuilder();

            int[] responseVector = new int[request.ordersLength()];

            LocateCheckResult[] locateCheckResults = new LocateCheckResult[request.ordersLength()];
            for (int i = 0; i < request.ordersLength(); i++) {
                // standard bump necessary on orderIds for connection keeping
                Order order = request.orders(i);
                increaseReconnectOffsetPerHundredBillionOrderIds(order.hedgeOsOrderId(), request.connection());

                // get the result from the system
                LocateCheckResult locateCheckResult = multiplexer.locateVerifyOrder(order);
                locateCheckResults[i] = locateCheckResult;
            }

            int overallResult = OrderSubmitStatus.SUCCESSFUL_ORDER;
            // map the results to the protobuf API
            for (int i=0; i<request.ordersLength(); i++) {
                Order order = request.orders(i);
                LocateCheckResult result = locateCheckResults[i];
                if (result.orderStatusCode != OrderSubmitStatus.SUCCESSFUL_ORDER) {
                    overallResult = OrderSubmitStatus.LOCATE_VERIFY_FAILURE;
                }
                responseVector[i] = LocatesVerifyMessageFactory.createLocateVerifyOrderResponse(builder, order, result);
            }

            int vectorOffset =
                    LocateVerifyResponse.createResponsesVector(builder, responseVector);
            int offset = LocateVerifyResponse.createLocateVerifyResponse(builder, overallResult, vectorOffset);
            builder.finish(offset);
            LocateVerifyResponse response =
                    LocateVerifyResponse.getRootAsLocateVerifyResponse(builder.dataBuffer());

            // 1 response with the sub-responses
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
        catch (Exception e) {
            log.error("Exception on LOCATE verify, requestId={}", request.requestId(), e);
            responseObserver.onError(e);
        }
    }

    /**
     * See design docs on reconnection and unique id generation for hedgeOsOrderID.
     * The reconnectCount on the connection is used to calculate an offset of a billion on the counter.
     * If the client happens to send a billion orders, we can throw an exception; or instead,
     * make an artificial increase to the reconnectCount (which this handles.)
     *
     * @param hedgeOsOrderId current order id passed
     * @param connection current connection from client
     */
    private void increaseReconnectOffsetPerHundredBillionOrderIds(long hedgeOsOrderId, Connection connection) {
        // can remove after excessive unit testing, also good for speed
        long strippedTrillions = hedgeOsOrderId % ONE_TRILLION_CONNECTION_ID_OFFSET;
        // no test until we hit one billion orders, then a *tiny* speed bump..
        if (strippedTrillions >= ONE_BILLION_ORDERS_PER_RECONNECT_OFFSET) {
            connectionManager.checkOrIncreaseReconnectCountOffset(hedgeOsOrderId, connection);
        }
    }
}
