package com.hedgeos.ems.service;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.order.LocateCheckResult;
import order.*;

public class LocatesVerifyMessageFactory {
    static int createNoLocatesUsedOffset(FlatBufferBuilder builder, Order order) {
        String message = "Buy does not use locates";
        int messageOffset = builder.createString(message);
        LocateVerifyOrderResponse.startLocateVerifyOrderResponse(builder);
        LocateVerifyOrderResponse.addMessage(builder, messageOffset);
        LocateVerifyOrderResponse.addHedgeOsOrderId(builder, order.hedgeOsOrderId());
        LocateVerifyOrderResponse.addStatus(builder, OrderSubmitStatus.SUCCESSFUL_ORDER);
        //noinspection UnnecessaryLocalVariable
        int offset = LocateVerifyOrderResponse.endLocateVerifyOrderResponse(builder);
        return offset;
    }

    static int createLocateVerifyOrderResponse(FlatBufferBuilder builder, Order order, LocateCheckResult locateCheckResult) {
        int locateRowOffset = createLocateRow(builder, locateCheckResult);

        int messageOffset = 0;
        if (locateCheckResult.getMessage() != null)
            messageOffset = builder.createString(locateCheckResult.getMessage());

        LocateVerifyOrderResponse.startLocateVerifyOrderResponse(builder);
        if (messageOffset != 0)
            LocateVerifyOrderResponse.addMessage(builder, messageOffset);

        LocateVerifyOrderResponse.addHedgeOsOrderId(builder, order.hedgeOsOrderId());
        LocateVerifyOrderResponse.addStatus(builder, locateCheckResult.getOrderStatusCode());
        LocateVerifyOrderResponse.addIsLocateRequested(builder, locateCheckResult.isOrderUsingLocates);
        LocateVerifyOrderResponse.addTotalLocates(builder, locateCheckResult.getTotalLocates());
        LocateVerifyOrderResponse.addCurrentPosition(builder, locateCheckResult.getCurrentPosition());
        LocateVerifyOrderResponse.addOrderQty(builder, locateCheckResult.getOrderQty());
        if (locateRowOffset != 0) {
            LocateVerifyOrderResponse.addLocateFileRow(builder, locateRowOffset);
        }
        int offset = LocateVerifyOrderResponse.endLocateVerifyOrderResponse(builder);
        return offset;
    }

    private static int createLocateRow(FlatBufferBuilder builder, LocateCheckResult locateCheckResult) {
        int locateRowOffset = 0;
        if (locateCheckResult.getRow() != null) {
            int pmGroupOffset = 0;
            if (locateCheckResult.getRow().getPmGroup() != null) {
                pmGroupOffset = builder.createString(locateCheckResult.getRow().getPmGroup());
            }

            int fileNameOffsetRow = 0;
            if (locateCheckResult.getRow().getFile() != null) {
                fileNameOffsetRow = builder.createString(locateCheckResult.getRow().getFile().getName());
            }

            int venueOffset = 0;
            if (locateCheckResult.getRow().getVenue() != null) {
                int venueNameOffset = 0;
                if (locateCheckResult.getRow().getVenue().getName() != null) {
                   venueNameOffset = builder.createString(locateCheckResult.getRow().getVenue().getName());
                } else {
                    venueOffset = builder.createString("UNSET_VENUE_NAME");
                }

                ExecutionVenue.startExecutionVenue(builder);
                ExecutionVenue.addId(builder, locateCheckResult.getRow().getVenue().getId());
                ExecutionVenue.addName(builder, venueNameOffset);
                venueOffset = ExecutionVenue.endExecutionVenue(builder);
            }

            LocateFileRow.startLocateFileRow(builder);
            LocateFileRow.addLocates(builder, locateCheckResult.getRow().getLocates());
            LocateFileRow.addRowNumber(builder, locateCheckResult.getRow().getRowNumber());
            LocateFileRow.addSid(builder, locateCheckResult.getRow().getSid());
            if (venueOffset != 0) {
                LocateFileRow.addVenue(builder, venueOffset);
            }
            if (pmGroupOffset != 0) {
                LocateFileRow.addPmGroup(builder, pmGroupOffset);
            }
            if (fileNameOffsetRow != 0) {
                LocateFileRow.addFileName(builder, fileNameOffsetRow);
            }

            locateRowOffset = LocateFileRow.endLocateFileRow(builder);
        }
        return locateRowOffset;
    }
}
