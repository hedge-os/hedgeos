package com.hedgeos.ems.service;

import com.hedgeos.ems.util.WorkstationDevelopmentConstants;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@Slf4j
@Getter
public class EmsServerIdManager {

    // env variable, property, or emsServerId.number file must be set in working directory of EMS
    public static final String EMS_SERVER_ID = "EMS_SERVER_ID";
    public static final String PATH_TO_EMS_SERVER_ID_NUMBER_FILE = "./emsServerId.number";
    public static final String PATH_TO_EMS_SERVER_ID = "PATH_TO_EMS_SERVER_ID";

    // this must be globally unique for each ems-service which is run
    private final int emsServerId;

    public EmsServerIdManager() {
        // TODO:  Get emsServerID from a central vending service
        emsServerId = locateEmsServerIdInFileOrEnvVariable();
    }

    private int locateEmsServerIdInFileOrEnvVariable() {
        String path = PATH_TO_EMS_SERVER_ID_NUMBER_FILE;
        // default to the working directory of the file, allow to look elsewhere
        if (System.getProperty(PATH_TO_EMS_SERVER_ID) != null) {
            path = System.getProperty(PATH_TO_EMS_SERVER_ID);
            log.warn("Looking for EMS Server Id file emsServerId.number at path="+path);
        }

        // first look in the file
        File emsServerIdFile = new File(path);
        int found = lookForEmsServerIdInServerFile(emsServerIdFile);

        if (found != Integer.MIN_VALUE) {
            log.warn("Set emsServerId from emsServerId.number file.  Value="+emsServerId);
            return found;
        }

        // 2nd, look in the environment variables
        String emsServerIdFromEnv = System.getenv(EMS_SERVER_ID);
        if (emsServerIdFromEnv != null && emsServerIdFromEnv.isEmpty() == false) {
            return Integer.parseInt(emsServerIdFromEnv);
        }

        // 3rd, look for a dash D command line property
        String fromProp = System.getProperty(EMS_SERVER_ID);
        if (fromProp != null && fromProp.isEmpty() == false)
        {
            return Integer.parseInt(fromProp);
        }

        // 4th, if a developer workstation, load what we find locally
        String sourceTreeRelativeLocation = WorkstationDevelopmentConstants.DEVELOPER_EMS_SERVER_ID_NUMBER;
        File devWorkstationFile = new File(sourceTreeRelativeLocation);
        if (devWorkstationFile.exists()) {
            log.warn("Dev workstation file found, loading emsServerId from: "+sourceTreeRelativeLocation);
            int value = lookForEmsServerIdInServerFile(devWorkstationFile);
            return value;
        }

        // 5th, try another relative path for the developer workstation file
        String secondSourceTreeLocation = WorkstationDevelopmentConstants.ALT_DEVELOPER_EMS_SERVER_ID_NUMBER;
        File devWorkstationFile2 = new File(secondSourceTreeLocation);
        if (devWorkstationFile2.exists()) {
            log.warn("Dev workstation 2 file found, loading emsServerId from: "+secondSourceTreeLocation);
            int value = lookForEmsServerIdInServerFile(devWorkstationFile2);
            return value;
        }

        // finally, explode; we can't run without an EMS-server ID
        String msg = "Must set env variable or property: "+EMS_SERVER_ID+" or provide emsServerId.number file.";
        log.error(msg);
        throw new Error(msg);
    }

    private static int lookForEmsServerIdInServerFile(File emsServerIdFile) {
        try {

            if (emsServerIdFile.exists()) {
                String content = new String(Files.readAllBytes(Paths.get(emsServerIdFile.getAbsolutePath())));
                // remove any new lines
                String[] split = content.split(System.lineSeparator());
                for (String line : split) {
                    if (line.trim().startsWith("#") || line.trim().startsWith("//")) {
                        log.warn("Skipping comment: " + line.trim());
                        continue;
                    }

                    if (line.trim().isEmpty()) {
                        log.warn("Skipping blank line");
                    }

                    // allow key=value for the serverId
                    if (line.trim().contains("=")) {
                        String[] sides = line.trim().split("=");
                        String serverId = sides[1];
                        log.warn("Found emsServerid=" + serverId + ", in line=" + line.trim());
                        return Integer.parseInt(serverId);
                    }

                    try {
                        Integer emsServerId = Integer.parseInt(line.trim());
                        return emsServerId;
                    } catch (Exception e) {
                        // we tried to set it, but the file is malformed
                        String msg = "Serious error parsing emsServerId.number from root folder=" + emsServerIdFile.getAbsolutePath();
                        System.out.println(msg);
                        log.error(msg, e);
                        // indeed, this is an error; we can't start
                        throw new Error(e);
                    }
                }
            }

            return Integer.MIN_VALUE;
        } catch (Exception e) {
            String msg = "Exception reading emsServerId.number file: "+e;
            log.error(msg);
            throw new Error(msg);
        }
    }

}
