package com.hedgeos.ems.service.risklimits.trades;

import com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitLoad;
import com.hedgeos.ems.service.ArraySearcher;
import com.hedgeos.ems.service.EmsEnvironmentManager;
import com.hedgeos.ems.order.PmGroupSidKey;
import general.Quantity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import order.OrderSubmitStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
@RequiredArgsConstructor
@Slf4j
public class TradeLimitsManager {

    public final EmsEnvironmentManager emsEnvironmentManager;

    public final TradeAmountTracker tradeAmountTracker;

    public static final int HOLD_31_DAYS_SIZE = 32;

    // a ReentrantReadWriteLock, set of them in an array, might be better, as COWAL has a volatile array
    // however; once it is cached in memory, and not written... TODO: check volatile memory semantics
    private final CopyOnWriteArrayList<TradeLimitChecker> tradeLimitsPerDayOfMonth =
            new CopyOnWriteArrayList<>(new TradeLimitChecker[HOLD_31_DAYS_SIZE]);

    public void setTradeLimits(TradeLimitLoad tradeLimits) {
        int dayOfMonth = tradeLimits.getFileDate().getDayOfMonth();
        TradeLimitChecker tradeLimitChecker = new TradeLimitChecker(tradeAmountTracker, tradeLimits);
        // written very rarely, replace the entire array.
        this.tradeLimitsPerDayOfMonth.set(dayOfMonth, tradeLimitChecker);
    }

    public TradingLimitCheckRowResult checkTradingLimit(boolean isPreCheck,
                                                        PmGroupSidKey key,
                                                        int side,
                                                        Quantity quantity,
                                                        long utcOrderSubmitTimeMillis,
                                                        LocalDate orderTradeDate, long hedgeOsOrderId) {

        TradeLimitChecker checker = getMostRecentChecker(orderTradeDate);

        // we couldn't find any viable limits, even with stale
        if (checker == null && emsEnvironmentManager.isStaleLimitsEnabled()) {
            log.error("No viable stale Trade LIMITS for tradeDate={}, for replaceId={}",
                    orderTradeDate, hedgeOsOrderId);
        }

        // stale limits not enabled, we did not find limits for the given trade date
        if (checker == null) {
            return new TradingLimitCheckRowResult(null,
                    "no limits set for date="+orderTradeDate,
                    OrderSubmitStatus.MISSING_TRADE_LIMITS_FOR_TRADE_DATE,
                    0L, 0.0, 0.0);
        }

        // if these limits are in use, need to check them
        // look for a specific limit in the file

        TradingLimitCheckRowResult result =
                checker.checkTradeLimit(isPreCheck, utcOrderSubmitTimeMillis,side,
                quantity, hedgeOsOrderId, key);

        return result;
    }


    public void recordNewTradePlacedInTracker(long timestampMillis,
                                              PmGroupSidKey key,
                                              long hedgeOsOrderId,
                                              LocalDate orderTradeDate,
                                              double signedOrderQty)
    {
        int signedOrderQtyInt = (int) signedOrderQty;
        TradeLimitChecker checker = getMostRecentChecker(orderTradeDate);
        if (checker == null) {
            // never happens; check trading limits logic finds one
            String errorString =
                    String.format("No trade limit checker for trade date=%s, hedgeOsOrderId=%d",
                    orderTradeDate,hedgeOsOrderId);
            log.error(errorString);
            throw new Error(errorString);
        }

        checker.recordNewTradePlacedInTracker(timestampMillis, key.pmGroup, key.sid, signedOrderQtyInt);
    }
    
    private TradeLimitChecker getMostRecentChecker(LocalDate orderTradeDate) {
        // get the trade checker for the right date
        TradeLimitChecker checker = tradeLimitsPerDayOfMonth.get(orderTradeDate.getDayOfMonth());

        if (checker != null)
            return checker;

        if (false == emsEnvironmentManager.isStaleLimitsEnabled())
        {
            log.error("Missing TRADE LIMITS for tradeDate={}", orderTradeDate);
            return null;
        }

        checker = ArraySearcher.findPriorDateValue(orderTradeDate, tradeLimitsPerDayOfMonth);
        return checker;
    }
}
