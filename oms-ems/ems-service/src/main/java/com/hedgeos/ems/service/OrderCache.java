package com.hedgeos.ems.service;

import com.hedgeos.ems.order.OrderChain;
import com.hedgeos.ems.util.TradingDayUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class OrderCache {

    // TODO:  Need to rebuild this on recovery, to handle cancels.

    public static final int THREE_DAY_BUFFER = 3;
    // TODO:  hedgeOsOrderIds are not forever; add a tradeDate

    // TODO:  Use the ring buffer again..
    final Map<LocalDate, Map<Long, OrderChain>> dateToOrderMap = new ConcurrentHashMap<>();

    final Map<Long, Boolean> limitsReturnedPerOrder = new ConcurrentHashMap<>();

    public void addOrder(long hedgeOsOrderId, OrderChain wrapper) {
        this.dateToOrderMap.putIfAbsent(wrapper.getOrderTradeDate(), new ConcurrentHashMap<>());
        this.dateToOrderMap.get(wrapper.getOrderTradeDate()).put(hedgeOsOrderId, wrapper);
    }


    public void tryDeleteOrder(long hedgeOsOrderId, OrderChain chain) {
        if (this.dateToOrderMap.get(chain.getOrderTradeDate()) == null) {
            // if locates fails, the order never makes it to the cache
            return;
        }

        this.dateToOrderMap.get(chain.getOrderTradeDate()).remove(hedgeOsOrderId);
    }


    public OrderChain getOrder(LocalDate orderTradeDate, long hedgeOsOrderId) {
        Map<Long, OrderChain> ordersForDate = dateToOrderMap.get(orderTradeDate);
        if (ordersForDate == null) {
            return null;
        }

        return ordersForDate.get(hedgeOsOrderId);
    }

    @Scheduled(fixedRate = 1000*60*60*24)
    public void cleanOrderMap() {
        LocalDate tradeDate = TradingDayUtil.getTradeDate();
        LocalDate oldDate = tradeDate.minusDays(THREE_DAY_BUFFER);
        Set<LocalDate> toRemove = new HashSet<>();
        for (LocalDate localDate : dateToOrderMap.keySet()) {
            if (localDate.isBefore(oldDate)) {
                toRemove.add(localDate);
            }
        }

        for (LocalDate localDate : toRemove) {
            dateToOrderMap.remove(localDate);
        }
    }

    public boolean getAndSetLimitsReturned(long hedgeOsOrderId) {
        Boolean previouslySet = limitsReturnedPerOrder.putIfAbsent(hedgeOsOrderId, true);
        return previouslySet != null && previouslySet == true;
    }

}
