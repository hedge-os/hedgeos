package com.hedgeos.ems.service.locates;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class LocateKey {
    private final int venueId;
    private final long sid;

    private final String pmGroup;
}
