package com.hedgeos.ems.loader;

public interface FileCopier {
    void copyFilesIntoPlace();
}
