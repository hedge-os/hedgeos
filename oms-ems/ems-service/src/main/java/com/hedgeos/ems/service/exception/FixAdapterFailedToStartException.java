package com.hedgeos.ems.service.exception;

import org.springframework.boot.ExitCodeGenerator;

/**
 * Critical:  This is thrown when a single FixAdapter fails to start, and we stop the EMS.
 * This way, the system stops, and operations is aware.
 *
 * This can be turned off with configuration:
 *  STOP_ON_CONNECTION_LOAD_FAILURE
 *
 *  But it should not be turned off, particularly not in production.
 */
public class FixAdapterFailedToStartException extends RuntimeException implements ExitCodeGenerator {

    public FixAdapterFailedToStartException(String msg, Exception e) {
        super(msg, e);
    }

    @Override
    public int getExitCode() {
        return -99;
    }
}