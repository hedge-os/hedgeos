package com.hedgeos.ems.service.connection;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;


// TODO:  We need to periodically delete the connection storage; could be Saturday
// TODO:  Otherwise, if they reconnect every day, it keeps going up by a hundred billion
// TODO:  If the date rolls, and then they reconnect; e.g. stock trading, should not increment the Order IDs
@Slf4j
public class TmpFileEmsConnectionStorage implements EmsConnectionStorage {

    public static final String EMS_FILE_EXTENSION = ".ems";
    public static final String EMS_FILE_PREFIX = "emsServerId_";
    public static final String LOGIN_KEY = "login";
    public static final String CONNECTION_ID_KEY = "connectionId";
    public static final String RECONNECT_COUNT_KEY = "reconnectCount";
    private static final String CONNECT_TIME_UTC_MILLIS = "connectionTimeUtcMillis";

    private static String getFilenamePerLogin(int emsServerId, ConnectionAndReconnectCount connectionInfo) {
        String tmpFolder = System.getProperty("java.io.tmpdir");
        return tmpFolder + File.separator + EMS_FILE_PREFIX + emsServerId + "_login_" + connectionInfo.getLogin() + EMS_FILE_EXTENSION;
    }

    @Override
    public void writeConnectionCountFile(int emsServerId, ConnectionAndReconnectCount connectionInfo) {
        File perLoginCounter = new File(getFilenamePerLogin(emsServerId, connectionInfo));
        if (perLoginCounter.exists()) {
            log.error("per login file already exists for login at="+perLoginCounter.getAbsolutePath());
            updateConnectionCountFile(emsServerId, connectionInfo);
        }

        storeNewFile(connectionInfo, perLoginCounter);
    }


    @Override
    public void updateConnectionCountFile(int emsServerId, ConnectionAndReconnectCount connectionInfo) {
        File perLoginCounter = new File(getFilenamePerLogin(emsServerId, connectionInfo));
        storeNewFile(connectionInfo, perLoginCounter);
    }


    @SneakyThrows
    private static void storeNewFile(ConnectionAndReconnectCount connectionInfo, File perLoginCounter) {
        Exception lastException = null;
        int backoffSeconds = 5;
        // Because this is critical; backoff and try again three times if it fails
        // This happens at connection time, it need not be immediate
        for (int i=0; i<3; i++) {
            Properties properties = new Properties();
            properties.setProperty(LOGIN_KEY, connectionInfo.getLogin());
            properties.setProperty(CONNECT_TIME_UTC_MILLIS, String.valueOf(connectionInfo.getConnectionTimeUtcMillis()));
            properties.setProperty(CONNECTION_ID_KEY, String.valueOf(connectionInfo.getConnectionId()));
            properties.setProperty(RECONNECT_COUNT_KEY, String.valueOf(connectionInfo.getReconnectCounter()));
            try (FileOutputStream out = new FileOutputStream(perLoginCounter)) {
                properties.store(out, null);
                // success, return
                return;

            } catch (Exception e) {
                log.error("Exception writing file=" + perLoginCounter.getAbsolutePath(), e);
                lastException = e;
                Thread.sleep(backoffSeconds);
                backoffSeconds += 5;
            }
        }

        log.error("Exception writing file after multiple tries with backoff="+perLoginCounter.getAbsolutePath(), lastException);
        throw new RuntimeException(lastException);
    }

    @Override
    public List<ConnectionAndReconnectCount> loadConnectionInfo(int emsServerId) {
        String tmpFolder = System.getProperty("java.io.tmpdir");
        File tmpDir = new File(tmpFolder);
        String prefixWildcardSuffix = EMS_FILE_PREFIX + "*" + EMS_FILE_EXTENSION;
        FileFilter fileFilter = WildcardFileFilter.builder().setWildcards(prefixWildcardSuffix).get();
        File[] files = tmpDir.listFiles(fileFilter);
        List<ConnectionAndReconnectCount> priorConnections = new ArrayList<>();
        if (files == null || files.length == 0) {
            log.warn("Zero connection recover files, returning.");
            return priorConnections;
        }

        for (File file : files) {
            Properties properties = new Properties();
            try {
                properties.load(new FileInputStream(file));
                String login = properties.getProperty(LOGIN_KEY);
                long connectTimeUtcMillis = Long.parseLong(properties.getProperty(CONNECT_TIME_UTC_MILLIS));
                int connectionId = Integer.parseInt(properties.getProperty(CONNECTION_ID_KEY));
                int reconnectCount = Integer.parseInt(properties.getProperty(RECONNECT_COUNT_KEY));
                ConnectionAndReconnectCount carc =
                        new ConnectionAndReconnectCount(
                                login,
                                connectTimeUtcMillis,
                                connectionId,
                                new AtomicInteger(reconnectCount));
                priorConnections.add(carc);

            } catch (IOException e) {
                // TODO:  Perhaps if the file is 24 hours old, can ignore this.
                String msg = "IO exception reading file=" + file;
                log.error(msg, e);
                throw new RuntimeException(msg, e);
            }
        }
        return priorConnections;
    }

}
