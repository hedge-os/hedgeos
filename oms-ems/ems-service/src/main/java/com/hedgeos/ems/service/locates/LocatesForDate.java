package com.hedgeos.ems.service.locates;

import com.hedgeos.ems.order.*;
import com.hedgeos.ems.csvfiles.locates.LocatesMapAndLatestTimestamp;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Provides a per date, per ExecutionVenue in memory cache of locates provided by banks, loaded from flat files.
 **/
@Slf4j
public class LocatesForDate {

    public final LocatesMapAndLatestTimestamp originalLoad;
    private final Map<PmGroupSidKey, Map<EmsExecutionVenue, LocatesFileRow>> originalLocatesByVenue = new ConcurrentHashMap<>();

    private final Map<LocateKey, LocatesFileRow> locatesByKeyAndVenue = new ConcurrentHashMap<>();
    private final Set<Integer> venuesWithLocates = new ConcurrentSkipListSet<>();

    public LocatesForDate(LocatesMapAndLatestTimestamp locates) {
        this.originalLoad = locates;
        for (LocatesFileRow locateRow : locates.getLocatesFileRowMap().values()) {
            LocateKey locateKey = new LocateKey(locateRow.getVenue().getId(), locateRow.getSid(), locateRow.getPmGroup());
            // map it up for reloading if necessary on the fly, when positions are reloaded
            locatesByKeyAndVenue.put(locateKey, locateRow);
            venuesWithLocates.add(locateRow.getVenue().getId());
        }
    }

    public boolean isBefore(LocalDate thirtyDaysPrior) {
        return originalLoad.getFileDate().isBefore(thirtyDaysPrior);
    }

    public LocalDate getFileDate() {
        return originalLoad.getFileDate();
    }

    public LocatesFileRow getByKeyAndVenue(PmGroupSidKey key, EmsExecutionVenue venue) {
        return locatesByKeyAndVenue.get(new LocateKey(venue.getId(), key.sid, key.pmGroup));
    }

    public boolean locatesExistForVenue(EmsExecutionVenue venue) {
        return venuesWithLocates.contains(venue.getId());
    }

}
