package com.hedgeos.ems.loader;

public interface FileReloadScheduler {
    void addReloads(Runnable allReloads);

    void scheduledRun();
}
