
# Checks WE are currently doing

1. Compliance
    2. Expiration Dates
    3. Name (Security)  Classification Reg M
    4. Restiction Actived (Yes / No)
    5. Buy / Sell / Buy to Cover / Sell Short
        6. Buy Groups
        7. One Desk or One Group
        8. Not allowed to sell short for 5 days.
9. Tax restrictions
    10. PTP rule
        11. Large Tax if you trade a partnership on Cash
        12. List imported from Bloomberg
13. Moving to trading with Bloomberg, EMSX 
    14. Calls to EMSX
14. Agg Units - can be NR1 - any group in NR1 contributes to long and short
15. Locates - call to a vendor called Enzo(?)
16. Execution Desk checks vs. Out to the Street orders
    17. The parent orders don't count for anything
    18. They have not left the firm, they get marked as child orders
    19. But do we tell the PM that it is restricted
        20. Up front, let them know before they realize it is restricted.
21. Basket Trading PMS
    22. They want to send the entire basket, or not at all
        23. Check things up front.
24. No one cares about the Account Limits - the PM limits are what matters
    25. The account level limits get in the way, per Don
    26. Could make it flexible; have a "deskCode", for limit for the desk.
        27. Would need to add this to the position and trading limits.

