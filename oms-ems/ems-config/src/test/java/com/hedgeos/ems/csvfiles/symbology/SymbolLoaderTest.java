package com.hedgeos.ems.csvfiles.symbology;

import com.hedgeos.ems.csvfiles.util.MissingAnyFilesError;
import com.hedgeos.ems.util.TradingDayUtil;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.LocalDate;
import java.util.Map;

import static com.hedgeos.ems.csvfiles.symbology.SymbolLoaderFileLocationUtil.EMS_SYMBOL_FOLDER;
import static com.hedgeos.ems.csvfiles.util.FlatFileUtil.findMostRecentFile;
import static org.junit.jupiter.api.Assertions.*;

public class SymbolLoaderTest {

    public static final String RELATIVE_PATH_TO_CONFIG_DIR = "../ems-config-mock-files";

    @Test
    public void testWithEmsConfigFolderUsed() {
        testSymbolLoading(new File(RELATIVE_PATH_TO_CONFIG_DIR));
    }


    public void testSymbolLoading(File emsConfigFolder) {

        SymbolLoader symbolLoader = new SymbolLoader(emsConfigFolder);
        File currentFile = findMostRecentFile(TradingDayUtil.getTradeDate(),
                symbolLoader.getSymbologyFolder(), "bloomberg");
        SymbolMapAndLatestTimestamp mapAndLatestTimestamp = symbolLoader.loadSymbols(currentFile, true);
        Map<Long, SymbolFileRow> sidMap = mapAndLatestTimestamp.symbolMap;
        SymbolFileRow usdJpy = sidMap.get(3L);
        assertNotNull(usdJpy, "No USDJPY symbol loaded");
        // test that the overlay worked
        assertEquals("USD/JPY FX", usdJpy.symbol);
        // test that priority worked and non-overlay
        assertEquals(5252, sidMap.size());
        SymbolFileRow spy = sidMap.get(2L);
        assertEquals(1, spy.getPriority(), "failed to load priority field");
    }

    @Test
    public void testErrorOnNoFiles() {
        SymbolLoader symbolLoader = new SymbolLoader(new File(RELATIVE_PATH_TO_CONFIG_DIR));
        try {
            File missingFile = new File(symbolLoader.symbologyFolder+"/noFile.txt");
            // reuters is no longer in market data..
            File currentFile = findMostRecentFile(TradingDayUtil.getTradeDate(), symbolLoader.symbologyFolder, "reuters");
            SymbolMapAndLatestTimestamp sidMap = symbolLoader.loadSymbols(currentFile, true);
        }
        catch (Error e) {
            assertTrue(e instanceof MissingAnyFilesError);
        }
    }

    @Test
    public void testSymbolLoadingSystemProperty() {
        // test use of the system property
        System.setProperty(EMS_SYMBOL_FOLDER, RELATIVE_PATH_TO_CONFIG_DIR+"/symbology");
        testSymbolLoading(new File("unused due to property above"));
        System.setProperty(EMS_SYMBOL_FOLDER, "");
    }
}
