package com.hedgeos.ems.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.hedgeos.ems.config.beans.ActiveFixConnections;
import com.hedgeos.ems.config.beans.EmsConfigFiles;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import com.hedgeos.ems.config.beans.TradingEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = {FixAdapterConfigLoader.class})
@Slf4j
public class FixAdapterConfigLoaderTest {

    @Autowired
    FixAdapterConfigLoader fixAdapterConfigLoader;

    @Test
    public void testLoadFixAdapterConfig() {
        File f = new File(".");
        log.warn("Current working dir="+f.getAbsolutePath());
        // this folder should be up one dir from the working folder of this test
        String mockDir = "../ems-config-mock-files";
        File mockDirFile = new File(mockDir);
        log.warn("Full path to mockdir="+mockDirFile);
        if (mockDirFile.exists() == false) {
            // try from root of everything..
            mockDir = "./ems-config-mock-files";
            if (mockDirFile.exists() == false) {
                log.error("Can't find the mock config dir in a relative location?");
            }
        }

        String active = mockDir+"/active_mock_fix_connections.yml";
        EmsConfigFiles files = new EmsConfigFiles(new File(active), new File(mockDir));

        // ActiveFixConnections activeFixConnections = makeActiveConfig();

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();


        try {
            ActiveFixConnections activeFixConnections =
                    mapper.readValue(new File(active), ActiveFixConnections.class);

            List<FixAdapterConfig> adapterConfigs =
                    fixAdapterConfigLoader.loadFixAdapterConfig(files, activeFixConnections);

            // make sure the config we need is there
            FixAdapterConfig first = adapterConfigs.get(0);
            assertNotNull(first, "no config returned");
            assertNotNull(first.getExecutionVenue(), "no execution venue");
            assertEquals(7,  first.getExecutionVenue().getId());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private static ActiveFixConnections makeActiveConfig() {

        String name = "TEST_VENUE";
        List<String> configFileList = new ArrayList<>();
        configFileList.add("us_stock_connection_mock.yml");

        ActiveFixConnections activeFix =
                new ActiveFixConnections();
        activeFix.setName(name);
        activeFix.setTradingEnvironment(TradingEnvironment.TEST);
        activeFix.setIsTest(true);
        activeFix.setFixGatewayConfigFiles(configFileList);

        return activeFix;
    }
}
