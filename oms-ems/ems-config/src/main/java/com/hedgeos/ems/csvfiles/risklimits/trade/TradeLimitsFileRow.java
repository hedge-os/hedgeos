package com.hedgeos.ems.csvfiles.risklimits.trade;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.io.File;
import java.time.Duration;

@RequiredArgsConstructor
@Getter
@ToString
public class TradeLimitsFileRow {

    // for error messages regarding limit breaches
    final int rowNumberInFile;
    final File fileOfRow;

    // account and sid are the key fields for the rows by which positions are fetched in EMS
    final String pmGroup; // may be null, see docs
    final boolean pmGroupWildcard; // is the account a wildcard

    final Long sid;
    final boolean sidWildcard; // is the sid a wildcard

    // these two are parsed into a Duration
    final String timeUnit;
    final Long numTimeUnits;
    final Duration duration;

    final long qtyLong;
    final long qtyShort;

    // optional
    final Long exposureLong;
    final Long exposureShort;
}
