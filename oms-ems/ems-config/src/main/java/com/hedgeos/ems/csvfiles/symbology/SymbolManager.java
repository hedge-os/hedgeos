package com.hedgeos.ems.csvfiles.symbology;

import com.hedgeos.ems.service.EmsEnvironmentManager;
import com.hedgeos.ems.util.TradingDayUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Symbols for symbology like 'bloomberg' can be shared across several FixAdapters.
 * Centralize the logic in SymbolManager.
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class SymbolManager {
    public static final int ONE_MONTH_AND_A_DAY = 32;
    private final SortedMap<String, SortedMap<LocalDate, SymbolMaps>> symbolsPerDate = new ConcurrentSkipListMap<>();

    private final EmsEnvironmentManager environmentManager;

    public void updateSymbolMaps(String symbology, SymbolMaps reloadMaps) {
        log.warn("QuickfixAdapter setting new symbolMaps="+
                reloadMaps.latestSymbolFileTime+
                ", size="+reloadMaps.forwardSymbolMaps.normalSymbolMap().size());

        symbolsPerDate.putIfAbsent(symbology, new ConcurrentSkipListMap<>());

        if (symbolsPerDate.get(symbology).containsKey(reloadMaps.symbolsDate)) {
            log.info("Symbols exist for date="+reloadMaps.symbolsDate+", amending.");
            SymbolMaps existingSymbolsForDate = symbolsPerDate.get(symbology).get(reloadMaps.symbolsDate);

            existingSymbolsForDate.updateWith(reloadMaps);
        } else {
            // add the new date
            symbolsPerDate.get(symbology).put(reloadMaps.symbolsDate, reloadMaps);
        }

        // TODO:  A variable can hold the current date, and update in a background thread
//        LocalDate tradeDate = TradingDayUtil.getTradeDate();
//        // this becomes the data for currentDate
//        currentSymbolMaps = symbolsPerDate.getOrDefault(tradeDate, reloadMaps);
    }

    public void cleanOldSymbols(String symbology, LocalDate tradeDate) {
        // TODO:  Not clear we want this, in dev
        if (false == environmentManager.isStaleSymbolsEnabled()) {
            Set<LocalDate> toRemove = new HashSet<>();
            for (LocalDate localDate : symbolsPerDate.get(symbology).keySet()) {
                if (localDate.isBefore(tradeDate.minusDays(ONE_MONTH_AND_A_DAY))) {
                    log.warn("Trade date data from {} is before tradeDate {}, removing.", localDate, tradeDate);
                    toRemove.add(localDate);
                }
            }

            Map<LocalDate, SymbolMaps> perSymbology = symbolsPerDate.get(symbology);
            toRemove.forEach(perSymbology::remove);
        }
        else {
            log.warn("Not clearing SymbolMaps; in dev mode.");
        }
    }

    /**
     * Make a symbol adapter which already de-references the symbology in the primary map.
     * @param symbology the per fix adapter symbology
     * @return an adapter pointing to this specific symbology
     */
    public SymbolAdapter buildSymbolAdapter(String symbology) {

        SortedMap<LocalDate, SymbolMaps> perSymbologyMaps = this.symbolsPerDate.get(symbology);
        return new SymbolAdapter() {

            @Override
            public String getSymbology() {
                return symbology;
            }

            @Override
            public Long getSidForSymbol(String symbol) {
                LocalDate tradeDate = TradingDayUtil.getTradeDate();
                SymbolMaps maps = perSymbologyMaps.get(tradeDate);
                if (maps == null) {
                    // look backwards for prior days symbol maps
                    SortedMap<LocalDate, SymbolMaps> headMap = perSymbologyMaps.headMap(tradeDate);
                    if (headMap.isEmpty()) {
                        // should never happen; system won't start without a symbol map
                        log.error("No symbol maps for date or prior date={}, symbols={}", tradeDate, symbology);
                        return null;
                    }

                    // get the latest map less than trade date
                    maps = headMap.get(headMap.lastKey());
                }

                Long sid = maps.reverseSymbolMaps.priorityMap().get(symbol);
                if (sid != null)
                    return sid;

                return maps.reverseSymbolMaps.normalSymbolMap().get(symbol);
            }

            @Override
            public String getSymbolForSid(long sid) {
                LocalDate tradeDate = TradingDayUtil.getTradeDate();
                SymbolMaps maps = perSymbologyMaps.get(tradeDate);
                if (maps == null) {
                    // look backwards for prior days symbol maps
                    SortedMap<LocalDate, SymbolMaps> headMap = perSymbologyMaps.headMap(tradeDate);
                    if (headMap.isEmpty()) {
                        // should never happen; system won't start without a symbol map
                        log.error("No symbol maps for date or prior date={}, symbols={}", tradeDate, symbology);
                        return null;
                    }

                    // get the latest map less than trade date
                    maps = headMap.get(headMap.lastKey());
                }
                String symbol = maps.forwardSymbolMaps.priorityMap().get(sid);
                if (symbol != null)
                    return symbol;

                return maps.forwardSymbolMaps.normalSymbolMap().get(sid);
            }
        };
    }
}
