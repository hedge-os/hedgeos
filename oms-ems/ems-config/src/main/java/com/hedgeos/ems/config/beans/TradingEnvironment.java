package com.hedgeos.ems.config.beans;

public enum TradingEnvironment {

    WORKSTATION,TEST,QA,UAT,PROD
}
