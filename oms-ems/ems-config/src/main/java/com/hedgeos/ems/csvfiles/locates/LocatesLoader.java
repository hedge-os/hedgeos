package com.hedgeos.ems.csvfiles.locates;

import com.hedgeos.ems.config.beans.EmsVenueMap;
import com.hedgeos.ems.order.EmsExecutionVenue;
import com.hedgeos.ems.csvfiles.util.FlatFileUtil;
import com.hedgeos.ems.csvfiles.util.LoaderConstants;
import com.hedgeos.ems.order.LocatesFileRow;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Long.parseLong;

/**
 * Loader for loading the locates from ${EMS_CONFIG_FOLDER}/locates folder.
 * See decision 0009 for the file format and documentation.
 */
@Slf4j
@Getter
public class LocatesLoader {

    // locates file named like "locates_20230523.csv"
    public static final String LOCATES_FILE_PREFIX = "locates";

    // column names in the CSV
    public static final String LOCATES_COLUMN_NAME = "locates";
    public static final String PM_GROUP_COLUMN_NAME = "pmGroup";
    public static final String SECURITY_ID_COLUMN_NAME = "sid";
    public static final String VENUE_COLUMN_NAME = "venue";

    private final File locatesFolder;

    public LocatesLoader(File emsConfigFolder) {
        String locatesFileLocation = LocatesFileLocationUtil.getFolder(emsConfigFolder);
        locatesFolder = new File(locatesFileLocation);
    }

    public LocatesMapAndLatestTimestamp loadLocates(EmsVenueMap emsVenueMap,
                                                    File currentFile,
                                                    boolean loadLocates)
    {
        Map<LocatesFileRow, LocatesFileRow> locatesFileRowMap = new HashMap<>();

        if (loadLocates) {
            List<LocatesFileRow> locatesList = loadFile(emsVenueMap, currentFile);
            for (LocatesFileRow locatesFileRow : locatesList) {
                locatesFileRowMap.put(locatesFileRow, locatesFileRow);
            }
        }

        long latestTimestamp = currentFile.lastModified();
        for (int i = LoaderConstants.FIRST_OVERRIDE_FILE_SUFFIX; i< LoaderConstants.LAST_OVERRIDE_FILE_SUFFIX; i++) {
            File overlay = FlatFileUtil.findOverlay(currentFile, i);
            if (overlay == null) {
                break;
            }

            if (overlay.lastModified() > latestTimestamp) {
                latestTimestamp = overlay.lastModified();
            }

            if (loadLocates) {
                List<LocatesFileRow> overlayList = loadFile(emsVenueMap, overlay);
                for (LocatesFileRow positionOverlay : overlayList) {
                    log.info("Overlay found=" + positionOverlay);
                    locatesFileRowMap.put(positionOverlay, positionOverlay);
                }
            }
        }

        LocalDate dateForFile = FlatFileUtil.getDateForFile(currentFile);

        // need to track what date these positions are for, thus 'positionDate'
        LocatesMapAndLatestTimestamp mapAndLatestTimestamp =
                new LocatesMapAndLatestTimestamp(locatesFileRowMap, latestTimestamp, dateForFile, currentFile);

        return mapAndLatestTimestamp;
    }

    public static List<LocatesFileRow> loadFile(EmsVenueMap emsVenueMap, File currentFile) {
        Instant start = Instant.now();

        int rowNumber = 1;
        List<LocatesFileRow> locatesFileList = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader( currentFile.toPath() , StandardCharsets.UTF_8))
        {
            CSVFormat format = CSVFormat.Builder.create().setHeader().build();
            CSVParser parser = CSVParser.parse(reader,format);

            for (CSVRecord csvRecord : parser ) {
                rowNumber++;

                if (csvRecord.size() == 0) {
                    // skip blank lines
                    continue;
                }

                if (csvRecord.values()[0].startsWith("#")) {
                    // treat this as comments
                    continue;
                }

                String sidString = csvRecord.get(SECURITY_ID_COLUMN_NAME);

                long sid = parseLong(sidString);

                // pmGroup, NOT optional
                String pmGroup = null;
                if (csvRecord.isMapped(PM_GROUP_COLUMN_NAME)) {
                    pmGroup = csvRecord.get(PM_GROUP_COLUMN_NAME);
                }
                else {
                    throw new Error("Missing pmGroup for locates row="+rowNumber+", file="+currentFile.getAbsolutePath());
                }

                String locatesString = csvRecord.get(LOCATES_COLUMN_NAME);
                int locates = Integer.parseInt(locatesString);

                // locates are provided per Execution Venue.
                EmsExecutionVenue ev = null;
                if (csvRecord.isSet(VENUE_COLUMN_NAME)) {
                    String venueString = csvRecord.get(VENUE_COLUMN_NAME);

                    ev = emsVenueMap.getByName(venueString);
                    if (ev == null) {
                        String msg = MessageFormat.format("No Execution Venue for name, file={0}, row={1}, venue={2}",
                                currentFile.getAbsolutePath(), rowNumber, venueString);
                        log.error(msg);
                        throw new Error(msg);
                    }
                }
                else {
                    throw new Error("Missing venue column for locates, row="+rowNumber+", file="+currentFile.getAbsolutePath());
                }

                LocatesFileRow row = new LocatesFileRow(ev, sid, pmGroup, locates, currentFile, rowNumber);

                locatesFileList.add(row);
            }
        }
        catch (NumberFormatException nfe) {
            throw new Error("Error loading file="+currentFile, nfe);
        }
        catch ( IOException e )
        {
            log.error("Error loading file="+currentFile, e);
            // cant start the ems-service if symbols are off
            throw new Error("Error loading file="+currentFile, e);
        }

        Instant stop = Instant.now();
        Duration d = Duration.between( start , stop );
        log.warn( "Read Locates file CSV for count: " + rowNumber+ ", Elapsed: " + d );
        return locatesFileList;
    }

}
