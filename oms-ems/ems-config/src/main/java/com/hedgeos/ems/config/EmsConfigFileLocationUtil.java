package com.hedgeos.ems.config;

import com.hedgeos.ems.config.beans.EmsConfigFiles;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * Class for checking the files exist for two key environment variables
 * used to configure ems-service.
 *
 * EMS_CONFIG_FOLDER:  A folder of yaml containing potential fix connections.
 * EMS_CONFIG_FILE:  A file which picks which specific fix connections to run of the available set.
 */
@Component
@Slf4j
public class EmsConfigFileLocationUtil {
    public final static String FOLDER_MISSING_MESSAGE =
            "EMS_CONFIG_FOLDER **environment variable** is unset." +
                    "This environment variable must be set to the location containing the set of fix connection yaml config files." +
                    "See README.md for instructions on configuring ems-service.";

    public final static String FILE_MISSING_MESSAGE =
            "EMS_CONFIG_FILE **environment variable** is unset.  " +
                    "This environment variable must be set to the location of a file containing the set of FIX connections this server should load."+
                    "See README.md for instructions on configuring ems-service.";
    public static final String EMS_CONFIG_FOLDER = "EMS_CONFIG_FOLDER";
    public static final String EMS_CONFIG_FILE = "EMS_CONFIG_FILE";

    public EmsConfigFiles loadEmsConfig(EmsConfigVariables emsConfigVariables) {

        String folderMessage = "EMS_CONFIG_FOLDER="+ emsConfigVariables.emsConfigFolder;
        String fileMessage = "EMS_CONFIG_FILE="+ emsConfigVariables.emsConfigFile;

        log.warn("Looking for ems config folder: "+folderMessage);
        log.warn("Looking for ems config file: "+fileMessage);

        // warn level, and system out for good measure
        log.warn(folderMessage);
        log.warn(fileMessage);
        System.out.println(folderMessage);
        System.out.println(fileMessage);

        if (emsConfigVariables.emsConfigFile == null || emsConfigVariables.emsConfigFile.length() == 0) {
            log.error(FOLDER_MISSING_MESSAGE);
            System.err.println(FOLDER_MISSING_MESSAGE);
            // System.exit(-1);
            throw new RuntimeException(FOLDER_MISSING_MESSAGE);
        }

        if (emsConfigVariables.emsConfigFolder == null || emsConfigVariables.emsConfigFolder.length() == 0) {
            log.error(FILE_MISSING_MESSAGE);
            System.err.println(FILE_MISSING_MESSAGE);
            // System.exit(-1);
            throw new RuntimeException(FILE_MISSING_MESSAGE);
        }

        logWorkingDirectory();

        String checkForFolderMsg = "Checking for EMS_CONFIG_FOLDER="+ emsConfigVariables.emsConfigFolder;
        log.warn(checkForFolderMsg);
        System.out.println(checkForFolderMsg);

        File emsConfigFolder = new File(emsConfigVariables.emsConfigFolder);
        if (emsConfigFolder.exists() == false) {
            String folderExistsMessage = "Folder EMS_CONFIG_FOLDER="+ emsConfigVariables.emsConfigFolder +", does not exist. Exiting.";
            log.error(folderExistsMessage);
            System.out.println(folderExistsMessage);
            throw new RuntimeException(folderExistsMessage);
        }

        if (emsConfigFolder.isDirectory() == false) {
            String notDirectoryMessage = "Folder EMS_CONFIG_FOLDER is not a directory: "+ emsConfigVariables.emsConfigFolder;
            log.error(notDirectoryMessage);
            System.out.println(notDirectoryMessage);
            // System.exit(-3);
            throw new RuntimeException(notDirectoryMessage);
        }

        // check for the absolute then relative path of EMS_CONFIG_FILE
        File emsConfigFile = new File(emsConfigVariables.emsConfigFile);
        if (emsConfigFile.exists() == false) {
            String maybeSlash = "/";
            if (emsConfigVariables.emsConfigFolder.endsWith("/")) {
                maybeSlash = ""; // remove the slach, because it is on the end of the folder name
            }

            String combinedPath = emsConfigVariables.emsConfigFolder +maybeSlash+emsConfigFile;
            String combinedMsg = "Checking combined path for EMS_CONFIG_FILE="+combinedPath;
            log.warn(combinedMsg);
            System.out.println(combinedMsg);
            emsConfigFile = new File(combinedPath);
            if (emsConfigFile.exists() == false) {
                String fileNotExists = "No EMS Config File: Combined path EMS_CONFIG_FILE="+emsConfigFile;
                System.out.println(fileNotExists);
                log.error(fileNotExists);
                throw new RuntimeException(fileNotExists);
            }
        }

        // the file must be a file
        if (emsConfigFile.isFile() == false) {
            String emsConfigNotFile = "EMS_CONFIG_FILE is not a file: "+emsConfigFile.getAbsolutePath();
            log.error(emsConfigNotFile);
            System.err.println(emsConfigNotFile);
            // System.exit(-5);
            throw new RuntimeException(emsConfigNotFile);
        }

        // success: we found a folder, and a file in a folder
        EmsConfigFiles emsConfigSetting =
                new EmsConfigFiles(emsConfigFile, emsConfigFolder);

        // important to know this completes
        String successMsg = "Success locating EMS_CONFIG_FILE and EMS_CONFIG_FOLDER.";
        log.warn(successMsg);
        System.out.println(successMsg);

        return emsConfigSetting;
    }

    private static void logWorkingDirectory() {
        File workingDir = new File(".");
        String workingDirMessage = "Working directory="+workingDir.getAbsolutePath();
        log.warn(workingDirMessage);
        System.out.println(workingDirMessage);
    }
}
