package com.hedgeos.ems.csvfiles.risklimits.trade;

public class TradeLimitConstants {
    public static final int FIVE_SECOND_SUPPORTED_TRADE_UNIT_SECONDS = 5;
    public static final int ONE_SECOND_TRADE_UNIT_SUPPORTED = 1;
    public static final int ONE_MINUTE_TRADE_UNIT_SUPPORTED = 1;
    public static final int FIVE_MINUTE_TRADE_UNIT_SUPPORTED = 5;

    public static final int ONE_DAY_TRADE_UNIT_SUPPORTED = 1;
    public static final int ONE_HOUR_TRADE_UNIT_SUPPORTED = 1;
}
