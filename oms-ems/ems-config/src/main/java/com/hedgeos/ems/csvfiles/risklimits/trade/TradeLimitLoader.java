package com.hedgeos.ems.csvfiles.risklimits.trade;

import com.hedgeos.ems.csvfiles.risklimits.RiskLimitsFileLocationUtil;
import com.hedgeos.ems.csvfiles.util.FlatFileUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

import static com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitConstants.*;
import static com.hedgeos.ems.csvfiles.util.LoaderConstants.DASH_MEANS_IGNORE_COLUMN;
import static com.hedgeos.ems.csvfiles.util.LoaderConstants.WILDCARD_CHARACTER;
import static com.hedgeos.ems.csvfiles.util.LoaderConstants.FIRST_OVERRIDE_FILE_SUFFIX;
import static com.hedgeos.ems.csvfiles.util.LoaderConstants.LAST_OVERRIDE_FILE_SUFFIX;
import static java.lang.Long.parseLong;

@Slf4j
@Getter
public class TradeLimitLoader {
    // position limits file named like "position_limits_20230523.csv"
    public static final String TRADE_LIMIT_FILE_PREFIX = "trade_limits";

    // column names in the CSV
    public static final String PM_GROUP_COLUMN_NAME = "pmGroup";
    public static final String SECURITY_ID_COLUMN_NAME = "sid";
    private static final String TIME_UNIT_COLUMN_NAME = "timeUnit";
    private static final String NUM_TIME_UNITS_COLUMN_NAME = "numTimeUnits";
    private static final String QTY_LONG_COLUMN_NAME = "qtyLong";
    private static final String QTY_SHORT_COLUMN_NAME = "qtyShort";
    private static final String EXPOSURE_LONG_COLUMN_NAME = "exposureLong";
    private static final String EXPOSURE_SHORT_COLUMN_NAME = "exposureShort";
    public static final String UNDERSCORE_CHAR = "_";
    public static final String EMPTY_STRING = "";
    public static final String MAX_SECONDS_LIMIT_COUNT = "MAX_SECONDS_LIMIT_COUNT";

    public static int MAX_SECONDS_LIMIT_SUPPORTED = 500_000;

    private final File riskLimitsFolder;

    public TradeLimitLoader(File emsConfigFolder) {
        String riskLimitsFolderString = RiskLimitsFileLocationUtil.getFolder(emsConfigFolder);
        riskLimitsFolder = new File(riskLimitsFolderString);

        setMaxSecondsLimitCountFromSystemEnvIfSet();
    }

    private static void setMaxSecondsLimitCountFromSystemEnvIfSet() {
        if (System.getenv(MAX_SECONDS_LIMIT_COUNT) != null) {
            MAX_SECONDS_LIMIT_SUPPORTED = Integer.parseInt(System.getenv(MAX_SECONDS_LIMIT_COUNT));
            log.warn("Setting {}={}",MAX_SECONDS_LIMIT_COUNT, MAX_SECONDS_LIMIT_SUPPORTED);
        }
    }

    public TradeLimitLoad loadTradeLimits(File currentFile,
                                          boolean loadLimits)
    {
        Map<TradeLimitsFileRow, TradeLimitsFileRow> tradeLimitsMap = new LinkedHashMap<>();

        if (loadLimits) {
            List<TradeLimitsFileRow> tradeLimitsFileRows = loadFile(currentFile);
            for (TradeLimitsFileRow row : tradeLimitsFileRows) {
                tradeLimitsMap.put(row, row);
            }
        }

        long latestTimestamp = currentFile.lastModified();
        for (int i = FIRST_OVERRIDE_FILE_SUFFIX; i< LAST_OVERRIDE_FILE_SUFFIX; i++) {
            File overlay = FlatFileUtil.findOverlay(currentFile, i);
            if (overlay == null) {
                break;
            }

            if (overlay.lastModified() > latestTimestamp) {
                latestTimestamp = overlay.lastModified();
            }

            if (loadLimits) {
                List<TradeLimitsFileRow> overlayList = loadFile(overlay);
                for (TradeLimitsFileRow row : overlayList) {
                    log.info("Overlay found=" + row);
                    tradeLimitsMap.put(row, row);
                }
            }
        }

        LocalDate dateFromFile = FlatFileUtil.getDateForFile(currentFile);

        // need to track what date these positions are for, thus 'positionDate'
        TradeLimitLoad mapAndLatestTimestamp =
                new TradeLimitLoad(tradeLimitsMap, latestTimestamp, dateFromFile);

        return mapAndLatestTimestamp;
    }

    public static List<TradeLimitsFileRow> loadFile(File currentFile) {
        Instant start = Instant.now();

        int rowNum = 0;
        // the "SECONDS" limit cleaner thread, must complete in under 1 second.
        // therefore,
        int countSecondsLimits = 0;
        List<TradeLimitsFileRow> positionFileRows = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader( currentFile.toPath() , StandardCharsets.UTF_8))
        {
            CSVFormat format = CSVFormat.Builder.create().setHeader().build();
            CSVParser parser = CSVParser.parse(reader,format);

            for (CSVRecord csvRecord : parser ) {
                rowNum++;

                if (csvRecord.size() == 0) {
                    // skip blank lines
                    continue;
                }

                String firstValue = csvRecord.values()[0];
                if (firstValue.startsWith("#")) {
                    // skip a comment
                    continue;
                }

                boolean pmGroupWildcard = false;

                String pmGroup = csvRecord.get(PM_GROUP_COLUMN_NAME);
                if (pmGroup.trim().equals("*")) {
                    pmGroupWildcard = true;
                }

                boolean sidWildcard = false;
                Long sid = null;
                String sidString = csvRecord.get(SECURITY_ID_COLUMN_NAME);
                if (sidString.trim().equals(WILDCARD_CHARACTER)) {
                    sidWildcard = true;
                }
                else //noinspection PointlessBooleanExpression
                    if (sidString.isEmpty() == false &&
                            false == sidString.equals(DASH_MEANS_IGNORE_COLUMN)) {
                        sid = parseLong(sidString);
                    }


                String timeUnitString = csvRecord.get(TIME_UNIT_COLUMN_NAME);
                String numTimeUnitString = csvRecord.get(NUM_TIME_UNITS_COLUMN_NAME);
                Long numTimeUnit = Long.parseLong(numTimeUnitString);

                // the first char determines duration in Java Duration strings
                // S=second, H=hour,D=day, M=minute (case-insensitive)
                String firstCharTimeUnit = timeUnitString.substring(0,1);
                Duration duration =
                        parseDuration(currentFile, firstCharTimeUnit, numTimeUnit, timeUnitString);

                // can only handle 500,000 "SECONDS" limits; the cleaner thread
                // (this is a terrific amount, but it is not an infinite resource)
                // however, 5000 stocks on nasdaq, * 100 accounts would break it.
                // (if each uses 1 SECOND limit)
                // workaround - use 5 second limit.
                if (duration.getSeconds() == 1) {
                    countSecondsLimits++;
                    if (countSecondsLimits > MAX_SECONDS_LIMIT_SUPPORTED) {
                        String msg = "Exceeded the MAX for 1 SECOND limit";
                        log.error(msg);
                        // stop the system.
                        throw new Error(msg);
                    }
                }

                String qtyLongString = csvRecord.get(QTY_LONG_COLUMN_NAME);
                String noUnderscoresQtyLong = qtyLongString.replace(UNDERSCORE_CHAR, EMPTY_STRING);
                if (noUnderscoresQtyLong.trim().isEmpty()) {
                    String msg = "Blank qtyLong found in trade_limits file="+currentFile;
                    log.error(msg);
                    throw new Error(msg);
                }
                long qtyLong = Long.parseLong(noUnderscoresQtyLong);

                String qtyShortString = csvRecord.get(QTY_SHORT_COLUMN_NAME);
                String noUnderscoresQtyShort = qtyShortString.replace(UNDERSCORE_CHAR, EMPTY_STRING);
                if (noUnderscoresQtyShort.trim().isEmpty()) {
                    String msg = "Blank qtyShort found in trade_limits file="+currentFile;
                    log.error(msg);
                    throw new Error(msg);
                }
                long qtyShort = Long.parseLong(noUnderscoresQtyShort);

                // optional long exposure risk checks
                Long exposureLong = null;
                if (csvRecord.isSet(EXPOSURE_LONG_COLUMN_NAME)) {
                    String exposureLongString = csvRecord.get(EXPOSURE_LONG_COLUMN_NAME).trim();
                    //noinspection PointlessBooleanExpression
                    if (exposureLongString.isEmpty() == false) {
                        exposureLongString = exposureLongString.replace(UNDERSCORE_CHAR, EMPTY_STRING);
                        try {
                            exposureLong = (long) Double.parseDouble(exposureLongString);
                        } catch (NumberFormatException e) {
                            String msg = "Exception parsing file=" + currentFile.getAbsolutePath() + ", value=" + exposureLongString;
                            log.error(msg, e);
                            throw new Error(e);
                        }
                    }
                }

                // optional short exposure risk checks
                Long exposureShort = null;
                if (csvRecord.isSet(EXPOSURE_SHORT_COLUMN_NAME)) {
                    String exposureShortString = csvRecord.get(EXPOSURE_SHORT_COLUMN_NAME).trim();
                    //noinspection PointlessBooleanExpression
                    if (exposureShortString.isEmpty() == false) {
                        exposureShortString = exposureShortString.replace(UNDERSCORE_CHAR, EMPTY_STRING);
                        try {
                            exposureShort = (long) Double.parseDouble(exposureShortString);
                        } catch (NumberFormatException e) {
                            String msg = "Exception parsing file=" + currentFile.getAbsolutePath() + ", value=" + exposureShortString;
                            log.error(msg, e);
                            throw new Error(e);
                        }
                    }
                }

                TradeLimitsFileRow row =
                        new TradeLimitsFileRow(rowNum, currentFile, pmGroup, pmGroupWildcard, sid, sidWildcard, timeUnitString,
                                numTimeUnit, duration, qtyLong, qtyShort,
                                exposureLong, exposureShort);

                validateRowValidity(timeUnitString, numTimeUnit, row);

                positionFileRows.add(row);
            }
        }
        catch (NumberFormatException nfe) {
            // we can't start the system without correct limits
            throw new Error("Error loading file="+currentFile, nfe);
        }
        catch ( IOException e ) {
            log.error("Error loading file="+currentFile, e);
            // cant start the ems-service if the file is broken
            throw new Error("Error loading file="+currentFile, e);
        }

        Instant stop = Instant.now();
        Duration d = Duration.between( start , stop );
        log.warn( "Read Locates file CSV for count: " + positionFileRows.size()+ ", Elapsed: " + d );
        return positionFileRows;
    }

    private static void validateRowValidity(String timeUnitString, Long numTimeUnit, TradeLimitsFileRow row) {
        if (timeUnitString.toUpperCase().startsWith("S")) {
            if (numTimeUnit != ONE_SECOND_TRADE_UNIT_SUPPORTED && numTimeUnit != FIVE_SECOND_SUPPORTED_TRADE_UNIT_SECONDS) {
                String msg = "Seconds time unit only supports single and five second intervals (1 or 5), badRow=" + row;
                log.error(msg);
                throw new Error(msg);
            }
        }

        if (timeUnitString.toUpperCase().startsWith("M")) {
            if (numTimeUnit != ONE_MINUTE_TRADE_UNIT_SUPPORTED && numTimeUnit != FIVE_MINUTE_TRADE_UNIT_SUPPORTED) {
                String msg = "Minute time unit only supports single and five second intervals (1 or 5), badRow=" + row;
                log.error(msg);
                throw new Error(msg);
            }
        }

        if (timeUnitString.toUpperCase().startsWith("H")) {
            if (numTimeUnit != ONE_HOUR_TRADE_UNIT_SUPPORTED) {
                String msg = "Hours trading limit, time unit only supports single and five second intervals (1 or 5), badRow=" + row;
                log.error(msg);
                throw new Error(msg);
            }
        }

        if (timeUnitString.toUpperCase().startsWith("D")) {
            if (numTimeUnit != ONE_DAY_TRADE_UNIT_SUPPORTED) {
                String msg = "Days trading limit, time unit only supports single and five second intervals (1 or 5), badRow=" + row;
                log.error(msg);
                throw new Error(msg);
            }
        }
    }

    private static Duration parseDuration(File currentFile,
                                          String firstCharTimeUnit,
                                          Long numTimeUnit,
                                          String timeUnitString) {
        switch (firstCharTimeUnit.toUpperCase()) {
            case "S" -> {
                if (numTimeUnit == 1) {
                    return TradeLimitDurations.ONE_SECOND_LIMIT;
                }
                else if (numTimeUnit == 5) {
                    return TradeLimitDurations.FIVE_SECOND_LIMIT;
                }
                else {
                    String msg = "Only one and five second limit supported.  Passed=" + numTimeUnit;
                    log.error(msg);
                    throw new Error(msg);
                }
            }
            case "M" -> {
                if (numTimeUnit == 1)
                {
                    return TradeLimitDurations.ONE_MINUTE_LIMIT;
                }
                else if (numTimeUnit == 5) {
                    return TradeLimitDurations.FIVE_MINUTE_LIMIT;
                }
                else {
                    String msg = "Only one and five minute limit supported.  Passed=" + numTimeUnit;
                    log.error(msg);
                    throw new Error(msg);
                }
            }
            case "H" -> {
                if (numTimeUnit == 1) {
                    return TradeLimitDurations.ONE_HOUR_LIMIT;
                }
                else {
                    String msg = "Only one hour limit supported.  Passed=" + numTimeUnit;
                    log.error(msg);
                    throw new Error(msg);
                }
            }
            case "D" -> {
                if (numTimeUnit == 1) {
                    return TradeLimitDurations.ONE_DAY_LIMIT;
                }
                else {
                    String msg = "Only one single day limit supported.  Passed=" + numTimeUnit;
                    log.error(msg);
                    throw new Error(msg);
                }
            }
            default -> {
                String msg = "Unknown duration=" + timeUnitString + ", in file=" + currentFile.getAbsolutePath();
                log.error(msg);
                throw new Error(msg);
            }
        }
    }

    private static Double nullSafeExposureLong(CSVRecord csvRecord, String exposureShortColumnName) {
        String exposureShortString = csvRecord.get(exposureShortColumnName);
        try {
            return Double.parseDouble(exposureShortString);
        } catch (NumberFormatException e) {
            // ignore; column may be blank
        }
        return null;
    }
}
