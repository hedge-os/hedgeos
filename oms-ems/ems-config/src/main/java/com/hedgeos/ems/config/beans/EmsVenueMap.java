package com.hedgeos.ems.config.beans;

import com.hedgeos.ems.order.EmsExecutionVenue;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

public class EmsVenueMap {
    final Map<String, EmsExecutionVenue> executionVenueNameMap = new ConcurrentSkipListMap<>();
    final Map<Integer, EmsExecutionVenue> executionVenueIdMap = new ConcurrentSkipListMap<>();

    public void addVenue(EmsExecutionVenue executionVenue) {
        // map the venue name and ID; used to find locates which are per venue
        this.executionVenueNameMap.put(executionVenue.getName(), executionVenue);
        this.executionVenueIdMap.put(executionVenue.getId(), executionVenue);
    }

    public EmsExecutionVenue getByName(String name) {
        return this.executionVenueNameMap.get(name);
    }


    public EmsExecutionVenue getById(int id) {
        return this.executionVenueIdMap.get(id);
    }
}
