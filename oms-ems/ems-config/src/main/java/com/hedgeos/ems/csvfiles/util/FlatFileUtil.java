package com.hedgeos.ems.csvfiles.util;

import com.hedgeos.ems.util.TradingDayUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.springframework.cglib.core.Local;
import org.springframework.format.datetime.standard.DateTimeFormatterFactory;

import java.io.File;
import java.io.FileFilter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalField;
import java.util.NavigableMap;
import java.util.TreeMap;

@Slf4j
public class FlatFileUtil {

    public static final String YYYYMMDD = "yyyyMMdd";

    public static File findOverlay(File currentFile, int i) {
        String overlayName = currentFile.getAbsolutePath().replace(".csv", "_"+i+".csv");
        log.debug("Looking for overlay="+overlayName);
        File f = new File(overlayName);
        if (f.exists()) {
            log.debug("Found overlay file="+overlayName);
            return f;
        }
        log.debug("No overlay exists named="+overlayName);
        return null;
    }

    /**
     * Finds all files like prefix*.csv
     *
     * @param tradeDate the desired trade date
     * @param folder the folder to check; typically EMS_CONFIG_FOLDER/positions or similar
     * @param filePrefix "positions", "position_limits", "trading_limits", "locates",
     *                      or symbology like: "bloomberg"
     * @return the nearest file in descending filename order which matches the file
     *  for example if trade date = 20230525, filePrefix=bloomberg
     *      but bloomberg_20230522.csv is the most recent file by date in the file name.
     *      then bloomberg_20230522 is most recent (by this file name 20230522), it is returned.
     */
    public static File findMostRecentFile(LocalDate tradeDate, File folder, String filePrefix) {
        String prefixWildcardSuffix = filePrefix + "*.csv";
        FileFilter fileFilter =
                WildcardFileFilter
                        .builder()
                        .setWildcards(prefixWildcardSuffix)
                        .setIoCase(IOCase.INSENSITIVE)
                        .get();

        File[] files = folder.listFiles(fileFilter);
        // find the most recent file relative to today

        DateTimeFormatter format = new DateTimeFormatterFactory(YYYYMMDD).createDateTimeFormatter();

        String localDateString = format.format(tradeDate);

        log.debug("looking for file like="+localDateString);
        NavigableMap<String, File> filenames = new TreeMap<>();
        if (files == null || files.length == 0) {
            String msg = "No files match wildcard="+fileFilter.toString();
            log.error(msg);
            throw new MissingAnyFilesError(msg, filePrefix);
        }

        for (File file : files) {
            filenames.put(file.getName().toLowerCase(), file);
        }

        // bloomberg_20230525.csv, or positions_20230525.csv
        String currentDay = filePrefix +"_"+localDateString+".csv";
        if (filenames.containsKey(currentDay.toLowerCase())) {
            return filenames.get(currentDay.toLowerCase());
        }

        NavigableMap<String, File> descendingFiles = filenames.descendingMap();
        for (String fileName : descendingFiles.keySet()) {
            int underscore = fileName.indexOf("_", filePrefix.length()+1+8);
            if (underscore > 0) {
                // skipping override file, overrides happens after finding the base symbol file
                continue;
            }

            int dotCsv = fileName.indexOf(".csv");
            String yyyyMMdd = fileName.substring(dotCsv-8, dotCsv);
            int isAfter = yyyyMMdd.compareTo(localDateString);
            if (isAfter >= 1) {
                log.warn("Ignoring file for date later than trade date="+yyyyMMdd+", "+fileName);
            }
            else {
                return descendingFiles.get(fileName);
            }
        }

        String msg = "Failed to find files in folder="+folder.getAbsolutePath()+", with name="+filePrefix+", date before="+localDateString;
        throw new MissingAnyFilesError(msg, filePrefix);
    }

    public static LocalDate getDateForFile(File currentFile) {
        LocalDate tradeDate = TradingDayUtil.getTradeDate();
        String filename = currentFile.getName();

        int yearLocation = filename.indexOf(String.valueOf(tradeDate.getYear()));
        // the file may be for Jan 2 if it is Dec 31st
        if (yearLocation == -1) {
            String nextYear = String.valueOf(tradeDate.plusYears(1).getYear());
            yearLocation = filename.indexOf(nextYear);
        }

        String fullDate = filename.substring(yearLocation, yearLocation+YYYYMMDD.length());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(YYYYMMDD);
        return LocalDate.parse(fullDate, formatter);
    }
}
