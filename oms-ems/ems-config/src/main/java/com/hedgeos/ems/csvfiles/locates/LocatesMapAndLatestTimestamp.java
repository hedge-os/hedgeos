package com.hedgeos.ems.csvfiles.locates;

import com.hedgeos.ems.order.LocatesFileRow;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.time.LocalDate;
import java.util.Map;

@RequiredArgsConstructor
@Getter
public class LocatesMapAndLatestTimestamp {
    final Map<LocatesFileRow, LocatesFileRow> locatesFileRowMap;
    final long latestTimestamp;
    final LocalDate fileDate;

    final File file;

    // are accounts used or is it only per venue?
    boolean isAccountFieldUsed;
}
