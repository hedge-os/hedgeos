package com.hedgeos.ems.config.beans;

/**
 * The system can connect to more than one type of FIX Engine.
 * Current support is for Quickfix/J, but support
 * is planned and can be added for commercial FIX Engines.
 */
public enum FixAdapterType {
    QUICKFIX, // free, implemented
    ONIXS, // planned, $10,000 annual license
    CHRONICLE // planned, $60,000 annual license
}
