package com.hedgeos.ems.csvfiles.risklimits;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.File;

@Slf4j
public class RiskLimitsFileLocationUtil {
    public static final String EMS_RISK_LIMITS_FOLDER = "EMS_RISK_LIMITS_FOLDER";
    public static final String RISK_LIMITS_SUB_FOLDER = "/risklimits";

    public static String getFolder(File emsConfigFolder) {
        String envVariable = System.getenv(EMS_RISK_LIMITS_FOLDER);
        if (StringUtils.hasLength(envVariable)) {
            log.warn("Found env variable {}={}",EMS_RISK_LIMITS_FOLDER,envVariable);
            return envVariable;
        }

        String propertyValue = System.getProperty(EMS_RISK_LIMITS_FOLDER);
        if (StringUtils.hasLength(propertyValue)) {
            log.warn("Found system property {}={}", EMS_RISK_LIMITS_FOLDER,propertyValue);
            return propertyValue;
        }

        // default to the EMS_CONFIG_FOLDER value
        return emsConfigFolder.getAbsolutePath()+ File.separator+ RISK_LIMITS_SUB_FOLDER;
    }
}
