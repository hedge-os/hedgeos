package com.hedgeos.ems.csvfiles.positions;

import com.hedgeos.ems.order.PositionFileRow;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.Map;

@RequiredArgsConstructor
@Getter
public class PositionLoadMap {
    final Map<PositionFileRow, PositionFileRow> positionFileRowMap;
    final long latestTimestamp;

    // TODO:  This determines position dates; is it UTC or local?
    // TODO:  Should it be UTC, or local?
    final LocalDate fileDate;
}
