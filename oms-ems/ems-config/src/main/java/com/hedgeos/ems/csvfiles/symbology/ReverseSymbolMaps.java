package com.hedgeos.ems.csvfiles.symbology;

import java.util.Map;

public record ReverseSymbolMaps(Map<String, Long> priorityMap, Map<String, Long> normalSymbolMap) {
}
