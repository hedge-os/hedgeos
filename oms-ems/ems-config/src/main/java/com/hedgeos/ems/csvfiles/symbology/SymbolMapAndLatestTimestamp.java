package com.hedgeos.ems.csvfiles.symbology;

import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.Map;

@RequiredArgsConstructor
public class SymbolMapAndLatestTimestamp {
    final Map<Long, SymbolFileRow> symbolMap;
    final long latestFileTimestamp;
    final LocalDate fileDate;
}
