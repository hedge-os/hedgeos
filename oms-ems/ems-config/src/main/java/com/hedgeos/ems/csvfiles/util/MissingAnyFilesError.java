package com.hedgeos.ems.csvfiles.util;

public class MissingAnyFilesError extends Error {
    private final String symbologyName;

    public MissingAnyFilesError(String msg, String symbologyName) {
        super(msg);
        this.symbologyName = symbologyName;
    }
}
