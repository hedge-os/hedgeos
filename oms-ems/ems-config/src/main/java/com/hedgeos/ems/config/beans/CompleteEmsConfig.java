package com.hedgeos.ems.config.beans;

import com.hedgeos.ems.order.EmsExecutionVenue;
import lombok.Data;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Class to hold the combined config for EMS once loaded, prior to starting Fix Adapters.
 */
@Data
public class CompleteEmsConfig {
    private final EmsConfigFiles emsConfigSetting;
    private final ActiveFixConnections activeFixConnections;
    private final List<FixAdapterConfig> fixAdapterConfigList;

    private final EmsVenueMap emsVenueMap;

    public CompleteEmsConfig(EmsConfigFiles configFiles, ActiveFixConnections activeFixConnections,
                             List<FixAdapterConfig> fixAdapterConfigList)
    {
        this.emsConfigSetting = configFiles;
        this.activeFixConnections = activeFixConnections;
        this.fixAdapterConfigList = fixAdapterConfigList;
        emsVenueMap = new EmsVenueMap();
        for (FixAdapterConfig config : fixAdapterConfigList) {
            emsVenueMap.addVenue(config.getExecutionVenue());
        }
    }

    public File getEmsConfigFolder() {
        return emsConfigSetting.getEMS_CONFIG_FOLDER();
    }
}
