package com.hedgeos.ems.csvfiles.risklimits.position;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.time.LocalDate;
import java.util.Map;

@RequiredArgsConstructor
@Getter
public class PositionLimitLoad {
    final Map<PositionLimitsFileRow, PositionLimitsFileRow> positionLimitsMap;
    final long latestTimestamp;
    final LocalDate fileDate;

}
