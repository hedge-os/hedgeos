package com.hedgeos.ems.csvfiles.locates;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.File;

@Slf4j
public class LocatesFileLocationUtil {

    public static final String EMS_LOCATES_FOLDER = "EMS_LOCATES_FOLDER";
    public static final String LOCATES_SUBFOLDER = "/locates";

    public static String getFolder(File emsConfigFolder) {
        String envVariable = System.getenv(EMS_LOCATES_FOLDER);
        if (StringUtils.hasLength(envVariable)) {
            log.warn("Found env variable {}={}",EMS_LOCATES_FOLDER,envVariable);
            return envVariable;
        }

        String propertyValue = System.getProperty(EMS_LOCATES_FOLDER);
        if (StringUtils.hasLength(propertyValue)) {
            log.warn("Found system property {}={}", EMS_LOCATES_FOLDER,propertyValue);
            return propertyValue;
        }

        // default to the EMS_CONFIG_FOLDER value
        return emsConfigFolder.getAbsolutePath()+ File.separator+ LOCATES_SUBFOLDER;
    }
}
