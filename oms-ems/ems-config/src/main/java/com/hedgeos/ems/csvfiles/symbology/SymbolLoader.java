package com.hedgeos.ems.csvfiles.symbology;

import com.hedgeos.ems.csvfiles.util.FlatFileUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.hedgeos.ems.csvfiles.util.LoaderConstants.FIRST_OVERRIDE_FILE_SUFFIX;
import static com.hedgeos.ems.csvfiles.util.LoaderConstants.LAST_OVERRIDE_FILE_SUFFIX;
import static java.lang.Long.parseLong;

/**
 * Loads files with symbology name like "bloomberg_20230525.csv"
 * Allows overlays for corrections like "bloomberg_20230525_01.csv"
 * Keeps looking backwards until it finds a date with a file.
 * See the docs folder decision on symbol loading for details.
 */
@Slf4j
@Getter
public class SymbolLoader {

    // column names for the CSV loading
    public static final String SECURITY_ID_COLUMN_NAME = "sid";
    public static final String SYMBOL_COLUMN_NAME = "symbol";
    // priority need not be set; is for minuscule latency improvement
    public static final String PRIORITY_COLUMN_NAME = "priority";

    final File symbologyFolder;
    public SymbolLoader(File emsConfigFolder) {
        String folderName = SymbolLoaderFileLocationUtil.getFolder(emsConfigFolder);
        symbologyFolder = new File(folderName);
        if (symbologyFolder.exists() == false) {
            String msg = "No folder exists for symbology="+symbologyFolder.getAbsolutePath();
            log.error(msg);
            // this is an Error, will prevent system startup
            throw new SymbologyFolderException(msg);
        }
    }

    public SymbolMapAndLatestTimestamp loadSymbols(File currentFile, boolean loadTheSymbols)
    {
        Map<Long, SymbolFileRow> mapped = new HashMap<>();
        if (loadTheSymbols) {
            List<SymbolFileRow> symbolMap = loadFile(currentFile);
            mapped = symbolMap.stream().collect(
                            Collectors.toMap(SymbolFileRow::getSid, x -> x));
        }

        long latestTimestamp = currentFile.lastModified();
        for (int i = FIRST_OVERRIDE_FILE_SUFFIX; i< LAST_OVERRIDE_FILE_SUFFIX; i++) {
            File overlay = FlatFileUtil.findOverlay(currentFile, i);
            if (overlay == null) {
                break;
            }

            if (overlay.lastModified() > latestTimestamp) {
                latestTimestamp = overlay.lastModified();
            }

            if (loadTheSymbols) {
                List<SymbolFileRow> overlayList = loadFile(overlay);
                for (SymbolFileRow symbolFileRow : overlayList) {
                    log.info("Overlay found=" + symbolFileRow);
                    mapped.put(symbolFileRow.sid, symbolFileRow);
                }
            }
        }

        LocalDate dateFromFile = FlatFileUtil.getDateForFile(currentFile);
        return new SymbolMapAndLatestTimestamp(mapped, latestTimestamp, dateFromFile);
    }

    public static List<SymbolFileRow> loadFile(File currentFile) {
        Instant start = Instant.now();

        int rowNumber = 0;
        List<SymbolFileRow> symbolsList = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader( currentFile.toPath() , StandardCharsets.UTF_8))
        {
            rowNumber++;
            CSVFormat format =
                    CSVFormat.Builder.create().setHeader().build();

            CSVParser parser = CSVParser.parse(reader,format);

            for (CSVRecord csvRecord : parser ) {
                if (csvRecord.size() == 0) {
                    // skip blank lines
                    continue;
                }

                if (csvRecord.values()[0].startsWith("#")) {
                    // treat this as comments
                    continue;
                }

                String sidString = csvRecord.get(SECURITY_ID_COLUMN_NAME);

                // skip the header if it is there, otherwise use the first row
                long sid = parseLong(sidString);
                String symbol = csvRecord.get(SYMBOL_COLUMN_NAME);

                int priority = 0;
                if (csvRecord.isSet(PRIORITY_COLUMN_NAME)) {
                    priority = Integer.parseInt(csvRecord.get(PRIORITY_COLUMN_NAME));
                }

                SymbolFileRow row = new SymbolFileRow(sid, symbol, priority);
                symbolsList.add(row);
            }
        }
        catch (NumberFormatException nfe) {
            // try without headers
        }
        catch ( IOException e )
        {
            log.error("Error loading file="+currentFile, e);
            // cant start the ems-service if symbols are off
            throw new Error("Error loading file="+currentFile, e);
        }

        Instant stop = Instant.now();
        Duration d = Duration.between( start , stop );
        log.debug( "Read Symbol file CSV for rowNumber: " + rowNumber+ ", Elapsed: " + d );
        return symbolsList;
    }

}
