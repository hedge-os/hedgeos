package com.hedgeos.ems.config.beans;

import lombok.Value;

import java.io.File;

@Value
public class EmsConfigFiles {
    File EMS_CONFIG_FILE;
    File EMS_CONFIG_FOLDER;
}
