package com.hedgeos.ems.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.hedgeos.ems.config.beans.ActiveFixConnections;
import com.hedgeos.ems.config.beans.CompleteEmsConfig;
import com.hedgeos.ems.config.beans.EmsConfigFiles;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
@Slf4j
public class EmsConfigLoader {

    private final EmsConfigFileLocationUtil fileLocationUtil;
    private final FixAdapterConfigLoader fixAdapterConfigLoader;

    @Autowired
    public EmsConfigLoader(EmsConfigFileLocationUtil fileLocationUtil,
                           FixAdapterConfigLoader fixAdapterConfigLoader) {

        this.fileLocationUtil = fileLocationUtil;
        this.fixAdapterConfigLoader = fixAdapterConfigLoader;
    }

    public static final String SUCCESS_FOUND_FILES_MESSAGE = "Found and set files for EMS_CONFIG_FOLDER and EMS_CONFIG_FILE";
    public static final String START_CONFIG_MSG = "Checking for environment variables EMS_CONFIG_FOLDER and EMS_CONFIG_FILE";
    public static final String START_FILE_LOAD_MESSAGE = "Success locating EMS_CONFIG_FILE and EMS_CONFIG_FOLDER.";
    private static final String FINISHED_ACTIVE_LOAD_MESSAGE = "Finished loading EMS_CONFIG_FILE.";



    public CompleteEmsConfig loadConfig(EmsConfigVariables emsConfigVariables) {
        // load the environment variables
        EmsConfigFiles emsConfigSetting = locateConfigFolderAndFile(emsConfigVariables);

        // now load the files at the location specified by the env variables
        log.warn(START_FILE_LOAD_MESSAGE);
        System.out.println(START_FILE_LOAD_MESSAGE);

        ActiveFixConnections active =
                loadActiveFixConnectionsFile(emsConfigSetting);

        if (active == null) {
            String msg = "FAILED to load EMS_CONFIG_FILE="+emsConfigSetting.getEMS_CONFIG_FILE();
            log.error(msg);
            // System.exit(-7);
            throw new RuntimeException(msg);
        }

        log.warn(FINISHED_ACTIVE_LOAD_MESSAGE);
        System.out.println(FINISHED_ACTIVE_LOAD_MESSAGE);

        // using the known folders and the list of files, load the set of FIX Adapters
        List<FixAdapterConfig> adapterConfigs =
                fixAdapterConfigLoader.loadFixAdapterConfig(emsConfigSetting, active);

        String msg = "Success loading FixAdapter config Files, count="+adapterConfigs.size();
        log.warn(msg);
        System.out.println(msg);

        CompleteEmsConfig completeEmsConfig = new CompleteEmsConfig(emsConfigSetting, active, adapterConfigs);

        log.warn("Returning CompleteEmsConfig.");
        return completeEmsConfig;
    }

    private EmsConfigFiles locateConfigFolderAndFile(EmsConfigVariables emsConfigVariables)
    {
        // both log and system out these critical messages
        log.warn(START_CONFIG_MSG);
        System.out.println(START_CONFIG_MSG);

        // find the files: env variable EMS_CONFIG_FILE and EMS_CONFIG_FOLDER
        EmsConfigFiles emsConfigSetting = fileLocationUtil.loadEmsConfig(emsConfigVariables);

        // log we found the folder and files
        log.warn(SUCCESS_FOUND_FILES_MESSAGE);
        System.out.println(SUCCESS_FOUND_FILES_MESSAGE);

        return emsConfigSetting;
    }
    private ActiveFixConnections loadActiveFixConnectionsFile(EmsConfigFiles emsConfigFiles) {
        try {
            File activeConnectionsConfigFile = new File(emsConfigFiles.getEMS_CONFIG_FILE().getAbsolutePath());
            if (activeConnectionsConfigFile.exists() == false) {
                String msg = "No folder for active fix connections file="+activeConnectionsConfigFile.getAbsolutePath();
                log.error(msg);
                throw new RuntimeException(msg);
            }

            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            mapper.findAndRegisterModules();

            LogFileUtil.logFileContents(emsConfigFiles.getEMS_CONFIG_FILE().getAbsolutePath());

            ActiveFixConnections active = mapper.readValue(emsConfigFiles.getEMS_CONFIG_FILE(), ActiveFixConnections.class);

            String gatewayList =
                    StringUtils.collectionToDelimitedString(active.getFixGatewayConfigFiles(), ",");

            String loadedMsg = "LOADED config file, active connections, " +
                    "active count:"+active.getFixGatewayConfigFiles().size()+
                    ", list="+ gatewayList;

            // log and system.out, to make sure it is known
            log.warn(loadedMsg);
            System.out.println(loadedMsg);

            return active;

        } catch (IOException e) {
            String msg = "Failed to map EMS_CONFIG_FILE to ActiveFixConnections class.";
            log.error(msg, e);
            System.out.println(msg);
            throw new RuntimeException(msg);
        }
    }

}
