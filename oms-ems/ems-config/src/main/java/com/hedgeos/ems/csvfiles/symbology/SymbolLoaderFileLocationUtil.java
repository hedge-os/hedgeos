package com.hedgeos.ems.csvfiles.symbology;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.File;

@Slf4j
public class SymbolLoaderFileLocationUtil {
    public static final String EMS_SYMBOL_FOLDER = "EMS_SYMBOL_FOLDER";
    public static final String SYMBOLOGY_SUBFOLDER = "/symbology";

    public static String getFolder(File emsConfigVariables) {
        String envVariable = System.getenv(EMS_SYMBOL_FOLDER);
        if (StringUtils.hasLength(envVariable)) {
            log.warn("Found env variable EMS_SYMBOL_FOLDER="+envVariable);
            return envVariable;
        }

        String propertyValue = System.getProperty(EMS_SYMBOL_FOLDER);
        if (StringUtils.hasLength(propertyValue)) {
            log.warn("Found system property EMS_SYMBOL_FOLDER="+propertyValue);
            return propertyValue;
        }

        // default to the EMS_CONFIG_FOLDER value
        return emsConfigVariables.getAbsolutePath()+ File.separator+ SYMBOLOGY_SUBFOLDER;
    }
}
