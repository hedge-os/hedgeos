package com.hedgeos.ems.csvfiles.util;

public class LoaderConstants {
    // look for _1.csv to _100.csv
    public static final int FIRST_OVERRIDE_FILE_SUFFIX = 1;
    public static final int LAST_OVERRIDE_FILE_SUFFIX = 100;
    public static final String WILDCARD_CHARACTER = "*";
    public static final String DASH_MEANS_IGNORE_COLUMN = "-";
}
