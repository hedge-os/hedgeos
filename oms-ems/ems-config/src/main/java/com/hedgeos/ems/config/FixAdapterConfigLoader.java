package com.hedgeos.ems.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.hedgeos.ems.config.beans.ActiveFixConnections;
import com.hedgeos.ems.config.beans.EmsConfigFiles;
import com.hedgeos.ems.order.EmsExecutionVenue;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor
public class FixAdapterConfigLoader {

    Map<EmsExecutionVenue, FixAdapterConfig> executionVenues = new HashMap<>();

    public List<FixAdapterConfig> loadFixAdapterConfig(EmsConfigFiles emsConfigSetting,
                                                       ActiveFixConnections active)
    {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();

        List<FixAdapterConfig> fixAdapterConfigList = new ArrayList<>();
        // log them one at a time as well
        log.warn("-----------------------------------------------");
        log.warn("Start loading Config for FIX Adapters from: "+emsConfigSetting.getEMS_CONFIG_FOLDER());
        for (String gatewayConfigFile : active.getFixGatewayConfigFiles()) {
            // all EMS config files must be in the EMS_CONFIG_FOLDER
            String fullPath = emsConfigSetting.getEMS_CONFIG_FOLDER()+"/"+gatewayConfigFile;
            try {
                log.warn("Loading active gateway:" + gatewayConfigFile);
                log.warn("Full path: "+fullPath);
                File oneFixAdapter = new File(fullPath);

                // we want to see every file loaded in the log, for safety
                LogFileUtil.logFileContents(fullPath);

                FixAdapterConfig fixAdapterConfig =
                        mapper.readValue(oneFixAdapter, FixAdapterConfig.class);

                // need a symbology for trading
                if (fixAdapterConfig.getSymbology() == null) {
                    String msg = "name="+fixAdapterConfig.getName()+", lacks a symbology setting.";
                    log.error(msg);
                    throw new RuntimeException(msg);
                }

                // can't load the venues twice
                if (executionVenues.containsKey(fixAdapterConfig.getExecutionVenue())) {
                    FixAdapterConfig other = executionVenues.get(fixAdapterConfig.getExecutionVenue());
                    String msg = "Venue is already routed for another fix adapter="+
                            fixAdapterConfig.getConfigFileName()+"," +
                            ", venue="+fixAdapterConfig.getExecutionVenue().getName()+", " +
                            "the one using this venue="+other.getName();
                    log.error(msg);
                    throw new RuntimeException(msg);
                }

                // must have an execution venue
                if (fixAdapterConfig.getExecutionVenue() == null) {
                    String msg = "name="+fixAdapterConfig.getName()+", lacks an ExecutionVenue setting";
                    log.error(msg);
                    throw new RuntimeException(msg);
                }

                // add to the map, should not have multiple gateways connected to one venue
                executionVenues.put(fixAdapterConfig.getExecutionVenue(), fixAdapterConfig);

                fixAdapterConfigList.add(fixAdapterConfig);
                log.warn("Success loading gateway config:" + gatewayConfigFile);

            } catch (Exception e) {
                String errorMsg = "Exception loading active gateway="+gatewayConfigFile;
                log.error(errorMsg, e);
                throw new RuntimeException(errorMsg, e);
            }
        }

        log.warn("-----------------------------------------------");
        log.warn("Completed loading config, for active fix gateways, " +
                "count="+active.getFixGatewayConfigFiles().size());
        log.warn("-----------------------------------------------");
        return fixAdapterConfigList;
    }


}
