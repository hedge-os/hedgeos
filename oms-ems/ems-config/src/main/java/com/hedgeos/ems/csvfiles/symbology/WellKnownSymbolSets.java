package com.hedgeos.ems.csvfiles.symbology;

/**
 * Used only to generate a warning message if we don't recognize the configured
 * symbology; more to be added here.
 */
public enum WellKnownSymbolSets {
    BLOOMBERG,REFINITIV,NYSE,LSE,NASDAQ,OCC,CBOE,CBOT,FX
}
