package com.hedgeos.ems.config.beans;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.hedgeos.ems.order.EmsExecutionVenue;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class FixAdapterConfig {

    private String name;
    private FixAdapterType adapter_type;
    private String description;
    private String quickfix_adapter_config_file;

    // there must be a file like bloomberg_20230525.csv
    // in the config sub-folder '/symbology'
    private String symbology;

    // this is the venue serviced by this Fix Adapter
    // TODO:  Define all terms in a "definitions" document
    // TODO:  Review it with friends, and update names everywhere
    private EmsExecutionVenue executionVenue;

    private boolean resetSeqNum;

    public String getConfigFileName() {
        if (adapter_type == FixAdapterType.QUICKFIX) {
            return quickfix_adapter_config_file;
        }

        throw new RuntimeException("No other FIX engines in use yet.");
    }
}
