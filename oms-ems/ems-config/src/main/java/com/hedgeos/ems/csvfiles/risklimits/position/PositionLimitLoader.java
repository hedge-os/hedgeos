package com.hedgeos.ems.csvfiles.risklimits.position;

import com.hedgeos.ems.csvfiles.risklimits.RiskLimitsFileLocationUtil;
import com.hedgeos.ems.csvfiles.util.FlatFileUtil;
import com.hedgeos.ems.csvfiles.util.LoaderConstants;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitLoader.EMPTY_STRING;
import static com.hedgeos.ems.csvfiles.risklimits.trade.TradeLimitLoader.UNDERSCORE_CHAR;
import static com.hedgeos.ems.csvfiles.util.LoaderConstants.FIRST_OVERRIDE_FILE_SUFFIX;
import static com.hedgeos.ems.csvfiles.util.LoaderConstants.LAST_OVERRIDE_FILE_SUFFIX;
import static java.lang.Long.parseLong;

@Slf4j
@Getter
public class PositionLimitLoader {

    // position limits file named like "position_limits_20230523.csv"
    public static final String POSITION_LIMITS_FILE_PREFIX = "position_limits";

    // column names in the CSV
    public static final String PM_GROUP_COLUMN_NAME = "pmGroup";
    public static final String SECURITY_ID_COLUMN_NAME = "sid";
    private static final String QTY_LONG_COLUMN_NAME = "qtyLong";
    private static final String QTY_SHORT_COLUMN_NAME = "qtyShort";
    private static final String EXPOSURE_LONG_COLUMN_NAME = "exposureLong";
    private static final String EXPOSURE_SHORT_COLUMN_NAME = "exposureShort";

    private final File riskLimitsFolder;

    public PositionLimitLoader(File emsConfigFolder) {
        String riskLimitsFolder = RiskLimitsFileLocationUtil.getFolder(emsConfigFolder);
        this.riskLimitsFolder = new File(riskLimitsFolder);
    }

    public PositionLimitLoad loadPositionLimits(File currentFile,
                                                boolean loadLimits)
    {
        Map<PositionLimitsFileRow, PositionLimitsFileRow> positionLimitsMap = new HashMap<>();

        if (loadLimits) {
            List<PositionLimitsFileRow> positionLimitRows = loadFile(currentFile);
            for (PositionLimitsFileRow locatesFileRow : positionLimitRows) {
                positionLimitsMap.put(locatesFileRow, locatesFileRow);
            }
        }

        long latestTimestamp = currentFile.lastModified();
        for (int i = FIRST_OVERRIDE_FILE_SUFFIX; i< LAST_OVERRIDE_FILE_SUFFIX; i++) {
            File overlay = FlatFileUtil.findOverlay(currentFile, i);
            if (overlay == null) {
                break;
            }

            if (overlay.lastModified() > latestTimestamp) {
                latestTimestamp = overlay.lastModified();
            }

            if (loadLimits) {
                List<PositionLimitsFileRow> overlayList = loadFile(overlay);
                for (PositionLimitsFileRow positionOverlay : overlayList) {
                    log.info("Overlay found=" + positionOverlay);
                    positionLimitsMap.put(positionOverlay, positionOverlay);
                }
            }
        }

        LocalDate dateFromFile = FlatFileUtil.getDateForFile(currentFile);

        // need to track what date these positions are for, thus 'positionDate'
        PositionLimitLoad mapAndLatestTimestamp =
                new PositionLimitLoad(positionLimitsMap, latestTimestamp, dateFromFile);

        return mapAndLatestTimestamp;
    }

    public static List<PositionLimitsFileRow> loadFile(File currentFile) {
        Instant start = Instant.now();

        List<PositionLimitsFileRow> positionFileRows = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader( currentFile.toPath() , StandardCharsets.UTF_8))
        {
            CSVFormat format = CSVFormat.Builder.create().setHeader().build();
            CSVParser parser = CSVParser.parse(reader,format);

            int fileRowNumber = 0;
            for (CSVRecord csvRecord : parser ) {

                fileRowNumber++; // helpful for logging errors

                if (csvRecord.size() == 0) {
                    // skip blank lines
                    continue;
                }

                if (csvRecord.values()[0].startsWith("#")) {
                    // treat this as comments
                    continue;
                }

                boolean accountWildcard = false;
                String account = csvRecord.get(PM_GROUP_COLUMN_NAME);
                if (account.trim().equals("*")) {
                    accountWildcard = true;
                }

                boolean sidWildcard = false;
                Long sid = null;
                String sidString = csvRecord.get(SECURITY_ID_COLUMN_NAME);
                if (sidString.trim().equals(LoaderConstants.WILDCARD_CHARACTER)) {
                    sidWildcard = true;
                }
                else //noinspection PointlessBooleanExpression
                    if (sidString.isEmpty() == false &&
                        false == sidString.equals(LoaderConstants.DASH_MEANS_IGNORE_COLUMN)) {
                    sid = parseLong(sidString);
                }

                String qtyLongString = csvRecord.get(QTY_LONG_COLUMN_NAME);
                String noUnderscoresQtyLong = qtyLongString.replace(UNDERSCORE_CHAR, EMPTY_STRING);
                if (noUnderscoresQtyLong.trim().isEmpty()) {
                    String msg = "Blank qtyLong found in position_limits file="+currentFile;
                    log.error(msg);
                    throw new Error(msg);
                }
                long qtyLong = Long.parseLong(noUnderscoresQtyLong);

                String qtyShortString = csvRecord.get(QTY_SHORT_COLUMN_NAME);
                String noUnderscoresQtyShort = qtyShortString.replace(UNDERSCORE_CHAR, EMPTY_STRING);
                if (noUnderscoresQtyShort.trim().isEmpty()) {
                    String msg = "Blank qtyShort found in position_limits file="+currentFile;
                    log.error(msg);
                    throw new Error(msg);
                }
                long qtyShort = Long.parseLong(noUnderscoresQtyShort);
                if (qtyShort < 0) {
                    log.error("QuantityShort must be a positive value; the limit for the short shares quantity.");
                    qtyShort = Math.abs(qtyShort);
                }

                // optional
                Double exposureLong = null;
                if (csvRecord.isSet(EXPOSURE_LONG_COLUMN_NAME)) {
                    String exposureLongString = csvRecord.get(EXPOSURE_LONG_COLUMN_NAME);
                    exposureLongString = exposureLongString.replace(UNDERSCORE_CHAR, EMPTY_STRING);
                    if (exposureLongString.trim().isEmpty() == false) {
                        try {
                            exposureLong = Double.parseDouble(exposureLongString);
                        } catch (NumberFormatException e) {
                            String msg = "Exception parsing file=" + currentFile.getAbsolutePath() + ", value=" + exposureLongString;
                            log.error(msg, e);
                            throw new Error(e);
                        }
                    }
                }

                // optional
                Double exposureShort = null;
                if (csvRecord.isSet(EXPOSURE_SHORT_COLUMN_NAME)) {
                    String exposureShortString = csvRecord.get(EXPOSURE_SHORT_COLUMN_NAME);
                    exposureShortString = exposureShortString.replace(UNDERSCORE_CHAR, EMPTY_STRING);
                    if (exposureShortString.isEmpty() == false) {
                        try {
                            exposureShort = Double.parseDouble(exposureShortString);
                        } catch (NumberFormatException e) {
                            String msg = "Exception parsing file=" + currentFile.getAbsolutePath() + ", value=" + exposureShortString;
                            log.error(msg, e);
                            throw new Error(e);
                        }
                    }
                }

                PositionLimitsFileRow row =
                        new PositionLimitsFileRow(currentFile, fileRowNumber, account, accountWildcard, sid, sidWildcard,
                                qtyLong, qtyShort, exposureLong, exposureShort);

                // this row is thrown out; is a syntax error, not allowed
                if (row.isSidWildcard() && row.isPmGroupWildcard()) {
                    log.error("(skipping row) Syntax error, both sid and account are wildcard in positionLimits="+currentFile.getAbsolutePath());
                    continue;
                }

                positionFileRows.add(row);
            }
        }
        catch (NumberFormatException nfe) {
            throw new Error("Error loading file="+currentFile, nfe);
        }
        catch ( IOException e )
        {
            log.error("Error loading file="+currentFile, e);
            // cant start the ems-service if symbols are off
            throw new Error("Error loading file="+currentFile, e);
        }

        Instant stop = Instant.now();
        Duration d = Duration.between( start , stop );
        log.warn( "Read Position LIMITS file CSV for count: " + positionFileRows.size()+ ", Elapsed: " + d );
        return positionFileRows;
    }
}
