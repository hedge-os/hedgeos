package com.hedgeos.ems.csvfiles.symbology;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
public class SymbolFileRow {

    final long sid;
    public final String symbol;
    final int priority;
}
