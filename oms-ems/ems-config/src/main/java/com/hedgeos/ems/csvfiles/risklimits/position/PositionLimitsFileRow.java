package com.hedgeos.ems.csvfiles.risklimits.position;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.io.File;

@RequiredArgsConstructor
@Getter
@ToString
public class PositionLimitsFileRow {

    final File limitsFile; // could be from any overlay file; error msg states the file
    final int fileRowNumber;
    final String pmGroup; // may be null, or "*" (see docs)
    final boolean pmGroupWildcard; // is account "*" value

    final Long sid; // may be null (see docs)
    final boolean sidWildcard; // is the sid column "*" value

    final long qtyLong;
    final long qtyShort;

    // these are allowed to be null
    final Double exposureLong;
    final Double exposureShort;
}
