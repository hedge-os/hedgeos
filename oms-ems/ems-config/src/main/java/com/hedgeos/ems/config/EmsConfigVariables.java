package com.hedgeos.ems.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class EmsConfigVariables {
    final String emsConfigFolder;
    final String emsConfigFile;
}
