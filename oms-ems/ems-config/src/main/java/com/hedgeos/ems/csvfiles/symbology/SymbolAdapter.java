package com.hedgeos.ems.csvfiles.symbology;

public interface SymbolAdapter {
    Long getSidForSymbol(String symbol);
    String getSymbolForSid(long sid);

    // what is the symbology used for error messages
    String getSymbology();
}
