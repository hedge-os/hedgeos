package com.hedgeos.ems.csvfiles.symbology;

import java.util.Map;

public record ForwardSymbolMap(Map<Long, String> priorityMap, Map<Long, String> normalSymbolMap) {
}
