package com.hedgeos.ems.config.beans;

import lombok.Data;
import lombok.ToString;

import java.util.List;


@Data
@ToString
public class ActiveFixConnections {

    private String name;
    private TradingEnvironment tradingEnvironment;
    private Boolean isTest;

    private List<String> fixGatewayConfigFiles;

}
