package com.hedgeos.ems.csvfiles.risklimits.trade;

import java.time.Duration;

public class TradeLimitDurations {
    public static final Duration ONE_SECOND_LIMIT = Duration.ofSeconds(1);
    public static final Duration FIVE_SECOND_LIMIT = Duration.ofSeconds(5);
    public static final Duration ONE_MINUTE_LIMIT = Duration.ofMinutes(1);
    public static final Duration FIVE_MINUTE_LIMIT = Duration.ofMinutes(5);
    public static final Duration ONE_HOUR_LIMIT = Duration.ofHours(1);
    public static final Duration ONE_DAY_LIMIT = Duration.ofDays(1);

    public static final Duration[] DURATIONS =
                    {ONE_SECOND_LIMIT,
                            FIVE_SECOND_LIMIT,
                            ONE_MINUTE_LIMIT,
                            FIVE_MINUTE_LIMIT,
                            ONE_HOUR_LIMIT, ONE_DAY_LIMIT};
}
