package com.hedgeos.ems.config;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Log every config file we read, keeping a history of what was used
 * to start the system.
 */
@Slf4j
public class LogFileUtil {
    public static void logFileContents(String fullPath) throws FileNotFoundException {
        String fileContents = // the resulting String is stored here
                new Scanner( // Create a scanner to read the file
                        new File(fullPath)) // pass a new file to the scanner to read
                        .useDelimiter("\\Z") // set the delimiter to the end of the file
                        .next();
        log.warn( "\n*******************************************************\n"+
                "File contents\n" + fullPath + "\n" +
                "------------------------------------------------------\n"
                + fileContents
                + "\n*********************************************************");
    }
}
