package com.hedgeos.ems.csvfiles.symbology;

public class SymbologyFolderException extends Error {
    public SymbologyFolderException(String msg) {
        super(msg);
    }
}
