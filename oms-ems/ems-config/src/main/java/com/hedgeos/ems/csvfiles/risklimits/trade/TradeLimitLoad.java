package com.hedgeos.ems.csvfiles.risklimits.trade;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.time.LocalDate;
import java.util.Map;

@RequiredArgsConstructor
@Getter
@Slf4j
public class TradeLimitLoad {
    final Map<TradeLimitsFileRow, TradeLimitsFileRow> tradeLimitsMap;
    final long latestTimestamp;
    final LocalDate fileDate;
}
