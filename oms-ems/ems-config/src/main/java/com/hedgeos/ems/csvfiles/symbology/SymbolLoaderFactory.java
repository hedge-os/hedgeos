package com.hedgeos.ems.csvfiles.symbology;

import com.hedgeos.ems.csvfiles.util.FlatFileUtil;

import java.io.File;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class SymbolLoaderFactory {

    public static SymbolMaps getPriorityAndFullSymbolMap(LocalDate localDate,
                                                         String symbology,
                                                         File emsConfigFolder) {
        // load symbology
        SymbolLoader symbolLoader = new SymbolLoader(emsConfigFolder);

        File currentFile =
                FlatFileUtil.findMostRecentFile(localDate, symbolLoader.symbologyFolder, symbology);

        // needed for symbol mapping in the Fix Adapter
        SymbolMapAndLatestTimestamp mapAndLatestTimestamp = symbolLoader.loadSymbols(currentFile, true);

        Map<Long, String> priorityMap = new HashMap<>();
        Map<Long, String> normalSymbolMap = new HashMap<>();

        for (SymbolFileRow value : mapAndLatestTimestamp.symbolMap.values()) {
            if (value.getPriority() > 0) {
                priorityMap.put(value.getSid(), value.getSymbol());
            }

            // all symbols are in the normal priority map
            normalSymbolMap.put(value.getSid(), value.getSymbol());
        }

        ForwardSymbolMap forwardMap = new ForwardSymbolMap(priorityMap, normalSymbolMap);

        ReverseSymbolMaps reverseMap = reverseMaps(forwardMap);

        // only put the truth in memory; this symbolMap is for a specific date
        LocalDate symbolFileDate = FlatFileUtil.getDateForFile(currentFile);

        SymbolMaps symbolMaps =
                new SymbolMaps(mapAndLatestTimestamp.latestFileTimestamp,
                    forwardMap, reverseMap, symbolFileDate);

        return symbolMaps;
    }

    private static ReverseSymbolMaps reverseMaps(ForwardSymbolMap forwardMap) {
        // TODO:  Review, do these need to be ConcurrentHashMaps; can likely be HashMaps due to when constructed
        Map<String, Long> priorityMap = new HashMap<>(invertMapUsingForLoop(forwardMap.priorityMap()));
        Map<String, Long> normalMap = new HashMap<>(invertMapUsingForLoop(forwardMap.normalSymbolMap()));
        ReverseSymbolMaps reverseSymbolMaps = new ReverseSymbolMaps(priorityMap, normalMap);
        return reverseSymbolMaps;
    }

    public static <V, K> Map<V, K> invertMapUsingForLoop(Map<K, V> map) {
        Map<V, K> inversedMap = new HashMap<V, K>();
        for (Map.Entry<K, V> entry : map.entrySet()) {
            inversedMap.put(entry.getValue(), entry.getKey());
        }
        return inversedMap;
    }

}
