package com.hedgeos.ems.csvfiles.positions;

import com.hedgeos.ems.csvfiles.util.FlatFileUtil;
import com.hedgeos.ems.order.PositionFileRow;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

import static java.lang.Long.parseLong;

@Slf4j
@Getter
public class PositionLoader {

    // prefix for the files, named like positions_20230525.csv
    public static final String POSITIONS_FILES_PREFIX = "positions";

    // column names for the CSV File loading
    public static final String SECURITY_ID_COLUMN_NAME = "sid";
    public static final String PM_GROUP_COLUMN_NAME = "pmGroup";
    public static final String POSITION_COLUMN_NAME = "position";
    public static final String PRICE_COLUMN_NAME = "price";

    private final File positionsFolder;
    public PositionLoader(File emsConfigFolder) {
        String positionFileLocation = PositionLoaderFileLocationUtil.getFolder(emsConfigFolder);
        positionsFolder = new File(positionFileLocation);
    }

    public PositionLoadMap loadPositions(File currentFile, boolean loadThePositions, LocalDate dateFromFile)
    {
        Map<PositionFileRow, PositionFileRow> mapped = new HashMap<>();

        if (loadThePositions) {
            List<PositionFileRow> positionFileRowList = loadFile(currentFile);
            for (PositionFileRow positionFileRow : positionFileRowList) {
                mapped.put(positionFileRow, positionFileRow);
            }
        }

        long latestTimestamp = currentFile.lastModified();
        for (int i=1; i<100; i++) {
            File overlay = FlatFileUtil.findOverlay(currentFile, i);
            if (overlay == null) {
                break;
            }

            if (overlay.lastModified() > latestTimestamp) {
                latestTimestamp = overlay.lastModified();
            }

            if (loadThePositions) {
                List<PositionFileRow> overlayList = loadFile(overlay);
                for (PositionFileRow positionOverlay : overlayList) {
                    log.debug("Overlay found=" + positionOverlay);
                    mapped.put(positionOverlay, positionOverlay);
                }
            }
        }

        // need to track what date these positions are for, thus 'positionDate'
        PositionLoadMap mapAndLatestTimestamp =
                new PositionLoadMap(mapped, latestTimestamp, dateFromFile);

        return mapAndLatestTimestamp;
    }

    public static List<PositionFileRow> loadFile(File currentFile) {
        Instant start = Instant.now();

        int rowNumber = 0;
        List<PositionFileRow> positionFileList = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader( currentFile.toPath() , StandardCharsets.UTF_8))
        {
            CSVFormat format =
                    CSVFormat.Builder.create().setHeader().build();
            CSVParser parser = CSVParser.parse(reader,format);

            for (CSVRecord csvRecord : parser ) {
                rowNumber++;

                if (csvRecord.size() == 0) {
                    // skip blank lines
                    continue;
                }

                if (csvRecord.values()[0].startsWith("#")) {
                    // treat this as comments
                    continue;
                }

                String sidString = csvRecord.get(SECURITY_ID_COLUMN_NAME);

                long sid = parseLong(sidString);

                String pmGroup = csvRecord.get(PM_GROUP_COLUMN_NAME);

                // TODO:  Make this a Quantity or BigDecimal
                double position = Double.parseDouble(csvRecord.get(POSITION_COLUMN_NAME));

                Double price = null;

                if (csvRecord.isSet(PRICE_COLUMN_NAME)) {
                    price = Double.parseDouble(csvRecord.get(PRICE_COLUMN_NAME));
                }

                PositionFileRow row = new PositionFileRow(sid, pmGroup, position, price, rowNumber);
                positionFileList.add(row);
            }
        }
        catch (NumberFormatException nfe) {
            throw new Error("Error loading file="+currentFile, nfe);
        }
        catch ( IOException e )
        {
            log.error("Error loading file="+currentFile, e);
            // cant start the ems-service if symbols are off
            throw new Error("Error loading file="+currentFile, e);
        }

        Instant stop = Instant.now();
        Duration d = Duration.between( start , stop );
        log.warn( "Read Position file CSV for rowNumber: " + rowNumber+ ", Elapsed: " + d );
        return positionFileList;
    }

}
