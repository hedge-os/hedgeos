package com.hedgeos.ems.csvfiles.symbology;

import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class SymbolMaps {
    // only reload symbols if the symbol file time changes
    public final long latestSymbolFileTime;
    public final ForwardSymbolMap forwardSymbolMaps;
    public final ReverseSymbolMaps reverseSymbolMaps;
    public final LocalDate symbolsDate;

    public void updateWith(SymbolMaps reloadMaps) {
        // TODO: This could be in a monitor
        // copy and amend the normal maps
        forwardSymbolMaps.normalSymbolMap().putAll(reloadMaps.forwardSymbolMaps.normalSymbolMap());
        // copy and amend the reverse symbol maps, normal map
        reverseSymbolMaps.normalSymbolMap().putAll(reloadMaps.reverseSymbolMaps.normalSymbolMap());

        // priority map reload
        forwardSymbolMaps.priorityMap().putAll(reloadMaps.forwardSymbolMaps.priorityMap());
        reverseSymbolMaps.priorityMap().putAll(reloadMaps.reverseSymbolMaps.priorityMap());
    }
}
