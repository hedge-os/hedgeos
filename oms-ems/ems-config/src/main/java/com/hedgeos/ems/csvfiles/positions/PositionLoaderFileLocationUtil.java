package com.hedgeos.ems.csvfiles.positions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.File;

@Slf4j
public class PositionLoaderFileLocationUtil {
    public static final String EMS_POSITION_FOLDER = "EMS_POSITION_FOLDER";
    public static final String POSITION_SUBFOLDER = "/positions";

    public static String getFolder(File emsConfigFolder) {
        String envVariable = System.getenv(EMS_POSITION_FOLDER);
        if (StringUtils.hasLength(envVariable)) {
            log.warn("Found env variable {}={}",EMS_POSITION_FOLDER,envVariable);
            return envVariable;
        }

        String propertyValue = System.getProperty(EMS_POSITION_FOLDER);
        if (StringUtils.hasLength(propertyValue)) {
            log.warn("Found system property {}={}",EMS_POSITION_FOLDER,propertyValue);
            return propertyValue;
        }

        // default to the EMS_CONFIG_FOLDER value
        return emsConfigFolder.getAbsolutePath()+ File.separator+ POSITION_SUBFOLDER;
    }
}
