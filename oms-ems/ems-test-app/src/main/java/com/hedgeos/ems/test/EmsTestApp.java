package com.hedgeos.ems.test;

import com.hedgeos.ems.order.EmsRequestBuilder;
import com.hedgeos.client.ems.EmsClient;
import com.hedgeos.client.ems.EmsClientCallback;
import lombok.extern.slf4j.Slf4j;
import order.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
public class EmsTestApp {

    public static final String ACCOUNT_1 = "account1";
    public static final String ACCOUNT_2 = "account2";

    // MUST match the venueId in the FIX Config
    public static final int MORGAN_STANLEY_MOCK_VENUE = 7;
    // MUST be a PM Group defined in limits config, EMS server
    public static final String PM_GROUP_CODE = "CMR";
    public static final double MOCK_EXCHANGE_FILL_BUY_PRICE = 99.9;
    public static long FAKE_SECURITY_ID = 33;

    EmsClientCallback callback = new EmsClientCallback();

    EmsClient emsClient;

    public EmsTestApp() {

        String[] accountIds = new String[]{ACCOUNT_1, ACCOUNT_2};
        List<String> accountiDList = Arrays.asList(accountIds);

        String login = "test";

        // note the "silly" security; to prevent accidental connections to Prod
        String passwordHash = login+"WORKSTATION";
        passwordHash = passwordHash + passwordHash.length();

        // construct the client
        emsClient = new EmsClient("127.0.0.1", 9090, -1, login, passwordHash, accountiDList);
    }



    public void sendLimitOrder(Connection connection, long requestId, long hedgeOsOrderId, int side, long quantity, double price) {
        if (emsClient.isConnected() == false) {
            log.error("Not connected, wait for connection.");
            return;
        }

        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(connection,
                        requestId,
                        PM_GROUP_CODE, ACCOUNT_1,
                        hedgeOsOrderId,
                        quantity,
                        side,
                        MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        FAKE_SECURITY_ID,
                        price);

        List<order.OrderResponse> statusList = emsClient.sendOrder(request);
        log.warn("Orders sent!");
        for (OrderResponse OrderResponse : statusList) {
            String status = "FAILED";
            // TODO:  Translate to messages for each status code
            if (OrderResponse.status() == 1) {
                status = "SUCCESS";
            }
            log.warn("     status="+status+", orderId="+OrderResponse.hedgeosOrderId()+", statusCode="+OrderResponse.status());
        }
    }


    // TODO:  Move this to EmsClient
    // TODO:  Make convenience methods for common order placements,
    //  LIMIT order on security, Cancel, Replace
    public void sendMarketOrder(Connection connection, long requestId,
                                long hedgeOsOrderId,  int side, long quantity)
    {
        if (emsClient.isConnected() == false) {
            log.error("Not connected, wait for connection.");
            return;
        }

        // causes the builder to make a market order
        Double nullPrice = null;

        OrderRequest request =
                EmsRequestBuilder.createOrderRequest(connection,
                        requestId,
                        PM_GROUP_CODE, ACCOUNT_1,
                        hedgeOsOrderId,
                        quantity,
                        side,
                        MORGAN_STANLEY_MOCK_VENUE, // MUST match the venueId in the FIX Config
                        FAKE_SECURITY_ID,
                        nullPrice);

        List<order.OrderResponse> statusList = emsClient.sendOrder(request);
        log.warn("Orders sent!");
        for (OrderResponse orderResponse : statusList) {
            if (orderResponse.status() != OrderSubmitStatus.SUCCESSFUL_ORDER) {
                log.warn("  @@@@@@@@@@@@@@@@@@@@@@@@@ FAILED. @@@@@@@@@@@@@@@@@@@@@@@@@");
            }
            log.warn("     status="+orderResponse.status()+", orderId="+orderResponse.hedgeosOrderId()+", statusCode="+orderResponse.status());
        }
    }


    private void cancelOrder(Connection connection, long requestId, long cancelOrderId, long originalOrderIdToCancel) {
        if (emsClient.isConnected() == false) {
            log.error("Not connected, wait for connection.");
            return;
        }

        CancelRequest request =
                EmsRequestBuilder.createCancelRequest(connection, requestId, cancelOrderId, originalOrderIdToCancel);

        List<order.CancelResponse> statusList = emsClient.sendCancel(request);
        log.warn("Cancel sent!");
        for (CancelResponse cancelResponse : statusList) {
            String status = "CANCEL FAILED";
            // TODO:  Translate to messages for each status code
            if (cancelResponse.status() == CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS) {
                status = "CANCEL SUCCESS";
            }
            log.warn("     status="+status+
                    ", orderId="+cancelResponse.hedgeOsOrderId()+
                    ", statusCode="+cancelResponse.status());
        }

    }


    private  void connectAndSendOrders() throws InterruptedException {

        // start trying to reconnect every minute, and reconnect if stopped
        // returns if we connected once, or are waiting to connect.
        // returns false if ems-service is up, but returned failure to connect
        boolean isConnected =
                emsClient.reconnectLoop(callback);

        if (isConnected == false) {
            log.error("Failed to connect, exiting.");
            return;
        }

        // send an order very ten seconds
        while (true) {
            try {
                if (emsClient.isConnected() == false) {
                    System.out.println("Not connected, sleeping..");
                    Thread.sleep(3_000);
                    continue;
                }

                // sleep for 5, any messages.
                // TODO:  perhaps a shutdown status may arrive on a (separate) shutdown channel
                System.out.println("Sending market order -- Ems test app running at: "+new Date());

                // ideSafeReturnCarraigeInput();

                long quantity = 9;
                int side = Side.BUY;
                // long hedgeOsOrderId = emsClient.getNextOrderId();
                // testApp.sendMarketOrder(testApp.emsClient.connection, hedgeOsOrderId, side, quantity);

                log.warn("Sending LIMIT order");
                long hedgeOsOrderId2 = emsClient.getNextOrderId();
                double price = MOCK_EXCHANGE_FILL_BUY_PRICE + 1.0;
                sendLimitOrder(emsClient.getConnection(), getRequestId(), hedgeOsOrderId2, side, quantity, price);

                // ideSafeReturnCarraigeInput();

                log.warn("Sending CANCEL for the limit");
                long hedgeOsCancelId = emsClient.getNextOrderId();
                cancelOrder(emsClient.getConnection(), getRequestId(), hedgeOsCancelId, hedgeOsOrderId2);

                // wait for a return carriage; works in IntelliJ output window
                ideSafeReturnCarraigeInput();
                // then will send another order

            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("Error sending market or cancel order="+e);
                // don't hot spin on an error
                Thread.sleep(120_000);
            }
        }
    }

    static AtomicLong requestId = new AtomicLong(0);

    private long getRequestId() {
        return requestId.getAndIncrement();
    }

    private static void ideSafeReturnCarraigeInput() throws IOException {
        // necessary for the readline to work in an IDE
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Press return to send another order.");
        String returnCarriage = br.readLine();
        System.out.println("Received return carriage:"+returnCarriage);
    }

    public static void main(String[] args) throws InterruptedException {
        EmsTestApp testApp = new EmsTestApp();
        testApp.connectAndSendOrders();

    }

}
