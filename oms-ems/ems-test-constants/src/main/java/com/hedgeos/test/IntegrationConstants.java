package com.hedgeos.test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IntegrationConstants {

    // 99.9 is the magic level which BUYS and SELLS will REST in the MockExchange
    public static final double MAGIC_FILL_PRICE = 99.9;

    public static final double FILL_BUY_PRICE = MAGIC_FILL_PRICE;
    public static final double FILL_SELL_PRICE = MAGIC_FILL_PRICE;

    // These prices are NOT 99.9, and will be crossed by the Mock Exchange.
    public static final double BUY_LIMIT_PRICE_RESTS = 88.88;
    public static final double SELL_PRICE_LIMIT_RESTS = 777.77;
    public static final double RESTS_EITHER_BUY_OR_SELL = 666.66;
    private final static long CANCEL_REJECT_TEST_OFFSET = 1;
    // MAX_Value - 1 = big number to use for this.

    // This securityId will be cancel rejected for no reason.
    // and is setup in the bloomberg symbosl file.
    public final static long CANCEL_REJECT_TEST_SECURITY_ID = Long.MAX_VALUE - CANCEL_REJECT_TEST_OFFSET;

    static {
        log.warn("CANCEL_REJECT_TEST_SECURITY_ID="+CANCEL_REJECT_TEST_SECURITY_ID);
    }

    public static void main(String[] args) {
        System.out.println(IntegrationConstants.CANCEL_REJECT_TEST_SECURITY_ID);
    }
}
