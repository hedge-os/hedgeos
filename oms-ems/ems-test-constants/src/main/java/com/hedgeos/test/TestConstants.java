package com.hedgeos.test;

public class TestConstants {
    public static final String ACCOUNT_1 = "account1";
    public static final String ACCOUNT_2 = "account2";
    public static final int MORGAN_STANLEY_MOCK_VENUE = 7;
    // MUST be a PM Group defined in limits config, EMS server
    public static final String PM_GROUP_CODE = "CMR";
    public static final long UNKNOWN_SID = 987654321L;
    public static long FAKE_SECURITY_ID = 33;

    public static long FAKE_REPEATED_SHORT_SELL_THEN_CANCEL_ID = 9223372036854775805L;
    public static long FAKE_TEST_LOCATES_SECURITY_ID = 9223372036854775804L;

    public static long POSITION_LONG_SECURITY_ID = 9223372036854775803L;
    public static long POSITION_SHORT_SECURITY_ID = 9223372036854775802L;
}
