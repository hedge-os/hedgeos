# Gradle + flatbuffers + grpc + spring boot

## Notes

1.  Linux, need `cmake`
    - ```shell
        sudo apt install cmake
      ```
1.  Linux, to build 'flatc' need 'build-essentials' to run flatc,
with version **GLIBC_2.32**
    - ```shell
        sudo apt install build-essential
        ``` 
2. Open JDK 17:  
    ```shell
    sudo apt install openjdk-17-jdk
    sudo update-alternatives --config java
    ```
    The java alternative needs to be Java 17 for Spring boot.  This can also
    be done using gradle properties in your home folder .gradle.
1.  Reasonable gradle install for Mint
    https://www.markaicode.com/gradle-installation-on-linux-mint-21/

1.  IntelliJ, change Gradle to be Java 17 (if need be)
https://stackoverflow.com/questions/72117858/incompatible-because-this-component-declares-a-component-compatible-with-java-11

2.  The flatbuffers compiler needs to be a binary.
"flatc" in "fb-lib/flatc", needs to be binary, and 23.3.3.

The compiler is available for download, 
as windows and linux binaries
on the releases page:
https://github.com/google/flatbuffers/releases

    
## Build
```shell
./gradlew build
```
## Run
To run gRPC server
```shell
./gradlew :ems-service:bootRun
```
