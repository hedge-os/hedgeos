## Business plan for HedgeOs / HedgeEMS

### Marketing plan

1.  Make a great website
   2. Look at existing, but start fresh
2.  Demo:  Link to a demo on AWS, with demo login and password
3.  My linked-in profile:  Link to the code in my LinkedIn personal page (in six months?)
4.  Buy "open source ems" keyword on Google, Bing, Duck Duck Go., pay $100 a month.
   5.  Reference a consulting company, Force Software, with a contact info `sales/support@hedgeos.com`

### Monetize

1.  Deck of features and benefits - one page War sheet like Leticia said.
1.  Page of rates; review Abe's latest rates page and Confluent rates
2.  Rates example
   3. $30,000 for a one-week stand-up.
      4. $50,000 a year for Bronze support
      5. $100,000 a year for Silver support, and the UX for EMS
      6. $250,000 for Gold support and the UX / other value add, all components
3.  Sell components
   4. Security-Bank:  $25,000 for the security-bank as the second
   5. Data-Manager: $3,000 for data-manager a year.
   6. EMS support:  $25,000 a year for one day email support, phone support 1 hour a month, scheduled.
7. Keith / Christian as sales, marketing
8. Attend any trade shows
9. Write a blog article about something on Medium, and reference the company and system
   10. Use the threading article.
1.  Send an email to all my contacts, a release email
   2. Keith, Adrian, Paul-Mark, Ben, Bill Calloway, Bob Durie, Zach, Tom Dube, 
      3. Mine my entire linked in contacts for that email.
      4. Alex the AI guy, his fund, **as a first client** - ask him if he can run it.
      5. Test bed client where we can vet any bugs, improve the system.
      6. Kolja, Brian, Oleg.
      7. Abe, with his contacts if possible.
      8. Anri, and anyone they know who additionally could help.
1.  Sell the source with the system
   2. Can be enhanced
      3. Review license models / will start out mostly open source.  UX as closed source.

### Needed to be in business

1.  One client at $50,000 will be enough to pay the bills.
2.  One client at $100,000 will be enough to live well, enjoy life.

