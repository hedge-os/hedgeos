1.  **Firm ahead of ego**
    1. Put the software and firm ahead of your own self-interests.
    2. Failure to do so is considered treason.
1.  **Suggestions in writing**
    1. If there is a suggestion for software improvement, as many specifics as possible
    should be provided, in writing.  Before making the suggestion,
    it is ok to ask, 'is it worth writing up this suggestion', very briefly
    to 'riff' on a suggestion; however, until written down
    it will not be considered a viable suggestion.  (There are no bad ideas,
    only stupid people, is a harsh reality.)  The suggestion
    must include a rough cost estimate and benefits.
    2. The suggestion must be totally objective, and weigh carefully,
       the pros and cons, against the existing approach; favoring the **"status-quo"**
1. **Prove you need more complexity**
   1. If you need more complexity; must prove it.  Failure to argue vehmently in a document
        for the less complex option, is considered a non-starter for the alternative.
   1. **Data**
      1. Why doesn't Postgres, Timescale or MySQL (or DuckDB) sole your needs?  How high
        do those scale?  Do you need 99.99% uptime, or is 99.9% ok, or 99%.
      1. If 99.9% is even debatable as ok; must prefer a database, due to the velocity cost
        of not using a database, due to the elegance of SQL; and the ability to
        count, sort, "like" clauses, and "SUM,GROUP BY".
   1. **Architecture**
      1. Why doesn't a simpler architecture work, with trade-offs (including maintenance and complexity)
         1. for java, before Micro-services, prefer Spring w/ IOC (or Dagger)
         1. for React, before Micro-front ends, prefer libraries, strongly
   1. **Code level**
      1. At the code level, before ANTLR or a Rules engine, 
         must prefer and contrast using Java interfaces and IOC.
      2. Before building a rules engine, must compare a DSL in ANTLR and other DSLs; even Java.
      3. Must prefer the simpler and less complex option in the comparison.
 