#### Potential deal breakers

1. Regular kill must gracefully shut down the FIX connections.
1. Document and test behavior of system missing limits and positions files
1. Overlay loading issues
   1. Once an overlay file is found, 
      1. We must keep checking the timestamp of the non-overlay to see if it is newer.
   1. Overlay positions, must only wipe the one overlay, 
       not wipe out the entire set of Positions in Memory.

### Risky areas to review
4. Trade date rolling, must test extensively
2. System should fail-fast if positions not loaded for date by a certain time
2. Recovery, catastrophe handling
4. Many connections
5. QuantityConverter speed
1. revisit logging

### MVP

1. **UTC time for startup, of all servers**
   1.  Check in Java and fail if not set, little utility
1.  Document 'Getting started' for deployment
1.  Put a firewall in between and test breaking connections
2. Set the keepalive correctly
1.  S3 connection for all flat files (using an S3 simulator)
    2. With a switch in the code to detect S3 in the EMS_CONFIG_FOLDER variable

### Process
1. Get a working prototype, functionally complete
2. Start doing very extensive unit and functional testing end-end, automated w/ recovery.

# Next task

Stack on recovery:

789 seems to be preferred: 
https://www.fixtrading.org/standards/fix-session-layer-online/#using-nextexpectedmsgseqnum789-to-synchronize-a-fix-session

1. Recovery
   1. (done) if Gap Fill is sent; clear out the file (comment out GapFill.txt)
   1. (done) Provide a GapFill.txt situation
   1. (done) Wednesday  Provide a web-page to make the same call as GapFill.txt
      1. (done) Provide an ems-service admin web-page, link to the limits and GapFill page
   1. Wednesday: Add handling for PosDup=Y, state and cache changes.
   1. Wednesday: Test restarting again, see if it recovers.
   1. (in progress) Thursday: Recover from Kafka first, then use GapFill.
      2. Stack:  
         3. If Logon fails, and we can't find the ems-server-id topic on Kafka, should stop, error,
            4. Unless a very specific override is set.  And do more thinking.
         5. 
   1. If Kafka fails, only use GapFill(s), in blocks of thousands
   2. If Kafka fails, use disk for writes as a backup to /tmp
   1. ems-service must not be available until it completes.
   2. Client recovers from Kafka, follows this by connecting to the EMS with GapFills

1.  Recovery Web stuff and Harness
   2. Web stuff
      2. Web Page which shows you any Gaps in the Message history
         3. Identify Gaps
         4. Fill the Gaps; up to X messages at a time from the Config File, where X is probably 1,000.
      3. Web page which loads the FIX file, cracks it; internal.  Click any FIX message, it gets cracked.
   2. Provide a flag to handle NextExpectedSeqNum
      1. Add to mock exchange
   1. Build a recovery harness, using EmsMultiplexer
      2. Perhaps, one level beyond multiplexer; session mock in process.
      3. Indeed, session mock.
      1. Build recovery using MockExchange
         2. Does a resend request get sent?
         3. How do you trigger a resend request, and script it.

1.  Follow up on recovery
   2. // TODO:  Push an ADMIN type message on the Callback regarding this.
      3. (push Admin messages to the callbacks; to display alerts in the UX via Kafka)
      4. log.error should push specific EmsReport messages to Kafka as well; 
         5. Admin messages on System connection = 1.
         1. Add an Error channel to the callback.

1.  CancelOnDisconnect; as an option.

// TODO:  Option B:  Can parse this message, get how far we are off, and then..
// TODO:  1.  Logon   2.  Send a resend request for the difference.
// TODO:  3.  Need to handle duplicates (ExecID checking, keep them in memory.)
// TODO:  4.  If we see the same ExecID, discard it.
// TODO:  Before automating, look manually for a file; if we find a resend, do it.
// TODO:  This allows an operator to get any messages they need !!! :)

1.  Should we (yes?) set:  CancelOnDisconnect=Y, to get out of all open orders on disconnect.

### Trading limits API, + pre-check limits, then integration test

1.  Trading blocks are not by date - // TODO:  Bug here, trading per security needs to be by date

1. // TODO:  Careful about how this date is computed; should TRULY BE LOCAL

1. Document the internal object model order chain (diagrams.)

3. Test a chain of cancel replace; integration test
   4. https://www.onixs.biz/fix-dictionary/4.4/app_dD.2.a.html

---------------------------------------------
## Recovery and reconnect

2. Reconnect to the EMS:
   2. Is this just Kafka?
       3. // TODO:  Probably need to hold the messages, and play them
          // TODO:  Once someone connects to us, if replay = true.

1. Friday  - Start Recovery - mid june - through the weekend - do a design
   1. Could get from our own Kafka rewind.. but get from the broker, or think about it, or a hybrid, or both
            2. The locates cache must be populated
               3. Must update the Order Cache
               4. Must update the position manager
               5. Must update the limits
   1. Get from Broker first; request rewind.

-----------------------------------------------
## Restricted list

1. Next week: Bolt restricted onto Limits; or think about it
1. Thurs: PositionManager bug where it needs the ring buffer, 
   1. Or a future date which rolls to today.
   1. Make a ring buffer Generic
## Upcoming tasks
1.  The TT Certification script, as an Integration Suite.
1.  Gap Fill mid-day must only work if CumQty is supported; otherwise, fail the page.
1.  TEST all of these cases, and involving out of order GapFill recovery.
   1. https://www.onixs.biz/fix-dictionary/4.4/app_d.html#1
   1. Particularly all the cancel cases
1.  Make a 'pre-trading' checklist.
   1. Add sending a test trade in production, limit and cancel, and limit and fill, and market.
      1. Add testing using GapFill emergency catastrophe recovery
         1. Gap fill on a test trade after it is placed
   1. Add testing the clear limits button(s.) and the big red clear limits button.
1. Add optional flag to cancel on disconnect
2. Document async log decision - in the context of recovery.
1. Locate view-ability:
   1. Web page for locates in use vs. positions (position, locates used, and free locates)
   1. Second web page for locates, start of day, current, and usage
1. Put broken ExecutionReports into a File DLQ and a Kakfa DLQ
1. Improve the Base2 code.
2. Venue lookup page and calls; for this EMS
3. Pre-compile the JSPs
3. Fail a cancel replace due to already filled
2. UML for everything on GitUML
3. Logging; QuickFix plugin, to configur logging and a class which 
   4. has all behaviors; not two config files which need to be married.
2. **Tues** Recovery from Venue, adjusts positions
1.  Micrometer for order calls, in EmsService, timings
1. Add prometheus:
2. https://ryanharrison.co.uk/2021/01/06/gathering-metrics-micrometer-spring-boot-actuator.html
6. ?? Account mapping client side
   3. Mapping between 'desk' and 'account' on the client side
   4. Risk limit changes to support 'desk' concept

2. **Tues** Document the PositionChecker; and TradeLimits checker
3. **Wed**  Trade PnL limit checks (ignore exposure for now)

1.  Document mono-repo usage; open oms-ems in one project window
4. **Wed / Th / Fri**: Position file loading, with short selling flags
    4. Locates loaded
       5. Total locates for an EMS per Venue
           5. Locates shared across tag=1 accounts (?)
           5. TODO: Need client side: mapping between desks and accounts. 
           5. Set the locate required flag based on position and locates

## Then start end-end testing
3. Harness for full cancel-replace trading etc (certifications)
1.  Parent order ID to child order id mapping (design)

#### Testing
3. Then two clients from one Java client program
4. Then two mock exchanges, from the harness
5. Then add certification script.

#### Deployment and logging
8. Docker deployment script.
7. Logging deep dive, xml file config for logging

**May 2024 EMS Sprint**

FIX connectivity sprint:

**(May Week 2)** Need a FIX Engine abstraction, with QuickFix/J in the back end

2. Route end to end
   2. DONE: Make the OrderStatus result. (Wednesday)
   2. DONE: Start the FIX Engine (Thursday)
   2. DONE: Connect to Mock Exchange (Thursday)
   3. DONE: Send a message and a response (Thursday)
   4. Build up a test harness INCOMING/STREET syntax
      5. Document this test harness
   6. Cancel an order, with the harness. (Thursday)
   6. Test latency end to end

1.  DONE: Connection "suggestions" - Sticky connectionIds per login

### For others TODO, Day2, pre-launch
1. A risk limits database, also locates
   2. A module in ems which exports to Files, then to S3 of the folder
      3. For this specific database tables
      4. A UX (JHipster / Solid?) for managing those tables
      5. Then need a JHipster project to manage these tables; or a generic "table manager" app.
      6. Better:  Generic Table manager
1. Unit test reconnection and EMS system catastrophe
   2. Good learning experience to gain expertise in the EMS
   3. Great chance to debug / improve catstrophe handling, making another person capable of this.
2. Stop the EMS service if one Fix Adapter does not start
   1. This is work in EmsServiceGrpc startup, FixServerLifecycle
1. Exposure checks, using
   2. Order limit price, position prices, trading prices
      3. A latest price manager, collecting info.
      4. Ability to load and reload
         5. price files, for universe not in position
         6. Use the file loaders and override system

### Other notes
4. Campus:   (July Week 3-4)
   5. **(Week 3)** Need drop copy to Kafka for campus play of messages
   1. **(Week 3)** Need an EMS blotter server (sketch)
   2. **(Week 4)** Need an EMS blotter in AgGrid (ugly)
   
*Now, we have an EMS!*

#### WANT EMS
1. Calculate what the position will be, if the trade hits (Position Service internal.)
1. Limits check (retrieve from limits database, per account, every 24 hours with email alert failure)
3. Portfolio manager to accounts mapping table in postgres
2. Accounts to limits mapping table in Postgres
4. Need email alert library which throttles emails, log4j appender, added here and everywhere

### Security / Safety
1. Add a TEST user, or a TEST flag, which is detected and destroyed.
1. Need a default PROD environment, which destroys TEST user messages
1. Shut down if PROD sees a TEST message.
1. Decisions - Need a FIX Engine, QuickficJ, but can be abstracted 
1. Could be Chronicle, looks good:  https://chronicle.software/fix-engine/
1. Need a performance unit test.
1. Need risk checks, per Account (account flag must be set)
4. Position size (total position)
    5. Total net buys, and total net sells
    6. Need to feed the daily positions, save them (push to a file)
    7. Need to require the file exist before the start of trading.
5. (orders per second, min, dollars per second, min, trading pnl per sec, per min)

### Testing / Dev process
5. Need a simulator, run by log files, and for easy replay
6. Need files for the simulator to respond to certain messages with certain FIX log messages
7. Need a unit test harness with a mock simulator covering all the state changes


**EMS TODO**
1.  Need a replay flag to replay the days orders for **on connect**


# DONE THINGS

(DONE) Password of:
1.  "login+env+charsTotal"
2. As a stupidity check, preventing the same login
   3. being used in the wrong place by mistake.
* TODO:  Need a password like 'prod' for prod, and with a silly password: 'login+prod+numChars in login'

#### NEED EMS (Week 2 tasks)
4.  DONE Thursday / Friday:  Configuration in Spring for what fix connectors to run; pin it to a thread (later)
5.  DONE Thursday / Friday:  FIX Engine abstraction (perhaps), for QuickFix J
2. DONE Saturday: Symbology mapping (from security master flat file export)
   1.  DONE Why?  Handles emergencies manually, fast.
1. DONE Sunday: Route from an Exchange.proto API file with IB, RAZOR, LMAX, etc., to a fix connector

1. DONE Trading date must be mapped for symbols; as they may change "tomorrow".
3. DONE Need a map, Symbol --> SID, which is updated as the date rolls
1.  Done:  700ms currently - Send all 5000 Nasdaq stocks in one OrderRequest message (a basket trade)


### DONE: used reconnect logic: On reconnect, not receiving EmsExecution Reports
#### To reproduce
1. Connect, send an order, get an exec
2. Restart test app
3. Starts, connects, sends another order.
4. Exec report is generate by Server
5. Exec report **not** received by Test App

### DONE:
- Connected, could not send a response; netty thread death on callback.
```text
io.netty.handler.codec.http2.Http2Exception$StreamException: Stream closed before write could take place
	at io.netty.handler.codec.http2.Http2Exception.streamError(Http2Exception.java:173) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.handler.codec.http2.DefaultHttp2RemoteFlowController$FlowState.cancel(DefaultHttp2RemoteFlowController.java:481) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.handler.codec.http2.DefaultHttp2RemoteFlowController$1.onStreamClosed(DefaultHttp2RemoteFlowController.java:105) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.handler.codec.http2.DefaultHttp2Connection.notifyClosed(DefaultHttp2Connection.java:357) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.handler.codec.http2.DefaultHttp2Connection$ActiveStreams.removeFromActiveStreams(DefaultHttp2Connection.java:1007) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.handler.codec.http2.DefaultHttp2Connection$ActiveStreams.deactivate(DefaultHttp2Connection.java:963) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.handler.codec.http2.DefaultHttp2Connection$DefaultStream.close(DefaultHttp2Connection.java:515) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.handler.codec.http2.DefaultHttp2Connection.close(DefaultHttp2Connection.java:153) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.handler.codec.http2.Http2ConnectionHandler$BaseDecoder.channelInactive(Http2ConnectionHandler.java:217) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.handler.codec.http2.Http2ConnectionHandler.channelInactive(Http2ConnectionHandler.java:432) ~[netty-codec-http2-4.1.91.Final.jar:4.1.91.Final]
	at io.grpc.netty.NettyServerHandler.channelInactive(NettyServerHandler.java:637) ~[grpc-netty-1.54.1.jar:1.54.1]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:305) ~[netty-transport-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:281) ~[netty-transport-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.channel.AbstractChannelHandlerContext.fireChannelInactive(AbstractChannelHandlerContext.java:274) ~[netty-transport-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.channel.DefaultChannelPipeline$HeadContext.channelInactive(DefaultChannelPipeline.java:1405) ~[netty-transport-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:301) ~[netty-transport-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.channel.AbstractChannelHandlerContext.invokeChannelInactive(AbstractChannelHandlerContext.java:281) ~[netty-transport-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.channel.DefaultChannelPipeline.fireChannelInactive(DefaultChannelPipeline.java:901) ~[netty-transport-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.channel.AbstractChannel$AbstractUnsafe$7.run(AbstractChannel.java:813) ~[netty-transport-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.util.concurrent.AbstractEventExecutor.runTask$$$capture(AbstractEventExecutor.java:174) ~[netty-common-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.util.concurrent.AbstractEventExecutor.runTask(AbstractEventExecutor.java) ~[netty-common-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.util.concurrent.AbstractEventExecutor.safeExecute$$$capture(AbstractEventExecutor.java:167) ~[netty-common-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.util.concurrent.AbstractEventExecutor.safeExecute(AbstractEventExecutor.java) ~[netty-common-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.util.concurrent.SingleThreadEventExecutor.runAllTasks(SingleThreadEventExecutor.java:470) ~[netty-common-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.channel.epoll.EpollEventLoop.run(EpollEventLoop.java:403) ~[netty-transport-classes-epoll-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.util.concurrent.SingleThreadEventExecutor$4.run(SingleThreadEventExecutor.java:997) ~[netty-common-4.1.91.Final.jar:4.1.91.Final]
	at io.netty.util.internal.ThreadExecutorMap$2.run(ThreadExecutorMap.java:74) ~[netty-common-4.1.91.Final.jar:4.1.91.Final]
	at com.hedgeos.grpc.utils.PinnedCoreAffinityThreadFactory.lambda$newThread$0(PinnedCoreAffinityThreadFactory.java:24) ~[main/:na]
	at java.base/java.lang.Thread.run(Thread.java:833) ~[na:na]
``` 

# Docs

#### EMSX
https://emsx-api-doc.readthedocs.io/en/latest/programmable/requestResponse.html
https://github.com/tkim/emsx_api_repository/blob/master/EMSXFullSet_Java/AssignTrader.java


## Questions for senior folks
1.  Account Tag1 mapping: Can we make the EMS account based, with a mapping
    2. The mapping will be client side per security per country, (per of orders)
    3. Does this mapping table exist, with a UX to manage it
    4. I can provide this schema; should be in Postgres in Docker
    5. Then need a JHipster project to manage these tables; or a generic "table manager" app.
        6. The "table manager" would be a good one for Chris to write
3. restricted list + risk checks, how behaves.
4. locates, how behaves related to account tag, overall locates