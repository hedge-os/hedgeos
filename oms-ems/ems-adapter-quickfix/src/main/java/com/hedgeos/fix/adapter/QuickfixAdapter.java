package com.hedgeos.fix.adapter;

import com.hedgeos.ems.config.beans.EmsConfigFiles;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import com.hedgeos.ems.csvfiles.symbology.SymbolAdapter;
import com.hedgeos.ems.order.OrderChain;
import com.hedgeos.ems.service.EmsEnvironmentManager;
import com.hedgeos.fix.adapter.recovery.*;
import lombok.extern.slf4j.Slf4j;
import order.CancelReplace;
import order.Order;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import quickfix.*;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component(FixAdapterFactory.QUICKFIX_ADAPTER_EMS) // Notice how this is tied to the factory
public class QuickfixAdapter implements FixAdapter {

    public static final String GAP_FILL_FILENAME = "GapFill.txt";
    private final EmsEnvironmentManager emsEnvironmentManager;
    // THESE ADAPTERS MUST NOT BE SINGLETONS, protect against it
    private volatile boolean hasBeenLoaded = false;

    private volatile boolean initiatorStarted = false;

    private FixAdapterConfig config;

    private EmsCallback emsCallback;

    // this changes very rarely, and is a small list - thus CopyOnWriteArrayList
    private Initiator initiator;

    private QuickfixMessageAdapter quickfixMessageAdapter;

    // INTENTIONALLY STATIC
    private static final Map<SessionID, SessionID> duplicateSessionKeyProtection = new ConcurrentHashMap<>();
    private volatile SessionID sessionID;
    private QuickfixSender quickfixSender;
    private FileWatch gapFillFileWatch;
    private CreateModifyCallback triggerGapFillCallback;
    private int emsServerId;

    // TODO:  Can speed up with a variable for today's symbols
    // private SymbolMaps currentSymbolMaps;

    public QuickfixAdapter(EmsEnvironmentManager environmentManager) {
        this.emsEnvironmentManager = environmentManager;
        QuickFixSendConnectionImpl impl = new QuickFixSendConnectionImpl();
        quickfixSender = new QuickfixSender(impl);

        // make a trigger to call back to here, when GapFill.txt is dropped in the connection folder.
        triggerGapFillCallback = new TriggerGapFillCallback(this);
    }

    @Override
    public String getName() {
        return config.getName();
    }

    @Override
    public int sendGapFill(int from, int to) {
        return quickfixSender.sendGapFill(sessionID, from, to);
    }

    @Override
    public void subscribe(EmsCallback callback) {
        this.emsCallback = callback;
    }

    @Override
    public FixAdapterConfig getConfig() {
        return this.config;
    }

    @Override
    public void loadAndConnect(int emsServerId,
                               EmsConfigFiles emsConfig,
                               FixAdapterConfig config,
                               SymbolAdapter symbolAdapter)
    {
        protectAgainstAdapterReuseOnTwoConnections();

        this.emsServerId = emsServerId;

        this.config = config;

        // Could be an assertion, means code written incorrectly
        if (emsCallback == null) {
            throw new RuntimeException("Must set emsCallback before loadAndConnect");
        }

        bootQuickfixConnection(emsConfig, symbolAdapter);
    }


    private void bootQuickfixConnection(EmsConfigFiles emsConfig, SymbolAdapter symbolAdapter)
    {
        try {
            SessionSettings quickfixSettings =
                    QuickfixSettingsLoader.loadQuickfixSettings(emsConfig, this.config);
            // TODO:  Look for / review, need a bridge to slf4j
            // TODO:  Every session has a specific log
            LogFactory logFactory = new SLF4JLogFactory(quickfixSettings);
            // LogFactory quicfixLogFactory = new FileLogFactory(quickfixSettings);
            Dictionary d = quickfixSettings.get();

            File fileStorePath = createAndLogPath(d, "FileStorePath");

            File newGapFillFile =
                    new File(fileStorePath.getAbsolutePath()+File.separator+GAP_FILL_FILENAME);
            boolean created = newGapFillFile.createNewFile();
            if (!created) {
                log.error("Could not create GapFill.txt at: "+newGapFillFile.getAbsolutePath());
            }

            FileWatcherImpl.writeFreshGapFillFile(newGapFillFile);

            // watch the FileStore path for a file ResetSeqNum
            gapFillFileWatch = new FileWatcherImpl();
            // monitor the connection folder for the GapFill.txt file create, modify
            gapFillFileWatch.watchFile(fileStorePath.toPath(), newGapFillFile.getName(), triggerGapFillCallback);

            createAndLogPath(d, "FileLogPath");

            FileStoreFactory messageStoreFactory = new FileStoreFactory(quickfixSettings);

            MessageFactory messageFactory = new DefaultMessageFactory();

            // note:  We need the symbolAdapter in the return from the FIX Venue;
            // for speed, allowing us to avoid cracking the flatbuffer and adding they symbol
            // during return routing.
            quickfixMessageAdapter =
                    new QuickfixMessageAdapter(
                            config.isResetSeqNum(),
                            emsServerId,
                            emsEnvironmentManager,
                            emsCallback, symbolAdapter,
                            quickfixSender, config,
                            config.getExecutionVenue());

            // network connect using QuickFixJ
            initiator =
                    new SocketInitiator(quickfixMessageAdapter,
                            messageStoreFactory,
                            quickfixSettings,
                            logFactory,
                            messageFactory);

            // JmxExporter exporter = new JmxExporter();
            // exporter.register(initiator);

            logon();
        }
        catch (Exception e) {
            String msg = "Exception loading QuickFix adapter="+ this.config.getName();
            log.error(msg, e);
            throw new RuntimeException(msg, e);
        }
    }

    private static File createAndLogPath(Dictionary d, String key) throws ConfigError, FieldConvertError {
        String fileStorePath = d.getString(key);
        File fileStorePathFile = new File(fileStorePath);
        if (fileStorePathFile.exists() == false) {
            boolean madeDirs = fileStorePathFile.mkdirs();
            if (madeDirs) {
                log.warn("created folders -->"+key+"=" + fileStorePathFile.getAbsolutePath());
            }
        }
        else {
            log.warn("FOUND EXISTING "+key+"="+fileStorePathFile.getAbsolutePath());
        }
        return fileStorePathFile;
    }


    private void protectAgainstAdapterReuseOnTwoConnections() {
        if (hasBeenLoaded) {
            log.error("Adapter is a singleton, but must be a prototype.");
            throw new RuntimeException("Adapter is a singleton but must be a prototype");
        }
        this.hasBeenLoaded = true;
    }

    public synchronized void logon() {
        if (!initiatorStarted) {
            try {
                initiator.start();
                initiatorStarted = true;

                for (SessionID sessionId : initiator.getSessions()) {
                    // check a static Map; avoid an error where two Quickfix Adapters use the same session key, breaks
                    // the EMS system
                    // TODO:  Move this to a "configCheck" step; not here, too late
                    // and won't be caught until startup (bad)
                    // TODO:  Define the "config validation" process, and document it.

                    if (duplicateSessionKeyProtection.get(sessionId) != null) {
                        log.error("Two quickfix sessions with teh same SessionID="+sessionId);
                        throw new RuntimeException("Two quickfix sessions with teh same SessionID="+sessionId);
                    }

                    duplicateSessionKeyProtection.put(sessionId, sessionId);
                    this.sessionID = sessionId;
                }

            } catch (Exception e) {
                log.error("Logon failed", e);
            }
        } else {
            for (SessionID sessionId : initiator.getSessions()) {
                Session.lookupSession(sessionId).logon();
            }
        }
    }

    /**
     * called when kill is received by the ems-service (not kill -9) **/
    @Override
    public void stop() {
        initiator.stop();

//        for (SessionID sessionId : initiator.getSessions()) {
//            Session.lookupSession(sessionId).logout("user requested");
//        }
    }

    @Override
    public int submitNewOrder(String symbol, Order order, String clientOrderIdStuffed,
                              boolean isOrderUsingLocates) {
        return this.quickfixSender.sendNewOrderSingle(sessionID, order, clientOrderIdStuffed,symbol, isOrderUsingLocates);
    }

    @Override
    public int cancelOrder(String symbol,
                           long hedgeOsOrderId,
                           int side,
                           long submitTimeUtcMillis,
                           String clientOrderIdStuffed,
                           String originalClientOrderIdStuffed) {
        return quickfixSender.cancelOrder(sessionID, side, submitTimeUtcMillis, clientOrderIdStuffed, originalClientOrderIdStuffed, symbol);
    }

    @Override
    public int cancelReplaceOrder(CancelReplace replace, String symbol, OrderChain chain,
                                  String clientOrderIdStuffed,
                                  String originalClientOrderIdStuffed,
                                  boolean isOrderUsingLocates) {
        return quickfixSender.replaceOrder(replace, sessionID, chain,
                clientOrderIdStuffed, originalClientOrderIdStuffed, symbol,
                isOrderUsingLocates);
    }
}
