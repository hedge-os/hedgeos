package com.hedgeos.fix.adapter;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import com.hedgeos.ems.csvfiles.symbology.SymbolAdapter;
import com.hedgeos.ems.order.EmsExecutionVenue;
import com.hedgeos.ems.service.EmsEnvironmentManager;
import com.hedgeos.ems.util.QuantityConverter;
import com.hedgeos.ems.util.TradingDayUtil;
import com.hedgeos.fix.adapter.recovery.RecoveryUtil;
import environment.EmsEnvironment;
import general.Quantity;
import google.type.Date;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import order.*;
import quickfix.*;
import quickfix.field.*;
import quickfix.field.Side;
import quickfix.fix44.ExecutionReport;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Optional;

import static java.time.ZoneOffset.UTC;

@Slf4j
public class QuickfixMessageAdapter extends MessageCracker
        implements quickfix.Application
{
    private final EmsCallback emsCallback;
    private final SymbolAdapter symbolAdapter;
    private final boolean resetSeqNum;

    // for ResendRequest; we need a sender here.
    private final QuickfixSender quickfixSender;

    // the venue for this Adapter, to populate envelopes
    private final EmsExecutionVenue venue;
    private final EmsEnvironmentManager emsEnvManager;
    private final EmsReportEnvelope envelope;

    // the ems-server ID from EmsServerIdManager; for sanity check
    private final int emsServerId;
    private boolean isDebugMode;
    private volatile SessionID sessionId;
    private volatile boolean resetAndRecover;

    // expected message number is set when we receive "Logout" on recovery; if it is not a normal logout
    // then after we logon; we follow with a Gap Fill message, using the expected message number
    private volatile int expectedMessageNumber;
    private final FixAdapterConfig fixAdapterConfig;


    public QuickfixMessageAdapter(boolean resetSeqNum,
                                  int emsServerId,
                                  EmsEnvironmentManager environmentManager,
                                  EmsCallback cb,
                                  SymbolAdapter symbolAdapter,
                                  QuickfixSender quickfixSender,
                                  FixAdapterConfig fixAdapterConfig,
                                  EmsExecutionVenue venue)
    {
        this.emsServerId = emsServerId;
        this.emsEnvManager = environmentManager;
        this.resetSeqNum = resetSeqNum;
        this.emsCallback = cb;
        this.symbolAdapter = symbolAdapter;
        this.quickfixSender = quickfixSender;
        this.fixAdapterConfig = fixAdapterConfig;
        this.venue = venue;

        FlatBufferBuilder envBuilder = new FlatBufferBuilder();
        int tradeDateOffset = TradingDayUtil.getProtoCurrentTradeDate(envBuilder);
        int venueOffset = venue.asExecutionVenueFlatbuffer(envBuilder);
        int envOffset =
                EmsReportEnvelope.createEmsReportEnvelope(
                        envBuilder,
                        emsEnvManager.getEmsEnvironment(), emsServerId, venueOffset, tradeDateOffset);

        envBuilder.finish(envOffset);
        EmsReportEnvelope envelope =
                EmsReportEnvelope.getRootAsEmsReportEnvelope(envBuilder.dataBuffer());
        this.envelope = envelope;
    }

    // TODO:  Make debug mode populate the Fix session IDs on the response
    public void setDebugMode(boolean debugMode) {
        this.isDebugMode = debugMode;
    }

    @Override
    public void onCreate(SessionID sessionId) {
        log.warn("On create="+sessionId);
        if (sessionId == null) {
            log.error("Created session with NULL sessionId="+sessionId);
            throw new RuntimeException("Can't create a null sessionId");
        }

        if (this.sessionId != null) {
            String msg = "Only one sessionId supported; make a new FIX Adapter for new Sessions.";
            log.error(msg);
            throw new RuntimeException(msg);
        }
        // record the session for use sending back to QuickfixJ engine.
        this.sessionId = sessionId;
    }

    @Override
    public void onLogon(SessionID sessionId) {
        log.warn("On login="+sessionId);
    }

    @Override
    public void onLogout(SessionID sessionId) {
        log.warn("On logout="+sessionId);
    }

    @Override
    public void toAdmin(Message message, SessionID sessionId) {
        MsgType msgType = new MsgType();
        // reset sequence numbers
        try {
            message.getHeader().getField(msgType);
        } catch (FieldNotFound e) {
            log.error("No messageType? "+message);
        }

        if (msgType.getValue().equals(MsgType.HEARTBEAT)) {
            log.info("HEARTBEAT toAdmin-->"+sessionId+", msg="+message);
        }
        else if (msgType.getValue().equals(MsgType.LOGON)) {
            log.warn("fromAdmin: LOGON, session="+sessionId+", message="+message);

            // note;  this will connect you; however, dangerous, as you may lose messagess
            // should only be used in development
            if (resetSeqNum) {
                log.info("Setting reset sequence due to configuration");
                message.setBoolean(ResetSeqNumFlag.FIELD, true);
            }
        }
        else if (msgType.getValue().equals(MsgType.SEQUENCE_RESET)) {
            log.info("Sequence reset being sent; will send a gap fill now");
        }
        else if (msgType.getValue().equals(MsgType.RESEND_REQUEST)) {
            log.warn("Quickfix sending RESEND_REQUEST="+message);
        }
        else {
            log.warn("toAdmin: (NON-HEARTBEAT) sessionId=" + sessionId + ", message=" + message);
        }
    }

    @Override
    public void fromAdmin(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {
        MsgType msgType = new MsgType();
        try {
            message.getField(msgType);
            message.getHeader().getField(msgType);
        } catch (FieldNotFound e) {
            // if not a heartbeat, a real problem.  Quickfix, not seeing 35=0.
            if (!message.toRawString().contains("35=0"))
                log.error("No messageType? "+message);
        }

        if (msgType.getValue().equals(MsgType.HEARTBEAT)) {
            log.info("HEARTBEAT fromAdmin-->"+sessionId+", msg="+message);
        }
        else if (msgType.getValue().equals(MsgType.LOGON)) {
            log.warn("fromAdmin: LOGON, session="+sessionId+", message="+message);

            // we logged on after resetting sequence numbers, now recover all messages from today
            if (resetAndRecover) {
                Runnable r = new Runnable() {
                    @SneakyThrows
                    @Override
                    public void run() {
                        // wait one second, then send a resend
                        Thread.sleep(1000);
                        // sends a 1-0 reset resend.
                        // Note;  THis is working(ish) already
                        quickfixSender.sendResendAllMessages(sessionId);
                    }
                };
                Thread t = new Thread(r);
                t.start();
            }
        }
        else if (msgType.getValue().equals(MsgType.LOGOUT)) {
            handleCatastrophicRecoverySequenceNumberMismatch(message, sessionId);
        }
        else if (msgType.getValue().equals(MsgType.RESEND_REQUEST)) {
            BeginSeqNo beginSeqNo = new BeginSeqNo();
            message.getField(beginSeqNo);
            MsgSeqNum seqNum = new MsgSeqNum();
            message.getField(seqNum);
            // log.error because we want to generate an email when this happens
            log.error("Exchange ACK of Resend Request; it will resend.  " +
                    "BeginSeqNo={}, MsgSeqNum={}", beginSeqNo.getValue(), seqNum.getValue());
        }
        else if (msgType.getValue().equals(MsgType.SEQUENCE_RESET)) {
            log.warn("SEQUENCE RESET received="+message);
        }
        else {
            log.warn("fromAdmin: (NON-HEARTBEAT) sessionId=" + sessionId + ", message=" + message);
        }
    }

    private void handleCatastrophicRecoverySequenceNumberMismatch(Message message, SessionID sessionId) throws FieldNotFound {

        String text = null;
        Text textField = new Text();
        if (message.getField(textField) != null) {
            text = textField.getValue();
        }

        log.warn("fromAdmin: LOGOUT OUT, session="+ sessionId +", message="+ message +", text="+text);

        // TODO:  Make this a pluggable strategy; utility
        // the highest number in the error text is the expected sequence num
        Optional<Integer> possibleExpectedSequenceNumberForRecovery =
                RecoveryUtil.findHighestNumberInTextString(text);

        // this can be empty
        if (possibleExpectedSequenceNumberForRecovery.isEmpty()) {
            log.warn("Normal logout from venue="+fixAdapterConfig.getName());
            return;
        }

        // we fell out of sync with the counterparty;
        // setting this flag will cause us to GapFill after we logon
        resetAndRecover = true;
        expectedMessageNumber = possibleExpectedSequenceNumberForRecovery.get();

        // https://stackoverflow.com/questions/50837890/quickfixj-initiator-manually-resend-reset-to-a-seqnum-at-logon
        log.error("Will login to FixVenue="+this.fixAdapterConfig.getName()+
                    ", with new expected="+expectedMessageNumber+
                    ", parsed from text="+text);

        // try using this as the next sequence number
        // this will connect us; without losing history.
        // Then, because we set 'resetAndRecover'
        // the system will send a GapFill after we are connected (Logon)
        try {
            Session.lookupSession(sessionId)
                    .setNextSenderMsgSeqNum(expectedMessageNumber);
        } catch (IOException e) {
            log.error("VERY BAD; could not set nextSenderMsgSeqNum, " +
                    "tried setting="+expectedMessageNumber+
                    ", name="+fixAdapterConfig.getName());
            throw new Error(e);
        }

    }

    @Override
    public void toApp(Message message, SessionID sessionId) throws DoNotSend {
        // Intentional NO-OP, it is already sent.
        // log.warn("toApp, sessionId="+sessionId+", message-->"+message);
    }

    @Override
    public void fromApp(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        log.debug("fromApp, sessionId="+sessionId+", message-->"+message);
        crack(message, sessionId);
    }

    public void onMessage(quickfix.fix44.BusinessMessageReject reject, SessionID session) {
        // without this the business message rejects go back and forth.
        log.error("Business message REJECT from Exchange, response was not supported, " +
                "message-->"+reject+", session-->"+session);
    }

    public void onMessage(quickfix.fix44.OrderCancelReject message, SessionID sessionId) throws FieldNotFound {
        log.warn("Cancel REJECTED, incoming message-->"+message);
        // push it to the system
        ClOrdID clOrdID = new ClOrdID();
        message.get(clOrdID);

        int emsServerId = (int) ClientOrderIdStuffer.emsServerIdFromClOrdId(clOrdID.getValue());
        // critical for routing, the ems connection to return the message
        long hedgeOsConnectionId = ClientOrderIdStuffer.connectionIdFromClOrdId(clOrdID.getValue());

        // needed to match our orderId tracking for positions, risk.
        long hedgeOsOrderId = ClientOrderIdStuffer.orderIdFromClOrdID(clOrdID.getValue());

        String pmGroup = ClientOrderIdStuffer.pmGroupIdFromClOrdId(clOrdID.getValue());

        OrigClOrdID origClOrdID = new OrigClOrdID();
        message.get(origClOrdID);
        long origHedgeOsOrderId = ClientOrderIdStuffer.orderIdFromClOrdID(origClOrdID.getValue());

        FlatBufferBuilder builder = new FlatBufferBuilder();

        // side is required on Exec Report
        // add the pmGroup to the builder
        int pmGroupOffset = builder.createString(pmGroup);

        EmsReport.startEmsReport(builder);

        EmsReport.addEmsExecType(builder, EmsExecType.REJECT_EXEC_TYPE);
        EmsReport.addOrderStatus(builder, OrderStatus.REJECTED_ORDER_STATUS);

        EmsReport.addHedgeOsEmsServerId(builder, (int) emsServerId);
        EmsReport.addHedgeOsConnectionId(builder, (int) hedgeOsConnectionId);
        EmsReport.addHedgeOsOrderId(builder, hedgeOsOrderId);
        EmsReport.addHedgeOsOriginalOrderId(builder, origHedgeOsOrderId);
        EmsReport.addPmGroup(builder, pmGroupOffset);
        int execReportOffset = EmsReport.endEmsReport(builder);
        builder.finish(execReportOffset);

        EmsReport executionReport = EmsReport.getRootAsEmsReport(builder.dataBuffer());


        // route back to the EMS with the adapted message
        emsCallback.callback(envelope, executionReport);
    }

    public void onMessage(quickfix.fix44.ExecutionReport message, SessionID sessionID)
            throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue {

        // good flatbuffer example on line 191
        // https://github.com/google/flatbuffers/blob/master/java/src/test/java/JavaTest.java
        try {

            ClOrdID clOrdID = new ClOrdID();
            message.get(clOrdID);

            long emsServerId = ClientOrderIdStuffer.emsServerIdFromClOrdId(clOrdID.getValue());
            // sanity check
            if (emsServerId != this.emsServerId) {
                log.error("EMS server, assigned ID="+this.emsServerId+", is receiving messages from a"+
                        ", venue with emsServerId={}", emsServerId);
            }

            // critical for routing, the ems connection to return the message
            long hedgeOsConnectionId = ClientOrderIdStuffer.connectionIdFromClOrdId(clOrdID.getValue());

            // needed to match our orderId tracking for positions, risk.
            long hedgeOsOrderId = ClientOrderIdStuffer.orderIdFromClOrdID(clOrdID.getValue());

            String pmGroup = ClientOrderIdStuffer.pmGroupIdFromClOrdId(clOrdID.getValue());

            // side is required on Exec Report
            Side side = new Side();
            message.get(side);
            int emsSide = EmsFixTranslator.getEmsSide(side.getValue());

            Symbol symbol = new Symbol();
            message.get(symbol);

            Long sid = 0L;
            try {
                sid = symbolAdapter.getSidForSymbol(symbol.getValue());
                if (sid == null) {
                    log.error("No SID found in reverse symbol map for symbol={}, clOrdId={}",
                            symbol.getValue(), clOrdID.getValue());
                }
            } catch (Exception e) {
                log.error("exception getting symbols, VERY BAD, symbol="+symbol.getValue()
                        +", orderId="+hedgeOsOrderId+
                        ", clientOrderId="+clOrdID.getValue(),e);
            }

            // TODO:  Stuffing symbol in the ClOrdId for speed..
//            if (clOrdID.getValue().length() >= INCLUDES_SECURITY_ID) {
//
//            }
//            else {

            // }

            LastQty lastQty = new LastQty();

            FlatBufferBuilder builder = new FlatBufferBuilder();
            // add the pmGroup to the builder
            int pmGroupOffset = builder.createString(pmGroup);

            int totalQtyOffset = getTotalQtyOffsetOrZero(message, builder);
            int lastQtyOffset = getLastQtyOffsetOrZero(message, lastQty, builder);
            int cumQtyOffset = getCumQtyOffsetOrZero(message, builder);
            int leavesQtyOffset = getLeavesQtyOrZero(message, builder);
            int lastPxOffset = getLastPriceOffsetOrZero(message, builder);

            // start making the ExecutionReport flatbuffer to respond with to the client
            // note: Must start the execution report LAST after other objects added
            // to the flatbuffer (quantity, price etc, compound objects.)
            EmsReport.startEmsReport(builder);

            if (message.isSetExecType()) {
                int emsReportType =
                        EmsFixTranslator.getEmsReportTypeForExecReport(message.getExecType().getValue());
                EmsReport.addEmsExecType(builder, emsReportType);
            }

            if (message.isSetOrdStatus()) {
                int orderStatus =
                        EmsFixTranslator.getEmsOrderStatusForOrdStatus(message.getOrdStatus().getValue());
                EmsReport.addOrderStatus(builder, orderStatus);
            }

            if (message.isSetTransactTime()) {
                LocalDateTime localDateTime = message.getTransactTime().getValue();
                // use UTC millis everywhere; clearer API
                ZonedDateTime zdt = localDateTime.atZone(UTC);
                long utcMillis = zdt.toInstant().toEpochMilli();
                EmsReport.addTransactTime(builder, utcMillis);
            }

            EmsReport.addHedgeOsEmsServerId(builder, (int) emsServerId);
            EmsReport.addHedgeOsConnectionId(builder, (int) hedgeOsConnectionId);
            EmsReport.addHedgeOsOrderId(builder, hedgeOsOrderId);

            // if an ExecReport on a cancel, origOrdId will have our original order ID
            if (message.isSetOrigClOrdID()) {
                String origClOrdId = message.getOrigClOrdID().getValue();
                long origHedgeOsOrdId = ClientOrderIdStuffer.orderIdFromClOrdID(origClOrdId);
                EmsReport.addHedgeOsOriginalOrderId(builder, origHedgeOsOrdId);
            }

            EmsReport.addSide(builder, emsSide);
            EmsReport.addPmGroup(builder, pmGroupOffset); // reference the offset
            // sid should always be here.
            if (sid != null) {
                EmsReport.addSid(builder, sid);
            }

            if (totalQtyOffset != 0) {
                EmsReport.addOrderQty(builder, totalQtyOffset);
            }
            // lastQty is how much traded
            if (lastQtyOffset != 0) {
                EmsReport.addLastQty(builder, lastQtyOffset);
            }

            if (leavesQtyOffset != 0) {
                EmsReport.addLeavesQty(builder, leavesQtyOffset);
            }

            if (cumQtyOffset != 0) {
                EmsReport.addCumulativeQty(builder, cumQtyOffset);
            }

            if (lastPxOffset != 0) {
                EmsReport.addLastPrice(builder, lastPxOffset);
            }

            // ExecutionReport.addTotalQty(builder, totalQty);
            // good enough
            // ExecutionReport.addAvgPrice(builder, avgPriceOffset);

            if (isDebugMode) {
                ExecID execID = new ExecID();
                message.get(execID);
                int execIdOffset = builder.createString(execID.getValue());
                int symbolStringOffset = builder.createString(symbol.getValue());
                // ExecutionReport.addEms

                // FIXMessageDebugInfo debugInfo =
                // add the FIX IDs and sequence numbers to the FIX Debug report message
                // ExecutionReport.addFixDebugInfo(builder, debugOffset);
            }

            int execReportOffset = EmsReport.endEmsReport(builder);
            builder.finish(execReportOffset);

            EmsReport executionReport = EmsReport.getRootAsEmsReport(builder.dataBuffer());

            // route back to the EMS with the adapted message
            emsCallback.callback(envelope, executionReport);

        } catch (Exception e) {
            log.error("VERY BAD: Exception handling ExecutionReport, message="+message, e);
            // TODO:  MUST PUT THE MESSAGE ON A KAFKA DLQ, and a File DLQ
        } catch (Error e) {
            log.error("Even worse VERY BAD, Error on Execution Report, message="+message, e);
        }

    }


    private int getTotalQtyOffsetOrZero(ExecutionReport message, FlatBufferBuilder builder) {
        if (false == message.isSetOrderQty()) {
            return 0;
        }

        try {
            double orderQty = message.getOrderQty().getValue();
            Quantity orderQtyEms = QuantityConverter.convertDoubleToQty(orderQty);
            return general.Quantity.createQuantity(builder, orderQtyEms.quantity(), orderQtyEms.nanoQty());

        } catch (FieldNotFound e) {
            log.error("OrderQty set, but then not found. message={}",message);
            return 0;
        }
    }

    private int getLastPriceOffsetOrZero(ExecutionReport message, FlatBufferBuilder builder) throws FieldNotFound {
        LastPx lastPrice = new LastPx();
        if (false == message.isSet(lastPrice)) {
            return 0;
        }

        message.get(lastPrice);

        general.Quantity lastPxRecord = QuantityConverter.convertDoubleToQty(lastPrice.getValue());

        return general.Quantity.createQuantity(builder, lastPxRecord.quantity(), lastPxRecord.nanoQty());
    }

    private static int getLastQtyOffsetOrZero(ExecutionReport message, LastQty lastQty, FlatBufferBuilder builder) throws FieldNotFound {
        int lastQtyOffset = 0;
        if (message.isSet(lastQty)) {
            message.get(lastQty);

            double lastSharesDouble = lastQty.getValue();

            general.Quantity lastQtyEms = QuantityConverter.convertDoubleToQty(lastSharesDouble);
            lastQtyOffset =
                    general.Quantity.createQuantity(builder, lastQtyEms.quantity(), lastQtyEms.nanoQty());

        }
        return lastQtyOffset;
    }

    private static int getLeavesQtyOrZero(ExecutionReport message, FlatBufferBuilder builder) throws FieldNotFound {
        LeavesQty leavesQty = new LeavesQty();
        int leavesQtyOffset = 0;
        if (message.isSet(leavesQty)) {
            message.get(leavesQty);

            double leavesQtyValue = leavesQty.getValue();

            general.Quantity qtyEms = QuantityConverter.convertDoubleToQty(leavesQtyValue);
            leavesQtyOffset =
                    general.Quantity.createQuantity(builder, qtyEms.quantity(), qtyEms.nanoQty());
        }
        return leavesQtyOffset;
    }

    private static int getCumQtyOffsetOrZero(ExecutionReport message, FlatBufferBuilder builder) throws FieldNotFound {
        CumQty cumQty = new CumQty();
        int cumQtyOffset = 0;
        if (message.isSet(cumQty)) {
            message.get(cumQty);
            double cumQtyDouble = cumQty.getValue();
            general.Quantity qtyEms = QuantityConverter.convertDoubleToQty(cumQtyDouble);
            cumQtyOffset =
                    general.Quantity.createQuantity(builder, qtyEms.quantity(), qtyEms.nanoQty());
        }
        return cumQtyOffset;
    }

    public void onMessage(quickfix.fix44.OrderCancelRequest message, SessionID sessionID)
            throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue {

        ClOrdID clOrdID = new ClOrdID();
        message.get(clOrdID);

        // compile time error!! field not defined for OrderCancelRequest
        // ClearingAccount clearingAccount = new ClearingAccount();
        // message.get(clearingAccount);

    }


}
