package com.hedgeos.fix.adapter;

import com.hedgeos.ems.config.beans.EmsConfigFiles;
import com.hedgeos.ems.config.beans.FixAdapterConfig;
import lombok.extern.slf4j.Slf4j;
import quickfix.ConfigError;
import quickfix.SessionSettings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Complain loudly if the Quickfix Settings does not exist.
 */
@Slf4j
public class QuickfixSettingsLoader {
    public static SessionSettings loadQuickfixSettings(EmsConfigFiles rootConfig, FixAdapterConfig config) {
        log.warn("Starting to load FIX Adapter config:" + config.getName());

        String fullQuickfixSettingPath =
                rootConfig.getEMS_CONFIG_FOLDER() + File.separator +
                        config.getQuickfix_adapter_config_file();

        log.warn("Full path to QuickfixJ file=" + fullQuickfixSettingPath);

        File quickfixSetting = new File(fullQuickfixSettingPath);

        // verify a file and non-empty
        if (quickfixSetting.exists() == false && quickfixSetting.length() > 10) {
            log.error("Missing QuickFixJ settings file=" + fullQuickfixSettingPath);
            throw new RuntimeException("Missing QuickFixJ settings file=" + fullQuickfixSettingPath);
        }

        log.warn("Quickfix J path for config:" + fullQuickfixSettingPath);

        // load
        try {
            SessionSettings settings = new SessionSettings(new FileInputStream(quickfixSetting));
            return settings;
        } catch (ConfigError e) {
            String msg = "Config error in QuickfixJ setting=" + config.getName() +
                    ", file=" + config.getQuickfix_adapter_config_file();
            log.error(msg, e);
            throw new RuntimeException(msg, e);
        } catch (FileNotFoundException e) {
            String msg = "Config file not found for Quickfix, connection=" + config.getName() +
                    ", file=" + fullQuickfixSettingPath;
            log.error(msg);
            throw new RuntimeException(msg, e);
        }
    }
}
