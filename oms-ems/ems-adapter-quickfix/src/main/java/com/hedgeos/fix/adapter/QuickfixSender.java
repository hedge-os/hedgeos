package com.hedgeos.fix.adapter;

import com.hedgeos.ems.order.OrderChain;
import lombok.extern.slf4j.Slf4j;
import order.*;
import org.springframework.stereotype.Component;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.SessionNotFound;
import quickfix.field.*;
import quickfix.field.OrdType;
import quickfix.fix44.ResendRequest;

import static com.hedgeos.ems.util.QuantityConverter.priceToDouble;
import static com.hedgeos.fix.adapter.QuickfixTranslator.*;
import static order.OrdType.FOREX;
import static order.Side.SELL_SHORT;
import static order.Side.SELL_SHORT_EXEMPT;

@Component
@Slf4j
public class QuickfixSender {

    private final QuickFixSendConnection connection;

    public QuickfixSender(QuickFixSendConnection conn) {
        this.connection = conn;
    }

    public int sendNewOrderSingle(quickfix.Message message, SessionID sessionID) {
        return connection.sendNewOrderSingle(message, sessionID);
    }


    public int sendCancel(quickfix.Message message, SessionID sessionID) {
        return connection.sendCancel(message, sessionID);
    }


    public int sendReplace(quickfix.Message message, SessionID sessionID) {
        return connection.sendReplace(message, sessionID);
    }


    public int cancelOrder(SessionID sessionId,
                           int side,
                           long submitTimeUtcMillis,
                           String clientOrderIdStuffed,
                           String originalClientOrderIdStuffed,
                           String symbol)
    {

        quickfix.fix44.OrderCancelRequest orderCancelRequest =
                new quickfix.fix44.OrderCancelRequest(new OrigClOrdID(originalClientOrderIdStuffed),
                        new ClOrdID(clientOrderIdStuffed),
                        sideToQuickFixSide(side),
                        transactTimeFromUtcMillis(submitTimeUtcMillis));
        // symbol is required on a cancel; if it was used on the order (which it is)
        orderCancelRequest.set(new Symbol(symbol));
        return sendCancel(orderCancelRequest, sessionId);
    }

    public int replaceOrder(CancelReplace replace,
                            SessionID sessionId,
                            OrderChain order,
                            String clientOrderIdStuffed,
                            String originalClientOrderIdStuffed,
                            String symbol,
                            boolean isOrderUsingLocates) {

        quickfix.fix44.OrderCancelReplaceRequest replaceRequest =
                new quickfix.fix44.OrderCancelReplaceRequest(
                        new OrigClOrdID(originalClientOrderIdStuffed),
                        new ClOrdID(clientOrderIdStuffed),
                        sideToQuickFixSide(order.side()),
                        transactTimeFromUtcMillis(order.submitTimestampMillisUtc()),
                        new OrdType(typeToFIXType(order.orderType())));

        if (replace.quantity().nanoQty() > 0) {
            log.error("NanoQuantity not supported="+replace.hedgeOsOrderId());
        }

        if (replace.limitPrice() != null) {
            double limitPriceDub = priceToDouble(replace.limitPrice());
            replaceRequest.set(new Price(limitPriceDub));
        }

        if (replace.stopPrice() != null) {
            double stopPrice = priceToDouble(replace.stopPrice());
            replaceRequest.set(new StopPx(stopPrice));
        }


        if (isOrderUsingLocates) {
            replaceRequest.set(new LocateReqd(true));
        } else {
            replaceRequest.set(new LocateReqd(false));
        }

        // need OrderQty on a replace
        replaceRequest.set(new OrderQty((replace.quantity().quantity())));
        // symbol is required on a cancel; if it was used on the order (which it is)
        replaceRequest.set(new Symbol(symbol));
        return sendReplace(replaceRequest, sessionId);
    }

    public int sendNewOrderSingle(SessionID sessionId,
                                  Order order,
                                  String clientOrderIdStuffed,
                                  String symbol,
                                  boolean isOrderUsingLocates)
    {
        if (order.orderType() <= 0 || order.orderType() > FOREX) {
            log.error("Invalid OrdType, likely UNSET, orderId={}",order.hedgeOsOrderId());
            return OrderSubmitStatus.MISSING_ORDER_TYPE;
        }

        quickfix.field.OrdType qfOrdType =
                new quickfix.field.OrdType(typeToFIXType(order.orderType()));

        quickfix.fix44.NewOrderSingle newOrderSingle =
                new quickfix.fix44.NewOrderSingle(
                        new ClOrdID(clientOrderIdStuffed),
                        sideToQuickFixSide(order.side()),
                        transactTimeFromUtcMillis(order.submitTimestampMillisUtc()),
                        qfOrdType);

        if (order.quantity() == null) {
            log.error("NULL quantity on orderId="+order.hedgeOsOrderId());
            return OrderSubmitStatus.NULL_QUANTITY;
        }

        if (order.quantity().quantity() == 0 && order.quantity().nanoQty() == 0) {
            log.error("Zero quantity on order={}", order.hedgeOsOrderId());
            return OrderSubmitStatus.ZERO_QUANTITY;
        }

        if (order.quantity().quantity() <= 0) {
            log.error("Negative quantity on order={}", order.hedgeOsOrderId());
            return OrderSubmitStatus.NEGATIVE_QUANTITY;
        }

        // note: discarding nanoQty, for a stock venue
        // TODO: Think about FX trading with this adapter; or another Sender
        if (order.quantity().nanoQty() != 0) {
            log.error("Stock venue does not support nanoQty, orderId={}",order.hedgeOsOrderId());
            return OrderSubmitStatus.NANO_QTY_NOT_AVAILABLE;
        }

        // for Stocks, the entire quantity; we errored earlier if nanoQty was sent to this Venue
        newOrderSingle.set(new OrderQty(order.quantity().quantity()));

        newOrderSingle.set(new Account(order.account().code()));

        newOrderSingle.set(new Symbol(symbol));

        if (isOrderUsingLocates) {
            newOrderSingle.set(new LocateReqd(true));
        } else {
            newOrderSingle.set(new LocateReqd(false));
        }

        // automated execution; probably not necessary
        newOrderSingle.set(new HandlInst('1'));

        if (order.limitPrice() != null) {
            double limitPrice = priceToDouble(order.limitPrice());
            newOrderSingle.set(new Price(limitPrice));
        }

        quickfix.Message quickfixOrder = populateOrder(order, newOrderSingle);

        return sendNewOrderSingle(quickfixOrder, sessionId);
    }

    public quickfix.Message populateOrder(Order order, quickfix.Message newOrderSingle) {

        int type = order.orderType();

        if (type == OrdType.LIMIT) {
            double limitPriceDouble =
                    priceToDouble(order.limitPrice());
            newOrderSingle.setField(new Price(limitPriceDouble));
        }
        else if (type == OrdType.STOP_STOP_LOSS) {
            double stopPriceDouble =
                    priceToDouble(order.stopPrice());
            newOrderSingle.setField(new StopPx(stopPriceDouble));
        } else if (type == OrdType.STOP_LIMIT) {
            double limitPriceDouble =
                    priceToDouble(order.limitPrice());
            double stopPriceDouble =
                    priceToDouble(order.stopPrice());
            newOrderSingle.setField(new Price(limitPriceDouble));
            newOrderSingle.setField(new StopPx(stopPriceDouble));
        }

        if (order.side() == SELL_SHORT
                || order.side() == SELL_SHORT_EXEMPT) {
            newOrderSingle.setField(new LocateReqd(false));
        }

        newOrderSingle.setField(QuickfixTranslator.tifToFIXTif(order.timeInForce()));
        return newOrderSingle;
    }

    public void sendResendAllMessages(SessionID sessionID) {
        log.warn("Sending Resend All messages for sessionID="+sessionID);
        quickfix.fix44.ResendRequest resendRequest =
                new ResendRequest(new BeginSeqNo(1), new EndSeqNo(0));
        connection.sendResendRequest(resendRequest, sessionID);
        log.warn("Sending GAP FILL, sessionID="+sessionID);
    }

    public int sendGapFill(SessionID sessionID, int from, int to) {
        log.warn("Sending GAP FILL for from="+from+", to="+to+", sessionID="+sessionID);
        quickfix.fix44.ResendRequest resendRequest =
                new ResendRequest(new BeginSeqNo(from), new EndSeqNo(to));
        int result = connection.sendResendRequest(resendRequest, sessionID);
        log.warn("Done sending resend request to sessionID="+sessionID+", result="+result);
        return result;
    }

}
