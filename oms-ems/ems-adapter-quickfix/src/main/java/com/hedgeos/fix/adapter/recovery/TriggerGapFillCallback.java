package com.hedgeos.fix.adapter.recovery;

import com.hedgeos.fix.adapter.QuickfixAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.Trigger;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

@Slf4j
public class TriggerGapFillCallback implements CreateModifyCallback {

    private final QuickfixAdapter adapter;

    public TriggerGapFillCallback(QuickfixAdapter adapter) {
        this.adapter = adapter;
    }
    @Override
    public void callback(File f) {
        try {
            log.warn("Start of reading GapFill file="+f.getAbsolutePath());
            List<String> allLines = Files.readAllLines(f.toPath());

            int from = 0;
            int to = 0;
            for (String line : allLines) {
                log.warn("line in GapFill.txt="+line);
                if (line.trim().toLowerCase().contains("from")) {
                    String[] split = line.trim().split("=");
                    from = Integer.parseInt(split[1]);
                }

                if (line.trim().toLowerCase().contains("to")) {
                    String[] split = line.trim().split("=");
                    to = Integer.parseInt(split[1]);
                }
            }
            log.warn("Done reading GapFill.txt, from={}, to={}",from,to);

            if (from == 0 || to == 0) {
                log.error("GapFill.txt created, but either from or to not entered.  from="+from+", to="+to);
            }
            else {
                adapter.sendGapFill(from, to);
            }

            log.warn("Finished reading and sending GapFill for sequence, from="+from+", to="+to);
        } catch (Exception e) {
            log.error("Error reading lines from file="+f.getAbsolutePath(), e);
        }
    }
}
