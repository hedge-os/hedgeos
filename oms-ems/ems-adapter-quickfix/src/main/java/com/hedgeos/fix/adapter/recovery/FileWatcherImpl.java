package com.hedgeos.fix.adapter.recovery;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class FileWatcherImpl implements FileWatch {

    private final ExecutorService es = Executors.newFixedThreadPool(1);

    public static void writeFreshGapFillFile(File newGapFillFile) throws IOException {
        boolean deleted = newGapFillFile.delete();
        log.info("Deleted gapFill file, to replace="+deleted);
        String gapFillContents = "#from=2\n#to=3\n";
        try (FileWriter fileWriter = new FileWriter(newGapFillFile)) {
            fileWriter.write(gapFillContents);
        }
    }

    @Override
    public void watchFile(final Path folder, final String watchFileName,
                          final CreateModifyCallback callback)
    {

        Runnable r = () -> {
            try {
                // Create a watch service
                WatchService watchService = FileSystems.getDefault().newWatchService();

                // Register the folder with the watch service for create and modify events
                folder.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);

                // Start an infinite loop to watch for events
                while (true) {
                    WatchKey key;
                    try {
                        // Wait for an event
                        key = watchService.take();
                    } catch (InterruptedException ex) {
                        return;
                    }

                    // Process the events
                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();

                        // Handle create or modify events
                        if (kind == StandardWatchEventKinds.ENTRY_CREATE ||
                                kind == StandardWatchEventKinds.ENTRY_MODIFY)
                        {
                            // Get the file name of the event
                            WatchEvent<Path> ev = (WatchEvent<Path>) event;
                            Path filePath = folder.resolve(ev.context());

                            // Check if the event file matches the desired file name
                            if (watchFileName.equals(filePath.getFileName().toString())) {
                                // Handle the file creation or modification
                                log.warn("Watched GapFill created or modified: " + filePath);
                                callback.callback(filePath.toFile());
                            } else {
                                // Skip processing for other files
                                continue;
                            }
                        }
                    }

                    // Reset the watch key
                    boolean valid = key.reset();
                    if (!valid) {
                        break;
                    }
                }

                // Close the watch service
                watchService.close();
            }
            catch (Exception e) {
                log.error("Exception trying to watch folder="+folder, e);
                throw new Error("Exception watching folder="+folder, e);
            }
        };

        // start the runnable watching forever
        es.submit(r);
    }
}
