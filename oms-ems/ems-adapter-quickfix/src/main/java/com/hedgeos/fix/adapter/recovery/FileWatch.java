package com.hedgeos.fix.adapter.recovery;

import java.nio.file.Path;

public interface FileWatch {
    void watchFile(Path folder, String fileName, CreateModifyCallback callback);
}