package com.hedgeos.fix.adapter.recovery;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
public class RecoveryUtil {
    public static Optional<Integer> findHighestNumberInTextString(String text) {

        // ignore typical Logout success messages
        if (text == null || text.isEmpty() || text.isBlank() ||
                text.toLowerCase().contains("success") ||
                text.toLowerCase().contains("ack"))
        {
            log.warn("Successful logout message; {}", text);
            return Optional.empty();
        }

        // look for numbers in the text;
        // For example; QuickFix mock exchange:
        // 58=MsgSeqNum too low, expecting 14132 but received 7
        String[] split = text.split(" ");
        List<Integer> numbers = new ArrayList<>();
        for (String textWord : split) {
            try {
                if (textWord.endsWith(",")) {
                    textWord = textWord.substring(0, textWord.length() - 1);
                }
                int seqNumInText = Integer.parseInt(textWord.trim());
                // we found a numeric field; consider it a potential recovery
                // sequence number
                numbers.add(seqNumInText);
            } catch (NumberFormatException nfe) {
                log.debug("Dropping word from logon text="+textWord);
            }
        }

        // if there are no numbers in the Text field; assume this is a normal logout
        if (numbers.isEmpty()) {
            return Optional.empty();
        }

        // we have numbers; get the highest one (14132 above, not 7)
        // 58=MsgSeqNum too low, expecting 14132 but received 7
        Collections.sort(numbers);
        // reverse and grab the first number, which is the highest
        Collections.reverse(numbers);
        // the highest number in the text message is the expected sequence number
        int expectedSeqNum = numbers.get(0);
        return Optional.of(expectedSeqNum);
    }
}
