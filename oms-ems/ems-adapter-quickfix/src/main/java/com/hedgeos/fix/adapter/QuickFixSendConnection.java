package com.hedgeos.fix.adapter;

import quickfix.SessionID;
import quickfix.fix44.ResendRequest;

public interface QuickFixSendConnection {
    int sendNewOrderSingle(quickfix.Message message, SessionID sessionID);

    int sendCancel(quickfix.Message message, SessionID sessionID);

    int sendReplace(quickfix.Message message, SessionID sessionID);

    int sendResendRequest(ResendRequest resendRequest, SessionID sessionID);
}
