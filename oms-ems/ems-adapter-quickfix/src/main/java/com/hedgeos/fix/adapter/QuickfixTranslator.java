package com.hedgeos.fix.adapter;

import lombok.extern.slf4j.Slf4j;
import quickfix.field.OrdType;
import quickfix.field.Side;
import quickfix.field.TimeInForce;
import quickfix.field.TransactTime;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Slf4j
public class QuickfixTranslator {

    public static char typeToFIXType(int hedgeOsOrderType) {
        return switch (hedgeOsOrderType) {
            // TODO:  Can convert the int to a char instead of this switch
            case order.OrdType.MARKET -> OrdType.MARKET;
            case order.OrdType.LIMIT -> OrdType.LIMIT;
            case order.OrdType.STOP -> OrdType.STOP_STOP_LOSS;
            case order.OrdType.STOP_LIMIT -> OrdType.STOP_LIMIT;
            default ->
                throw new RuntimeException("Unknown ordType="+hedgeOsOrderType);
        };
    }


    public static TimeInForce tifToFIXTif(int orderTimeInForce) {
        return switch (orderTimeInForce) {
            case order.TimeInForce.DAY -> new TimeInForce(TimeInForce.DAY);
            case order.TimeInForce.FILL_OR_KILL -> new TimeInForce(TimeInForce.FILL_OR_KILL);
            case order.TimeInForce.GOOD_TILL_DATE -> new TimeInForce(TimeInForce.GOOD_TILL_DATE);
            default -> throw new RuntimeException("No support for timeINForce="+orderTimeInForce);
        };
    }

    public static quickfix.field.Side sideToQuickFixSide(int side) {
        return new Side(EmsFixTranslator.sideToFixSide(side));
    }

    public static TransactTime transactTimeFromUtcMillis(long utcMillis) {
        return new TransactTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(utcMillis), (ZoneOffset.UTC)));
    }
}
