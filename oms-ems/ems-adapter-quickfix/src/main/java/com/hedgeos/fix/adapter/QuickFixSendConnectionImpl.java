package com.hedgeos.fix.adapter;

import lombok.extern.slf4j.Slf4j;
import order.CancelSubmitStatus;
import order.OrderSubmitStatus;
import org.springframework.stereotype.Component;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.SessionNotFound;
import quickfix.fix44.ResendRequest;

@Component
@Slf4j
public class QuickFixSendConnectionImpl implements QuickFixSendConnection {


    @Override
    public int sendNewOrderSingle(quickfix.Message message, SessionID sessionID) {
        if (sessionID == null) {
            return OrderSubmitStatus.NOT_CONNECTED;
        }

        try {
            // odd QuickFix static map of sessions underneath..
            Session session = Session.lookupSession(sessionID);
            if (session.isLoggedOn() == false) {
                log.error("Session is not logged in: "+sessionID);
                return OrderSubmitStatus.SESSION_NOT_LOGGED_IN;
            }

            boolean isSent = Session.sendToTarget(message, sessionID);
            if (isSent == false) {
                return OrderSubmitStatus.FAILED_TO_SEND;
            }

            return OrderSubmitStatus.SUCCESSFUL_ORDER;

        } catch (SessionNotFound e) {
            log.error("No session="+sessionID);
            return OrderSubmitStatus.NO_SESSION;
        }
    }


    @Override
    public int sendCancel(quickfix.Message message, SessionID sessionID) {
        if (sessionID == null) {
            return CancelSubmitStatus.NOT_CONNECTED_CANCEL;
        }

        try {
            // odd QuickFix static map of sessions underneath..
            Session session = Session.lookupSession(sessionID);
            if (session.isLoggedOn() == false) {
                log.error("Session is not logged in: "+sessionID);
                return CancelSubmitStatus.SESSION_NOT_LOGGED_IN_CANCEL;
            }

            boolean isSent = Session.sendToTarget(message, sessionID);
            if (isSent == false) {
                return CancelSubmitStatus.FAILED_TO_SEND_CANCEL;
            }

            return CancelSubmitStatus.CANCEL_SUBMIT_SUCCESS;

        } catch (SessionNotFound e) {
            log.error("No session="+sessionID);
            return CancelSubmitStatus.NO_SESSION_CANCEL;
        }
    }


    @Override
    public int sendReplace(quickfix.Message message, SessionID sessionID) {
        if (sessionID == null) {
            return OrderSubmitStatus.NO_SESSION;
        }

        try {
            // odd QuickFix static map of sessions underneath..
            Session session = Session.lookupSession(sessionID);
            if (session.isLoggedOn() == false) {
                log.error("Session is not logged in: "+sessionID);
                return OrderSubmitStatus.SESSION_NOT_LOGGED_IN;
            }

            boolean isSent = Session.sendToTarget(message, sessionID);
            if (isSent == false) {
                return OrderSubmitStatus.FAILED_TO_SEND;
            }

            return OrderSubmitStatus.SUCCESSFUL_ORDER;

        } catch (SessionNotFound e) {
            log.error("No session="+sessionID);
            return OrderSubmitStatus.NO_SESSION;
        }
    }

    @Override
    public int sendResendRequest(ResendRequest resendRequest, SessionID sessionID) {
        if (sessionID == null) {
            return OrderSubmitStatus.NO_SESSION;
        }

        try {
            Session session = Session.lookupSession(sessionID);
            if (session.isLoggedOn() == false) {
                log.error("Session is not logged on, can it send Resend?");
                return OrderSubmitStatus.SESSION_NOT_LOGGED_IN;
            }

            boolean isSent = Session.sendToTarget(resendRequest, sessionID);
            if (isSent) {
                return OrderSubmitStatus.SUCCESSFUL_ORDER;
            }

            return OrderSubmitStatus.FAILED_TO_SEND;
        }
        catch (SessionNotFound e) {
            log.error("No session="+e);
            return OrderSubmitStatus.NO_SESSION;
        }

    }

}
