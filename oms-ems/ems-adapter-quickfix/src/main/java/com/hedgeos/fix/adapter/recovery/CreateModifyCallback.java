package com.hedgeos.fix.adapter.recovery;

import java.io.File;

public interface CreateModifyCallback {
    void callback(File f);
}
