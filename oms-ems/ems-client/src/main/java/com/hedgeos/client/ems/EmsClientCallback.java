package com.hedgeos.client.ems;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import order.EmsReport;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * This could be used to read EmsReport messages returning from the ems-service
 *  ; however, in most cases, your own callback should be implemented.
 */
@Slf4j
public class EmsClientCallback implements EmsReportCallback {
    public static final int MESSAGE_THRESHOLD = 10_000_000;

    // needs to be thread safe, easy removal at the front
    public final LinkedBlockingDeque<EmsReport> returnedMessages = new LinkedBlockingDeque<>();


    public List<EmsReport> pollUntilEmpty() {
        List<EmsReport> totalMessages = new ArrayList<>();
        List<EmsReport> messages = new ArrayList<>();
        while (true) {
            int messagesReturned = returnedMessages.drainTo(messages);
            if (messagesReturned == 0)
                break;
            totalMessages.addAll(messages);
        }

        return totalMessages;
    }

    @SneakyThrows
    public EmsReport takeFirstEmsReport() {
        return returnedMessages.takeFirst();
    }

    @SneakyThrows
    public EmsReport takeFirstEmsReport(int timeoutSeconds) {
        return returnedMessages.pollFirst(timeoutSeconds, TimeUnit.SECONDS);
    }


    @Override
    public void onEmsReport(EmsReport emsReport) {
        log.info("EMS REPORT received on orderId="+ emsReport.hedgeOsOrderId()+
                ", pmGroup="+ emsReport.pmGroup());

        // add to the back of a Blocking Queue for reading
        returnedMessages.add(emsReport);

        // leave 1000 messages; you should drain these
        if (returnedMessages.size() >= MESSAGE_THRESHOLD) {
            log.error("More than "+MESSAGE_THRESHOLD+" EmsReport messages were waiting in EmsClientCallback");

            log.error("Removing "+MESSAGE_THRESHOLD/2+", messages from queue.");
            for (int i=0; i<(MESSAGE_THRESHOLD/2); i++) {
                returnedMessages.removeFirst();
            }
            log.error("[FINISHED] Removing "+MESSAGE_THRESHOLD/2+", messages from queue.");
        }
    }

    @SneakyThrows
    public void clear() {
        returnedMessages.poll(100, TimeUnit.MILLISECONDS);
        returnedMessages.clear();
    }

    public boolean isEmpty() {
        return returnedMessages.size() == 0;
    }
}
