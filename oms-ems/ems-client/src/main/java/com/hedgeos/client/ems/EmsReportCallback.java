package com.hedgeos.client.ems;

import order.EmsReport;

public interface EmsReportCallback {
    public void onEmsReport(EmsReport emsReport);
}
