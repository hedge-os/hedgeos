package com.hedgeos.client.ems;

import com.google.flatbuffers.FlatBufferBuilder;
import com.hedgeos.ems.order.OrderIdGenerator;
import com.hedgeos.ems.service.EmsEnvironmentManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import order.*;
import org.apache.commons.lang3.StringUtils;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

/**
 * Importantly, we make one EMS client per Ems Server.
 */
@Slf4j
@Getter
public class EmsClient {

    public static final int FAILED_TO_CONNECT_TIME = 10_000;
    public static final int RECONNECT_TIME_CHECK = 5_000;
    private final String emsHost;
    private final int emsPort;
    private final long userId;
    private final String user;
    private final String password;
    private final EmsEnvironmentManager emsEnvManager;

    // request to be connected with the same connectionId as used previously
    // this is used as the preference on the server,
    // ahead of generation a new connectionId for a login
    // it handles the catastrophe of all EMS server side storage lost (intraday), and a new machine started.
    private volatile int stickyConnectionIdRequest = Integer.MIN_VALUE;
    // account Ids you want to trade on this client
    private final List<String> accountIds;

    private EmsServiceGrpc.EmsServiceBlockingStub blockingStub;

    volatile Iterator<EmsReport> statusUpdates;
    private volatile EmsReportCallback callback;
    private volatile Connection connection;

    private volatile Date connectedTime = null;

    // reset the order ID counter if we use 100 billion orders, and been connected 24 hours
    private final OrderIdGenerator orderIdGenerator = new OrderIdGenerator();
    private volatile boolean failedToConnect;

    // safety checks; can be disabled
    final Map<Long, OrderRequest> requestIdsUsed = new ConcurrentHashMap<>();
    final Map<Long, Order> orderIdsUsed = new ConcurrentHashMap<>();


    public EmsClient(String host, int port,
                     long userId, String user, String password,
                     List<String> accountIds)
    {
        this.emsHost = host;
        this.emsPort = port;
        // app specific
        if (userId == 0)
            throw new RuntimeException("UserId should not be 0");
        this.userId = userId;
        if (user == null)
            throw new RuntimeException("User must not be null");
        this.user = user;
        this.password = password;
        // accounts we can trade on
        // TODO:  Verify this matches on connection
        this.accountIds = accountIds;

        this.emsEnvManager = new EmsEnvironmentManager();
    }

    public void networkStart() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(emsHost, emsPort)
                .usePlaintext() // disable SSL (do this in Envoy sidecar or Consul.)
                .build();

        blockingStub = EmsServiceGrpc.newBlockingStub(channel);
        // .withDeadlineAfter(3, TimeUnit.SECONDS);
    }

    public void connect(EmsReportCallback callback)
    {
        if (blockingStub == null) {
            log.error("Must call 'networkStart' before connect");
            throw new Error("Must call 'networkStart' before connect");
        }

        if (isConnected()) {
            log.error("Already connected at:"+connectedTime);
            return;
        }

        FlatBufferBuilder builder = new FlatBufferBuilder();
        int loginOffset = builder.createString(ByteBuffer.wrap(user.getBytes()));
        int passwordOffset = builder.createString(ByteBuffer.wrap(password.getBytes()));

        // TODO: perhaps not send as a comma separated list
        String accounts = StringUtils.join(accountIds, ", ");
        int accountIds = builder.createString(accounts);

        // defaults to workstation; must be set as EMS_ENVIRONMENT variable for actual use.
        int emsEnvironment = emsEnvManager.getEmsEnvironment();

        int offset =
                ConnectRequest.createConnectRequest(
                        builder, userId, loginOffset, passwordOffset,
                        emsEnvironment,
                        stickyConnectionIdRequest,
                        accountIds);

        builder.finish(offset);

        ConnectRequest request =
                ConnectRequest.getRootAsConnectRequest(builder.dataBuffer());

        this.callback = callback;

        // set status updates for later
        connection = blockingStub.connect(request);

        if (connection.status() != ConnectionStatus.SUCCESS) {
            int reason = connection.reason();
            String msg =
                    "Failure to connect, failureReason="+ConnectionStatusReason.name(reason)+", check EMS_ENVIRONMENT="+emsEnvManager.getEnvName()+", and password is correct.";
            log.error(msg);
            throw new Error(msg);
        }

        // keep the connectionId; we will request it on future connection requests.
        stickyConnectionIdRequest = connection.connectionId();

        if (connection.reconnectCount() > 10) {
            log.error("More than 10 reconnects from this login="+request.login());
        }

        connectedTime = new Date();
        orderIdGenerator.setConnectionTime(connectedTime);

        // use the response to subscribe (connection ID)
        this.statusUpdates = blockingStub.subscribe(connection);

        // start a thread for status updates, keep it running
        Runnable r = () -> {
            while (true) {
                try {
                    EmsReport execReport = statusUpdates.next();
                    callback.onEmsReport(execReport);
                }
                catch (Exception e) {
                    log.error("Disconnected from EMS!, will try to reconnect..");
                    // causes us to reconnect in 10 seconds, the "reconnectLoop" method handles this
                    setDisconnected();
                    break;
                }
            }
        };

        // kick off the status updater thread
        Thread t = new Thread(r, "emsClientOrderStatusUpdates");
        t.start();
    }

    // TODO:  May need a lock here
    public boolean isConnected() {
        return connectedTime != null;
    }

    private void setDisconnected() {
        connectedTime = null;
    }

    public Iterator<order.OrderResponse> sendOrderAsync(OrderRequest orderRequest) {
        Iterator<order.OrderResponse> placedStatus = blockingStub.submitOrders(orderRequest);
        return placedStatus;
    }

    public List<order.OrderResponse> sendOrder(OrderRequest orderRequest) {
        log.debug("Sending order count={}, requestId={}",orderRequest.ordersLength(), orderRequest.requestId());
        if (System.getenv("EMS_NO_CHECKS") == null) {
            newIdSafteyCheck(orderRequest);
        }

        Iterator<order.OrderResponse> placedStatus = blockingStub.submitOrders(orderRequest);
        List<order.OrderResponse> statusList = new ArrayList<>();
        // this is blocking; collect them here
        while (placedStatus.hasNext()) {
            order.OrderResponse oneStatus = placedStatus.next();
            statusList.add(oneStatus);
        }
        return statusList;
    }

    private void newIdSafteyCheck(OrderRequest orderRequest) {
        if (requestIdsUsed.get(orderRequest.requestId()) != null)
            throw new RuntimeException("RequestID was already used="+ orderRequest.requestId());
        requestIdsUsed.put(orderRequest.requestId(), orderRequest);
        for (int i = 0; i< orderRequest.ordersLength(); i++) {
            if (orderIdsUsed.get(orderRequest.orders(i).hedgeOsOrderId()) != null) {
                throw new RuntimeException("OrderId was previously used: " + orderRequest.orders(i).hedgeOsOrderId());
            }
            orderIdsUsed.put(orderRequest.orders(i).hedgeOsOrderId(), orderRequest.orders(i));
        }
    }

    public List<CancelResponse> sendCancel(CancelRequest request) {
        log.debug("Sending cancel count={}",request.cancelsLength());
        Iterator<order.CancelResponse> placedStatus = blockingStub.cancelOrders(request);
        List<order.CancelResponse> statusList = new ArrayList<>();
        // this is blocking; collect them here
        while (placedStatus.hasNext()) {
            order.CancelResponse oneStatus = placedStatus.next();
            statusList.add(oneStatus);
        }
        return statusList;
    }

    public List<CancelReplaceResponse> sendReplace(CancelReplaceRequest replaceRequest) {
        log.debug("Sending replace requestId={}, count={}",
                replaceRequest.requestId(),
                replaceRequest.cancelReplacesLength());
        Iterator<order.CancelReplaceResponse> placedStatus
                = blockingStub.cancelReplaceOrders(replaceRequest);
        List<order.CancelReplaceResponse> statusList = new ArrayList<>();
        // this is blocking; collect them here
        while (placedStatus.hasNext()) {
            order.CancelReplaceResponse oneStatus = placedStatus.next();
            statusList.add(oneStatus);
        }
        return statusList;
    }


    public LocateVerifyResponse sendLocateVerify(OrderRequest request) {
        order.LocateVerifyResponse response = blockingStub.locatesVerifyPreTrade(request);
        return response;
    }


    public order.LimitVerifyResponse sendLimitVerify(OrderRequest request) {
        return blockingStub.limitVerifyPreTrade(request);
    }

    @SneakyThrows
    public boolean reconnectLoop(EmsReportCallback callback)
    {
        CountDownLatch connectionAttemptedOnce = new CountDownLatch(1);

        Runnable reconnectRunnable = new Runnable() {
            @Override
            @SneakyThrows
            public void run() {
                long totalChecks = 0;
                while (true) {
                    if (isConnected()) {
                        // log every minute
                        if ( ((totalChecks * FAILED_TO_CONNECT_TIME) % 60_000) == 0) {
                            log.warn("Still connected since=" + connectedTime);
                        }
                        totalChecks ++;

                        Thread.sleep(RECONNECT_TIME_CHECK);
                        continue;
                    }

                    log.warn("Not connected, will reconnect..");
                    try {
                        // connect to the server
                        networkStart();

                        // start the callback
                        connect(callback);

                        log.warn("Connected!");
                        connectionAttemptedOnce.countDown();

                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error("Exception in connect!, will try to reconnect", e);
                        Thread.sleep(FAILED_TO_CONNECT_TIME);
                    }
                    catch (Error e) {
                        log.error("FAILED TO CONNECT, WILL NOT RETRY, error="+e);
                        setFailedConnection();
                        // notify the client can continue
                        connectionAttemptedOnce.countDown();
                        break;
                    }
                }
            }

            
            ;
        };

        Thread reconnectThread = new Thread(reconnectRunnable, "ReconnectToEmsThread");
        reconnectThread.start(); // run forever, trying to reconnect

        log.warn("Waiting for connection one time..");
        connectionAttemptedOnce.await();

        if (failedToConnect) {
            return false;
        }

        return true;
    }

    private void setFailedConnection() {
        this.failedToConnect = true;
    }

    
    public long getNextOrderId() {
        long orderId = orderIdGenerator.getNextHedgeOsOrderId(connection);
        return orderId;
    }

    public Order getOrderForId(long hedgeOsOrderId) {
        return this.orderIdsUsed.get(hedgeOsOrderId);
    }

}
