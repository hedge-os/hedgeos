package com.hedgeos.client.ems;

import lombok.RequiredArgsConstructor;
import order.EmsReport;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@RequiredArgsConstructor
public class CallbackWaitFuture {
    private final EmsClientCallback callback;
    private ExecutorService executor = Executors.newSingleThreadExecutor();
    public Future<EmsReport> waitForNextEmsReport() {
        return executor.submit(() -> {
            EmsReport emsReport = callback.takeFirstEmsReport();
            return emsReport;
        });
    }

}
