export EXTENSIONS_JAR=druid-extensions/target/hedgeos-druid-extensions-1.0-SNAPSHOT.jar
if [[ ! -f $EXTENSIONS_JAR ]] ; then
    echo 'Must first build "hedgeos-druid-extensions-1.0-SNAPSHOT.jar", aborting.'
    exit
fi

mkdir target
cd target
# need Druid 20, expand it for you
tar -xvf ../dist/kafka_2.13-2.6.0.tgz
# Copy my fix to run Kafka on windows
cp kafka_patch_windows/kafka-run-class.bat target/kafka_2.13-2.6.0/bin/windows
tar -xvf ../dist/apache-druid-0.20.0-bin.tar.gz
# need java 8, expand it for you
tar -xvf ../dist/OpenJDK8U-jdk_x64_linux_hotspot_8u275b01.tar.gz
cd ..

echo "now copy the HedgeOs, Druid extensions into Druid extensions dir"

# make a folder for our extensions (druid does one folder per extension)
export HEDGEOS_EXTENSIONS=target/apache-druid-0.20.0/extensions/hedge-os
mkdir -vp $HEDGEOS_EXTENSIONS

# copy our extensions jar into Druid extensions folder
cp -v $EXTENSIONS_JAR $HEDGEOS_EXTENSIONS

echo "copy our own config into Druid, with our extensions dir and settings"
export DRUID_NANO_CONF_FOLDER=target/apache-druid-0.20.0/conf/druid/single-server/nano-quickstart/_common
export OUR_COMMON_PROPS=config/nano-quickstart/common.runtime.properties
cp -v $OUR_COMMON_PROPS $DRUID_NANO_CONF_FOLDER

echo "All set deploying Druid! .. run 'start_druid.sh' "
