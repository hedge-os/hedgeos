package data.input.protobuf;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.os72.protobuf.dynamic.DynamicSchema;
import com.google.common.collect.ImmutableList;
import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.druid.data.input.ByteBufferInputRowParser;
import org.apache.druid.data.input.InputRow;
import org.apache.druid.data.input.MapBasedInputRow;
import org.apache.druid.data.input.impl.ParseSpec;
import org.apache.druid.java.util.common.parsers.ParseException;
import org.apache.druid.java.util.common.parsers.Parser;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Protobuf parsing which assumes flat messages.
 * - Avoids conversion to JSON and then Druid format as the Apache authored one does
 * - but drops the capability for nested messages, which the JSON format conversion handles.
 * - 2x speed improvement (at least), with 1/2 CPU load on the machine due to this parser.
 */
public class ProtobufInputRowParser implements ByteBufferInputRowParser {

    private final ParseSpec parseSpec;
    private final String descriptionFilePath;
    private final String protoMessageType;
    private Descriptors.Descriptor descriptor;
    private Parser<String, Object> parser;
    private final List<String> dimensions;

    private static class ProtoDruidTuple {
        Descriptors.FieldDescriptor fieldDescriptor;
        String druidDimension;

        public ProtoDruidTuple(Descriptors.FieldDescriptor fieldUsed, String druidDim) {
            this.fieldDescriptor = fieldUsed;
            this.druidDimension = druidDim;
        }
    }

    private volatile List<ProtoDruidTuple> fieldDescriptorToDruid;

    @JsonCreator
    public ProtobufInputRowParser(@JsonProperty("parseSpec") ParseSpec sparseSpec,
                                  @JsonProperty("descriptor") String descriptorFilePath,
                                  @JsonProperty("protoMessageType") String protoMessageType) {
        this.parseSpec = sparseSpec;
        this.descriptionFilePath = descriptorFilePath;
        this.protoMessageType = protoMessageType;
        this.dimensions = parseSpec.getDimensionsSpec().getDimensionNames();
    }

    @Override
    public ParseSpec getParseSpec() {
        return parseSpec;
    }

    @Override
    public ProtobufInputRowParser withParseSpec(ParseSpec parseSpec) {
        return new ProtobufInputRowParser(parseSpec, descriptionFilePath, protoMessageType);
    }

    // done lazily to allow druid to boot even if this ingestion spec is broken
    void init() {
        if (this.descriptor == null) {
            this.descriptor = getDescriptor(descriptionFilePath);
        }

        List<ProtoDruidTuple> fieldMapList = new ArrayList<>();

        for (String dimension : dimensions) {
            Descriptors.FieldDescriptor fieldUsed = descriptor.findFieldByName(dimension);
            if (fieldUsed == null) {
                System.err.println("not in proto=" + dimension);
                continue;
            }

            ProtoDruidTuple mapped = new ProtoDruidTuple(fieldUsed, dimension);
            fieldMapList.add(mapped);
        }

        fieldDescriptorToDruid = fieldMapList;
    }

    @Override
    public List<InputRow> parseBatch(ByteBuffer input) {
        if (parser == null) {
            parser = parseSpec.makeParser();
            init();
        }

        Map<String, Object> record = new HashMap<>();

        try {
            DynamicMessage message = DynamicMessage.parseFrom(descriptor, input.array());

            for (ProtoDruidTuple fwd : fieldDescriptorToDruid) {
                try {
                    Object value = message.getField(fwd.fieldDescriptor);
                    record.put(fwd.druidDimension, value);
                } catch (Exception e) {
                    System.err.println("Failed on a field=" + fwd);
                }
            }
        }
        catch (InvalidProtocolBufferException ie) {
            throw new ParseException(ie, "Protobuf failed parse");
        }

        return ImmutableList.of(
                new MapBasedInputRow(parseSpec.getTimestampSpec().extractTimestamp(record),
               dimensions, record));
    }

    private Descriptors.Descriptor getDescriptor(String descriptionFilePath) {
        InputStream fin = this.getClass().getClassLoader().getResourceAsStream(descriptionFilePath);
        if (fin == null) {
            URL url;
            try {
                url = new URL(descriptionFilePath);
            }
            catch (MalformedURLException me) {
                throw new ParseException(me, "Descriptor found in classpath or malformed URL:"+descriptionFilePath);
            }
            try {
                fin = url.openConnection().getInputStream();
            }
            catch (IOException e) {
                throw new ParseException(e, "cannot read descriptor file="+url);
            }
        }

        DynamicSchema dynamicSchema;
        try {
            dynamicSchema = DynamicSchema.parseFrom(fin);
        }
        catch (Exception e) {
            throw new ParseException(e, "Invalid descriptor file="+descriptionFilePath);
        }

        Set<String> messageTypes = dynamicSchema.getMessageTypes();
        if (messageTypes.size() == 0) {
            throw new ParseException("No message types found in="+descriptionFilePath);
        }

        String messageType = protoMessageType == null ? (String) messageTypes.toArray()[0] : protoMessageType;
        Descriptors.Descriptor desc = dynamicSchema.getMessageDescriptor(messageType);
        if (desc == null) {
            throw new ParseException("protobuf format not found in descriptor="+descriptionFilePath);
        }
        return desc;
    }



}
