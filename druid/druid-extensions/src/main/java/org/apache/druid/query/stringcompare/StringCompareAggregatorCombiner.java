package org.apache.druid.query.stringcompare;

import org.apache.druid.query.aggregation.ObjectAggregateCombiner;
import org.apache.druid.segment.ColumnValueSelector;

public class StringCompareAggregatorCombiner extends ObjectAggregateCombiner<String> {

    private String folded;

    private boolean isReset = false;

    @Override
    public void reset(ColumnValueSelector selector) {
        if (selector.isNull())
            folded = null;
        else
            folded = (String) selector.getObject();

        isReset = true;
    }

    @Override
    public void fold(ColumnValueSelector columnValueSelector) {
        if (!isReset) {
            folded = null;
            return;
        }

        if (folded == null) {
            return;
        }

        String temp = (String) columnValueSelector.getObject();
        if (temp == null || columnValueSelector.isNull()) {
            folded = null;
        } else if (!temp.equals(folded)) {
            folded = null;
        }
    }

    @Override
    public String getObject() {
        return folded;
    }

    @Override
    public Class<? extends String> classOfObject() {
        return String.class;
    }
}
