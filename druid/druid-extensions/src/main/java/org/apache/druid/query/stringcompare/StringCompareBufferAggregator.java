package org.apache.druid.query.stringcompare;

import com.metamx.common.StringUtils;
import org.apache.druid.java.util.common.ISE;
import org.apache.druid.query.aggregation.BufferAggregator;
import org.apache.druid.query.aggregation.SerializablePairLongString;
import org.apache.druid.query.monomorphicprocessing.RuntimeShapeInspector;
import org.apache.druid.segment.ColumnValueSelector;

import java.nio.ByteBuffer;

import static org.apache.druid.query.stringcompare.StringCompareAggregatorFactory.MISMATCH_TOKEN;

public class StringCompareBufferAggregator implements BufferAggregator {
    private final ColumnValueSelector timeSelector;
    private final ColumnValueSelector valueSelector;
    private final int maxStringBytes;

    public StringCompareBufferAggregator(ColumnValueSelector timeSelector,
                                         ColumnValueSelector valueSelector,
                                         int maxStringBytes)
    {
        this.timeSelector = timeSelector;
        this.valueSelector = valueSelector;
        this.maxStringBytes = maxStringBytes;

    }

    @Override
    public void init(ByteBuffer byteBuffer, int position) {
        byteBuffer.putLong(position, Long.MIN_VALUE);
        byteBuffer.putInt(position + Long.BYTES, 0);
    }

    @Override
    public void aggregate(ByteBuffer buf, int position) {
        ByteBuffer mutationBuffer = buf.duplicate();
        mutationBuffer.position(position);
        Long existingTimeValue = mutationBuffer.getLong(position);

        Object value = valueSelector.getObject();
        long time = timeSelector.getLong();

        // setting the time is initializing the buffer
        mutationBuffer.putLong(position, time);

        String currentValueExamined = null;
        if (value == null) {
            // value was null, set bufer as empty
            mutationBuffer.putInt(position + Long.BYTES, 0);
            return;
        }
        else {
            if (value instanceof SerializablePairLongString) {
                SerializablePairLongString pair = (SerializablePairLongString) value;
                time = pair.lhs;
                currentValueExamined = pair.rhs;
            }
            else if (value instanceof String) {
                currentValueExamined = (String) value;
            }
            else {
                throw new ISE("don't know how to aggregate this type.");
            }
        }

        // has never been set, set it as null or the value
        if (existingTimeValue == Long.MIN_VALUE) {
            if (currentValueExamined == null) {
                mutationBuffer.putInt(position+Long.BYTES, 0);
                return;
            }
            else {
                byte[] valueBytes = StringUtils.toUtf8(currentValueExamined);
                // buffer format: first push how long the value will be
                mutationBuffer.putInt(position+Long.BYTES, valueBytes.length);
                // next the value itself
                mutationBuffer.position(position + Long.BYTES + Integer.BYTES);
                mutationBuffer.put(valueBytes);
                return;
            }
        }

        CompareAndSetMutationBuffer(position, mutationBuffer, currentValueExamined);
    }


    public static void CompareAndSetMutationBuffer(int position, ByteBuffer mutationBuffer, String currentValueExamined)
    {
        // null current value, go to null everywhere
        if (currentValueExamined == null) {
            mutationBuffer.putInt(position+Long.BYTES, 0);
            return;
        }

        // here means we are examining teh buffer
        int stringSizeBytes = mutationBuffer.getInt(position + Long.BYTES);

        // if we had null before, stay null
        if (stringSizeBytes == 0) {
            return; // nothing to do, it was null, keep going
        }

        mutationBuffer.position(position+Long.BYTES+Integer.BYTES); // move to right after the string size bytes
        byte[] bufferValueArray = new byte[stringSizeBytes];
        mutationBuffer.get(bufferValueArray);

        String bufferValueString = StringUtils.fromUtf8(bufferValueArray);

        // see if the strings match
        // TODO:  Could byte compare, would be faster.
        if (!bufferValueString.equals(currentValueExamined)) {
            // when a mismatch occurs
            // put in null, this way it rolls up to null, which bubbles up as null
            mutationBuffer.putInt(position+Long.BYTES, 0); // set the string size as zero means empty
        }
        // otherwise simply leave the value which is there, in there, keep checking..
    }


    @Override
    public Object get(ByteBuffer buf, int position) {
        ByteBuffer mutationBuffer = buf.duplicate();
        mutationBuffer.position(position);

        Long timeValue = mutationBuffer.getLong(position);
        int stringSizeBytes = mutationBuffer.getInt(position+Long.BYTES);

        if (stringSizeBytes == 0) {
            return MISMATCH_TOKEN;
        }

        SerializablePairLongString pair;
        byte[] valueBytes = new byte[stringSizeBytes];
        mutationBuffer.position(position+Long.BYTES + Integer.BYTES);

        return new SerializablePairLongString(timeValue, StringUtils.fromUtf8(valueBytes));

    }

    @Override
    public float getFloat(ByteBuffer byteBuffer, int i) {
        throw new RuntimeException("Nope.");
    }

    @Override
    public long getLong(ByteBuffer byteBuffer, int i) {
        throw new RuntimeException("Nope.");
    }

    @Override
    public void close() {
        // nothing to do
    }

    @Override
    public void inspectRuntimeShape(RuntimeShapeInspector inspector) {
        inspector.visit("timeSelector", timeSelector);
        inspector.visit("valueSelector", valueSelector);
    }
}
