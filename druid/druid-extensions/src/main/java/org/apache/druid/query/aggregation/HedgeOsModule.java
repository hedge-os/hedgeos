package org.apache.druid.query.aggregation;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.ImmutableList;
import com.google.inject.Binder;
import data.input.protobuf.ProtobufInputRowParser;
import org.apache.druid.initialization.DruidModule;
import org.apache.druid.query.stringcompare.StringCompareAggregatorFactory;

import java.util.List;

public class HedgeOsModule  implements DruidModule {

    @Override
    public void configure(Binder binder) { }

    @Override
    public List<? extends Module> getJacksonModules() {
        return ImmutableList.of(new SimpleModule(getClass().getSimpleName()).registerSubtypes(
                new NamedType(ProtobufInputRowParser.class, "protobuf"),
                new NamedType(StringCompareAggregatorFactory.class, "stringCompare")
        ));
    }
}
