package org.apache.druid.query.stringcompare;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.druid.java.util.common.StringUtils;
import org.apache.druid.query.aggregation.Aggregator;
import org.apache.druid.query.aggregation.BufferAggregator;
import org.apache.druid.query.aggregation.SerializablePairLongString;
import org.apache.druid.segment.BaseObjectColumnValueSelector;
import org.apache.druid.segment.ColumnSelectorFactory;

import java.nio.ByteBuffer;

@JsonTypeName("stringMatchFold")
public class StringCompareFoldingAggregatorFactory extends StringCompareAggregatorFactory {

    public StringCompareFoldingAggregatorFactory(@JsonProperty("name") String name,
                                                 @JsonProperty("fieldName") String fieldName,
                                                 @JsonProperty("maxStringBytes") int maxStringBytes)
    {
        super(name, fieldName, maxStringBytes);
    }

    @Override
    public Aggregator factorize(ColumnSelectorFactory metricFactory) {
        BaseObjectColumnValueSelector selector = metricFactory.makeColumnValueSelector(fieldName);
        return new StringCompareAggregator(null, null, maxStringBytes) {
            @Override
            public void aggregate()
            {
                SerializablePairLongString pair = (SerializablePairLongString) selector.getObject();

                if (pair != null) {
                    if (markerTime == Long.MAX_VALUE) {
                        matchValue = pair.rhs;
                        markerTime = pair.lhs;
                    }
                    else {
                        if (matchValue == null || pair.rhs == null || false == pair.rhs.equals(matchValue)) {
                            markerTime = pair.lhs;
                            matchValue = null;
                        }
                    }
                }
                else {
                    matchValue = null;
                }
            }
        };
    }

    @Override
    public BufferAggregator factorizeBuffered(ColumnSelectorFactory columnSelectorFactory) {
        BaseObjectColumnValueSelector selector = columnSelectorFactory.makeColumnValueSelector(fieldName);

        return new StringCompareBufferAggregator(null, null, maxStringBytes) {
            @Override
            public void aggregate(ByteBuffer buf, int position) {
                SerializablePairLongString pair = (SerializablePairLongString) selector.getObject();

                ByteBuffer mutationBuffer = buf.duplicate();
                mutationBuffer.position(position);

                if (pair != null && pair.lhs != null) {
                    long lastTime = mutationBuffer.getLong(position);

                    // handle boundary condition where first value is null or missing
                    if (lastTime == Long.MIN_VALUE) {
                        if (pair.rhs == null) {
                            mutationBuffer.putLong(position, pair.lhs);
                            mutationBuffer.putInt(position + Long.BYTES, 0);
                            return;
                        }


                        // never set, time to put a value in
                        byte[] valueBytes = StringUtils.toUtf8(pair.rhs);
                        mutationBuffer.putLong(position, pair.lhs);
                        mutationBuffer.putInt(position + Long.BYTES, valueBytes.length);
                        mutationBuffer.position(position + Long.BYTES + Integer.BYTES);
                        mutationBuffer.put(valueBytes);
                        return;

                    }

                    StringCompareBufferAggregator.CompareAndSetMutationBuffer(position, mutationBuffer, pair.rhs);
                }
            }

        };
    }


}
