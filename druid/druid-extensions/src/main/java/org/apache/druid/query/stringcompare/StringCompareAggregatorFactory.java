package org.apache.druid.query.stringcompare;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.druid.query.aggregation.Aggregator;
import org.apache.druid.query.aggregation.AggregatorFactory;
import org.apache.druid.query.aggregation.BufferAggregator;
import org.apache.druid.query.aggregation.SerializablePairLongString;
import org.apache.druid.query.aggregation.first.StringFirstAggregatorFactory;
import org.apache.druid.query.aggregation.first.StringFirstLastUtils;
import org.apache.druid.query.aggregation.last.StringLastAggregator;
import org.apache.druid.query.cache.CacheKeyBuilder;
import org.apache.druid.segment.BaseObjectColumnValueSelector;
import org.apache.druid.segment.ColumnSelectorFactory;
import org.apache.druid.segment.ColumnValueSelector;
import org.apache.druid.segment.column.ColumnHolder;
import org.apache.druid.segment.column.ValueType;

import java.util.*;

@JsonTypeName("stringMatch")
public class StringCompareAggregatorFactory extends AggregatorFactory  {

    public static final SerializablePairLongString MISMATCH_TOKEN =
            new SerializablePairLongString(Long.MAX_VALUE, null);

    final String name;
    final String fieldName;
    final int maxStringBytes;


    @JsonCreator
    public StringCompareAggregatorFactory(@JsonProperty("name") String name,
                                          @JsonProperty("fieldName") final String fieldName,
                                          @JsonProperty("maxStringBytes") Integer maxStringBytes) {
        this.name = name;
        this.fieldName = fieldName;
        this.maxStringBytes = maxStringBytes == null ? StringFirstAggregatorFactory.DEFAULT_MAX_STRING_SIZE : maxStringBytes;
    }

    @Override
    public Aggregator factorize(ColumnSelectorFactory factory) {
        final BaseObjectColumnValueSelector<?> valueSelector = factory.makeColumnValueSelector(fieldName);
        return new StringLastAggregator(factory.makeColumnValueSelector(ColumnHolder.TIME_COLUMN_NAME),
                valueSelector,
                maxStringBytes,
                StringFirstLastUtils.selectorNeedsFoldCheck(valueSelector, factory.getColumnCapabilities(fieldName)));
    }

    @Override
    public BufferAggregator factorizeBuffered(ColumnSelectorFactory factory) {
        ColumnValueSelector selector = factory.makeColumnValueSelector(fieldName);
        return new StringCompareBufferAggregator(factory.makeColumnValueSelector(ColumnHolder.TIME_COLUMN_NAME),
                selector, maxStringBytes);

    }

    @Override
    public Comparator getComparator() {
        return StringFirstAggregatorFactory.VALUE_COMPARATOR;
    }

    @Override
    public Object combine(Object lhs, Object rhs) {
        SerializablePairLongString o1 = (SerializablePairLongString) lhs;
        SerializablePairLongString o2 = (SerializablePairLongString) rhs;
        if (o1.rhs == null || o2.rhs == null) {
            return MISMATCH_TOKEN;
        }
        else if (o1.rhs.equals(o2.rhs) == false) {
            return MISMATCH_TOKEN;
        }

        // match
        return o1;
    }

    @Override
    public AggregatorFactory getCombiningFactory() {
        return new StringCompareFoldingAggregatorFactory(name, name, maxStringBytes);
    }

    @Override
    public List<AggregatorFactory> getRequiredColumns() {
        return Collections.singletonList(new StringCompareAggregatorFactory(fieldName, fieldName, maxStringBytes));
    }

    @Override
    public Object deserialize(Object o) {
        Map map = (Map) o;
        return new SerializablePairLongString(((Number) map.get("lhs")).longValue(), ((String)map.get("rhs")));
    }

    @Override
    public Object finalizeComputation(Object o) {
        return ((SerializablePairLongString) o).rhs;
    }

    @Override
    @JsonProperty
    public String getName() {
        return name;
    }

    @Override
    public List<String> requiredFields() {
        return Arrays.asList(ColumnHolder.TIME_COLUMN_NAME, fieldName);
    }

    @Override
    public ValueType getType() {
        return ValueType.STRING;
    }

    @Override
    public ValueType getFinalizedType() {
        return ValueType.STRING;
    }

    @Override
    public int getMaxIntermediateSize() {
        return Long.BYTES + Integer.BYTES + maxStringBytes;
    }

    @Override
    public byte[] getCacheKey() {
        return new CacheKeyBuilder((byte) 0x33).appendString(fieldName).appendInt(maxStringBytes).build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringCompareAggregatorFactory that = (StringCompareAggregatorFactory) o;
        return maxStringBytes == that.maxStringBytes &&
                name.equals(that.name) &&
                fieldName.equals(that.fieldName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, fieldName, maxStringBytes);
    }

    @Override
    public String toString() {
        return "StringMatchAggregatorFactory {" +
                "name='" + name + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", maxStringBytes=" + maxStringBytes +
                '}';
    }
}
