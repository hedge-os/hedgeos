package org.apache.druid.common;

import javax.annotation.Nullable;
import java.util.Objects;

public class Triplet<T1, T2, T3>
{
    public static<T1, T2, T3> Triplet<T1, T2, T3> of (@Nullable T1 lhs, @Nullable T2 rhs, @Nullable T3 three) {
        return new Triplet<>(lhs, rhs, three);
    }

    @Nullable
    public final T1 lhs;
    @Nullable
    public final T2 rhs;
    @Nullable
    public final T3 three;

    public Triplet(@Nullable T1 lhs, @Nullable T2 rhs, @Nullable T3 three) {
        this.lhs = lhs;
        this.rhs = rhs;
        this.three = three;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Triplet))
            return false;

        Triplet t = (Triplet) o;
        return Objects.equals(lhs, t.lhs) && Objects.equals(rhs, t.rhs) && Objects.equals(three, t.three);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lhs, rhs, three);
    }

    @Override
    public String toString() {
        return "Triplet{" +
                "lhs=" + lhs +
                ", rhs=" + rhs +
                ", three=" + three +
                '}';
    }
}
