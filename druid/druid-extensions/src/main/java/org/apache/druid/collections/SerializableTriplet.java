package org.apache.druid.collections;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.druid.common.Triplet;

public class SerializableTriplet<T1, T2, T3> extends Triplet<T1, T2, T3> {
    @JsonCreator
    public SerializableTriplet(@JsonProperty("lhs") T1 lhs, @JsonProperty("rhs") T2 rhs, @JsonProperty("three") T3 three)
    {
        super(lhs, rhs, three);
    }

    @JsonProperty public T1 getLhs() { return lhs; }

    @JsonProperty public T2 getRhs() { return rhs; }

    @JsonProperty public T3 getThree() { return three; }
}
