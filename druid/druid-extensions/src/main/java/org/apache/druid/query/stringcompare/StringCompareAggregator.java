package org.apache.druid.query.stringcompare;

import org.apache.druid.java.util.common.ISE;
import org.apache.druid.query.aggregation.Aggregator;
import org.apache.druid.query.aggregation.SerializablePairLongString;
import org.apache.druid.segment.BaseLongColumnValueSelector;
import org.apache.druid.segment.BaseObjectColumnValueSelector;

/**
 * String match aggregator returns the value for the column, only if every row has the same value for this column.
 * If they don't match, then don't roll them up.
 * e.g. if you have a portfolio of IBM and AMZN, you don't want to show "Symbol" field as either one of these
 * in the event you have an aggregation across IBM and AMZN, like "marketValue", you can add this, but it is not for
 * any symbol, it is across two symbols, therefore eliminate the symbol field as it is now invalid.
 */
public class StringCompareAggregator implements Aggregator {

    private final BaseObjectColumnValueSelector valueSelector;
    private final BaseLongColumnValueSelector timeSelector;

    protected long markerTime;
    protected String matchValue;
    protected boolean init;

    public StringCompareAggregator(
            BaseLongColumnValueSelector timeSelector,
            BaseObjectColumnValueSelector valueSelector,
            int maxStringBytes) {
        this.valueSelector = valueSelector;
        this.timeSelector = timeSelector;

        // min value means never assigned
        markerTime = Long.MIN_VALUE;
        matchValue = null;
        init = false;
        // todo:  honor max bytes
    }

    @Override
    public void aggregate() {
        if (!init) {
            matchValue = (String) this.valueSelector.getObject();
            markerTime = timeSelector.getLong();
            if (markerTime == Long.MIN_VALUE) {
                markerTime = System.nanoTime();
            }

            init = true;
            return;
        }

        if (init && matchValue == null) {
            return;  // we've been here before, failed to match already
        }

        Object value = valueSelector.getObject();

        if (value == null) {
            // we found a null, which turns the result into null
            matchValue = null;
            // happened here
            markerTime = timeSelector.getLong();
            return;
        }

        String currentValue = null;

        if (value instanceof String) {
            currentValue = (String) value;
        } else if (value instanceof SerializablePairLongString) {
            // if it is a pair, take the right hand side
            currentValue = ((SerializablePairLongString) value).rhs;
        } else {
            throw new ISE("Can't aggregate this kind of class=" + value.getClass().getCanonicalName());
        }

        // do the comparison
        if (matchValue.equals(currentValue)) {
            return; // good we matched, keep the current value of matchValue, bubbling up
        } else {
            // we failed, fail the comparison for this column of the query to null
            matchValue = null;
            // time of the value we failed at for reference
            markerTime = timeSelector.getLong();
            if (markerTime == Long.MIN_VALUE)
                markerTime = System.nanoTime(); // handle lack of time, shoould not occur
        }
    }

    @Override
    public Object get() {
        return new SerializablePairLongString(markerTime, matchValue);
    }

    @Override
    public float getFloat() {
        throw new UnsupportedOperationException("String match doesn't handle getFloat()");
    }

    @Override
    public double getDouble() {
        throw new UnsupportedOperationException("not a double");
    }

    @Override
    public boolean isNull() {
        return matchValue == null;
    }

    @Override
    public long getLong() {
        throw new UnsupportedOperationException("no get long on strings");
    }

    @Override
    public void close() {}
}