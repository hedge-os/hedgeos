create table security_type
(
    security_type_id serial not null
        constraint security_type_pk
            primary key,
    name varchar(200) not null,
    table_name varchar(100) not null,
    market_sector varchar(50)
);

alter table security_type owner to hedgeos;

create table security
(
    sid bigserial not null
        constraint security_pk
            primary key,
    security_type_id integer not null
        constraint security_type_id_fk
            references security_type,
    security_sub_type integer,
    name varchar(200),
    bb_ticker varchar(100),
    figi varchar(100),
    isin varchar(12),
    cusip varchar(9),
    sedol varchar(7),
    ric varchar(50),
    occ_code varchar(21),
    primary_exchange_mic varchar(5),
    primary_exchange_ticker varchar(50),
    description varchar(200),
    start_trading_date date,
    death_date date
);

comment on column security.isin is 'isins are always 12 chars';

comment on column security.cusip is 'cusips are always 9 chars';

comment on column security.sedol is 'stock exchange daily official list, LSE, UK and Ireland';

comment on column security.ric is 'Reuters Information Code';

comment on column security.occ_code is 'Options Clearing corp code in the US for Options, always 21 chars';

alter table security owner to hedgeos;

create unique index security_sid_uindex
    on security (sid);

create unique index security_type_description_uindex
    on security_type (name);

create unique index security_type_id_uindex
    on security_type (security_type_id);

create unique index security_type_table_name_uindex
    on security_type (table_name);

create table equity
(
    sid bigint not null
        constraint equity_pk
            primary key
        constraint equity_security_sid_fk
            references security,
    is_etf boolean,
    is_etn integer,
    original_sid bigint
        constraint equity_original_sid_fk
            references security,
    adjustment_factor double precision,
    creation_reason_code varchar(10),
    death_reason_code varchar(10),
    primary_country_iso varchar(10),
    parent_company_id bigint,
    parent_company_country_iso varchar(10)
);

alter table equity owner to hedgeos;

create table equity_option
(
    sid bigint
        constraint equity_option_security_sid_fk
            references security
        constraint equity_option_und_equity_sid_fk
            references security,
    und_equity_sid bigint,
    expiration_date date,
    strike double precision,
    call_put_flag varchar(1)
);

alter table equity_option owner to hedgeos;

create table equity_index_option
(
    sid bigint not null
        constraint equity_index_option_pk
            primary key
        constraint equity_index_option_security_sid_fk
            references security,
    und_index_sid bigint not null
        constraint equity_index_option_und_sid_fk
            references security,
    expiration_date date not null,
    strike double precision,
    call_put_flag varchar(1) not null
);

alter table equity_index_option owner to hedgeos;

create unique index equity_index_option_sid_uindex
    on equity_index_option (sid);

create table equity_right
(
    sid bigint not null
        constraint equity_right_pk
            primary key
        constraint equity_right_security_sid_fk
            references security,
    und_equity_sid bigint not null
        constraint equity_right_und_sid_fk
            references security,
    expiration_date date,
    ratio double precision
);

alter table equity_right owner to hedgeos;

create unique index equity_right_sid_uindex
    on equity_right (sid);

create table equity_warrant
(
    sid bigint not null
        constraint equity_warrant_pk
            primary key
        constraint equity_warrant_security_sid_fk
            references security,
    und_equity_sid bigint not null
        constraint equity_warrant_und_sid_fk
            references security,
    premium double precision,
    gearing double precision,
    expiration_date date
);

alter table equity_warrant owner to hedgeos;

create unique index equity_warrant_sid_uindex
    on equity_warrant (sid);

create table index
(
    sid bigint not null
        constraint index_pk
            primary key
        constraint index_security_sid_fk
            references security,
    index_vendor varchar(50),
    name varchar(100),
    index_code varchar(50),
    description varchar(200)
);

alter table index owner to hedgeos;

create unique index index_sid_uindex
    on index (sid);

create table future_chain
(
    future_chain_code varchar(5) not null,
    name varchar(100) not null,
    future_type_code varchar(5),
    description varchar(200),
    inception_date date,
    death_date date
);

comment on column future_chain.future_type_code is 'STIR,BOND, etc';

alter table future_chain owner to hedgeos;

create unique index future_chain_future_chain_code_uindex
    on future_chain (future_chain_code);

create unique index future_chain_name_uindex
    on future_chain (name);

create table future
(
    sid bigint not null
        constraint future_pk
            primary key
        constraint future_security_sid_fk
            references security,
    index_sid bigint
        constraint future_index_sid_security_fk
            references security,
    future_chain_code varchar(5) not null
        constraint future_chain_code_fk
            references future_chain (future_chain_code),
    expiration_date date,
    first_delivery_date date,
    last_delivery_date date,
    start_accrual_date date,
    alias_ticker varchar(50)
);

comment on column future.alias_ticker is 'midcurves like eurodollar future or options';

alter table future owner to hedgeos;

create unique index future_sid_uindex
    on future (sid);

create table future_option
(
    sid bigint
        constraint future_option_sid_fk
            references security,
    und_future_sid bigint
        constraint future_option_future_fk
            references future,
    strike double precision,
    expiration_date date
);

alter table future_option owner to hedgeos;

create table payment_schedule
(
    payment_schedule_id serial not null
        constraint payment_schedule_pk
            primary key,
    parent_sid bigint
        constraint payment_schedule_parent_sid_fk
            references security,
    payment_date date not null,
    payment_amount double precision,
    payment_rate double precision,
    payment_notional double precision
);

alter table payment_schedule owner to hedgeos;

create table equity_swap
(
    sid bigint not null
        constraint equity_swap_sid_fk
            references security,
    parent_equity_sid bigint not null
        constraint equity_swap_parent_sid_fk
            references equity,
    financing_rate double precision,
    financing_index_sid integer
        constraint equity_swap_financing_index_sid_fk
            references index,
    financing_spread_rate double precision,
    maturity_date date,
    tenor varchar(2),
    payment_schedule_id integer
        constraint equity_swap_payment_schedule_fk
            references payment_schedule
);

alter table equity_swap owner to hedgeos;

create unique index equity_swap_sid_uindex
    on equity_swap (sid);

create table bond
(
    sid bigserial not null
        constraint bond_pk
            primary key
        constraint bond_sid_fk
            references security,
    company_id integer,
    rate double precision,
    tenor varchar(5),
    payment_schedule_id integer
        constraint bond_payment_schedule_fk
            references payment_schedule,
    coupon_period_tenor varchar(5),
    start_accrual_date date,
    maturity_date date,
    callable_or_puttable_flag varchar(1),
    cp_schedule_id integer
        constraint bond_cp_schedule_id_fk
            references payment_schedule,
    sink_or_float_flag varchar(1),
    sf_schedule_id integer
        constraint bond_sf_schedule_fk
            references payment_schedule
);

alter table bond owner to hedgeos;

create table call_put_schedule
(
    cp_schedule_id bigserial not null
        constraint call_put_schedule_pk
            primary key
        constraint call_put_schedule_parent_sid_fk
            references security,
    parent_sid bigint not null,
    event_date date not null,
    quantity double precision not null
);

alter table call_put_schedule owner to hedgeos;

create table sink_float_schedule
(
    sf_schedule_id serial not null
        constraint sink_float_schedule_pk
            primary key,
    parent_sid integer
        constraint sink_float_schedule_parent_sid_fk
            references security,
    event_date date not null,
    quantity double precision not null,
    sink_float_flag varchar(1) not null
);

alter table sink_float_schedule owner to hedgeos;

create table company
(
    company_id bigserial not null
        constraint company_pk
            primary key,
    parent_company_id bigint
        constraint company_parent_id_fk
            references company,
    name varchar(300),
    entity_type varchar(100),
    country_domiciled_iso varchar(5),
    city varchar(200),
    state varchar(200),
    zip_code varchar(15),
    province varchar(200),
    spinoff_parent_id bigint
);

alter table company owner to hedgeos;

create table currency
(
    sid bigserial not null
        constraint currency_pk
            primary key
        constraint currency_sid_fk
            references security,
    code_iso varchar(5) not null,
    name varchar(100) not null
);

alter table currency owner to hedgeos;

create table currency_pair
(
    sid bigint not null
        constraint currency_pair_pk
            primary key
        constraint currency_sid_fk
            references currency,
    pair_code varchar(5),
    name varchar(100),
    base_code varchar(5),
    quote_code varchar(5),
    base_sid bigint,
    quote_sid bigint
        constraint currency_quote_sid_fk
            references currency
);

alter table currency_pair owner to hedgeos;

create unique index currency_sid_uindex
    on currency (sid);

create table day_count_convention
(
    code varchar(10) not null
        constraint day_count_convention_pk
            primary key,
    long_name varchar(50)
);

alter table day_count_convention owner to hedgeos;

create table forward_rate_agreement
(
    sid bigserial not null
        constraint forward_rate_agreement_pk
            primary key
        constraint forward_rate_agreement_sid_fk
            references security,
    notional_amount double precision,
    fixed_rate double precision,
    reference_index_sid bigint
        constraint forward_rate_agreement_ref_sid_fk
            references index,
    reference_rate_spread double precision,
    tenor varchar(5),
    day_count_convention_code varchar(7)
        constraint forward_rate_agreement_day_count_fk
            references day_count_convention,
    termination_date date
);

alter table forward_rate_agreement owner to hedgeos;

create unique index forward_rate_agreement_sid_uindex
    on forward_rate_agreement (sid);

create table fx_forward
(
    sid bigint not null
        constraint fx_forward_pk
            primary key
        constraint fx_forward_sid_fk
            references security,
    expiration_date date not null,
    und_currency_sid bigint not null
        constraint fx_forward_und_cp_sid__fk
            references currency_pair,
    price double precision not null
);

alter table fx_forward owner to hedgeos;

create table interest_rate_swap
(
    sid bigint not null
        constraint interest_rate_swap_pk
            primary key
        constraint interest_rate_swap_sid_fk
            references security,
    leg1_fixed_float_flag varchar(10),
    leg2_fixed_float_flag varchar(10),
    leg1_index_sid bigint,
    leg2_index_sid bigint,
    leg1_rate double precision,
    leg2_rate double precision,
    leg1_counterparty_code varchar(5),
    leg2_counterparty_code varchar(5),
    clearing_broker_code varchar(5),
    column_11 integer,
    exec_broker_code varchar(5),
    cleared_type_flag varchar(1),
    clearing_exchange_mic varchar(5)
);

alter table interest_rate_swap owner to hedgeos;

