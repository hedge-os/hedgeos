-- TSLA and MSFT as base table security
INSERT INTO public.security (sid, security_type_id, security_sub_type, name, bb_ticker, figi, isin, cusip, sedol, ric, occ_code, primary_exchange_mic, primary_exchange_ticker, description, start_trading_date, death_date) VALUES (2, 14, null, 'Tesla Inc.', 'TSLA', 'BBG000N9MNX3', 'US88160R1014', '88160R101', null, 'TSLA.N', null, 'XNAS', 'TSLA', 'Tesla Motors Inc.', null, null);
INSERT INTO public.security (sid, security_type_id, security_sub_type, name, bb_ticker, figi, isin, cusip, sedol, ric, occ_code, primary_exchange_mic, primary_exchange_ticker, description, start_trading_date, death_date) VALUES (3, 14, null, 'Microsoft Corp', 'MSFT', 'BBG000BPH459', 'US5949181045', '594918104', null, 'MSFT.N', null, 'XNAS', 'MSFT', 'Microsoft Inc.', null, null);

-- TSLA and MSFT as equity table security
INSERT INTO public.equity (sid, is_etf, is_etn, original_sid, adjustment_factor, creation_reason_code, death_reason_code, primary_country_iso, parent_company_id, parent_company_country_iso) VALUES (2, false, null, null, null, null, null, 'US', null, null);
INSERT INTO public.equity (sid, is_etf, is_etn, original_sid, adjustment_factor, creation_reason_code, death_reason_code, primary_country_iso, parent_company_id, parent_company_country_iso) VALUES (3, false, null, null, null, null, null, 'US', null, null);
