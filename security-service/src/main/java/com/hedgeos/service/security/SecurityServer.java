package com.hedgeos.service.security;

import com.hedgeos.key.SecurityKey;
import com.hedgeos.security.Security;
import com.hedgeos.security.SecurityOrBuilder;
import io.grpc.stub.StreamObserver;
import org.springframework.stereotype.Component;

import static com.hedgeos.api.Ports.FAKE_SEC_KEY;
import static com.hedgeos.api.Ports.FAKE_SWAP_NAME;

@Component
public class SecurityServer extends SecurityServiceGrpc.SecurityServiceImplBase {

    @Override
    public void getSecurity(SecurityRequest request, StreamObserver<SecurityResponse> responseObserver) {
        if (request.getSecurityKey(0).getName().equals(FAKE_SWAP_NAME)) {
            Security fakeSwap =
                    Security.newBuilder()
                        .setSecurityKey(FAKE_SEC_KEY)
                        .build();

            SecurityResponse resp =
                    SecurityResponse.newBuilder().addSecurities(fakeSwap).build();

            responseObserver.onNext(resp);
            responseObserver.onCompleted();
            return;
        }

        throw new RuntimeException("Must implement getSecurity");
    }

    @Override
    public void securitySubscription(SecurityRequest request, StreamObserver<SecurityResponse> responseObserver) {
        super.securitySubscription(request, responseObserver);
    }
}
