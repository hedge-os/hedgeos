package com.hedgeos.service.security;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class SecurityServiceApplication implements CommandLineRunner {

    @Autowired
    SecurityGrpc securityGrpc;

    @Autowired
    ApplicationContext applicationContext;

    public void run(String... args) throws Exception {

        securityGrpc.startGrpc();

    }

    public static void main(String[] args) {
        SpringApplication.run(SecurityServiceApplication.class, args);
    }

}
