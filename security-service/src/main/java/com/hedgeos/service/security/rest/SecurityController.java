package com.hedgeos.service.security.rest;

import com.hedgeos.service.security.domain.EquityJpa;
import com.hedgeos.service.security.domain.SecurityJpa;
import com.hedgeos.service.security.repository.EquityRepository;
import com.hedgeos.service.security.repository.SecurityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RequiredArgsConstructor
@RestController
@RequestMapping("/security")
public class SecurityController {

    final SecurityRepository securityRepository;
    final EquityRepository equityRepository;

    @GetMapping("/msft")
    public SecurityJpa  msft() {
        SecurityJpa security = securityRepository.findBySid(3);
        return security;
    }

    @GetMapping("/msfteq")
    public EquityJpa  msfteq() {
        EquityJpa equityJpa = equityRepository.findBySid(3);
        return equityJpa;
    }


    @GetMapping("/test")
    public String test() {
        return new Date().toString();
    }

    @GetMapping("/bbg")
    public String bbg() {
        return "todo";
    }
}
