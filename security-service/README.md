Security Service
====

--------------
Primarily an in-memory, gRPC request response and streaming
interface, using, **security.proto**

Also, can be exported into other databases, via replication.

------------

- TODO:  Surfaces, made of points
- TODO:  Rates table, for generic, swap rates?
- TODO:  FX Options table and ODavey Notes
- TODO:  Pull down some Open Figi data from Open Figi
-----------


### Background Info
- Open Figi files, are free from bloomberg, if you buy **any** back office files, they drop them every day
- Then, you may create, the entire listed universe, by loading the files every night
  - this technique, is extremely useful for booking and trading
- there is also an open figi api available at openfigi.com
  - openfigi.com, via REST (http web calls) will do 250 requests per 12 seconds (free version), and has a bunch of types to download and put in the database (this is only a file to a database)

### Bi-temporality
- every security  master entry (really everything) will have
  - **valid_from** column
  - **valid_till** column
  - (and perhaps, version)
- Valid Till = null, means forever
- Version means what version of the fact it is (if necessary, nice to have)
- May want, but likely not:  
  - *Entered* column
  - *Superseded* column
- Above times, are UTC timestamps
  - Everything in the database, is UTC as a rule

### Audit
- every row will also have
    - created_time
    - updated_time
    - create_user
    - update_user
- and if rows are replaced
  - they will be moved to audit tables
  - but we will most likely not replace security master rows

#### Notation

- ***Italics*** means a **key**, either primary or foreign  
- **bold** is used for emphasis

---
Tables
---

*The one main table is SECURITY*

The security table, has a sid = security_id
(and sid sounds better than iid, or cid)

e.g. IBM is an equity, which has a sid.
The sid of IBM is 60002, 
and this is also the sid which is on the equity table.

e.g. If you have an equity option, it has one sid on it,
which is in the instrument table, 
and on the equity option table, as the sid.

---
Security Type
--- 

The **security_type** table,  has a security_type_id.
This defines which additional table is married with the sid, 
for additional security specific info.

- **security_type**
  - *security_type_id*
    - defines the security type
  - description (Common Stock)
  - table_name (equity)
    - what child table to read
  - market_sector (Equity)

--- 
Security
---

**security** table, is the root of everything.

_Any_ entry in any other table, e.g. equity, future,
also has an entry in the **security** table, with a matching **sid**

The two tables, the more specific info, along with any generic info on the security table, are married on
**"sid"** (security id) which is identical (shared), across the two tables.
This is enforced by foreign key relationships, and also by the software.

We keep one table with counters in this way, the security id, but maintain tables 
for the more specific security information, on its own table.

Advantages of this approach:
1. this way the basic IDs (symbology), are on the bottom table, for ease of use.
1. symbols are what is most used, for trading and booking
1. it makes it obvious to listed people, what is happening here
1. very well organized  

#### Security Table
  
- **security**
    - ***sid***  (123 = IBM)
      - (security id, on every child table as well)
    - ***security_type_id*** (1 = equities)
      - from security_type table
      - future, equity, etc, tells you which child table to read for more specific info about the security
    - ***security_sub_type_id*** 
      - (bond future, vs, stir future)
    - name (International Business Machines)
    - currency (USD, from the ref_data list)
    - bb_ticker (IBM US Equity)
    - figi (BBG004BWKQK6) 
      - note: FIGI is open source, we will be based on this
      - https://www.openfigi.com/id/BBG004BWKQK6
    - isin (global, commercial)
    - cusip (north america, american bankers association)
    - sedol (lse, uk and ireland)
    - ric (reuters information code, e.g. IBM.N)
    - occ_code (options clearing corp)
    - primary_exchange_mic (ISO for market, e.g.: XNYS)
    - primary_exchange_mic (XNYS)
    - primary_exchange_ticker (IBM)
    - description (anything else, like notes)
    - start_trading_date (1980)
    - death_date (null)
    

----
Listed
----

- **equity**
    - *sid* (equity is an instrument)
    - is_etf_flag
    - is_etn_flag (like ETFs but not really)
    - original_equity_sid (when something was spun off, what from)
    - adjustment_factor (how much of the parent is it worth)
    - creation_reason_code (ipo, spinoff)
    - death_reason_code (merger, acquisition, why the security died)
    - primary_country_iso
    - *parent_company_id*
      - equity and debt, need a *company* table which is helpful for debt markets etc
    - parent_country_country_iso

- **equity_option**
    - *sid* (equity option is an security)
    - *und_equity_sid* (the sid of the underlying)
    - TODO:  SPX Weeklies
    - expiration_date
    - strike
    - call_put_flag

TODO:  Deserves a table or not?
    
- **equity_index_option**
    - *sid* (this is a security)
    - *und_index_sid* (underlying index sid)
    - expiration_date
    - strike
    - call_put_flag

TODO:  Should these, be flags on equity, leaning, no

- **equity_right**
  - *sid* (this is a security)
  - und_equity_sid (the equity of the right)
  - expiration_date
  - ratio
  
- **equity_warrant**
  - *sid*
  - und_equity_sid (the equity of the warrant)
  - premium
  - gearing
  - expiration_date

---
Futures
--- 
    
- **index**
    - *sid* (index is an instrument)
      - (the index for the futures table)
      - VIX, S&P 500, Fed Funds, Libor, Crude Oil, are indexes
    - index_vendor (S&P, MSCI)
    - name (Three Month Libor)
    - index_code (LIBOR_3M)
    - description (Three month libor trading USD London)
    
- **future_chain**
    - *future_chain_code* (primary key, the code for the future)
      - CL, ES, ED, TU, lean hogs, etc
    - name (Crude Oil, S&P Mini)
    - future_type (short term interest rate, bond future, etc.)
    - description (S&P Mini trading at CBOE)
    - inception_date
    - death_date (if the future chain, is turned off, rare)

- **future**
    - *sid* (future is an instrument)
    - *index_sid* (the index for the future, S&P 500, LIBOR)
    - *future_chain_code* (foreign key to future_chain)
    - expiration_date 
      -(generated from Bloomberg and using OG dates)
      - one time, batch job, generate every future for every chain, for 10 years in the future, according to the rules
    - first_delivery_date
    - last_delivery_date
    - start_accrual_date
    - alias_ticker (midcurves like Eurodollar futures/options.)
         
- **future_option**
  - *sid* (future option is an instrument)
  - *und_future_sid* (future the option is on)
  - strike
  - expiration_date
  

---
Equity OTC
---

- **equity_swap**
  - sid (equity swap is an instrument)
  - parent_equity_sid (equity swapped on)
  - financing_rate
  - financing_index_sid
  - financing_index_spread_rate
  - start_date
  - maturity_date
  - tenor
  - payment_schedule_id (if the schedule is non-standard, make a schedule)

---
Payment Schedule
---
Payment schedules are important.

Whenever a swap, bond or otherwise, doesn't
fit into a well defined schedule, define it here,
with specific dates, rates and amounts.  

This is *far easier* than trying to define, 
rules for every paying asset which doesn't fit normal schedules.

- **payment_schedule**
    - payment_schedule_id
      - if this is set, this defines, the schedule, **exactly** (to handle one-offs)
      - it could be the case, that every schedule is here, but probably not, only where needed
      - this handles when debt securityWithMarks are in default, or any non-stanard payment schedules
    - this table is actually for anything which needs a one off payment schedule, not only bonds (swaps etc)
    - *parent_sid*
    - payment_date
    - payment_amount
    - payment_rate

  
---
Corporate Bonds
--- 

- **bond**
  - *sid* (bond is an instrument)
  - *company_id* (every corporate bond has a company)
  - rate
  - tenor
  - payment_schedule_id
    - if set, this implies it pays by schedule
  - coupon_period_tenor (1M, 3M, 5M, 1Y)
  - start_accrual_date
  - maturity_date
  - callable_or_puttable_flag (C/P)
  - cp_schedule_id (call put schedule pk)
  - sink_or_float_flag (S/F)
  - sf_schedule_id (the pk of the schedule)
  
 - **call_put_schedule**
    - cp_schedule_id (primary key)
    - parent_sid (the instrument tied to this schedule)
    - event_date (the date it can be called or put on)
    - quantity (how much is called or put)
    
- **sink_float_schedule**
     - sf_schedule_id
     - *parent_sid* (the instrument for this schedule)
     - event_date (sink or float happening)
     - amount
     - sink_float_flag
     - if sinkable or float-able, and traded, populate this table
     - this is fine as a rule, as there are not many, make the schedule up front and save it

---
Company 
---
Company is not an instrument.
Instruments may have companies 
e.g. a bond issued by IBM, company is IBM
 or IBM equity, both have company_id foreign key 
 to a primary key company

- **company**
   - *company_id*
   - *parent_company_id* (many subsidiaries, one parent company)
   - name
   - country_domiciled_iso
   - city
   - state
   - zip
   - province

---
Day Count Convention
---
Shared across many systems, this table stores
the codes for day count convetions used by other tables.

The code field is used in other tables for readability,
as a foreign key to the code on this table.

- **day_count_convention**
  - *code*
  - long_name 

---
FX Related
---
- **currency** table (you hold currency)
  - *sid* (fx is an instrument)
  - code_iso (USD, JPY)
  - name (Unites States Dollar, Japnese Yen)
    
- **currency_pair**  (tradable pair)
  - *sid* (you can trade it, making it an instrument)
  - pair_code (USD/JPY)
  - base_code (USD)
  - quote_code (JPY)
  - base_sid (sid of USD currency table)
  - quote_sid (sid of JPY currency table)
  
- **fx_forward** 
  - *sid* (fx forward is an OTC instrument)
  - expiration_date (6/14/2022)
  - *und_currency_sid* (EUR/USD)
  - *price* (1.012)

- **forward_rate_agreement**
  - *sid* (floating rate agreement is an instrument)
  - notional_amount (100,000,000)
  - fixed_rate (3.2%)
  - reference_index_sid (LIBOR)
  - reference_rate_spread (30 bps)
  - tenor (3M)
  - day_count_convention_code (fk to day_count_convention)
  - termination_date

---
Fixed Income
----

#### Swap Rate

Swap rate are well-known rates published

  - *swap_rate* table (rates which swaps are based on)

  - *generic_swap* table

#### IR Swap

- An IR Swap, is a swap you put on, generally, opened and closed
    - two legs, 
    - but done with, *columns*, as this is more straightforward
    - n_leg swaps can be done elsewhere, two legs, is the majority of swaps
 
- **interest_rate_swap**
    - *sid* (to security fk)
    - leg1_fixed_float_flag
    - leg2_fixed_float_flag
    - leg1_index
    - leg2_index
    - leg1_rate
    - leg2_rate
    - counterparty
    - clearing_broker
    - exec_broker
    - cleared_flag (T/F)
    - clearing_exchange_mic
    - notional:  optional, can be used or qty or both
    - start_date: date
    - termination_date: standard swap stuff
##### Swaption

Swaption is on a swap, with an expriation and strike

- *swaption*
  - expiration
  - strike_rate
  - ir_swap_sid

---
Credit tables
---
- credit_default_swap
  - reference_asset_sid
  - reference_defaulted_flag (T/F)
  - premium
  - tenor_code (5Y, 3Y)
  - inception_date

- cdo_table
  - phase two for CDOs


---
Technology
---

-  Its own postgres database, which could be exported elsewhere if need be
-  Find out how to export a database into another, and keep these streaming, in compose
1.  One table, entire thing loaded in memory
2.  Fed from the Open Figi files
-  Use a sample, but can get the entire thing for a small amount of money or do it myself via scraping or otherwise, or simply hit their site to get things when I need them.
-  Load the entire table in memory
3.  Ability to add ("master"), a new security, and return the ID for the security
-  This would be from a pool of available to be mastered securities, reserved in memory.
-  The pool of to be mastered, is refreshed to keep a certain number of open slots for new entires
4.  Trie with Prefix, then Infix, to do the lookup of the securities, as part of the API
5.  Bulk lookup of securities
6.  Streaming updates to any securities (gRPC)
6a.  And, streaming updates to any changes on any securities, via gRPC, after asking for securities
