package com.hedgeos.service.refdata.web.rest;

import com.hedgeos.service.refdata.RefdataApp;
import com.hedgeos.service.refdata.domain.CurveSecurity;
import com.hedgeos.service.refdata.repository.CurveSecurityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CurveSecurityResource} REST controller.
 */
@SpringBootTest(classes = RefdataApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CurveSecurityResourceIT {

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final Integer DEFAULT_SECURITY_ID = 1;
    private static final Integer UPDATED_SECURITY_ID = 2;

    @Autowired
    private CurveSecurityRepository curveSecurityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCurveSecurityMockMvc;

    private CurveSecurity curveSecurity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CurveSecurity createEntity(EntityManager em) {
        CurveSecurity curveSecurity = new CurveSecurity()
            .version(DEFAULT_VERSION)
            .securityId(DEFAULT_SECURITY_ID);
        return curveSecurity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CurveSecurity createUpdatedEntity(EntityManager em) {
        CurveSecurity curveSecurity = new CurveSecurity()
            .version(UPDATED_VERSION)
            .securityId(UPDATED_SECURITY_ID);
        return curveSecurity;
    }

    @BeforeEach
    public void initTest() {
        curveSecurity = createEntity(em);
    }

    @Test
    @Transactional
    public void createCurveSecurity() throws Exception {
        int databaseSizeBeforeCreate = curveSecurityRepository.findAll().size();
        // Create the CurveSecurity
        restCurveSecurityMockMvc.perform(post("/api/curve-securities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(curveSecurity)))
            .andExpect(status().isCreated());

        // Validate the CurveSecurity in the database
        List<CurveSecurity> curveSecurityList = curveSecurityRepository.findAll();
        assertThat(curveSecurityList).hasSize(databaseSizeBeforeCreate + 1);
        CurveSecurity testCurveSecurity = curveSecurityList.get(curveSecurityList.size() - 1);
        assertThat(testCurveSecurity.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testCurveSecurity.getSecurityId()).isEqualTo(DEFAULT_SECURITY_ID);
    }

    @Test
    @Transactional
    public void createCurveSecurityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = curveSecurityRepository.findAll().size();

        // Create the CurveSecurity with an existing ID
        curveSecurity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCurveSecurityMockMvc.perform(post("/api/curve-securities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(curveSecurity)))
            .andExpect(status().isBadRequest());

        // Validate the CurveSecurity in the database
        List<CurveSecurity> curveSecurityList = curveSecurityRepository.findAll();
        assertThat(curveSecurityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCurveSecurities() throws Exception {
        // Initialize the database
        curveSecurityRepository.saveAndFlush(curveSecurity);

        // Get all the curveSecurityList
        restCurveSecurityMockMvc.perform(get("/api/curve-securities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(curveSecurity.getId().intValue())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())))
            .andExpect(jsonPath("$.[*].securityId").value(hasItem(DEFAULT_SECURITY_ID)));
    }
    
    @Test
    @Transactional
    public void getCurveSecurity() throws Exception {
        // Initialize the database
        curveSecurityRepository.saveAndFlush(curveSecurity);

        // Get the curveSecurity
        restCurveSecurityMockMvc.perform(get("/api/curve-securities/{id}", curveSecurity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(curveSecurity.getId().intValue()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()))
            .andExpect(jsonPath("$.securityId").value(DEFAULT_SECURITY_ID));
    }
    @Test
    @Transactional
    public void getNonExistingCurveSecurity() throws Exception {
        // Get the curveSecurity
        restCurveSecurityMockMvc.perform(get("/api/curve-securities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCurveSecurity() throws Exception {
        // Initialize the database
        curveSecurityRepository.saveAndFlush(curveSecurity);

        int databaseSizeBeforeUpdate = curveSecurityRepository.findAll().size();

        // Update the curveSecurity
        CurveSecurity updatedCurveSecurity = curveSecurityRepository.findById(curveSecurity.getId()).get();
        // Disconnect from session so that the updates on updatedCurveSecurity are not directly saved in db
        em.detach(updatedCurveSecurity);
        updatedCurveSecurity
            .version(UPDATED_VERSION)
            .securityId(UPDATED_SECURITY_ID);

        restCurveSecurityMockMvc.perform(put("/api/curve-securities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCurveSecurity)))
            .andExpect(status().isOk());

        // Validate the CurveSecurity in the database
        List<CurveSecurity> curveSecurityList = curveSecurityRepository.findAll();
        assertThat(curveSecurityList).hasSize(databaseSizeBeforeUpdate);
        CurveSecurity testCurveSecurity = curveSecurityList.get(curveSecurityList.size() - 1);
        assertThat(testCurveSecurity.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testCurveSecurity.getSecurityId()).isEqualTo(UPDATED_SECURITY_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingCurveSecurity() throws Exception {
        int databaseSizeBeforeUpdate = curveSecurityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCurveSecurityMockMvc.perform(put("/api/curve-securities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(curveSecurity)))
            .andExpect(status().isBadRequest());

        // Validate the CurveSecurity in the database
        List<CurveSecurity> curveSecurityList = curveSecurityRepository.findAll();
        assertThat(curveSecurityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCurveSecurity() throws Exception {
        // Initialize the database
        curveSecurityRepository.saveAndFlush(curveSecurity);

        int databaseSizeBeforeDelete = curveSecurityRepository.findAll().size();

        // Delete the curveSecurity
        restCurveSecurityMockMvc.perform(delete("/api/curve-securities/{id}", curveSecurity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CurveSecurity> curveSecurityList = curveSecurityRepository.findAll();
        assertThat(curveSecurityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
