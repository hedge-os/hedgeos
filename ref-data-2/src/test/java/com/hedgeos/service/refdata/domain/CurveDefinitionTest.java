package com.hedgeos.service.refdata.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hedgeos.service.refdata.web.rest.TestUtil;

public class CurveDefinitionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CurveDefinition.class);
        CurveDefinition curveDefinition1 = new CurveDefinition();
        curveDefinition1.setId(1L);
        CurveDefinition curveDefinition2 = new CurveDefinition();
        curveDefinition2.setId(curveDefinition1.getId());
        assertThat(curveDefinition1).isEqualTo(curveDefinition2);
        curveDefinition2.setId(2L);
        assertThat(curveDefinition1).isNotEqualTo(curveDefinition2);
        curveDefinition1.setId(null);
        assertThat(curveDefinition1).isNotEqualTo(curveDefinition2);
    }
}
