package com.hedgeos.service.refdata.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PortfolioMapperTest {

    private PortfolioMapper portfolioMapper;

    @BeforeEach
    public void setUp() {
        portfolioMapper = new PortfolioMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(portfolioMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(portfolioMapper.fromId(null)).isNull();
    }
}
