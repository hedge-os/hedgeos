package com.hedgeos.service.refdata.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hedgeos.service.refdata.web.rest.TestUtil;

public class CurveSecurityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CurveSecurity.class);
        CurveSecurity curveSecurity1 = new CurveSecurity();
        curveSecurity1.setId(1L);
        CurveSecurity curveSecurity2 = new CurveSecurity();
        curveSecurity2.setId(curveSecurity1.getId());
        assertThat(curveSecurity1).isEqualTo(curveSecurity2);
        curveSecurity2.setId(2L);
        assertThat(curveSecurity1).isNotEqualTo(curveSecurity2);
        curveSecurity1.setId(null);
        assertThat(curveSecurity1).isNotEqualTo(curveSecurity2);
    }
}
