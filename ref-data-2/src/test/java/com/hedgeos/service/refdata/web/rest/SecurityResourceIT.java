package com.hedgeos.service.refdata.web.rest;

import com.hedgeos.service.refdata.RefdataApp;
import com.hedgeos.service.refdata.domain.Security;
import com.hedgeos.service.refdata.repository.RefDataSecurityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SecurityResource} REST controller.
 */
@SpringBootTest(classes = RefdataApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class SecurityResourceIT {

    private static final Integer DEFAULT_SECURITY_ID = 1;
    private static final Integer UPDATED_SECURITY_ID = 2;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private RefDataSecurityRepository refDataSecurityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSecurityMockMvc;

    private Security security;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Security createEntity(EntityManager em) {
        Security security = new Security()
            .securityId(DEFAULT_SECURITY_ID)
            .name(DEFAULT_NAME);
        return security;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Security createUpdatedEntity(EntityManager em) {
        Security security = new Security()
            .securityId(UPDATED_SECURITY_ID)
            .name(UPDATED_NAME);
        return security;
    }

    @BeforeEach
    public void initTest() {
        security = createEntity(em);
    }

    @Test
    @Transactional
    public void createSecurity() throws Exception {
        int databaseSizeBeforeCreate = refDataSecurityRepository.findAll().size();
        // Create the Security
        restSecurityMockMvc.perform(post("/api/securities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(security)))
            .andExpect(status().isCreated());

        // Validate the Security in the database
        List<Security> securityList = refDataSecurityRepository.findAll();
        assertThat(securityList).hasSize(databaseSizeBeforeCreate + 1);
        Security testSecurity = securityList.get(securityList.size() - 1);
        assertThat(testSecurity.getSecurityId()).isEqualTo(DEFAULT_SECURITY_ID);
        assertThat(testSecurity.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createSecurityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = refDataSecurityRepository.findAll().size();

        // Create the Security with an existing ID
        security.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSecurityMockMvc.perform(post("/api/securities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(security)))
            .andExpect(status().isBadRequest());

        // Validate the Security in the database
        List<Security> securityList = refDataSecurityRepository.findAll();
        assertThat(securityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSecurities() throws Exception {
        // Initialize the database
        refDataSecurityRepository.saveAndFlush(security);

        // Get all the securityList
        restSecurityMockMvc.perform(get("/api/securities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(security.getId().intValue())))
            .andExpect(jsonPath("$.[*].securityId").value(hasItem(DEFAULT_SECURITY_ID)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    public void getSecurity() throws Exception {
        // Initialize the database
        refDataSecurityRepository.saveAndFlush(security);

        // Get the security
        restSecurityMockMvc.perform(get("/api/securities/{id}", security.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(security.getId().intValue()))
            .andExpect(jsonPath("$.securityId").value(DEFAULT_SECURITY_ID))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingSecurity() throws Exception {
        // Get the security
        restSecurityMockMvc.perform(get("/api/securities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSecurity() throws Exception {
        // Initialize the database
        refDataSecurityRepository.saveAndFlush(security);

        int databaseSizeBeforeUpdate = refDataSecurityRepository.findAll().size();

        // Update the security
        Security updatedSecurity = refDataSecurityRepository.findById(security.getId()).get();
        // Disconnect from session so that the updates on updatedSecurity are not directly saved in db
        em.detach(updatedSecurity);
        updatedSecurity
            .securityId(UPDATED_SECURITY_ID)
            .name(UPDATED_NAME);

        restSecurityMockMvc.perform(put("/api/securities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSecurity)))
            .andExpect(status().isOk());

        // Validate the Security in the database
        List<Security> securityList = refDataSecurityRepository.findAll();
        assertThat(securityList).hasSize(databaseSizeBeforeUpdate);
        Security testSecurity = securityList.get(securityList.size() - 1);
        assertThat(testSecurity.getSecurityId()).isEqualTo(UPDATED_SECURITY_ID);
        assertThat(testSecurity.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingSecurity() throws Exception {
        int databaseSizeBeforeUpdate = refDataSecurityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSecurityMockMvc.perform(put("/api/securities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(security)))
            .andExpect(status().isBadRequest());

        // Validate the Security in the database
        List<Security> securityList = refDataSecurityRepository.findAll();
        assertThat(securityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSecurity() throws Exception {
        // Initialize the database
        refDataSecurityRepository.saveAndFlush(security);

        int databaseSizeBeforeDelete = refDataSecurityRepository.findAll().size();

        // Delete the security
        restSecurityMockMvc.perform(delete("/api/securities/{id}", security.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Security> securityList = refDataSecurityRepository.findAll();
        assertThat(securityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
