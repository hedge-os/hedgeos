package com.hedgeos.service.refdata.web.rest;

import com.hedgeos.service.refdata.RefdataApp;
import com.hedgeos.service.refdata.domain.CurveDefinition;
import com.hedgeos.service.refdata.repository.CurveDefinitionRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CurveDefinitionResource} REST controller.
 */
@SpringBootTest(classes = RefdataApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CurveDefinitionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_CURRENCY = "BBBBBBBBBB";

    @Autowired
    private CurveDefinitionRepository curveDefinitionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCurveDefinitionMockMvc;

    private CurveDefinition curveDefinition;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CurveDefinition createEntity(EntityManager em) {
        CurveDefinition curveDefinition = new CurveDefinition()
            .name(DEFAULT_NAME)
            .currency(DEFAULT_CURRENCY);
        return curveDefinition;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CurveDefinition createUpdatedEntity(EntityManager em) {
        CurveDefinition curveDefinition = new CurveDefinition()
            .name(UPDATED_NAME)
            .currency(UPDATED_CURRENCY);
        return curveDefinition;
    }

    @BeforeEach
    public void initTest() {
        curveDefinition = createEntity(em);
    }

    @Test
    @Transactional
    public void createCurveDefinition() throws Exception {
        int databaseSizeBeforeCreate = curveDefinitionRepository.findAll().size();
        // Create the CurveDefinition
        restCurveDefinitionMockMvc.perform(post("/api/curve-definitions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(curveDefinition)))
            .andExpect(status().isCreated());

        // Validate the CurveDefinition in the database
        List<CurveDefinition> curveDefinitionList = curveDefinitionRepository.findAll();
        assertThat(curveDefinitionList).hasSize(databaseSizeBeforeCreate + 1);
        CurveDefinition testCurveDefinition = curveDefinitionList.get(curveDefinitionList.size() - 1);
        assertThat(testCurveDefinition.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCurveDefinition.getCurrency()).isEqualTo(DEFAULT_CURRENCY);
    }

    @Test
    @Transactional
    public void createCurveDefinitionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = curveDefinitionRepository.findAll().size();

        // Create the CurveDefinition with an existing ID
        curveDefinition.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCurveDefinitionMockMvc.perform(post("/api/curve-definitions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(curveDefinition)))
            .andExpect(status().isBadRequest());

        // Validate the CurveDefinition in the database
        List<CurveDefinition> curveDefinitionList = curveDefinitionRepository.findAll();
        assertThat(curveDefinitionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCurveDefinitions() throws Exception {
        // Initialize the database
        curveDefinitionRepository.saveAndFlush(curveDefinition);

        // Get all the curveDefinitionList
        restCurveDefinitionMockMvc.perform(get("/api/curve-definitions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(curveDefinition.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY)));
    }
    
    @Test
    @Transactional
    public void getCurveDefinition() throws Exception {
        // Initialize the database
        curveDefinitionRepository.saveAndFlush(curveDefinition);

        // Get the curveDefinition
        restCurveDefinitionMockMvc.perform(get("/api/curve-definitions/{id}", curveDefinition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(curveDefinition.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.currency").value(DEFAULT_CURRENCY));
    }
    @Test
    @Transactional
    public void getNonExistingCurveDefinition() throws Exception {
        // Get the curveDefinition
        restCurveDefinitionMockMvc.perform(get("/api/curve-definitions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCurveDefinition() throws Exception {
        // Initialize the database
        curveDefinitionRepository.saveAndFlush(curveDefinition);

        int databaseSizeBeforeUpdate = curveDefinitionRepository.findAll().size();

        // Update the curveDefinition
        CurveDefinition updatedCurveDefinition = curveDefinitionRepository.findById(curveDefinition.getId()).get();
        // Disconnect from session so that the updates on updatedCurveDefinition are not directly saved in db
        em.detach(updatedCurveDefinition);
        updatedCurveDefinition
            .name(UPDATED_NAME)
            .currency(UPDATED_CURRENCY);

        restCurveDefinitionMockMvc.perform(put("/api/curve-definitions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCurveDefinition)))
            .andExpect(status().isOk());

        // Validate the CurveDefinition in the database
        List<CurveDefinition> curveDefinitionList = curveDefinitionRepository.findAll();
        assertThat(curveDefinitionList).hasSize(databaseSizeBeforeUpdate);
        CurveDefinition testCurveDefinition = curveDefinitionList.get(curveDefinitionList.size() - 1);
        assertThat(testCurveDefinition.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCurveDefinition.getCurrency()).isEqualTo(UPDATED_CURRENCY);
    }

    @Test
    @Transactional
    public void updateNonExistingCurveDefinition() throws Exception {
        int databaseSizeBeforeUpdate = curveDefinitionRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCurveDefinitionMockMvc.perform(put("/api/curve-definitions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(curveDefinition)))
            .andExpect(status().isBadRequest());

        // Validate the CurveDefinition in the database
        List<CurveDefinition> curveDefinitionList = curveDefinitionRepository.findAll();
        assertThat(curveDefinitionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCurveDefinition() throws Exception {
        // Initialize the database
        curveDefinitionRepository.saveAndFlush(curveDefinition);

        int databaseSizeBeforeDelete = curveDefinitionRepository.findAll().size();

        // Delete the curveDefinition
        restCurveDefinitionMockMvc.perform(delete("/api/curve-definitions/{id}", curveDefinition.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CurveDefinition> curveDefinitionList = curveDefinitionRepository.findAll();
        assertThat(curveDefinitionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
