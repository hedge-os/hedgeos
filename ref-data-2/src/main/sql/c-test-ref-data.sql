
-- note:  two is known here to be 'test-pm' user
-- TODO:  Permissions should be exported to here as a schema
-- TODO:  ; however, the handles it in Java, on creation
insert into portfolio (name, pm_user_id, start_date)
values ('TEST_PORTFOLIO', 2, '2020-06-04');

-- make a 'Go Long' strategy
insert into strategy (portfolio_id, name, start_date)
select id, 'Go Long', start_date from portfolio where name = 'TEST_PORTFOLIO';
-- make sub strategies for Go Long
insert into sub_strategy (strategy_id, name)
select id, 'Value Stocks' from strategy where name = 'Go Long';
insert into sub_strategy (strategy_id, name)
select id, 'Mom Stocks' from strategy where name = 'Go Long';

-- make a 'Sell High' strategy
insert into strategy (portfolio_id, name, start_date)
select id, 'Sell High', start_date from portfolio where name = 'TEST_PORTFOLIO';
-- sell high has one sub-strategy
insert into sub_strategy (strategy_id, name, start_date)
select id, 'All', start_date from strategy where name = 'Sell High';

