create table strategy
(
    id                  serial       not null
        constraint strategy_pk
            primary key,
    portfolio_id        integer      not null,
    name                varchar(200) not null,
    start_date          date         not null,
    death_date          date,
    intestment_style_id integer
);

alter table strategy
    owner to hedgeos;

create unique index strategy_id_uindex
    on strategy (id);

create table investment_style
(
    id   serial       not null
        constraint investment_style_pk
            primary key,
    name varchar(200) not null
);

alter table investment_style
    owner to hedgeos;

create table portfolio
(
    id                  serial       not null
        constraint portfolio_pk
            primary key,
    name                varchar(200) not null,
    pm_user_id          integer,
    start_date          date         not null,
    investment_style_id integer
        constraint portfolio_investment_style_fk
            references investment_style
);

alter table portfolio
    owner to hedgeos;

create unique index portfolio_id_uindex
    on portfolio (id);

create unique index portfolio_name_uindex
    on portfolio (name);

create unique index investment_style_id_uindex
    on investment_style (id);

create unique index investment_style_name_uindex
    on investment_style (name);

create table sub_strategy
(
    id                  serial  not null
        constraint sub_strategy_pk
            primary key,
    strategy_id         integer not null
        constraint sub_strategy_strategy_fk
            references strategy,
    name                varchar(200),
    investment_style_id integer
        constraint sub_strategy_style__fk
            references investment_style,
    start_date          date,
    death_date          date
);

alter table sub_strategy
    owner to hedgeos;

create unique index sub_strategy_id_uindex
    on sub_strategy (id);

