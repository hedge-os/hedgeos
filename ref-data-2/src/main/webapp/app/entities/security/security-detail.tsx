import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './security.reducer';
import { ISecurity } from 'app/shared/model/security.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISecurityDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SecurityDetail = (props: ISecurityDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { securityEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Security [<b>{securityEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="securityId">Security Id</span>
          </dt>
          <dd>{securityEntity.securityId}</dd>
          <dt>
            <span id="name">Name</span>
          </dt>
          <dd>{securityEntity.name}</dd>
        </dl>
        <Button tag={Link} to="/security" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/security/${securityEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ security }: IRootState) => ({
  securityEntity: security.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SecurityDetail);
