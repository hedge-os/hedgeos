import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction,
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ICurveDefinition, defaultValue } from 'app/shared/model/curve-definition.model';

export const ACTION_TYPES = {
  FETCH_CURVEDEFINITION_LIST: 'curveDefinition/FETCH_CURVEDEFINITION_LIST',
  FETCH_CURVEDEFINITION: 'curveDefinition/FETCH_CURVEDEFINITION',
  CREATE_CURVEDEFINITION: 'curveDefinition/CREATE_CURVEDEFINITION',
  UPDATE_CURVEDEFINITION: 'curveDefinition/UPDATE_CURVEDEFINITION',
  DELETE_CURVEDEFINITION: 'curveDefinition/DELETE_CURVEDEFINITION',
  RESET: 'curveDefinition/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICurveDefinition>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type CurveDefinitionState = Readonly<typeof initialState>;

// Reducer

export default (state: CurveDefinitionState = initialState, action): CurveDefinitionState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CURVEDEFINITION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CURVEDEFINITION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CURVEDEFINITION):
    case REQUEST(ACTION_TYPES.UPDATE_CURVEDEFINITION):
    case REQUEST(ACTION_TYPES.DELETE_CURVEDEFINITION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CURVEDEFINITION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CURVEDEFINITION):
    case FAILURE(ACTION_TYPES.CREATE_CURVEDEFINITION):
    case FAILURE(ACTION_TYPES.UPDATE_CURVEDEFINITION):
    case FAILURE(ACTION_TYPES.DELETE_CURVEDEFINITION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CURVEDEFINITION_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_CURVEDEFINITION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CURVEDEFINITION):
    case SUCCESS(ACTION_TYPES.UPDATE_CURVEDEFINITION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CURVEDEFINITION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/curve-definitions';

// Actions

export const getEntities: ICrudGetAllAction<ICurveDefinition> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CURVEDEFINITION_LIST,
    payload: axios.get<ICurveDefinition>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<ICurveDefinition> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CURVEDEFINITION,
    payload: axios.get<ICurveDefinition>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ICurveDefinition> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CURVEDEFINITION,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const updateEntity: ICrudPutAction<ICurveDefinition> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CURVEDEFINITION,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICurveDefinition> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CURVEDEFINITION,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
