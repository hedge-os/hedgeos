import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './curve-definition.reducer';
import { ICurveDefinition } from 'app/shared/model/curve-definition.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICurveDefinitionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CurveDefinitionDetail = (props: ICurveDefinitionDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { curveDefinitionEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          CurveDefinition [<b>{curveDefinitionEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">Name</span>
          </dt>
          <dd>{curveDefinitionEntity.name}</dd>
          <dt>
            <span id="currency">Currency</span>
          </dt>
          <dd>{curveDefinitionEntity.currency}</dd>
        </dl>
        <Button tag={Link} to="/curve-definition" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/curve-definition/${curveDefinitionEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ curveDefinition }: IRootState) => ({
  curveDefinitionEntity: curveDefinition.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CurveDefinitionDetail);
