import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CurveDefinition from './curve-definition';
import CurveDefinitionDetail from './curve-definition-detail';
import CurveDefinitionUpdate from './curve-definition-update';
import CurveDefinitionDeleteDialog from './curve-definition-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CurveDefinitionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CurveDefinitionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CurveDefinitionDetail} />
      <ErrorBoundaryRoute path={match.url} component={CurveDefinition} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={CurveDefinitionDeleteDialog} />
  </>
);

export default Routes;
