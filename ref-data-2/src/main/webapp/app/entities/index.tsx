import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CurveDefinition from './curve-definition';
import CurveSecurity from './curve-security';
import Security from './security';
import Portfolio from './portfolio';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}curve-definition`} component={CurveDefinition} />
      <ErrorBoundaryRoute path={`${match.url}curve-security`} component={CurveSecurity} />
      <ErrorBoundaryRoute path={`${match.url}security`} component={Security} />
      <ErrorBoundaryRoute path={`${match.url}portfolio`} component={Portfolio} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
