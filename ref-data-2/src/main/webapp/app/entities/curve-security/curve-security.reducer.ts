import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction,
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ICurveSecurity, defaultValue } from 'app/shared/model/curve-security.model';

export const ACTION_TYPES = {
  FETCH_CURVESECURITY_LIST: 'curveSecurity/FETCH_CURVESECURITY_LIST',
  FETCH_CURVESECURITY: 'curveSecurity/FETCH_CURVESECURITY',
  CREATE_CURVESECURITY: 'curveSecurity/CREATE_CURVESECURITY',
  UPDATE_CURVESECURITY: 'curveSecurity/UPDATE_CURVESECURITY',
  DELETE_CURVESECURITY: 'curveSecurity/DELETE_CURVESECURITY',
  RESET: 'curveSecurity/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICurveSecurity>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type CurveSecurityState = Readonly<typeof initialState>;

// Reducer

export default (state: CurveSecurityState = initialState, action): CurveSecurityState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CURVESECURITY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CURVESECURITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CURVESECURITY):
    case REQUEST(ACTION_TYPES.UPDATE_CURVESECURITY):
    case REQUEST(ACTION_TYPES.DELETE_CURVESECURITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CURVESECURITY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CURVESECURITY):
    case FAILURE(ACTION_TYPES.CREATE_CURVESECURITY):
    case FAILURE(ACTION_TYPES.UPDATE_CURVESECURITY):
    case FAILURE(ACTION_TYPES.DELETE_CURVESECURITY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CURVESECURITY_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_CURVESECURITY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CURVESECURITY):
    case SUCCESS(ACTION_TYPES.UPDATE_CURVESECURITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CURVESECURITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/curve-securities';

// Actions

export const getEntities: ICrudGetAllAction<ICurveSecurity> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CURVESECURITY_LIST,
    payload: axios.get<ICurveSecurity>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<ICurveSecurity> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CURVESECURITY,
    payload: axios.get<ICurveSecurity>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ICurveSecurity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CURVESECURITY,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const updateEntity: ICrudPutAction<ICurveSecurity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CURVESECURITY,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICurveSecurity> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CURVESECURITY,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
