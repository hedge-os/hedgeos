import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ICurveDefinition } from 'app/shared/model/curve-definition.model';
import { getEntities as getCurveDefinitions } from 'app/entities/curve-definition/curve-definition.reducer';
import { getEntity, updateEntity, createEntity, reset } from './curve-security.reducer';
import { ICurveSecurity } from 'app/shared/model/curve-security.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ICurveSecurityUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CurveSecurityUpdate = (props: ICurveSecurityUpdateProps) => {
  const [curveDefinitionId, setCurveDefinitionId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { curveSecurityEntity, curveDefinitions, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/curve-security');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }

    props.getCurveDefinitions();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...curveSecurityEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="refdataApp.curveSecurity.home.createOrEditLabel">Create or edit a CurveSecurity</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : curveSecurityEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="curve-security-id">ID</Label>
                  <AvInput id="curve-security-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="versionLabel" for="curve-security-version">
                  Version
                </Label>
                <AvField id="curve-security-version" type="string" className="form-control" name="version" />
              </AvGroup>
              <AvGroup>
                <Label id="securityIdLabel" for="curve-security-securityId">
                  Security Id
                </Label>
                <AvField id="curve-security-securityId" type="string" className="form-control" name="securityId" />
              </AvGroup>
              <AvGroup>
                <Label for="curve-security-curveDefinition">Curve Definition</Label>
                <AvInput id="curve-security-curveDefinition" type="select" className="form-control" name="curveDefinition.id">
                  <option value="" key="0" />
                  {curveDefinitions
                    ? curveDefinitions.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/curve-security" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  curveDefinitions: storeState.curveDefinition.entities,
  curveSecurityEntity: storeState.curveSecurity.entity,
  loading: storeState.curveSecurity.loading,
  updating: storeState.curveSecurity.updating,
  updateSuccess: storeState.curveSecurity.updateSuccess,
});

const mapDispatchToProps = {
  getCurveDefinitions,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CurveSecurityUpdate);
