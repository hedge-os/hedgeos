import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CurveSecurity from './curve-security';
import CurveSecurityDetail from './curve-security-detail';
import CurveSecurityUpdate from './curve-security-update';
import CurveSecurityDeleteDialog from './curve-security-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CurveSecurityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CurveSecurityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CurveSecurityDetail} />
      <ErrorBoundaryRoute path={match.url} component={CurveSecurity} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={CurveSecurityDeleteDialog} />
  </>
);

export default Routes;
