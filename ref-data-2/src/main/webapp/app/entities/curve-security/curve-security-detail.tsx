import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './curve-security.reducer';
import { ICurveSecurity } from 'app/shared/model/curve-security.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICurveSecurityDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CurveSecurityDetail = (props: ICurveSecurityDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { curveSecurityEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          CurveSecurity [<b>{curveSecurityEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="version">Version</span>
          </dt>
          <dd>{curveSecurityEntity.version}</dd>
          <dt>
            <span id="securityId">Security Id</span>
          </dt>
          <dd>{curveSecurityEntity.securityId}</dd>
          <dt>Curve Definition</dt>
          <dd>{curveSecurityEntity.curveDefinition ? curveSecurityEntity.curveDefinition.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/curve-security" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/curve-security/${curveSecurityEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ curveSecurity }: IRootState) => ({
  curveSecurityEntity: curveSecurity.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CurveSecurityDetail);
