export interface ICurveDefinition {
  id?: number;
  name?: string;
  currency?: string;
}

export const defaultValue: Readonly<ICurveDefinition> = {};
