import { Moment } from 'moment';

export interface IPortfolio {
  id?: number;
  name?: string;
  startDate?: string;
  deathDate?: string;
}

export const defaultValue: Readonly<IPortfolio> = {};
