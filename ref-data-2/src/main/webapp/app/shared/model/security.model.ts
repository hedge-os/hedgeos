export interface ISecurity {
  id?: number;
  securityId?: number;
  name?: string;
}

export const defaultValue: Readonly<ISecurity> = {};
