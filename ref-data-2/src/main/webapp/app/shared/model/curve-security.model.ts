import { ICurveDefinition } from 'app/shared/model/curve-definition.model';

export interface ICurveSecurity {
  id?: number;
  version?: number;
  securityId?: number;
  curveDefinition?: ICurveDefinition;
}

export const defaultValue: Readonly<ICurveSecurity> = {};
