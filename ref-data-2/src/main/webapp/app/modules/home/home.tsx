import './home.scss';

import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { Row, Col, Alert } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';

export type IHomeProp = StateProps;

export const Home = (props: IHomeProp) => {
  const { account } = props;

  return (
    <Row>
      <Col md="9">
        <h2>Welcome, Hedge-OS user!</h2>
        <p className="lead">This is your homepage</p>
        {account && account.login ? (
          <div>
            <Alert color="success">You are logged in as user {account.login}.</Alert>
          </div>
        ) : (
          <div>
            <Alert color="warning">
              If you want to
              <Link to="/login" className="alert-link">
                {' '}
                sign in
              </Link>
              , you can try the default accounts:
              <br />- Administrator (login=&quot;admin&quot; and password=&quot;admin&quot;)
              <br />- User (login=&quot;user&quot; and password=&quot;user&quot;).
            </Alert>

            <Alert color="warning">
              You do not have an account yet?&nbsp;
              <Link to="/account/register" className="alert-link">
                Register a new account
              </Link>
            </Alert>
          </div>
        )}
        <p>If you have any questions on Hedge-OS:</p>

        <ul>
          <li>
            <a href="https://www.hedge-os.com/" target="_blank" rel="noopener noreferrer">
              Hedge-OS homepage
            </a>
          </li>
          <li>
            <a href="http://stackoverflow.com/tags/hedgeos/info" target="_blank" rel="noopener noreferrer">
              Hedge-OS on Stack Overflow
            </a>
          </li>
          <li>
            <a href="https://gitlab.com/hedge-os/hedge-os/issues?state=open" target="_blank" rel="noopener noreferrer">
              Hedge-OS Bug Tracker
            </a>
          </li>
          <li>
            <a href="https://gitter.im/hedge-os/hedge-os" target="_blank" rel="noopener noreferrer">
              Hedge-OS Public Chat
            </a>
          </li>
          <li>
            <a href="https://twitter.com/hedgeos" target="_blank" rel="noopener noreferrer">
              follow @hedgeos on Twitter
            </a>
          </li>
        </ul>

        <p>
          If you like Hedge-OS, do not forget to give us a star on{' '}
          <a href="https://gitlab.com/hedge-os/hedge-os" target="_blank" rel="noopener noreferrer">
            GitLab
          </a>
          !
        </p>
      </Col>
      <Col md="3" className="pad">
        <span className="hipster rounded" />
      </Col>
    </Row>
  );
};

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Home);
