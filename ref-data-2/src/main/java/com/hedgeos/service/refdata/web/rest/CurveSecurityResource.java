package com.hedgeos.service.refdata.web.rest;

import com.hedgeos.service.refdata.domain.CurveSecurity;
import com.hedgeos.service.refdata.repository.CurveSecurityRepository;
import com.hedgeos.service.refdata.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hedgeos.service.refdata.domain.CurveSecurity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CurveSecurityResource {

    private final Logger log = LoggerFactory.getLogger(CurveSecurityResource.class);

    private static final String ENTITY_NAME = "curveSecurity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CurveSecurityRepository curveSecurityRepository;

    public CurveSecurityResource(CurveSecurityRepository curveSecurityRepository) {
        this.curveSecurityRepository = curveSecurityRepository;
    }

    /**
     * {@code POST  /curve-securities} : Create a new curveSecurity.
     *
     * @param curveSecurity the curveSecurity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new curveSecurity, or with status {@code 400 (Bad Request)} if the curveSecurity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/curve-securities")
    public ResponseEntity<CurveSecurity> createCurveSecurity(@RequestBody CurveSecurity curveSecurity) throws URISyntaxException {
        log.debug("REST request to save CurveSecurity : {}", curveSecurity);
        if (curveSecurity.getId() != null) {
            throw new BadRequestAlertException("A new curveSecurity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CurveSecurity result = curveSecurityRepository.save(curveSecurity);
        return ResponseEntity.created(new URI("/api/curve-securities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /curve-securities} : Updates an existing curveSecurity.
     *
     * @param curveSecurity the curveSecurity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated curveSecurity,
     * or with status {@code 400 (Bad Request)} if the curveSecurity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the curveSecurity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/curve-securities")
    public ResponseEntity<CurveSecurity> updateCurveSecurity(@RequestBody CurveSecurity curveSecurity) throws URISyntaxException {
        log.debug("REST request to update CurveSecurity : {}", curveSecurity);
        if (curveSecurity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CurveSecurity result = curveSecurityRepository.save(curveSecurity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, curveSecurity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /curve-securities} : get all the curveSecurities.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of curveSecurities in body.
     */
    @GetMapping("/curve-securities")
    public ResponseEntity<List<CurveSecurity>> getAllCurveSecurities(Pageable pageable) {
        log.debug("REST request to get a page of CurveSecurities");
        Page<CurveSecurity> page = curveSecurityRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /curve-securities/:id} : get the "id" curveSecurity.
     *
     * @param id the id of the curveSecurity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the curveSecurity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/curve-securities/{id}")
    public ResponseEntity<CurveSecurity> getCurveSecurity(@PathVariable Long id) {
        log.debug("REST request to get CurveSecurity : {}", id);
        Optional<CurveSecurity> curveSecurity = curveSecurityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(curveSecurity);
    }

    /**
     * {@code DELETE  /curve-securities/:id} : delete the "id" curveSecurity.
     *
     * @param id the id of the curveSecurity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/curve-securities/{id}")
    public ResponseEntity<Void> deleteCurveSecurity(@PathVariable Long id) {
        log.debug("REST request to delete CurveSecurity : {}", id);
        curveSecurityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
