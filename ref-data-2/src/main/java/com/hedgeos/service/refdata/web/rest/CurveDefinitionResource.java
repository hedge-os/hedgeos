package com.hedgeos.service.refdata.web.rest;

import com.hedgeos.service.refdata.domain.CurveDefinition;
import com.hedgeos.service.refdata.grpc.RefDataServer;
import com.hedgeos.service.refdata.repository.CurveDefinitionRepository;
import com.hedgeos.service.refdata.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hedgeos.service.refdata.domain.CurveDefinition}.
 */
@RestController
@RequestMapping("/api")
// TDO:O  Lombok failing in my IDE
// @RequiredArgsConstructor
@Transactional
public class CurveDefinitionResource {

    private final Logger log = LoggerFactory.getLogger(CurveDefinitionResource.class);

    private static final String ENTITY_NAME = "curveDefinition";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CurveDefinitionRepository curveDefinitionRepository;
    private final RefDataServer refDataServer;

    public CurveDefinitionResource(CurveDefinitionRepository reop, RefDataServer rds) {
        this.curveDefinitionRepository = reop;
        this.refDataServer = rds;
    }

    /**
     * {@code POST  /curve-definitions} : Create a new curveDefinition.
     *
     * @param curveDefinition the curveDefinition to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new curveDefinition, or with status {@code 400 (Bad Request)} if the curveDefinition has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/curve-definitions")
    public ResponseEntity<CurveDefinition> createCurveDefinition(@RequestBody CurveDefinition curveDefinition) throws URISyntaxException {
        log.debug("REST request to save CurveDefinition : {}", curveDefinition);
        if (curveDefinition.getId() != null) {
            throw new BadRequestAlertException("A new curveDefinition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CurveDefinition result = curveDefinitionRepository.save(curveDefinition);
        // let gRPC know
        refDataServer.curveCreate(result);
        return ResponseEntity.created(new URI("/api/curve-definitions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /curve-definitions} : Updates an existing curveDefinition.
     *
     * @param curveDefinition the curveDefinition to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated curveDefinition,
     * or with status {@code 400 (Bad Request)} if the curveDefinition is not valid,
     * or with status {@code 500 (Internal Server Error)} if the curveDefinition couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/curve-definitions")
    public ResponseEntity<CurveDefinition> updateCurveDefinition(@RequestBody CurveDefinition curveDefinition) throws URISyntaxException {
        log.debug("REST request to update CurveDefinition : {}", curveDefinition);
        if (curveDefinition.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CurveDefinition result = curveDefinitionRepository.save(curveDefinition);
        // let gRPC know
        refDataServer.updateCurve(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, curveDefinition.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /curve-definitions} : get all the curveDefinitions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of curveDefinitions in body.
     */
    @GetMapping("/curve-definitions")
    public ResponseEntity<List<CurveDefinition>> getAllCurveDefinitions(Pageable pageable) {
        log.debug("REST request to get a page of CurveDefinitions");
        Page<CurveDefinition> page = curveDefinitionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /curve-definitions/:id} : get the "id" curveDefinition.
     *
     * @param id the id of the curveDefinition to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the curveDefinition, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/curve-definitions/{id}")
    public ResponseEntity<CurveDefinition> getCurveDefinition(@PathVariable Long id) {
        log.debug("REST request to get CurveDefinition : {}", id);
        Optional<CurveDefinition> curveDefinition = curveDefinitionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(curveDefinition);
    }

    /**
     * {@code DELETE  /curve-definitions/:id} : delete the "id" curveDefinition.
     *
     * @param id the id of the curveDefinition to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/curve-definitions/{id}")
    public ResponseEntity<Void> deleteCurveDefinition(@PathVariable Long id) {
        log.debug("REST request to delete CurveDefinition : {}", id);
        curveDefinitionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
