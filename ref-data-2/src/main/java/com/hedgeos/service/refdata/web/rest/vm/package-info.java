/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hedgeos.service.refdata.web.rest.vm;
