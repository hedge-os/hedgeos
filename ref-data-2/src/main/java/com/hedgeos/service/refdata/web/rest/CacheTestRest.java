package com.hedgeos.service.refdata.web.rest;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.impl.proxy.MapProxyImpl;
import com.hazelcast.spring.cache.HazelcastCache;
import com.hedgeos.curve.Curve;
import com.hedgeos.service.refdata.domain.CurveDefinition;
import com.hedgeos.service.refdata.repository.CurveDefinitionRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.List;


@RestController
@RequestMapping("/test")
public class CacheTestRest {

    private final CacheManager cacheManager;
    private final CurveDefinitionRepository repo;

    public CacheTestRest(CacheManager cacheManager,
                         CurveDefinitionRepository repo) {
        this.repo = repo;
        this.cacheManager = cacheManager;
    }
//
//    @GetMapping("/cacheQuery")
//    public String cacheQuery() {
//        StringBuffer sb = new StringBuffer();
//        try {
//
//            TypedQuery<CurveDefinition> query =
//                entityManager.createQuery("select c from CurveDefinition c where c.name = :name", CurveDefinition.class);
//
//            query.setParameter("name", "USD-OIS");
//
//            List<CurveDefinition> cds = query.getResultList();
//
//            for (CurveDefinition cd : cds) {
//                System.out.println((", def="+cd.getName()));
//                sb.append("<br>"+cd.getName()+", currency="+cd.getCurrency());
//            }
//
//            return sb.toString();
//        }
//        catch (Exception e) {
//            System.out.println("Exception="+e);
//            return ""+e;
//        }
//    }



//    @GetMapping("/cacheTest")
//    public String cacheAttempts() {
//        StringBuffer sb = new StringBuffer();
//        try {
//
//            CurveDefinition cd = entityManager.find(CurveDefinition.class, 1101L);
//            System.out.println("Curve="+cd);
//
//            CurveDefinition byId = repo.findById(1101L);
//            System.out.println("Curve by id="+byId);
////
//            Collection<CurveDefinition> cds = repo.findAll();
//            for (CurveDefinition curveDefinition : cds) {
//                System.out.printf("CD="+curveDefinition);
//            }
//
//            for (String s : cacheManager.getCacheNames()) {
//                System.out.printf("Cache=" + s);
//                Cache cache = cacheManager.getCache(s);
//                System.out.println("Cache=" + cache.getName());
//                sb.append(s + "<br>");
//
//
//                Object  nativeCache = cache.getNativeCache();
//                System.out.println("nativeCache="+nativeCache.getClass()+", nativeCache="+nativeCache);
//
//                MapProxyImpl cacheNative = (MapProxyImpl) nativeCache;
//
//                for (Object o : cacheNative.keySet()) {
//                    System.out.println("Key="+o);
//                }
//
//            }
//
//            Cache cache = cacheManager.getCache("com.hedgeos.service.refdata.domain.CurveDefinition");
//            System.out.println("Cache to string="+cache.toString());
//
//            return sb.toString();
//        }
//        catch (Exception e) {
//            sb.append("<br>---------<br>"+e);
//            e.printStackTrace();
//            return sb.toString();
//        }
//
//    }


}
