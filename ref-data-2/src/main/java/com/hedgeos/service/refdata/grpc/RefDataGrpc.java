package com.hedgeos.service.refdata.grpc;

import com.hedgeos.refdata.RefData;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component
// TOD:  Lombok still broken in this project
// @RequiredArgsConstructor
public class RefDataGrpc {

    final RefDataServer refDataServer;

    public RefDataGrpc(RefDataServer rds) {
        this.refDataServer = rds;
    }

    @Value("${server.grpcPort}")
    public int grpcPort;

    public void startGrpc() throws IOException {

        final Server server = ServerBuilder.forPort(grpcPort)
            .addService(refDataServer)
            // .addService(ProtoReflectionService.newInstance())
            // .addService(health.getHealthService())
            .build()
            .start();

        System.out.println("RefData gRPC, Listening on port " + grpcPort);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Start graceful shutdown
                server.shutdown();
                try {
                    // Wait for RPCs to complete processing
                    if (!server.awaitTermination(30, TimeUnit.SECONDS)) {
                        // That was plenty of time. Let's cancel the remaining RPCs
                        server.shutdownNow();
                        // shutdownNow isn't instantaneous, so give a bit of time to clean resources up
                        // gracefully. Normally this will be well under a second.
                        server.awaitTermination(5, TimeUnit.SECONDS);
                    }
                } catch (InterruptedException ex) {
                    server.shutdownNow();
                }
            }
        });

        // This would normally be tied to the service's dependencies. For example, if HostnameGreeter
        // used a Channel to contact a required service, then when 'channel.getState() ==
        // TRANSIENT_FAILURE' we'd want to set NOT_SERVING. But HostnameGreeter has no dependencies, so
        // hard-coding SERVING is appropriate.
        // health.setStatus("", ServingStatus.SERVING);

        //server.awaitTermination();
    }
}
