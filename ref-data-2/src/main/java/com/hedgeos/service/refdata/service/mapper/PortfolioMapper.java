package com.hedgeos.service.refdata.service.mapper;


import com.hedgeos.service.refdata.domain.Portfolio;
import com.hedgeos.service.refdata.service.dto.PortfolioDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Portfolio} and its DTO {@link PortfolioDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PortfolioMapper extends EntityMapper<PortfolioDTO, Portfolio> {

    default Portfolio fromId(Long id) {
        if (id == null) {
            return null;
        }
        Portfolio portfolio = new Portfolio();
        portfolio.setId(id);
        return portfolio;
    }
}
