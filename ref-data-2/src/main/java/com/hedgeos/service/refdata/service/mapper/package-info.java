/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package com.hedgeos.service.refdata.service.mapper;
