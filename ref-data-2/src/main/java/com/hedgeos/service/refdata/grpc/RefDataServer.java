package com.hedgeos.service.refdata.grpc;

import com.hedgeos.curve.CurveDef;
import com.hedgeos.key.SecurityKey;
import com.hedgeos.refdata.service.CurveDefRequest;
import com.hedgeos.refdata.service.CurveDefResponse;
import com.hedgeos.refdata.service.RefDataServiceGrpc;
import com.hedgeos.security.Security;
import com.hedgeos.service.refdata.domain.CurveDefinition;
import com.hedgeos.service.refdata.domain.CurveSecurity;
import com.hedgeos.service.refdata.domain.Portfolio;
import com.hedgeos.service.refdata.repository.CurveDefinitionRepository;
import com.hedgeos.service.refdata.repository.CurveSecurityRepository;
import com.hedgeos.service.refdata.repository.PortfolioRepository;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class RefDataServer extends RefDataServiceGrpc.RefDataServiceImplBase {

    // TODO:  Fix SLF4j annotation
    static Logger log = LogManager.getLogger(RefDataServer.class);

    private CurveDefinitionRepository curveDefinitionRepository;
    private CurveSecurityRepository curveSecurityRepository;
    private PortfolioRepository portfolioRepository;

    Map<String, CurveDefinition> curveDefinitionMap = new ConcurrentHashMap<>();
    Map<String, Portfolio> portMap = new ConcurrentHashMap<>();

    public RefDataServer(PortfolioRepository portRepo,
                         CurveDefinitionRepository curveRepo,
                         CurveSecurityRepository curveSecurityRepo)
    {
        this.curveDefinitionRepository = curveRepo;
        this.curveSecurityRepository = curveSecurityRepo;
        for (CurveDefinition value : curveRepo.findAll()) {
            curveDefinitionMap.put(value.getName(), value);
        }
        this.portfolioRepository = portRepo;

        // TODO:  This is not good, need to pre-load every definition
        //  (and may need to update the cache.)
        // TODO:  If I am doing this manually, hmm.  Why bother with Spring.
        // TODO:  Is there a way to tell it to load the entire repo on startup?
//        for(Portfolio portfolio : portRepo.findAll()) {
//            portMap.put(portfolio.getName(), portfolio);
//        }

    }

    @Override
    public void fetchCurveDefs(CurveDefRequest request, StreamObserver<CurveDefResponse> responseObserver) {

        // TODO:  Put this in a cache, perhaps, or cache in the Router

        CurveDefResponse.Builder response = CurveDefResponse.newBuilder();

        for (String curveName : request.getCurveNamesList()) {
            CurveDefinition curveDefinition = curveDefinitionMap.get(curveName);
            if (curveDefinition == null) {
                log.error("Curve Def requested, does not exist="+curveName);
                continue;
            }

            CurveDef.Builder curveDefBuilder = CurveDef.newBuilder();
            curveDefBuilder.setName(curveName);
            // get the points for it
            CurveSecurity cs = new CurveSecurity();
            cs.setCurveDefinition(curveDefinition);

            // TODO: may not work, what is Example.of?
            // ExampleMatcher.matchingAny() ?
            List<CurveSecurity> curveSec = curveSecurityRepository.findAll(Example.of(cs));
            // get every point for the security
            for (CurveSecurity curveSecurity : curveSec) {
                // TODO:  Change to a long
                Integer secId = curveSecurity.getSecurityId();
                // TODO: Get the fully dressed security?
                // TODO: Or is this, not on the curve.. (or not here yet, fill it out in the Router)
                curveDefBuilder.addPillarSecurityKeys(SecurityKey.newBuilder().setSecurityId(secId));
            }

            response.addCurveDef(curveDefBuilder.build());
            responseObserver.onNext(response.build());
        }

        responseObserver.onCompleted();
    }

    public void curveCreate(CurveDefinition result) {
        this.refreshCurveCache(result);
    }

    public void updateCurve(CurveDefinition result) {
        this.refreshCurveCache(result);
    }

    private void refreshCurveCache(CurveDefinition result) {
        log.error("Must implement, refresh Curve Cache.");
    }

}
