package com.hedgeos.service.refdata.web.rest;

import com.hedgeos.service.refdata.domain.Security;
import com.hedgeos.service.refdata.repository.RefDataSecurityRepository;
import com.hedgeos.service.refdata.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hedgeos.service.refdata.domain.Security}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SecurityResource {

    private final Logger log = LoggerFactory.getLogger(SecurityResource.class);

    private static final String ENTITY_NAME = "security";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RefDataSecurityRepository refDataSecurityRepository;

    public SecurityResource(RefDataSecurityRepository refDataSecurityRepository) {
        this.refDataSecurityRepository = refDataSecurityRepository;
    }

    /**
     * {@code POST  /securities} : Create a new security.
     *
     * @param security the security to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new security, or with status {@code 400 (Bad Request)} if the security has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/securities")
    public ResponseEntity<Security> createSecurity(@RequestBody Security security) throws URISyntaxException {
        log.debug("REST request to save Security : {}", security);
        if (security.getId() != null) {
            throw new BadRequestAlertException("A new security cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Security result = refDataSecurityRepository.save(security);
        return ResponseEntity.created(new URI("/api/securities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /securities} : Updates an existing security.
     *
     * @param security the security to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated security,
     * or with status {@code 400 (Bad Request)} if the security is not valid,
     * or with status {@code 500 (Internal Server Error)} if the security couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/securities")
    public ResponseEntity<Security> updateSecurity(@RequestBody Security security) throws URISyntaxException {
        log.debug("REST request to update Security : {}", security);
        if (security.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Security result = refDataSecurityRepository.save(security);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, security.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /securities} : get all the securities.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of securities in body.
     */
    @GetMapping("/securities")
    public ResponseEntity<List<Security>> getAllSecurities(Pageable pageable) {
        log.debug("REST request to get a page of Securities");
        Page<Security> page = refDataSecurityRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /securities/:id} : get the "id" security.
     *
     * @param id the id of the security to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the security, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/securities/{id}")
    public ResponseEntity<Security> getSecurity(@PathVariable Long id) {
        log.debug("REST request to get Security : {}", id);
        Optional<Security> security = refDataSecurityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(security);
    }

    /**
     * {@code DELETE  /securities/:id} : delete the "id" security.
     *
     * @param id the id of the security to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/securities/{id}")
    public ResponseEntity<Void> deleteSecurity(@PathVariable Long id) {
        log.debug("REST request to delete Security : {}", id);
        refDataSecurityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
