permission-service
-----------------
Manage permissions to any action, 
in any application, and restrict access to 
any data in the applications.
(across hedge-os but
not specific to this platform)

Included are detailed instructions,
for connecting to Windows Server kerberos 
from the docker image, for single sign on.

To demo, with an embedded Postgres Docker DB
(usable in production, mounts data volume)

- start.sh

API Details

- the API description is in "permission.proto"
- (in the API project, src/main/proto)
