# If you would like a real install of postgres, outside docker
sudo apt install postgresql postgresql-contrib
# test the install
sudo -u postgres psql -c "SELECT version();"
# unix system, add a user for the dbadmin
sudo adduser hedgeos-dbadmin
# switch to postgres user (should be super-user)
su - postgres
