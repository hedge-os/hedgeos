echo "this is destructive, to the local copy of permissions db"
cd ../..
docker-compose down
# Clean up the permission-db-storage, locally, testing usage only
sudo rm -rf ./permission-db-storage
docker-compose -f ./docker-compose.yml up -d --build
