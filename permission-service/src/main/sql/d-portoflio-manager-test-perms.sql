-- Note:  Examples here of setting up PMs using SQL, for migrations

-- a test pm
insert into public.users (username, first_name, last_name, start_date, end_date, manager_user_id, work_phone, emergency_phone, external_system_id, hr_key_id_a, hr_key_id_b, alt_email, primary_group_email, user_type)
values ('test-pm', 'Test PM', 'Test PM', '2020-06-02', null, null, null, null, null, null, null, null, null, null);

-- the 'test-pm', can see, the 'TEST_PORTFOLIO', in, portfolio viewer
insert into app_user (app_id, user_id, action_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, dt.data_type_id, 'TEST_PORTFOLIO'
from application a, users u, action c, data_type dt
where a.name = 'Portfolio Viewer'
  and u.username = 'test-pm'
  and c.action_type = 'READER'
  and dt.data_type_name = 'PORTFOLIO';

-- the 'test-pm', can see, the 'TEST_PORTFOLIO', in, portfolio viewer
insert into app_user (app_id, user_id, action_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, dt.data_type_id, 'TEST_PORTFOLIO'
from application a, users u, action c, data_type dt
where a.name = 'Trade Blotter'
  and u.username = 'test-pm'
  and c.action_type = 'READER'
  and dt.data_type_name = 'PORTFOLIO';

-- the 'test-pm', can see, the 'TEST_PORTFOLIO', in, portfolio viewer
insert into app_user (app_id, user_id, action_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, dt.data_type_id, 'TEST_PORTFOLIO'
from application a, users u, action c, data_type dt
where a.name = 'Trade Booking'
  and u.username = 'test-pm'
  and c.action_type = 'SUBMIT_TRADE'
  and dt.data_type_name = 'PORTFOLIO';

-- the 'test-pm', can see, the 'TEST_PORTFOLIO', in, REPORTING (general reports)
insert into app_user (app_id, user_id, action_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, dt.data_type_id, 'TEST_PORTFOLIO'
from application a, users u, action c, data_type dt
where a.name = 'Reporting'
  and u.username = 'test-pm'
  and c.action_type = 'READER'
  and dt.data_type_name = 'PORTFOLIO';

-- nmake a new feature to the platform called 'Delta Hedger', which only test-pm can use
insert into public.feature ( name) values ('Delta Hedger');

-- the 'test-pm', can see, the 'TEST_PORTFOLIO', in, REPORTING (general reports)
insert into app_user (app_id, user_id, action_id, feature_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, f.feature_id, dt.data_type_id, 'TEST_PORTFOLIO'
from application a, users u, action c, feature f, data_type dt
where a.name = 'Trade Booking'
  and u.username = 'test-pm'
  and c.action_type = 'WRITER'
  and f.name = 'Delta Hedger'
  and dt.data_type_name = 'PORTFOLIO';




