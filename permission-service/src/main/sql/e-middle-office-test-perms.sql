-- Note:  Examples here of setting up PMs using SQL, for migrations

-- a test middle office user
insert into public.users (username, first_name, last_name, start_date, end_date, manager_user_id, work_phone, emergency_phone, external_system_id, hr_key_id_a, hr_key_id_b, alt_email, primary_group_email, user_type)
values ('test-mo', 'Test MO', 'TestMO', '2020-06-02', null, null, null, null, null, null, null, null, null, null);


-- Middle Office can 'UPDATE_TRADE', for corrections to EVERYTHING
-- (note:  null data_value, removes any restrictions)
insert into app_user (app_id, user_id, action_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, dt.data_type_id, null
from application a, users u, action c, data_type dt
where a.name = 'Trade Blotter'
  and u.username = 'test-mo'
  and c.action_type = 'UPDATE_TRADE'
  and dt.data_type_name = 'EVERYTHING';

insert into app_user (app_id, user_id, action_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, dt.data_type_id, null
from application a, users u, action c, data_type dt
where a.name = 'Security Master'
  and u.username = 'test-mo'
  and c.action_type = 'WRITER'
  and dt.data_type_name = 'EVERYTHING';

insert into app_user (app_id, user_id, action_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, dt.data_type_id, null
from application a, users u, action c, data_type dt
where a.name = 'Ref Data Admin'
  and u.username = 'test-mo'
  and c.action_type = 'WRITER'
  and dt.data_type_name = 'EVERYTHING';

insert into app_user (app_id, user_id, action_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, dt.data_type_id, null
from application a, users u, action c, data_type dt
where a.name = 'Portfolio Viewer'
  and u.username = 'test-mo'
  and c.action_type = 'READER'
  and dt.data_type_name = 'EVERYTHING';

insert into app_user (app_id, user_id, action_id, data_type_id, data_value)
select a.app_id, u.user_id, c.action_id, dt.data_type_id, null
from application a, users u, action c, data_type dt
where a.name = 'Reporting'
  and u.username = 'test-mo'
  and c.action_type = 'READER'
  and dt.data_type_name = 'EVERYTHING';

