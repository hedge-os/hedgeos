-- generate the default actions
insert into public.action ( action_type) values ( 'PLATFORM_ADMIN');
insert into public.action ( action_type) values ( 'PERMISSIONS_ADMIN');
insert into public.action ( action_type) values ( 'APP_ADMIN');
insert into public.action ( action_type) values ( 'READER');
insert into public.action ( action_type) values ( 'WRITER');
insert into public.action ( action_type) values ( 'SUBMIT_TRADE');
insert into public.action ( action_type) values ( 'UPDATE_TRADE');
insert into public.action ( action_type) values ( 'SUBMIT_LOCATE');
insert into public.action ( action_type) values ( 'LOGIN');
insert into public.action ( action_type) values ( 'IMPERSONATION');
insert into public.action ( action_type) values ( 'APP_SUPPORT');
insert into public.action ( action_type) values ( 'CUSTOM_A');
insert into public.action ( action_type) values ( 'CUSTOM_B');
insert into public.action ( action_type) values ( 'CUSTOM_C');

-- data type enum
INSERT INTO public.data_type ( data_type_name) VALUES ('EVERYTHING');
INSERT INTO public.data_type ( data_type_name) VALUES ('PORTFOLIO');
INSERT INTO public.data_type ( data_type_name) VALUES ('PORTFOLIO_GROUP');
INSERT INTO public.data_type ( data_type_name) VALUES ('BUSINESS_LINE');
INSERT INTO public.data_type ( data_type_name) VALUES ('STRATEGY');
INSERT INTO public.data_type ( data_type_name) VALUES ('TRADES_ONLY');
INSERT INTO public.data_type ( data_type_name) VALUES ('AGGREGATE_ONLY');
INSERT INTO public.data_type ( data_type_name) VALUES ('SUB_STRATEGY');
INSERT INTO public.data_type ( data_type_name) VALUES ('BOOK');
INSERT INTO public.data_type ( data_type_name) VALUES ('SUB_BOOK');

-- admin user
insert into public.users (username, first_name, last_name, start_date, end_date, manager_user_id, work_phone, emergency_phone, external_system_id, hr_key_id_a, hr_key_id_b, alt_email, primary_group_email, user_type)
values ('hedgeos-admin', 'HedgeOS Admin', 'HedgeOS Admin', '2020-06-01', null, null, null, null, null, null, null, null, null, null);

-- generate the hedge-os list of applications
-- freely add more, manage other apps as well, not only hedge-os
insert into public.application (name, description, create_user_id, created_date, end_date)
values ('Permissions', 'Permissions across the platform', 1, '2020-06-01 02:16:17.684802', null);
insert into public.application (name, description, create_user_id, created_date, end_date)
values ('Trade Blotter', 'Trade review and edit', 1, '2020-06-01 02:17:46.437711', null);
insert into public.application (name, description, create_user_id, created_date, end_date)
values ('Security Master', 'Securities Editing', 1, '2020-06-01 02:19:26.893818', null);
insert into public.application (name, description, create_user_id, created_date, end_date)
values ('Trade Booking', 'Place orders', 1, '2020-06-01 02:19:26.893818', null);
insert into public.application (name, description, create_user_id, created_date, end_date)
values ('Reporting', 'General reporting', 1, '2020-06-01 02:20:39.976352', null);
insert into public.application (name, description, create_user_id, created_date, end_date)
values ('Ref Data Admin', 'Reference Data Editing', null, '2020-06-01 02:57:10.805811', null);

-- hedge-os enterprise app, portfolio viewer
insert into public.application ( name, description, create_user_id, created_date, end_date)
values ('Portfolio Viewer', 'P&L and positions monitoring', 1, '2020-06-01 02:29:01.392014', null);

-- the hedge-os admin needs, admin perms
insert into public.app_user (app_id, user_id, action_id, data_type_id, data_value)
values (1, 1, 1, null, null);

