create table users
(
    user_id             serial             not null
        constraint users_pk
            primary key,
    username            varchar(200)       not null,
    first_name          varchar(200),
    last_name           varchar(200),
    start_date          date default now() not null,
    end_date            date,
    manager_user_id     integer
        constraint users_manager_id_fk
            references users,
    work_phone          varchar(30),
    emergency_phone     varchar(100),
    external_system_id  integer,
    hr_key_id_a         varchar(100),
    hr_key_id_b         varchar(100),
    alt_email           varchar(200),
    primary_group_email varchar(100),
    user_type           varchar(30)
);

alter table users
    owner to hedgeos;

create unique index users_user_id_uindex
    on users (user_id);

create unique index users_username_uindex
    on users (username);

create table user_group
(
    group_id            serial       not null
        constraint user_group_pk
            primary key,
    group_name          varchar(200) not null,
    email_address       varchar(200),
    group_owner_user_id integer
        constraint user_group_owner_fk
            references users,
    parent_group_id     integer
        constraint user_group_parent_id_fk
            references user_group
);

alter table user_group
    owner to hedgeos;

create unique index user_group_group_id_uindex
    on user_group (group_id);

create unique index user_group_group_name_uindex
    on user_group (group_name);

create table group_map
(
    id       serial  not null
        constraint group_map_pk
            primary key,
    user_id  integer not null
        constraint group_map_users_fk
            references users,
    group_id integer not null
        constraint group_map_group_fk
            references user_group
);

comment on table group_map is 'users in groups';

alter table group_map
    owner to hedgeos;

create unique index group_map_id_uindex
    on group_map (id);

create unique index group_map_uindex
    on group_map (user_id, group_id);

create table application
(
    app_id         serial       not null
        constraint application_pk
            primary key,
    name           varchar(200) not null,
    description    varchar(200),
    create_user_id integer
        constraint application_created_user_fk
            references users,
    created_date   timestamp default now(),
    end_date       date
);

alter table application
    owner to hedgeos;

create unique index application_app_id_uindex
    on application (app_id);

create unique index application_name_uindex
    on application (name);

create table action
(
    action_id   serial       not null
        constraint action_pk
            primary key,
    action_type varchar(100) not null
);

comment on table action is 'what people can do';

alter table action
    owner to hedgeos;

create unique index action_action_id_uindex
    on action (action_id);

create unique index action_action_type_uindex
    on action (action_type);

create table feature
(
    feature_id serial       not null
        constraint feature_pk
            primary key,
    name       varchar(100) not null
);

comment on table feature is 'feature of the platform or apps';

alter table feature
    owner to hedgeos;

create unique index feature_feature_id_uindex
    on feature (feature_id);

create unique index feature_feature_name_uindex
    on feature (name);

create table data_type
(
    data_type_id   serial       not null
        constraint data_type_pk
            primary key,
    data_type_name varchar(100) not null
);

alter table data_type
    owner to hedgeos;

create unique index data_type_data_type_id_uindex
    on data_type (data_type_id);

create unique index data_type_data_type_name_uindex
    on data_type (data_type_name);

create table app_group
(
    id           serial  not null
        constraint app_group_pk
            primary key,
    app_id       integer not null
        constraint app_group_app_id_fk
            references application,
    group_id     integer not null
        constraint app_group_group_fk
            references user_group,
    action_id    integer not null
        constraint app_group_action_id_fk
            references action,
    feature_id   integer
        constraint app_group_feature_id_fk
            references action,
    data_type_id integer
        constraint app_group_data_type_fk
            references data_type,
    data_value   varchar(300)
);

comment on table app_group is 'groups use apps';

alter table app_group
    owner to hedgeos;

create unique index app_group_app_group_id_uindex
    on app_group (id);

create table app_user
(
    id           serial  not null
        constraint app_user_pk
            primary key,
    app_id       integer not null
        constraint app_user_app_id_fk
            references application,
    user_id      integer not null
        constraint app_user_user_id_fk
            references users,
    action_id    integer not null
        constraint app_user_action_id_fk
            references action,
    feature_id   integer
        constraint app_user_feature_id_fk
            references feature,
    data_type_id integer
        constraint app_user_data_type_fk
            references data_type,
    data_value   varchar(300)
);

alter table app_user
    owner to hedgeos;

create unique index app_user_id_uindex
    on app_user (id);

