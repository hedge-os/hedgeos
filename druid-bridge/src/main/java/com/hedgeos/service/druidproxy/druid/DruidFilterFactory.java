package com.hedgeos.service.druidproxy.druid;

import com.google.gson.Gson;
import com.hedgeos.ui.api.aggrid.ColumnFilter;
import com.hedgeos.ui.api.aggrid.NumberColumnFilter;
import com.hedgeos.ui.api.aggrid.SetColumnFilter;
import com.hedgeos.ui.api.aggrid.StringColumnFilter;
import in.zapr.druid.druidry.filter.*;
import in.zapr.druid.druidry.query.config.SortingOrder;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
public class DruidFilterFactory {

    // Note: Case insensitive search via lower case
    // >= 0 means it matched in the string
    // Note:  Druid JavaScript MUST BE ENABLED for this to work.
    static String JS_CONTAINS_FUNC = "function (x) { return x != null && x.toLowerCase().indexOf('@VALUE') >= 0 }";
    // < 0, means it is not in the string in JS indexOf
    static String JS_NOT_CONTAINS_FUNC = "function (x) { return x == null || x.toLowerCase().indexOf('@VALUE') < 0 }";
    // Note:  used because, this is case insensitive equals
    static String JS_EQUALS_FUNC = "function (x) { return x != null && x.toLowerCase() == '@VALUE' } ";
    static String JS_NOT_EQUALS_FUNC = "function (x) { return x != null && x.toLowerCase() != '@VALUE' } ";
    // starts and ends with
    static String JS_STARTS_WITH = "function (x) { return x != null && x.toLowerCase().startsWith('@VALUE') } ";
    static String JS_ENDS_WITH = "function (x) { return x != null && x.toLowerCase().endsWith('@VALUE') } ";
    // in a list of values
    static String JS_IN_FILTER = "function (x) { let myArray = [@VALUE]; return myArray.includes(x); } ";

    public static DruidFilter makeDruidFilter(String columnName, ColumnFilter agGridFilter) {

        if (agGridFilter instanceof SetColumnFilter) {
            SetColumnFilter scf = (SetColumnFilter) agGridFilter;
            return new InFilter(columnName, scf.getValues());
        }

        if (agGridFilter instanceof StringColumnFilter) {
            StringColumnFilter scf = (StringColumnFilter) agGridFilter;
            if (scf.getType().equals("contains")) {
                // Note: case insensitive match via lower case on both the value and the filter
                String javaScriptFunction = JS_CONTAINS_FUNC.replace("@VALUE", scf.getFilter().toLowerCase());
                return new JavaScriptFilter(columnName, javaScriptFunction);
            }

            if (scf.getType().equals("not contains")) {
                String jsFunction = JS_NOT_CONTAINS_FUNC.replace("@VALUE", scf.getFilter().toLowerCase());
                return new JavaScriptFilter(columnName, jsFunction);
            }

            if (scf.getType().equals("equals")) {
                String jsFunction = JS_EQUALS_FUNC.replace("@VALUE", scf.getFilter().toLowerCase());
                return new JavaScriptFilter(columnName, jsFunction);
            }

            if (scf.getType().equals("not equals")) {
                String jsFunction = JS_NOT_EQUALS_FUNC.replace("@VALUE", scf.getFilter().toLowerCase());
                return new JavaScriptFilter(columnName, jsFunction);
            }

            if (scf.getType().equals("starts with")) {
                String jsFunc = JS_STARTS_WITH.replace("@VALUE", scf.getFilter().toLowerCase());
                return  new JavaScriptFilter(columnName, jsFunc);
            }

            if (scf.getType().equals("ends with")) {
                String jsFunc = JS_ENDS_WITH.replace("@VALUE", scf.getFilter().toLowerCase());
                return new JavaScriptFilter(columnName, jsFunc);
            }

            if (scf.getType().equals("in")) {
                // TODO:  Debug
                // need the fields in quotes
                String quotedInFilter =
                        Arrays.stream(scf.getFilter().split(","))
                                .map(x -> "\""+x+"\",")
                                .collect(Collectors.joining());
                String jsFunc = JS_IN_FILTER.replace("@VALUE", quotedInFilter);
                log.warn("In JS function="+jsFunc);
                return new JavaScriptFilter(columnName, jsFunc);
            }

            throw new RuntimeException("Unknown STRING function sent to makeDruidFilter="+((StringColumnFilter) agGridFilter).getFilter());
        }

        if (agGridFilter instanceof NumberColumnFilter) {
            NumberColumnFilter ncf = (NumberColumnFilter) agGridFilter;

            BoundFilter.BoundFilterBuilder builder = BoundFilter.builder().dimension(columnName);

            builder.ordering(SortingOrder.NUMERIC);

            switch (ncf.getType()) {
                case "equals":
                    return new SelectorFilter(columnName, ncf.getFilter());
                case "less than":
                    builder.upper("" + ncf.getFilter());
                    builder.upperStrict(true);
                    return builder.build();
                case "less than or equal":
                    builder.upper("" + ncf.getFilter());
                    return builder.build();
                case "greater than or equal":
                    builder.lower("" + ncf.getFilter());
                    return builder.build();
                case "greater than":
                    builder.lower("" + ncf.getFilter());
                    builder.lowerStrict(true);
                    return builder.build();
                case "not equal":
                    SelectorFilter filter = new SelectorFilter(columnName, ncf.getFilter());
                    return new NotFilter(filter); // not the equals
                default:
                    throw new RuntimeException("Unknown numeric filter function:" + ncf.getType());
            }
        }

        throw new RuntimeException("Unknown AG Filter Type="+new Gson().toJson(agGridFilter));
    }

}
