package com.hedgeos.service.druidproxy.workflow;

import com.hedgeos.mock.curve.DruidResultMock;
import com.hedgeos.mock.curve.RiskReportMockGenerator;
import com.hedgeos.position.RiskReportRow;
import com.hedgeos.thread.Sleep;
import com.sun.el.lang.ExpressionBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static com.hedgeos.mock.curve.PositionMockGenerator.generateDruidMockResult;
import static com.hedgeos.mock.curve.PositionMockGenerator.getMockEquities;
import static com.hedgeos.mock.curve.RiskReportMockGenerator.mockStockRiskReport;

@Component
@Slf4j
public class MockWorkflow {
    
    public static final String MOCK_US_RISK_REPORT_TOPIC = "US_CUBE";

    Properties props = new Properties();

    volatile boolean stopped = false;

    Producer<byte[], byte[]> producer;

    // store the fake snaps we generate
    public Map<Long, Long> snaps = new ConcurrentHashMap<>();

    public MockWorkflow(Environment env) {

        String kafkaServers = env.getProperty("spring.kafka.bootstrap-servers");
        log.warn("kafka Servers:"+kafkaServers);
        props.put("bootstrap.servers", kafkaServers);
        props.put("key.serializer", StringSerializer.class.getName());
        props.put("value.serializer", ByteArraySerializer.class.getName());
        props.put("client.id", "mock-producer-"+System.currentTimeMillis());
        producer = new KafkaProducer<>( props );

        // tuning (not necessary)
//        props.put("acks", "0");
//        props.put("retries", 1);
//        props.put("batch.size", 16384);
//        props.put("linger.ms", 1);
//        props.put("buffer.memory", 33554432);

        startWorkflow();
    }

    public void startWorkflow() {

        Runnable r = () -> {
            while (true) {
                if (stopped) {
                    log.warn("Stopped, exiting");
                }
                pnlReportLoop();
                log.warn("Sleeping 500 seconds, in main mock loop");
                Sleep.sleepSec(500);
            }
        };

        new Thread(r).start();
    }

    public void stop() {
        stopped = true;
    }


    public void pnlReportLoop() {
        long snap = System.currentTimeMillis();

        // TODO:  Soon, will call to the Report Router to get PnL
        // TODO:  Need to make a protobuf which is flat for Druid ingestion of the results,
        // TODO:  Run the report and push to it.

        // for now, write to Kafka something simple
        List<RiskReportRow.Builder> mockRisk = mockStockRiskReport();

        for (RiskReportRow.Builder mockPosition : mockRisk) {
            mockPosition.setSnap(snap);

            ProducerRecord<byte[], byte[]> message = new ProducerRecord<>(MOCK_US_RISK_REPORT_TOPIC, mockPosition.build().toByteArray());
            producer.send( message );
            System.out.println("message sent="+mockPosition.getBbTicker());
        }

        List<DruidResultMock> mockPositions = generateDruidMockResult();

        // Add koljas risk rows

        int RUNS = 1;

        for (int i=0; i< RUNS; i++) {
            List<RiskReportRow.Builder> mockRiskKolja =
                    RiskReportMockGenerator.mockStockForPositions(getMockEquities());

            for (RiskReportRow.Builder mockPosition : mockRiskKolja) {
                mockPosition.setSnap(snap);

                ProducerRecord<byte[], byte[]> message = new ProducerRecord<>(MOCK_US_RISK_REPORT_TOPIC, mockPosition.build().toByteArray());
                producer.send(message);
                System.out.println("message sent=" + mockPosition.getBbTicker());
            }
        }

        log.warn("Sending snap="+snap);


        producer.flush();

        this.snaps.put(snap, snap);

        // producer.close(); // don't forget this

        log.warn("Finished snap="+snap+", records="+mockPositions.size());
    }


}
