package com.hedgeos.service.druidproxy.druid;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DruidConfig {

    @Value("${spring.druid.host:localhost}")
    public String host;

    @Value("${spring.druid.port:8082}")
    public int druidPort;
}
