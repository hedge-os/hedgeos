package com.hedgeos.service.druidproxy.aggregator;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class HedgeOsDruidAggregator extends HedgeOsAggregator {

    public final in.zapr.druid.druidry.aggregator.DruidAggregator druidAggregator;
}
