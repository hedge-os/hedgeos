package com.hedgeos.service.druidproxy;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DruidBridgeApplication implements CommandLineRunner {


//    @Autowired
//    CurveServerGrpc curveServerGrpc;

    @Override
    public void run(String... args) throws Exception {

        // curveServerGrpc.startGrpc();
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(DruidBridgeApplication.class, args);
    }
}
