package com.hedgeos.service.druidproxy.websocket;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Detect a full snap is in Druid, tell the GUIs to refresh.
 */
@Slf4j
@Component
@Controller
@EnableScheduling
public class DruidNotificationController {

    public static final String DESTINATION = "/topic/snaps";

    // the US PnL
    public static final String US_CUBE = "US_CUBE";

    private final SnapBean latestSnaps = new SnapBean();

    @Autowired
    private SimpMessagingTemplate webSocket;

    public DruidNotificationController() {
        Runnable r = () -> {
            while (true) {
                try {
                    long usSnapSimulated = System.currentTimeMillis();
                    Thread.sleep(3000);
                    PerCubeSnap snap = new PerCubeSnap(US_CUBE, usSnapSimulated);
                    latestSnaps.snapsSet.add(snap);

                    // push to every websocket
                    webSocket.convertAndSend(DESTINATION, snap);
                }
                catch (Exception e) {
                    log.error("bad websocket send", e);
                }
            }
        };

        new Thread(r).start();

    }

    @RequiredArgsConstructor
    public static class PerCubeSnap implements Message<String> {
        final String cubeName;
        final long latestSnapId;

        @Override
        public String getPayload() {
            return new Gson().toJson(this);
        }

        @Override
        public MessageHeaders getHeaders() {
            return new MessageHeaders(new HashMap<>());
        }
    }

    public static class SnapBean {
        // no need to sort, but we do to keep it organized
        final List<PerCubeSnap> snapsSet = new ArrayList<>();
    }

    // when you connect, you get the latest snaps
    @MessageMapping("/hello")
    @SendTo(DESTINATION)
    public String call() throws Exception {
        Thread.sleep(300); // simulated delay

        return new Gson().toJson(latestSnaps);
        // return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }

}
