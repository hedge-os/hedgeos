package com.hedgeos.service.druidproxy.rest;

import com.google.gson.Gson;
import com.google.protobuf.util.JsonFormat;
import com.hedgeos.mock.curve.DruidResultMock;
import com.hedgeos.mock.curve.PositionMockGenerator;
import com.hedgeos.position.PositionAndMeta;
import com.hedgeos.service.druidproxy.druid.DruidMaxSnap;
import com.hedgeos.ui.api.aggrid.DataResult;
import com.hedgeos.service.druidproxy.workflow.MockWorkflow;
import com.hedgeos.thread.Sleep;
import com.hedgeos.ui.api.aggrid.ServerSideGetRowsRequest;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.hedgeos.mock.curve.PositionMockGenerator.generateDruidMockResult;

/**
 * Useful for testing without Druid running.
 */
@RestController
public class MockDruidBridgeController {

    private final MockWorkflow mockWorkflow;

    public MockDruidBridgeController(MockWorkflow mockWorkflow, DruidMaxSnap dmx)
    {
        this.mockWorkflow = mockWorkflow;

        // push from the mock workflow to Druid Max Snap
        Runnable r = () -> {
            while (true) {
                for (Long value : mockWorkflow.snaps.values()) {
                    dmx.pushMockSnap(
                            MockWorkflow.MOCK_US_RISK_REPORT_TOPIC, value);
                }
                Sleep.sleepSec(3);
            }
        };

        new Thread(r).start();
    }

    @GetMapping("/stopMock")
    public void stopMock() {
        mockWorkflow.stop();
    }

    @SneakyThrows
    @RequestMapping(method = RequestMethod.POST, value = "/getRowsMock")
    public ResponseEntity<String> getRowsMock(@RequestBody ServerSideGetRowsRequest request) {

        PositionAndMeta pam = PositionMockGenerator.generatePositionAndMeta();
        // PositionAndMeta pam = PositionMockGenerator.generatePositionAndMeta();

        String pamJson = JsonFormat.printer().print(pam);
        List<DruidResultMock> mockPositions = generateDruidMockResult();

        List<String> jsonRows = new ArrayList<>();
        jsonRows.add(pamJson);

        for (DruidResultMock mockPosition : mockPositions) {
            jsonRows.add(new Gson().toJson(mockPosition));
        }


        int lastRow = jsonRows.size();

        DataResult dataResult = new DataResult(jsonRows, lastRow, new ArrayList<>());
        return new ResponseEntity<>(DataResult.asJsonResponse(dataResult), HttpStatus.OK);
    }

}
