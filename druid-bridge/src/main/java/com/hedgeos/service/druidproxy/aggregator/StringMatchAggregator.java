package com.hedgeos.service.druidproxy.aggregator;

import in.zapr.druid.druidry.aggregator.DruidAggregator;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper = true)
public class StringMatchAggregator extends DruidAggregator {

    private static final String STRING_MATCH = "stringMatch";
    public String fieldName;

    public StringMatchAggregator(String name, String fieldName) {
        this.type = STRING_MATCH;
        this.name = name;
        this.fieldName = fieldName;
    }
}
