package com.hedgeos.service.druidproxy.druid;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.hedgeos.service.druidproxy.aggregator.HedgeOsAggregator;
import com.hedgeos.service.druidproxy.aggregator.HedgeOsDruidAggregator;
import com.hedgeos.service.druidproxy.aggregator.StringMatchAggregator;
import com.hedgeos.service.druidproxy.rest.DruidRequest;
import com.hedgeos.thread.Sleep;
import com.hedgeos.ui.api.aggrid.ColumnFilter;
import com.hedgeos.ui.api.aggrid.ColumnVO;
import com.hedgeos.ui.api.aggrid.ServerSideGetRowsRequest;
import in.zapr.druid.druidry.aggregator.DoubleMaxAggregator;
import in.zapr.druid.druidry.aggregator.DoubleSumAggregator;
import in.zapr.druid.druidry.aggregator.DruidAggregator;
import in.zapr.druid.druidry.aggregator.StringLastAggregator;
import in.zapr.druid.druidry.client.DruidConfiguration;
import in.zapr.druid.druidry.client.DruidJerseyClient;
import in.zapr.druid.druidry.client.DruidQueryProtocol;
import in.zapr.druid.druidry.dataSource.DataSource;
import in.zapr.druid.druidry.dataSource.TableDataSource;
import in.zapr.druid.druidry.dimension.DruidDimension;
import in.zapr.druid.druidry.dimension.SimpleDimension;
import in.zapr.druid.druidry.filter.AndFilter;
import in.zapr.druid.druidry.filter.DruidFilter;
import in.zapr.druid.druidry.filter.SelectorFilter;
import in.zapr.druid.druidry.granularity.Granularity;
import in.zapr.druid.druidry.granularity.PredefinedGranularity;
import in.zapr.druid.druidry.granularity.SimpleGranularity;
import in.zapr.druid.druidry.query.aggregation.DruidGroupByQuery;
import in.zapr.druid.druidry.query.config.Interval;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.StringReader;
import java.util.*;
import java.util.stream.Collectors;

import static com.hedgeos.classpath.CpUtil.loadFromClasspath;
import static java.util.stream.Collectors.toList;

@Component
@Slf4j
public class DruidQueryAdapter {
    // TODO:  Defaulted, move to the request itself based on a choice
    public static final String CUBE_NAME = "US_CUBE";
    // comes from the classpath src/main/resources/aggregator.properties
    String AGG_PROPS_FILE = "aggregation.properties";

    // the props file is parsed into this aggregator config which is two maps
    private final AggregatorConfig aggregatorConfig;

    // holds the max snap available once published
    private final DruidMaxSnap druidMaxSnap;
    private final DruidConfig druidConfig;

    static final int MAX_STRING_BYTES = 1024*1024; // a meg per string for comparisons

    // we always need full granularity (never roll up prior to the query)
    static final Granularity ALL_GRANULARITY = new SimpleGranularity(PredefinedGranularity.ALL);

    // important, the HTTP client making Druid Calls
    private DruidJerseyClient druidClient;

    public DruidQueryAdapter(DruidMaxSnap druidMaxSnap,
                             DruidConfig druidConfig)
    {
        this.druidMaxSnap = druidMaxSnap;
        aggregatorConfig = loadAggregators();

        this.druidConfig = druidConfig;
    }

    // druid may not be up, in which case, connect to it again
    @PostConstruct
    public void connectToDruid() {

        DruidConfiguration druidConfiguration =
                DruidConfiguration.builder()
                        .protocol(DruidQueryProtocol.HTTP)
                        .port(druidConfig.druidPort)
                        .host(druidConfig.host).endpoint("druid/v2")
                        .concurrentConnectionsRequired(1000)
                        .build();

        druidClient = new DruidJerseyClient(druidConfiguration);

        Runnable r = () -> {
            while (true) {
                try {
                    druidClient.connect();
                    Sleep.sleepMin(10);
                    return; // exit once connected
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("Exception connecting to druid=" + e);
                }
            }
        };

        // keep trying to reconnect every ten minutes
        new Thread(r).start();
    }

    public JsonArray getRows(ServerSideGetRowsRequest request) {

        // TODO:  need to do the total row
        // TODO:  Add the cube to the request (handle other regions, multliple cubes)
        DruidRequest druidRequest = new DruidRequest(request, CUBE_NAME, false);

        JsonArray rows = getDataWithCubeType(druidRequest);
        return rows;
    }

    public JsonArray getDataWithCubeType(DruidRequest druidRequest) {
        // TODO:  Multiple cubes
        druidRequest.snap = druidMaxSnap.getLatestSnap(druidRequest.cubeName);

        return executeDruidQuery(druidRequest);
    }

    private JsonArray executeDruidQuery(DruidRequest druidRequest) {
        log.info("Start of executeDruidQuery="+new Gson().toJson(druidRequest));

        ServerSideGetRowsRequest agGridRequest = druidRequest.agGridRequest;
        // what grouping are we in (Business line)
        List<String> groupKeys = agGridRequest.getGroupKeys();
        // what is rolled up here (Portfolio, Book, Security)
        List<String> rowGroups = agGridRequest.getRowGroupCols().stream().map(ColumnVO::getField).collect(toList());

        // filters if any
        Map<String, ColumnFilter> filterModel = agGridRequest.getFilterModel();

        Interval interval = makeIntervalMatchingSnap(druidRequest);

        // snap is a long, and we limit here again (despite the interval doing the same thing.)
        // limiting to one snap
        // TODO:  Is this necessary with the Interval there
        DruidFilter snapFilter = new SelectorFilter("snap", druidRequest.snap);

        List<DruidFilter> columnFilters = getColumnFilters(rowGroups, groupKeys);
        columnFilters.add(snapFilter);

        // drill in to one beyond the group level, dimension wise
        int groupLevel = agGridRequest.getGroupKeys().size() + 1;

        Map<String, DruidDimension> dimensionMap = makeDimensionsForOpenGroupers(druidRequest, groupLevel);

        // what values does Druid serve up
        Collection<ColumnVO> druidColumns = onlyDruidColumns(druidRequest.agGridRequest.getValueCols());

        List<DruidAggregator> aggregators =
                makeDruidAggregators( druidColumns, dimensionMap, druidRequest.isTotalRow);

        List<DruidFilter> dimensionFilters = makeDimensionFilters(filterModel);

        columnFilters.addAll(dimensionFilters);

        // make a set of the columns we need
        // We should already know this but it may be filtered down on Aggregators we don't understand yet
        Set<String> colsUsed = aggregators.stream().map(DruidAggregator::getName).collect(Collectors.toSet());
        colsUsed.addAll(dimensionMap.keySet());

        List<DruidFilter> completeFilterSet = new ArrayList<>();

        // AND relationship of the dimension filters
        DruidFilter andTheDimensionFilters = new AndFilter(columnFilters);
        completeFilterSet.add(andTheDimensionFilters);

        // TODO:  Permissions filters by Portfolio
        // TODO:  Ask the permissions library or system, what Portfolios this user can see

        // interesting, there is a UNION Data Source..
        DataSource tableDataSource = new TableDataSource(druidRequest.cubeName);

        // finally, make the Druid Group By Query
        DruidGroupByQuery.DruidGroupByQueryBuilder groupByQueryBuilder =
                DruidGroupByQuery.builder()
                        .dataSource(tableDataSource)
                        .dimensions(new ArrayList<>(dimensionMap.values()))
                        .granularity(ALL_GRANULARITY).aggregators(aggregators)
                        .intervals(Lists.newArrayList(interval));

        if (!completeFilterSet.isEmpty()) {
            groupByQueryBuilder.filter(andTheDimensionFilters);
        }

        DruidGroupByQuery groupByQuery = groupByQueryBuilder.build();

        JsonArray response = runQuery(groupByQuery);

        return response;
    }

    private JsonArray runQuery(DruidGroupByQuery groupByQuery) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String jsonRequest = objectMapper.writeValueAsString(groupByQuery);
            long start = System.currentTimeMillis();

            // TODO:  Change back to debug
            log.warn("Running Druid Request={}",jsonRequest);

            String response = druidClient.query(groupByQuery);
            JsonArray responseArray = new Gson().fromJson(response, JsonElement.class).getAsJsonArray();
            long took = System.currentTimeMillis()-start;

            log.info("Druid query took(ms)="+took);
            return responseArray;
        }
        catch (Exception e) {
            log.error("Exception running Druid Query="+e);
            throw new RuntimeException(e);
        }

    }

    private List<DruidFilter> makeDimensionFilters(Map<String, ColumnFilter> filterModel) {
        List<DruidFilter> filters = new ArrayList<>();

        for (Map.Entry<String, ColumnFilter> entry : filterModel.entrySet()) {
            DruidFilter filter =
                    DruidFilterFactory.makeDruidFilter(entry.getKey(), entry.getValue());
            filters.add(filter);
        }

        return filters;
    }

    private List<DruidAggregator> makeDruidAggregators(Collection<ColumnVO> druidColumns,
                                                       Map<String, DruidDimension> dimensionMap,
                                                       boolean isTotalRow) {

        // the results
        List<DruidAggregator> aggregators = new ArrayList<>();

        // don't duplicate the columns requested
        Set<String> usedColumns = new HashSet<>();

        Set<String> dimensionKeys = dimensionMap.keySet();

        // check each column in the request if it is intended for Druid
        for (ColumnVO druidColumn : druidColumns) {
            String fieldId = druidColumn.getId().trim();

            // protect against requesting the same thing twice
            if (usedColumns.contains(fieldId)) {
                log.warn("Requested column twice="+fieldId);
                continue;
            }

            // keep track we requested this
            usedColumns.add(fieldId);

            // if it is the total row and needs column magic, don't try to aggregate it
            if (isTotalRow && aggregatorConfig.columnMagicIncludedColumns.containsKey(fieldId)) {
                continue;
            }

            // can't add as both a dimension and an measure
            // (check if it is a dimension, then it is not a measure)
            if (dimensionMap.containsKey(fieldId))
                continue;

            if (!aggregatorConfig.aggregatorMap.containsKey(fieldId)) {
                log.warn("Column requested but no aggregator configured="+fieldId);
                continue;
            }

            // critical:  If the column is Quantity, it needs "security_id" or similar to continue
            if (isColumnIgnored(dimensionKeys, fieldId)) {
                continue;
            }

            // it either is not a column magic column, or it is and the correct additional field is there
            HedgeOsAggregator agg = aggregatorConfig.aggregatorMap.get(fieldId);
            // ignore if it not Druid aggregated (aggregated in this server directly)
            if (agg instanceof HedgeOsDruidAggregator) {
                HedgeOsDruidAggregator da = (HedgeOsDruidAggregator) agg;
                aggregators.add(da.druidAggregator);
            }
        }

        return aggregators;
    }

    private Collection<ColumnVO> onlyDruidColumns(List<ColumnVO> valueCols) {
        return valueCols.stream().filter(this::isDruidColumn).collect(Collectors.toList());
    }

    private boolean isDruidColumn(ColumnVO x) {
        return this.aggregatorConfig.aggregatorMap.containsKey(x.getField()) &&
                this.aggregatorConfig.aggregatorMap.get(x.getField()) instanceof HedgeOsDruidAggregator;
    }

    // Important; if this level grouper is "opened" in the Tree (vs closed, skipped)
    // Important; dimensions goes one beyond the group key size, the way AG Grid constructs the request
    private Map<String, DruidDimension> makeDimensionsForOpenGroupers(DruidRequest druidRequest,
                                                                      int groupLevel)
    {
        Map<String, DruidDimension> dimensionMap = new HashMap<>();
        // total row is the entire cube summated by the filters,
        // therefore, no "dimensions"; the group keys are the totals
        if (druidRequest.isTotalRow) {
            return new LinkedHashMap<>();
        }

        // walk down the entire set of groupers
        // and where it is less than the row groups open, add a dimension
        for (int i=0; i< groupLevel; i++) {
            if (i < druidRequest.agGridRequest.getRowGroupCols().size()) {
                // make the dimension
                ColumnVO dimension = druidRequest.agGridRequest.getRowGroupCols().get(i);
                dimensionMap.put(dimension.getField(), new SimpleDimension(dimension.getField()));
            }
        }

        return dimensionMap;
    }

    private List<DruidFilter> getColumnFilters(List<String> rowGroups, List<String> groupKeys) {
        if (rowGroups.size() == 0)
            return Lists.newArrayList();

        List<DruidFilter> filters = new ArrayList<>();
        for (int i=0; i< rowGroups.size(); i++) {
            if (i < groupKeys.size()) {
                String groupKey = groupKeys.get(i);
                SelectorFilter groupFilter = new SelectorFilter(rowGroups.get(i), groupKey);
                filters.add(groupFilter);
            }
        }
        return filters;
    }

    private Interval makeIntervalMatchingSnap(DruidRequest druidRequest) {
        // TODO:  Review it is really UTC
        DateTime startTime = new DateTime(druidRequest.snap, DateTimeZone.UTC);
        startTime = startTime.minusMillis(1);
        DateTime endTime = startTime.plusMillis(2);
        // TODO:  Not sure this works as the interval
        Interval interval = new Interval(startTime, endTime);
        return interval;
    }

    /**
     * Configures:
     * a) which aggregator to use,
     * b) when it may be used (when it is enabled by additional columns)
     */
    @RequiredArgsConstructor
    private static class AggregatorConfig {
        // a map of columnName to the AggregatorFunction (primarily Druid but can be others.)
        final Map<String, HedgeOsAggregator> aggregatorMap;
        // a map of columns which enable other columns to be queried
        // (e.g. quantity is enabled by "security_id" and others)
        final Map<String, Set<String>> columnMagicIncludedColumns;
    }

    @SneakyThrows
    private AggregatorConfig loadAggregators() {

        // two maps are generated from the properties file
        Map<String, HedgeOsAggregator> aggMap = new HashMap<>();
        Map<String, Set<String>> columnMagicEnableMap = new HashMap<>();

        String propsString = loadFromClasspath(AGG_PROPS_FILE);
        Properties columnNameToAggregatorFunction = new Properties();
        columnNameToAggregatorFunction.load(new StringReader(propsString));

        for (Object o : columnNameToAggregatorFunction.keySet()) {
            String colName = (String)o;
            String aggregatorFunctionAndColumns = (String) columnNameToAggregatorFunction.get(o);

            log.warn("Adding column aggregator="+colName+", function="+aggregatorFunctionAndColumns);

            StringTokenizer st = new StringTokenizer(aggregatorFunctionAndColumns, ",");

            String aggregatorFunction = st.nextToken();

            if (st.hasMoreTokens()) {
                // if there is a comma, it means this column needs another column to "turn it on"
                // "quantity=doubleSum,security_id"
                // means use the doubleSum, but only compute quantity, if security_id is also requested
                // because we never want to roll up quantity *across* different securities (only roll up by security)
                columnMagicEnableMap.computeIfAbsent(colName, k -> new HashSet<>());
                String colNeededToEnable = st.nextToken().trim();
                columnMagicEnableMap.get(colName).add(colNeededToEnable);

            }

            addToAggMap(aggMap, colName, aggregatorFunction);
        }

        return new AggregatorConfig(aggMap, columnMagicEnableMap);
    }

    private void addToAggMap(Map<String, HedgeOsAggregator> aggMap, String colName, String aggregatorFunction) {
        // set up the Aggregators in the aggregator map
        switch (aggregatorFunction) {
            case "doubleSum":
                aggMap.put(colName, new HedgeOsDruidAggregator(new DoubleSumAggregator(colName, colName)));
                break;
            case "stringCompare":
                aggMap.put(colName, new HedgeOsDruidAggregator(new StringMatchAggregator(colName, colName)));
                break;
            case "stringLast":
                StringLastAggregator.StringLastAggregatorBuilder builder = StringLastAggregator.builder();
                StringLastAggregator stringLastAggregator =
                        builder.name(colName).fieldName(colName).maxStringBytes(MAX_STRING_BYTES).build();
                aggMap.put(colName, new HedgeOsDruidAggregator(stringLastAggregator));
                break;
            case "doubleMax":
                aggMap.put(colName, new HedgeOsDruidAggregator(new DoubleMaxAggregator(colName, colName)));
                break;
        }
    }

    private boolean isColumnIgnored(Collection<String> dimensionsInRequest, String fieldRequested) {
        // if the column is not involved in columnMagic, then it is not ignored
        // meaning, send it to druid
        if (!aggregatorConfig.columnMagicIncludedColumns.containsKey(fieldRequested)) {
            return false;
        }

        // it is a column magic candidate.
        // ignore it, unless one of the other dimensions which makes the column we need "valid"
        // e.g. "security_id" makes the "quantity" column valid
        // then if "quantity" is requested, don't ignore if "security_id" was also Requested
        Set<String> stringMagicEnablesThisColumn = aggregatorConfig.columnMagicIncludedColumns.get(fieldRequested);
        boolean anyMatches = stringMagicEnablesThisColumn.stream().anyMatch(dimensionsInRequest::contains);

        // if any of the "enabling" columns like "security_id" was present when "quantity" was requested
        // then don't ignore this column
        if (anyMatches) {
            return false;
        }

        // e.g. "quantity" was requested but "security_id",
        //  "BB Ticker", (etc), columns which would make the quantity rows make sense, are not present
        // (because otherwise, we would roll up quantity, and we don't want to do this.)
        return true;
    }

}
