package com.hedgeos.service.druidproxy.websocket;

import java.util.Comparator;

public class PnlCubeSnapComparator implements Comparator<DruidNotificationController.PerCubeSnap> {
    @Override
    public int compare(DruidNotificationController.PerCubeSnap o1, DruidNotificationController.PerCubeSnap o2) {
        return o1.cubeName.compareTo(o2.cubeName);
    }
}
