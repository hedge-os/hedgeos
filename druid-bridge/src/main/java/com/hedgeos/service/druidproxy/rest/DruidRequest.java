package com.hedgeos.service.druidproxy.rest;

import com.hedgeos.ui.api.aggrid.ServerSideGetRowsRequest;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DruidRequest {

    public final ServerSideGetRowsRequest agGridRequest;
    public final String cubeName;
    public final boolean isTotalRow;

    // looked up and added at request time
    public Long snap;
}
