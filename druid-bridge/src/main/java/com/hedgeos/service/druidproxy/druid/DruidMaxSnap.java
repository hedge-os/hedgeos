package com.hedgeos.service.druidproxy.druid;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Slf4j
public class DruidMaxSnap {
    Map<String, TreeSet<Long>> cubeToSnaps = new ConcurrentHashMap<>();

    public Long getLatestSnap(String cubeName) {
        TreeSet<Long> completedSnaps = cubeToSnaps.get(cubeName);
        if (completedSnaps == null) {
            log.error("No snaps exist for cubeName="+cubeName);
            throw new RuntimeException("No snaps exist for cubeName="+cubeName);
        }

        return completedSnaps.last();
    }

    public void pushMockSnap(String cube, Long value) {
        if (cubeToSnaps.get(cube) == null) {
            cubeToSnaps.put(cube, new TreeSet<>());
        }

        cubeToSnaps.get(cube).add(value);
    }
}
