package com.hedgeos.service.druidproxy.rest;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DruidResponse {
    private final DruidRequest druidRequest;
    private final String data;
}
