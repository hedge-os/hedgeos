package com.hedgeos.service.druidproxy.rest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.hedgeos.service.druidproxy.druid.DruidQueryAdapter;
import com.hedgeos.ui.api.aggrid.ServerSideGetRowsRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@RequiredArgsConstructor
@Slf4j
public class DruidRequestCaller implements Callable<DruidResponse> {

    // TODO:  Debug Mode: Add the original request to the response
    private final ServerSideGetRowsRequest origRequest;
    private final DruidRequest druidRequest;
    private final DruidQueryAdapter adapter;

    @Override
    public DruidResponse call() {
        try {
            JsonArray data = adapter.getDataWithCubeType(druidRequest);

            return new DruidResponse(druidRequest, new Gson().toJson(data));
        }
        catch (Exception e) {
            log.error("Error making one druid request="+e+
                    ", orig="+new Gson().toJson(origRequest)+
                    ", druidRequest="+new Gson().toJson(druidRequest), e);
            throw new RuntimeException(e);
        }
    }
}
