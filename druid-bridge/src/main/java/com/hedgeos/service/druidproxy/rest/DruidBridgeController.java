package com.hedgeos.service.druidproxy.rest;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.hedgeos.service.druidproxy.druid.DruidQueryAdapter;
import com.hedgeos.ui.api.aggrid.DataResult;
import com.hedgeos.ui.api.aggrid.ServerSideGetRowsRequest;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@RestController
@Slf4j
public class DruidBridgeController {

    public final DruidQueryAdapter druidQueryAdapter;

    @SneakyThrows
    @RequestMapping(method = RequestMethod.POST, value = "/getRows")
    public ResponseEntity<String> getRows(@RequestBody ServerSideGetRowsRequest request) {

        try {

            List<String> jsonRows = new ArrayList<>();

            JsonArray jsonArray = druidQueryAdapter.getRows(request);

            // put this back to test without druid locally
//        for (DruidResultMock mockPosition : mockPositions) {
//            jsonRows.add(new Gson().toJson(mockPosition));
//        }

            for (JsonElement jsonElement : jsonArray) {
                jsonRows.add(jsonElement.getAsJsonObject().get("event").toString());
            }

            int rowCount = jsonRows.size();

            DataResult dataResult = new DataResult(jsonRows, rowCount, new ArrayList<>());
            return new ResponseEntity<>(DataResult.asJsonResponse(dataResult), HttpStatus.OK);
        }
        catch (Exception e) {
            System.err.println("Exception in getRows="+e);
            System.out.println("exception in getRows="+e);
            log.error("Exception in getRows="+e);
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


}