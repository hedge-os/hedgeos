// Don't use underscores in field names, Druid doesn't like it
let columnDefs = [
  { field: "businessLine", filter: 'number', rowGroup: true, enableRowGroup: true, enablePivot: false},
  { field: "portfolio", filter: 'text', rowGroup: true, enableRowGroup: true, enablePivot: false },
  { field: "book", enableRowGroup: true, enablePivot: false },
  { field: "strategy", enableRowGroup: true, rowGroup: true, enablePivot: false },
  { field: "secCurrency", filter: 'text', enablePivot: true, enableRowGroup: true },
  { field: "secCountry", filter: 'text', enablePivot: true, enableRowGroup: true },
  { field: "bbTicker", filter: 'text', rowGroup: true, enablePivot: true, enableRowGroup: true },
  { field: "securityId", filter: 'text', enableRowGroup: true, enablePivot: true},
  { field: "marketValue", type: "measure" },
  { field: "price", type: "measure" },
  { field: "quantity", type: "measure" },
];

let gridOptions = {
  columnTypes: {
    measure: {
      width: 150,
      enableValue: true,
      aggFunc: 'sum',
      allowedAggFuncs: ['sum']
    }
  },

  defaultColDef: {
    width: 180,
    filter: "agNumberColumnFilter",
    filterParams: {
      applyButton: true,
      newRowsAction: 'keep'
    }
  },
  enableSorting: true,
  enableFilter: true,
  columnDefs: columnDefs,
  enableColResize: true,
  rowModelType: 'serverSide',
  // bring back data 50 rows at a time
  cacheBlockSize: 100,
  rowGroupPanelShow: 'always',
  pivotPanelShow: 'always'
};

function ServerSideDatasource() {}

ServerSideDatasource.prototype.getRows = function (params) {
  let request = params.request;

  let jsonRequest = JSON.stringify(request, null, 2);
  console.log(jsonRequest);

  let httpRequest = new XMLHttpRequest();

  // TODO:  URL Sniffing
  // httpRequest.open('POST', 'http://localhost:8097/getRows');

  httpRequest.open('POST', 'http://18.216.22.242:8097/getRows');
  httpRequest.setRequestHeader("Content-type", "application/json");
  httpRequest.send(jsonRequest);
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState === 4 && httpRequest.status === 200) {
      let result = JSON.parse(httpRequest.responseText);
      params.successCallback(result.data, result.lastRow);

      updateSecondaryColumns(request, result);
    }
  };
};

// setup the grid after the page has finished loading
document.addEventListener('DOMContentLoaded', function () {
  let gridDiv = document.querySelector('#myGrid');
  new agGrid.Grid(gridDiv, gridOptions);
  gridOptions.api.setServerSideDatasource(new ServerSideDatasource());
});

let updateSecondaryColumns = function (request, result) {
  if (request.pivotMode && request.pivotCols.length > 0) {
    let secondaryColDefs = createSecondaryColumns(result.secondaryColumns);
    gridOptions.columnApi.setSecondaryColumns(secondaryColDefs);
  } else {
    gridOptions.columnApi.setSecondaryColumns([]);
  }
};

let createSecondaryColumns = function (fields) {
  let secondaryCols = [];

  function addColDef(colId, parts, res, isGroup) {
    if (parts.length === 0) return [];

    let first = parts.shift();
    let existing = res.find((r) => r.groupId === first);

    if (existing) {
      existing['children'] = addColDef(colId, parts, existing.children);
    } else {
      let colDef = {};
      if(isGroup) {
        colDef['groupId'] = first;
        colDef['headerName'] = first;
      } else {
        colDef['colId'] = colId;
        colDef['headerName'] = "sum(" + first + ")";
        colDef['field'] = colId;
      }

      let children = addColDef(colId, parts, []);
      children.length > 0 ? colDef['children'] = children : null;

      res.push(colDef);
    }

    return res;
  }

  fields.forEach(field => addColDef(field, field.split('_'), secondaryCols, true));
  return secondaryCols;
};