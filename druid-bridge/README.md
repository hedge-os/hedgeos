Current steps for installation (Druid + Druid Proxy)
1.  Docker for Windows
2.  docker-compose up (in the druid folder)
3.  Start Zookeeper for kafka
4.  Start Kafka
5.  Run "generate_druid_risk_report_desc.bat/sh"
   - this generates the desc file Druid needs to post
6.  "risk_report_cube.json" - copy to the Ingestion page
- Must set the IP address of your host in this file
- Copy paste to the Ingestion page and kick off Ingestion
7.  Start "DruidProxyApplication"
- This starts pushing to a topic US_CUBE
- Druid ingests from this topic into a Datasource "US_CUBE"
8.  Open "index.html" to see it pull the latest snap
- Need to make it push snaps and update on the websocket
